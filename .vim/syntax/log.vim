if exists("b:current_syntax")
	finish
endif
syn sync fromstart
syn region ffstring start="`"hs=s+1 end="`"he=e-1
syn keyword info II
syn keyword error EE
syn keyword warn WW
syn keyword crit CC
syn keyword dbg DB
syn keyword keyterms clicks subclicks
syn match numbers /\<[0-9]*\>/
hi spec ctermfg=43 guifg=#00d7af "rgb=0,215,175
hi ffstring ctermfg=62 guifg=#5f5fd7 "rgb=95,95,215
hi keyterms ctermfg=96 guifg=#875f87 "rgb=135,95,135
hi numbers ctermfg=100 guifg=#878700 "rgb=135,135,0
hi error ctermbg=160 guifg=#d70000 "rgb=215,0,0
hi warn ctermfg=148 guifg=#afd700 "rgb=175,215,0
hi info ctermfg=146 guifg=#afafd7 "rgb=175,175,215
hi crit ctermfg=212 guifg=#ff87d7 "rgb=255,135,215
hi dbg ctermfg=144 guifg=#afaf87 "rgb=175,175,135
