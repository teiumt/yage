if exists("b:current_syntax")
	finish
endif
syn sync fromstart
runtime! syntax/c.vim

syn keyword ff _8_u _8_i _8_s _16_u _16_i _16_s _32_u _32_i _32_s _64_u _64_i _64_s _int_u _int_s _int_i _int
syn keyword ff _flu_int_u _flu_size
syn keyword funcs mem_cpy mem_dup m_alloc m_free m_realloc mem_set str_len str_dup str_cpy bzero bcopy mem_cmp str_cmp
hi ff ctermfg=209 guifg=#ff875f "rgb=255,135,95
hi funcs ctermfg=136 guifg=#af8700 "rgb=175,135,0
