# include "linux/types.h"
# include "linux/stat.h"
# include "linux/fcntl.h"
# include "linux/unistd.h"
# include "io.h"
# include "string.h"
# include "linux/mman.h"
# include "linux/ioctl.h"
# include "system/errno.h"
# include "m_alloc.h"
# include "drm.h"
# include "system/string.h"
# include "linux/limits.h"
# include "strf.h"
//# include "linux/mkdev.h"
#define DRM_CARD "/dev/dri/card%u"
int static drm_open_minor(int __minor) {
	char buf[256];

	ffly_strf(buf, 255, DRM_CARD, __minor);
	int fd;
	if ((fd = open(buf, O_RDWR, 0)) == -1) {
		printf("failed to open, %s\n", buf);
	}

	return fd;
}

void drm_get_busid(char *__buf, _int_u __len, int __fd) {
	struct drm_unique u;
	u.unique_len = __len;
	u.unique = __buf;
	int r;
	r = drm_ioctl(__fd, DRM_IOCTL_GET_UNIQUE, &u);
	if (r == -1) {
		printf("ioctl failed.\n");
	}
	__buf[u.unique_len] = '\0';
}

void static
set_interface_vers(int __fd, struct drm_set_version *__ver) {
	drm_ioctl(__fd, DRM_IOCTL_SET_VERSION, __ver);
}

int drm_open_by_busid(_32_u __domain, _8_u __bus, _8_u __dev, _8_u __func) {
	int i;
	i = 0;
	for(;i != DRM_MAX_MINOR;i++) {
		int fd;
		fd = drm_open_minor(i);
		if (fd >= 0) {
			struct drm_set_version ver = {
				.drm_di_major = 1,
				.drm_di_minor = 4,
				.drm_dd_major = -1,
				.drm_dd_minor = -1
			};
			set_interface_vers(fd, &ver);
			char buf[256];
			drm_get_busid(buf, 255, fd);
			printf("drm device : '%s'\n", buf);
			_int_u dom, bus, dev, fc;
			struct scn_block blks[] = {
				{_scnb_h4, 4, 4, &dom},
				{_scnb_h2, 9, 2, &bus},
				{_scnb_h2, 12, 2, &dev},
				{_scnb_i, 15, 1, &fc}
			};

			ffly_scnf(4, buf, blks);
			if (dom == __domain && bus == __bus && dev == __dev && fc == __func) {
				printf("found matching drm device.\n");
				return fd;
			}
			close (fd);
		}
	}
}
int ffly_drm_open(char const *__file) {
	int fd;
	if ((fd = open(__file, O_RDWR, 0)) == -1) {
		printf("failed to open.\n");
	}
//	int fd;
//	drm_open_minor();
	return fd;
}

void ffly_drm_close(int __fd) {
	close(__fd);
}

int drm_ioctl(int __fd, unsigned long __req, void *__arg) {
	int ret;
	ret = ioctl(__fd, __req, __arg);
	return ret;
}

int drm_dma(int __fd, struct drm_dma_request *__req) {
	int r;

	r = ioctl(__fd, DRM_IOCTL_DMA, &__req->dma);

	return r;
}
/*
void static device_open(dev_t __dev, int __minor) {
	struct stat st;
	char path[256];
	ffly_strf(path, 255, "/dev/dri/card%u", __minor);
	if (stat(path, &st) == -1) {
		mknod(path, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IFCHR, __dev);
	}
}
*/
void* drm_map_add(int __fd, unsigned long __offset, _int_u __size, drm_map_type_t __maptype, drm_map_flags_t __flags) {
	struct drm_map map;
	mem_set(&map, 0, sizeof(map));
	map.offset = __offset;
	map.size = __size;
	map.type = __maptype;
	map.flags = __flags;
	int r;
	r = ioctl(__fd, DRM_IOCTL_ADD_MAP, &map);
	if (r == -1) {
		printf("map add failure, %s\n", strerror(errno));
	}

	return map.handle;
}

int drm_map_rm(int __fd, void* __handle) {
	struct drm_map map;
	mem_set(&map, 0, sizeof(map));
	map.handle = __handle;
	int r;
	r = ioctl(__fd, DRM_IOCTL_RM_MAP, &map);
	if (r == -1) {
		printf("map rm failure, %s\n", strerror(errno));
	}
	return r;
}


void drm_master_set(int __fd) {
	int r;
	r = ioctl(__fd, DRM_IOCTL_SET_MASTER, NULL);
	if (r == -1) {
		printf("failed to set master, %s\n", strerror(errno));
	}
}

void drm_master_drop(int __fd) {
	int r;
	r = ioctl(__fd, DRM_IOCTL_DROP_MASTER, NULL);
	if (r == -1) {
		printf("failed to drop master, %s\n", strerror(errno));
	}
}

int drm_get_magic(int __fd, int unsigned *__magic) {
	struct drm_auth auth;
	auth.magic = 0;
	int r;
	r = ioctl(__fd, DRM_IOCTL_GET_MAGIC, &auth);
	if (r == -1) {
		printf("failed to get magic, error: %s\n",strerror(errno));
	}
	printf("got magic: %u for FD: %u, %d\n", auth.magic, __fd, r);
	*__magic = auth.magic;
	return r;
}

int drm_auth_magic(int __fd, int unsigned __magic) {
	struct drm_auth auth;
	auth.magic = __magic;
	int r;
	r = ioctl(__fd, DRM_IOCTL_AUTH_MAGIC, &auth);
	if (r == -1) {
		printf("failed to auth, error: %s\n",strerror(errno));
	}
	printf("AUTH-MAGIC: %u.\n",__magic);
	return r;
}

void* drm_mmap(int __fd, _int_u __size, unsigned long __handle) {
	void *ptr;

	ptr = mmap(0, __size, PROT_READ|PROT_WRITE, MAP_SHARED, __fd, __handle);
}

void drm_unmap(void *__ptr, _int_u __size, int __fd) {
	munmap(__ptr, __size);
}

struct drm_buf_map* drm_bufs_map(int __fd) {
	int r;

	struct drm_buf_map *bufs;

	bufs->count = 0;
	bufs->virt = NULL;
	bufs = (struct drm_buf_map*)m_alloc(sizeof(struct drm_buf_map));
	r = ioctl(__fd, DRM_IOCTL_MAP_BUFS, bufs);

	_int_u i;
	i = 0;
	for(;i != bufs->count;i++) {
		struct drm_buf_pub *buf;
		buf = bufs->list+i;
	}
	return bufs;
}
#include "system/errno.h"
int drm_prime_fd_to_handle(int __fd, int __prime, _32_u *__handle) {
	struct drm_prime_handle args;
	mem_set(&args,0,sizeof(struct drm_prime_handle));
	args.fd = __prime;
	int r;
	r = ioctl(__fd, DRM_IOCTL_PRIME_FD_TO_HANDLE, &args);
	if (r<0) {
		printf("failed prime fd to handle, %s, prime: %u\n",strerror(errno),__prime);
		return -1;
	}
	printf("FD_TO_HANDLE: %d, HANDLE: %d\n", r,args.handle);
	*__handle = args.handle;
	return 0;
}

int drm_prime_handle_to_fd(int __fd, int *__prime, _32_u __handle, _32_u __flags) {
	struct drm_prime_handle args;
	mem_set(&args,0,sizeof(struct drm_prime_handle));
	int r;
	args.fd = -1;
	args.handle = __handle;
	args.flags = __flags;
	r  = ioctl(__fd, DRM_IOCTL_PRIME_HANDLE_TO_FD, &args);
	if (r<0) {
		printf("failed prime handle to fd, %s, handle: %u\n",strerror(errno),__handle);
		return -1;
	}
	printf("PRIME: %d, %d\n", args.fd,r);
	*__prime = args.fd;
	return 0;
}


int drm_bufs_unmap(struct drm_buf_map *__map) {

}

int ffly_drm_cmd(int __fd, unsigned long __cmd, void *__ptr, unsigned long __size, unsigned long __mask) {
	unsigned long req;

	req = DRM_IOC((DRM_IOC_READ|DRM_IOC_WRITE)&__mask, DRM_IOCTL_BASE, DRM_COMMAND_BASE+__cmd, __size);
	int r;
	r =  ioctl(__fd, req, __ptr);
	if (r<0) {
		printf("io control failure, %s.\n",strerror(errno));
		return -1;
	}
	return 0;
}

int ffly_drm_context_add(int __fd, drm_context_t *__handle) {
	struct drm_context ctx;
	bzero(&ctx, sizeof(struct drm_context));
	if (drm_ioctl(__fd, DRM_IOCTL_ADD_CTX, &ctx) == -1)
		return -1;
	*__handle = ctx.handle;
	return 0;
}

int ffly_drm_context_rm(int __fd, drm_context_t __handle) {
	struct drm_context ctx;
	ctx.handle = __handle;
	if (drm_ioctl(__fd, DRM_IOCTL_RM_CTX, &ctx) == -1)
		return -1;
	return 0;
}

void
ffly_drm_pci_dev(struct drm_device *__dev,
	int __major, int __minor)
{
	char conf[PATH_MAX+1];
	char *p = conf;
	str_cpy(p, "/sys/dev/char");
	p+=13;
	*(p++) = '/';
	p+=ffly_nots(__major, p);
	*(p++) = ':';
	p+=ffly_nots(__minor, p);
	*(p++) = '/';
	str_cpy(p, "device/config");
	p+=13;
	*p = '\0';

	printf("config: %s\n", conf);
	int fd;
	fd = open(conf, O_RDONLY, 0);

	_8_u config[64];
	read(fd, config, 64);
	close(fd);

	__dev->devinfo.pci.vendor_id = *(_16_u*)config;
	__dev->devinfo.pci.device_id = *(_16_u*)(config+2); 
}
