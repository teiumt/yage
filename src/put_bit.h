# ifndef __ffly__put__bit__h
# define __ffly__put__bit__h
# include "y_int.h"
# include "types.h"
typedef struct ffly_putbit {
    _8_u buf;
    _8_u left;
    void(*out)(_8_u);
} *ffly_putbitp;
# define ffly_init_put_bit(__putbit, __out) \
    (__putbit)->buf = 0x0; \
    (__putbit)->left = 8; \
    (__putbit)->out = out;

void ffly_put_bit(ffly_putbitp, _8_u, _8_u);
# endif /*__ffly__put__bit__h*/
