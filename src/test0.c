#include "y_int.h"
#include "types.h"
void _h_init(){}
void _h_deinit(){}
void _h_loop(){}
void _h_msg() {}
void _h_tick(){}
#include "crypt.h"
#include "string.h"
#include "io.h"
#include "time.h"
#include "clock.h"
#include "tmu.h"
#include "crypt/rest.h"
#include "assert.h"
#include "rand.h"
#include "bin_tree.h"
#include "flue/common.h"
#include "tools.h"
#include "amdgpu.h"
#include "havoc/psp.h"
#include "havoc/common.h"
#include "thread.h"
#include "clay.h"
#include "clay/lexer.h"
static _8_s done = -1;
void static a_thread(void *__arg) {
	printf("hi there.\n");
	done = 0;
}
#include "amdgpu.h"
#include "havoc/common.h"
//rename to decompartamentalization
#include "test/maths/spatial_decomposition.c"
#include "afac/afac.h"
#include "lib.h"
void _h_draw(void){};
#include "vat.h"
#include "bog.h"
#include "piston.h"
#include "m/test0.c"
#include "opt.h"
#define POT_SIZE_SHIFT 23
#define POT_SIZE (1<<POT_SIZE_SHIFT)
#define POT_MASK (POT_SIZE-1)
#define POT_REAL_SIZE (POT_MASK^sizeof(struct pot))
#define BALE_POTFIT (POT_SIZE-(POT_SIZE/16))
#include "sched.h"
#include "slab_dealer.h"
_8_s static test_sched(_ulonglong __arg){
	printf("hello, %u\n",__arg);
	doze(0,__arg);
	return -1;
}
#include "file.h"
#include "wh.h"
#include "flue/test.c"
#include "flue/test/program/program.c"
static struct f_rw_struc rws_priv = {.funcs = &rws_file};
_y_err main(int __argc, char const *__argv[]) {
//	flue_test();

	test_program();
/*
	wh_init();

	_64_u st;
	wh_stat(21299,&st);
	printf("stat result: %x.\n",st);

	void *a;
	a = wh_open(21299);
	wh_stat(21299,&st);
	printf("stat result: %x.\n",st);
	if(!(st&WH_BACKING)){
		wh_backing(a,4096);
		_64_u count;
		count = 0;
		wh_write(a,&count,sizeof(_64_u),0);
	}

	_64_u count;
	wh_read(a,&count,sizeof(_64_u),0);
	
	printf("COUNTER: %u.\n",count++);
	wh_write(a,&count,sizeof(_64_u),0);

	if(count == 4){
		wh_backing(a,512);
	}

	wh_close(a);
	wh_deinit();
*/
/*
struct afac af;

	struct rws_schematic sch;
	sch.path = "untitled.obj";
	sch.funcs = &rws_file;
	afac_load(&af,&sch);

	struct afac_object *o;
	o = af.obj;
	while(o != NULL){
		afac_objdump(&af,o,AFAC_OBJDUMP_ONLY_OBJECTS_AND_GROUPS);
		o = o->next;
	}
*/
/*	void *test;
#define EXTENT_SIZE (9*POT_SIZE)
	test = m_alloc(EXTENT_SIZE);
	test = m_realloc(test,EXTENT_SIZE+4096);
	printf("memory pointer: %p.\n",test);
//	mem_set(test,0xff,EXTENT_SIZE+4096);

	m_free(test);
	*/
return 0;
//	ar_test();
/*
	struct afac_wf w;
	struct _y_vec3 vec[] = {
		{1.0,0.0,1.0},
		{1.0,0.0,1.0},
		{1.0,0.0,1.0},
		{1.0,0.0,1.0}
	};
	struct afac_blob blob = {.v = vec,.len=4};
	w.v = &blob;
	w.n_v = 1;
	struct afac_wf_face faces[] = {
		{0,0},
	};
	w.vf = faces;
	w.n_vf = 1;
	afac_wf_export(&w,"test.obj");
*/
//	spatial_decomposition();
/*	int fd;
	fd = open("/dev/dri/card0", O_RDWR, 0);
	struct amdgpu_dev *d;
	d = amdgpu_dev_init(fd);
	if(!d) {
		printf("failed to init device.\n");
	}else
		amdgpu_dev_de_init(d);

	close(fd);
*/	
/*	struct psp_node root;
	root.map = NULL;
	root.cnt = 0;
	_int_u y,x;
	float _x,_y;
	_y = 0;
	y = 0;
	for(;y != 512;y+=4) {
		x = 0;
		_x = 0;
		for(;x != 512;x+=4) {
			float buf[] = {
				x,y,0,0,
				x+4,y,0,0,
				x,y+4,0,0
			};
			psp_add((void*)x,&root,buf,buf+1,buf+2);
		}
	}

	_int_u i;
	void **list[64];
	_int_u cnt;
	cnt = psp_find(list,&root,1,1,0);

	i = 0;
	for(;i != cnt;i++) {
		void **p = list[i];
		_int_u j;
		j = 0;
		while(*(p+j) != NULL) {
			printf("###: %u, %u ~~ %u\n",i,j,(_64_u)*(p+j));
			j++;
		}
	}

	return;
*/
/*
	_64_u hitc[0x100];
	mem_set(hitc,0,0x100*sizeof(_64_u));
	_64_u i;
	i = 0;
#define N 1000000
	_64_u pre = N/100;
	_64_u r = 0;
	while(i != N) {
		_int_u n;
		_64_u buf[256];
		mem_set(buf,0,256*8);
		buf[0] = i;
		n = y_hash64(buf,32+(i&0xff));
		hitc[n&0xff]++;
		if ((i-(r*pre))>=pre) {
			printf("%u precent completem, %x:%x for %u\n",r, n, n>>32,i);
			r++;
		}
		i++;
	}

	i = 0;
	for(;i != 0x100;i++) {
		printf("HIT%u: %u_N\n",i,hitc[i]);
	}


	printf("%x, %x\n",y_hash64("k",1), y_hash64("k",1));
*/
/*	struct bin_tree tree;
	bt_init(&tree);
	_64_u i;
	i = 0;
#define N 20000
	_64_u r = N/100;
	_64_u cnt = 0;
	for(;i != N;i++) {
		bt_embed(&tree,i);
		if (i>=r) {
			r+=N/100;
			printf("completeion: %u.\n", cnt++);
		}
	}
	printf("finished.\n");
//	bt_find(&tree,2129);
//	bt_find(&tree,1019);
	printf("cleaning up.\n");
	bt_cleanup(&tree);
*/
}
