# include "mod.h"
# include "types.h"
# include "call.h"
# include "system/io.h"
# include "linux/mman.h"
# include "linux/sched.h"
# include "linux/unistd.h"
# include "linux/stat.h"
# include "linux/types.h"
# include "linux/signal.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/nanosleep.h"
# include "linux/wait.h"
# include "system/errno.h"
# include "system/string.h"
# include "mod/port.h"
# include "system/port.h"
# include "config.h"
# include "system/config.h"
# include "dep/str_cpy.h"
# include "system/mutex.h"
# include "system/errno.h"

/*
	TODO:
		pass load type option
		e.g.
			independent - load with its own prossess and we will talk back and forth
		-
			add dl option <- will have to imp
	TODO:
		remove clone and replace with fork?????

		test execve <- i dont know if the syscall will kill {clone, fork} proc 
*/
# define DSS 262144
void ffly_mod() {
	if (*sysconf_get(loaded) == -1)
		return;
	char const **modl;
	modl = *sysconf_get(modl);

	if (!*modl) return;

	char buf[1024];
	char *bufp = buf;
	bufp+=f_str_cpy(bufp, *sysconf_get(root_dir));
	*(bufp++) = '/';
	bufp+=f_str_cpy(bufp, *sysconf_get(moddir));
	*(bufp++) = '/';
	char const **mod = modl;

	while(*mod != NULL) {		
		f_str_cpy(bufp, *mod);	
		ffly_fprintf(ffly_log, "module path: %s\n", buf);
		if (access(buf, F_OK) == -1) {
			ffly_fprintf(ffly_log, "file access has been denied, or file doesent exist. skipping...\n", buf);
			goto _sk;	
		}
		ff_mod_load((char const*)buf);
	_sk:
		mod++;
	}
}

_8_i static shut = -1, done = -1;
mlock static lock = FFLY_MUTEX_INIT;
void ff_mod_init() {
	ff_mod_port_open();
}

void ff_mod_de_init() {
	shut = 0;
	while(done<0);
	ff_mod_port_close();
}

void static
execmod() {
	/*
		50/50 chance might get stuck
	*/
	ffly_nanosleep(2, 0); // replace this 
	char const *file;
	__asm__("movq -8(%%rbp), %%rdi\n\t"
		"movq %%rdi, %0": "=m"(file) : : "rdi");

	char buf[128];
	ffly_nots((_64_u)ffmod_port_shmid(), buf);
	printf("portno: %s\n", buf);
	char *argv[] = {(char*)buf, NULL};
	_32_s res = execve(file, (char*const)argv, NULL);
	if (res < 0) {
		printf("error, %s\n", strerror(errno));
	}

	exit(0);
}

void static
ffmod_printf(_32_u __band) {
	__ffmod_debug
		printf("printf.\n");
	ffpap p, bk;
	_f_err_t err;
	ffly_port_recv(__band, (void*)&p, sizeof(void*), ffmod_portno());

	ffcall(_ffcal_printf, NULL, &p);

	while(p != NULL) {
		__f_mem_free(p->p);
		bk = p;
		p = p->next;
		__f_mem_free(bk);
	}
}

void static
ffmod_malloc(_32_u __band) {
	__ffmod_debug
		printf("malloc.\n");
	_int_u bc;
	_f_err_t err;
	void *ret;
	ffly_port_recv(__band, &bc, sizeof(_int_u), ffmod_portno());
	__ffmod_debug
		printf("inbound, bc: %u\n", bc);

	ret = __f_mem_alloc(bc);
	ffly_port_send(__band, &ret, sizeof(void*), ffmod_portno());
	__ffmod_debug
		printf("outbound: %p\n", ret);
}

void static
ffmod_free(_32_u __band) {
	__ffmod_debug
		printf("free.\n");
	_f_err_t err;
	void *p;
	ffly_port_recv(__band, (void*)&p, sizeof(void*), ffmod_portno());
	__ffmod_debug
		printf("inbound, p: %p\n", p);
	__f_mem_free(p);
}

void static
ffmod_dcp(_32_u __band) {
	__ffmod_debug
		printf("dcp.\n");
	_f_err_t err;
	void *src;
	_int_u n;

	ffly_port_recv(__band, (void*)&src, sizeof(void*), ffmod_portno());
	ffly_port_recv(__band, &n, sizeof(_int_u), ffmod_portno());
	ffly_port_send(__band, src, n, ffmod_portno());
}

void static
ffmod_scp(_32_u __band) {
	__ffmod_debug
		printf("scp.\n");
	_f_err_t err;
	void *dst;
	_int_u n;

	ffly_port_recv(__band, (void*)&dst, sizeof(void*), ffmod_portno());
	ffly_port_recv(__band, &n, sizeof(_int_u), ffmod_portno());
	ffly_port_recv(__band, dst, n, ffmod_portno());
}

static void(*process[])(_32_u) = {
	ffmod_printf,
	NULL,
	ffmod_malloc,
	ffmod_free,
	NULL,
	ffmod_dcp,
	ffmod_scp
};

struct module {
	_8_u *stack;
	_32_u band;
	__linux_pid_t pid;
	_8_i skip;
};


struct module *mods[20];
struct module **mod = mods;

void ff_mod_port_open() {
	ffmod_port();
	printf("shm: %lu\n", ffmod_port_shmid());
}

void ff_mod_port_close() {
	ffmod_port_close();	
}

# include "dep/str_cmp.h"
void ff_mod_load(char const *__file) {
	mt_lock(&lock);
	struct module *m = (*(mod++) = (struct module*)__f_mem_alloc(sizeof(struct module)));
	printf("---> %p\n", m);
	*mod = NULL;
	m->skip = 0;
	m->stack = (_8_u*)__f_mem_alloc(DSS);
	printf("file: %s\n", __file);

	*(void**)(m->stack+(DSS-8)) = (void*)execmod;
	*(void**)(m->stack+(DSS-16)) = (void*)__file;

	if ((m->pid = clone(CLONE_VM|CLONE_SIGHAND|CLONE_FILES|CLONE_FS,
		(_64_u)(m->stack+(DSS-8)), NULL, NULL, 0)) == (__linux_pid_t)-1)
	{
		printf("error.\n");
		goto _fail;
	}

	printf("module pid: %u\n", m->pid);
	mt_unlock(&lock);

	ffly_port_recv(MOD_LISTEN, &m->band, sizeof(_32_u), ffmod_portno());
	printf("module band: %u\n", m->band);
	ffly_nanosleep(1, 0);
	m->skip = -1;
	printf("done.\n");
	return;
_fail:
	__f_mem_free(m->stack);
	__f_mem_free(m);
}

void static*
mod_listen(void *__arg) {
	struct module **cur;
	struct module *m;
	printf("listening.\n");
_again:
	cur = mods;
	while(mod != mods) {
		mt_lock(&lock);

		m = *cur;
		if (!m) {
			printf("error.\n");
			mt_unlock(&lock);
			break;
		}
		_8_u no;
		_f_err_t err;

		if (!m->skip) {
			goto _end;
		}
		if (ffly_port_band(ffmod_portno(), m->band) == -1)
			goto _end;

		printf("--> %p\n", m);
		printf("working on band: %u\n", m->band);

		ffly_port_recv(m->band, &no, 1, ffmod_portno());

		if (no != 0xff) {
			if (no <= _ffcal_mod_scp) {
				process[no](m->band);
			} else {
				printf("somthing broke.\n");
				if (kill(m->pid, SIGKILL) == -1)
					printf("could not kill.\n");
				__f_mem_free(m->stack);
				__f_mem_free(m);
				*cur = *(--mod);
			}
		}

		if (no == 0xff) {
			printf("got terminator, band: %u\n", m->band);
			printf("waiting for prossess to finish, %u\n", m->pid);
			if (wait4(m->pid, NULL, __WALL|__WCLONE, NULL) == -1) {
				printf("error, %s\n", strerror(errno));
			}

			__f_mem_free(m->stack);
			__f_mem_free(m);	
		
			if (cur != mod-1)
				*cur = *(--mod);
			else
				*(--mod) = NULL;
			printf("module was killed off.\n");
		}
	
	_end:
		if (cur >= mod-1)
			cur = mods;
		else
			cur++;
		mt_unlock(&lock);
	}

	if (shut<0)
		goto _again;
	printf("shuting down.\n");
	done = 0;
}

# include "system/thread.h"
void ff_mod_handle() {
	_f_tid_t tid;
	ffly_thread_create(&tid, mod_listen, NULL);
}

void ffmodldl(char const **__file) {
	char const **file = __file;
	while(*file != NULL) {
		ff_mod_load(*file);
		file++;
	}
}
//#define DEBUG
# ifdef DEBUG

# include "system/realpath.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	char *file;
	ffmodld(file = ffly_realpath("../modules/a.out"));
	__f_mem_free(file);
}
# endif
