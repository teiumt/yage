# ifndef __ffly__line__h
# define __ffly__line__h
# include "y_int.h"
# include "system/file.h"
extern _int l_cursor_pos;

# define ffly_l_forward \
	l_cursor_pos++
# define ffly_l_backward \
	l_cursor_pos--

void ffly_l_show(FF_FILE*);
void ffly_l_put(char);
void ffly_l_del(void);
_int_u ffly_l_load(char*);
void ffly_l_reset(void);
# endif /*__ffly__line__h*/
