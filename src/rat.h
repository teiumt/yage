# ifndef __ffly__rat__h
# define __ffly__rat__h
# include "y_int.h"
# include <stdarg.h>
/*
	getting infomation from hard places
*/
enum {
	_ff_rat_0,
	_ff_rat_1,
	_ff_rat_2
};

void ff_rat(_8_u, void(*)(char const*, va_list), char const*, ...);
void ff_rat_put(_8_u);
# endif /*__ffly__rat__h*/
