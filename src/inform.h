# ifndef __ffly__inform__h
# define __ffly__inform__h
# include "y_int.h"
#define _inform ffly_inform
enum {
	// memory allocator failure
	_INF_MAF
};
/*
	if we get an error or issue then inform the us or the user first.
*/
void(*ffly_inform)(_16_u);

// user should use this or if they want use ffly_inform
void(*ffly_inform_user)(_16_u);
# endif /*__ffly__inform__h*/
