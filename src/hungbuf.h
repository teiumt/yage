#ifndef __hungbuf__h
#define __hungbuf__h
/*
	this buffer can only insert at the end.
	nothing more thing less.

	NAMING: like coathangers they are 'hung' in place
*/
#include "y_int.h"
struct hb_hunk {
	void *ptr;
};
struct y_hungbuf {
	_int_u bsz;
	struct hb_hunk *hks;
	_int_u nhks;
	_int_u off;
};

void y_hbinit(struct y_hungbuf*,_int_u);
void* y_hungbuf_alloc(struct y_hungbuf*);
void* y_hungbuf_at(struct y_hungbuf*,_64_u);
#endif/*__hungbuf__h*/
