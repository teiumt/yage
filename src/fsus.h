# ifndef __fsus__h
# define __fsus__h
# include "y_int.h"
typedef struct f_fsus_node *f_fsus_nodep;
# include "system/fs_struc.h"
# include "fs.h"
typedef struct f_fsus_node {
	union {
		_32_u fd;
		struct f_vmfs_node *n;
	};
	struct ffly_fsop *ops;
} *f_fsus_nodep;

#define fsus_write(__m, ...) (__m)->f->write((__m)->ctx, __VA_ARGS__)
#define fsus_read(__m, ...) (__m)->f->read((__m)->ctx, __VA_ARGS__)
typedef struct f_fsus_f {
	void(*write)(void*, void*, _int_u, _64_u);
	void(*read)(void*, void*, _int_u, _64_u);
} *f_fsus_fp;

#define F_FSUS_FN 0x01
typedef struct f_fsus_m {
	f_fsus_fp f;
	void *ctx;
	_8_u flags;
} *f_fsus_mp;

# endif /*__fsus__h*/
