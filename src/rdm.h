# ifndef __f__rdm__h
# define __f__rdm__h
# include "y_int.h"
/* resin dismantler */
void rdm(void(*)(_int_u, _int_u, void*), _32_u, _32_u);
void rdmp(_8_u*, _32_u);
void rdmf(char const*, _32_u, _int_u);
# endif /*__f__rdm__h*/
