#include "crypt.h"
#include "crypt/rest.h"
#include "string.h"
static struct cpt_context *priv_cpt;
void cpt_init(void) {
}

void y_hash(void *__data, _int_u __length, void *__key) {
	struct rest_context ct;
	rest_init(&ct);
	rest_more(&ct,__data,__length);
	rest_final(&ct);
	
	mem_cpy(__key,ct.block,REST_BLKSIZE);
}

_64_u y_hash64(void *__data, _int_u __length) {
	struct rest_context ct;
	rest_init(&ct);
	rest_more(&ct,__data,__length);
	rest_final(&ct);
	return rest_sum64(&ct);
}

void y_encpt(void *__data, _int_u __length, void *__dst, void *__key) {
	rest_encpt(__data,__length,__dst,__key);
}

void y_decpt(void *__data, _int_u __length, void *__dst, void *__key) {
	rest_decpt(__data,__length,__dst,__key);
}
