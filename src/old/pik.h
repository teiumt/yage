# ifndef __f__pik__h
# define __f__pik__h
# include "y_int.h"
# include "system/file.h"
# include "net.h"

/*
	rws helper
*/
/*
	what does pik mean???

	nothing i chose a random set of characters

	pik will provide prebuild rws func set for user to use 
*/

# include "rws.h"

/*
	provides rws functions


	struct f_pik_func func;
	f_pik_load(&func, 0);

	struct f_rw_struc rws;

	rws.funcs = &func.rws;

	struct f_pik_file par;

	rws.arg = func.init((void*)&par);
	rws.funcs->write(rws.arg, NULL, 0);

	example:
	printf(&rws, "Hello number-%u\n", 2); -> rws->write "Hello number-2";

	this is built to work in compliance with system/io rws structure
*/

// params needed to init
struct f_pik_f_pipe {
	_int_u pipe;
};

struct f_pik_file {
	int fd;
};

struct f_pik_f_file {
	FF_FILE *f;
};

struct f_pik_f_nskt {
	FF_SOCKET *sock;
};

struct f_pik_func {
	void*(*init)(void*);
	void(*de_init)(void*);
	struct f_rw_funcs rws;

	/*
		say we are using this on a serial port
		and we want to change the speeds how ????

		we provide a struct thru here

		struct {
			// will contain other stuff might be union 
			speed
		}
		#define SETSPEED 0
		udf(SETSPEED, &struct)
	*/
	void(*udf)(_8_u, _ulonglong);
};

enum {
	_F_PIK_FILE,		//platform file
	_F_PIK_F_FILE,		//firefly file
	_F_PIK_F_NSKT,		//firefly network socket
	_F_PIK_F_PIPE		//firefly pipe
};

void f_pik_load(struct f_pik_func*, _8_u);
# endif /*__f__pik__h*/
