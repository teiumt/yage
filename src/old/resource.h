# ifndef __resource__h
# define __resource__h

typedef struct rs_loaf {
	_64_u off;
	_8_u bits;
} *rs_loafp;

typedef struct rs {
	int fd;
	_int_u lc;
	rs_loafp loafs;
} *rsp;

rsp rs_at(char const*);

# endif /*__resource__h*/
