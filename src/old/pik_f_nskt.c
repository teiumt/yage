# include "pik.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "net.h"
struct context {
	FF_SOCKET *sock;
};

static void* _init(struct f_pik_f_nskt *__par) {
	struct context *ctx;
	ctx = (struct context*)__f_mem_alloc(sizeof(struct context));
	ctx->sock = __par->sock;
	return ctx;
}

static void _de_init(struct context *__ctx) {
	__f_mem_free(__ctx);
}

static _32_u
_read(struct context *__ctx, void *__buf, _int_u __size, _8_s *__error) {
	_f_err_t err;
	ff_net_recv(__ctx->sock, __buf, __size, 0, &err);
	return 0;
}

static _32_u
_write(struct context *__ctx, void *__buf, _int_u __size, _8_s *__error) {
	_f_err_t err;
	ff_net_send(__ctx->sock, __buf, __size, 0, &err);
	return 0;
}

#define CAST (void*)
static struct f_pik_func func = {
	CAST _init,
	CAST _de_init,
	.rws = {
		NULL,
		NULL,
		CAST _read,
		CAST _write,
		NULL,
		NULL,
		NULL
	}
};

void f_pik_f_nskt_load(struct f_pik_func *__func) {
	*__func = func;
}
