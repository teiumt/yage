# include "wh.h"
# include "def.h"
# include "m_alloc.h"
# include "io.h"
# include "mutex.h"
# include "msg.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
# include "strf.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_WH)
#define REALM_MAX 20
struct slab;
struct realm {
	struct slab *sb_bin;
	int fd;
	_int_u rlm_n;
	_32_u off;
	mlock lock;
};
static struct realm rlms[REALM_MAX];
static struct realm *next = rlms;

static struct realm* realm_new(void) {
	struct realm *rlm;
	rlm = next++;
	rlm->off = 0;
	rlm->rlm_n = rlm-rlms;
	char path[256];
	ffly_strf(path, 255, "wh.rlm%u", rlm->rlm_n);
	rlm->sb_bin = NULL;
	rlm->fd = open(path, O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	rlm->lock = F_MUTEX_INIT;
	return rlm;
}

static void realm_destroy(struct realm *__rlm) {
	close(__rlm->fd);
}

struct realm static*
find_suitable_realm(void) {
	struct realm *rlm, *r;
	r = rlms;
	rlm = rlms+1;
	for(;rlm != next;rlm++) {
		if (rlm->off<r->off)
			r = rlm;
	}
	return r;
}

struct slab {
	_32_u off;
	struct slab *next;
};

#define slabsize sizeof(struct slab)
#define SLAB_SHIFT 4
#define SLAB_SIZE (1<<SLAB_SHIFT)
struct slab static*
slab_alloc(struct realm *__rlm) {
	struct slab *sb;
	mt_lock(&__rlm->lock);
	if (__rlm->sb_bin != NULL) {
		sb = __rlm->sb_bin;
		__rlm->sb_bin = sb->next;
		return sb;
	}

	sb = (struct slab*)m_alloc(slabsize);
	sb->off = __rlm->off;
	__rlm->off++;
	mt_unlock(&__rlm->lock);
	return sb;
}


void static
slab_free(struct slab *__sb, struct realm *__rlm) {
	mt_lock(&__rlm->lock);
	if (__sb->off = __rlm->off-1) {
		__rlm->off--;
		m_free(__sb);
		return;
	}
	__sb->next = __rlm->sb_bin;
	__rlm->sb_bin = __sb;
	mt_unlock(&__rlm->lock);
}

struct area {
	struct realm *rlm;
	struct slab **slabs;
	_int_u sb_c;
};

static _int_u rlm_n;
void wh_init(_int_u __realms) {
	_int_u i;
	for(i = 0;i!= __realms;i++) {
		realm_new();
	}

	rlm_n = __realms;
	
}

void wh_de_init(void) {
	_int_u i;
	for(i = 0;i!= rlm_n;i++) {
		realm_destroy(rlms+i);
	}
}

void* wh_alloc(_int_u __size) {
	struct area *a;
	a = (struct area*)m_alloc(sizeof(struct area));

	_int_u nsbs;
	nsbs = (__size+(SLAB_SIZE-1))>>SLAB_SHIFT;
	a->slabs = (struct slab**)m_alloc(nsbs*sizeof(struct slab*));
	a->sb_c = nsbs;
	a->rlm = find_suitable_realm();

	MSG(INFO, "allocation in realm-%u.\n", a->rlm->rlm_n)
	_int_u sb;
	for(sb = 0;sb != nsbs;sb++)
		a->slabs[sb] = slab_alloc(a->rlm);
	return a;
}

void wh_free(void *__p) {
	struct area *a;
	a = (struct area*)__p;
	_int_u sb;
	for(sb = 0;sb != a->sb_c;sb++)
		slab_free(a->slabs[sb], a->rlm);
	m_free(a);
}

void _read(struct realm *__rlm, void *__buf, _int_u __size, _32_u __offset) {
	pread(__rlm->fd, __buf, __size, __offset);
}

void static
_write(struct realm *__rlm, void *__buf, _int_u __size, _32_u __offset) {
    pwrite(__rlm->fd, __buf, __size, __offset);
}

void wh_read(void *__area, void *__buf, _int_u __size, _32_u __off) {
	struct area *a;
	a = (struct area*)__area;
	struct slab **sb;
	struct realm *rlm;
	rlm = a->rlm;
	_int_u ss, sb_off;
	ss = __off>>SLAB_SHIFT;
	sb_off = __off-(ss<<SLAB_SHIFT);
	_8_u *p;
	p = (_8_u*)__buf;
	sb = a->slabs+ss;

	if (sb_off>0 && __size>=SLAB_SIZE) {
		_int_u sz;
		_read(rlm, p, sz = (SLAB_SIZE-sb_off), ((*sb)->off<<SLAB_SHIFT)+sb_off);
		sb_off = 0;
		p+=sz;
		sb++;
	}

	if (sb_off+__size>SLAB_SIZE && __size<SLAB_SIZE) {
		_int_u a0, a1;
		a0 = SLAB_SIZE-sb_off;
		a1 = __size-a0;
		_read(rlm, p, a0, ((*sb)->off<<SLAB_SHIFT)+sb_off);
		_read(rlm, p+a0, a1, (*(sb+1))->off<<SLAB_SHIFT);
		return;
	}

	struct slab **end;
	end = sb+(__size>>SLAB_SHIFT);
	while(sb<end) {
		_read(rlm, p, SLAB_SIZE, (*sb)->off<<SLAB_SHIFT);
		sb++;
		p+=SLAB_SIZE;
	}

	_int_u left;
	left = __size-(p-(_8_u*)__buf);
	if (left>0) {
		_read(rlm, p, left, ((*sb)->off<<SLAB_SHIFT)+sb_off);
	}
}

void wh_write(void *__area, void *__buf, _int_u __size, _32_u __off) {
	struct area *a;
	a = (struct area*)__area;

	struct slab **sb;
	struct realm *rlm;
	rlm = a->rlm;
	_int_u ss, sb_off;
	ss = __off>>SLAB_SHIFT;
	sb_off = __off-(ss<<SLAB_SHIFT);
	_8_u *p;
	p = (_8_u*)__buf;
	sb = a->slabs+ss;

	if (sb_off>0 && __size>=SLAB_SIZE) {
		_int_u sz;
		_write(rlm, p, sz = (SLAB_SIZE-sb_off), ((*sb)->off<<SLAB_SHIFT)+sb_off);
		sb_off = 0;
		p+=sz;
		sb++;
	}

	if (sb_off+__size>SLAB_SIZE && __size<SLAB_SIZE) {
		_int_u a0, a1;
		a0 = SLAB_SIZE-sb_off;
		a1 = __size-a0;
		_write(rlm, p, a0, ((*sb)->off<<SLAB_SHIFT)+sb_off);
		_write(rlm, p+a0, a1, (*(sb+1))->off<<SLAB_SHIFT);
		return;
	}

	struct slab **end;
	end = sb+(__size>>SLAB_SHIFT);
	while(sb<end) {
		_write(rlm, p, SLAB_SIZE, (*sb)->off<<SLAB_SHIFT);
		sb++;
		p+=SLAB_SIZE;
	}

	_int_u left;
	left = __size-(p-(_8_u*)__buf);
	if (left>0) {
		_write(rlm, p, left, ((*sb)->off<<SLAB_SHIFT)+sb_off);
	}
}

//# include "sms.h"
void static
sms_read(_32_u __addr, void *__buf, _int_u __size, _ulonglong __arg) {
	wh_read((void*)__arg, __buf, __size, __addr);
}

void static
sms_write(_32_u __addr, void *__buf, _int_u __size, _ulonglong __arg) {
	wh_write((void*)__arg, __buf, __size, __addr);
}
/*
void f_wh_sms_map(_64_u __addr, _int_u __len, void *__area) {
	struct f_sms_area area = {
		(_ulonglong)__area,
		sms_read,
		sms_write
	};
	f_sms_map(__addr, __len, &area);
}*/
