# ifndef __f__crk__struc__h
# define __f__crk__struc__h
# include "y_int.h"
# include "rws.h"
struct crk_func_struc;
struct f_creek {
	void *ctx;
	struct crk_func_struc *funcs;
	struct f_creek *link;
	struct f_rw_struc rws;
	_32_u n;
};

struct crk_func_struc {
	void*(*init)(void*);
	void(*de_init)(void*);

	struct f_rw_funcs rws;

	// undefined functionality
	void(*udf)(_8_u, _ulonglong);
};

# endif /*__f__crk__struc__h*/
