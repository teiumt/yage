# include "resource.h"
# include "m_alloc.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
#define LOAF_SHFT 4
#define LOAF_SIZE (1<<LOAF_SHFT)
#define LOAD_MASK (LOAD_SIZE-1)

void static loaf(rsp __r, _int_u __size) {
	_int_u lc;
	lc = (__size+LOAD_MASK)>>LOAF_SHFT;
	__r->loafs = (rs_loafp)m_alloc(lc*sizeof(struct rs_loaf));
	rs_loafp l;
	_int_u i;
	i = 0;
	for(;i != lc;i++) {
		l = __r->loafs+i;
		l->bits = 0x00;
		l->off = 1<<LOAF_SHFT;
	}
	__r->lc = lc;
}

rsp rs_at(char const *__path) {
	rsp r;
	r = (rsp)m_alloc(sizeof(struct rs));
	r->fd = open(__path, O_RDONLY, 0);
	struct stat st;
	fstat(r->fd, &st);
	loaf(r, st.st_size);
	return r;
}
