#include "haze.h"
#include "m_alloc.h"
#include "string.h"
#include "io.h"
#include "system/flags.h"
#include "tools.h"
/*
	needs working on

	TODO:
	
*/
void* haze_rest_init(void);
void haze_rest_preform_hash(void*, struct haze_data*, struct f_cpt_out*);
void haze_rest_preform_enc(void*, struct haze_data*, struct f_cpt_out*);
void haze_rest_preform_dec(void*, struct haze_data*, struct f_cpt_out*);
extern struct haze_priv haze_rest_pv;


void haze_rest_de_init(void*);
struct haze_func_struc haze_funcs[1] = {
	{haze_rest_init, {haze_rest_preform_hash,haze_rest_preform_enc,haze_rest_preform_dec}, haze_rest_de_init,&haze_rest_pv}	
};


static void blt_loadblock(struct haze_block *__b, struct haze_data *__d) {
	__b->p = m_alloc(HAZE_BLOCK_SIZE);
	blt_read(__d->blt, __b->p, HAZE_BLOCK_SIZE, __b->offset);
}

static void blt_load(struct y_blt *blt, void *__buf, _int_u __length, _32_u __offset) {
	blt_read(blt, __buf, __length, __offset);
}
static void blt_store(struct y_blt *blt, void *__buf, _int_u __length, _32_u __offset) {
	blt_write(blt, __buf, __length, __offset);
}




struct haze_context*
haze_ctx_new(struct haze_func_struc *__funcs) {
	struct haze_func_struc *fns = __funcs;

	struct haze_context *c;
	c = (struct haze_context*)m_alloc(sizeof(struct haze_context));
	c->data.loadblock = blt_loadblock;
	c->data.load = blt_load;
	c->data.store = blt_store;
	c->top = NULL;
	c->funcs = fns;
	c->ctx = fns->init();
	c->off = 0;
	c->bufoff = 0;
	c->hasbuf = -1;
	c->data.blt = blt_new();
	return c;
}

void haze_ctx_destroy(struct haze_context *__ctx) {
	struct haze_func_struc *fns;
	fns = __ctx->funcs;
	fns->de_init(__ctx->ctx);
	m_free(__ctx);
}

void
haze_rest_encpt(void);
void haze_tick(_ulonglong __arg) {
	haze_rest_encpt();
}

void haze_more(struct haze_context *__ctx, void *__data, _int_u __length) {
//	return;
	_int_u nblks, left;
	nblks = __length>>HAZE_BLOCK_SHIFT;
	left = __length&HAZE_BLOCK_MASK;

	_8_u *src;
	src = (_8_u*)__data;
	_32_u off;
	off = __ctx->off;
	struct haze_block *b;

	if (!__ctx->hasbuf) {
		_int_u needs;
		needs = HAZE_BLOCK_SIZE-__ctx->bufoff;
		if (__length>=needs) {
			b = (struct haze_block*)m_alloc(sizeof(struct haze_block));
			b->next = __ctx->top;
			__ctx->top = b;
			b->size = HAZE_BLOCK_SIZE;
			b->offset = off;
			b->flags = 0x00;
			mem_cpy(__ctx->buf+__ctx->bufoff, src, needs);
			src+=needs;
			__ctx->data.store(__ctx->data.blt, __ctx->buf, HAZE_BLOCK_SIZE, off);
			off+=HAZE_BLOCK_SIZE;
			__ctx->bufoff = 0;
			left = 0;
		}
	}

	_int_u i;
	i = 0;
	for(;i != nblks;i++) {
		b = (struct haze_block*)m_alloc(sizeof(struct haze_block));
		__ctx->data.store(__ctx->data.blt, src, HAZE_BLOCK_SIZE, off);	
		b->offset = off;
		b->flags = 0x00;
		b->size = HAZE_BLOCK_SIZE;
		src+=HAZE_BLOCK_SIZE;
		off+=HAZE_BLOCK_SIZE;
		b->next = __ctx->top;
		__ctx->top = b;
	}

	__ctx->off = off;
	if (left>0) {
		__ctx->hasbuf = 0;
		mem_cpy(__ctx->buf+__ctx->bufoff, src, left);
		__ctx->bufoff+=left;
	}

}

void haze_final(struct haze_context *__ctx, void* __op, struct cpt_out *__out) {
	/*
		there is not point in storing this in billet
	*/
	if (__ctx->bufoff>0) {
		struct haze_block *b;
		b = (struct haze_block*)m_alloc(sizeof(struct haze_block));
		b->next = __ctx->top;
		__ctx->top = b;
		b->offset = ~0;
		b->p = __ctx->buf;
		b->size = __ctx->bufoff;
		b->flags = _HAZE_BLOCK_ALOAD;
		__ctx->bufoff = 0;
		printf("####");
		ffly_hexdump(__ctx->buf, HAZE_BLOCK_SIZE);
	}

	__ctx->data.b = __ctx->top;
	struct haze_block *b;
	b = __ctx->top;
	while(b != NULL) {
		printf("block: off: %u, size: %u.\n", b->offset, b->size);
		b = b->next;
	}

	if (!__ctx->top) {
		printf("no blocks.\n");
		return;
	}

//	((void(*)(void*, struct haze_data*, struct cpt_out*))__op)(__ctx->ctx, &__ctx->data, __out);
	__ctx->top = NULL;
	__ctx->off = 0;
	__ctx->bufoff = 0;
	__ctx->hasbuf = -1;
}
