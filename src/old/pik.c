# include "pik.h"
void f_pik_file_load(struct f_pik_func*);
void f_pik_f_file_load(struct f_pik_func*);
void f_pik_f_nskt_load(struct f_pik_func*);
void f_pik_f_pipe_load(struct f_pik_func*);
static void(*func_tbl[])(struct f_pik_func*) = {
	f_pik_file_load,
	f_pik_f_file_load,
	f_pik_f_nskt_load,
	f_pik_f_pipe_load
};

void f_pik_load(struct f_pik_func *__func, _8_u __what) {
	func_tbl[__what](__func);
}
