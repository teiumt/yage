#ifndef __y__tools__h
#define __y__tools__h
#include "y_int.h"
#include "iis.h"
#include "dem.h"
#include "rws.h"
void y_mat4print(float *m);
void y_floatprint(struct f_rw_struc *__dst, float *__f, _int_u __n);
void ffly_prbin(_8_u const*, _int_u);
void f_hexdump(struct f_iis*, _8_u*, _int_u);
void ffly_hexdump(_8_u*, _int_u);
void _f_chrdump(struct f_dem*, struct f_avs_page*, _int_u);
void f_chrdump(struct f_iis*, _8_u*, _int_u);
void ffly_chrdump(_8_u*, _int_u);
#endif /*__y__tools__h*/
