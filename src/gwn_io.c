# include "gwn.h"
void static _rws_in(_ulonglong __arg, void *__buf, _int_u __size, _64_u __offset) {
	struct f_rw_struc *rws = (struct f_rw_struc*)__arg;
	_8_s error;
	f_rws_pread(rws, __buf, __size, __offset, &error);
}

void static _rws_out(_ulonglong __arg, void *__buf, _int_u __size, _64_u __offset) {
	struct f_rw_struc *rws = (struct f_rw_struc*)__arg;
	_8_s error;
	f_rws_pwrite(rws, __buf, __size, __offset, &error);
}

static struct f_gwn_io rws_io[] = {
	{.io = _rws_in},
	{.io = _rws_out}
};
void f_gwn_io(_8_u __type, struct f_gwn_io *__io, void *__arg) {
	*__io = rws_io[__type];
	rws_io[__type].arg = (_ulonglong)((struct f_gwn_io_rws*)__arg)->rws;
}
