# define __ffly_debug
# include "y_int.h"
# include "types.h"
# include "system/io.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "dep/str_dup.h"
# include "dep/str_len.h"
# include "dep/mem_cpy.h"
# include "dep/str_cmp.h"
# include "shell.h"
static struct hash cmdmap;

struct cmd {
	char const *ident;
	_int_u func;
};

#define CMD(__name)\
	&(struct cmd){#__name, _cmd_ ## __name}
static struct cmd *cmd_tbl[] = {
	CMD(help),
	CMD(exit),
	CMD(info),
	CMD(f4),
	CMD(exec),
	&(struct cmd){"p", _cmd_er},
	NULL
};

static void
ldcmds(void) {
	struct cmd **cur = cmd_tbl, *c;
	while((c = *cur) != NULL) {
		ffsh_cmdput(&cmdmap, c->ident, c->func);
		cur++;
	}
}

static char line[512];
_int_u static off = 0;
_int_u static e;

void static
_get(_int_u __n, _int_u __offset, void *__buf) {
	f_mem_cpy((_8_u*)__buf+__offset, line+off, __n);	
	off+=__n;
}

_8_u static
_at_eof(void) {
	return off>=e;
}

# include "system/string.h"
# include "panel.h"
# include "m.h"
void static 
_er(char const *__cmd, _int_u __argc, char const *__argv[]) {	
	if (!f_str_cmp(__cmd, "connect")) {
		_int_u shm_id;
		shm_id = ffly_stno(*__argv);
		ff_p_connect(shm_id);
	} else if (!f_str_cmp(__cmd, "disconnect")) {
		ff_p_disconnect();
	} else if (!f_str_cmp(__cmd, "meminfo")) {
		struct ffly_meminfo info;
		ff_p_meminfo(&info);
		ffly_dmi(&info);	
	}
}

// will be set to -1 if exit command is executed
_8_i static run = 0;
# include "linux/termios.h"
/*
	can break needs fixing and shit.
*/

# include "line.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	ffsh_run = &run;
	struct termios term, old;
	int fd;
	fd = _ffly_in->fn->fd;
	tcgetattr(fd, &term);
	old = term;
	term.c_lflag &= ~(ICANON|ECHO);
	tcsetattr(fd, &term);

	ffsh_er = _er;
	get = _get;
	at_eof = _at_eof;
	_f_err_t err;
	hash_init(&cmdmap);
	ldcmds();
	struct node *n;
	char *p;
	char buf[26];
_again:
	p = line;
	/*
		will be moved later just piecing it together
	*/
	while(1) {
		ffly_l_show(_ffly_out);
		_int_u n, i = 0;
		n = read(fd, buf, sizeof(buf));
		char c;
		while(i != n) {
			c = *(buf+i);
			if (c == '\n') goto _out;
			if (c == 27) {
				i++;
				i++;
				if (*(buf+2) == 'D') {
					ffly_l_backward;
				} else if (*(buf+2) == 'C') {
					ffly_l_forward;
				}
			} else if (c == 127) {
				ffly_l_del();
			} else if (c>=32 && c<=126) {
				ffly_l_put(c);
			}
			i++;
		}
	}
_out:
	p+=ffly_l_load(line);
	ffly_l_reset();
	*p = '\0';
	printf("\n: line: %s\n", line);
	ffly_fdrain(_ffly_out);
	e = p-line;
	off = 0;

	n = ffsh_parse();
	if (n != NULL) {
		if (n->kind == _cmd) {
			printf("command: %s\n", n->name);
			void *p = ffsh_cmdget(&cmdmap, n->name);
			if (!p) {
				printf("command does not exist.\n");
			} else
				ffsh_exec_cmd(p, n->argc, (struct arg_s**)n->args);
		}
	}

	/*
		cleanup memory
	*/
	mem_cleanup();
	if (!run)
		goto _again;

	hash_destroy(&cmdmap);
	if (tcsetattr(fd, &old) == -1) {
		printf("failed to set tc attr.\n");
	}
}
