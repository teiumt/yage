# include "camera.h"
# include "common.h"
# include "../maths.h"
void h_camera_orbitprep(struct h_camera *__cam, _flue_float __dist) {
	__cam->pos.z = __dist;
	__cam->pos.x = 0;
	__cam->pos.y = 0;
	__cam->zoom = 1;
}
void h_camera_start(struct h_camera *__cam) {
	h_matcopY(__cam->m, __cam->ident);
}

void h_camera_orbit(struct h_camera *__cam) {	
	h_camera_lookat(__cam, __cam->pos.x, __cam->pos.y, __cam->pos.z, __cam->at.x, __cam->at.y, __cam->at.z, 0, 1, 0);
}
void h_camera_orbitadj(struct h_camera *__cam, struct h_vec3 *__change) {
	_flu_float dx,dy,dz;
	dx = __change->x;
	dy = __change->y;
	dz = __change->z;
	_flu_float rx = (dx/0.1)*(PI/180.);
	_flu_float ry = (dy/0.1)*(PI/180.);
	_flu_float rz = (dz/0.1)*(PI/180.);

	_flu_float m[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	
	_flu_float m0[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	
	_flu_float m1[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};


	m[0] = cos(ry);
	m[2] = sin(ry);
	m[8] = -sin(ry);
	m[10] = cos(ry);

	m0[5] = cos(rx);
	m0[6] = -sin(rx);
	m0[9] = sin(rx);
	m0[10] = cos(rx);

	m1[0] = cos(rz);
	m1[1] = -sin(rz);
	m1[4] = sin(rz);
	m1[5] = cos(rz);

	h_matrix_mul4(m, m, m0);
	h_matrix_mul4(m, m, m1);

	_flu_float ex,ey,ez;
	ex = __cam->at.x-(__cam->at.x-__cam->pos.x)*__cam->zoom;
	ey = __cam->at.y-(__cam->at.y-__cam->pos.y)*__cam->zoom;
	ez = __cam->at.z-(__cam->at.z-__cam->pos.z)*__cam->zoom;
	h_matcopY(__cam->m, __cam->ident);	
	h_lookat(__cam->m, ex, ey, ez, __cam->at.x, __cam->at.y, __cam->at.z, 0, 1, 0);
	h_matrix_mul4(__cam->m, __cam->m, m);
}

void h_camera_rotate(struct h_camera *__cam, struct h_vec3 *__change) {
    _flu_float dx,dy,dz;
    dx = __change->x;
    dy = __change->y;
    dz = __change->z;
    _flu_float rx = (dx/0.1)*(PI/180.);
    _flu_float ry = (dy/0.1)*(PI/180.);
    _flu_float rz = (dz/0.1)*(PI/180.);

    _flu_float m[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
   
    _flu_float m0[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };
   
    _flu_float m1[16] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    };


    m[0] = cos(ry);
    m[2] = sin(ry);
    m[8] = -sin(ry);
    m[10] = cos(ry);

    m0[5] = cos(rx);
    m0[6] = -sin(rx);
    m0[9] = sin(rx);
    m0[10] = cos(rx);

    m1[0] = cos(rz);
    m1[1] = -sin(rz);
    m1[4] = sin(rz);
    m1[5] = cos(rz);

    h_matrix_mul4(m, m, m0);
    h_matrix_mul4(m, m, m1);
	h_matrix_mul4(__cam->m, __cam->m, m);
}

void h_camera_lookat(struct h_camera *__cam,
	_flu_float __eyx, _flu_float __eyy, _flu_float __eyz,
    _flu_float __atx, _flu_float __aty, _flu_float __atz,
	_flu_float __upx, _flu_float __upy, _flu_float __upz)
{
	h_matcopY(__cam->m, __cam->ident);
	h_lookat(__cam->m, __eyx, __eyy, __eyz, __atx, __aty, __atz, __upx, __upy, __upz);
}
