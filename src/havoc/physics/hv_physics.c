#include "../subs.h"


void h_intergrate_forces_onto(h_subsisp __s) {
	hvec3 gravity = (hvec3){0.0,0.0,0.0};
	h_3vsub(&__s->phy.pos,&__s->phy.pos,&gravity);
	hvec3 vel;
	h_3vmulfloat(&vel,&__s->phy.vel,_h_is.world.phy_div);
	h_3vsub(&__s->phy.pos,&__s->phy.pos,&vel);
}
