#ifndef __psp__h
#define __psp__h
#include "../y_int.h"
struct psp_node {
	struct psp_node *parent;
	struct psp_node *map;
	_int_u cnt;
	void *list[4096*3];
	_64_u x,y,z;
	_64_u size;
};

struct psp {
	struct psp_node *root;
};
_int_u psp_find(void **__list[], struct psp_node *__root, float,float,float);
struct psp_node* psp_add(void *__p, struct psp_node *__root, float*,float*,float*);
void psp_fire_ray(struct psp_node *__root, _64_u __x, _64_u __y, _64_u __z, float dx, float dy, float dz);
#endif /*__psp__h*/
