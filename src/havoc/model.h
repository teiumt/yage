#ifndef __havoc__model__h
#define __havoc__model__h
#include "mesh.h"
#include "hv_tex.h"
#include "types.h"
#include "hv_pantry.h"
/*
	things to keep in mind when dealing with hv_premises,
	if the models underlying mesh where to be swaped out then the
	premises would have to change right?

	THINGS TO KEEP IN MIND:

	a model and its meshes dont account for extra meshes.
	when loading a model file or that it may contain additional meshes that are for the model 
	but are an accessory, like diffrent ear types for charicters.
	the accessory meshes are not apart of a model resource unless bound to the resource(i.e. inuse)
*/
struct h_mdlobject{
	h_meshp mesh;
	hv_texp tx;
};

/*
	a vehicle model consists of 2 parts(no over complicating)
	we have the 'wheels' and the 'body' of the car.

	for the wheels we may have diffrent objects for parts of the wheel.
	so it could consist of a 'tire' and the 'wheels core' two differing materials.

	this is where things get a bit 'i dont realy know'. we could have a case where the tire is dammaged and has a hole in it.
	this means A) we replace the wheel model completely or B) we replace the 'tire' object.

	the other thing is the car itself dosent handle ground contact as that happens on an indevidual wheel basis, meaning
	that each wheel contributes to the collission detection with the ground as suspension will have to be done like this as well.


	most engines assume models are not unique, meaning models may be shared accoss many game objects. so im going to assume
	selection of what to draw is left for the render routine.
	
	this is where i hit a brick wall.
	
	if you have a Car,Train,Helicopter

	i have looked at the XRay and Source engine to get some idea how this is delt with.
	my assumption is that rending is delt in its own way.

	Car_Render{
		Render(Car_Body)
		for every Wheel{
			Render(Car_Wheel)
		}
	}

	Helicopter_Render{
		Render(Helicopter_Shell)

		RPM = {Air Density(may be based on Air temperature + air humidity)+Up draft+Down draft}
		ROT = get_rot(RPM)
		# do some transform(rotation) based on engine RPM output/speed
		Render(Helicopter_Blades)
	}

	this is where things for me get IDK.


	a model may consist of parts but many models may share the same parts?
	this makes the model structure obsolete as the parts of a model can be stored else where(like part of the 'car' structure).

	struct Car{
		struct mdlobject car_body;
		struct mdlobject car_wheel;
	}

	as you see the struct is in non-ptr form as there is no point for pointers as its only placeholder(for textures and mesh).

	you end up as the conclusion that models are just a decription of model(ie. what it consists of(parts)).
	however it still cant be directly connected to an game object as a game object may not be renderable(waste of memory).
	
	# for example bullets may not be rendered.
	
	and objects not in view of player may be offloaded.

	a better name would be 'render_object'

	struct render_object{
		struct render_car car;
		struct render_train train;
	}

	some things dont fit, like the bounding box. this isent needed for rendering but for physiscs.


	for Cars there head and rear lights need to be controlable. this means thses meshes for those need to be 
	registers in the Car structure.
*/
/*
	model is a very bad name for this.
*/
typedef struct h_model {
	struct h_mdlobject obj[32];
	_32_u obj_bits;
	_int_u n_obj;
	void*priv;
	_int_u n;
	struct hv_premises prem;
} *h_modelp;


#define HV_MDLFILE		1
#define HV_MDLPANTRY	2
/*
	pantry is passed by name or pointer
*/
#define HV_MDLPBN			4
struct hv_mdlpar{
	_64_u bits;
	union{
		//file system file parth
		char const *path;
		char const *pty_name;
		struct hv_pantry *pty_ptr;
	};

	/*
		list of objects names that this model uses;
	*/
	char const **chosen;
	_16_u *layout;
};
/*
	CODE:
	struct hv_pantry *pty = mypty;

	char const *obj[] = {
		"engine_body_type0",
		"engine_core_type1",
		NULL
	};

	struct hv_mdlpar par;
	par.bits = HV_MDLPANTRY|HV_MDLPBN;
	par.pty_ptr = pty;
	par.chosen = obj;

	h_modelp m;
	m = mymodel;
	h_model_config(m,&par);
*/
void h_model_config(h_modelp,struct hv_mdlpar*);
void h_model_new(h_modelp);
#endif/*__havoc__model__h*/
