# ifndef __havoc__terrain__h
# define __havoc__terrain__h
# include "vec.h"
#include "psp.h"
struct terrain_chunk {
    void *vertex_buffer;
    void *normal_buffer;
	void *index_buffer;
};
struct h_terrain {
	struct terrain_chunk *chunks;
	void *vertex_buffer;
	void *normal_buffer;
	void *index_buffer;
	_int_u nvtx;
	_int_u tri_cnt;
	struct psp_node psp;
};
struct h_terrain* h_terrain_test(void);
void h_terrain_destroy(struct h_terrain*);
# endif /*__havoc__terrain__h*/
