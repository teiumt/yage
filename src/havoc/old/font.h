# ifndef __havoc__font__h
# define __havoc__font__h
# include "../y_int.h"
# include "types.h"
# include "../flue/types.h"
# include "obj.h"
/*

	how do real deal with the allocation of memory 
	for text???? 
	why dont we just allocate a largesh image and use that?
	
	waste some of the image is wasted(WASTED memory!)

*/
typedef struct font *fontp;
struct font_driver {
	void(*load)(fontp);
};

struct font_char {
	_8_u *bm;
	_flu_float u0, v0;
	_flu_float u1, v1;
};

typedef struct font {
	_int_u width, height;
	struct font_driver *d;
	struct font_char cmap[0x100];
	//active char array
	_8_u *acarr;
	_int_u nac;
} *fontp;

struct font_state {
	struct h_obj *j;
};

void font_state_init(struct font_state*);
#define FNT_HALIGN 0x01
#define FNT_VALIGN 0x02
#define FNT_bf fnt_drv
struct font_driver fnt_drv[20];
fontp font_load(void);
void font_test(fontp, char);
void font_draw(fontp __f, char const *__str, _int_u __len, _int_u __x, _int_u __y, _8_u __bits, _flu_float __size, struct h_colour *__colour, struct h_colour *__bg);
_8_u *font_char(fontp, char);
# endif /*__havoc__font__h*/
