# include "widget.h"
/*
	TODO:
		change anything with wg
		for example) h_wg_link -> h_wglink
*/
void ui_widget_hide(struct h_widget *__wg) {
}

void h_wg_update(struct h_composer *b, struct h_widget *__wg) {
	struct h_hbox *hb = __wg->hb;
	float x = __wg->x,y=__wg->y;
	/*
		setup the widgets hitbox area
	*/
	hb->x = x*b->ww;
	hb->y = y*b->wh;
	hb->xfar = (x+__wg->w)*b->ww;
	hb->yfar = (y+__wg->h)*b->wh;


}

void h_wg_link(struct h_composer *b, struct h_widget *__wg) {
	struct h_rectbox *rb;
	__wg->b = b;
	__wg->hb = &(rb = h_rbx_new(b))->dsc->hb;
	rb->skim = -1;
	rb->dsc->arg = __wg;
	rb->dsc->update = __wg->update;
	h_wg_update(b,__wg);
	__wg->rb = rb;
}
