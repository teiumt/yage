# include "btn.h"
# include "../../m_alloc.h"
# include "../../string.h"
# include "../../io.h"
# include "../../assert.h"
#include "../font.h"
float static temp = (1./255.)*181;
void static update(_ulonglong __arg, struct h_msg *m) {
	struct ui_btn *bt = (void*)__arg;
	hv_printf("button was pressed or state was altered!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.\n");
	if (m->code == H_BTN_LEFT) {
		hv_printf("have left button event.\n");
		struct h_msg _m;
		_int_u code = 0;

		if (m->value == H_PRESS) {
			code = UI_BTN_PRESS;
		}else
		if (m->value == H_RELEASE) {
			code = UI_BTN_RELEASE;
		}else
			goto _sk;

		
		if (bt->w.output != NULL) {
			_m.code = bt->w.codetab[code];
			bt->w.output(bt->w.out,&_m);

		}else{
			hv_printf("there no way to output.\n");
		}
		return;
	}
_sk:
	hv_printf("message value : %u.\n",m->value);
	if (m->value == H_PTR_ENTER && bt->inside == -1) {
		h_vdivf(&bt->rct.c,bt->rct.c,1.2);
		hv_printf("GOT ENTER.\n");
		bt->inside = 0;
	}
	if (m->value == H_PTR_EXIT && bt->inside == 0) {
		h_vmulf(&bt->rct.c,bt->rct.c,1.2);
		hv_printf("GOT EXIT.\n");
		bt->inside = -1;
	}
}
//interleaving message
void static input(_ulonglong __arg, struct h_msg *m) {
	
}


void ui_btn_new(struct ui_btn *bt) {
	bt->w.output = NULL;

	bt->w.draw = ui_btn_draw;
	bt->w.update = update;
	bt->w.input = input;
	bt->w.move = ui_btn_move;
	ui_rect_new(&bt->rct);
	
	text_creat(&bt->tx,0x00);
	bt->w.hb = NULL;
	bt->w._w = 1;
	bt->w._h = 1;
	bt->inside = -1;
	return bt;
}

void ui_btn_load(struct ui_btn *__bt, struct ui_btn_strc *__str, fontp __f) {
//	assert(__bt->w.hb != NULL);
	__bt->w.w = __str->w*__bt->w._w;
	__bt->w.h = __str->h*__bt->w._h;
	__bt->tx.w = __bt->w.w*__str->tw;
	__bt->tx.h = __bt->w.h*__str->th;
	__bt->tx.f = __f;
	__bt->tx.colour = __str->tx.colour;
	__bt->tx.bg = __str->tx.bg;
	__bt->rct.c = __str->bg;
	__bt->tx.len = __str->len;
	__bt->tx.text = __str->text;
//	mem_cpy(__bt->tx.text, __str->text, __str->len);
//	ui_btn_move(__bt, __str->x*__bt->w._w, __str->y*__bt->w._h);
}

void ui_btn_move(struct ui_btn *__bt, _flue_float __x, _flue_float __y) {
	struct h_composer *b = __bt->w.b;
	__bt->w.x = __x;
	__bt->w.y = __y;
	float mx, my;
	mx = __bt->w.w*0.5;
	my = __bt->w.h*0.5;
//	__x-=mx;
//	__y-=my;
/*
	for text to be align be need to alter its ancor point
*/
	__bt->tx.x = __x;
	__bt->tx.y = __y;
	if(__bt->tx.bits&FNT_HALIGN){
		__bt->tx.x+=mx;
	}
	if(__bt->tx.bits&FNT_VALIGN){
		__bt->tx.y+=my;
	}

	__bt->rct.vtx[0] = __x;
	__bt->rct.vtx[1] = __y;
	__bt->rct.vtx[2] = __x+__bt->w.w;
	__bt->rct.vtx[3] = __y+__bt->w.h;
	__bt->w.hb->x = (__x)*b->ww;
	__bt->w.hb->y = (__y)*b->wh;
	__bt->w.hb->xfar = ((__x)+__bt->w.w)*b->ww;
	__bt->w.hb->yfar = ((__y)+__bt->w.h)*b->wh;
}

void ui_btn_destroy(struct ui_btn *__bt) {
}
# include "../renderer.h"
void ui_btn_draw(struct ui_btn *__bt) {
	ui_rect_draw(&__bt->rct);
	ui_text_draw(&__bt->tx);
}
