# include "text.h"
# include "../../m_alloc.h"
# include "../../string.h"
# include "../common.h"
void text_creat(ui_textp tx, _8_u __flags) {
	if (!__flags) goto _sk;
_sk:
	tx->_w.draw = ui_text_draw;
	tx->bits = 0x00;
	tx->x = 0;
	tx->y = 0;
	tx->xoff = 0;
	tx->yoff = 0;
//	font_state_init(&tx->fs);

	return tx;
}

void ui_text_update(_ulonglong __arg, struct h_msg *m) {
}

void ui_text_draw(ui_textp __tx) {
	font_draw(__tx->f, __tx->text, __tx->len, __tx->x, __tx->y, __tx->bits, __tx->size, &__tx->colour, &__tx->bg);
}

void text_alter(ui_textp __tx, struct ui_text_struc *__str) {
	if (__str->bits&UI_TX_SETTEXT) {
		mem_cpy(__tx->text, __str->text.p, __str->text.len);
		__tx->len = __str->text.len;
	}

	if ((__str->bits&UI_TX_SETFONT)>0) {
		__tx->f = __str->f;
	}
}

void text_destroy(ui_textp __tx) {
}
