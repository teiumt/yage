# include "rect.h"
# include "../../m_alloc.h"
# include "../renderer.h"
void static update(_ulonglong __arg, struct h_msg *m) {
	struct ui_rect *bt = (void*)__arg;
	if (m->code == H_BTN_LEFT) {
		 struct h_msg _m;
		_m.x = m->x;
		_m.y = m->y;
		_int_u code = 0;

		if (m->value == H_PRESS) {
			code = UI_BTN_PRESS;
		}else
		if (m->value == H_RELEASE) {
			code = UI_BTN_RELEASE;
		}else
			return;

		if (bt->w.output != NULL) {
			_m.code = bt->w.codetab[code];
			bt->w.output(bt->w.out,&_m);
		}else{
			hv_printf("there no way to output.\n");
		}
		return;
	}
}

void static input(_ulonglong __arg, struct h_msg *m) {
    struct ui_rect *w = (void*)__arg;
    if (m->code == UI_RECT_HIDE) {
        w->w.rb->skim = 0;
    }else
    if(m->code == UI_RECT_SHOW) {
		w->w.rb->skim = -1;
    }
}

void ui_rect_new(struct ui_rect *__r) {
	__r->w.update = update;
	__r->w.draw = ui_rect_draw;
	__r->w.input = input;
}

void ui_rect_destroy(struct ui_rect *__r) {
}

void ui_rect_draw(struct ui_rect *__r) {
	h_rect(_h_is._ctx,__r->vtx, __r->c.r,__r->c.g,__r->c.b,__r->c.a);
}
