# ifndef __havoc__btn__h
# define __havoc__btn__h
# include "text.h"
# include "rect.h"
# include "../common.h"
# include "../../flue/types.h"
# include "widget.h"
struct ui_btn {
	struct h_widget w;

	struct ui_rect rct;
	struct ui_text tx;
	_8_s inside;
};

struct ui_btn_strc {
	_flue_float x, y;
	_flue_float w, h;
	_flue_float tw, th;
	struct {
		struct h_colour colour;
		struct h_colour bg;
	} tx;
	struct h_colour bg;
	char const *text;
	_int_u len;
};

void ui_btn_load(struct ui_btn*, struct ui_btn_strc*, fontp);
void ui_btn_move(struct ui_btn*, _flue_float, _flue_float);
void ui_btn_new(struct ui_btn*);
void ui_btn_destroy(struct ui_btn*);
void ui_btn_draw(struct ui_btn*);
# endif /*__havoc__btn__h*/
