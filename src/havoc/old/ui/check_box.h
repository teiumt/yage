#ifndef __ui__checkbox__h
#define __ui__checkbox__h
#include "widget.h"
#include "../icon.h"
#define UI_CHECKBOX_CHECKED 0
#define UI_CHECKBOX_UNCHECKED 1
struct ui_checkbox {
	struct h_widget w;
	_8_s state;
	struct h_icon_data *tick;
	struct h_icon_data *cross;

	struct h_icon *icon;
};
void ui_checkbox_new(struct ui_checkbox*);
void ui_checkbox_config(struct ui_checkbox *__cb, float __x, float __y, float __w, float __h);
#endif /*__ui__checkbox__h*/
