#ifndef __ui__dropdown__h
#define __ui__dropdown__h
#include "widget.h"
#include "../common.h"

/*
	not to be used when signaling
*/
#define UI_DROPDOWN_EXTEND	1
#define UI_DROPDOWN_RETRACT	2

#define UI_DROPDOWN_EXTEND_IDX		0
#define UI_DROPDOWN_RETRACT_IDX		1


struct ui_dropdown {
	struct h_widget w;
	struct h_widget *parent;

	struct h_widget *item[16];
	_int_u items;
	_8_s visable;
};
void ui_dropdown_move(struct ui_dropdown *__dd, float __x, float __y);
void ui_dropdown_add(struct ui_dropdown*,struct h_widget*);
void ui_dropdown_new(struct ui_dropdown*);
void ui_dropdown_draw(struct ui_dropdown*);
#endif /*__ui__dropdown__h*/
