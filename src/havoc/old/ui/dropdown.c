#include "dropdown.h"
void static input(_ulonglong __arg, struct h_msg *m) {
	struct ui_dropdown *dd = (void*)__arg;
	if (m->code&UI_DROPDOWN_EXTEND && dd->visable == -1) {
		hv_printf("!!!!!!!!!! got signal for dropdown to extend.\n");
		dd->visable = 0;
	}else	
	if (m->code&UI_DROPDOWN_RETRACT) {
		hv_printf("!!!!!!!!!! got signal for dropdown to retract.\n");
		dd->visable = -1;
	}
}

void ui_dropdown_new(struct ui_dropdown *__dd) {
	__dd->w.input = input;
	__dd->visable = -1;
	__dd->items = 0;
}

void ui_dropdown_move(struct ui_dropdown *__dd, float __x, float __y) {
	_int_u i;
	i = 0;
	float y = __dd->parent->y+__dd->parent->h;
	for(;i != __dd->items;i++) {
		__dd->item[i]->move(__dd->item[i],__x+__dd->parent->x,__y+y);
		y+=__dd->item[i]->h;
	}
}

void ui_dropdown_add(struct ui_dropdown *__dd,struct h_widget *__w) {
	__dd->item[__dd->items++] = __w;
}
#include "../renderer.h"
extern struct h_context hct;
void ui_dropdown_draw(struct ui_dropdown *__dd) {
	if (!__dd->visable) {
	//	float vtxlist[] = {
	//		0.5,0.5,
	//		1,1
	//	};
	//	h_rect(&hct, vtxlist,1,0,0,1);
		_int_u i;
		i = 0;
		for(;i != __dd->items;i++) {
			__dd->item[i]->draw(__dd->item[i]);
		}
	}
}
