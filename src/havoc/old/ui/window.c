#include "window.h"

void ui_window_move(struct ui_window *w, float __x, float __y) {
	w->w.x = __x;
	w->w.y = __y;
	h_wg_update(w->w.b,w);
	w->move_decor(w);
}
void
ui_message(struct h_composer *b, struct h_msg *m);
void static update(_ulonglong __arg, struct h_msg *m) {
	struct ui_window *w = (void*)__arg;
	if (m->value == H_RELEASE && !w->griped) {
		w->griped = -1;
		_h_is.msg = h_ui_message;
	}else
	if (m->value == H_PTR_M && !w->griped) {
		hv_printf("window is griped and ya.\n");
		ui_window_move(w,m->x*w->w.b->vpw-w->xdis,m->y*w->w.b->vph-w->ydis);
	}else
	if (m->value == H_PTR_ENTER) {
	}else
	if (m->value == H_PTR_EXIT) {
		struct h_rectbox *bx;
		bx = w->comp.bx;
		while(bx != NULL) {
			_int_u i;
			struct h_rbdesc *dsc, *_dsc;
			i = 0;
			for(;i != bx->n+1;i++) {
				_dsc = bx->dsc+i;
				/*
					to ensure consistency, make sure this is like this
				*/
				_dsc->inside = -1;

			}
			bx = bx->next;
		}
	}else{
		hv_printf("window got message, displacemtn: %f, %f\n",w->w.x*w->w.b->ww,w->w.y*w->w.b->wh);
		struct h_msg _m = *m;
		_m.x-=w->w.x*w->w.b->ww;
		_m.y-=w->w.y*w->w.b->wh;
		ui_message(&w->comp,&_m);
	}
}

void static input(_ulonglong __arg, struct h_msg *m) {
	struct ui_window *w = (void*)__arg;
	if (m->code == UI_WINDOW_HIDE) {
		w->hidden = 0;		
		w->w.rb->skim = 0;
	}else
	if(m->code == UI_WINDOW_SHOW) {
		w->hidden = -1;
		w->w.rb->skim = -1;
	}else
	if(m->code == UI_WINDOW_GRAB) {
		hv_printf("window grab.\n");
		w->xdis = ((float)m->x*w->w.b->vpw)-w->w.x;
		w->ydis = ((float)m->y*w->w.b->vph)-w->w.y;
		_h_is.msg_arg = w;
		_h_is.msg = update;
		w->griped = 0;
		return;
	}
	if (w->w.output != NULL) {
		struct h_msg _m;
		_m.code = w->w.codetab[m->code];
		w->w.output(w->w.out,&_m);
	}
}

void ui_window_new(struct ui_window *__w) {
	__w->w.input = input;
	__w->w.update = update;
	__w->comp.bx = NULL;
	__w->n = 0;
	__w->hidden = 0;
	__w->griped = -1;
	__w->w.output = NULL;
}

void ui_window_add(struct ui_window *w,struct h_widget *__w) {
	w->ws[w->n++] = __w;
}
void ui_window_config(struct ui_window *w, float __x, float __y, float __w, float __h) {
	w->w.w = __w;
	w->w.h = __h;
	w->w.x = __x;
	w->w.y = __y;
	w->comp.ww = _h_is.comp.ww*__w;
	w->comp.wh = _h_is.comp.wh*__h;
	w->comp.asp = w->comp.ww/w->comp.wh;
}
#include "../renderer.h"
extern struct h_context hct;
void ui_window_draw(struct ui_window *__w) {
	 float vtxlist[] = {
	 	__w->w.x,__w->w.y,
	   __w->w.x+__w->w.w,__w->w.y+__w->w.h,
	};
	h_rect(&hct, vtxlist,0.2,0.2,0.2,1);
	struct h_viewport vp0 = {
		.xyzw = {__w->w.x*__w->w.b->ww,__w->w.y*__w->w.b->wh,0},
		.scale = {__w->w.w*__w->w.b->ww,__w->w.h*__w->w.b->wh,1}
	};
	_h_is.rd->viewport(&vp0);

	_int_u i;
	i = 0;
	for(;i != __w->n;i++) {
		__w->ws[i]->draw(__w->ws[i]);
	}
}

