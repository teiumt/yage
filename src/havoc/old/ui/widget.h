# ifndef __havoc__widget__h
# define __havoc__widget__h
# include "../common.h"
struct h_widget {
	/*
		is an event is issued is press,release
		then codetab is the code translation BTN_PRESS->(do exit)	
	*/

	//more codetab???? codetab2
	struct h_composer *b;

	_int_u *codetab;

	_ulonglong value;
	struct h_msgbucket *mbkt;

	/*
		hitbox is a pointer, why? because its faster to iterate over all exiting hitboxes to check is we have an intersection.
		most likly scenario is that are widget doesent move thus not accessing hb
	*/
	struct h_hbox *hb;
	struct h_rectbox *rb;
	_flue_float x, y;
	_flue_float w, h;
	_flue_float _w, _h;
	_flu_float dis;
	_8_s hidden;
	void(*update)(struct h_widget*, struct h_msg*);
	void(*move)(struct h_widget*, _flu_float, _flu_float);
	void(*draw)(struct h_widget*);
	void(*input)(struct h_widget*,struct h_msg*);

	/*
		signal output
	*/
	void(*output)(struct h_widget*,struct h_msg*);
	_ulonglong out;

};
void h_wg_update(struct h_composer *b, struct h_widget *__wg);
void h_wg_link(struct h_composer*, struct h_widget*);
# endif /*__havoc__widget__h*/
