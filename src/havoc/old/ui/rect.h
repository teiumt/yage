# ifndef __havoc__rect__h
# define __havoc__rect__h
#include "widget.h"
# include "../types.h"
# include "../../flue/types.h"
#define UI_RECT_HIDE 0
#define UI_RECT_SHOW 1
struct ui_rect {
	struct h_widget w;
	struct h_colour c;
	_flue_float vtx[4];
};

void ui_rect_new(struct ui_rect*);
void ui_rect_destroy(struct ui_rect*);
void ui_rect_draw(struct ui_rect*);
# endif /*__havoc__rect__h*/
