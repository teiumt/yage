# ifndef __havoc__mbar__h
# define __havoc__mbar__h
// headerbar
# include "widget.h"
# include "btn.h"
# include "rect.h"
struct ui_mbar {
	struct h_widget w;
	struct h_widget *boxes[24];
	_int_u n;
	_flu_float x;
	struct ui_rect *rct;
};

void ui_mbar_enlock(struct ui_mbar*);
void ui_mbar_add(struct ui_mbar*, struct h_widget*);
void ui_mbar_draw(struct ui_mbar*);
struct ui_mbar* ui_mbar_new(void);
struct ui_btn* ui_mbar_btn(void);
# endif /*__havoc__mbar__h*/
