#include "check_box.h"
#include "../renderer.h"
extern struct h_context hct;
void static draw(struct ui_checkbox *__cb) {
	struct h_icon_data *icon = !__cb->state?__cb->tick:__cb->cross;
	float vtxlist[] = {
		__cb->w.x,__cb->w.y,
		__cb->w.x+__cb->w.w,__cb->w.y+__cb->w.h,
		icon->uv[0],icon->uv[1],
		icon->uv[2],icon->uv[3],
		icon->uv[4],icon->uv[5]
	};
//	h_rect(&hct, vtxlist,__cb->state == -1?1:0,__cb->state == -1?0:1,0,1);

	h_uirect(&hct,vtxlist,1,__cb->icon->tx, 1,1,1,1);

}

void static update(_ulonglong __arg, struct h_msg *m) {
	struct ui_checkbox *cb = (void*)__arg;
	if (m->code == H_BTN_LEFT) {
		if (m->value == H_PRESS) {
			cb->state = !cb->state?-1:0;
		
			struct h_msg m;
			m.code = !cb->state?UI_CHECKBOX_CHECKED:UI_CHECKBOX_UNCHECKED;
			cb->w.output(NULL,&m);
		}
	}
}

void ui_checkbox_config(struct ui_checkbox *__cb, float __x, float __y, float __w, float __h) {
	__cb->w.x = __x;
	__cb->w.y = __y;
	__cb->w.w = __w;
	__cb->w.h = __h;
}

void ui_checkbox_new(struct ui_checkbox *__cb) {
	__cb->state = -1;
	__cb->w.update = update;
	__cb->w.draw = draw;
}
