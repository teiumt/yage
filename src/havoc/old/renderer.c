# include "renderer.h"
# include "../m_alloc.h"
# include "../flue/common.h"
# include "../io.h"
# include "common.h"
# include "../assert.h"
# include "resource.h"
struct h_rstate _h_rs;
struct h_rsturc *_h_r;
static struct flue_canister cans[8];
_int_u static curcan = 0;
static struct flue_op ops[0x1000];
static _int_u n_ops = 0;
/*
	NOTE:
	 no memory managment why?
	 because its in testing stages
*/
struct {
	struct flue_prog *pgs[2];
} landscape;

#define NEWOP(__op, __n, __bits, __text)\
	op = ops+(n_ops++);\
	op->op = __op;\
	op->n = __n;\
	op->bits = __bits;\
	op->t = __text;
static _flu_float ident[16];
void emit_tgl3(struct h_job *__j) {
	printf("TGL-EMIT.\n");
	_flue_float *p = __j->prim.p;
	_int_u i, n = __j->prim.n;
	i = 0;

	struct flue_op *op;
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc(n*sizeof(struct flue_optext));
	
	NEWOP(FLUE_TRI3, n, 0x00, t);
	for(;i != n;i++) {
		t->buf = p+(i*18);
		t++;
	}
}

void emit_rect(struct h_job *__j) {
	printf("RCT-EMIT.\n");
	struct flue_op *op;
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc(sizeof(struct flue_optext));
	NEWOP(FLUE_RECT, 1, 0x00, t);
	t->buf = __j->prim.p;
}

void emit_crect(struct h_job *__j) {
	printf("CRCT-EMIT.\n");
	struct flue_colour c = {
		__j->prim.colour.r,
		__j->prim.colour.g,
		__j->prim.colour.b,
		__j->prim.colour.a
	};
	struct flue_op *op;
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc(sizeof(struct flue_optext));
	NEWOP(FLUE_CRECT, 1, 0x00, t);	
	t->buf = __j->prim.p;
	t->c = c;
}

void emit_lpcm(struct h_job *__j) {
	printf("LPCM-EMIT.\n");
	struct flue_op *op;
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc(sizeof(struct flue_optext));
	NEWOP(FLUE_LPCM, 1, 0x00, t);
	t->buf = __j->prim.p;
}

void emit_ilpcm(struct h_job *__j) {
	printf("ILPCM-EMIT.\n");
	struct flue_op *op;
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc(sizeof(struct flue_optext));
	NEWOP(FLUE_ILPCM, 1, 0x00, t);
	t->buf = __j->prim.p;
}

void emit_quad(struct h_job *__j) {
	printf("QUAD-EMIT.\n");
	struct flue_op *op;
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc(sizeof(struct flue_optext));
	NEWOP(FLUE_QUAD, 1, 0x00, t);
	t->buf = __j->prim.p;
	t->n = __j->prim.n;
}

static _flue_float world_lines[33];
# include "../maths.h"

static struct flue_optext *temp = NULL;
void emit_landscape(struct h_job *__j) {
	struct flue_context *ct = FLUE_CTX;
	printf("LANDSCAPE-EMIT.\n");
	struct flue_canister *c;
	c = __j->can;
	c->bits = SWAPOUTSHADER;
	c->pgm = landscape.pgs;
	ct->priv->m.tp[ct->priv->m.ntex++] = __j->ls->tx;
	c->n_ops = 0;
	c->tex[0] = __j->ls->tx;
	c->ntx = 1;
	c->m = _h_rs.cam->m;
	_flue_float *p = __j->ls->t->vertex_buffer->cpu_map;
	_int_u i, n = __j->ls->t->nvtx/15;
	i = 0;

	assert(n != 0);	

	struct flue_op *op;
	op = ops+n_ops;
	op->op = FLUE_TRI3;
	op->n = n;
	op->bits = 0x00;
	n_ops++;
	c->ops = op;
	c->n_ops = 2;
	if (temp != NULL) {
		m_free(temp);
	}
	struct flue_optext *t;
	t = (struct flue_optext*)m_alloc((n+1)*sizeof(struct flue_optext));
	op->t = t;
	for(;i != n;i++) {
		(t+i)->buf = p+(i*18);
	}
	temp = t;
	t = t+n;
	op = ops+n_ops;
	n_ops++;
	op->op = FLUE_LINE;
	op->n = 1;
	op->t = t;
	t->n = 3;
	op->bits = 0x00;
	t->buf = world_lines;
	// x-axis
	world_lines[0] = -.5-2;
	world_lines[1] = 0;
	world_lines[2] = 0;
	world_lines[3] = .5+2;
	world_lines[4] = 0;
	world_lines[5] = 0;

	// colour
	world_lines[6] = 1;
	world_lines[7] = 0;
	world_lines[8] = 0;
	world_lines[9] = 1;
	world_lines[10] = 2;

	// y-axis
	world_lines[11] = 0;
	world_lines[12] = -.5-2;
	world_lines[13] = 0;
	world_lines[14] = 0;
	world_lines[15] = .5+2;
	world_lines[16] = 0;

	world_lines[17] = 0;
	world_lines[18] = 1;
	world_lines[19] = 0;
	world_lines[20] = 1;
	world_lines[21] = 2;

	// z-axis
	world_lines[22] = 0;
	world_lines[23] = 0;
	world_lines[24] = -.5-2;
	world_lines[25] = 0;
	world_lines[26] = 0;
	world_lines[27] = .5+2;

	world_lines[28] = 0;
	world_lines[29] = 0;
	world_lines[30] = 1;
	world_lines[31] = 1;
	world_lines[32] = 2;
}

void h_landscape(struct h_landscape *__ls) {
	printf("graphics JOB(LANDSCAPE).\n");

	struct h_job *j;
	j = _h_rs.cur++;
	j->ls = __ls;
	j->can = cans+curcan++;
	j->func = emit_landscape;
}

void h_tgl3(_flue_float *__p, _int_u __n) {
	printf("graphics JOB(TGL3).\n");
	struct h_job *j;
	j = _h_rs.at->cur++;
	j->prim.p = __p;
	j->prim.n = __n;
	j->func = emit_tgl3;
}

void h_rect(_flue_float *__p) {
	struct h_job *j;
	j = _h_rs.at->cur++;
	j->prim.p = __p;
	j->prim.n = 0;
	j->func = emit_rect;
}

void h_crect(_flue_float *__p, struct h_colour *__c) {
	struct h_job *j;
	j = _h_rs.at->cur++;
	j->prim.p = __p;
	j->prim.colour = *__c;
	j->prim.n = 0;
	j->func = emit_crect;
}

void h_lpcm(_flue_float *__p) {
	struct h_job *j;
	j = _h_rs.at->cur++;
	j->prim.p = __p;
	j->prim.n = 0;
	j->func = emit_lpcm;
}

void h_ilpcm(_flue_float *__p) {
	struct h_job *j;
	j = _h_rs.at->cur++;
	j->prim.p = __p;
	j->prim.n = 0;
	j->func = emit_ilpcm;
}

void h_quad(_flue_float *__p, _int_u __n) {
	struct h_job *j;
	j = _h_rs.at->cur++;
	j->prim.p = __p;
	j->prim.n = __n;
	j->func = emit_quad;
}

static struct flue_bo *bo;
static struct flue_yard yrd;
static struct flue_prog *pgs[2];
void renderer_init(void) {
	h_ident(ident);
	pgs[0] = flue_prog();
	pgs[1] = flue_prog();
	bo = flue_bo_new();
	bo->size = 512;
	flue_bo_map(bo);
	struct flu_plarg arg;

	arg.p = PITHDIR "ps.ysa";
	//ysa_loader(&arg, pgs[0]);
//	flue_prog_ready(pgs[0]);

	arg.p = PITHDIR "vs.ysa";
	//ysa_loader(&arg, pgs[1]);
//	flue_prog_ready(pgs[1]);
	yrd.abouts = 0;
	yrd.dwc = 1;
	yrd.bo = bo;
	pgs[0]->binds[0] = &yrd;
	pgs[0]->nb = 1;
	pgs[1]->nb = 0;
	landscape.pgs[0] = pgs[0];

	landscape.pgs[1] = flue_prog();
	arg.p = PITHDIR "landscape.vs.ysa";
//	ysa_loader(&arg, landscape.pgs[1]);
//	flue_prog_ready(landscape.pgs[1]);

	landscape.pgs[1]->nb = 0;
	_h_r = (struct h_rsturc*)m_alloc(8*sizeof(struct h_rsturc));
	_int_u i;
	i = 0;
	for(;i != 8;i++) {
		_h_r[i].cur = _h_r[i].buf;
		_h_r[i].next = NULL;
	}
	_h_rs.at = _h_r;
	_h_rs.at->m = ident;
	_h_rs.n = 1;
	_h_rs.cur = _h_rs.buf;
	_h_r[0].can = 0;
	_h_r[0].tex = _h_rs.t0;
	_h_r[0].ntx = 0;
	curcan++;
	_h_rs.tc = _h_rs.t0;
	_h_rs.tex = _h_rs.t0;
	_h_rs.n_old = 0;
}
void static *dummytex[12];
static struct h_rsturc buff[24];
static _int_u next_buf = 0;
void _h_brk(void) {
	struct h_rsturc *cur = buff+next_buf;
	_h_rs.at->next = cur;
	_h_rs.at = cur;
	cur->next = NULL;
	
	cur->tex = dummytex;
	cur->cur = cur->buf;
	next_buf++;
	cur->can = curcan++;
	cur->m = ident;
	cur->ntx = 0;
}
void flush(void) {
	/*
		load/unload textures
		compare previous render to this and find textures
		that have been removed and unload them as we dont need them
	*/
	struct flue_context *ct;
	ct = _h_is.ctx;
	void **tx;
	tx = _h_rs.tex;
	while(tx != _h_rs.tc) {
		texp x = *tx;
		if (!x->ignore) {
			if (x->loaded == -1) {
			
				//	tell flue that the texture is to be loaded
			
				ct->priv->m.tp[ct->priv->m.ntex++] = x;
				x->loaded = 0;
			} else {
				x->reloaded = 0;
			}
		}
		tx++;
	}
	_int_u i;
	i = 0;
	for(;i != _h_rs.n_old;i++) {
		texp x = _h_rs.old[i];
		// unload texture that did not get reused
		if (x->reloaded == -1 && !x->ignore) {
			ct->priv->m.u_tp[ct->priv->m.u_ntex++] = x;
			x->loaded = -1;
		}
	}

	_h_rs.n_old = _h_rs.tc-_h_rs.tex;
	_h_rs.old = _h_rs.tex;
	if (_h_rs.tex == _h_rs.t0)
		_h_rs.tex = _h_rs.t1;
	else
		_h_rs.tex = _h_rs.t0;

	printf("flushing jobs.\n");
	struct h_job *j;
	struct h_rsturc *r;
	i = 0;
	for(;i != _h_rs.n;i++) {
		r = _h_r+i;
		while(r != NULL) {
			j = r->buf;
			struct flue_canister *c;
			c = cans+r->can;
			c->bits = 0;
			c->ops = ops+n_ops;
			_int_u pos = n_ops;
			c->tex[0] = r->tex[0];
			c->ntx = r->ntx;
			c->m = r->m;
			while(j != r->cur) {
				j->func(j);
				j++;
			}
			c->n_ops = n_ops-pos;
			r->cur = r->buf;	
			r = r->next;
		}
		_h_r[i].next = NULL;
	}
	j = _h_rs.buf;
	while(j != _h_rs.cur) {	
		j->func(j);
		j++;
	}
	_h_rs.cur = _h_rs.buf;

	struct flue_blob b;
	/*
		the place we are rendering to(the window surface)
	*/
	b.rtgs[0] = _h_is.sf;
	b.can = cans;
	b.n = curcan;
	b.pgm = pgs;
	curcan = 1;
	n_ops = 0;
	ct->b = &b;

	gc_update(_h_is.ctx);
	next_buf = 0;
	_h_rs.at = _h_r;
	_h_rs.at->m = ident;
	_h_rs.at->ntx = 0;
	_h_rs.at->next = NULL;
	_h_rs.tc = _h_rs.tex;
}
