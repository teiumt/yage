# ifndef __havoc__renderer__h
# define __havoc__renderer__h
# include "../y_int.h"
# include "types.h"
# include "../flue/types.h"
# include "landscape.h"
# include "camera.h"
#define _h_prim 0x00
struct h_job {
	_8_u id;

	struct {
		_int_u n;
		_flue_float *p;
		struct h_colour colour;
	} prim;
	struct h_landscape *ls;
	void *can;
	void(*func)(struct h_job*);
	struct h_job *next;
};


struct h_rsturc {
	void **tex;
	_int_u ntx;
	_int_u can;
	_flu_float *m;
	/*
		primative renders
		e.g. rect triangles, etc 
	*/
	struct h_job buf[0x100];
	struct h_job *cur;
	struct h_rsturc *next;
};

struct h_rstate {
	struct h_camera *cam;
	void **tex;

	void **tc;


	void *t0[24];
	void *t1[24];
	void **old;
	_int_u n_old;
/*
	indepentend renders
	ie have there own states e.g. textures
*/
	struct h_job buf[0x100];
	struct h_job *cur;

	struct h_rsturc *at;
	_int_u n;
};
void renderer_init(void);
extern struct h_rstate _h_rs;
extern struct h_rsturc *_h_r;
void flush(void);
void _h_brk(void);
void h_quad(_flue_float*, _int_u);
void h_tgl3(_flue_float*, _int_u);
void h_rect(_flue_float*);
void h_lpcm(_flue_float*);
void h_ilpcm(_flue_float*);
void h_crect(_flue_float*, struct h_colour*);
void h_landscape(struct h_landscape*);
struct h_rsstruc* h_brk(void);
# endif /*__havoc__renderer__h*/
