# include "shader.h"
# include "../system/file.h"
# include "../error.h"
# include "../types.h"
#include "../string.h"
#include "../m_alloc.h"
void h_shader_create(struct h_shader *__sh, _64_u __id) {
	__sh->priv = _h_is.rd->shader_create(__id);
}
void h_shader_load_file(struct h_shader *sh, char const *__path) {
	_h_is.rd->shader_compile(sh->priv,__path,str_len(__path));
}
