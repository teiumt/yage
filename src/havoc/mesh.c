#include "mesh.h"
#include "common.h"
#include "../m_alloc.h"
#include "../afac/afac.h"
#include "../assert.h"
#include "../io.h"
/*
	there are to ways for the mesh data to find its way into GPU memory.

	the first way is to call apon DMA its what glBufferData does if possible(i might be wrong, however memcpy would be very fucking slow)

	second way is to map the memory on the GPU to CPU address space where its more tangible, so a memcpy is plausible(the buffer approch)
	file -> HOST memory -> memcpy(GPU memory, HOST memory)

	there are other ways like fetching on the GPU side, where the GPU causes HOST memory to be read directly.
*/
void h_mesh_preload(h_meshp __m){
	
}

/*
this routine loads directly from original source location
*/

void static read_vert(struct afac *af, afac_vert_indx *v,_flu_float *vec,_flu_float *norm,_flu_float *uv){
	struct afac_vec3 *n;
	struct afac_vec3 *p;
	struct afac_vec3 *u;

	n = y_hungbuf_at(&af->norms,v->norm);
	p = y_hungbuf_at(&af->vecs,v->pos);
	u = y_hungbuf_at(&af->uv,v->uv);
	
	vec[0] = p->x;vec[1] = p->y;vec[2] = p->z;vec[3] = 1;
	norm[0] = n->x;norm[1] = n->y;norm[2] = n->z;norm[3] = 0;
	uv[0] = u->x;uv[1] = u->y;uv[2] = 0;uv[3] = 0;
}

void static read_tri(struct afac *af, afac_vert_indx *v,_flu_float *vec,_flu_float *norm,_flu_float *uv,afac_tri_indx *__idx){
	afac_vert_indx *v0,*v1,*v2;

	v0 = v+__idx->verts[0];
	v1 = v+__idx->verts[1];
	v2 = v+__idx->verts[2];

	read_vert(af,v0,vec,norm,uv);
	read_vert(af,v1,vec+4,norm+4,uv+4);
	read_vert(af,v2,vec+8,norm+8,uv+8);
}

struct data_share{
	_flu_float *vec;
	_flu_float *norm;
	_flu_float *uv;
};

void static loadgroup_data(struct afac *af, struct afac_group *g, struct data_share *__share){
	_int_u i;
	i = 0;
	for(;i != g->faces.off;i++) {
		struct afac_face *f = y_hungbuf_at(&g->faces,i);		
		_int_u j;
		j = 0;
		for(;j != f->n;j++){
			read_tri(af,f->v,__share->vec,__share->norm,__share->uv,f->tris_comprised+j);
			__share->vec+=12;
			__share->norm+=12;
			__share->uv+=12;
		}
	}
}

void static mesh_loaddata(struct afac *af, struct afac_group **__groups, _flu_float *vec,_flu_float *norm,_flu_float *uv){	
	struct data_share share = {
		vec,norm,uv
	};
	struct afac_group *g;
	_int_u i;
	i = 0;
	while((g = __groups[i]) != NULL){
		loadgroup_data(af,g,&share);
		i++;
	}
}

static _int_u fmtsizetab[] = {
	sizeof(_flue_float)*4
};

void static* hwbuf_withptr(void *__ptr,_int_u __size){
	void *hw;
	hw = _h_is.rd->buffer_create(__size);
	void *cpu_ptr;
	cpu_ptr = _h_is.rd->buffer_map(hw);
	mem_cpy(cpu_ptr,__ptr,__size);
	_h_is.rd->buffer_unmap(hw);
	return hw;
}

_int_u num_tris(struct afac_group **groups){
	_int_u res = 0;
	_int_u i;
	i = 0;
	struct afac_group *g;
	while((g = groups[i]) != NULL){
		res+=g->n_faces;
		i++;
	}
	return res;
}

void hv_mesh_load(struct h_mesh *__m) {
	if(!(__m->rs.info&RSRC_CIR)){
		printf("loading mesh data.\n");
		void *vecs;
		void *norms;
		void *uvs;

		_int_u vecsize;
		vecsize = fmtsizetab[0];
	
		/*
			first we make a memory copy as if we directly loaded it from HOST to HARDWARE then 
			it would be slow or congest things.
			im not sure how the kernel deals with GPU memory maps on the CPU side, however im going to assume its
			CPU -> PCI -> GPU
			
			but im not sure if there is any caching as i would assume so as using the PCI for writing one byte to GPU memory seems bad.
	
			the other way it might? would just be store every thing in HOST memory and upload when needed?.
		*/
		_int_u size_in_bytes = (__m->tri_cnt = num_tris(__m->af.groups))*3*vecsize;
		printf("space taken by mesh in bytes: %u.\n",size_in_bytes);
		__m->c.size_in_bytes = size_in_bytes;
		vecs	= m_alloc(size_in_bytes);
		norms	= m_alloc(size_in_bytes);
		uvs		= m_alloc(size_in_bytes);

		__m->c.vtx	= vecs;
		__m->c.norm	= norms;
		__m->c.uv		= uvs;
		
		mesh_loaddata(__m->af.ins,__m->af.groups,vecs,norms,uvs);
		__m->rs.info |= RSRC_CIR;
	}

	if(__m->rs.info&RSRC_LDRNDR){
		if(!(__m->rs.info&RSRC_COH)){
			printf("loading data onto hardware, buffers.\n");
			__m->vtx_buffer		= hwbuf_withptr(__m->c.vtx,__m->c.size_in_bytes);
			__m->norm_buffer	= hwbuf_withptr(__m->c.norm,__m->c.size_in_bytes);
			__m->uv_buffer		= hwbuf_withptr(__m->c.uv,__m->c.size_in_bytes);
			__m->rs.info |= RSRC_COH;
		}
	}
//	__m->tri_cnt = obj->tot;
}

void
hv_mesh_unload(struct h_mesh *__m){
	if(__m->rs.info&RSRC_COH){
		_h_is.rd->buffer_destory(__m->vtx_buffer);
		_h_is.rd->buffer_destory(__m->norm_buffer);
		_h_is.rd->buffer_destory(__m->uv_buffer);
	}
	__m->rs.info &= ~RSRC_COH;
}

#include "alloc.h"
struct h_render_object* h_render_object(void);
void hv_mesh_init(struct h_mesh *__m){
	h_rsrc_init(&__m->rs);
	__m->rs.load		= hv_mesh_load;
	__m->rs.unload	= hv_mesh_unload;
	__m->rs.info |= RSRC_LDRNDR;
	__m->tri_cnt = 0;
}
