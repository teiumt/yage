# include "common.h"
# include "../m_alloc.h"
# include "../vekas/common.h"
# include "../tmu.h"
# include "mare.h"
#include "../string.h"
#include "../m_alloc.h"
# include "window.h"
# include "renderer.h"
#include "hv_tex.h"
#include "resource.h"
#include "../env.h"
struct h_context hct;
static struct h_wd w = {
	.x = 0, .y = 64,
	.width = 512, .height = 512
};
void hvct_tapinto(void);
_8_s h_init(void) {
	_h_is.bits = 0;
	char const *graphics_EN = envget("NO_GRAPHICS");
	if(graphics_EN != NULL){
		if(!str_cmp(graphics_EN,"1")){
			_h_is.bits = HV_NO_GRAPHICS;
		}
	}

	f_lhash_init(&_h_is.treasury);

	mem_set(_h_is.signals,0,8*4096);
	y_log_file(&_h_is.log,"hvlog");
	hv_printf("start.\n");
	_h_is.rd = &h_rd_flue;
	w.width = _h_is.width;
	w.height = _h_is.height;
	
/*
	init maj and connect to vekas display server
	must be inited before flue-graphics
*/

	h_maj_init();

// init graphics core
	gc_init();
	void *ctx;
	ctx = gc_new();
	gc_make(ctx);
	mare_maj.wd_map(&w);
	mare_maj.make(&w, ctx);
	_h_is.ctx = ctx;
	
	_h_is.wd = &w;

	_h_is.comp.vpw = 1./(float)w.width;
	_h_is.comp.vph = 1./(float)w.height;
	_h_is.comp.ww = w.width;
	_h_is.comp.wh = w.height;
	_h_is.target = CLICK_BASE/_h_is.tgt_fps;
	_h_is.subsis = NULL;

	_h_is.consts = h_buffer_create(sizeof(float)*512);
	_h_is.consts_ptr = h_buffer_map(_h_is.consts);

	_h_is.maps_map[0].tex = tex_new();
	tex_data(_h_is.maps_map[0].tex,128,128);
	_h_is.maps_map[0].ptr = _h_is.maps_map[0].tex->tx->ptr;
	mem_set(_h_is.maps_map[0].ptr,255,128*128*4);
	_h_is.maps_alloc->at = 0;
	_h_is.maps_map[0].width = 128;
	_h_is.maps_map[0].height = 128;

	_h_is.routtop = NULL;
	hvct_tapinto();
	h_renderer_prime();
	h_render_init();


	_h_is.missing = tex_from_file(PITHDIR "missing.ppm",TEX_RGB);
#ifndef __noengine
	__asm__("movq %rbp, %rsp\n\tpop %rbp\n\tjmp _h_init");
#endif
}

_8_s h_de_init(void) {
	h_renderer_shutdown();
	h_buffer_destory(_h_is.consts);
	mare_maj.wd_unmap(&w);
	gc_deinit();
	h_maj_deinit();
	y_log_fileend(&_h_is.log);
#ifndef __noengine
	__asm__("movq %rbp, %rsp\n\tpop %rbp\n\tjmp _h_deinit");
#endif
}
