#ifndef __havoc__mesh__h
#define __havoc__mesh__h
#include "../y_int.h"
#include "../flue/types.h"
#include "resource.h"
#include "../afac/afac.h"
#include "hv_tex.h"
#include "types.h"
/*
	meshes cant contain there own textures,materials and such.
	when rendering the mesh those things are passed as parameters along with the mesh.


	there is two options here, either we bind textures,materials to meshes in an array.
	then when it comes to draw time we pass an index to what materials,textures?


FOR-REFERENCE:
	for openrw mesh structure is defined as "struct Geometry"
	vertices are stored in a "GeometryBuffer" that provides interfaces such
	as uploadVertices.

	uploadVertices GL{
		glGenBuffers
		glBindBuffer
		glBufferData
	}

	it also contains a materials organized in such fashion:

	struct Geometry{
		struct Texture{
			TextureData *data;
		}
		struct Material{
			vector<Texture> textures;
		}

		GeometryBuffer verts;
		vector<Material> materials;
	}

	the Geometry structure is incased in a Atomic class that to render:

	renderClump(struct Clump*)
		for every Atomic{
			->renderAtomic()
				->renderGeometry()
				for every SubGeom{
					[push render command to list]
				}
		}

	struct Atomic{
		//meaning for frame of refrence(world transforms and matrix tranforms)
		ModelFrame *frame;
		struct Geometry *geometry;
	}

	struct Clump{
		vector<Atomic> atomics;
	}

DOOM-3:
	for DOOM-3 a model is defined as "idRenderModel"
	it has a function called AddSurface.

	struct modelSurface{
		idMaterial
		srfTriangle {
			verts and indexes
		}
	}

	Model.AddSurface(struct modelSurface*)
*/


/*
	whats going to happen with this is this:
	
	struct geomdesc{
		int material_id;
		int offset_from_vertex_buffer;
		int number_of_triangles;
	};
	
	struct h_mesh{
		struct geomdesc geom[32];	
		void *vertex_buffer;
	}

	struct part{
		struct h_mesh mesh;
		struct h_texture textures[32];
	};

	struct model{
		struct part *parts_of_mode[32];
	}


*/

/*
	NOTE: meshes are to include geometrydesc
	struct geomdesc{
		int material_id;
		int offset_from_vertex_buffer;
    int number_of_triangles;
	}
	
	and also the person rendering sets materials and texture input for materials.

*/
#define MESH_TEX_MAX 32
typedef struct h_mesh {
	struct h_rsrc rs;

	struct hv_premises prem;
	_int_u tri_cnt;

	_int_u n;
	struct{
		//vertex groups this uses
		struct afac_group *groups[32];
		_int_u n_groups;
		struct afac *ins;
	}af;
	
	void *vtx_buffer;
	void *norm_buffer;

	/*
		NOTE:
			UV coords are swappable, meaning that we have three ways to changing textures on the mesh.

			A) we change the texture for another.
			B) we change UV coords to diffrent location on texture by swapping out buffer.
			C) perform transform on UV coords to relocate.
	*/
	void *uv_buffer;

	struct{
		_int_u size_in_bytes;
		void *vtx;
		void *norm;
		void *uv;
	}c;
} *h_meshp;

void hv_mesh_init(h_meshp);
#endif/*__havoc__mesh__h*/
