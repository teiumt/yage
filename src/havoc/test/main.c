#include "../mesh.h"
#include "../model.h"
# include "../../io.h"
#include "../../string.h"
# include "../common.h"
#include "../renderer.h"
#include "../camera.h"
#include "../../maths.h"
#include "../terrain.h"
#include "../../strf.h"
#include "../../m_alloc.h"
/*
	this is here for visualization
*/
#define RCTBUF_SIZE 1024
#define RCTSIZE (4*3)
static _int_u testing = 0;
_err_t main(int __argc, char const *__argv[]) {
	if(__argc<2) {
			goto _end;
	}
	if(!str_cmp(__argv[1],"uniform")) {
		testing = 1;
	}else if(!str_cmp(__argv[1],"scatter")){
		testing = 0;
	}else{
	_end:
		printf("args are 'uniform' or 'scatter'\n");
		return -1;
	}
	int width, height;
	width = 1024;
	height = 1024;
	_h_is.width = width;
	_h_is.height = height;
	_h_is.tgt_fps = 40;
	havoc_start();
}
struct point {
	_64_s x,y;
};
static void *rctbuf;
static float *rctbuf_ptr;
static void *pntbuf;
static float *pntbuf_ptr;

static void *rctwbuf;
static float *rctwbuf_ptr;


static struct node *intersect_list[1024];
static _int_u ncnt_intersect;

struct node {
	_64_u x,y;
	_64_u size;
	_8_s highlight;
	struct node *subnodes;
	_int_u cnt;
	struct point points[128];
};
static struct node root_node;

void static initnode(struct node *__n) {
	__n->subnodes = NULL;
	__n->cnt = 0;
	__n->size = 0;
	__n->highlight = -1;
}

_8_s plane_intersect(_64_u __xs, _64_u __xe, _64_u y, _64_u __x, _64_u __y, float dx, float dy) {
	float r;
	r = dx/dy;
	float d = y-__y;
	float j;
	j = (r*d)+__x;
	if(j>=__xs && j<__xe) {
		return 0;
	}		
	return -1;
}

static _int_u list_idx = 0;
void static fire_ray(struct node *n,struct node **list,_64_u __x, _64_u __y, float dx, float dy) {
	_8_u mask = 0;
	if(!plane_intersect(
		n->x,n->x+(n->size>>1),n->y,
		__x,__y,dx,dy
	)) {
		mask |= (1);
	}
	
	if(!plane_intersect(
		n->x+(n->size>>1),n->x+n->size,n->y,
		__x,__y,dx,dy
	)) {
		mask |= (2);
	}

	if(!plane_intersect(
		n->x,n->x+(n->size>>1),n->y+(n->size>>1),
		__x,__y,dx,dy
	)) {
		mask |= (4);
	}
	
	if(!plane_intersect(
		n->x+(n->size>>1),n->x+n->size,n->y+(n->size>>1),
		__x,__y,dx,dy
	)) {
		mask |= (8);
	}

	//y intersections
	//near section
	if(!plane_intersect(
		n->y,n->y+(n->size>>1),n->x,
		__y,__x,dy,dx
	)) {
		mask |= (1);
	}

	if(!plane_intersect(
		n->y+(n->size>>1),n->y+n->size,n->x,
		__y,__x,dy,dx
	)) {
		mask |= (4);
	}

	//far section
	if(!plane_intersect(
		n->y,n->y+(n->size>>1),n->x+(n->size>>1),
		__y,__x,dy,dx
	)) {
		mask |= (2);
	}

	if(!plane_intersect(
		n->y+(n->size>>1),n->y+n->size,n->x+(n->size>>1),
		__y,__x,dy,dx
	)) {
		mask |= (8);
	}

	if(!n->subnodes)return;
	struct node *_n;
	if(mask&1) {
		list[list_idx++] = _n = n->subnodes;
		fire_ray(_n,list,__x,__y,dx,dy);	
	}
	if(mask&2) {
		list[list_idx++] = _n = n->subnodes+1;
		fire_ray(_n,list,__x,__y,dx,dy);
	}
	if(mask&4) {
		list[list_idx++] = _n = n->subnodes+2;
		fire_ray(_n,list,__x,__y,dx,dy);
	}
	if(mask&8) {
		list[list_idx++] = _n = n->subnodes+3;
		fire_ray(_n,list,__x,__y,dx,dy);
	}
}

#include "../../assert.h"
void static place(_64_u __x, _64_u __y) {
	_64_u shft = 9;
	struct node *n;
	n = &root_node;

	_64_u x,y;
	_64_u dx = 0,dy = 0;
	while(1) {
		n->x = dx;
		n->y = dy;
		n->size = 1<<(shft+1);
		if(!testing){
		if(shft == 1 || n->cnt<2) {
			break;
		}
		}else{
			if(shft == 5) {
				break;
			}
		}
		
		//check which quadrant the point is in
		x = (__x>>shft)&1;
		y = (__y>>shft)&1;
		if(!n->subnodes) {
			n->subnodes = m_alloc(sizeof(struct node)*4);
			initnode(n->subnodes);
			initnode(n->subnodes+1);
			initnode(n->subnodes+2);
			initnode(n->subnodes+3);
		}

		n = n->subnodes+x+(y<<1);
		if(x) {
			dx+=(1<<shft);
		}
		if(y){
			dy+=(1<<shft);
		}
		shft--;
	}
	if(n->cnt == 128) {
		assert(1 == 0);
	}
	n->points[n->cnt].x = __x;
	n->points[n->cnt].y = __y;
	n->cnt++;
}
static _int_u rctbuf_pos;
static _int_u pntbuf_pos;
static _int_u rctwbuf_pos;
void static start_render(void) {
	rctbuf_pos = 0;
	pntbuf_pos = 0;
	rctwbuf_pos = 0;
}
//yes im that lazy to be using rects and not lines
void static draw_rect(float *r, float x, float y, float _x, float _y) {
	float rct[4] = {
		x,y,
		_x,_y
	};
	h_rect_geom(r,rct);
}

#define RCTBUF (rctbuf_ptr+(rctbuf_pos*RCTSIZE))
#define PNTBUF (pntbuf_ptr+(pntbuf_pos*RCTSIZE))
#define RCTWBUF (rctwbuf_ptr+(rctwbuf_pos*RCTSIZE))
void static draw_rect_wrect(float x, float y, float _x, float _y) {
	draw_rect(
		RCTWBUF,
		x,y,_x,_y
	);
	rctwbuf_pos++;
}


void static draw_rect_rect(float x, float y, float _x, float _y) {
	draw_rect(
		RCTBUF,
		x,y,_x,_y
	);

	rctbuf_pos++;

}

void static draw_rect_pnt(float x, float y, float _x, float _y){
	draw_rect(
		PNTBUF,
		x,y,_x,_y
	);
	pntbuf_pos++;
}

void static drawtree(struct node *__n) {
	if(__n->subnodes != NULL) {
		drawtree(__n->subnodes);
		drawtree(__n->subnodes+1);
		drawtree(__n->subnodes+2);
		drawtree(__n->subnodes+3);
	}
	if(__n->size>0) {
		if(!__n->highlight) {
			draw_rect_wrect(
				__n->x,__n->y,
				__n->x+__n->size,__n->y+__n->size	
			);
		}
		_64_u halfway = __n->size>>1;
		//vertical
		draw_rect_rect(
			__n->x+halfway-1,__n->y+16,
			__n->x+halfway+1,__n->y+__n->size-16
		);
		//horizontal
		draw_rect_rect(
			__n->x+16,__n->y+halfway-1,
			__n->x+__n->size-16,__n->y+halfway+1
		);
	}
	if(__n->cnt>0) {
		_int_u i;
		i = 0;
		for(;i != __n->cnt;i++) {
			struct point *pnt = __n->points+i;
			draw_rect_pnt(
				pnt->x-2,pnt->y-2,
				pnt->x+2,pnt->y+2
			);
		}
	}
}

static _64_s x = 0,y = 0;
static _8_s _rtexit = -1;
void _h_msg(struct h_msg *__m) { 
	if (__m->value == H_PTR_M) {
			x = __m->x;
			y = __m->y;
	}
}
#include "../../rand.h"
struct h_context hct;
HV_INIT {
	initnode(&root_node);
	rctbuf = _h_is.rd->buffer_create(RCTSIZE*RCTBUF_SIZE*sizeof(float));
	rctbuf_ptr = _h_is.rd->buffer_map(rctbuf);

	pntbuf = _h_is.rd->buffer_create(RCTSIZE*RCTBUF_SIZE*sizeof(float));
	pntbuf_ptr = _h_is.rd->buffer_map(pntbuf);

	rctwbuf = _h_is.rd->buffer_create(RCTSIZE*RCTBUF_SIZE*sizeof(float));
	rctwbuf_ptr = _h_is.rd->buffer_map(rctwbuf);

	if(!testing) {
	_int_u i;
	i = 0;
	for(;i != 256;i++){
		_64_u x,y;
		x = ffly_rand()%1023;
		y = ffly_rand()%1023;
		place(x,y);		
	}
	}else if (testing == 1) {
#define UNITS 128
	_int_u j,k;
	j = UNITS;
	for(;j != 1024+UNITS;j+=UNITS) {
		k = UNITS;
		for(;k != 1024+UNITS;k+=UNITS) {
			place(k-1,j-1);
		}
	}
	}
}

HV_DEINIT {
}

#include "../maths/dot.h"
#include "../maths/barycentric.h"

#include "../../flue/common.h"
_int_u static cntr = 0;
HV_TICK {

	return _rtexit;
}
#include "../../tmu.h"
#include "../../time.h"
void _h_draw(void) {
	list_idx = 0;
	struct node *list[1024];
	fire_ray(&root_node, list, 0,0,x,y);
	_int_u i;
	i = 0;
	for(;i != list_idx;i++) {
//		if(list[i]->subnodes != NULL)
//			continue;
		list[i]->highlight = 0;
	}

	start_render();
	drawtree(&root_node);
	if(rctwbuf_pos> 0)
		h_rect_withbuf(&hct,rctwbuf,rctwbuf_pos,0.8627,0.902,0.3804,0.1);
	if(pntbuf_pos> 0)
		h_rect_withbuf(&hct,pntbuf,pntbuf_pos,1.0,0.0,0.0,1.0);
	if(rctbuf_pos> 0)
		h_rect_withbuf(&hct,rctbuf,rctbuf_pos,1.0,1.0,1.0,1.0);

	float vtx[] = {0,0,x,y};
	h_lineto(&hct,vtx,1.0,0.0,0.0,1.0);
	i = 0;
	for(;i != list_idx;i++) {
//		if(list[i]->subnodes != NULL)
//			continue;
		list[i]->highlight = -1;
	}
}




