# include "barycentric.h"
# include "dot.h"
void h_barycentric(double __x, double __y, double __z, hvec3p __a, hvec3p __b, hvec3p __c, double *__alpha, double *__beta, double *__gama) {
    hvec3 v0 = {
        .x=__c->x-__a->x,
        .y=__c->y-__a->y,
        .z=__c->z-__a->z
    };

    hvec3 v1 = {
        .x=__b->x-__a->x,
        .y=__b->y-__a->y,
        .z=__b->z-__a->z
    };

    hvec3 v2 = {
        .x=__x-__a->x,
        .y=__y-__a->y,
        .z=__z-__a->z
    };

    double d00, d01, d11, d02, d12, den;
    d00 = h_dot(v0.x, v0.x, v0.y, v0.y, v0.z, v0.z);
    d01 = h_dot(v0.x, v1.x, v0.y, v1.y, v0.z, v1.z);
    d11 = h_dot(v1.x, v1.x, v1.y, v1.y, v1.z, v1.z);
    d02 = h_dot(v0.x, v2.x, v0.y, v2.y, v0.z, v2.z);
    d12 = h_dot(v1.x, v2.x, v1.y, v2.y, v1.z, v2.z);
    den = 1.0/(d00*d11-d01*d01);

    double a, b, g;
    a = (d11*d02-d01*d12)*den;
    b = (d00*d12-d01*d02)*den;
    *__gama = (1.0-a)-b;
    *__alpha = a;
    *__beta = b;
}
