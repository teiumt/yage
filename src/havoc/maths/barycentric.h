#ifndef __h__barycentric__h
#define __h__barycentric__h
#include "../vec.h"
void h_barycentric(double __x, double __y, double __z, hvec3p __a, hvec3p __b, hvec3p __c, double *__alpha, double *__beta, double *__gama);
#endif /*__h__barycentric__h*/
