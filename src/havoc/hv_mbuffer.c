#include "hv_mbuffer.h"
#include "../m_alloc.h"
#include "common.h"
void hv_mbuffer_data(struct hv_mbuffer *__buf,void *__ptr,_64_u __size){
	__buf->rs.info |= RSRC_CIR;	
	__buf->ptr = __ptr;
	__buf->size = __size;
}

void hv_mbuffer_load(struct hv_mbuffer *__buf){
	//ignored
	if(!(__buf->rs.info&RSRC_CIR)){
		__buf->rs.info |= RSRC_CIR;
	}

	if(__buf->rs.info&RSRC_LDRNDR){
		if(!(__buf->rs.info&RSRC_COH)){
			__buf->hw = _h_is.rd->buffer_create(_buf->size);
			void *cpu_ptr;
			cpu_ptr = _h_is.rd->buffer_map(__buf->hw);
			mem_cpy(cpu_ptr,__buf->ptr,__buf->size);
			_h_is.rd->buffer_unmap(__buf->hw);
			__buf->rs.info |= RSRC_COH;
		}
	}
}


void hv_mbuffer_unload(struct hv_mbuffer *__buf){
	if(__buf->rs.info&RSRC_COH){
		_h_is.rd->buffer_destory(__buf->hw);
		__buf->rs.info &= ~RSRC_COH;
	}
}

void hv_mbuffer_init(struct hv_mbuffer *__buf){
	h_rsrc_init(&__buf->rs);
	__buf->rs.load			=	hv_mbuffer_load;
	__buf->rs.unload		= hv_mbuffer_unload;
}
