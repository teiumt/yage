#ifndef __hv__pantry__h
#define __hv__pantry__h
#include "../afac/afac.h"
/*
	look i dont know how to name such a thing
	its purpose is as it name applies.

	EXAMPLE

	hv_ptyload(pty,"engines.obj");
	hv_ptyload(pty,"wheels.obj");
	

	struct model_desc dsc = {
		.pantry = "engines"
		.body = "steam_engine"
		.mainbuild = "engine0"
	};


	h_model_load(pty,&dsc);

	look the issue here is parts.
	how do we deal with model files that contain customizable parts?????
	those parts come strictly under a object and not all parts may be used.

	for example we have a train model, its split into diffrent objects.
	
	train_body,			<- the outside body
	train_frame,		<- all the other crap engine and stuff.
	train_wheels		<- wheels of train


	this is where we hit another issue(WHEELS)

	if we have alot of train models and they share a common traits
	like they use the same wheels.

	how do we manage somthing like this????

	we could have somthing like 

	/resources/train/wheels.obj
	/resources/train/engines.obj

	OR.
	/resources/train/wheels_and_engines.obj

	ive look at other engines and all follow this rull

	MODEL = "/resources/train/steam_engine.obj" <- includes wheels, and parts of engine.
*/

/*
	could be called a catalog
*/

/*
	NOTE: and object may have numerous vertex groups.

	many objects could be made out of the same object in this case.

	object(A) = group(A)+group(B)
	object(B) = group(A)+group(C)

	for example on a character model the nose may be diffrent in some small way(geometric wise).
*/
struct hv_ptyent{
	struct afac_object *obj;
	struct afac *loader;
	_64_u priv;
	void *meshes[32];
	_int_u n_meshes;
};

#include"../lib/hash.h"

#define HV_PTYAFAC 0
struct hv_pantry{
	struct f_lhash ents;
	_32_u bits;
};

void hv_namedptyload(char const*,char const*);
void hv_ptyload(struct hv_pantry*,char const*);
#endif/*__hv__pantry__h*/
