#ifndef __sdc__h
#define __sdc__h
#include "vec.h"
struct hv_premises{
	hvec3 near, far;
};

struct sdc_node{
	struct psp_node *down[8];
	_64_u userdata[512];
	struct hv_premises *prem[512];
	_64_u udcnt;
};

struct sdc_node* sdc_add(struct sdc_node*,struct hv_premises*);
extern struct sdc_node sdc_root;
#endif/*__sdc__h*/
