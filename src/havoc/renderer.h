#ifndef __h__renderer__h
#define __h__renderer__h
#include "common.h"
#include "model.h"
#include "hv_tex.h"
#include "hv_rdr_struc.h"
#include "subs.h"
#include "shader.h"
extern struct h_shader mesh_ps;
extern struct h_shader mesh_vs;

extern struct h_shader terr_ps;
extern struct h_shader terr_vs;

extern struct h_shader crect_ps;
extern struct h_shader crect_vs;

extern struct h_shader ui_ps;
extern struct h_shader ui_vs;
extern _64_u rect_link[8];
extern _64_u ui_link[8];
extern _64_u terr_link[8];

extern struct h_terrain *landscape;
/*
	this is whats going to happen, the main process is going to thug though 
	all the things that it needs to draw. this will in turn push render objects
	for the renderer.

	what im thinking is on the next relapse, the main process will do what ever it needs to do
	while the next frame is dished out.
*/

void h_render_render(void);
void h_renderer_wait(void);
void h_renderer_prime(void);
void h_renderer_shutdown(void);


void h_lineto(struct h_context *__ct,float*,float __r, float __g, float __b, float __a);
void h_rect_withbuf(struct h_context *__ct, void *__buf, _int_u __cnt, float __r, float __g, float __b, float __a);
void h_render_init(void);
void h_model_render(struct h_context *__ct, struct h_model*);
void h_uirect(struct h_context*,float *__vtx, _int_u, hv_texp __tx, float __r, float __g, float __b, float __a);
void h_rect(struct h_context*,float *__vtx, float __r, float __g, float __b, float __a);
void h_step(void);
#endif/*__h__renderer__h*/
