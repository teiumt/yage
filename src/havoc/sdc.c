#include "sdc.h"
#define LIMIT 20
#define SCALE (1)
#define toint(__x) ((_64_u)(__x*SCALE))

void static prime_node(struct sdc_node *__n){
	__n->down[0] = NULL;
	__n->down[1] = NULL;
	__n->down[2] = NULL;
	__n->down[3] = NULL;
	__n->down[4] = NULL;
	__n->down[5] = NULL;
	__n->down[6] = NULL;
	__n->down[7] = NULL;
	__n->udcnt = 0;
}

struct sdc_node* sdc_add(struct sdc_node *__rt,struct hv_premises *__p,_64_u __userdata){
	_64_u shft;
	shft = LIMIT;
	_64_u _xn = toint(__p->near->x);
	_64_u _yn = toint(__p->near->y);
	_64_u _zn = toint(__p->near->z);

	_64_u _xf = toint(__p->far->x);
	_64_u _yf = toint(__p->far->y);
	_64_u _zf = toint(__p->far->z);
	
	_64_u x0,y0,z0;
	_64_u x1,y1,z1;
	while(1){
		x0 = (_xn>>shft)&1;
		x1 = (_xf>>shft)&1;
		
		y0 = (_yn>>shft)&1;
		y1 = (_yf>>shft)&1;
		
		z0 = (_zn>>shft)&1;
		z1 = (_zf>>shft)&1;
	
		/*
			the basic thing here is that each of these should lay in
			there own area.
		*/
		if(x0 != x1 || y0 != y1 || z0 != z1){
			break;
		}

		_64_u index;
		index = x0+(y0<<1)+(z0<<2);
	
		struct sdc_node *n;
		if(!(n = __rt->down[index])){
			n = m_alloc(sizeof(struct sdc_node));
			__rt->down[index] = n;
			__rt = n;
			prime_node(n);
		}
		shft-=1;
	}
	
	_64_u index;
	index = __rt->udcnt++;
	__rt->prem[index] = __p;
	__rt->userdata = __userdata;
	return __rt;
}

struct sdc_node static* confining_node(struct sdc_node *__rt,struct hv_premises *__p){
	_64_u shft;
	shft = LIMIT;
	_64_u _xn = toint(__p->near->x);
	_64_u _yn = toint(__p->near->y);
	_64_u _zn = toint(__p->near->z);

	_64_u _xf = toint(__p->far->x);
	_64_u _yf = toint(__p->far->y);
	_64_u _zf = toint(__p->far->z);
	
	_64_u x0,y0,z0;
	_64_u x1,y1,z1;
	while(1){
		x0 = (_xn>>shft)&1;
		x1 = (_xf>>shft)&1;
		
		y0 = (_yn>>shft)&1;
		y1 = (_yf>>shft)&1;
		
		z0 = (_zn>>shft)&1;
		z1 = (_zf>>shft)&1;
	
		/*
			the basic thing here is that each of these should lay in
			there own area.
		*/
		if(x0 != x1 || y0 != y1 || z0 != z1){
			break;
		}

		_64_u index;
		index = x0+(y0<<1)+(z0<<2);
		if(!__rt->down[index])
			break;
		__rt = __rt->down[index];
		shft-=1;
	}
	return __rt;
}

_64_u static
premises_intrusion(struct sdc_node *n,struct hv_premises *__p, _64_u *__list, _int_u __size){
	_int_u i;
	i = 0;
	for(;i != n->udcnt;i++){

	}
}


void sdc_find(struct sdc_node *__rt,struct hv_premises *__p, _64_u *__list, _int_u __size){
	struct sdc_node *n;
	n = confining_node(__rt,__p);

}
