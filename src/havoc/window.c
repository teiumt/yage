# include "window.h"
# include "../m_alloc.h"
h_wdp h_wd_new(void) {
	h_wdp w;
	w = (h_wdp)m_alloc(sizeof(struct h_wd));
	w->m = &mare_maj;
	return w;
}

void h_wd_map(h_wdp __w) {
	__w->m->wd_map(__w);
}

void h_wd_unmap(h_wdp __w) {
	__w->m->wd_unmap(__w);
}
