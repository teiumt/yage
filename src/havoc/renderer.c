#include "renderer.h"
#include "shader.h"
#include "resource.h"
#include "../io.h"
#include "../assert.h"
#include "hv_tex.h"
struct h_shader mesh_ps = {
	.params = (struct h_shader_param[]){
		{"texture",H_SHADER_SAMPLE,0,3},
		{"incolour",H_SHADER_CONST,0,0},
		{NULL}
	},
	.src = PITHDIR "mesh_ps.fsl"
};
struct h_shader mesh_vs = {
	.params = (struct h_shader_param[]){
		{"position",H_SHADER_VERTEXBUF,0,0},
		{"normal",H_SHADER_VERTEXBUF,0,1},
		{"uvc",H_SHADER_VERTEXBUF,0,2},
		{"model",H_SHADER_CONST,208,0},
		{"normalmatrix",H_SHADER_CONST,144,0},
		{"proj",H_SHADER_CONST,80,0},
		{"trans",H_SHADER_CONST,16,0},
		{NULL}
	},
	.src = PITHDIR "mesh_vs.fsl"
};

struct h_shader terr_ps;
struct h_shader terr_vs = {
	.params = (struct h_shader_param[]){
		{"position",H_SHADER_VERTEXBUF,0,0},
		{"normal",H_SHADER_VERTEXBUF,0,1},
		{"uvc",H_SHADER_VERTEXBUF,0,2},
		{"model",H_SHADER_CONST,208,0},
		{"normalmatrix",H_SHADER_CONST,144,0},
		{"proj",H_SHADER_CONST,80,0},
		{"trans",H_SHADER_CONST,16,0},
		{NULL}
	},
	.src = PITHDIR "terr_vs.fsl"
};

struct h_shader crect_ps;
struct h_shader crect_vs;

struct h_shader ui_ps;
struct h_shader ui_vs;


struct h_shader c3d_ps;
struct h_shader c3d_vs;


_64_u rect_link[8];
_64_u ui_link[8];
_64_u terr_link[8] = {
	[H_RECT_TEXTURE] = 3,
	[H_RECT_SAMPLER] = 4,
	[H_RECT_POSITION] = 0,
	[H_RECT_NORMAL] = 1,
	[H_RECT_UV] = 2
};
void static _install(struct h_shader *sh){
	_int_u i;
	i = 0;
	struct h_shader_param *par;
	for(;(par = &sh->params[i])->name != NULL;i++){
		_h_is.rd->shader_link(sh->priv,par->name,par->type,par->offset,par->placement);
	}
}
void static _load_shader(struct h_shader *sh){
	h_shader_create(sh,0);
	_install(sh);
	h_shader_load_file(sh,sh->src);
}


void static
load_terrain_shader(void) {
	/*
		terrain shader
	*/
	h_shader_create(&terr_ps,0);
	h_shader_create(&terr_vs,0);
	_h_is.rd->shader_make(terr_ps.priv,terr_vs.priv);
	
	_h_is.rd->shader_enter();
	terr_link[H_RECT_CONSTS] = 0;
	terr_link[H_RECT_COLOUR] = 0;
	terr_link[H_RECT_TEXTURE] = 3;
	terr_link[H_RECT_SAMPLER] = 4;
	_h_is.rd->shader_link(terr_ps.priv,"consts",H_SHADER_POOL,0,0);
	_h_is.rd->shader_link(terr_ps.priv,"texture",H_SHADER_POOL,0,3);
	//	const data requires no placement	
	_h_is.rd->shader_link(terr_ps.priv,"incolour",H_SHADER_CONST,0,0);

	h_shader_load_file(&terr_ps,PITHDIR "terr_ps.fsl");	
	_h_is.rd->shader_enter();
	terr_link[H_RECT_POSITION] = 1;
	terr_link[H_RECT_NORMAL] = 2;
	_h_is.rd->shader_link(terr_vs.priv,"position",H_SHADER_VERTEXBUF,0,1);
	_h_is.rd->shader_link(terr_vs.priv,"model",H_SHADER_CONST,208,0);
	_h_is.rd->shader_link(terr_vs.priv,"normalmatrix",H_SHADER_CONST,144,0);
	_h_is.rd->shader_link(terr_vs.priv,"proj",H_SHADER_CONST,80,0);	
	_h_is.rd->shader_link(terr_vs.priv,"trans",H_SHADER_CONST,16,0);	
	_h_is.rd->shader_link(terr_vs.priv,"normal",H_SHADER_PRIMBUF,0,2);
	h_shader_load_file(&terr_vs,PITHDIR "terr_vs.fsl");
}

void static
load_colour_3d(void){
	h_shader_create(&c3d_ps,0);
	h_shader_create(&c3d_vs,0);
	_h_is.rd->shader_make(c3d_ps.priv,c3d_vs.priv);

	_h_is.rd->shader_enter();
	rect_link[H_RECT_COLOUR] = 0;
	_h_is.rd->shader_link(c3d_ps.priv,"incolour",H_SHADER_CONST,0,0);
	
	h_shader_load_file(&c3d_ps,PITHDIR "c3d_ps.fsl");
	
	_h_is.rd->shader_enter();
	rect_link[H_RECT_POSITION] = 0;
	_h_is.rd->shader_link(c3d_vs.priv,"position",H_SHADER_VERTEXBUF,0,0);
  _h_is.rd->shader_link(c3d_vs.priv,"model",H_SHADER_CONST,16,0);
  _h_is.rd->shader_link(c3d_vs.priv,"proj",H_SHADER_CONST,80,0);
  _h_is.rd->shader_link(c3d_vs.priv,"trans",H_SHADER_CONST,144,0);
 	h_shader_load_file(&c3d_vs,PITHDIR "c3d_vs.fsl");
}

void static
load_crect_shader(void) {
	h_shader_create(&crect_ps,0);
	h_shader_create(&crect_vs,0);
	_h_is.rd->shader_make(crect_ps.priv,crect_vs.priv);

	_h_is.rd->shader_enter();
	rect_link[H_RECT_CONSTS] = 0;
	rect_link[H_RECT_COLOUR] = 0;
	/*
		const data requires no placement
	*/
	_h_is.rd->shader_link(crect_ps.priv,"incolour",H_SHADER_CONST,0,0);
	h_shader_load_file(&crect_ps,PITHDIR "crect_ps.fsl");
	
	_h_is.rd->shader_enter();
	rect_link[H_RECT_POSITION] = 0;
	_h_is.rd->shader_link(crect_vs.priv,"position",H_SHADER_VERTEXBUF,0,0);
	h_shader_load_file(&crect_vs,PITHDIR "crect_vs.fsl");
}
void static
load_ui_shader(void){
	h_shader_create(&ui_ps,0);
	h_shader_create(&ui_vs,0);

	ui_link[H_UI_TEXTURE] = 0;
	_h_is.rd->shader_link(ui_ps.priv,"incolour",H_SHADER_CONST,0,0);
	_h_is.rd->shader_link(ui_ps.priv,"texture",H_SHADER_POOL,0,0);
	h_shader_load_file(&ui_ps,PITHDIR "ui_ps.fsl");

	ui_link[H_UI_POSITION] = 1;
	_h_is.rd->shader_link(ui_vs.priv,"position",H_SHADER_VERTEXBUF,0,1);
	_h_is.rd->shader_link(ui_vs.priv,"texcoords",H_SHADER_VERTEXBUF,4*4,1);
	_h_is.rd->shader_link(ui_vs.priv,"matrix",H_SHADER_CONST,4*4,0);
	h_shader_load_file(&ui_vs,PITHDIR "ui_vs.fsl");
}

void rd_shader_dump(void);
static hv_texp ground;
void h_render_init(void) {
	ground = tex_from_file(PITHDIR "ground.ppm",TEX_RGB);
	_h_is.rd->init();

	load_crect_shader();
	load_colour_3d();

	_load_shader(&mesh_ps);
	_load_shader(&mesh_vs);
	_load_shader(&terr_vs);
	load_ui_shader();

//	rd_shader_dump();
}

#include "terrain.h"
void 
draw_array0(void *vtxbuf, void *normal, void *indx, void *uvbuf, _int_u __n, void *__tx, void *__ps, void *__vs, _64_u *__linking);
void static mesh_render(struct h_context *__ct,struct h_mesh *__trn,hv_texp __texture) {
	tex_load(__texture);
	hv_rsrc_load(&__trn->rs);
	draw_array0(__trn->vtx_buffer,__trn->norm_buffer,NULL,__trn->uv_buffer,__trn->tri_cnt,__texture,mesh_ps.priv,mesh_vs.priv,terr_link);	
}

void h_model_render(struct h_context *__ct, struct h_model *__m) {
	_int_u i;
	i = 0;
	for(;i != __m->n_obj;i++) {
		struct h_mdlobject *o = __m->obj+i;
		void *tx = o->tx;
		if(!o->tx){
			tx = _h_is.missing;
		}
		struct h_mesh *m = o->mesh;
		mesh_render(__ct,m,tx);
	}
}
void h_terrain(struct h_context *__ct, struct h_terrain *__trn) {
	float *consts = _h_is.perc_const;
	consts[0] = 0;
	consts[1] = 1;
	consts[2] = 0;
	consts[3] = 1;	
	h_matcopY(consts+4,__ct->trans);
	h_matcopY(consts+4+16,__ct->proj);
	float *normmat = consts+4+16+16;
/*
	we dont have a model matrix as word space and model space are the same for now.
*/

	h_matcopY(normmat,h_m_ident);
	h_matrix_inv16(normmat,__ct->model);
	h_transpose(normmat);

	tex_load(ground);
	h_matcopY(consts+4+16+16+16,__ct->model);
	ffly_fprintf(ffly_err, "ZCOMP: %f.\n",__ct->proj[10]);
	_h_is.rd->draw_array(__trn->vertex_buffer,__trn->normal_buffer,__trn->index_buffer,__trn->tri_cnt,ground,mesh_ps.priv,terr_vs.priv,terr_link);	
}
#include "hv_render.h"
#include "../maths.h"
void static _3d_line(float *__vtx){
	float vtx[16];
	
	vtx[0] = __vtx[4];
	vtx[1] = __vtx[1];
	vtx[2] = __vtx[2];
	vtx[3] = 1;

	vtx[4] = __vtx[0];
	vtx[5] = __vtx[1];
	vtx[6] = __vtx[2];
	vtx[7] = 1;


	vtx[8] = __vtx[0];
	vtx[9] = __vtx[5];
	vtx[10] = __vtx[6];
	vtx[11] = 1;

	vtx[12] = __vtx[4];
	vtx[13] = __vtx[5];
	vtx[14] = __vtx[6];
	vtx[15] = 1;

	void *data;
	data = h_data(vtx,1,H_QUAD);
	h_quad(data,1,c3d_ps.priv,c3d_vs.priv,rect_link);
	h_buffer_destroy(data);
}

void h_static_subsis_render_prem(struct rdr_job *__r){
	h_subsisp __s = __r->data;
	struct h_context *__ct = _h_is.ct;

	float *consts = _h_is.perc_const;
	consts[0] = 0.325;//r
	consts[1] = 0.615;//g
	consts[2] = 0.756;//b
	consts[3] = 1;//a
	h_matcopY(consts+4+32,__ct->trans);
	float *trans = consts+4+16+16;
	trans[3]	-= __s->phy.pos.x;
	trans[7]	-= __s->phy.pos.y;
	trans[11] -= __s->phy.pos.z;

	trans[5] = -1;
	h_matcopY(consts+4+16,__ct->proj);
	h_matcopY(consts+4,__ct->model);

	h_modelp m = &__s->render.model;
	float vtx[8];
	vtx[0] = m->prem.near.x;
	vtx[1] = m->prem.near.y;
	vtx[2] = m->prem.near.z;
	vtx[3] = 1;

	vtx[4] = m->prem.far.x;
	vtx[5] = m->prem.far.y;
	vtx[6] = m->prem.far.z;
	vtx[7] = 1;
	_3d_line(vtx);
}


void h_static_subsis_render(struct rdr_job *__r) {
	h_static_subsis_render_prem(__r);

	h_subsisp __s = __r->data;
	struct h_context *__ct = _h_is.ct;
	float *consts = _h_is.perc_const;
	consts[0] = 0;
	consts[1] = 1;
	consts[2] = 0;
	consts[3] = 1;
	h_matcopY(consts+4,__ct->trans);
	float *trans = consts+4;
	trans[3]	-= __s->phy.pos.x;
	trans[7]	-= __s->phy.pos.y;
	trans[11] -= __s->phy.pos.z;

	trans[5] = -1;
	h_matcopY(consts+4+16,__ct->proj);
	float *normmat = consts+4+16+16;
/*
	we dont have a model matrix as word space and model space are the same for now.
*/

	h_matcopY(normmat,h_m_ident);
	h_matrix_inv16(normmat,__ct->model);
	h_transpose(normmat);
	float *model;
	h_matcopY(model = (consts+4+16+16+16),__ct->model);
	
	if(__s->control&1) {
		/* very bad! FIXME*/
		h_myrot(model,trad((-__s->rotx)+180));
	}
	
	ffly_fprintf(ffly_err, "ZCOMP: %f.\n",__ct->proj[10]);

	h_model_render(__ct,&__s->render.model);
}
void hv_subs_subs_contact(h_subsisp);
void hv_terrain_contact(h_subsisp);
void h_step(void) {
	h_subsisp s;
	s = h_sstop;
	while(s != NULL) {
		if(s->flags&SUBS_CONTACT_SUBS){
			hv_subs_subs_contact(s);
		}
		if(s->flags&SUBS_CONTACT_TERRAIN){
			hv_terrain_contact(s);
		}
		if(s->flags&SUBS_PHYSICS){		
			h_intergrate_forces_onto(s);
		}

		if(s->seat != NULL){
			s->phy.pos = s->seat->phy.pos;
			h_3vadd(&s->phy.pos,&s->phy.pos,&s->loc);
		}
		//TO-BE-REMOVED
		if(s->view){
			s->view(s);
		}
		s = s->next;
	}
}

