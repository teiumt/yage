# include "common.h"
# include "../m_alloc.h"
# include "../flue/common.h"
#include "../flue/mjx.h"
#include "../assert.h"
struct maj_wd {
	struct mjx_window w;
};


struct {
	struct maj_conn *conn;
} maj;

void h_maj_init(void) {
	maj.conn = maj_connect();
	assert(maj.conn != NULL);
	mjx_connect(maj.conn);
}

void h_maj_deinit(void) {
	mjx_disconnect(maj.conn);
}

void static wd_map(h_wdp __w) {
	struct maj_wd *w = __w;
	mjx_wdmap(maj.conn,__w, __w->x, __w->y, __w->width, __w->height);
	__w->render = &w->w.fb;
}

void static wd_unmap(h_wdp __w) {
	mjx_wdunmap(maj.conn,__w);
}
#define _ 0xff
static _8_u codemap[512] = {
	H_KEY_a,H_KEY_b,H_KEY_c,H_KEY_d,H_KEY_e,H_KEY_f,H_KEY_g,H_KEY_h,H_KEY_i,H_KEY_j,H_KEY_k,H_KEY_l,H_KEY_m,H_KEY_n,H_KEY_o,H_KEY_p,H_KEY_q,H_KEY_r,H_KEY_s,H_KEY_t,H_KEY_u,H_KEY_v,H_KEY_w,H_KEY_x,H_KEY_y,H_KEY_z,
	H_KEY_A,H_KEY_B,H_KEY_C,H_KEY_D,H_KEY_E,H_KEY_F,H_KEY_G,H_KEY_H,H_KEY_I,H_KEY_J,H_KEY_K,H_KEY_L,H_KEY_M,H_KEY_N,H_KEY_O,H_KEY_P,H_KEY_Q,H_KEY_R,H_KEY_S,H_KEY_T,H_KEY_U,H_KEY_V,H_KEY_W,H_KEY_X,H_KEY_Y,H_KEY_Z,
	H_BTN_LEFT,
	H_BTN_RIGHT,
	H_KEY_ESC,
	H_KEY_SPACE,
	H_KEY_LEFTSHIFT,
	H_KEY_UP,
	H_KEY_DOWN,
	H_KEY_LEFT,
	H_KEY_RIGHT,
	HY_F1,
	HY_F2,
	HY_F3,
	HY_F4,
	HY_F5,
	HY_F6,
	HY_F7,
	HY_F8,
	HY_F9,
	HY_F10,
	HY_F11,
	HY_F12,
	H_KEY_BACKSPACE,
	HY_DOT,
	HY_SLASH,
	[255] = _
};

static _8_u valmap[512] = {
	[VK_RELEASE] = H_RELEASE,
	[VK_PRESS] = H_PRESS,
	[VK_PTR_M] = H_PTR_M,
	[15] = _
};
#undef _
#include "../assert.h"
_int_u static messages(struct h_msg *__m) {
	_8_u msgbuf[4+sizeof(struct vk_msg)*16];
	maj_msg(maj.conn,NULL, msgbuf, sizeof(msgbuf));
	if (!*(_32_u*)msgbuf)
		return NULL;
	struct vk_msg *mb = msgbuf+4, *_m;
	_32_u cnt = *(_32_u*)msgbuf;
	if (cnt>16) {
		printf("too many messages to process, error!, got %u\n",cnt);
	}
	assert(cnt<=16);
	struct h_msg *m;
	_int_u i = 0;
	_int_u j = 0;
	for(;i != cnt;i++) {
		m = __m+j;
		_m = mb+i;
		if(_m->type == (_VK_MSG_CONTROL|_VK_MSG_PATCH_MOVE)){
			struct maj_patch *pt;
 			pt = maj.conn->patches[_m->newpatch.patch&63];
			pt->x = _m->newpatch.x;
			pt->y = _m->newpatch.y;
      continue;
    }
		m->x = _m->x;
		m->y = _m->y;
		m->id = 0;
		m->code = codemap[_m->code&0xff];		
		m->value = valmap[_m->value&15];
		hv_printf("message: {x: %u, y: %u}, code: %u, value: %u, %u\n",m->x,m->y,m->code,m->value,_m->value&15);
		j++;
	}
	return j;
}
/*
	do flue context binding
*/
void static make(h_wdp __w, flue_contextp __ctx) {
/*	struct maj_wd *w = __w->userdata;
	__ctx->prv = maj.conn;
	w->m.d.width = __w->width;
	w->m.d.height = __w->height;
	__ctx->draw = &w->m;
	__ctx->scaf = __ctx->d->scaf+FLUE_MAJ;
	__ctx->scaf->dw_init(__ctx->draw);
	_h_is.sf = __ctx->draw->sf;
*/
}

void static
swapbufs(h_wdp __w) {
	mjx_swapbufs(maj.conn,__w);
}

struct h_mare mare_maj = {
	wd_map,
	wd_unmap,
	messages,
	make,
	swapbufs
};
