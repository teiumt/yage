#ifndef __h__icon__h
#define __h__icon__h
#include "common.h"
#include "hv_tex.h"
struct h_icon_data {
	float uv[8];
};
struct h_icon {
	hv_texp tx;
	struct h_icon_data icons[8];
};
#endif /*__h__icon__h*/
