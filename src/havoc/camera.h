# ifndef __havoc__camera__h
# define __havoc__camera__h
# include "common.h"
# include "vec.h"
struct h_camera {
	struct h_vec3 at;
	struct h_vec3 pos;
	_flue_float ident[16];
	_flue_float m[16];
	_flue_float dist;
	_flue_float zoom;
};

void h_camera_start(struct h_camera*);
void h_camera_rotate(struct h_camera*, struct h_vec3*);
void h_camera_orbitprep(struct h_camera*, _flue_float);
void h_camera_orbitadj(struct h_camera*, struct h_vec3*);
void h_camera_lookat(struct h_camera*,
	_flu_float, _flu_float, _flu_float,
	_flu_float, _flu_float, _flu_float,
	_flu_float, _flu_float, _flu_float);
# endif /*__havoc__camera__h*/
