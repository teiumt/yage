#include "psp.h"
#include "../assert.h"
#include "../io.h"
#include "../m_alloc.h"
#include "../maths.h"
#include "maths.h"
#define MAP_SHFT 1
#define MAP_SIZE (1<<MAP_SHFT)
#define MAP_MASK (MAP_SIZE-1)
#define LIMIT 20
#define SCALE (1)
#define abs(__x) (((_64_u)floor((__x)<0?-(__x):(__x)))*SCALE)
static _64_u node_c = 0;
struct psp_node* psp_add(void *__p, struct psp_node *__root, float *_x, float *_y, float *_z){
	struct psp_node *n = __root;

	_64_u __x[3] = {abs(_x[0]),abs(_y[0]),abs(_z[0])};
	_64_u __y[3] = {abs(_x[1]),abs(_y[1]),abs(_z[1])};
	_64_u __z[3] = {abs(_x[2]),abs(_y[2]),abs(_z[2])};

//	printf(";;;;;;;;;;;;;;;;;;;;;;;;;;;\n%u,%u,%u\n%u,%u,%u\n%u,%u,%u\n\n",__x[0],__y[0],__z[0],__x[1],__y[1],__z[1],__x[2],__y[2],__z[2]);
	_64_u x0,y0,z0;
	_64_u x1,y1,z1;
	_64_u x2,y2,z2;

	_64_u xx = 0,yy = 0,zz = 0;
	_64_u shft = LIMIT;
	while(1) {
		n->size = 1<<(shft+1);
		n->x = xx;
		n->y = yy;
		n->z = zz;
	/*
					000		000		000
		divide		8		64		512

		NOTE:
			&0x7 is correct as we are trecking downwards,
			and we know whats above us.

			so we can just >>=3 to subdivide

			we have a max depth of 19,
			to intotal we have 19*32 entrys per spiral,
			basicly we if rewalked the same path we would
			accumulate 19*32 entries so 608.	
	*/
		x0 = (__x[0]>>shft)&0x1;
		x1 = (__x[1]>>shft)&0x1;
		x2 = (__x[2]>>shft)&0x1;

		y0 = (__y[0]>>shft)&0x1;
		y1 = (__y[1]>>shft)&0x1;
		y2 = (__y[2]>>shft)&0x1;

		z0 = (__z[0]>>shft)&0x1;
		z1 = (__z[1]>>shft)&0x1;
		z2 = (__z[2]>>shft)&0x1;

//		printf("COUNT: %u: %u # bits:\n%u, %u, %u\n%u, %u, %u\n%u, %u, %u\n\n",n->cnt, shft,x0,y0,z0,x1,y1,z1,x2,y2,z2);
		/*
			make sure are polygon points are all inside the same subdevide
		*/
		if (shft == 0 || n->cnt<8 || (x0 != x1 || x0 != x2 || y0 != y1 || y0 != y2 || z0 != z1  || z0 != z2)) {
	//		printf("%u, %u\n",shft == 4,n->cnt<32);
			break;
		}
		if (n->cnt>7) {
			if (!n->map) {
				n->map = m_alloc(8*sizeof(struct psp_node));
				_int_u i;
				i = 0;
				node_c++;
				ffly_fprintf(ffly_err,"node: %u.\n",node_c-1);
				for(;i != 8;i++) {
					n->map[i].parent = n;
					n->map[i].cnt = 0;
					n->map[i].map = NULL;
					n->map[i].x = 0;
					n->map[i].y = 0;
					n->map[i].z = 0;
					n->map[i].size = 0;
				}
			}
			/*
				as top level contains to many entrys lower ourselfs down
			
				8x8x8 map


				NOTE:
				if x0 is (1) then we are in the higher portion
				this applys to y0,z0
				
				if x0>SPLIT then (1)

				LOWER(0-7)(0)           HIGHER(8-15)(1)
				########			SPLIT			########

			*/
			n = n->map+(x0+(y0<<MAP_SHFT)+(z0<<(MAP_SHFT+MAP_SHFT)));
			if(x0 == 1) {
				xx+=1<<shft;
			}
			if(y0 == 1) {
				yy+=1<<shft;
			}
			if(z0 == 1) {
				zz+=1<<shft;
			}
			shft-=1;
		}
//		printf("loop.\n");
	}
//	printf("NODE: %p.\n",n);
	if (n->cnt>4094) {
	
		_int_u i;
		i = 0;
		for(;i != 8;i++) {
			float *p = n->list[i];
			printf("%f,%f,%f.\n",p[0],p[1],p[2]);
		}
		printf("DEPTH: %u.\n",shft);
		assert(1 == 0);
	}
	n->list[n->cnt] = _x;
	n->list[4096+n->cnt] = _y;
	n->list[(4096*2)+n->cnt] = _z;
//	printf("#%u.\n",n->cnt);
	n->cnt++;
	return n;
}

_64_u static revbits(_64_u __bits) {
	_64_u bits = 0;

	_int_u i;
	i = 0;
	for(;i != 64;i++) {
		bits |= ((__bits>>i)&(_64_u)1)<<(63-i);
	}
	return bits;
}

/*
	look we only support positive numbers be cause i hate signed integers
*/
/*
_8_s static can_intersect(struct psp_node *__n,_64_u __x, _64_u __y, _64_u __z, float dx, float dy, float dz) {
	_64_u x = __n->x;
	_64_u y = __n->y;
	_64_u z = __n->z;
	_64_u size = __n->size;

	_8_s r;
	//does the ray intersect the back plane?
	r = xyplane_intersect(x,x+size,y,y+size,z,__x,__y,__z,dx,dy,dz);
	if(!r)
		return 0;

	//front
	r = xyplane_intersect(x,x+size,y,y+size,z+size,__x,__y,__z,dx,dy,dz);
	if(!r)
		return 0;

}
*/
_8_s static octrant_intersection(struct psp_node *__n,_64_u __x, _64_u __y, _64_u __z, float dx, float dy, float dz) {
}
void psp_fire_ray(struct psp_node *__root, _64_u __x, _64_u __y, _64_u __z, float dx, float dy, float dz) {
	

}

_int_u static find(void **__list[], struct psp_node *__root, _64_u __x, _64_u __y, _64_u __z) {
	struct psp_node *n = __root;
	_64_u x,y,z;
	_64_u shft = LIMIT;
	_int_u i;
	i = 0;
	while(1) {
		x = (__x>>shft)&0x1;
		y = (__y>>shft)&0x1;
		z = (__z>>shft)&0x1;
		ffly_fprintf(ffly_err, "LEVEL-%u: CNT: %u.\n",shft,n->cnt);
		if (n->map != NULL) {
			n = n->map+(x+(y<<MAP_SHFT)+(z<<(MAP_SHFT+MAP_SHFT)));
			__list[i] = n->list;
			n->list[n->cnt] = NULL;
			i++;
			shft-=1;
		}else{
			break;
		}
	}

	return i;


}
#include "../assert.h"
_int_u psp_find(void **__list[], struct psp_node *__root, float __x, float __y, float __z) {
	ffly_fprintf(ffly_err,"FIND: %f,%f,%f.\n",__x,__y,__z);
	struct psp_node *n = __root, *tn;//test node
	_int_u i;
	i = 1;
	__list[0] = n->list;
	n->list[n->cnt] = NULL;

	i+=find(__list+i,n,__x*SCALE,__y*SCALE,__z*SCALE);
	return i;
}


void psp_rm(struct psp_node *__node) {

}
/*
	NOTE: the width and hight are the limits taken of a 64b int what ever that value is
	2^64
*/
/*
void psp_fire_ray(struct psp_node *__root, float x, float y, float z, float xe, float ye, float ze) {
	struct psp_node *n = __root;

	float dx,dy,dz;
	dx = xe-x;
	dy = ye-y;
	dz = ze-z;

	_64_u shft = LIMIT;
	

}
*/
