#ifndef __hv__vehicle__h
#define __hv__vehicle__h
/*
	this is the final product we dont calculate things depending on what things.
*/
enum{
	VEH_ENGINE_DIESEL,
	VEH_ENGINE_PETROL,
	VEH_ENGINE_ELECTRIC,
};

enum{
	VEH_DRIVE_FRONT,
	VEH_DRIVE_REAR,
	VEH_DRIVE_ALL
};

/*
	creating these structures;

	struct mycar{
		.wheels = {a,b,c,d},
		.chassis = achassis,
		.suspension = asuspension,
		.breaksystem = abreaksystem
		.engine = aengine
	};

	// function will use relative infomation to produce a schematic
	struct hv_vehinfo *make_vehicle_info(mycar);

	NOTE: these structures only exist when needed.
*/
struct hv_vehinfo{
	_16_u engine_typ;
	_16_u drive_typ;
	float mass;

	/*
		geometric traits(centre of mass)
	*/

	/*
		determines how long it takes a car to get speed
	*/
	float acceleration_rate;

	/*
		stalling conditions
	*/

	//max velocity
	float max_veloc;
	_64_u flags;

	_16_u chassis;
};

/*
	infomation about type of wheel,
	performance uptakes and downsides.
*/
struct hv_wheelinfo{
	_16_u wheel_model;
};

struct hv_veh_suspension{
	_16_u wheel;
};

struct hv_veh_wheel{
	struct hv_wheelinfo *info;
	/*
		less pressure = bad performance
		dodgy wheel = more fuel consumption,suspension damage,damage to car engine
	*/
	float pressure;
};

/*
	vehicle could be static object, meaning nothing is interactable but you can drive it.
	this means wheels are static(dont move or rotate).
*/
struct hv_vehicle{
	/*
		if level is below, vehicle wont start.
	
		in the case for electric cars this will be used.
		else its a starter battery.
	*/
	float battery_level;

	//fuel_tank
	float fuel_level;

	struct{
		float health;
	}engine;

	struct hv_veh_suspension suspension[8];
	struct hv_veh_wheel wheels[8];

	_16_u flags;


};
//incompatable
#endif/*__hv__vehicle__h*/
