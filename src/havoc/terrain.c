#include "common.h"
#include "../assert.h"
# include "terrain.h"
# include "../io.h"
# include "../m_alloc.h"
#define Z 1
#define T_X 64
#define T_Y 64
#define HM_W 512
#define HM_H 1024
#define XD (2048./(float)T_X)
#define YD (2048./(float)T_Y)
#define ZD (2048./(float)T_X)
#define PSP 10000

#define VX 0
#define VY 1
#define VZ 2
void static cross(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1) {
	*__dst		= __v0[VY]*__v1[VZ]-__v0[VZ]*__v1[VY];
	__dst[1]	= __v0[VZ]*__v1[VX]-__v0[VX]*__v1[VZ];
	__dst[2]	= __v0[VX]*__v1[VY]-__v0[VY]*__v1[VX];
}

void static normfor(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1,_flu_float *__v2) {
	_flu_float j[3],k[3];
	j[0] = __v1[0]-__v0[0];
	j[1] = __v1[1]-__v0[1];
	j[2] = __v1[2]-__v0[2];
	
	k[0] = __v2[0]-__v0[0];
	k[1] = __v2[1]-__v0[1];
	k[2] = __v2[2]-__v0[2];


	cross(__dst,j,k);
/*	
	if (__dst[0]<0){
		__dst[0] = -__dst[0];
	}
	if (__dst[1]<0){
		__dst[1] = -__dst[1];
	}
*/	
//	if (__dst[2]<0){
//		__dst[2] = -__dst[2];
//	}

/*
	DONT FUCKING CHANGE!
	YOU WILL REGRET IT!
*/
	__dst[3] = 0;
}
/*
	strut/swade/sway
*/
void strut_gen(float *__stx, float *__sty,float *__hmx, float *__hmy) {
	_int_u x,y;
	y = 0;
	for(;y != (HM_H/2)-1;y++) {
		x = 0;
		for(;x != HM_W-1;x++) {
			float *stx = __stx+x+(y*HM_W);
			float *sty = __sty+x+(y*HM_W);
		   	float *hmx = __hmx+(x+(y*HM_W))*3;
			float *hmy = __hmy+(x+(y*HM_W))*3;

			float *v0, *v1, *v2, *v3;
			v0 = hmx;
			v1 = hmx+1;
			v2 = hmy;
			v3 = hmy+1;

			stx[0] = ((v0[2]-v0[0])+(v0[2]-v0[1]))/16.;
			stx[1] = ((v1[2]-v1[0])+(v1[2]-v1[1]))/16.;
			sty[0] = ((v2[2]-v2[0])+(v2[2]-v2[1]))/16.;
			sty[1] = ((v3[2]-v3[0])+(v3[2]-v3[1]))/16.;
		}
	}
}

void static gen_terrain(struct h_terrain *__trn, float *__hmx, float *__hmy) {
	__trn->psp.map = NULL;
	__trn->psp.cnt = 0;

	float *stx = m_alloc(HM_W*(HM_H/2)*sizeof(float));
	float *sty = m_alloc(HM_W*(HM_H/2)*sizeof(float));
	strut_gen(stx,sty,__hmx,__hmy);
	
	_int_s x, y;
	_flu_float *p;
	_int_u n;
	n = (T_X+1)*(T_Y+1);
	_int_u tri_cnt = T_X*T_Y*2;
	// real tri count is this
	__trn->tri_cnt = (T_X-1)*(T_Y-1)*2;
	void *index_buf;
	void *buf;
	void *normbuf;
	index_buf = _h_is.rd->buffer_create(tri_cnt*sizeof(_32_u)*3);
	__trn->index_buffer = index_buf;
	buf = _h_is.rd->buffer_create(n*sizeof(_flue_float)*4);
	normbuf = _h_is.rd->buffer_create(tri_cnt*sizeof(_flue_float)*4);
	
	__trn->vertex_buffer = buf;
	__trn->normal_buffer = normbuf;
	__trn->nvtx = n;
	p = _h_is.rd->buffer_map(buf);
	_32_u *indx = _h_is.rd->buffer_map(index_buf);
	_flu_float *normal = _h_is.rd->buffer_map(normbuf);
/*
	p[0] = -0.5;
	p[1] = -0.5;
	p[2] = 0;
	p[3] = 1;

	p[4] = 0.5;
	p[5] = -0.5;
	p[6] = 0;
	p[7] = 1;

	p[8] = 0.5;
	p[9] = 0.5;
	p[10] = 0;
	p[11] = 1;

	normal[0] = 0;
	normal[1] = 1;
	normal[2] = 0;
	normal[3] = 0;

	normal[4] = 0;
	normal[5] = 1;
	normal[6] = 0;
	normal[7] = 0;

	normal[8] = 0;
	normal[9] = 1;
	normal[10] = 0;
	normal[11] = 0;
*/
	_flu_float tex[6*4] = {
		0, 0, 0, 1, 1, 1,
		1, 0, 0, 0, 0, 1,
		0, 0, 0, 1, 1, 1,
		1, 0, 0, 0, 0, 1
	};
	_flu_float xd, yd;
	xd = ((float)HM_W)/(float)T_X;
	yd = ((float)(HM_H/2))/(float)T_Y;
	_flu_float *t;
	_int_u i, z;
	i = 0;
//	for(;i != Z;i++) {
	z = i;
	t = tex+(i*6);
	/*
		where we layout all the vectors that make up are plane
	*/
	y = 0;
	for(;y != T_Y;y++) {
		x = 0;
		for(;x != T_X;x++) {
			float _x = x;
			float _y = y;
			float *v0;
		   	v0 = (__hmx+(((_int_u)(x*xd))+(((_int_u)(y*yd))*HM_W))*3);		
			float st0;
			st0 = *(stx+(((_int_u)(x*xd))+(((_int_u)(y*yd))*HM_W)));
			float *vtx = p+((x+(y*T_X))*4);
			float h;
			h = (v0[0]+v0[1]+v0[2])/3.0;
			/*strut/stray
				NOTE:
					XST is not realy stray in the x direction directly
			*/
			h*=4;
			vtx[0] = (_x+st0)*XD;
			vtx[1] = h*YD;//(h*st0*YD);
			vtx[2] = (_y)*ZD;
			vtx[3] = 1;
		}
	}

	/*
		we use points above to construct are geometry
	*/
	y = 0;
	for(;y != T_Y-1;y++) {
		x = 0;
		for(;x != T_X-1;x++) {
			indx[0] = x+(y*T_X);
			indx[1] = (x+1)+(y*T_X);
			indx[2] = x+((y+1)*T_X);
			indx[3] = (x+1)+(y*T_X);
			indx[4] = (x+1)+((y+1)*T_X);
			indx[5] = x+((y+1)*T_X);


			float *vt0, *vt1,*vt2;
			vt0 = p+(((x)+((y)*T_X))*4);
			vt1 = p+(((x+1)+((y)*T_X))*4);
			vt2 = p+(((x)+((y+1)*T_X))*4);
			psp_add(vt0,&__trn->psp,vt0,vt1,vt2);
			normfor(normal,vt0,vt1,vt2);

			normal+=4;
			indx+=6;
			
			vt0 = p+(((x+1)+(y*T_X))*4);
			vt1 = p+(((x+1)+((y+1)*T_X))*4);
			vt2 = p+((x+((y+1)*T_X))*4);
			psp_add(vt0,&__trn->psp,vt0,vt1,vt2);
			normfor(normal,vt0,vt1,vt2);
			normal+=4;	
		}
	}
//	_h_is.rd->buffer_unmap(buf);
	_h_is.rd->buffer_unmap(normbuf);
}
#include "../im.h"
# include "../system/file.h"
# include "../io.h"
# include "resource.h"
struct h_terrain* h_terrain_test(void) {
	struct h_terrain *t;
	t = m_alloc(sizeof(struct h_terrain));
	_int_u npx;
	/*
		NOTE:
			the first line goes in hmx the second line in hmx
	*/
	float *hmx = m_alloc(HM_W*(HM_H/2)*sizeof(float)*3);
	float *hmy = m_alloc(HM_W*(HM_H/2)*sizeof(float)*3);
	_8_u *pixels = m_alloc((npx = (HM_W*HM_H))*3);
	struct y_img im;
	im_load(&im,PITHDIR "heightmap.ppm");

	im.rgb_8_8_8(&im,pixels);
	_8_u *s;
	float *d;
	_int_u x, y;
	y = 0;
	for(;y != HM_H/2;y++) {
		x = 0;
		for(;x != HM_W;x++) {
			d = hmx+(x+(y*HM_W))*3;
			s = pixels+(x+(y*(HM_W*2)))*3;
			d[0] = (((float)s[0])*(1./255.))*1.4;
			d[1] = (((float)s[1])*(1./255.))*1.4;
			d[2] = (((float)s[2])*(1./255.))*1.4;
		}
	}

	y = 0;
	for(;y != HM_H/2;y++) {
		x = 0;
		for(;x != HM_W;x++) {
			d = hmy+(x+(y*HM_W))*3;
			s = pixels+(x+((y+1)*(HM_W*2)))*3;
			d[0] = (((float)s[0])*(1./255.))*1.4;
			d[1] = (((float)s[1])*(1./255.))*1.4;
			d[2] = (((float)s[2])*(1./255.))*1.4;
		}
	}

	m_free(pixels);
	gen_terrain(t, hmx,hmy);
	m_free(hmx);
	m_free(hmy);
	return t;
}

void h_terrain_destroy(struct h_terrain *__trn) {
	_h_is.rd->buffer_destory(__trn->vertex_buffer);
	_h_is.rd->buffer_destory(__trn->normal_buffer);
	m_free(__trn);
}
