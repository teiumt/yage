#include "common.h"
#include "../flue/common.h"
#include "../flue/shader.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../string.h"
/*
	TODO:
		change program to include all shader stages and not just one shader.

		i dont realy like that as it means we cant swapout apart of the pipeline very easily or ?
*/
struct buffer_list {
	struct flue_bo *bo;
	struct buffer_list *next;
};

struct buffer_list static *bo_list = NULL;
void static finish(void) {
//	flue_done();
	while(bo_list != NULL) {
		flue_bo_destroy(bo_list->bo);	
		struct buffer_list *bk = bo_list;
		bo_list = bo_list->next;
		m_free(bk);
		printf("HAVOC: BUFFER DEST.\n");
	}
	bo_list = NULL;
}

void static
framebuffer(_int_u __wdt, _int_u __hgt) {
	flue_framebuffer(__wdt,__hgt);
}

void static 
viewport(struct h_viewport	*__vp) {
	struct flue_viewport vp;
	vp.scale[0] = __vp->scale[0];
	vp.scale[1] = __vp->scale[1];
	vp.scale[2] = __vp->scale[2];

	vp.translate[0] = __vp->xyzw[0];
	vp.translate[1] = __vp->xyzw[1];
	vp.translate[2] = __vp->xyzw[2];
	flue_viewport(&vp,0,1);
}

struct program {
	struct flue_shader prog;
	_8_s ready;
};

void static*
shader_create(_64_u __pad) {
	struct program *prog = m_alloc(sizeof(struct program));
	flue_shader_new(prog);
	return prog;
}
#include "../ihc/fsl/fsl.h"
void static
shader_enter(void) {
//	fsl_init();
}

void static
shader_compile(struct program *__shader, char const *__path, _int_u __pathlen) {
	flue_shader_compile(__shader,__path,__pathlen);
}

void static shader_make(struct program *__ps, struct program *__vs) {
}
void static
shader(struct program *__shader,_int_u __id) {	
	switch(__id) {
		case H_SHADER_PIXEL:
			flue_shader(&__shader->prog,0);
		break;
		case H_SHADER_VETX:
			flue_shader(&__shader->prog,1);
		break;
	}
}

void static*
buffer_create(_int_u __size) {
	return flue_bo_data(__size);
}

void static*
buffer_map(struct flue_bo *__buf) {
	flue_bo_map(__buf);
	return __buf->cpu_map;
}

void static
buffer_unmap(struct flue_bo *__buf) {
	flue_bo_unmap(__buf);
}

void static
buffer_destroy(struct flue_bo *__buf) {
	flue_bo_destroy(__buf);
	printf("buffer:: %p.\n",__buf);
}

void static
start(void) {
	flue_pushstate();
}

void static rendertgs(void **__tgs,_int_u __n) {
	flue_rendertgs(__tgs,__n);
}
static struct flue_resspec vs_res_withuv;
static struct flue_resspec vs_res;
static struct flue_resspec vs_res0;
static _64_u placement[] = {0,1};
void static init(void) {
	vs_res_withuv = (struct flue_resspec){
		.stride = 4*2*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSVS,
		.fmt = {.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)},
		.id = FLUE_RS_VERTEX
	};

	vs_res = (struct flue_resspec){
		.stride = 4*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSVS,
		.fmt = {.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)},
		.id = FLUE_RS_VERTEX,
		.desc = 3,
		.bits = FLUE_RS_BUFFER_VERTEX
	};

	vs_res0 = (struct flue_resspec){
		.stride = 4*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSPS,
		.fmt = {.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)},
		.id = FLUE_RS_OTHER

	};
	_h_is.perc_const = FLUE_CTX->priv->const_data;
	flue_texp t;
	struct flue_fmtdesc fmt = {.depth = sizeof(float),.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32)};
	t = flue_tex_new(1920,1072,&fmt,0);
	flue_initz(t);
	_h_is.z = t;
}

void static shader_link(struct program *__shader,char const *__ident,_64_u __id, _64_u __offset, _64_u __placement) {
	//we want to start at user slot0
	__placement = FLUE_USERSLOT0+__placement;
	struct flue_resspec *res;
	_64_u type = 0;
	switch(__id) {
		case H_SHADER_PRIMBUF:
			res = &vs_res;
			type = FLUE_ALLOT_PRIM;
		break;
		case H_SHADER_VERTEXBUF:
			res = &vs_res;
			type = FLUE_ALLOT_VERTEX;
		break;
		case H_SHADER_POOL:
			res = &vs_res0;
		break;
		case H_SHADER_CONST:
			type = FLUE_ALLOT_CONST;
		break;
		case H_SHADER_SAMPLE:
			type = FLUE_ALLOT_SAMPLE;
		break;
		default:
			return;
	}

	flue_shader_allot(__shader,__ident,str_len(__ident),type,__offset,__placement);
	if(__id == H_SHADER_SAMPLE){
//		flue_placement(r,1,__placement+1);
	}
}

void static vertex_resource(void **__rs,_int_u __n,_64_u __plot) {
//	vs_res.placement = __plot;
//	flue_resources(__rs,&vs_res,1);
}
// a pool resource
/*
	a resource thats globaly accessable no tricks only offsets(stationary)
*/
void static pool(void **__rs,_int_u __n,_64_u __plot) {
//	vs_res0.placement = __plot;
//	flue_resources(__rs,&vs_res0,__n);
}
static struct flue_bo *buf = NULL;
/*
	__linking	:	tells us where resources+constants are to be placed
*/
void static rect_withbuf(void *__buf, _int_u __cnt, struct program *__ps, struct program *__vs, _64_u *__linking){
	void *resources[] = {__buf};
	flue_resources(resources,&vs_res,1,__linking+H_RECT_POSITION);
	shader(__ps,H_SHADER_PIXEL);
	shader(__vs,H_SHADER_VETX);

	/*offset is for index buffers*/
	flue_draw_array(NULL,0,__cnt,FLUE_RECT);
}

void static rect(float *__vtx, struct program *__ps, struct program *__vs, _64_u *__linking) {
	buf = flue_bo_data(sizeof(float)*4*16);
	struct buffer_list *ent = m_alloc(sizeof(struct buffer_list));
	ent->next = bo_list;
	bo_list = ent;
	ent->bo = buf;
	printf("BUFFER: %p.\n",buf);
	flue_bo_map(buf);
	float *vtx = buf->cpu_map;
	/*
		V0 = {0,1}
		V1 = {2,3}
	*/
	vtx[0] = __vtx[0];
	vtx[1] = __vtx[1];
	vtx[2] = 0;
	vtx[3] = 1;

	vtx[4] = __vtx[2];
	vtx[5] = __vtx[1];
	vtx[6] = 0;
	vtx[7] = 1;

	vtx[8] = __vtx[0];
	vtx[9] = __vtx[3];
	vtx[10] = 0;
	vtx[11] = 1;

//	printf(">>>>>>>>>>>%f, %f, - %f, %f, - %f, %f.\n",vtx[0],vtx[1],vtx[4],vtx[5],vtx[8],vtx[9]);
	flue_bo_unmap(buf);

	rect_withbuf(buf,1,__ps,__vs,__linking);

}

void static* data(float *__vtx, _int_u __cnt, _int_u __type) {
	buf = NULL;
	_int_u size;
	if(__type == H_TRI3){
		
	}else if (__type == H_QUAD){
		size = sizeof(float)*4*4*__cnt;
		buf = flue_bo_data(size);
		flue_bo_map(buf);
		float *vtx = buf->cpu_map;
		_int_u i;
		i = 0;
		for(;i != __cnt;i++){
			vtx[0] = __vtx[0];
			vtx[1] = __vtx[1];
			vtx[2] = __vtx[2];
			vtx[3] = 1;

			vtx[4] = __vtx[4];
			vtx[5] = __vtx[5];
			vtx[6] = __vtx[6];
			vtx[7] = 1;
	
			vtx[8] = __vtx[8];
			vtx[9] = __vtx[9];
			vtx[10] = __vtx[10];
			vtx[11] = 1;

			vtx[12] = __vtx[12];
			vtx[13] = __vtx[13];
			vtx[14] = __vtx[14];
			vtx[15] = 1;

			vtx+=16;
			__vtx+=16;
		}
	}
	return buf;
}

void static quad(void *__buf,_int_u __cnt,struct program *__ps, struct program *__vs, _64_u *__linking) {
	void *resources[] = {__buf};
	flue_rsdesc(FLUE_USERSLOT0,__linking[H_RECT_POSITION],FLUE_RS_BUFFER_VERTEX);
	vs_res.desc = FLUE_USERSLOT0;
	flue_resources(resources,&vs_res,1,NULL);
//	flue_resources(consts,&vs_res0,1,__linking+H_RECT_CONSTS);

	shader(__ps,H_SHADER_PIXEL);
	shader(__vs,H_SHADER_VETX);

	/*offset is for index buffers*/
	flue_draw_array(NULL,0,__cnt,FLUE_QUAD);
}


void static 
ui_rect(float *__vtx, _int_u __cnt, hv_texp __tx, struct program *__ps, struct program *__vs, _64_u *__linking) {
	buf = flue_bo_data(sizeof(float)*4*8*__cnt);
	flue_bo_map(buf);
	float *vtx = buf->cpu_map;
	_int_u i = 0;
	for (;i != __cnt*24;i+=24) {
	float *vt = vtx+i;
	/*
		V0 = {0,1}
		V1 = {2,3}
	*/
	vt[0] = __vtx[0];
	vt[1] = __vtx[1];
	vt[2] = 0;
	vt[3] = 1;


	/*uv*/
	vt[4] = __vtx[4];
	vt[5] = __vtx[5];
	vt[6] = 0;
	vt[7] = 0;
	

	vt[8] = __vtx[2];
	vt[9] = __vtx[1];
	vt[10] = 0;
	vt[11] = 1;

	/*uv*/
	vt[12] = __vtx[6]-__vtx[4];
	vt[13] = 0;//__vtx[7];
	vt[14] = 0;
	vt[15] = 0;
	

	vt[16] = __vtx[0];
	vt[17] = __vtx[3];
	vt[18] = 0;
	vt[19] = 1;

	/*uv*/
	vt[20] = 0;//__vtx[8];
	vt[21] = __vtx[9]-__vtx[5];
	vt[22] = 0;
	vt[23] = 0;
	__vtx+=12;
	}

/*
	printf("##### %f, %f, %f, %f\n",vtx[0],vtx[1],vtx[2],vtx[3]);
	printf("##### %f, %f, %f, %f\n",vtx[4],vtx[5],vtx[6],vtx[7]);
	printf("##### %f, %f, %f, %f\n",vtx[8],vtx[9],vtx[10],vtx[11]);
	printf("##### %f, %f, %f, %f\n",vtx[12],vtx[13],vtx[14],vtx[15]);
	printf("##### %f, %f, %f, %f\n",vtx[16],vtx[17],vtx[18],vtx[19]);
	printf("##### %f, %f, %f, %f\n",vtx[20],vtx[21],vtx[22],vtx[23]);
*/
//	printf("%f, %f, - %f, %f, - %f, %f.\n",vtx[0],vtx[1],vtx[4],vtx[5],vtx[8],vtx[9]);
	flue_bo_unmap(buf);

	flue_clearslots();
#define H_ui(__x,__y) __x[H_UI_ ## __y]
	flue_rsdesc(
		FLUE_USERSLOT(H_ui(__linking,POSITION)),
		H_ui(__linking,POSITION),
		FLUE_RS_BUFFER_VERTEX
	);
	
	flue_txdesc(
		FLUE_USERSLOT(H_ui(__linking,TEXTURE)),
		H_ui(__linking,TEXTURE),-1
	);

	void *resources[] = {buf};
	void *textures[] = {__tx->tx};
	vs_res_withuv.desc = FLUE_USERSLOT(H_ui(__linking,POSITION));
	flue_resources(resources,&vs_res_withuv,1,NULL);

	_64_u linking[] = {
		FLUE_USERSLOT(H_ui(__linking,TEXTURE))
	};
	flue_textures(textures,FLUE_RSPS,1,linking);
 
	
	shader(__ps,H_SHADER_PIXEL);
	shader(__vs,H_SHADER_VETX);

	flue_draw_array(NULL,0,__cnt,FLUE_RECT);
	flue_bo_destroy(buf);
}
void
draw_array0(void *vtxbuf, void *normal, void *indx, void *uvbuf, _int_u __n, hv_texp __tx, void *__ps, void *__vs, _64_u *__linking){ 
	void *resources[] = {vtxbuf,normal,uvbuf};
	void *textures[] = {__tx->tx};

	flue_clearslots();
#define H_rect(__x,__y) __x[H_RECT_ ## __y]
	flue_rsdesc(
		FLUE_USERSLOT(H_rect(__linking,POSITION)),
		H_rect(__linking,POSITION),
		FLUE_RS_BUFFER_VERTEX
	);
	flue_rsdesc(
		FLUE_USERSLOT(H_rect(__linking,NORMAL)),
		H_rect(__linking,NORMAL),
		FLUE_RS_BUFFER_VERTEX
	);
	flue_rsdesc(
		FLUE_USERSLOT(H_rect(__linking,UV)),
		H_rect(__linking,UV),
		FLUE_RS_BUFFER_VERTEX
	);

	flue_txdesc(
		FLUE_USERSLOT(H_rect(__linking,TEXTURE)),
		H_rect(__linking,TEXTURE),
		H_rect(__linking,SAMPLER)
	);

	_64_u linking[] = {
		FLUE_USERSLOT(H_rect(__linking,TEXTURE)),
	};
	
	struct flue_sampler s;	
	flue_textures(textures,FLUE_RSPS,1,linking);
	flue_sampler(&s,1,linking);

	vs_res.desc = FLUE_USERSLOT(H_rect(__linking,POSITION));
	flue_resources(resources,&vs_res,1,NULL);
	
	vs_res.desc = FLUE_USERSLOT(H_rect(__linking,NORMAL));
	flue_resources(resources+1,&vs_res,1,NULL);

	vs_res.desc = FLUE_USERSLOT(H_rect(__linking,UV));
	flue_resources(resources+2,&vs_res,1,NULL);

	shader(__ps,H_SHADER_PIXEL);
	shader(__vs,H_SHADER_VETX);

	flue_draw_array(indx,0,__n,FLUE_TRI3);
}

void static
draw_array(void *vtxbuf, void *normal, void *indx, _int_u __n, hv_texp __tx, struct program *__ps, struct program *__vs, _64_u *__linking) {
	void *resources[] = {vtxbuf,normal};
	void *textures[] = {__tx->tx};

	flue_clearslots();
	flue_rsdesc(
		FLUE_USERSLOT(H_rect(__linking,POSITION)),
		H_rect(__linking,POSITION),
		FLUE_RS_BUFFER_VERTEX
	);
	flue_rsdesc(
		FLUE_USERSLOT(H_rect(__linking,NORMAL)),
		H_rect(__linking,NORMAL),
		FLUE_RS_BUFFER_VERTEX
	);

	flue_txdesc(
		FLUE_USERSLOT(H_rect(__linking,TEXTURE)),
		H_rect(__linking,TEXTURE),
		-1//we dont need a sampler for this
	);

	_64_u linking[] = {
		FLUE_USERSLOT(H_rect(__linking,TEXTURE)),
	};
	
	struct flue_sampler s;	
	flue_textures(textures,FLUE_RSPS,1,linking);
	flue_sampler(&s,1,linking);

	vs_res.desc = FLUE_USERSLOT(H_rect(__linking,POSITION));
	flue_resources(resources,&vs_res,1,NULL);
	
	vs_res.desc = FLUE_USERSLOT(H_rect(__linking,NORMAL));
	flue_resources(resources+1,&vs_res,1,NULL);
	
	shader(__ps,H_SHADER_PIXEL);
	shader(__vs,H_SHADER_VETX);

	flue_draw_array(indx,0,__n,FLUE_TRI3);
}


void static _clear(void **__list, _int_u __n,float r,float g,float b, float a) {
	flue_clear(__list,__n,r,g,b,a);
}

struct h_render_device h_rd_flue = {
	draw_array,
	finish,
	framebuffer,
	viewport,
	shader_compile,
	shader_create,
	shader,
	buffer_create,
	buffer_map,
	buffer_unmap,
	buffer_destroy,
	start,
	rendertgs,
	vertex_resource,
	pool,
	shader_enter,
	shader_link,
	init,
	rect,
	shader_make,
	_clear,
	ui_rect,
	rect_withbuf,
	quad,
	data
};
