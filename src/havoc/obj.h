# ifndef __havoc__obj__h
# define __havoc__obj__h
# include "../y_int.h"

struct h_obj {
	void *p;
	struct h_obj *next;
};

struct pray {
	void(*demolish)(struct h_obj*);	
};

struct pray_block {
	struct pray *pray;
	struct h_obj *j;
};
struct exp_block {
	_64_u exp;
	struct pray_block mem;
};

extern struct exp_block exp[1];

#define OBJ_EXP0_MEM &exp[0].mem
struct h_obj* obj_new(void);
void obj_exp(struct h_obj*, struct pray_block*);
void obj_switch(void);
void exp_preform(void);
# endif /*__havoc__obj__h*/
