#include "common.h"
#include "subs.h"
#include "../maths.h"
/*
	as seen in the Source Engine
	when entreing a vehicle a role is also specified,
	such as ROLE_DRIVER.

	or 
	ROLE_PASSENGER
*/
void hv_vehicle_control(h_subsisp __vehicle,struct h_msg *__m);
void hv_player_entre_vehicle(h_subsisp __player){
	hv_printf("player entering vehicle.\n");
	//disable physiscs on player
	__player->flags &= ~SUBS_PHYSICS;
	__player->flags |= SUBS_IN_VEHICLE;
	__player->seat = __player->contacter;
	h_3vsub(&__player->loc,&__player->phy.pos,&__player->seat->phy.pos);
}

void hv_player_leave_vehicle(h_subsisp __player){
	hv_printf("player leaving vehicle.\n");
	__player->flags |= SUBS_PHYSICS;
	__player->flags &= ~SUBS_IN_VEHICLE;
	__player->seat = NULL;
}


void hv_player_control(h_subsisp __player,struct h_msg *__m){
	 hv_printf("playser: {x: %u, y: %u}, code: %u, value: %u\n",__m->x,__m->y,__m->code,__m->value);
	if(__player->flags&SUBS_IN_VEHICLE){
		if(__m->value == H_PRESS && __m->code == H_KEY_E){
			hv_player_leave_vehicle(__player);
			return;
		}
		hv_vehicle_control(__player->contacter,__m);
		return;
	}
	if(__m->value == H_PTR_M){
		__player->rotx = ((float)(__m->x-(_h_is.width/2)))*0.2;
		__player->roty = ((float)(__m->y-(_h_is.height/2)))*0.2;
	}else
	if(__m->value == H_PRESS){
		switch(__m->code){
			case H_KEY_E:
				if(__player->share&SUBS_SHARE_VEHICLE){
					hv_player_entre_vehicle(__player);
				}
			break;
			case H_KEY_W:
				__player->player.z = 1;
			break;
			case H_KEY_A:
				__player->player.x = 1;
			break;
			case H_KEY_S:
				__player->player.z = -1;
			break;
			case H_KEY_D:
				__player->player.x = -1;
			break;
			case H_KEY_SPACE:
				__player->player.y = 1;
			break;
			case H_KEY_LEFTSHIFT:
				__player->player.y = -1;
			break;
			case H_KEY_LEFT:
				__player->roty+=10;
			break;
			case H_KEY_RIGHT:
				__player->roty-=10;
			break;
			case H_KEY_UP:
				__player->rotx+=10;
			break;
			case H_KEY_DOWN:
				__player->rotx-=10;
			break;
		}

	}else
	if(__m->value == H_RELEASE){
		switch(__m->code){
			case H_KEY_W:
			case H_KEY_S:
				__player->player.z = 0;
				__player->phy.vel.z = 0;
			break;
			case H_KEY_A:
			case H_KEY_D:
				__player->player.x = 0;
				__player->phy.vel.x = 0;
			break;
			case H_KEY_SPACE:
			case H_KEY_LEFTSHIFT:
				__player->player.y = 0;
				__player->phy.vel.y = 0;
			break;
		}
	}

	float _x = __player->player.x*8,_y = __player->player.y*8,_z = __player->player.z*8;
	__player->phy.vel.x = -((_x*cos(trad(__player->rotx)))+_z*sin(trad(-__player->rotx)));
	__player->phy.vel.y = -_y;
	__player->phy.vel.z = -(_z*cos(trad(-__player->rotx))+(_x*sin(trad(__player->rotx))));
}
