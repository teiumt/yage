#ifndef __havoc__keysym__h
#define __havoc__keysym__h
#define H_KEY_a        0
#define H_KEY_b        1
#define H_KEY_c        2
#define H_KEY_d        3
#define H_KEY_e        4
#define H_KEY_f        5
#define H_KEY_g        6
#define H_KEY_h        7
#define H_KEY_i        8
#define H_KEY_j        9
#define H_KEY_k        10
#define H_KEY_l        11
#define H_KEY_m        12
#define H_KEY_n        13
#define H_KEY_o        14
#define H_KEY_p        15
#define H_KEY_q        16
#define H_KEY_r        17
#define H_KEY_s        18
#define H_KEY_t        19
#define H_KEY_u        20
#define H_KEY_v        21
#define H_KEY_w        22
#define H_KEY_x        23
#define H_KEY_y        24
#define H_KEY_z        25

#define H_KEY_A        26
#define H_KEY_B        27
#define H_KEY_C        28
#define H_KEY_D        29
#define H_KEY_E        30
#define H_KEY_F        31
#define H_KEY_G        32
#define H_KEY_H        33
#define H_KEY_I        34
#define H_KEY_J        35
#define H_KEY_K        36
#define H_KEY_L        37
#define H_KEY_M        38
#define H_KEY_N        39
#define H_KEY_O        40
#define H_KEY_P        41
#define H_KEY_Q        42
#define H_KEY_R        43
#define H_KEY_S        44
#define H_KEY_T        45
#define H_KEY_U        46
#define H_KEY_V        47
#define H_KEY_W        48
#define H_KEY_X        49
#define H_KEY_Y        50
#define H_KEY_Z        51

#define H_BTN_LEFT     52
#define H_BTN_RIGHT    53


#define H_KEY_ESC          54
#define H_KEY_SPACE    55
#define H_KEY_LEFTSHIFT    56
#define H_KEY_UP       57
#define H_KEY_DOWN     58
#define H_KEY_LEFT     59
#define H_KEY_RIGHT    60

#define HY_F1           61
#define HY_F2           62
#define HY_F3           63
#define HY_F4           64
#define HY_F5           65
#define HY_F6           66
#define HY_F7           67
#define HY_F8           68
#define HY_F9           69
#define HY_F10          70
#define HY_F11          71
#define HY_F12          72
#define H_KEY_BACKSPACE 73
#define HY_DOT          74
#define HY_SLASH        75

#define H_RELEASE   0
#define H_PRESS     1
#define H_PTR_M     2
#endif/*__havoc__keysym__h*/
