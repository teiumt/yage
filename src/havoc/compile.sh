#!/bin/sh

$ffly_cc $cc_flags -c -o $dst_dir/entry.o $root_dir/entry.c
$ffly_cc $cc_flags -c -o $dst_dir/init.o $root_dir/init.c
$ffly_cc $cc_flags -c -o $dst_dir/loop.o $root_dir/loop.c
$ffly_cc $cc_flags -c -o $dst_dir/common.o $root_dir/common.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_tex.o $root_dir/hv_tex.c
$ffly_cc $cc_flags -c -o $dst_dir/shader.o $root_dir/shader.c
$ffly_cc $cc_flags -c -o $dst_dir/renderer.o $root_dir/renderer.c
$ffly_cc $cc_flags -c -o $dst_dir/obj.o $root_dir/obj.c
$ffly_cc $cc_flags -c -o $dst_dir/maj_m.o $root_dir/maj_m.c
$ffly_cc $cc_flags -c -o $dst_dir/terrain.o $root_dir/terrain.c
$ffly_cc $cc_flags -c -o $dst_dir/matrix.o $root_dir/matrix.c
$ffly_cc $cc_flags -c -o $dst_dir/camera.o $root_dir/camera.c
$ffly_cc $cc_flags -c -o $dst_dir/fsb.o $root_dir/fsb.c
$ffly_cc $cc_flags -c -o $dst_dir/mesh.o $root_dir/mesh.c
$ffly_cc $cc_flags -c -o $dst_dir/subs.o $root_dir/subs.c
$ffly_cc $cc_flags -c -o $dst_dir/flue.o $root_dir/flue.c
$ffly_cc $cc_flags -c -o $dst_dir/psp.o $root_dir/psp.c
$ffly_cc $cc_flags -c -o $dst_dir/maths/barycentric.o $root_dir/maths/barycentric.c
$ffly_cc $cc_flags -c -o $dst_dir/maths/plane.o $root_dir/maths/plane.c
$ffly_cc $cc_flags -c -o $dst_dir/vector.o $root_dir/vector.c
$ffly_cc $cc_flags -c -o $dst_dir/model.o $root_dir/model.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_rdr_antics.o $root_dir/hv_rdr_antics.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_physics.o $root_dir/physics/hv_physics.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_rsrc.o $root_dir/hv_rsrc.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_draw.o $root_dir/hv_draw.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_player.o $root_dir/hv_player.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_vehicle.o $root_dir/hv_vehicle.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_disk.o $root_dir/hv_disk.c
$ffly_cc $cc_flags -c -o $dst_dir/hv_pantry.o $root_dir/hv_pantry.c
$ffly_cc $cc_flags -c -o $dst_dir/citric.o $root_dir/citric.c


gcc $cc_flags -c -o $dst_dir/ct_mjx.o $root_dir/../citric/mjx.c
gcc $cc_flags -c -o $dst_dir/ct_flue.o $root_dir/../citric/flue.c
gcc $cc_flags -c -o $dst_dir/ct_font.o $root_dir/../citric/ct_font.c
gcc $cc_flags -c -o $dst_dir/ct_citric.o $root_dir/../citric/citric.c

#gcc $cc_flags -c -o $dst_dir/iu.o $root_dir/../iu/iu.c
#gcc $cc_flags -c -o $dst_dir/iu_widget.o $root_dir/../iu/iu_widget.c
#gcc $cc_flags -c -o $dst_dir/iu_init.o $root_dir/../iu/iu_init.c
#gcc $cc_flags -c -o $dst_dir/iu_btn.o $root_dir/../iu/iu_text.c
#gcc $cc_flags -c -o $dst_dir/iu_text.o $root_dir/../iu/iu_btn.c
#gcc $cc_flags -c -o $dst_dir/iu_rect.o $root_dir/../iu/iu_rect.c
#gcc $cc_flags -c -o $dst_dir/iu_toolbar.o $root_dir/../iu/iu_toolbar.c
#gcc $cc_flags -c -o $dst_dir/iu_window.o $root_dir/../iu/iu_window.c
#gcc $cc_flags -c -o $dst_dir/iu_menu.o $root_dir/../iu/iu_menu.c
#gcc $cc_flags -c -o $dst_dir/iu_scale.o $root_dir/../iu/iu_scale.c
#gcc $cc_flags -c -o $dst_dir/iu_board.o $root_dir/../iu/iu_board.c
#gcc $cc_flags -c -o $dst_dir/iu_label.o $root_dir/../iu/iu_label.c
#gcc $cc_flags -c -o $dst_dir/iu_signal.o $root_dir/../iu/iu_signal.c
#gcc $cc_flags -c -o $dst_dir/iu_shell.o $root_dir/../iu/iu_shell.c
#gcc $cc_flags -c -o $dst_dir/iu_draw.o $root_dir/../iu/iu_draw.c
#gcc $cc_flags -c -o $dst_dir/iu_tuck.o $root_dir/../iu/iu_tuck.c
#gcc $cc_flags -c -o $dst_dir/iu_tabs.o $root_dir/../iu/iu_tabs.c
#gcc $cc_flags -c -o $dst_dir/iu_renderer.o $root_dir/../iu/iu_renderer.c
#gcc $cc_flags -c -o $dst_dir/iu_composer.o $root_dir/../iu/iu_composer.c

ct="$dst_dir/ct_mjx.o $dst_dir/ct_flue.o $dst_dir/ct_font.o $dst_dir/ct_citric.o $dst_dir/citric.o"
iu="$dst_dir/iu_composer.o $dst_dir/iu_renderer.o $dst_dir/iu_tabs.o $dst_dir/iu_tuck.o $dst_dir/iu_draw.o $dst_dir/iu_shell.o $dst_dir/iu_signal.o $dst_dir/iu_label.o $dst_dir/iu_board.o $dst_dir/iu_scale.o $dst_dir/iu_menu.o $dst_dir/iu_window.o $dst_dir/iu_toolbar.o $dst_dir/iu_widget.o $dst_dir/iu_rect.o $dst_dir/iu_text.o $dst_dir/iu_btn.o $dst_dir/iu.o $dst_dir/iu_init.o"

export ffly_objs="$ct $dst_dir/maths/barycentric.o $dst_dir/hv_pantry.o $dst_dir/hv_disk.o $dst_dir/hv_vehicle.o $dst_dir/hv_player.o $dst_dir/hv_draw.o $dst_dir/maths/plane.o $dst_dir/hv_rsrc.o $dst_dir/hv_physics.o $dst_dir/hv_rdr_antics.o $dst_dir/model.o $dst_dir/vector.o $dst_dir/psp.o $dst_dir/flue.o $dst_dir/shader.o $dst_dir/mesh.o $dst_dir/subs.o $dst_dir/entry.o $dst_dir/fsb.o $dst_dir/init.o $dst_dir/loop.o $dst_dir/common.o $dst_dir/hv_tex.o \
$dst_dir/renderer.o $dst_dir/obj.o $dst_dir/maj_m.o \
$dst_dir/terrain.o $dst_dir/matrix.o $dst_dir/camera.o"
