#include "common.h"
#include "hv_pantry.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../rws.h"
#include"../lib/hash.h"
#include "../lib.h"
/*
	TODO:
	loading should be postphoned until usage
*/
void static _afac_load(struct hv_pantry *__pty,char const *__path){
	struct afac *af;
	af = m_alloc(sizeof(struct afac));
	struct rws_schematic sch;
	sch.path = __path;
	sch.funcs = &rws_file;
	afac_load(af,&sch);

	struct afac_object *obj;
	obj = af->obj;
	while(obj != NULL){
		struct hv_ptyent *ent;
		ent = m_alloc(sizeof(struct hv_ptyent));
		ent->loader = af;
		ent->obj = obj;
		ent->priv = 0;
		ent->meshes[0] = NULL;
		ent->n_meshes = 0;
		f_lhash_put(&__pty->ents,obj->name,str_len(obj->name),ent);
		obj = obj->next;
	}
}

void hv_ptyinit(struct hv_pantry *__pty){
	f_lhash_init(&__pty->ents);
	__pty->bits = 0;
}

void hv_namedptyload(char const *__pty,char const *__path){
	struct hv_tsyent *ent;
	struct hv_pantry *pty;
	ent = hv_tsyget(__pty);
	if(!ent->ptr){
		printf("making new pantry named: %s.\n",__pty);
		pty = m_alloc(sizeof(struct hv_pantry));
		ent->ptr = pty;
		hv_ptyinit(pty);
	}

	pty = ent->ptr;
	hv_ptyload(pty,__path);
}

void hv_ptyload(struct hv_pantry *__pty,char const *__path){
	_int_u pnt;	
	pnt = locofchar_l(__path,'.');

	if(!str_cmp(__path+pnt,".obj")){
		_afac_load(__pty,__path);
		__pty->bits |= HV_PTYAFAC;
	}
}
