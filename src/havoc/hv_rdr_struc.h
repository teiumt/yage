#ifndef __hv__rdr__struc__h
#define __hv__rdr__struc__h
#include "common.h"
#include "model.h"
#include "hv_tex.h"
struct h_render_object {
  void(*op)(struct h_render_object*);
  union {
    //user data
    struct {
      void *tex;
      struct h_mesh *mesh;
      struct h_context *ct;
    };
  };
  struct h_render_object *next;
};

#endif/*__hv__rdr__struc__h*/
