#ifndef __hv__comp__comp__physical__h
#define __hv__comp__comp__physical__h
#include "../vec.h"
/*
  physical props
  common trope
*/
struct h_sst_physical {
  /*
    where is this object located within space
  */
  hvec3 pos;

  /*
    the speed of whitch the object is traversing its axises
  */
  hvec3 vel;
};

#endif/*__hv__comp__comp__physical__h*/
