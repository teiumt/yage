# include "hv_tex.h"
# include "../flue/common.h"
# include "../linux/unistd.h"
# include "../m_alloc.h"
# include "../linux/fcntl.h"
# include "../io.h"
#include "../assert.h"
#include "../string.h"
#include "../lib.h"
struct img {
	_int_u width, height;
	int fd;
};
/*
	redundent?
*/
void static
_file_copy_rect(_ulonglong __arg, void *__dst, _32_u __x, _32_u __y, _32_u __width, _32_u __height) {
	struct img *i;
	i = (struct img*)__arg;
	_8_u *image;
	_int_u totsz;
	image = m_alloc(totsz = (i->width*i->height*4));
	if (pread(i->fd, image, totsz, 0)<0) {
		printf("read error.\n");
	}

	_32_u x, y;
	_32_u xx, yy;
	_8_u *s, *d;
	for(y = 0;y != __height;y++) {
		yy = __y+y;
//		if (yy>=4) break;
		for(x = 0;x != __width;x++) {
			xx = __x+x;
//			if (xx>=4)break;
			s = image+((xx+(yy*i->width))*4);
			d = ((_8_u*)__dst)+((x+(y*__width))*4);
			*(_32_u*)d = *(_32_u*)s;
		}
	}
	m_free(image);
}

void static*
_file_map(_ulonglong __arg) {

}

void static
_file_unmap(_ulonglong __arg) {

}

static struct flue_sips file_io = {
	.copy_rect = _file_copy_rect,
	.map = _file_map,
	.unmap = _file_unmap,
	.bits = 0x00
};

hv_texp tex_new(void) {
	hv_texp t;
	t = m_alloc(sizeof(struct hv_tex));
	t->tx = NULL;
	t->loaded = -1;
	return t;
}

void tex_data(hv_texp __tx, _int_u __width, _int_u __height) {
	struct flue_fmtdesc fmt = {.depth=4*sizeof(float),.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)};
	__tx->tx = flue_tex_new(__width,__height,&fmt,0);
}

/*
	look i know this is not realy the best,
	later im thinking of giving flue full access to such resources.

	this is because of more low level things that are quicker can be done.
*/
void tex_load(hv_texp t) {
	if(t->loaded == -1) {
		t->load(t);
		t->loaded = 0;	
	}
}
#include "../pixels.h"
/*
		function allocates video memory and load it in.	
*/
void static load_from_im(hv_texp t) {
	tex_data(t,t->im->width,t->im->height);
	mu_mat = NULL;
	switch(t->format) {
		case TEX_RGB:
			t->im->rgb_rgba_float(t->im,((flue_texp)t->tx)->tx.ptr);
		break;
		case TEX_RGBA:
			t->im->rgba_rgba_float(t->im,((flue_texp)t->tx)->tx.ptr);
		break;
		defualt:
			checkmate("no support for such pixel component arrangement.\n");
	}
}

#include "../im.h"
/*
	NOTE:
		here we dont preload the file until its needed,
		meaning no data until load function is called.
*/
void static
load_tx_using_im(char const *__file,hv_texp t) {
	struct y_img *im = im_creat();
	t->load = load_from_im;
	im_load(im,__file);
	t->im = im;
}

#define IM_IMG_MASK ((1<<HV_FMT_PPM))
hv_texp _tex_from_file(char const *__file, _8_u __format, _64_u __filefmt) {
	hv_texp t;
	//new texture instance
	t = tex_new();
	if((1<<__filefmt)&IM_IMG_MASK) {
		load_tx_using_im(__file,t);
	}else{
		checkmate("no support for file format in place.\n");
	}
	t->format = __format;
	return t;
}

hv_texp tex_from_file(char const *__file, _8_u __format) {
	printf("LOADING-texture from file{%s}.\n",__file);
	hv_texp t;
	_int_u dx;
	//returns index of character '.'
	dx = locofchar_l(__file,'.');
	if(!str_cmp(__file+dx,".ppm")) {
		t = _tex_from_file(__file,__format,HV_FMT_PPM);
	}else{
		t = NULL;
		checkmate("file format is not supported.\n");
	}
	return t;
}
