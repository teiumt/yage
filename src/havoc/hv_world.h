#ifndef __hv__world__h
#define __hv__world__h
/*
might get removed
*/

struct hv_world{
  /*
    this number will be 1./fps

    where each frame = 1 second of physics world time

    for example if we have an object moving at 1 meter/second.

    and are running 60 frames every second

    then the object moves 1/60 meters per frame
  */
  double phy_div;
  double phy_rate;
};

#endif/*__hv__world__h*/
