#include "renderer.h"
#include "../io.h"
#include "../assert.h"
#include "../m_alloc.h"
#include "subs.h"
#include "../mutex.h"
#include "hv_render.h"
struct h_terrain *landscape = NULL;
/*
	how a render works, going of what i know at the moment.

	everything drawn to the screen is given a render object,
	that object is attached to a list or array. later 
	when all "thing" that are to be drawn to the screen 
	have been registered a folling render_all call would
	be used. the routine goes over all attached render
	object and calles an operations assigned to it, in
	this case for example a draw_mesh.

	in the occasion that we get more then a single mesh draw call
	we can batch them together. as they may share common attributes
	like world space transformation, and other world related data.

	this results in two draw calles to the GPU but no rewriting of
	the already existing or inplace attributes.

	using the command structure like this, will also allow rendering to be done
	on a separate thread

	(NOTE: i have no clue)

	DEFOLD- they seem not to have a command like renderer like godot has,
	for example the rendering tactic used for defold is:

	dmGameObject::UpdateResult CompMeshRender()
	-> dmRender::RenderListSubmit()
	-> dmRender::Draw
	-> dmGraphics::Draw
	-> OpenGL/Vulkan


	GODOT: however uses another thread for rendering,
	by submiting a command like stream.

	struct job {
		int op;
	};

	where op = Item::Command::TYPE_MESH
	- thus drawing a mesh

	main process -> commands in structure form -> renderer process

	COCOS2D-X also uses the same command practice.
	by submitting commands to the renderer and then later thugging though them,
	i however dont know if it is using a diffrent thread for this know.

	for example
	renderer->addCommand(meshCommand);

	RENDERING
	-> void Scene::render(Renderer* renderer, const Mat4& eyeTransform, const Mat4* eyeProjection)
	-> void Renderer::render()
	-> void Renderer::doVisitRenderQueue(const std::vector<RenderCommand*>& renderCommands);
	-> void Renderer::processRenderCommand(RenderCommand* command);
			case RenderCommand::Type::MESH_COMMAND:{
				drawMeshCommand(command);
			}
	-> void Renderer::drawMeshCommand(RenderCommand *command);
	-> void Renderer::drawCustomCommand(RenderCommand *command);
	-> pure opengl or whatever gpu api interface its using
*/

#include "../linux/futex.h"
static void *tgs[8];
void h_terrain(struct h_context *__ct, struct h_terrain *__trn);
void _h_draw(void);
static mlock draw_list_lock = MUTEX_INIT;
static struct rdr_job *draw_list = NULL;
struct rdr_job* hv_rdr_job(void){
	return m_alloc(sizeof(struct rdr_job));
}

void hv_rjlink(struct rdr_job *__r){
	mt_lock(&draw_list_lock);
	__r->next = draw_list;
	draw_list = __r;
	mt_unlock(&draw_list_lock);
}
void render_commence(void) {
	tgs[0] = _h_is.wd->render;
	tgs[1] = _h_is.z;
	_h_is.rd->clear(tgs,1,0.078,0.227,0.29,1);
	_h_is.rd->clear(tgs+1,1,-1,-1,-1,-1);
	struct h_viewport vp = {
		.scale = {1,1,1},
		.xyzw = {0,0,0}
	};
	_h_is.rd->viewport(&vp);

	_h_is.rd->framebuffer(
		_h_is.width,_h_is.height
	);
	_h_is.rd->rendertgs(tgs,1);
	flue_zbuffer(NULL);
//interject
#ifndef __noengine
	_h_draw();
#endif
	
	vp = (struct h_viewport){
		.scale = {_h_is.width,_h_is.height,1},
		.xyzw = {((float)_h_is.width)*0.5,((float)_h_is.height)*0.5,0}
	};
	_h_is.rd->viewport(&vp);

	flue_zbuffer(_h_is.z);

	/*
		move this over so some render object

		object->render() like below this
	*/
	if(landscape != NULL) {
		h_terrain(_h_is.ct,landscape);
	}

	struct rdr_job *s;
	mt_lock(&draw_list_lock);
	
	s = draw_list;
	while(s != NULL){
		s->func(s);
		s = s->next;
	}


	mt_unlock(&draw_list_lock);
}

#include "../thread.h"
static _32_s wait = 0;
static _32_s running = 0;
void static
rdr_handle(void *__arg) {
	while(!running){
		futex(&wait,FUTEX_WAIT,0,NULL,NULL,0);	
		if(running == -1)return;
		if(!wait) {
			/*
				this might just be that futex got interrupted or somthing
			*/
			hv_printf("massive fucking error within renderer\n");
			continue;
		}
		//commence rendering
		render_commence();

		wait = 0;
	}
}

static tstrucp rdr_th;
void h_renderer_prime(void) {
	tcreat(&rdr_th,rdr_handle,NULL);
}

void h_renderer_shutdown(void) {
	running = -1;
	wait = -1;
	futex(&wait,FUTEX_WAKE,1,NULL,NULL,0);
}

void h_renderer_wait(void) {
	while(wait != 0);//not very good but okay for now
}

void h_render_render(void) {
	wait = -1;
	futex(&wait,FUTEX_WAKE,1,NULL,NULL,0);
}
