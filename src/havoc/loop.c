# include "common.h"
# include "renderer.h"
# include "obj.h"
# include "../m.h"
# include "../io.h"
# include "../linux/time.h"
# include "../time.h"
# include "../assert.h"
# include "../clock.h"
# include "window.h"
# include "../m_alloc.h"
#include "../signal.h"
#include "../msg.h"
#include "../domain.h"
#include "resource.h"
#define MSG_BITS MSG_DOMAIN(_DOM_HAVOC)
_8_s _h_tick(void);
void _h_msg(struct h_msg*);
double static smoothing = 1;
_64_u static clkc = 0;
_64_u static cc = 0;
static _8_s running = 0;
void sig_handler(int s){
	running = -1;
}
void rd_shader_dump();


void
ui_message(struct h_composer *b, struct h_msg *m) {
	struct h_rectbox *bx;
	bx = b->bx;
	while(bx != NULL) {
		if (bx->skim == -1) {
		struct h_rbdesc *dsc, *_dsc;
		dsc = bx->dsc;
		hv_printf("%u, %u, %u, %u --- %u\n", m->x, dsc->hb.x, m->y, dsc->hb.y, m->value);
		if (m->x>=dsc->hb.x && m->y>=dsc->hb.y && m->x<dsc->hb.xfar && m->y<dsc->hb.yfar && m->value) {

			// childs
			_int_u i;
			i = 0;
			for(;i != bx->n+1;i++) {
				_dsc = bx->dsc+i;
				hv_printf("MBOX: m%u, %u, m%u, %u --- value: %u, %u, %u, %d, %u\n", m->x, _dsc->hb.x, m->y, _dsc->hb.y, m->value, _dsc->hb.xfar, _dsc->hb.yfar, _dsc->inside, bx->n);
				if (m->x>=_dsc->hb.x && m->y>=_dsc->hb.y && m->x<_dsc->hb.xfar && m->y<_dsc->hb.yfar) {
					if (_dsc->inside == -1) {
						_dsc->inside = 0;
					   	struct h_msg m;
						m.value = H_PTR_ENTER;
						_dsc->update(_dsc->arg, &m);
					}
					_dsc->update(_dsc->arg,m);
				} else {
					if (!_dsc->inside) {
						struct h_msg m;
						m.value = H_PTR_EXIT;
						_dsc->update(_dsc->arg, &m);
						_dsc->inside = -1;
					}
				}
			}

			if (dsc->inside == -1)
				dsc->inside = 0;
		} else {
			if (!dsc->inside) {
				_int_u i;
				i = 0;
				// send exit signal
				for(;i != bx->n+1;i++) {
					_dsc = bx->dsc+i;
					if (!_dsc->inside) {
						struct h_msg m;
						  	m.value = H_PTR_EXIT;
					  	_dsc->update(_dsc->arg, &m);
					}
					_dsc->inside = -1;

				}
				dsc->inside = -1;
			}
		}
		}
		bx = bx->next;
	}
}
void h_ui_message(_ulonglong __arg,struct h_msg *m) {
	ui_message(&_h_is.comp, m);
}

void h_loop(void) {
	_h_is.world.phy_div = (1./60.)*_h_is.world.phy_rate;

	_h_is.counter = 0;
	_h_is.msg = h_ui_message;
	struct sigaction sig;
	sig.sa_handler = sig_handler;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags = 0;
	sigaction(SIGINT,&sig,NULL);
	struct y_timespec start, now;
	_64_s doze_time;
	_64_u tg = _h_is.target;

	doze_time = tg;
_again:
	y_clock_gettime(&start);
	/*
		TODO:
			fix time delay spikes
	*/
//	doze((doze_time&(CLICK_BASE>>3)), doze_time>>27);

	struct h_mhull *mh;
	mh = h_msg_hull();
	_int_u totm;
	totm = mare_maj.messages(mh);	
	mh->cnt = totm;
	struct h_msg *m;
	if (totm>0) {
		_int_u mn = 0;
		for(;mn != totm;mn++) {
		m = mh->inards+mn;
		_h_is.msg(_h_is.msg_arg,m);
		printf("event caught, %u, %u, code: %u\n", m->x, m->y, m->code);
		_64_u mk;
		mk = MESG_KEY(m->value,m->code);
		if(mk<4096){
		if(_h_is.signals[mk] != NULL){
			_h_is.signals[mk](_h_is.sigarg[mk],m);
		}
		}
#ifndef __noengine
		_h_msg(m);
#endif
//		h_msg_free(m);
		}
	}
	m_free(mh->ptr);
	_8_s r;
//	h_rsrc_tidy();
#ifndef __noengine
	r = _h_tick();
	rd_shader_dump();
#endif
//	#define no_render
#ifndef no_render
	h_step();
	if(_h_is.routtop != NULL)
		_h_is.routtop->func(_h_is.routtop->priv);
	h_render_render();
#endif
	/*
		TODO:
			while the frame is being rendered, we can prepare the next frame
	*/
#ifndef no_render
	h_renderer_wait();		
	_h_is.rd->finish();
	mare_maj.swapbufs(_h_is.wd);
#endif
	y_clock_gettime(&now);

	_64_u clks;
	clks = TIME_WHO(now.lower,now.higher)-TIME_WHO(start.lower,start.higher);
	_h_is.cps = ((double)CLICK_BASE)/clks;///(double)(clks*smoothing);
	_h_is.world.phy_div = (1./(double)_h_is.cps)*_h_is.world.phy_rate;

	_64_s ch = tg-(clks-doze_time);
	if (ch>0) {
		doze_time = ch;
	} else {
		doze_time = 0;
	}
/*
	if (cc>20) {
		double x; 	
		x = clks*(1./((double)clkc/cc));
		smoothing = 1;
		cc = 0;
		clkc = 0;
	}
	cc++;
	clkc+=clks;
*/
	_h_is.counter++;
	_h_is.delay = clks;
	dmi();	
	MSG(INFO,"CPS: %f, DELAY: %u.%u-clicks\n", _h_is.cps, _h_is.delay>>27,_h_is.delay&(CLICK_BASE>>3));
	/*
		vekas cant keep up if more then 60fps,
		and we will have read write issues with the window buffer.
		this is because of delay routines in vekas that fix it to a specfic fps
	*/

	if (!running && r == -1) goto _again;

}
