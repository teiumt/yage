#ifndef __hv__disk__h
#define __hv__disk__h
#include "../y_int.h"
#include "../types.h"
struct hv_disk{
	
};

struct hv_disk* hvd_open(_ulonglong);
void hvd_close(struct hv_disk*);

void hvd_write(struct hv_disk*,void*,_64_u);
void hvd_read(struct hv_disk*,void*,_64_u);

#endif/*__hv__disk__h*/
