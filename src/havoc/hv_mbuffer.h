#ifndef __hv__mbuffer__h
#define __hv__mbuffer__h
#include "resource.h"
#include "hv_disk.h"
/*
	a storage place for data.

	storage places could include

	HOST memory,
	from DISK,
	GPU memory
*/
struct hv_mbuffer{
	struct h_rsrc rs;
	/*
		would be valid if from memory or if cpu
		mappings for DISK or GPU are possible
	*/
	_64_u size;
	void *ptr;

	/*
		TODO:
		loading semantics?
		
		what happens if cpu format dosent match the format whats wanted?

		for example what happens in the case a shader intakes a format that this buffer is not in?
		reformatting stage?????
	*/

	void *hw;
};
void hv_mbuffer_data(struct hv_mbuffer*,void*,_64_u);
void hv_mbuffer_load(struct hv_mbuffer*);
void hv_mbuffer_unload(struct hv_mbuffer*);
void hv_mbuffer_init(struct hv_mbuffer*);
#endif/*__hv__mbuffer__h*/
