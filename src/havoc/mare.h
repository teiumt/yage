# ifndef __havoc__mare__h
# define __havoc__mare__h
# include "common.h"
/*
	window -> mare -> [server, vekas, x11(future for linux), etc]
*/
typedef struct h_mare {
	void(*wd_map)(void*);
	void(*wd_unmap)(void*);
	_int_u(*messages)(struct h_msg*);
	void(*make)(void*, void*);
	void(*swapbufs)(void*);
} *h_marep;

extern struct h_mare mare_maj;
// init/de server connection
void h_maj_init(void);
void h_maj_deinit(void);
# endif /*__havoc__mare__h*/
