#include "common.h"
#include "model.h"
#include "../m_alloc.h"
#include "../afac/afac.h"
#include "../limits.h"
#include "../lib.h"
#include "../lib/hash.h"
#include "alloc.h"
#include "../io.h"
#include "vec.h"
/*
void static _compute_premises(h_modelp __m){
	hv_printf("measured model premises to be from {%f, %f, %f} to {%f, %f, %f}.\n",
		near->x,near->y,near->z,
		far->x,far->y,far->z
	);
}
*/

#include "../rws.h"
h_meshp static _afac_load(h_modelp __m,struct hv_pantry *pty,char const *__name){
	struct hv_ptyent *ent;
	ent = f_lhash_get(&pty->ents,__name,str_len(__name));
	if(!ent){
		printf("failed to load %s from pantry.\n",__name);
		return NULL;
		//fuck
	}
	
	struct afac_object *obj;
	struct afac *af;

	af = ent->loader;
	obj = ent->obj;

	/*
		we do this like this to prevent doing it again and putting a if statment in a loop = bad
		
		purpose: setup meshes
	*/
	h_meshp m;
	struct afac_group *g;
	if(!(ent->priv&1)){	
		m = hv_alloc_struc(HV_MESH_STRUC);
		hv_mesh_init(m);

		_int_u i = 0;
		g = obj->groups;
		while(g != NULL){
			struct hv_premises prem;
			/*
				NOTE: near = closest to (-)
				and far = closent to (+)
			*/
			prem.near.x = g->prem.near.x; 
			prem.near.y = g->prem.near.y;
			prem.near.z = g->prem.near.z;

			prem.far.x = g->prem.far.x;
			prem.far.y = g->prem.far.y;
			prem.far.z = g->prem.far.z;

			h_vsstmin(&m->prem.near,&prem.near);
			h_vsstmax(&m->prem.far,&prem.far);
			m->af.groups[i++] = g;
			g = g->next;
		}
		m->af.groups[i] = NULL;
		m->af.ins = af;
		ent->meshes[0] = m;
		ent->priv = 1;
	}

	m = ent->meshes[0];
	if(!m){
		printf("not fucking working.\n");
		return NULL;
	}
	h_vsstmin(&__m->prem.near,&m->prem.near);
	h_vsstmax(&__m->prem.far,&m->prem.far);
/*
	hv_printf("measured model premises to be from {%f, %f, %f} to {%f, %f, %f}.\n",
		__m->prem.near.x,__m->prem.near.y,__m->prem.near.z,
		__m->prem.far.x,__m->prem.far.y,__m->prem.far.z);
*/
	return m;
	/*
		TODO:
			check AFAC and determan if a premises already exists else bruteforce/manualy calculate it.
	*/
}


void h_model_config(h_modelp __m,struct hv_mdlpar *__par){
	struct hv_pantry *pty;
	if((__par->bits&HV_MDLFILE)>0){
		printf("model_config) %s.\n",__par->path);
		struct hv_tsyent *ent;
		ent = hv_tsyget(__par->path);
		if(!ent->ptr){	
			/*
				here we load the file with its own name as its name
			*/
			hv_namedptyload(__par->path,__par->path);
		}
		pty = ent->ptr;
	}else{
		printf("warning: model parm dosent make sence.\n");
		return;
	}

	if(pty->bits == HV_PTYAFAC){
		_int_u i;
		i = 0;
		char const *cur;
		while((cur = __par->chosen[i]) != NULL){
			__m->obj[__par->layout[i]].mesh = _afac_load(__m,pty,cur);
			i++;
		}
		__m->n_obj = i;
	}
}
void h_model_init(h_modelp m){
	m->n_obj = 0;
	m->prem = (struct hv_premises){
		.near ={//near
			DBL_MAX,
			DBL_MAX,
			DBL_MAX
		},
		.far = {//far
			-DBL_MAX,
			-DBL_MAX,
			-DBL_MAX
		}			
	};
}
