#include "resource.h"
#include "common.h"
#include "../real_tree.h"
static struct h_rsrc *top = NULL;
static struct rtnode *stump;
void hv_rsrc_sysinit(void){
	stump = rtree_new();
}

void h_rsrc_link(struct h_rsrc *__r) {
	__r->next = top;
	top = __r;
}
void h_rsrc_stimulate(struct h_rsrc *__r) {
	__r->stamp = _h_is.counter;
}
void h_rsrc_init(struct h_rsrc *__r) {
	__r->refcnt = 0;
	__r->stamp = 0;
	__r->info = 0;
}
void hv_rsrc_load(struct h_rsrc *__r){
	if(!(__r->info&RSRC_LINKED)){
		h_rsrc_link(__r);
		__r->load(__r);
		__r->info |= RSRC_LINKED;
	}
	/*
		how many instances has this been used thought out the render.
	*/
	__r->refcnt++;
	//stimulate
}

/*
	NOTE:
	 when dealing with resources do dont so much care about unloading,
	 are main focus is loading.

	if by the end of the frame its no longer used then we remove it.
*/
void h_rsrc_tidy(void) {
	struct h_rsrc *r;
	r = top;
	struct h_rsrc **bk = &top;
	while(r != NULL){	
		/*
			if the refcount reaches zero then a count down will start and the resource will then be evicted.
		*/
		if(!r->refcnt){
			if(r->stamp>8){
				r->unload(r);
				*bk = r->next;
			}
			r->stamp++;
		}else{
			r->stamp = 0;
		}
		//needs to be reset
		r->refcnt = 0;
		bk = &r->next;
		r = r->next;
	}
}
