# include "workshop.h"
# include "../font.h"
# include "../../clay.h"
# include "../../io.h"
# include "../common.h"
#include "../../y.h"
_err_t main(int __argc, char const *__argv[]) {
	struct clay clay;
	clay_init(&clay);
	clay_load(&clay, "settings.clay");
	clay_read(&clay);

	void *screen;
	screen = clay_get("screen", &clay);
	int width, height;
	width = _clay_16(clay_tget("width", screen));
	height = _clay_16(clay_tget("height", screen));
	printf("screen, width: %u, height: %u\n", width, height);
	clay_de_init(&clay);
	_h_is.width = width;
	_h_is.height = height;
	
	havoc_start();
}
struct workshop _ws;
static _8_s _rtexit = -1;
/*
	get all events and check
*/
void _h_msg(struct h_msg *__m) {
	if (__m->value == H_PRESS) {
		switch(__m->code) {
			case H_KEY_ESC:
				_rtexit = 0;
			break;
		}
	}
}
void static _enter(void) {

}

void static _exit(void) {
}


void static
_tick(void) {

}

static struct ws_scene dummy = {
	.enter = _enter,
	.exit = _exit,
	.tick = _tick
};

struct ws_scene *curscene = &dummy;
static struct ui_btn main_menu_bt;
static struct ui_btn settings_bt;
static struct ui_btn import_bt;
static struct ui_btn exit_bt;
static struct ui_dropdown dropdown;
void static exit_button(_ulonglong __arg, struct h_msg *m) {
	if (m->code == UI_BTN_PRESS) {
		_rtexit = 0;
	}
}

extern struct h_context hct;
static _int_u transmap_btn_to_dropdown[64];
static _int_u transmap_btn_to_exit[64];
_int_u transmap_btn_to_window[64];
_int_u transmap_btn_to_window0[64];
#define D(__x) ((1./255.)*__x)
HV_INIT {
	_ws._16_16 = tex_from_file(PITHDIR "icons.ppm",TEX_RGB);
	_ws.fnt = font_load();
	ws_settings_init();
	ui_dropdown_new(&dropdown);
	dropdown.parent = &main_menu_bt;
	transmap_btn_to_dropdown[UI_BTN_PRESS] = UI_DROPDOWN_EXTEND|UI_DROPDOWN_RETRACT;
	transmap_btn_to_exit[UI_BTN_PRESS] = UI_BTN_PRESS;
	transmap_btn_to_window[UI_BTN_PRESS] = UI_WINDOW_HIDE;
	transmap_btn_to_window0[UI_BTN_PRESS] = UI_WINDOW_SHOW;
	_h_is._ctx = &hct;
	fontp fnt = _ws.fnt;
#define TEXT "file"
	struct ui_btn_strc btn_config = {
		.x = 0,
		.y = 0,
		.w = 0.08,
		.h = 0.017,
		.tw = 0.8,
		.th = 0.8,
		.text = TEXT,
		.len = sizeof(TEXT)-1,
		.bg = {D(255),D(255),D(255),1},
		.tx = {
			.colour = {D(0),D(0),D(0),1}
		}
	};
	main_menu_bt.tx.linelen = btn_config.len;
	btn_config.w = 0.025;
	ui_btn_new(&main_menu_bt);
	ui_btn_new(&settings_bt);
	ui_btn_new(&import_bt);
	ui_btn_new(&exit_bt);
	main_menu_bt.tx.bits = FNT_HALIGN|FNT_VALIGN;
	settings_bt.tx.bits = FNT_VALIGN;
	import_bt.tx.bits = FNT_VALIGN;
	exit_bt.tx.bits = FNT_VALIGN;
	settings_bt.tx.xoff = 0.01;
	import_bt.tx.xoff = 0.01;
	exit_bt.tx.xoff = 0.01;
	

	ui_btn_load(&main_menu_bt,&btn_config,fnt);

	btn_config.h = 0.017;
	btn_config.w = 0.08;	
	btn_config.tw = 0.5;
	btn_config.text = "settings";
	btn_config.len = sizeof("settings")-1;
	settings_bt.tx.linelen = 16;
	ui_btn_load(&settings_bt,&btn_config,fnt);
	
	btn_config.text = "import";
	btn_config.len = sizeof("import")-1;
	import_bt.tx.linelen = 16;
	ui_btn_load(&import_bt,&btn_config,fnt);
	
	btn_config.text = "exit";
	btn_config.len = sizeof("exit")-1;
	exit_bt.tx.linelen = 16;
	ui_btn_load(&exit_bt,&btn_config,fnt);
	
	h_wg_link(&_h_is.comp,&main_menu_bt);
	h_wg_link(&_h_is.comp,&settings_bt);
	h_wg_link(&_h_is.comp,&import_bt);
	h_wg_link(&_h_is.comp,&exit_bt);

	ui_btn_move(&main_menu_bt,0.0,0.0);
	ui_btn_move(&settings_bt,0.0,0.0);
	ui_btn_move(&import_bt,0.0,0.0);
	ui_btn_move(&exit_bt,0.0,0.0);

	settings_bt.w.output = _ws.settings_w.w.input;
	settings_bt.w.out = &_ws.settings_w;
	settings_bt.w.codetab = transmap_btn_to_window0;

	ui_dropdown_add(&dropdown,&settings_bt);
    ui_dropdown_add(&dropdown,&import_bt);
	ui_dropdown_add(&dropdown,&exit_bt);
    ui_dropdown_move(&dropdown,0,0);

	exit_bt.w.output = exit_button;
	exit_bt.w.codetab = transmap_btn_to_exit;
	main_menu_bt.w.output = dropdown.w.input;
	main_menu_bt.w.out = &dropdown;
	main_menu.enter();
	main_menu_bt.w.codetab = transmap_btn_to_dropdown;
}

HV_DEINIT {
	main_menu.exit();
}

HV_TICK {	
	void *tgs[] = {_h_is.wd->render};
	h_clear(tgs,1);
	h_rtgs(tgs,1);
	
	struct h_viewport vp = {
		.xyzw = {0,0,0},
		.scale = {_h_is.comp.ww,_h_is.comp.wh,1}
	};
	_h_is.rd->viewport(&vp);

	_h_is.rd->framebuffer();

	flue_zbuffer(NULL);

	ui_btn_draw(&main_menu_bt);
	ui_dropdown_draw(&dropdown);
	ws_settings_tick();
	main_menu.tick();
	return _rtexit;
}
