# ifndef __havoc__workshop__h
# define __havoc__workshop__h
#include "../font.h"
#include "../ui/rect.h"
#include "../ui/text.h"
#include "../ui/btn.h"
#include "../ui/dropdown.h"
#include "../ui/window.h"
#include "../ui/check_box.h"
#include "../icon.h"
#include "../resource.h"
struct ws_scene {
	void(*enter)(void);
	void(*exit)(void);
	void(*tick)(void);
};
struct workshop {	
	fontp fnt;
	texp _16_16;
	struct ui_window settings_w;
};
void
ws_settings_init(void);
void
ws_settings_tick(void);
extern struct ws_scene main_menu;
extern struct workshop _ws;
# endif /*__havoc__workshop__h*/
