# include "workshop.h"
# include "../../m_alloc.h"
static _flu_float cube[] = {
	//face0 - front
	-.5, -.5, .5,
	.5, -.5, .5,
	-.5, .5, .5,
	.5, .5, .5,
	0, 0, 1,
	//face1 - back
	-.5, -.5, -.5,
	.5, -.5, -.5,
	-.5, .5, -.5,
	.5, .5, -.5,
	0, 0, -1,
	//face2 - top
	-.5, -.5, .5,
	.5, -.5, .5,
	-.5, -.5, -.5,
	.5, -.5, -.5,
	0, -1, 0,
	//face3 - bottom
	-.5, .5, .5,
	.5, .5, .5,
	-.5, .5, -.5,
	.5, .5, -.5,
	0, 1, 0,
	//face4 - left
	-.5, -.5, .5,
	-.5, -.5, -.5,
	-.5, .5, .5,
	-.5, .5, -.5,
	-1, 0, 0,
	//face5 - right
	.5, -.5, -.5,
	.5, -.5, .5,
	.5, .5, -.5,
	.5, .5, .5,
	1, 0, 0,
    //tex0
    0, 0, .25, 0, 0, .25, .25, .25,
    //tex1
    .25, 0, .5, 0, .25, .25, .5, .25,
    //tex2
    .5, 0, .75, 0, .5, .25, .75, .25,
    //tex3
    .75, 0, 1, 0, .75, .25, 1, .25,
    //tex4
    0, .25, .25, .25, 0, .5, .25, .5,
    //tex5
    .25, .25, .5, .25, .25, .5, .5, .5
};

struct ws_mesh* ws_cube_mesh(void) {
	struct ws_mesh *m;
	m = m_alloc(sizeof(struct ws_mesh));
	m->buf = m_alloc((6*2)*(sizeof(_flu_float)*18));

	_flu_float *p;
	p = m->buf;
	_flu_float *vtx, *uv;
	_int_u i;
	i = 0;
	for(;i != 6;i++) {
		vtx = cube+(i*(5*3));
		uv = cube+(i*8)+(15*6);

		p[0] = vtx[0];
		p[1] = vtx[1];
		p[2] = vtx[2];
		
		p[3] = vtx[3];
		p[4] = vtx[4];
		p[5] = vtx[5];
		
		p[6] = vtx[6];
		p[7] = vtx[7];
		p[8] = vtx[8];

		p[9] = vtx[9];
		p[10] = vtx[10];
		p[11] = vtx[11];

		p[12] = uv[0];
		p[13] = uv[1];
		p[14] = uv[2];
		p[15] = uv[3];
		p[16] = uv[4];
		p[17] = uv[5];

		p[18] = vtx[3];
		p[19] = vtx[4];
		p[20] = vtx[5];
		
		p[21] = vtx[6];
		p[22] = vtx[7];
		p[23] = vtx[8];
		
		p[24] = vtx[9];
		p[25] = vtx[10];
		p[26] = vtx[11];

		p[27] = vtx[9];
		p[28] = vtx[10];
		p[29] = vtx[11];

		p[30] = uv[2];
		p[31] = uv[3];
		p[32] = uv[4];
		p[33] = uv[5];
		p[34] = uv[6];
		p[35] = uv[7];
		p+=36;
	}

	return m;
}
/*
void ws_mesh_draw(struct ws_mesh *__m) {

}*/
