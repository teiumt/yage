# ifndef __engine__common
# define __engine__common
#include "hv_world.h"
#define HV_NO_GRAPHICS 1

/*
	DESIGN:

	models and meshes.

	i lack experance when dealing with these things,

	however the just idea is


	model -> meshlist

	however this dosent make too much sence to me why?

	because 

	model -> meshlist(only one mesh)


	this this case a model is just a single mesh

	i understand that textures, and shaders are on the model side of things.

	however it would be better just to merge both into a single structure.

	like

	manifestation(front, includes textures and shit) -> manifestation(child of front, includes mesh)

	however that method is flawed, the issue being is that top level may include working directives.

	
	building -> building blocks


	the building may have specific properties but the building block dont include such data to incompase the whole building.
	this conseption is kindof skewed as you could have a building within a building.
	where each building has its own propities. however such things are never going to exist as
	you dont need two models bound to each other as they can remain independntfrom one another.

	an example of this could be a tours and in the mits of that tours exists another object.

	the issue is i realy dont like this.
	___________________________________

	model -> onemesh = just a mesh

	however this can be justified as the model would contain strict textures that are for the mesh its working with.

	for example minecraft.

 	every single block in the game shares the same mesh, the only thing diffrent is the textures.

	SO it would be 

	 _>block dirt
  |-> dirt texture
	|-> mesh(Y)

	 _>block grass
  |-> grass texture
	|-> mesh(Y)


	both blocks share the same mesh, mesh(Y).


	models may contain bones,textures,materials,meshes


	GPU/HOST memory transfer

	everything cant fit in GPU memory, and if we did go that route then memory will be wasted on things that we dont need anymore.

	Textures:
	textures would have to undergo reformatting in a diffrent way then vertex or other resources.

	for exmaple if we where to upload the texture data to the GPU colour formats may have to be adjusted or 
	preprocessing may be needed?

	however this dosent realy matter as the resources can be in a ready state with only copying required.

	but im unsure as to how approching it would be done.
	other engines would have seperate loading semantics for meshes and models.
	because of OpenGLs glBufferData and glTexImage2D.

	my assumption is that it would work like this.



	beginFrame

# traverse all render commands and mark resources as 'TO_BE_USED'
	purgeRedundant
# if resource wasent given the thumbs up in the form of a 'TO_BE_USED' flags then purge it from GPU memory

# this would cause any non GPU memory to then be uploaded before draws
# and 'TO_BE_USED' will be removed
	drawEverything

	endFrame

# LOOP BACK

	HOST memory should be expressed as it were on GPU memory
*/
#include "../y_int.h"
#include "../mutex.h"
#include "../flue/common.h"
#include "hv_tex.h"
#include "../ffly_def.h"
#include "psp.h"
#include "../log.h"
#include "vec.h"
#include "../lib/hash.h"
#define hv_printf(...) log_printf(&_h_is.log,__VA_ARGS__)
#define HV_TICK _8_s _h_tick(void)
#define HV_INIT _8_s _h_init(void)
#define HV_DEINIT _8_s _h_deinit(void)

#define MSG_CODENULL 255
//treasury
struct hv_tsyent* hv_tsyget(char const*);
void hv_tsyput(char const*,void*);

typedef double h_float;
struct h_wd;
void h_rect_geom(float *__dst, float *__vtx);
void gc_init(void);
void gc_deinit(void);
void *gc_new(void);
void gc_make(void*);
void gc_update(void*);
struct h_routine {
	void(*func)(void*);
	void *priv;
	struct h_routine *next;
};

#define MESG_KEY(__value,__code)\
	((__value)|(__code<<4))
struct h_viewport {
	float xyzw[4];
	float scale[4];
};

/*
	hitbox(for UI)
*/
struct h_hbox {
	_int_u x, y;
	_int_u xfar, yfar;
};
/*

	NOTE: 
	in a rectbox the fist desc is the parent of the others below
	n is total-1
*/
struct h_rbdesc {
	struct h_hbox hb;
	_ulonglong arg;
	_8_s inside;
	void(*update)(_ulonglong, struct h_msg*);
};
struct h_rectbox {
	struct h_rbdesc dsc[8];
	_int_u n;
	_8_s skim;
	/*
		n-1
	*/
	struct h_rectbox *next;
};

#include "keysym.h"
#define h_clear	_h_is.rd->clear
#define h_rtgs	_h_is.rd->rendertgs
/*
	pointer has exited or entered an area ie rectbox/desc
*/
#define H_PTR_ENTER		3
#define H_PTR_EXIT		4
struct h_msg {
	_int_u id;
	_int_u code;
	_int_u value;
	_64_s x, y;
};

/*
	example
	A64
	alloc 64*64 block
*/
#define FSB_A1  0
#define FSB_A2  1
#define FSB_A4  2
#define FSB_A8  3
#define FSB_A16 4
#define FSB_A32 5
#define FSB_A64 6
struct fsb_area {
	_int_u x, y;
	_int_u size;
	struct fsb_area *child;
};

struct fsb_alloc_8_8 {
	_64_u at;
	_64_u free[256];
};
#define FSB_X(__n)((((__n)>>6)&15)<<3)
#define FSB_Y(__n)(((__n)>>10)<<3)
#define UI_BTN_RELEASE  0
#define UI_BTN_PRESS	1
// message bucket message
struct h_mbm {
	_int_u code;
	// the pointer to ???
	_ulonglong value;
	struct h_mbm *next;
};

struct h_msgbucket {
	struct h_mbm *m;
};

struct h_render_device;
struct h_buffer {
	
};
struct texmap {
	struct hv_tex *tex;
	void *ptr;
	_int_u width,height;	
};
struct h_context {
	_flu_float trans[16];
	_flu_float proj[16];
	_flu_float model[16];
};
struct h_mhull {
    struct h_msg inards[16];
    _32_u cnt;
	void *ptr;
};
//its composer?
struct h_composer {
	struct h_rectbox *bx;
	_flue_float vpw;
	_flue_float vph;
	_flue_float ww,wh;
	_flue_float asp;
};

struct hv_tsyent{
	void *ptr;
};

// havoc internal structure
struct havoc_is {
	struct h_composer comp;
	mlock lock;

	struct h_subsis *subsis;
	void *ctx;
	struct h_wd *wd;
	void *mb;
	_int_u mb_width, mb_height;
	_8_u *tx_map;

	struct fsb_alloc_8_8 maps_alloc[1];
	struct texmap  maps_map[1];

	/*
		an 8x8 map
		for storing images that are 8x8 in size
		for example TEXT
	*/

	double cps;
	double target;
	_64_u delay;

	_int_u width, height;
	
	void *sf;
	void *z;
	struct h_render_device *rd;
	struct h_buffer *consts;
	void *consts_ptr;
	void *perc_const;
	float tgt_fps;
	struct h_context *ct;
	struct h_context *_ctx;
	struct log_struc log;
	void(*msg)(_ulonglong __arg, struct h_msg *m);
	_ulonglong msg_arg;

	void(*signals[4096])(_ulonglong,struct h_msg*);
	_ulonglong sigarg[4096];
	_64_u counter;
	struct h_routine *routtop;

	/*
		if the texture is missing we use this texture as a replacement
	*/
	void *missing;
	struct f_lhash treasury;
	_64_u bits;

	struct hv_world world;
};
void h_ui_message(_ulonglong __arg,struct h_msg *m);
enum {
	H_SHADER_PIXEL,
	H_SHADER_VETX
};

enum {
	H_PRIM_RECT,
	H_PRIM_TRI
};
/*
	setup{framebuffer,viewport}

	start();

	draw stuff
	finish

*/
_64_u H_8_8_ALLOC(struct fsb_alloc_8_8 *__a);
#define H_SHADER_VERTEXBUF 0
#define H_SHADER_POOL 1
#define H_SHADER_CONST 2
#define H_SHADER_PRIMBUF 3
#define H_SHADER_SAMPLE 4
enum {
	H_RECT_CONSTS,
	H_RECT_POSITION,
	H_RECT_COLOUR,
	H_RECT_NORMAL,
	H_RECT_TEXTURE,
	H_RECT_UV,
	H_RECT_SAMPLER
};
enum {
	H_UI_POSITION,
	H_UI_TEXTURE
};
enum {
	H_TRI3,
	H_QUAD
};
struct h_render_device {
/*
	other engines and or things that use opengl or directx
	simply use compiler runtime checking.
*/
	void(*draw_array)(void*,void*,void*,_int_u,struct hv_tex*,void*,void*,void*);
	void(*finish)(void);
	void(*framebuffer)(_int_u,_int_u);
	void(*viewport)(struct h_viewport*);
	void(*shader_compile)(void*,char const*,_int_u);
	void*(*shader_create)(_64_u);
	void(*shader)(void*,_int_u);
	void*(*buffer_create)(_int_u);
	void*(*buffer_map)(void*);
	void(*buffer_unmap)(void*);
	void(*buffer_destory)(void*);
	void(*start)(void);
	void(*rendertgs)(void**,_int_u);
	void(*vertex_resource)(void**,_int_u,_64_u);
	void(*pool)(void**,_int_u,_64_u);
	void(*shader_enter)(void);
	void(*shader_link)(void*,char const*,_64_u,_64_u,_64_u);
	void(*init)(void);
	void(*rect)(float*,void*,void*,void*);
	void(*shader_make)(void*,void*);
	void(*clear)(void**,_int_u,float,float,float,float);
	void(*ui_rect)(float*,_int_u,void*,void*,void*,void*);
	void(*rect_withbuf)(void*,_int_u,void*,void*,_64_u*);
	void(*quad)(void*,_int_u,void*,void*,void*);
	//packdata
	void*(*data)(float*,_int_u,_int_u);	
};
extern struct h_render_device h_rd_flue;
extern struct havoc_is _h_is;
#define h_rddv(__what) _h_is.rd->__what

#define h_buffer_create		h_rddv(buffer_create)
#define	h_buffer_map			h_rddv(buffer_map)
#define	h_buffer_unmap		h_rddv(buffer_unmap)
#define h_buffer_destory	h_rddv(buffer_destory)
#define h_buffer_destroy	h_buffer_destory

#define h_quad h_rddv(quad)
#define h_data h_rddv(data)

#include "../flue/tex.h"
#include "../flue/types.h"
#include "hv_tex.h"
void hv_sigconnect(_64_u,void(*)(_ulonglong,struct h_msg*),_ulonglong);
struct fsb_area *fsb_alloc(_int_u);
void h_transpose(_flu_float *__m);
void h_matcopY(_flu_float*, _flu_float*);
void h_matrix_mul4(_flu_float*, _flu_float*, _flu_float*);
void h_perspective(_flu_float*, _flu_float, _flu_float);
void h_translate(_flu_float*, _flu_float, _flu_float, _flu_float);
void h_lookat(_flu_float*, _flu_float, _flu_float, _flu_float,
    _flu_float, _flu_float, _flu_float,
    _flu_float, _flu_float, _flu_float);
void h_frustum(_flu_float*, _flu_float, _flu_float, _flu_float, _flu_float,_flu_float, _flu_float);
void h_orthographic(_flu_float*, _flu_float, _flu_float, _flu_float, _flu_float);
void h_oblique(_flu_float*, _flu_float, _flu_float);
void h_matrix_inv16(_flu_float*,_flu_float*);
void h_mxrot(_flu_float*, _flu_float);
void h_myrot(_flu_float*, _flu_float);
void h_mzrot(_flu_float*, _flu_float);
void h_supplement(_flu_float *__m, _flu_float __x, _flu_float __y, _flu_float __z);
void h_mtilttowards(_flu_float *__m, hvec3p __v);
void h_tranform_vector(hvec3p __v, _flu_float *__m);
struct h_rectbox *h_rbx_new(struct h_composer*);
void h_rbx_destroy(struct h_rectbox*);
struct h_msgbucket* h_mbkt_new(void);
void h_mbkt_destroy(struct h_msgbucket*);
struct h_msg* h_msg_hull(void);
void h_msg_free(struct h_msg *__m);
void h_mscale(_flu_float *__m, float __factor);
void setmatrix(_flue_float*);
void havoc_start(void);
#include "window.h"
extern _flue_float h_m_ident[16];
#include "maths.h"
# endif /*__engine__common*/
