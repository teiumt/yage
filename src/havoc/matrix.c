# include "common.h"
# include "../maths.h"
# include "../flue/types.h"
/*
	yes this if messed up
*/
/*
		copy 16-bytes
*/

#define copy128(__dst, __src)\
	__asm__("movdqu (%1), %%xmm0\n\t"\
			"movdqu %%xmm0, (%0)\n\t"\
		: : "r"(__dst), "r"(__src) : "xmm0");

_flue_float h_m_ident[16] = {
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, -1/*this needs removing from ident(only used for projection matrix)*/, 1
};


void
h_matcopY(_flu_float *__dst, _flu_float *__src) {
		copy128(__dst, __src);
		copy128(__dst+4, __src+4);
		copy128(__dst+8, __src+8);
		copy128(__dst+12, __src+12);
}

#define M0(__row, __col) m0[__row+__col]
#define M1(__row, __col) m1[__row+__col]
#define M2(__row, __col) m2[__row+__col]
/*
	good way to think about it is that W0 and W2 are transforms of all the X components (E0 and E2) of the first matrix


	[ E0, E1 ]   [ W0, W1 ]   [ E0.W0+E1.W2, E0.W1+E1.W3 ]
	[ E2, E3 ] . [ W2, W3 ] = [ E2.W0+E3.W2, E2.W1+E3.W3 ]

	ROW.COL
*/
void h_matrix_mul4(_flue_float *m0, _flue_float *m1, _flue_float *m2) {
	_int_u i;
	i = 0;
	for(;i != 16;i+=4) {
		_flu_float a, b, c, d;
		a = M1(i,0);
		b = M1(i,1);
		c = M1(i,2);
		d = M1(i,3);
		M0(i,0) = a*M2(0,0)+b*M2(4,0)+c*M2(8,0)+d*M2(12,0);
		M0(i,1) = a*M2(0,1)+b*M2(4,1)+c*M2(8,1)+d*M2(12,1);
		M0(i,2) = a*M2(0,2)+b*M2(4,2)+c*M2(8,2)+d*M2(12,2);
		M0(i,3) = a*M2(0,3)+b*M2(4,3)+c*M2(8,3)+d*M2(12,3);
	}
}

void h_matrix_inv16(_flu_float *__d, _flu_float *__m) {
	_flu_float *m = __m;
	_flu_float iv[16];

	iv[0]	= m[5]*m[10]*m[15]-m[5]*m[11]*m[14]-m[9]*m[6]*m[15]+m[9]*m[7]*m[14]+m[13]*m[6]*m[11]-m[13]*m[7]*m[10];
	iv[4]	= -m[4]*m[10]*m[15]+m[4]*m[11]*m[14]+m[8]*m[6]*m[15]-m[8]*m[7]*m[14]-m[12]*m[6]*m[11]+m[12]*m[7]*m[10];
	iv[8]	= m[4]*m[9]*m[15]-m[4]*m[11]*m[13]-m[8]*m[5]*m[15]+m[8]*m[7]*m[13]+m[12]*m[5]*m[11]-m[12]*m[7]*m[9];
	iv[12]	= -m[4]*m[9]*m[14]+m[4]*m[10]*m[13]+m[8]*m[5]*m[14]-m[8]*m[6]*m[13]-m[12]*m[5]*m[10]+m[12]*m[6]*m[9];
	iv[1]	= -m[1]*m[10]*m[15]+m[1]*m[11]*m[14]+m[9]*m[2]*m[15]-m[9]*m[3]*m[14]-m[13]*m[2]*m[11]+m[13]*m[3]*m[10];
	iv[5]	= m[0]*m[10]*m[15]-m[0]*m[11]*m[14]-m[8]*m[2]*m[15]+m[8]*m[3]*m[14]+m[12]*m[2]*m[11]-m[12]*m[3]*m[10];
	iv[9]	= -m[0]*m[9]*m[15]+m[0]*m[11]*m[13]+m[8]*m[1]*m[15]-m[8]*m[3]*m[13]-m[12]*m[1]*m[11]+m[12]*m[3]*m[9];
	iv[13]	= m[0]*m[9]*m[14]-m[0]*m[10]*m[13]-m[8]*m[1]*m[14]+m[8]*m[2]*m[13]+m[12]*m[1]*m[10]-m[12]*m[2]*m[9];
	iv[2]	= m[1]*m[6]*m[15]-m[1]*m[7]*m[14]-m[5]*m[2]*m[15]+m[5]*m[3]*m[14]+m[13]*m[2]*m[7]-m[13]*m[3]*m[6];
	iv[6]	= -m[0]*m[6]*m[15]+m[0]*m[7]*m[14]+m[4]*m[2]*m[15]-m[4]*m[3]*m[14]-m[12]*m[2]*m[7]+m[12]*m[3]*m[6];
	iv[10]	= m[0]*m[5]*m[15]-m[0]*m[7]*m[13]-m[4]*m[1]*m[15]+m[4]*m[3]*m[13]+m[12]*m[1]*m[7]-m[12]*m[3]*m[5];
	iv[14]	= -m[0]*m[5]*m[14]+m[0]*m[6]*m[13]+m[4]*m[1]*m[14]-m[4]*m[2]*m[13]-m[12]*m[1]*m[6]+m[12]*m[2]*m[5];
	iv[3]	= -m[1]*m[6]*m[11]+ m[1]*m[7]*m[10]+m[5]*m[2]*m[11]-m[5]*m[3]*m[10]-m[9]*m[2]*m[7]+m[9]*m[3]*m[6];
	iv[7]	= m[0]*m[6]*m[11]-m[0]*m[7]*m[10]-m[4]*m[2]*m[11]+m[4]*m[3]*m[10]+m[8]*m[2]*m[7]-m[8]*m[3]*m[6];
	iv[11]	= -m[0]*m[5]*m[11]+m[0]*m[7]*m[9]+m[4]*m[1]*m[11]-m[4]*m[3]*m[9]-m[8]*m[1]*m[7]+m[8]*m[3]*m[5];
	iv[15]	= m[0]*m[5]*m[10]-m[0]*m[6]*m[9]-m[4]*m[1]*m[10]+m[4]*m[2]*m[9]+m[8]*m[1]*m[6]-m[8]*m[2]*m[5];
	_flu_float det;
	det = (m[0]*iv[0]+m[1]*iv[4]+m[2]*iv[8]+m[3]*iv[12]);
	if (!det) {
		det = 1;//onlydebug
	}else{
		det = 1./det;
	}
	__d[0]	= iv[0]*det;
	__d[1]	= iv[1]*det;
	__d[2]	= iv[2]*det;
	__d[3]	= iv[3]*det;
	__d[4]	= iv[4]*det;
	__d[5]	= iv[5]*det;
	__d[6]	= iv[6]*det;
	__d[7]	= iv[7]*det;
	__d[8]	= iv[8]*det;
	__d[9]	= iv[9]*det;
	__d[10] = iv[10]*det;
	__d[11] = iv[11]*det;
	__d[12] = iv[12]*det;
	__d[13] = iv[13]*det;
	__d[14] = iv[14]*det;
	__d[15]	= iv[15]*det;
}


void h_transpose(_flu_float *__m) {
	_flu_float m[16];
	h_matcopY(m,__m);
	__m[0]	= m[0];
	__m[4]	= m[1];
	__m[8]	= m[2];
	__m[12]	= m[3];
	
	__m[1]	= m[4];
	__m[5]	= m[5];
	__m[9]	= m[6];
	__m[13]	= m[7];

	__m[2]	= m[8];
	__m[6]	= m[9];
	__m[10]	= m[10];
	__m[14]	= m[11];

	__m[3]	= m[12];
	__m[7]	= m[13];
	__m[11]	= m[14];
	__m[15]	= m[15];
}

void h_mtilttowards(_flu_float *__m, hvec3p __v) {
	_flu_float xr[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	_flu_float yr[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	_flu_float dv[] = {__v->x,__v->y,__v->z};
	float o = sqr(dv[0]*dv[0]+dv[2]*dv[2]);
	float h = sqr(o*o+dv[1]*dv[1]);

	xr[5]		= dv[1]/h;
	xr[6]		= o/h;
	xr[9]		= -(o/h);
	xr[10]	= dv[1]/h;
	h_matrix_mul4(__m, __m, xr);
}

void h_myrot(_flu_float *__m, _flu_float __rad) {
	_flu_float m[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	m[0] = cos(__rad);
	m[10] = cos(__rad);
	m[2] = sin(__rad);
	m[8] = -sin(__rad);
	h_matrix_mul4(__m, __m, m);
}

void h_mxrot(_flu_float *__m, _flu_float __rad) {
	_flu_float m[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	m[5] = cos(__rad);
	m[6] = sin(__rad);
	m[9] = -sin(__rad);
	m[10] = cos(__rad);
	h_matrix_mul4(__m, __m, m);
}


void h_mzrot(_flu_float *__m, _flu_float __rad) {
	_flu_float m[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	m[0] = cos(__rad);
	m[1] = -sin(__rad);
	m[4] = sin(__rad);
	m[5] = cos(__rad);
	h_matrix_mul4(__m, __m, m);
}


void h_tranform_vector(hvec3p __v, _flu_float *__m) {
	float v[] = {__v->x,__v->y,__v->z};
	__v->x = v[0]*__m[0]+v[1]*__m[1]+v[2]*__m[2];
	__v->y = v[0]*__m[4]+v[1]*__m[5]+v[2]*__m[6];
	__v->z = v[0]*__m[8]+v[1]*__m[9]+v[2]*__m[10];
}

# include "../maths.h"
void h_perspective(_flu_float *__mm, _flu_float __fv, _flu_float __m) {
	_flu_float fv;
	fv = 1./tan(__fv/2.);
	_flu_float rm[16] = {
		fv, 0, 0, 0,
		0, fv, 0, 0,
		0, 0, 1, 0,
		0, 0, -1, 1
	};
	/*
		NOTE: the closer to the view is 0, far away is -1
	*/
	h_matrix_mul4(__mm, __mm, rm);

}
//name factor to cofactor
void h_mscale(_flu_float *__m, float __factor) {
	_flu_float rm[16] = {
		__factor, 0, 0, 0,
		0, __factor, 0, 0,
		0, 0, __factor, 0,
		0, 0, 0, 1
	};
	h_matrix_mul4(__m,rm,__m);
}
/*
	identical to h_supplement but takes into account MATRIX multiplication with vector

	its sort for:
		h_supplement()
		h_matrix_mul4()
*/
void h_translate(_flu_float *__m, _flu_float __x, _flu_float __y, _flu_float __z) {
	__m[3]	= __m[0]*__x+__m[1]*__y+__m[2]*__z+__m[3];
	__m[7]	= __m[4]*__x+__m[5]*__y+__m[6]*__z+__m[7];
	__m[11]	= __m[8]*__x+__m[9]*__y+__m[10]*__z+__m[11];
	__m[15]	= __m[12]*__x+__m[13]*__y+__m[14]*__z+__m[15];
}

void static cross(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1) {
	*__dst		= __v0[1]*__v1[2]-__v0[2]*__v1[1];
	__dst[1]	= __v0[2]*__v1[0]-__v0[0]*__v1[2];
	__dst[2]	= __v0[0]*__v1[1]-__v0[1]*__v1[0];
}
void static norm(_flu_float *__v) {
	_flu_float r;
	r = sqr(__v[0]*__v[0]+__v[1]*__v[1]+__v[2]*__v[2]);
	if (r == .0) return;
	__v[0]/=r;
	__v[1]/=r;
	__v[2]/=r;
}

void h_lookat(_flu_float *__m, _flu_float __ex, _flu_float __ey, _flu_float __ez,
	_flu_float __atx, _flu_float __aty, _flu_float __atz,
	_flu_float __upx, _flu_float __upy, _flu_float __upz)
{
	_flu_float fw[3], side[3], up[3];
	*fw	 = __atx-__ex;
	fw[1]	 = __aty-__ey;
	fw[2]	 = __atz-__ez;

	*up	 = __upx;
	up[1]	 = __upy;
	up[2]	 = __upz;

	norm(fw);
	cross(side, fw, up);
	norm(side);
	cross(up, side, fw);

	_flu_float m[16] = {
		side[0], side[1], side[2], 0,
		up[0], up[1], up[2], 0,
		fw[0], fw[1], fw[2], 0,
		0, 0, 0, 1
	};

	h_matrix_mul4(__m, __m, m);
	h_translate(__m, -__ex, -__ey, -__ez);
}

void h_supplement(_flu_float *__m, _flu_float __x, _flu_float __y, _flu_float __z) {	
	__m[3]	= __x;
	__m[7]	= __y;
	__m[11]	= __z;
}

void h_orthographic(_flu_float *__m,
	_flu_float __left, _flu_float __right,
	_flu_float __top, _flu_float __bottom)
{
	_flu_float m[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	m[0] = 2./(__right-__left);
	m[5] = 2./(__top-__bottom);
	m[12] = -((__right+__left)/(__right-__left));
	m[13] = -((__top+__bottom)/(__top-__bottom));
	h_matrix_mul4(__m, __m, m);
}
// shear factor
void h_oblique(_flu_float *__m, _flu_float __sfx, _flu_float __sfy) {
	_flu_float m[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		__sfx, __sfy, 0, 1
	};

	h_matrix_mul4(__m, __m, m);
}

void h_frustum(_flu_float *__m, _flu_float __near, _flu_float __far, _flu_float __left, _flu_float __right, _flu_float __top, _flu_float __bottom) {
/*
	even if you change	2x2 and 2x3 the result from the W division stays the same,
	unless we add impurities to W or Z by addition/non-multible

	W0 = -1*Z
	so Z = Z0/W0 = -1 always
*/
	_flu_float rm[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1,0,
		0, 0, -1.,1
	};
	/*
		1/(-1+0.002) = -0.98....
		2/(-2+0.002) = -0.99....
		as Z increases it will get closer and closer to -1
		are depth buffer it setup to be grater-then -1 from the zero direction.
		NOTE:
			we are looking along the -Z axis,
			so the further away an object is and closer to -1 it is.
		also i dont realy like the setup 0.002 in 3x3 as this can affect the y and x W0 divide
	
		NOTE:
			this is not a good way of doing it as it effects W divide of XY
		im too lazy to fix
	*/
	h_matrix_mul4(__m, rm, __m);
}
