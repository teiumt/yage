#include "common.h"
#include "renderer.h"
#include "resource.h"
#include "../io.h"
#include "../assert.h"
#include "hv_tex.h"
#include "../citric/citric.h"
#include "../citric/ct_font.h"
//hijack
static struct ct_gc gctx;

void static _ui_rect(float *__vtx, _int_u __cnt, struct yarn_texture *__tx,float __r,float __g,float __b,float __a, float *__rgba){
	hv_printf("UI_RECT, %p, %u, %f, %f, %f, %f,\n",__vtx,__cnt,__r,__g,__b,__a);
	/*
		we can ignore __RGBA
	*/
	
	struct hv_tex tex;
	tex.tx = __tx->priv;

	float *consts = _h_is.perc_const;
	consts[0] = __r;
	consts[1] = __g;
	consts[2] = __b;
	consts[3] = __a;

	h_matcopY(consts+4,h_m_ident);
	_h_is.rd->ui_rect(__vtx, __cnt,&tex,ui_ps.priv,ui_vs.priv,ui_link);
}

void static _crect(float *__vtx, _int_u __cnt,float __r,float __g,float __b,float __a){
	
//	h_rect(_h_is._ctx,__vtx,__r,__g,__b,__a);

	hv_printf("CRECT: %f, %f, %f, %f, -> %f\n",__r,__g,__b,__a,__vtx[0]);
}

void hvct_tapinto(void){
	gctx = ct_flue;
	gctx.ui_rect = _ui_rect;
	gctx.crect = _crect;
	ct_init(&gctx);
}

