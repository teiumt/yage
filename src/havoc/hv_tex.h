# ifndef __havoc__tex
# define __havoc__tex
# include "../y_int.h"
#include "../struc.h"
#include "../im.h"
#include "resource.h"
/*
	the arrangment/comasition / file format
*/
#define HV_FMT_PPM 0
typedef struct hv_tex{
	struct h_rsrc rs;
	/*
		its like this because rsrc has to be placed at top of structure.
	*/
	//the handle
	struct y_his_tex *tx;
	void **slot;
	_8_s loaded;
	_8_s reloaded;
	_8_s ignore;
	_int_u format;
	void(*load)(struct hv_tex*);
	struct y_img *im;
} *hv_texp;

#define TEX_RGB		0
#define TEX_RGBA	1
hv_texp tex_new(void);
void tex_load(hv_texp t);
void tex_data(hv_texp,_int_u,_int_u);
hv_texp tex_from_file(char const*,_8_u);
# endif /*__havoc__tex*/
