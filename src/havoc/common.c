#include "mesh.h"
#include "model.h"
# include "common.h"
# include "../m_alloc.h"
# include  "hv_tex.h"
# include "../string.h"
struct havoc_is _h_is;
struct hv_tsyent* hv_tsyget(char const *__path) {
	struct hv_tsyent *ent;
	ent = f_lhash_get(&_h_is.treasury,__path,str_len(__path));
	if(!ent){
		ent = m_alloc(sizeof(struct hv_tsyent));
		f_lhash_put(&_h_is.treasury,__path,str_len(__path),ent);
		ent->ptr = NULL;
	}
	return ent;
}
void hv_tsyput(char const *__path,void *__ptr){
	struct hv_tsyent *ent;
	ent = hv_tsyget(__path);
	ent->ptr = __ptr;
}
void h_rect_geom(float *__dst, float *__vtx) {
	__dst[0] = __vtx[0];
	__dst[1] = __vtx[1];
	__dst[2] = 0;
	__dst[3] = 1;

	__dst[4] = __vtx[2];
	__dst[5] = __vtx[1];
	__dst[6] = 0;
	__dst[7] = 1;

	__dst[8] = __vtx[0];
	__dst[9] = __vtx[3];
	__dst[10] = 0;
	__dst[11] = 1;
}

void hv_sigconnect(_64_u __key,void(*__handler)(_ulonglong,struct h_msg*),_ulonglong __arg){
	_h_is.signals[__key] = __handler;
	_h_is.sigarg[__key] = __arg;
}

# include "../flue/common.h"
void gc_init(void) {
	flue_prime();
	flue_sweep();
}

void gc_deinit(void) {
	flue_deinit();
}

void *gc_new(void) {
	return flue_ctx_new();
}

void gc_make(void *__ctx) {
	FLUE_setctx(__ctx);
	return;
}

void gc_update(void *__ctx) {
	FLUE_CTX->priv->bits = FLUE_CLEAR;
	flue_done();
}

struct h_msgbucket* h_mbkt_new(void) {
	struct h_msgbucket *mb;
	mb = (struct h_msgbucket*)m_alloc(sizeof(struct h_msgbucket*));
	mb->m = NULL;
	return mb;
}

void h_mbkt_destroy(struct h_msgbucket *__bkt) {
	m_free(__bkt);
}

struct h_rectbox *h_rbx_new(struct h_composer *b) {
	struct h_rectbox *bx;
	bx = (struct h_rectbox*)m_alloc(sizeof(struct h_rectbox));
	bx->next = b->bx;
	_int_u i;
	i = 0;
	for(;i != 8;i++) {
		bx->dsc[i].inside = -1;
	}
	bx->n = 0;
	b->bx = bx;
	return bx;
}

void h_rbx_destroy(struct h_rectbox *__bx) {
	m_free(__bx);
}

struct h_msg* h_msg_hull(void) {
	struct h_mhull *h = m_alloc(sizeof(struct h_mhull)+1024);
	void *ptr = h;
	//align to 1024-byte boundary
	h = (((_64_u)h)+(_64_u)1023)&~(_64_u)1023;
	h->ptr = ptr;
	return h;
}

void h_msg_free(struct h_msg *__m) {
	struct h_mhull *h = ((_64_u)__m)&~(_64_u)1023;
	if (!h->cnt) {
		m_free(h->ptr);
	}else{
		h->cnt--;
	}
}

