# ifndef __havoc__resource__h
# define __havoc__resource__h
#include "../im.h"
#define PITHDIR "/home/blort/firefly/pith/"
/*
	what this structure will be used for.

	to handle large amounts of assets, images,textures and meshes alike.

	we cant always have them stored in memory or even video memory for that fact.
	
	lets say we are about to render a scene, 
	before that can be done all assets for that scene must be well loaded or prepared.
	the assets could come in any variety from (meshes, textures, sound) all of have there 
	own respected loading schemes/semantics.

	if we have a list of resources/assets that are to be loaded it would be much
	smoother if we had a common set of functions. 

	this is where this structure comes in to play.

	all resources with there apparent structure which
	contains all its private data for said resource, will have this structure appended
	to it.

	so when the time comes and we need to start loading are assets,
	we will just iterate over all of them and call the ->load() routine.



	after a lot of looking at existing engines, im betting on.

LOADING SEMANTICS:
	we have 3) forms of storage;

	DISK,HOST MEMORY,GPU MEMORY

	if the resource is not needed to produce a frame then its loaded out of GPU memory,
	a copy in HOST memory is keept for easy reloading. if the resource has been made redundent
	then its removed from HOST memory and resides on the DISK.

	ISSUES we face.

	when it comes to DISK storage its resides in a format thats has to be preprocessed before it can be used.
	also DMA might allow for loading from DISK directly to GPU memory so we want to keep things linear.


DEFOLD-ENGINE:

	defold defines resources like,

	sound,mesh,buffer,model.

	i am unsure about model and mesh.

	however the odd thing is that a model in defold dosent use a mesh and 
	vertex data is stored with the model?


	so its not: model->meshlist


	what im trying to figure out is 
	if we have a mesh should loading semantics be on the mesh side?

	or should the buffer data(vertex data) be a resource on its own?


DEFOLD MESH LOADING:

	res_mesh.cpp

	BuildVertices()
		dmBuffer::GetBytes(mesh_resource->m_BufferResource->m_Buffer, (void**)&bytes, &size);
		mesh_resource->m_VertexBuffer = dmGraphics::NewVertexBuffer(g_GraphicsContext, mesh_resource->m_VertSize * mesh_resource->m_ElementCount, bytes, dmGraphics::BUFFER_USAGE_STREAM_DRAW);

	
	BuildVertices is called apon by AcquireResources(); that loads the mesh and its associates like textures,materials and such

	AcquireResources() is called in the rendering sequence.

Resource STAGES:

	if load from file with specific format the file wont be touched until its needed for somthing,
	if its needed to a render then it will be loaded out of that format and directly into GPU memory.
	if its then later put on the back burner then its loaded out of GPU memory and into memory unless 
	a copy job has been submitted to do this while the render is taking place.


OTHER things:
	loading - we may want to load a resource for things other then graphics usage.
	we may want to state where it should be loaded, GPUmemm, DISK?


####### VERY IMPORTANT:
	loading resources from diffrent devices.

	we may have resources being loaded from undesired locations, for example a USB.
	so it would be a good idea to keep track of what resources are from where and what access speeds it has.
	
	diffrent storage devices include

	USB		- SLOW
	DISK 	- SLUGISH
	SSD		- FAST(MID-RANGE)

	we want to keep track of what resources are from where as seen above, not realy where but access time and w/r speeds assocated with those locations.


####### resource philosophy
	what every format the resource its in it is required without argument that its loaded only from that format and that location.
	we may think oh well we will just load the resource and then reformat to easy load times. there is one big dilemma with that
	if we have 200Gb worth of resources then we would be simple duplicating the resources so 200Gb turns into 400Gb so doing such things is not feasible or even considerable.
*/


/*
	TODO:
		if the resource isent loaded however is requires preprocessing then loaded it beforehand in just incase.
*/

/*
	where the resource can be located

	*contained
*/
#define RSRC_COH 4//on hardware
#define RSRC_CIR 8//in ram

/*
	this resource should be loaded directly from source and no other places.
*/
#define RSRC_SOURCE 2


/*
	tells us that we are loading this resource to be used 
	in a render.
*/
#define RSRC_LDRNDR 1

#define RSRC_LINKED 64

/*
	a resource in my definition is anything that contributes
	any material, would it be text,mesh,models,sounds,materials,shaders, and even non renderable things or things like scripts.
	OR.
	online files though HTTP images/icons and shit
*/
struct h_rsrc{
	/*
		TODO:
			add preload routine
	*/
	void(*load)(void*);
	void(*unload)(void*);

	_64_u stamp;
	_64_u info;
	_64_u refcnt;
	struct h_rsrc *next;
};
void hv_rsrc_load(struct h_rsrc *__r);
/*
void h_rs_tex_file(struct h_rs *__rs,char const*);
void h_rs_tex_load(struct h_rs *__rs);
void h_rs_tex_mark_away(struct h_rs *__rs);

void h_rs_mesh_load(struct h_rs *__rs);
void h_rs_mesh_mark_away(struct h_rs *__rs);
*/
void h_rsrc_init(struct h_rsrc*);
void h_rsrc_link(struct h_rsrc*);
void h_rsrc_stimulate(struct h_rsrc*);
void h_rsrc_tidy(void);
# endif /*__havoc__resource__h*/
