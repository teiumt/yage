#ifndef __havoc__landscape__h
#define __havoc__landscape__h
#include "terrain.h"
#include "hv_tex.h"
struct h_landscape {
	hv_texp tx;
	struct h_terrain *t;
};

#endif /*__havoc__landscape__h*/
