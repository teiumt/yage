/*
	also NOTE:
		a descriptor type structure for models is needed to describe where to place things(how to piece together the model)
*/

/* ## MODELS
  a vehicle model consists of 2 parts(no over complicating)
  we have the 'wheels' and the 'body' of the car.

  for the wheels we may have diffrent objects for parts of the wheel.
  so it could consist of a 'tire' and the 'wheels core' two differing materials.

  this is where things get a bit 'i dont realy know'. we could have a case where the tire is dammaged and has a hole in it.
  this means A) we replace the wheel model completely or B) we replace the 'tire' object.

  the other thing is the car itself dosent handle ground contact as that happens on an indevidual wheel basis, meaning
  that each wheel contributes to the collission detection with the ground as suspension will have to be done like this as well.


  most engines assume models are not unique, meaning models may be shared accoss many game objects. so im going to assume
  selection of what to draw is left for the render routine.
  
  this is where i hit a brick wall.
  
  if you have a Car,Train,Helicopter

  i have looked at the XRay and Source engine to get some idea how this is delt with.
  my assumption is that rending is delt in its own way.

  Car_Render{
    Render(Car_Body)
    for every Wheel{
      Render(Car_Wheel)
    }
  }

  Helicopter_Render{
    Render(Helicopter_Shell)

    RPM = {Air Density(may be based on Air temperature + air humidity)+Up draft+Down draft}
    ROT = get_rot(RPM)
    # do some transform(rotation) based on engine RPM output/speed
    Render(Helicopter_Blades)
  }
  this is where things for me get IDK.


  a model may consist of parts but many models may share the same parts?
  this makes the model structure obsolete as the parts of a model can be stored else where(like part of the 'car' structure).

  struct Car{
    struct mdlobject car_body;
    struct mdlobject car_wheel;
  }

  as you see the struct is in non-ptr form as there is no point for pointers as its only placeholder(for textures and mesh).

  you end up as the conclusion that models are just a decription of model(ie. what it consists of(parts)).
  however it still cant be directly connected to an game object as a game object may not be renderable(waste of memory).
  
  # for example bullets may not be rendered.
  
  and objects not in view of player may be offloaded.

  a better name would be 'render_object'

  struct render_object{
    struct render_car car;
    struct render_train train;
  }

  some things dont fit, like the bounding box. this isent needed for rendering but for physiscs.


  for Cars there head and rear lights need to be controlable. this means thses meshes for those need to be 
  registers in the Car structure.
*/


/* RENDERINGS

	for this example we are going to use a Car. 

	a car can be decomposed into parts.

	1) Body(The chassis)
	2) Wheels
	
	things like Vehicles have there own render function. but this brings issues to the table.
	this means that wheels may have there own render function.

	Render_Wheel{
		Render_Mesh(Wheel)
	}

	now we have an issue on how to deal with custom tires and rims.

	so we now have:

	Render_Wheel{
		for every submesh{
			Render_Mesh(submesh)
		}
	}

	you see that no transformation are needed for this. this works because
	the tire and rim are built around one another(they are built to fit).
	and also transformations on wheels would be very intangible.


	the issue is that every type of 'thing' would need its own Render function(not good! it could be accomplish but is unmanageable and difficult)

	struct made_for_oneanother{
		struct mesh meshes[N];
		_int_u nmesh;
	};


	on this subject there are TWO distinguishing 'things' there are frankenstein and cohesion meshes.

	for example mesh groups, you dont want them to undergo matrix transforms(however a displacement map may be used for this).


MESH removal:
	for example we will use a wheel. in the event the tire pops then the mesh must not be rendered.
	this would also cause the wheel contact point to change.


THINGS:

	1) meshes may have there own textures or may share with others.
	2) meshes may have there own shader or may share with others.
	3) meshes may have there own data or may share with others.



SAMPLES:
	struct storage_tray_wheelinfo{
		// where the wheel is placed on this model
		vec3 displacement;
	};
	struct storage_tray_info{
		struct storage_tray_wheelinfo wheels;
		struct{
			char const *file = "/resources/trays.obj";
			char const *name = "mytray";
		}model;
	};

	struct wheel_unidirectional{
		float rotation;
	};

	struct storage_tray{
		struct storage_tray_info *info;
		struct wheel_unidirectional wheels[4];
	};


	Render_storage_tray{
		Render(tray->info->model)
		i for every wheel{
			APPLY_TRANSFORM(tray->info->wheels[i].displacement)
			Render(wheel->model)
		};
	}


	we can conclude that there are diffrences between IMPLEMENTED and non-IMPLEMENTED objects.


	so we might do this:



	struct hv_vehicle{

	};

	struct hv_storage_tray{

	};


	// object with no functionality(its a static object) non-interactable
	struct hv_prop{

	};


*/
