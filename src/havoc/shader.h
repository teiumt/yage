# ifndef __havoc__shader__h
# define __havoc__shader__h
#include "common.h"

struct h_shader_param{
	char const *name;
	_64_u type;
	_64_u offset;
	_64_u placement;
};
typedef struct h_shader {
	void *priv;
	struct h_shader_param *params;
	char const *src;
} *h_shaderp;

void h_shader_create(struct h_shader*,_64_u);
void h_shader_load_file(h_shaderp,char const*);
# endif /*__havoc__shader__h*/
