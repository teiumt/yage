#include "common.h"
#include "subs.h"
void hv_vehicle_control(h_subsisp __vehicle,struct h_msg *__m){
	if(__m->value == H_PRESS){
		switch(__m->code){
			case H_KEY_W:
			__vehicle->phy.vel.z = 1;
			break;
			case H_KEY_A:
			__vehicle->phy.vel.z = -1;
			break;
			case H_KEY_S:
			__vehicle->phy.vel.x = -1;
			break;
			case H_KEY_D:
			__vehicle->phy.vel.x = 1;
			break;
		}
	}else
	if(__m->value == H_RELEASE){
		switch(__m->code){
			case H_KEY_W:
			case H_KEY_A:
			case H_KEY_S:
			case H_KEY_D:
			__vehicle->phy.vel.x = 0;
			__vehicle->phy.vel.z = 0;
			break;
		}
	}
}
