#include "common.h"
#include "maths.h"
#include "subs.h"
#include "../m_alloc.h"
h_subsisp h_sstop = NULL;
h_subsisp h_subsis(void) {
	h_subsisp s;
	s = m_alloc(sizeof(struct h_subsis));
	s->control = 0;
	s->phy.pos = (hvec3){0,0,0};
	s->phy.vel = (hvec3){0,0,0};
	s->flags = 0;
	s->share = 0;
	s->view = NULL;
	s->mesg = NULL;
	s->seat = NULL;
	return s;
}

void h_subsis_link(h_subsisp __s){
	__s->next = h_sstop;
	h_sstop = __s;

	
	struct rdr_job*
	j = hv_rdr_job();
	j->data = __s;	
	j->func = __s->data.render;
	hv_rjlink(j);
}

/*
	attach are view to observe an object
*/
#include "../maths.h"
#include "common.h"
float static rot = 0;
void hv_view_leach(h_subsisp __s) {
	struct h_context *ct = _h_is.ct;
	h_ident(ct->model);
	h_ident(ct->trans);
	ct->model[14] = 0;
	ct->trans[14] = 0;
	
	/*
		its goes like this.

		we translate the object to the position within are world
		- then we rotate the object around the camera
	*/
	h_ident(ct->proj);

	h_perspective(ct->proj,trad(90.),0);
	h_frustum(ct->proj,1,100,10,-10,10,-10);
	/*
		we translate all world objects to the world point(the location of the player)
	*/
	h_mxrot(ct->model,trad(__s->roty));
	h_myrot(ct->model,trad(__s->rotx));
	h_supplement(ct->trans,__s->phy.pos.x,__s->phy.pos.y,__s->phy.pos.z);

	h_supplement(ct->model,0,0,-100);
}

_8_s intersects(float __x, float __y, float __z, float *__v0, float *__v1, float *__v2, float *__yf) {
	hvec3 v0 = {
				.x=__v1[0]-__v0[0],
				.y=__v1[1]-__v0[1],
				.z=__v1[2]-__v0[2]
		};
	hvec3 v1 = {
				.x=__v2[0]-__v0[0],
				.y=__v2[1]-__v0[1],
				.z=__v2[2]-__v0[2]
		};

	hvec3 v2 = {
				.x=__x-__v0[0],
				.y=__y-__v0[1],
				.z=__z-__v0[2]
		};

		double d00, d01, d11, d02, d12, den;
		d00 = h_dot(v0.x, v0.x, v0.y, v0.y, v0.z, v0.z);
		d01 = h_dot(v0.x, v1.x, v0.y, v1.y, v0.z, v1.z);
		d11 = h_dot(v1.x, v1.x, v1.y, v1.y, v1.z, v1.z);
		d02 = h_dot(v0.x, v2.x, v0.y, v2.y, v0.z, v2.z);
		d12 = h_dot(v1.x, v2.x, v1.y, v2.y, v1.z, v2.z);
		den = 1.0/(d00*d11-d01*d01);

		double a, b, g;
		a = ((d11*d02)-(d01*d12))*den;
		b = ((d00*d12)-(d01*d02))*den;
		g = (1.0-a)-b;
	if (a>=0&&b>=0&&g>=0){
		*__yf = a*__v1[1]+b*__v2[1]+g*__v0[1];
//		ffly_fprintf(ffly_err, "BARY: %f, %f, %f\n",d00,d01, *__yf);
		return 0;

	}
	return -1;
}


void hv_terrain_contact(h_subsisp __s) {
	float x,y,z;
	x = __s->phy.pos.x;
	y = __s->phy.pos.y;
	z = __s->phy.pos.z;

	struct h_terrain *t = __s->contact.terr;
	void **list[4096];
	_int_u cnt;
	cnt = psp_find(list,&t->psp,-x,-y,-z);
	ffly_fprintf(ffly_err, "#PSP FIND: %u\n",cnt);
	_int_u i;
	_int_u tot = 0;
	i = 0;

	_int_u icnt = 0;
	for(;i != cnt;i++) {	
		void **p = list[i];
		_int_u j;
		j = 0;
		while(*(p+j) != NULL) {
			float yf;
			if (!intersects(-x,-y,-z,p[j],p[j+4096],p[j+4096*2],&yf)) {
			//	ffly_fprintf(ffly_err, "######in, change %f.\n",yf+y);	
				__s->phy.vel.y = 0;
				__s->phy.pos.y = -yf;
			 
				icnt++;
				
				return;
			}
			tot++;
			j++;
		}
	}
}

_8_s static premises_intrusion(h_subsisp __a, h_subsisp __b){
	hvec3 d0,d1,d2;

	hvec3 nb,fb;
	hvec3 na,fa;
	h_3vadd(&nb,&__b->render.model.prem.near,&__b->phy.pos);
	h_3vadd(&fb,&__b->render.model.prem.far,&__b->phy.pos);
	
	h_3vadd(&na,&__a->render.model.prem.near,&__a->phy.pos);
	h_3vadd(&fa,&__a->render.model.prem.far,&__a->phy.pos);

	h_3vsub(&d0,&fa,&nb);
	h_3vsub(&d1,&fa,&fb);
//	hv_printf("{%p # %p} %f, %f, %f --- %f, %f, %f\n",__a,__b,
//		d0.x,d0.y,d0.z,
//		d1.x,d1.y,d1.z
//	);

	if(d0.x>0 && d0.y>0 && d0.z>0){
		if(d1.x<0 && d1.y<0 && d1.z<0){
			return 0;
		}
	}

	h_3vsub(&d0,&na,&nb);
	h_3vsub(&d1,&na,&fb);
	if(d0.x>0 && d0.y>0 && d0.z>0){
		if(d1.x<0 && d1.y<0 && d1.z<0){
			return 0;
		}
	}


	return -1;
}


void hv_subs_subs_contact(h_subsisp __s) {
  if(!__s->mesg) return;
	h_subsisp s;
  s = h_sstop;
	/*
	TO BE REMOVED;
	*/
  while(s != NULL) {
		if(__s != s){
			if(s->flags&SUBS_CONTACT_SUBS){
				if(!premises_intrusion(__s,s)){
					__s->share = s->share|SUBS_SHARE_HERE;
					__s->contacter = s;
					__s->mesg(__s);
				}else{
					if(__s->share&SUBS_SHARE_HERE){
						if(__s->contacter == s)
							__s->share = 0;
					}
				}
			}
		}
		s = s->next;
	}
}

void h_subs_terrain_contact(h_subsisp __s, struct h_terrain *__terr) {
	return;
	__s->contact.terr = __terr;
	__s->flags |= SUBS_CONTACT_TERRAIN;
}

static struct h_routine view;
/*
	the camera/view can be attached to an object,
	so we push a routine that will position the view to
	the object in question.
*/
void h_subs_view_attach(h_subsisp __s) {
	__s->view = hv_view_leach;
	__s->control = 1;
}

void hv_subsis_static_model(h_subsisp __s){
	h_model_init(&__s->render.model);
}

struct h_subsis_data h_static_subsis = {
	.render = h_static_subsis_render
};
