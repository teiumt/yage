#ifndef __havoc__subs__h
#define __havoc__subs__h
struct h_subsis;
#include "../y_int.h"
#include "model.h"
#include "vec.h"
#include "common.h"
#include "terrain.h"
#include "hv_render.h"
#include "hv_vehicle.h"
/*
	the physical component of a subs
*/
#include "comp/comp_physical.h"
#include "hv_projectile.h"
#define SUBS_CONTACT_SUBS			1
#define SUBS_CONTACT_TERRAIN	2
#define SUBS_PHYSICS					4
#define SUBS_IN_VEHICLE       8
#define SUBS_SHARE_HERE 1
#define SUBS_SHARE_VEHICLE 2
/*
	subsistence - a state of existence(somthing that exists)
	this is an ingame object/body
*/

struct hv_player{
	float x,y,z;
};

struct h_sst_contact {
	struct h_terrain *terr;
};
struct h_subsis;
struct h_subsis_data{
	void(*render)(struct rdr_job*);
};

struct hv_renderable{
	/*
		NOTE: not pointer
	*/
	struct h_model model;
};
/*
	NOTE:
		a model should be outside of subs,
		a subs is a thing thats identity is decided
		and specifics cant be applyed to it, meaning
		applying a model to a subs is negligible.
*/
typedef struct h_subsis {
	/*
		NOTE: these are attributes or abilitys for the subs
	*/
	struct h_sst_physical phy;
	struct h_sst_contact contact;
	h_modelp model;
	_64_u control;
	_64_u flags;
	_64_u share;
	float rotx, roty;
	void*priv;
	void(*view)(h_subsisp);
	struct h_subsis_data data;
	struct hv_projectile proj;
	struct hv_player player;
	struct hv_vehicle vehicle;
	struct h_subsis *contacter;
	struct hv_renderable render;
	struct h_subsis *seat;
	void(*mesg)(struct h_subsis*);
	hvec3 loc;
	struct h_subsis *next;
} *h_subsisp;
extern struct h_subsis_data h_static_subsis;
void h_subsis_link(h_subsisp);
void h_intergrate_forces_onto(h_subsisp __s);
extern h_subsisp h_sstop;
h_subsisp h_subsis(void);
void hv_view_leach(h_subsisp);
void hv_player_control(h_subsisp __player,struct h_msg *__m);
void h_subs_view_attach(h_subsisp __s);
void h_subs_terrain_contact(h_subsisp __s, struct h_terrain*);


h_subsisp hv_projectile_create(struct hv_projinfo*);
void hv_projectile_destory(h_subsisp);

void hv_subsis_static_model(h_subsisp);
#endif/*__havoc__subs__h*/
