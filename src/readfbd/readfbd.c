# include <unistd.h>
# include <string.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <malloc.h>
# include "../y_int.h"
# include "../as/bd.h"
# include "../shs.h"
int main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("please provide (FBD)-file.\n");
		return -1;
	}
	int fd;
	if ((fd = open(__argv[1], O_RDONLY, 0)) == -1) {
		printf("failed to open file at '%s'.\n", __argv[1]);
		return -1;
	}

	struct f_bd_hdr hdr;
	read(fd, &hdr, sizeof(struct f_bd_hdr));

	printf("BDH{start: %u, size: %u, n_blobs: %u, symbol_table: %u, string_table: %u}.\n",
		hdr.start, hdr.size, hdr.n_blobs, hdr.syt, hdr.stt);

	struct f_bd_trek syt, stt;
	pread(fd, &syt, sizeof(struct f_bd_trek), hdr.syt);
	pread(fd, &stt, sizeof(struct f_bd_trek), hdr.stt);

	printf("syt_trek: start: %u, size: %u, n: %u.\n", syt.start, syt.size, syt.n);
	printf("stt_trek: start: %u, size: %u, n: %u.\n", stt.start, stt.size, stt.n);

	char *sbuf;
	sbuf = (char*)malloc(stt.size);
	pread(fd, sbuf, stt.size, stt.start);
	
	char *p;
	char buf[F_SHS_STR_MAX+1];
	_int_u i = 0;
	struct f_bd_sym *sybuf, *sy;
	sybuf = (struct f_bd_sym*)malloc(syt.size);
	pread(fd, sybuf, syt.size, syt.start);
	i = 0;
	for(;i != syt.n;i++) {
		sy = sybuf+i;
		p = sbuf+sy->name;
		memcpy(buf, p, sy->len);
		buf[sy->len] = '\0';

		printf("sym-%u: '%s', flags: %x, name: %u, name_len: %u\n", i, buf, sy->flags, sy->name, sy->len);
	}

	free(sybuf);
	free(sbuf);
	
	if (hdr.n_blobs>0) {
		struct f_bd_blob_desc *dsc;
		_8_u *buf = (_8_u*)malloc(hdr.size);
		pread(fd, buf, hdr.size, hdr.start);
		_int_u off = 0;
		struct f_bd_blob *b;
		i = 0;
		for(;i != hdr.n_blobs;i++) {
			b = (struct f_bd_blob*)(buf+off);
			dsc = f_bd_blob_dsc+b->op;
			printf("BLOB{op: %u}.\n", b->op);
			off+=dsc->size;
		}

		free(buf);
	}
	close(fd);
	return 0;
}
