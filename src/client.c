# include "net.h"
# include "system/io.h"
# include "dep/bzero.h"
# include "dep/str_len.h"
# include "dep/str_cpy.h"
# include "system/nanosleep.h"
# include "creek.h"
# include "inet.h"
# include "chrdump.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
/*
	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_TCP);
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(10198);
	ff_net_connect(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr));

	char s[128];
	f_bzero(s, 128);
	ff_net_recv(sock, s, 128, 0, &err); 
	printf("%s\n", s);
	ff_net_close(sock);

*/
/*	
	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_UDP);

	struct creek_ff_netsock par;
	par.sock = sock;
	ffly_creek_init(1, 1, &par);
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(10198);
	
	ff_net_bound(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr));

	char buf[128];
//	f_str_cpy(buf, "abd");
	ffly_fprintf(1, "Hello-%u.\n", 128);
	ff_net_send(sock, buf, 128, 0, &err);


	ff_net_close(sock);
*/
	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_TCP);
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(10198);
	struct f_netinfo ni = {
		.adr = &addr,
		.len = sizeof(struct sockaddr_in)
	};
	ff_net_connect(sock, &ni);
	char buf[21299];
//	sock->prot.recv(sock->ctx.priv_ctx, buf, 64, 0, &err);
	ff_net_recv(sock, buf, 21299, 0, &err);
//	ff_net_send(sock, buf, 21299, 0, &err);

	ffly_chrdump(buf, 21299);

	ff_net_close(sock);
/*
	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, AF_INET, SOCK_DGRAM, 0);

	struct sockaddr_in to;
	f_bzero(&to, sizeof(struct sockaddr_in));
	socklen_t len = sizeof(struct sockaddr_in);

	to.sin_addr.s_addr = inet_addr("127.0.0.1");
	to.sin_family = AF_INET;
	to.sin_port = htons(21299);

	char buf[200];
	f_bzero(buf, 200);
	f_str_cpy(buf, "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
	ff_net_sendto(sock, (struct sockaddr*)&to, len, (void*)buf, 200, &err);
	ffly_nanosleep(1, 0);
	
	ff_net_close(sock);
*/
}
