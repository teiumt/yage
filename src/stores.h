# ifndef __ffly__stores__h
# define __ffly__stores__h
# include "y_int.h"
# include "types.h"
enum {
	_ff_stores_motd
};

void* ff_stores_get(_8_u);
_f_err_t ff_stores_connect(char const*, _16_u, char const*);
_f_err_t ff_stores_login(char const*, char const*);
_f_err_t ff_stores_logout(void);
_f_err_t ff_stores_disconnect(void);
# endif /*__ffly__stores__h*/
