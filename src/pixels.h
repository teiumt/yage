#ifndef __y__pixels__h
#define __y__pixels__h
#include "y_int.h"
extern float *mu_mat;
void rgb_to_rgba(_8_u*,_8_u*,_int_u,_int_u);
void rgb_to_rgba_float(float*,_8_u*,_int_u,_int_u);
void rgba_to_rgba_float(float*,_8_u*,_int_u,_int_u);
#endif/*__y__pixels__h*/
