#define __thread_internal
#include "../thread.h"
#include "../ctl.h"
#include "../io.h"
#define _ ((void*)-1)
struct tm_struc _tm = {
	.active_threads = 0,
	.pot = NULL,
	.sig = 0,
	.uu = {
		.off = 64
	},
	.next_joined = 64,
	.holes = {
		_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
		_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _,
		_, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _
	},
	.lock = MUTEX_INIT
};
/*
	find new name

	start from 0 to NHOLES
	and reloop is -1 then the hole is unused if >=0 then its used

	we could use timing but that can be left for higher level function to do
*/
// only goes up to 32
/*
	and then should rotate backround
*/
#define MASK ((_32_u)0x1f)
_32_u static nhole = 0;

_32_i tm_obtain_hole(void) {
	if (*(_tm.holes+nhole) != TM_DEADEND) {
		return -1;
	}

	return ((nhole++)&MASK);
}
// thread relinquish
void tm_relinquish_hole(_16_u __hole) {
	*(_tm.holes+__hole) = TM_DEADEND;
}

#define no_threads\
	(_tm.next_joined)

/*
	its a deadbeet structure,
	if there thread structure dosent contain things like a stack then we dont need it.
*/
void static unjoin(threadp __th){
	threadp t2;
	t2 = get_thrbyid(_tm.joined[_tm.next_joined]);
	_tm.joined[__th->idx] = t2->id;
	t2->idx = __th->idx;
	_tm.next_joined++;	
}

_y_err
tm_del(threadp __th) {
	// keep memory on pot 0
	MSG(INFO, "goodbye from thread %u\n", __th->t.id)
	mt_lock(&_tm.lock);
	*(_tm.uu.free+(--_tm.uu.off)) = __th->id;
	tm_hset(__th->t.id, -1);
	mt_unlock(&_tm.lock);
	return FFLY_SUCCESS;
}

_8_s
tm_unused_fetch(threadp *__th, _32_u h) {
	_int_u reused = 0;
	if ((reused = (_tm.uu.off<64))) {
		/*
			pop are structure from the top of are heap
		*/
		threadp t;
		_32_u id;
		id = *(_tm.uu.free+(_tm.uu.off++));
		t = get_thrbyid(id);
		tm_hset(h, t);
		/*
			we have are thread structure, only thing left is to execute
		*/
		*__th = t;
		return 0;
	}
	return -1;
}

void tm_morealloc(void) {
	celr_overflow(&_tm._threads,PAGE_SHIFT){
		celr_more(&_tm._threads,BLKSIZE);
	}  
}
