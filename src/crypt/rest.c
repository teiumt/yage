# include "../m_alloc.h"
# include "../crypt.h"
# include "../io.h"
# include "../system/flags.h"
#include "rest.h"
#include "../string.h"
void rest_init(struct rest_context *__ct) {
	__ct->pos = 0;
	__ct->block[0] = 0xffffffffffffffff;
	__ct->block[1] = 0xffffffffffffffff;
	__ct->block[2] = 0xffffffffffffffff;
	__ct->block[3] = 0xffffffffffffffff;
	__ct->block[4] = 0xffffffffffffffff;
	__ct->block[5] = 0xffffffffffffffff;
	__ct->block[6] = 0xffffffffffffffff;
	__ct->block[7] = 0xffffffffffffffff;
}
/*
	NOTE:
		this is cheap its only to get other stuff working.
*/
_64_u rest_sum64(struct rest_context *__ct) {
	_64_u val;
	_int_u i;

	_64_u mask = 0xffffffff;
	val = 0xffffffffffffffff;
	i = 0;
	for(;i != REST_BLKSIZE;i++) {
		_int_u j;
		j = 0;
		for(;j != 8;j++) {
			mask = mask>>4|((mask&0xf)<<60);
			mask = val>>2|((val&0x3)<<62);
			val = (val+mask)^((_8_u*)__ct->block)[i];
		}
	}
	return val;
}


//void static _rest_rearrange(void *__key) {
void static _rest_hash(void *__buf, void *__key) {	
	_int_u i;
	i = 0;
	_64_u *p = __buf;
	_64_u val;
	val = 0xffffffffffffffff;
	while(i != REST_BLKSIZE>>3) {
		val = ((_64_u*)__key)[i]^val;
		val = val^*p++;
		((_64_u*)__key)[i] = val;
		i++;
	}
}

void rest_more(struct rest_context *__ct, void *__buf, _int_u __length) {
	_64_u *p = __buf;
	if (__ct->pos>0) {
		_int_u needed = REST_BLKSIZE-__ct->pos;
		if (__length<needed) {
			mem_cpy(__ct->buf+__ct->pos,__buf,__length);
			__ct->pos+=__length;
			return;
		}
		mem_cpy(__ct->buf+__ct->pos,__buf,needed);
		__ct->pos = 0;
		_rest_hash(__ct->buf,__ct->block);
		p = ((_8_u*)p)+needed;
		__length-=needed;
	}
	_int_u blc = __length>>REST_BLKSHFT;
	_int_u i;
	i = 0;
	for(;i != blc;i++) {
		_rest_hash(p,__ct->block);
		p+=(REST_BLKSIZE>>3);
	}

	_int_u leftov;
	//missalignment
	if ((leftov=(__length&REST_BLKMASK))>0) {
		mem_cpy(__ct->buf+__ct->pos,p,leftov);
		__ct->pos+=leftov;
	}
}

void rest_encpt(void *__buf, _int_u __size, void *__dst, void *__key) {
	_8_u *sr, *ds;
	sr = __buf;
	ds = __dst;
	_64_u key = *(_64_u*)__key;
	_8_u *end = sr+__size;
	while(sr != end) {
		*ds = *sr^key;
		sr++;
		ds++;
	}
}

void rest_decpt(void *__buf, _int_u __size, void *__dst, void *__key) {
	_8_u *sr, *ds;
	sr = __buf;
	ds = __dst;
	_64_u key = *(_64_u*)__key;
	_8_u *end = sr+__size;
	while(sr != end) {
		*ds = *sr^key;
		sr++;
		ds++;
	}
}

void rest_final(struct rest_context *__ct) {
	if ((REST_BLKSIZE-__ct->pos)>0) {
		mem_set(__ct->buf+__ct->pos,0xff,(REST_BLKSIZE-__ct->pos));
	}
	_rest_hash(__ct->buf,__ct->block);
	__ct->pos = 0;
}

