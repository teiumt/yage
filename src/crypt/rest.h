#ifndef __rest__h
#define __rest__h

#include "../cpt_struc.h"
#define REST_BLKSHFT 6
#define REST_BLKSIZE (1<<REST_BLKSHFT)
#define REST_BLKMASK (REST_BLKSIZE-1)
struct rest_block;
struct rest_context {
	_64_u block[REST_BLKSIZE];
	_8_u buf[REST_BLKSIZE];
	_int_u pos;
};
_64_u rest_sum64(struct rest_context*);
void rest_init(struct rest_context*);
void rest_more(struct rest_context*,void*,_int_u);
void rest_encpt(void*,_int_u,void*,void*);
void rest_decpt(void*,_int_u,void*,void*);
void rest_final(struct rest_context*);
#endif/*__rest__h*/
