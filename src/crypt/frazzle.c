# include "../crypto.h"
# include "../y_int.h"
void static
spook(_8_u *__p, _8_u __imn) {
	_8_u byte;
	_8_u l,r;
_again:
	byte = *__p;
	l = byte&0xe0;
	r = byte&0x7;
	*__p = ((((byte&0x8)<<4)|(l>>1))|((r<<1)|((byte&0x10)>>4)));
	if (__imn>0) {
		__imn--;
		goto _again;
	}
}

void static
despook(_8_u *__p, _8_u __imn) {
	_8_u byte;
	_8_u l,r;
_again:
	byte = *__p;
	l = byte&0x70;
	r = byte&0xe;
	*__p = ((((byte&0x80)>>4)|(l<<1))|((r>>1)|((byte&0x1)<<4)));
	if (__imn>0) {
		__imn--;
		goto _again;
	}
}

void ffly_frzz_enc(void *__p, _int_u __bc, _64_u __key) {
	_8_u *p = (_8_u*)__p;
	_8_u *end = p+__bc;
	while(p != end) {
		spook(p, __key&0xf);
		*p ^= __key;
		spook(p, __key>>4&0xf);
		__key = (__key>>60)|__key<<4;
		p++;
	}
}

void ffly_frzz_dec(void *__p, _int_u __bc, _64_u __key) {
	_8_u *p = (_8_u*)__p;
	_8_u *end = p+__bc;
	while(p != end) {
		despook(p, __key>>4&0xf);
		*p ^= __key;
		despook(p, __key&0xf);
		__key = (__key>>60)|__key<<4;
		p++;
	}
}

void static
frzz_enc(long long *__args) {
	ffly_frzz_enc((void*)*__args, __args[1], __args[2]);
}

void static
frzz_dec(long long *__args) {
	ffly_frzz_enc((void*)*__args, __args[1], __args[2]);
}

void static(*op[])(long long*) = {
	frzz_enc,
	frzz_dec
};

void frzz_op(struct crypto_op *__op) {
	op[__op->op](__op->args);
}
