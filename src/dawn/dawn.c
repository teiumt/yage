#include "dawn.h"
#include "../m_alloc.h"
#include "../ffly_def.h"

void dwn_init(void) {
	_dwn_main.xdis = 204;
	_dwn_main.ydis = 204;
	_dwn_main.nodes = NULL;
	_dwn_main.node_cnt = 0;
	y_log_file(&_dwn_main.log,"dwnlog");
	dwn_mesh_init(&_dwn_main.mesh);
	_dwn_main.front_face = NULL;

}
#include "../string.h"
void dwn_vec_copy(struct dwn_vec3 *__dst,struct dwn_vec3 *__src) {
	mem_cpy(__dst,__src,sizeof(struct dwn_vec3));
}

void dwn_deinit(void) {
	y_log_fileend(&_dwn_main.log);
}
#define dot(__x0, __x1, __y0, __y1)\
	((__x0*__x1)+(__y0*__y1))

_8_s dwn_tri_intersect(struct dwn_vec3 *__v, struct dwn_vec3 *__point) {
	struct dwn_vec3 v0 = {
		__v[1].x-__v[0].x,
		__v[1].y-__v[0].y
	};

	struct dwn_vec3 v1 = {
		__v[2].x-__v[0].x,
		__v[2].y-__v[0].y
	};

	struct dwn_vec3 v2 = {
		__point->x-__v[0].x,
		__point->y-__v[0].y
	};


	float d00, d11,d02,d12;
	d00 = dot(v0.x,v0.x,v0.y,v0.y);
	d11 = dot(v1.x,v1.x,v1.y,v1.y);

	d02 = dot(v0.x,v2.x,v0.y,v2.y);
	d12 = dot(v1.x,v2.x,v1.y,v2.y);

	float d01;
	d01 = dot(v0.x,v1.x,v0.y,v1.y);

	float den;
	den = 1./(d00*d11-d01*d01);

	float a,b,g;
	a = (d11*d02-d01*d12)*den;
	b = (d00*d12-d01*d02)*den;
	g = (1.0-a)-b;
	if(a>=0 && b>=0 && g>=0){
		return 0;
	}
	return 1;
}

struct dwn_cnode *dwn_node_new(void) {
	struct dwn_cnode *n = m_alloc(sizeof(struct dwn_cnode));
	n->next = _dwn_main.nodes;
	n->prev = NULL;
	if(_dwn_main.nodes != NULL)
		_dwn_main.nodes->prev = n;
	_dwn_main.nodes = n;
	_dwn_main.node_cnt++;
	return n;
}

void dwn_mesh_init(struct dwn_mesh *m){
	m->num_faces = 0;
	m->num_bunches = 0;
	dwn_mempool_init(&m->vtx,sizeof(struct dwn_vert));
	dwn_mempool_init(&m->faces,sizeof(struct dwn_face));
}
void static cross(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1) {
	*__dst	= __v0[1]*__v1[2]-__v0[2]*__v1[1];
	__dst[1]	= __v0[2]*__v1[0]-__v0[0]*__v1[2];
	__dst[2]	= __v0[0]*__v1[1]-__v0[1]*__v1[0];

}

void static normfor(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1,_flu_float *__v2) {
	_flu_float j[3],k[3];
	j[0] = __v1[0]-__v0[0];
	j[1] = __v1[1]-__v0[1];
	j[2] = __v1[2]-__v0[2];

	k[0] = __v2[0]-__v0[0];
	k[1] = __v2[1]-__v0[1];
	k[2] = __v2[2]-__v0[2];


	cross(__dst,j,k);
/*
	DONT FUCKING CHANGE!
	YOU WILL REGRET IT!
*/
	__dst[3] = 0;
}

#include "../tools.h"
void dwn_draw_model(void) {
	if(!_dwn_main.mesh.vtx.off)return;
	
	

	_flu_float *flt = _dwn_main.perc_const;

	_flu_float normmat[16];
	h_ident(normmat);
	h_matrix_inv16(normmat,_dwn_main.model);
	h_transpose(normmat);
	y_floatprint(&_dwn_main.log.ioc_out,_dwn_main.model,16);
	h_matcopY(flt,_dwn_main.trans);
	h_matcopY(flt+16,_dwn_main.proj);
	h_matcopY(flt+32,_dwn_main.model);
	h_matcopY(flt+48,normmat);
	struct iu_window *w = iu_grabwd_at(0);
	struct yarn_viewport vpor;
	vpor.translate[0] = w->surf->width*0.5;
	vpor.translate[1] = w->surf->height*0.5;
	vpor.translate[2] = 0;
	vpor.translate[3] = 0;

	vpor.scale[0] = 1024;
	vpor.scale[1] = 1024;
	vpor.scale[2] = 1;
	vpor.scale[3] = 1;

	yarn_flue_viewport(&vpor);

	dwn_clear(_dwn_main.zbuffer,-1,-1,-1,-1);
	dwn_zbuffer(_dwn_main.zbuffer);

	_int_u num_faces = _dwn_main.mesh.num_faces;
	struct yarn_buffer normbuf;
	yarn_flue_buffer_new(&normbuf);
	yarn_flue_buffer_data(&normbuf,sizeof(float)*4*num_faces*2);
	yarn_flue_buffer_map(&normbuf);

	struct yarn_buffer vtbuf;
	yarn_flue_buffer_new(&vtbuf);
	yarn_flue_buffer_data(&vtbuf,sizeof(float)*4*_dwn_main.mesh.vtx.off);
	yarn_flue_buffer_map(&vtbuf);

	float *norm = normbuf.cpu_ptr;
	float *vtx;
	vtx = vtbuf.cpu_ptr;
	float *vt;
	struct dwm_mphdr *h;
	h = _dwn_main.mesh.vtx.top;
	_int_u idx_cnt = 0;
	while(h != NULL) {
		struct dwn_vert *v = ((_8_u*)h)+sizeof(struct dwm_mphdr);
		v->indx = idx_cnt++;
		vt = vtx+(v->indx*4);
		vt[0] = v->pos.x;
		vt[1] = v->pos.y;
		vt[2] = v->pos.z;
		vt[3] = 1;
		h = h->next;
	}

	struct yarn_buffer idxbuf;
	yarn_flue_buffer_new(&idxbuf);
	yarn_flue_buffer_data(&idxbuf,sizeof(_32_u)*num_faces*6);
	yarn_flue_buffer_map(&idxbuf);

	_32_u *idx;
	idx = idxbuf.cpu_ptr;

	struct dwn_bunch *bch;
	struct dwn_face *face;
	_int_u i,j;
	i = 0;
	while(i != _dwn_main.mesh.num_bunches) {
		bch = _dwn_main.mesh.face_table+i;
		j = 0;
		for(;j != bch->n;j++) {
			face = bch->faces+j;
			norm[0] = face->normal.x;
			norm[1] = face->normal.y;
			norm[2] = face->normal.z;
			norm[3] = 0;
			norm[4] = face->normal.x;
			norm[5] = face->normal.y;
			norm[6] = face->normal.z;
			norm[7] = 0;
			norm+=8;
	
			idx[0] = face->verts[0]->indx;//a0
			idx[1] = face->verts[1]->indx;//a1
			idx[2] = face->verts[2]->indx;//b0

			idx[3] = face->verts[2]->indx;//b0
			idx[4] = face->verts[3]->indx;//b1
			idx[5] = face->verts[1]->indx;//a1
			idx+=6;
		}

		i++;
	}

	dwn_printf("DWN_DRAW_MODEL: %u.\n",num_faces);

	dwn_draw_begin();
	dwn_draw_generic(&idxbuf,&vtbuf,&normbuf);
	dwn_draw_array(num_faces*2,DWN_TRI);

//	the place that the 3d rendering will take foot

	dwn_zbuffer(NULL);

	yarn_flue_buffer_destroy(&vtbuf);
	yarn_flue_buffer_destroy(&normbuf);
	yarn_flue_buffer_destroy(&idxbuf);
}


