#include "dwn_mempool.h"
#include "../m_alloc.h"
#include "dawn.h"
//128x
#define PAGE_SHFT 7
#define PAGE_SIZE (1<<PAGE_SHFT)
#define PAGE_MASK (PAGE_SIZE-1)

void dwn_mempool_init(struct dwn_mempool *__pl,_int_u __elemsize) {
	dwn_printf("MEMPOOL ALLOC.\n");
	__pl->pages = m_alloc(sizeof(void*));
	__elemsize+=sizeof(struct dwm_mphdr);
	*__pl->pages = m_alloc(PAGE_SIZE*__elemsize);
	__pl->elem_sz = __elemsize;
	__pl->off = 0;
	__pl->pgc = 1;
	__pl->top = NULL;
	__pl->tail = NULL;
}

#define attach(__pl,h) \
		if(!__pl->top){__pl->top = h;} \
		if(__pl->tail != NULL){__pl->tail->next = h;} \
		__pl->tail = h;
void* dwn_mempool_alloc(struct dwn_mempool *__pl) {
	_int_u pge = __pl->off>>PAGE_SHFT;
	_int_u pof = __pl->off&PAGE_MASK;

	__pl->off++;
	_8_u *page = __pl->pages[pge];

	_8_u *ent = page+(pof*__pl->elem_sz);
	struct dwm_mphdr *h = ent;
	attach(__pl,h)
	h->next = NULL;

	/*
		look things can be done on removing 
		+sizeof(struct dwm_mphdr) too lazy.
	*/
	return ent+sizeof(struct dwm_mphdr);
}

/*
	this has to have controled usage,
	this method wasts memory

	TODO:
		create another track besides this one
	a dedacated region for this?
*/
void* dwn_mempool_bunch(struct dwn_mempool *__pl, _int_u __n) {
	_int_u pge = __pl->off>>PAGE_SHFT;
	_int_u pof = __pl->off&PAGE_MASK;

	__pl->off+=__n;
	_8_u *page = __pl->pages[pge];

	_8_u *ent = page+(pof*__pl->elem_sz);
	struct dwm_mphdr *h = ent;
	attach(__pl,h);

	return ent+sizeof(struct dwm_mphdr);
}

void dwn_mempool_deinit(struct dwn_mempool *__pl) {
}
