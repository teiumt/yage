#ifndef __mempool__h
#define __mempool__h
#include "../y_int.h"
#include "../types.h"
#define DWN_MEMPOOL_PAGE_SHFT 7
#define DWN_MEMPOOL_PAGE_SIZE (1<<DWN_MEMPOOL_PAGE_SHFT)
#define DWN_MEMPOOL_PAGE_MASK (DWN_MEMPOOL_PAGE_SIZE-1)

struct dwm_mphdr {
	struct dwm_mphdr *next;
};

struct dwn_mempool {
	void **pages;
	_int_u pgc;
	_int_u elem_sz;
	_int_u off;
	struct dwm_mphdr *top, *tail;
};
void* dwn_mempool_alloc(struct dwn_mempool *__pl);
void dwn_mempool_init(struct dwn_mempool*,_int_u);
void dwn_mempool_deinit(struct dwn_mempool*);
void* dwn_mempool_bunch(struct dwn_mempool*,_int_u);
#endif/*__mempool__h*/
