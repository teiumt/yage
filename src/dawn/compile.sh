rm -f *.o hw/*.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd dawn
ffly_objs="$ffly_objs dawn.o dwn_mempool.o export/dwn_wavefront.o $dst_dir/iu_composer.o $dst_dir/iu_renderer.o $dst_dir/iu_tabs.o $dst_dir/iu_tuck.o $dst_dir/iu_draw.o $dst_dir/iu_shell.o $dst_dir/iu_signal.o $dst_dir/iu_label.o $dst_dir/iu_board.o $dst_dir/iu_scale.o $dst_dir/ct_mjx.o dwn_flue.o $dst_dir/iu_menu.o $dst_dir/iu_window.o $dst_dir/iu_toolbar.o $dst_dir/iu_widget.o $dst_dir/iu_rect.o $dst_dir/iu_text.o $dst_dir/iu_btn.o $dst_dir/citric.o $dst_dir/ct_flue.o $dst_dir/iu.o $dst_dir/iu_init.o $dst_dir/ct_font.o"
gcc $cc_flags -c -o dwn_flue.o dwn_flue.c
gcc $cc_flags -c -o dwn_mempool.o dwn_mempool.c
gcc $cc_flags -c -o dawn.o dawn.c
gcc $cc_flags -c -o $dst_dir/ct_mjx.o $root_dir/../citric/mjx.c
gcc $cc_flags -c -o $dst_dir/ct_flue.o $root_dir/../citric/flue.c
gcc $cc_flags -c -o $dst_dir/iu.o $root_dir/../iu/iu.c
gcc $cc_flags -c -o $dst_dir/iu_widget.o $root_dir/../iu/iu_widget.c
gcc $cc_flags -c -o $dst_dir/iu_init.o $root_dir/../iu/iu_init.c
gcc $cc_flags -c -o $dst_dir/ct_font.o $root_dir/../citric/ct_font.c
gcc $cc_flags -c -o $dst_dir/citric.o $root_dir/../citric/citric.c
gcc $cc_flags -c -o $dst_dir/iu_btn.o $root_dir/../iu/iu_text.c
gcc $cc_flags -c -o $dst_dir/iu_text.o $root_dir/../iu/iu_btn.c
gcc $cc_flags -c -o $dst_dir/iu_rect.o $root_dir/../iu/iu_rect.c
gcc $cc_flags -c -o $dst_dir/iu_toolbar.o $root_dir/../iu/iu_toolbar.c
gcc $cc_flags -c -o $dst_dir/iu_window.o $root_dir/../iu/iu_window.c
gcc $cc_flags -c -o $dst_dir/iu_menu.o $root_dir/../iu/iu_menu.c
gcc $cc_flags -c -o $dst_dir/iu_scale.o $root_dir/../iu/iu_scale.c
gcc $cc_flags -c -o $dst_dir/iu_board.o $root_dir/../iu/iu_board.c
gcc $cc_flags -c -o $dst_dir/iu_label.o $root_dir/../iu/iu_label.c
gcc $cc_flags -c -o $dst_dir/iu_signal.o $root_dir/../iu/iu_signal.c
gcc $cc_flags -c -o $dst_dir/iu_shell.o $root_dir/../iu/iu_shell.c
gcc $cc_flags -c -o $dst_dir/iu_draw.o $root_dir/../iu/iu_draw.c
gcc $cc_flags -c -o $dst_dir/iu_tuck.o $root_dir/../iu/iu_tuck.c
gcc $cc_flags -c -o $dst_dir/iu_tabs.o $root_dir/../iu/iu_tabs.c
gcc $cc_flags -c -o $dst_dir/iu_renderer.o $root_dir/../iu/iu_renderer.c
gcc $cc_flags -c -o $dst_dir/iu_composer.o $root_dir/../iu/iu_composer.c
gcc $cc_flags -c -o export/dwn_wavefront.o export/dwn_wavefront.c
gcc $cc_flags -o a.out main.c $ffly_objs -nostdlib
