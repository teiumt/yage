#include "dawn.h"
#include "../yarn/yarn.h"
#include "../ihc/fsl/fsl.h"
#include "../flue/common.h"
#include "../io.h"
#include "../string.h"

struct program {
	struct flue_prog prog;
};

static struct program generic_ps,tame_ps;
static struct program generic_vs,tame_vs;
static struct flue_resspec vs_res;

enum {
	_SHADER_PRIMBUF,
	_SHADER_VERTEXBUF,
	_SHADER_POOL,
	_SHADER_CONST
};
#include "../assert.h"
void static shader_link(struct program *__shader,char const *__ident,_64_u __id, _64_u __offset, _64_u __placement) {

	struct flue_resspec *res;
	_64_u type = 0;
	switch(__id) {
		case _SHADER_PRIMBUF:
			res = &vs_res;
			type = 3;
		break;
		case _SHADER_VERTEXBUF:
			res = &vs_res;
			type = 1;
		break;
		case _SHADER_POOL:
			assert(1 == 0);
		break;
		case _SHADER_CONST:
			type = 2;
		break;
		default:
			return;
	}

	flue_allot(__shader,__ident,str_len(__ident),type,__offset,__placement);
}

void static shader_init(struct program *prog, _int_u __id) {
	flue_prog_init(prog);
	prog->prog.shader_id = __id;
}


#define PLACEMENT(__x) __x

static _64_u *bank;
static struct yarn_texture zbuf;
void dwn_gint(void) {
	bank = flue_shader_bank();
	void *tmp;
	flue_xchgbanks(&tmp,bank);
	vs_res = (struct flue_resspec){
		.stride = 4*sizeof(float),
		//what shader is this for?
		.sh = FLUE_RSVS,
		.fmt = {
			.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),
			.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)
			},
		.id = FLUE_RS_VERTEX
	};


	shader_init(&tame_ps,0);
	shader_init(&tame_vs,1);
#define TAME_PS	"tame_ps.fsl"
#define TAME_VS	"tame_vs.fsl"
	fsl_init();
	flue_file_shader(&tame_ps.prog,TAME_PS,sizeof(TAME_PS));

	fsl_init();
	shader_link(&tame_vs,"position",_SHADER_VERTEXBUF,0,PLACEMENT(0));
	shader_link(&tame_vs,"trans",_SHADER_CONST,0,0/*NULL*/);
	flue_file_shader(&tame_vs.prog,TAME_VS,sizeof(TAME_VS));

	flue_prog_ready(&tame_ps,FSH(0));
	flue_prog_ready(&tame_vs,FSH(1));


///END
	shader_init(&generic_ps,2);
	shader_init(&generic_vs,3);
#define GENERIC_PS "generic_ps.fsl"
#define GENERIC_VS "generic_vs.fsl"
	fsl_init();

	flue_file_shader(&generic_ps.prog,GENERIC_PS,sizeof(GENERIC_PS));

	fsl_init();
	shader_link(&generic_vs,"position",_SHADER_VERTEXBUF,0,PLACEMENT(0));
	shader_link(&generic_vs,"trans",_SHADER_CONST,0,0/*NULL*/);
	shader_link(&generic_vs,"proj",_SHADER_CONST,64,0/*NULL*/);
	shader_link(&generic_vs,"model",_SHADER_CONST,128,0/*NULL*/);
	shader_link(&generic_vs,"normmatrix",_SHADER_CONST,192,0/*NULL*/);
	
	shader_link(&generic_vs,"normal",_SHADER_PRIMBUF,0,PLACEMENT(1));
	flue_file_shader(&generic_vs.prog,GENERIC_VS,sizeof(GENERIC_VS));

	flue_prog_ready(&generic_ps,FSH(2));
	flue_prog_ready(&generic_vs,FSH(3));

	FLUE_BTAB = tmp;

    flue_texp t;
	struct flue_fmtdesc fmt = {.depth = sizeof(float),.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32)};
	t = flue_tex_new();
	flue_tex_data(t,1920,1072,&fmt);
	flue_initz(t);
	_dwn_main.zbuffer = &zbuf;
	zbuf.priv = t;
	_dwn_main.perc_const = FLUE_CTX->priv->const_data;
}
void dwn_clear(struct yarn_texture *__tex,float r, float g, float b, float a) {
	void *priv[1] = {__tex->priv};
	flue_clear(priv,1,r,g,b,a);
}

void dwn_zbuffer(struct yarn_texture *__tx) {
	if(!__tx) {
		flue_zbuffer(NULL);
	}else
		flue_zbuffer(__tx->priv);
}
#include "../assert.h"
void static *tmp;
void dwn_draw_begin(void) {
	flue_xchgbanks(&tmp,bank);
}
static void *indx_buf;
void dwn_draw_generic(struct yarn_buffer *inxbuf,struct yarn_buffer *vtxbuf,struct yarn_buffer *normbuf) {
	indx_buf = inxbuf->priv;
	_64_u linking[] = {0,1};
	void *resources[] = {vtxbuf->priv,normbuf->priv};

	flue_resources(resources,&vs_res,1,linking);
	flue_resources(resources+1,&vs_res,1,linking+1);
	flue_shader(&generic_ps.prog,FSH(generic_ps.prog.shader_id),FSH_PIXEL_I);
	flue_shader(&generic_vs.prog,FSH(generic_vs.prog.shader_id),FSH_VETX_I);
}

void dwn_draw_tame(struct yarn_buffer *vtxbuf) {
	indx_buf = NULL;
	_64_u linking[] = {0};
	void *resources[] = {vtxbuf->priv};

	flue_resources(resources,&vs_res,1,linking);
	flue_shader(&tame_ps.prog,FSH(tame_ps.prog.shader_id),FSH_PIXEL_I);
    flue_shader(&tame_vs.prog,FSH(tame_vs.prog.shader_id),FSH_VETX_I);
}


void dwn_draw_array(_int_u __cnt, _64_u __prim) {
	flue_pushstate();//push final states to begin draw calls
	
	_64_u prim;
	switch(__prim) {
		case DWN_TRI:
			prim = FLUE_TRI3;
		break;
		case DWN_QUAD:
			prim = FLUE_QUAD;
		break;
		default:
			assert(1 == 0);
	}
	flue_draw_array(indx_buf,0,__cnt,prim);

	flue_done();
	FLUE_BTAB = tmp;

}
