#ifndef __dawn__h
#define __dawn__h
#include "../havoc/common.h"

#include "../yarn/yarn.h"
#include "../iu/iu.h"
#include "../y_int.h"
#include "../types.h"
#include "../iu/iu_scale.h"
#include "../iu/iu_shell.h"
#include "../iu/iu_btn.h"
#include "../iu/iu_toolbar.h"
#include "../iu/iu_window.h"
#include "../iu/iu_menu.h"
#include "../iu/iu_board.h"
#include "../iu/iu_label.h"
#include "../iu/iu_tuck.h"//side product
#include "../iu/iu_tabs.h"
#include "../log.h"
#include "dwn_mempool.h"
#include "../maths/vec3.h"
#define dwn_vec3 _y_vec3

struct dwn_vert;
struct dwn_face {
	struct dwn_vert *verts[6];
	struct dwn_vec3 normal;
};

struct dwn_vert{
	struct dwn_vec3 pos;	
	_64_u indx;
};

struct dwn_bunch {
	struct dwn_face *faces;
	_int_u n;
};

struct dwn_mesh{
	struct dwn_mempool vtx;
	struct dwn_mempool faces;

	/*
		linear memory bunches of faces(could just be a single face)
	*/
	struct dwn_bunch face_table[64];
	_int_u num_faces;
	_int_u num_bunches;
};

struct dwn_model{
	struct dwn_mesh *msh;
};
#include "../log.h"
struct dwn_main{
	struct log_struc logfile;
	struct iu_tabs tabs_test;
	struct iu_shell entry_shell;
	struct iu_menu context_menu;
	struct iu_btn copy_button;

	struct iu_label node_label;
	struct iu_label move_label;
	struct iu_label xs_label;
	struct iu_label ys_label;

	struct iu_btn export_button;
	struct iu_text export_path;
	struct iu_board export_board;	
	struct iu_board side_board;
	
	struct iu_scale node_scale;
	struct iu_scale move_scale;
	struct iu_scale x_scale;
	struct iu_scale y_scale;

	float xdis,ydis;

	struct iu_text text_test;
	struct iu_rect wd2_decor;
	struct iu_rect wd2_bg;
	struct iu_window wd2;
	struct iu_text text;
	struct iu_window window;
	struct iu_toolbar tools;
	struct iu_toolbar side_tools;
	struct iu_btn add_button;
	struct iu_btn sel_button;
	struct iu_btn exit_button;

	struct iu_btn file_button;
	struct iu_btn fm_exit_button;
	struct iu_btn fm_export_button;
	struct iu_btn fm_settings_button;
	struct iu_menu file_menu;

	struct dwn_cnode *nodes;
	_int_u node_cnt;
	struct log_struc log;
	struct dwn_mesh mesh;
	struct dwn_vert *front_face;
	
	void *zbuffer;
	void *perc_const;
    _flu_float trans[16];
    _flu_float proj[16];
    _flu_float model[16];

	_flu_float node_trans[16];
};
#define DWN_TRI 0
#define DWN_QUAD 1
#define dwn_printf(...) log_printf(&_dwn_main.log, __VA_ARGS__)
#define NODE_RADIUS 10
extern struct dwn_main _dwn_main;
//construction node

struct dwn_track {
	struct dwn_vec3 shape[3];
	float x,y;
};
struct dwn_cnode {
	struct dwn_face *face;
	struct dwn_vert *verts[4];

	struct dwn_track tracks[2];
	struct dwn_track *trk;
	/*okay this is abit messed up by this is relavent to the creation of the linked list, not the nodes*/
	struct dwn_cnode *next, *prev;
	void *skin;
};
void dwn_mesh_init(struct dwn_mesh *m);
void dwn_vec_copy(struct dwn_vec3 *__dst,struct dwn_vec3 *__src);
_8_s dwn_tri_intersect(struct dwn_vec3 *__v, struct dwn_vec3 *__point);
void dwn_clear(struct yarn_texture*,float,float,float,float);
void dwn_zbuffer(struct yarn_texture*);
void dwn_init(void);
void dwn_deinit(void);
void dwn_draw_begin(void);
void dwn_draw_generic(struct yarn_buffer*,struct yarn_buffer*,struct yarn_buffer*);
void dwn_draw_tame(struct yarn_buffer *vtxbuf);
void dwn_draw_array(_int_u __cnt, _64_u __prim);
struct dwn_cnode *dwn_node_new(void);
void dwn_wf_export(char const *__path);
#endif/*__dawn__h*/
