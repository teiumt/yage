#include "../dawn.h"
#include "../../afac/afac.h"
#include "../../m_alloc.h"
#include "../../ffly_def.h"
#ifndef offsetof
#define offsetof(__type, __member) \
    ((_64_u)&((__type*)0)->__member)
#endif
void dwn_wf_export(char const *__path) {
	struct afac_wf w;
	struct afac_blob *b;
	struct dwn_mesh *m;
	m = &_dwn_main.mesh;

	struct dwn_mempool *mp = &m->vtx;
	w.vdis = mp->elem_sz;
	b = m_alloc(mp->pgc*sizeof(struct afac_blob));
	_int_u i;
	i = 0;
	for(;i != mp->pgc;i++) {
		struct dwn_vec3 *vert;
		vert = b[i].v = ((_8_u*)mp->pages[i])+(sizeof(struct dwm_mphdr)+offsetof(struct dwn_vert,pos));
		b[i].len = DWN_MEMPOOL_PAGE_SIZE;
	}
	i--;
	b[i].len = mp->off&DWN_MEMPOOL_PAGE_MASK;
	w.v = b;
	w.n_v = mp->pgc;

	struct afac_blob *norms;
	norms = m_alloc(m->num_faces*sizeof(struct afac_blob));
	
	struct afac_wf_face *faces;
	faces = m_alloc(m->num_faces*sizeof(struct afac_wf_face));
	_int_u j, k = 0;
	j = 0;
	i = 0;
	for(;i != m->num_bunches;i++) {
		j = 0;
		struct dwn_bunch *bn = m->face_table+i;
		for(;j != bn->n;j++) {
			struct dwn_face *face = bn->faces+j;
			struct afac_wf_face *f = faces+k;
			f->n = 4;

			norms[k].v = &face->normal;
			norms[k].len = 1;
			f->vn_idx[0] = k;
			f->vn_idx[1] = k;
			f->vn_idx[2] = k;
			f->vn_idx[3] = k;
			f->v_idx[0] = face->verts[0]->indx;
			f->v_idx[1] = face->verts[1]->indx;
			f->v_idx[2] = face->verts[3]->indx;
			f->v_idx[3] = face->verts[2]->indx;
			k++;
		}
	}
	w.vn = norms;
	w.n_vn = m->num_faces;
	w.vf = faces;
	w.n_vf = m->num_faces;
	afac_wf_export(&w,__path);
}
