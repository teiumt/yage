# ifndef __f__avs__h
# define __f__avs__h
# include "y_int.h"
#define F_AVS_PAGE_SHFT 4
#define F_AVS_PAGE_SIZE (1<<F_AVS_PAGE_SHFT)
#define F_AVS_PAGE_MASK (F_AVS_PAGE_SIZE-1)
/*
	alleviate strcture
*/

struct f_avs {
	void(*get)(_ulonglong, _64_u, void*, _int_u);
	_ulonglong arg;
};

struct f_avs_page {
	_64_u offset;
};
# endif /*__f__avs__h*/
