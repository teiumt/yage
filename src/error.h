# ifndef __error__h
# define __error__h
#define Y_SUCC 0
#define Y_FAIL -1

#define iferr(__x) if((__x) == Y_FAIL)
#define ifok(__x) if((__x) == Y_SUCC)
# endif /*__error__h*/
