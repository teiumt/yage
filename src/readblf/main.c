#include "../y_int.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "../blf.h"

void static outwa(struct blf_wa *__w, _int_u __n) {
	_int_u i = 0;
	struct blf_wa *w;
	if (__n>=0xffff) {
		return;
	}
	for(;i != __n;i++) {
		w = __w+i;
		printf("|%c%u: WA: %u\n", i == __n-1?'_':' ',i,w->adr);
	}
}
/*
	the gnu implementation of this is a pile of shit
	realy? no length return, what the fuck are they thinking?
*/
_int_u str_cpy(char const *__dst, char const *__src) {
	_int_u n;
	n = strlen(__src);
	memcpy(__dst,__src,n+1);
	return n;
}

char const static *smflags[64] = {
	NULL,
	"UD",
	"SECTION"
};

char const static* rlflags[64] = {
	NULL,
	NULL,
	"PART",
	"R8",
	"R16",
	"R32",
	"R64"
};
void static
flagsstr(char *__buf, _64_u __flags, char const **__table) {
	_int_u i = 0;
	while(__flags != 0) {
		if (__flags&1 && __table[i] != NULL) {
			__buf+=str_cpy(__buf,__table[i]);
			*__buf = ',';
			__buf++;
		}
		i++;
		__flags>>=1;
	}
	*__buf = '\0';
}
static char const *valtbl[] = {
	"JUMP",
	"CALL",
	"BSPACE"
};
int main(int __argc, char const *__argv[]) {
    int fd;
	if (__argc<2) {
        printf("please provide file,\n\tusage: readremf [file].\n");
        return -1;
    }

    if (access(__argv[1], F_OK) == -1) {
        printf("file doesen't exist.\n");
        return -1;
    }

    if ((fd = open(__argv[1], O_RDONLY)) == -1) {

        return -1;
    }

	struct blf_hdr h;
	read(fd, &h, 512);
	_int_u nsy = h.nsy;
	_int_u nfr = h.nfr;
	_int_u nw = h.nw;
	_int_u nrl = h.nr;
	
	printf("BLF-HEADER: syt{%x:%u}, ft{%x:%u}, wat{%x:%u}, rl{%x:%u}, program{%u- %u}, ENTRY{%s-%u}, ST_ENTS: %u:%u\n",
		h.syt, nsy, h.ft, nfr, h.wat, nw, h.rl, nrl, h.start, h.size, h.entry == BLF_NULL?"(NULL)":"PRESENT",h.entry == BLF_NULL?0:h.entry,h.st_ents,h.st_n);
	struct blf_smdata *sm;
	struct blf_sym *st;
	struct blf_frag *ft;
	struct blf_wa *wt;
	struct blf_reloc *rt;
	struct blf_section *sects;

	sm = malloc(h.smdn*sizeof(struct blf_smdata));
	pread(fd,sm,h.smdn*sizeof(struct blf_smdata),h.smd);
	st = malloc(h.sytsz);
	pread(fd,st,h.sytsz,h.syt);
	ft = malloc(h.ftsz);
	pread(fd,ft,h.ftsz,h.ft);
	wt = malloc(h.wsz);
	pread(fd,wt,h.wsz,h.wat);
	rt = malloc(h.rsz);
	pread(fd,rt,h.rsz,h.rl);
	sects = malloc(h.nsec*sizeof(struct blf_section));
	pread(fd,sects,h.nsec*sizeof(struct blf_section),h.sec);
	
	_8_u *stt = malloc(h.st_size);
	pread(fd,stt,h.st_size,h.st_ents);

	printf("STRING{offset: %u, size: %u-bytes}\n",h.st_ents,h.st_size);

	struct blf_sym *s;
	struct blf_frag *f;
	struct blf_reloc *r;
	struct blf_smdata *sd;
	char cbuf[256];	

#define smdshow(sd) "SMDx%u: sec: %u, value: %u, flags: %u{%s}, domain: %u, start-end{%u :%u},%u->%u\n",sd->x,sd->sec,sd->value,sd->flags,cbuf,sd->domain,sd->f_start,sd->f_end,sd->f_is,sd->f_ie
	_int_u i;
	i = 0;
	for(;i != h.smdn;i++) {
		sd = sm+i;
		flagsstr(cbuf,sd->flags,smflags);
		printf(smdshow(sd));

	}

	if (h.entry != BLF_NULL) {
		sd = ((_8_u*)sm)+h.entry;
		flagsstr(cbuf,sd->flags,smflags);
		printf(smdshow(sd));

	}

	i = 0;
	for(;i != nsy;i++) {
		s = st+i;
		_8_u *str = stt+s->name;
		struct blf_stent *sdsc = str;
		char *_str = str+sizeof(struct blf_stent);
		sd = sm+s->value;
		_int_u j;
		j = 0;
		for(;j != s->n;j++) {
			flagsstr(cbuf,sd->flags,smflags);
			printf("| -" smdshow(sd));
			sd++;
		}
		printf("\\ SYMB{%x} value: %u '%s{%u'%c', *%u}, N: %u\n\n",i*sizeof(struct blf_sym),s->value,_str,s->name,*_str,sdsc->len, s->n);
	}

	i = 0;
	for(;i != nfr;i++) {
		f = ft+i;
		printf("FRAG%u: adr: %u, off: %u, size: %u, over{%x, %u}, WA{%u, %u}, words: %u\n", i,f->adr, f->off, f->size, f->over, f->nov, f->wa, f->nw, f->words);
		outwa(wt+f->wa, f->nw);
	}
	printf("#####\n");
	outwa(wt, nw);

	i = 0;
	for(;i != nrl;i++) {
		r = rt+i;
		flagsstr(cbuf,r->flags,rlflags);
		printf("RELOC: dis: %d, where: %u, frag: %u, wa: %u, w0: %u, value: %s:%u, flags: %u{%s}\n", r->dis, r->where, r->f, r->w, r->w0, r->value == 255?"( NULL )":valtbl[r->value],r->value,r->flags,cbuf);
	}

	struct blf_section *sec;
	i = 0;
	for(;i != h.nsec;i++) {
		sec = sects+i;
		printf("SECTION%u: F{%u, %u}, W{%u, %u}, R{%u, %u}.\n", sec->idx, sec->ft,sec->nfr,sec->wat,sec->nw,sec->rl,sec->nr);
	}

	free(stt);
	close(fd);
}
