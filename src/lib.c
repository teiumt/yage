# include "lib.h"
# include <stdarg.h>
//# include "rand.h"
# include "linux/stat.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
#include "io.h"
#include "string.h"
#include "file.h"
#include "m_alloc.h"
/*_32_s ff_smktemp(char *__tmpl) {
	char *p = __tmpl;
	while(*p++ != '.');
	
	_32_u no = ffgen_rand32l();
	while(*p != '\0') {
		*(p++) = 'a'+(no&0xf);	
		no>>=4;
	}

	return open(__tmpl, O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
}
*/
#define _2pw0			1
#define _2pw1			2
#define _2pw2			3
#define _2pw3			4
#define _2pw4			5
#define _2pw5			6
#define _2pw6 		7
#define _2pw7			8
//power2 rounding table
_8_u rnd_pow2table[256]={
	0xff,//0
	_2pw0,//1
	
	_2pw1,//2
	_2pw1,//3
	
	_2pw2,//4
	_2pw2,//5
	_2pw2,//6
	_2pw2,//7

	//third power
	//8    9    10    11    12    13    14    15
	_2pw3,_2pw3,_2pw3,_2pw3,_2pw3,_2pw3,_2pw3,_2pw3,
	
	//fourth power
	//16   17    18    19    20    21    22    23    24    25    26    27    28    29    30    31
	_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,_2pw4,

	//fith power
	//32   33    34    35    36    37    38    39    40    41    42    43    44    45    46    47
	_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,
	//48   49    50    51    52    53    54    55    56    57    58    59    60    61    62    63
	_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,_2pw5,

	//six power
	//64   65    66    67    68    69    70    71    72    73    74    75    76    77    78    79
	_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,
	//80   81    82    83    84    85    86    87    88    89    90    91    92    93    94    95
	_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,
	//96   97    98    99    100   101   102   103   104   105   106   107   108   109   110   111
	_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,
	//112  113   114   115   116   117   118   119   120   121   122   123   124   125   126   127
	_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,_2pw6,
	
	//seven power
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,
	_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7,_2pw7
};

_32_u pw2_ranking(_32_u __num) {
	if(!__num)return 0;
	if((__num&(0xff<<24))>0){
		return rnd_pow2table[__num>>24]+24;
	}else
	if((__num&~(0xff<<16))>0){
		return rnd_pow2table[__num>>16]+16;
	}else
	if((__num&~(0xff<<8))>0){
		return rnd_pow2table[__num>>8]+8;
	}else{
		return rnd_pow2table[__num];
	}
}
//string number extraction
void _assert(char const *__file, _int_u __line, char const *__func, char const *__mesg,...) {
	printf("assert failure at %s:%u in function %s, ", __file,__line, __func);
	va_list args;
  va_start(args,__mesg);
  vprintf(__mesg,args);
  va_end(args);
	ffly_fdrain(_ffly_out);
	abort();
}

_int_u snumext(char *__buf, char *__start) {
	char c;
	_int_u i;
	i = 0;
	c = *__start;
	while(c>='0'&&c<='9') {
		__buf[i] = c;
		i++;
		c = *++__start;
	}
	return i;
}


_int_u sextract(char *__buf, char *__start) {
	char c;
	_int_u i;
	i = 0;
	c = *__start;
	while((c>='a'&&c<='z')||(c>='A'&&c<='Z')) {
		__buf[i++] = c;
		c = *++__start;
	}
	return i;
}

_int_u extractupto(char *__buf, char *__start, char __end) {
	_int_u i;
	i = 0;
	char c;
	c = *__start;
	while(c != __end) {
		__buf[i++] = c;
		c = *++__start;	
	}
	return i;
}

/*
	__buf = buf+__len

	trek on a backwards path till be reach a breakage charicter
*/
_int_u trekbackto(char *__buf, char __char) {
	_int_u i;
	i = 0;
	for(;;) {
		i++;
		if((__buf-i)[0] == __char) {
			break;
		}
	}
	return i;
}
/*
	map out all characters of __char
	and record there index.
*/
_int_u locofchar(char *__buf,_int_u *__map, char __char) {
	_int_u i;
	i = 0;
	_int_u idx = 0;
	for(;__buf[i] != '\0';i++) {
		if(__buf[i] == '.') {
			__map[idx++] = i;
		}
	}
	return idx;

}

/*
	return the index of the last known character equal to __char
*/
_int_u locofchar_l(char *__buf,char __char) {
	_int_u i;
	i = 0;
	_int_u ret = 0;
	for(;__buf[i] != '\0';i++) {
		if(__buf[i] == __char) {
			ret = i;
		}
	}
	return ret;

}

_int_u strchrl(char *__buf,char __char){
	_int_u i;
	i = 0;
	for(;__buf[i] != '\0';i++) {
		if(__buf[i] == __char) {
			return i;
		}
	}
	return 0;
}

_int_u harvest(char *buf,char *start,_16_u *__map,_16_u __mask) {
	char c;
	_int_u i;
	i = 0;
	while((__map[c = start[i]]&__mask)>0){
		buf[i] = c;
		i++;
	}
	return i;
}

char* harvest_s(char *buf,char *start,_16_u *__map,_16_u __mask,_int_u *__len){
	_int_u
	len = harvest(buf,start,__map,__mask);
	if(!len)
		return NULL;
	char *ret;
	mem_dup(&ret,buf,len+1);
	ret[len] = '\0';
	*__len = len;
	return ret;
}


void* cmalloc(_int_u __size,_16_u __block,_64_u __val){
	_8_u *r;
	r = m_alloc(__size);

	_int_u i;
	i = 0;
	for(;i != __size;){
		switch(__block){
			case 0:
				*(_8_u*)(r+i) = __val;
			break;
			case 1:
				*(_16_u*)(r+i) = __val;
			break;
			case 2:
				*(_32_u*)(r+i) = __val;
			break;
			case 3:
				*(_64_u*)(r+i) = __val;
			break;
		}

		i+=1<<__block;
	}
	return r;
}

void str_rmc(char *str,char *d,char *s,_int_u n){
	/*
		256 byts may seem like alot but its only 32-qwords
	*/
	char map[256];
	mem_set(map,'\0',25);
	_int_u i;
	i = 0;
	for(;i != n;i++){
		map[s[i]] = d[i];
	}

	char c;
	i = 0;
	for(;(c = str[i]) != '\0';i++){
		if(map[c] != '\0'){
			str[i] = map[c];
		}
	}
}

void str_rplc(char *str,char t,char r){
	_int_u i;
	i = 0;
	char c;
	for(;(c = str[i]) != '\0';i++){
		if(c == t){
			str[i] = r;
		}
	}
}

void str_cat(char *dst,char *src){
	_int_u l;
	l = str_len(dst);
	_int_u i;
	i = 0;
	char c;
	for(;(c = src[i]) != '\0';i++){
		dst[l+i] = c;
	}
	dst[l+i] = '\0';
}

void filesave(char const *__file,void *__buf,_int_u __size){
	struct file f;
	bopen(&f,__file,_O_WRONLY|_O_TRUNC|_O_CREAT,_S_IRUSR|_S_IWUSR);
	bwrite(&f,__buf,__size);
	bclose(&f);
}

void fileload(char const *__file,void *__buf,_int_u __size){
	struct file f;
	bopen(&f,__file,_O_RDONLY|_O_TRUNC|_O_CREAT,_S_IRUSR|_S_IWUSR);
	bread(&f,__buf,__size);
	bclose(&f);
}
