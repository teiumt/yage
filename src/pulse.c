# include "y_int.h"
# include "ffly_def.h"
# include "system/io.h"
# include "msg.h"
# include "tmu.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_PULSE)
/*
	repurpose for bog sched
	
	simple a simple sched for bog
*/

void(*ffly_pulse)(_64_u) = NULL;
struct entry {
	void(*func)(void);
	_64_u rate;
	_64_u cur;
	char const *codename;
};

void static
test(void) {
}

static struct entry entries[] = {
	{test, TIME_FMS(1000), 0, "TEST"}
};

#define WAITFOR 0xfff
#define N 1
_64_u ntick = 0;
void static
pulse(_64_u __scik) {
	struct entry *ent = entries;
	struct entry *end = entries+N;

	while(ent != end) {
		if (__scik >= ent->cur+ent->rate) {		
			if (!((ntick&WAITFOR)^WAITFOR)) {
			}

			ent->func();
			ent->cur = __scik;
		}
		ent++;
	}
	ntick++;
}

void ffly_pengage(void) {
	ffly_pulse = pulse;	
}

void ffly_pdisengage(void) {
	ffly_pulse = NULL;
}
