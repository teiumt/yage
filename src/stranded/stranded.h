# ifndef __stranded__h
# define __stranded__h
#include "../y_int.h"
#include "../havoc/common.h"
#include "../havoc/renderer.h"
#include "../havoc/scene.h"
#include "../havoc/model.h"
#include "../havoc/mesh.h"
#include "../havoc/subs.h"
extern struct h_scene main_menu;
void sd_load_models(void);

struct sd_static_object {
	h_modelp model;
	h_subsisp subs;
};
struct sd_common {
	h_modelp container;
	h_modelp t90a_tank;
	h_modelp donut;
	h_modelp house;
	struct h_context *ctx;
};

h_subsisp sd_static_object(void*);
extern struct sd_common sd_com;

# endif /*__stranded__h*/
