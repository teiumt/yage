# include "../y_int.h"
# include "../types.h"
# include "../engine.h"
# include "../havoc/common.h"
# include "../io.h"
# include "../string.h"
# include "../time.h"
# include "../m_alloc.h"
# include "../havoc/font.h"
# include "../havoc/ui/text.h"
# include "../havoc/dn.h"
# include "../strf.h"
# include "../m.h"
# include "../maths.h"
static fontp f;

ui_textp text;
_flue_float ident[16] = {
	1, 0, 0, 0,
	0, 1, 0, 0,
	0, 0, 1, 0,
	0, 0, -1, 1
};

#define T_X 20
#define T_Y 80
static _flu_float v[(T_X*T_Y)*15*2];
void static 
gen_terrain(void) {
	_int_s x, y;
	y = 0;
	_flu_float *p;
	p = v;
	for(;y != T_Y;y++) {
		x = 0;
		for(;x != T_X;x++) {
			p[0] = .5+x;
			p[1] = .5;
			p[2] = -(1+y);

			p[3] = -.5+x;
			p[4] = .5;
			p[5] = -(1+y);

			p[6] = -.5+x;
			p[7] = .5;
			p[8] = -y;

		
			p[9] = 0;
			p[10] = 0;
			p[11] = 0;
			p[12] = 1;
			p[13] = 1;
			p[14] = 1;

			p+=15;
			p[0] = -.5+x;
			p[1] = .5;
			p[2] = -y;//-(1+y);

			p[3] = .5+x;
			p[4] = .5;
			p[5] = -y;

			p[6] = .5+x;
			p[7] = .5;
			p[8] = -(1+y);

		
			p[9] = 1;
			p[10] = 0;
			p[11] = 0;
			p[12] = 0;
			p[13] = 0;
			p[14] = 1;

			p+=15;
		}
	}
}

static texp tx;
HV_INIT {
	printf("havoc init.\n");
return;
	h_meshp m;
	m = h_mesh_new();

	h_modelp md;
	md = h_model_new();
	md->m = m;

	_flu_float static vtx[] = {
		0, 0, 0,
		1, 0, 0,
		0, 1, 0
	};

	_flu_float static uv[] = {
		0, 0,
		1, 0,
		0, 1
	};

	m->n = 1;
	m->vtx = vtx;
	m->uv = uv;
	tx = tex_from_file("test.data");
	md->tx = tx;
	FLUE_CTX->priv->m.tp[0] = tx;
	FLUE_CTX->priv->m.ntex = 1;
	f = font_load();
	_h_is.mb = flue_tex_new(NULL, 0, 0, 512, 512);
	_h_is.mb->bits = FLUE_TEX_MMAP;
	_h_is.tx_map = _h_is.mb->mmap = m_alloc(512*512*4);

	text = text_creat(DN_ATTACH);
	char const str[] = "hello to the fucking world\nthis is the havoc engine enjoy your stay";
	struct ui_text_struc set = {
		{str, sizeof(str)},
		f,
		UI_TX_SETTEXT|UI_TX_SETFONT
	};

	text->x = 0.5;
	text->y = 0.1;
	text_alter(text, &set);
//	gen_terrain();


//	flue_frustum(0, 20, 0, 0);
//	flue_perspective(trad(45*0.5), 0);
//	flue_mbase();
}

HV_DEINIT {

}

_flu_float static test = 10;

void flush(void);
HV_TICK {
	_h_is.ct->can.tex[0] = tx;
	_h_is.ct->can.ntx = 1;
//	flue_perspective(trad(test*0.5), 0);
//	flue_lookat(5, -1.5, test, 5, -7.5, 80, 0, 1, 0);
//	flue_matrix();
//	h_tgl3(v, T_X*T_Y);
//	test-=0.5;




	flush();
//	doze(0, 5);
	return -1;
}

_err_t main(int __argc, char const *__argv[]) {
	havoc_start();
}
