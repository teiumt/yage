# include "stranded.h"
#include "../havoc/mesh.h"
#include "../havoc/model.h"
#include "../clay.h"
#include "../io.h"
#include "../string.h"
#include "../havoc/common.h"
#include "../havoc/renderer.h"
#include "../havoc/camera.h"
#include "../maths.h"
#include "../havoc/terrain.h"
#include "../strf.h"
//#include "../iu/iu.h"
//#include "../iu/iu_btn.h"
struct sd_common sd_com;
_err_t main(int __argc, char const *__argv[]) {
	struct clay clay;
	clay_init(&clay);
	clay_load(&clay, "settings.clay");
	clay_read(&clay);

	void *screen;
	screen = clay_get("screen", &clay);
	int width, height;
	width = *(_64_u*)clay_array(clay_tget("width",screen));
	height = *(_64_u*)clay_array(clay_tget("height",screen));

	printf("screen, width: %u, height: %u\n", width, height);
	clay_de_init(&clay);
	_h_is.width = width;
	_h_is.height = height;
	_h_is.tgt_fps = 40;
	havoc_start();
}
static _8_s _rtexit = -1;
/*
	allows us to intersept messages
*/
static _8_s dsp_stat = -1;

void _h_msg(struct h_msg *__m) { 
	if (__m->value == H_PRESS) { 
		switch(__m->code) { 
			case H_KEY_ESC: 
				_rtexit = 0; 
			break; 
			case HY_F4:
				if (dsp_stat<0)
					dsp_stat = 0;
				else 
					dsp_stat = -1;
			break;
		}
	}
}
struct h_hbox btn_hitbox;
struct h_context hct;
struct h_terrain *trn;
static void *container_texture;
static void *air_conditioner_texture;
static void *t90a_tank_texture;
static void *cursed_texture;
static void *house_texture;
static void *plant_variety1;
static void *plant_variety2;


struct{
	void *_5t_plane;
	void *ammo;
	void *details;
	void *kaba;
	void *kabi;
	void *motor1;
	void *opel_plane;
	void *pritsche;
	void *raeder;
	void *rahmen;
	void *tools;
}faun;

void static load_faun(void){
	faun._5t_plane		= tex_from_file("../../assets/faun/5t_plane.ppm",TEX_RGB);
	faun.ammo				= tex_from_file("../../assets/faun/ammo.ppm",TEX_RGB);
	faun.details		= tex_from_file("../../assets/faun/details.ppm",TEX_RGB);
	faun.kaba				= tex_from_file("../../assets/faun/kaba.ppm",TEX_RGB);
	faun.kabi				= tex_from_file("../../assets/faun/kabi.ppm",TEX_RGB);
	faun.motor1			= tex_from_file("../../assets/faun/motor1.ppm",TEX_RGB);
	faun.opel_plane	= tex_from_file("../../assets/faun/opel_plane.ppm",TEX_RGB);
	faun.pritsche		= tex_from_file("../../assets/faun/pritsche.ppm",TEX_RGB);
	faun.raeder			= tex_from_file("../../assets/faun/raeder.ppm",TEX_RGB);
	faun.rahmen			= tex_from_file("../../assets/faun/rahmen.ppm",TEX_RGB);
	faun.tools			= tex_from_file("../../assets/faun/tools.ppm",TEX_RGB);
}

h_subsisp static spawn_faun(void){
	h_subsisp s;

	s = h_subsis();
	s->data = h_static_subsis;
	hv_subsis_static_model(s);

	/*
		look we use this to usage can be easer.
		keeping to protocals is issuematic we could just do ++ or array like this.
	*/
	_16_u layout[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
	char const* comps[] = {"boxen","verdeck","motor","tools","plane1","pritsche","cargo","raeder","kabi","kaba","kf","rahmen","detail","detail_mun","detail_fuel","plane3",NULL};
	struct hv_mdlpar conf = {
		.bits = HV_MDLFILE,
		.path = "../../assets/faun/faun.obj",
		.chosen = comps,
		.layout = layout
	};
	h_model_config(
		&s->render.model,
		&conf
	);
	s->render.model.obj[0].tx = faun.pritsche;		//boxen
	s->render.model.obj[1].tx = faun.opel_plane;	//verdeck
	s->render.model.obj[2].tx = faun.motor1;			//motor
	s->render.model.obj[3].tx = faun.tools;				//tools
	s->render.model.obj[4].tx = faun._5t_plane;		//plane1
	s->render.model.obj[5].tx = faun.pritsche;		//pritsche
	s->render.model.obj[6].tx = faun.ammo;				//cargo
	s->render.model.obj[7].tx = faun.raeder;			//raeder
	s->render.model.obj[8].tx = faun.kabi;				//kabi
	s->render.model.obj[9].tx = faun.kaba;				//kaba
	s->render.model.obj[10].tx = faun.kaba;				//kf
	s->render.model.obj[11].tx = faun.rahmen;			//rahmen
	s->render.model.obj[12].tx = faun.details;		//detail
	s->render.model.obj[13].tx = faun.details;		//detail_mun
	s->render.model.obj[14].tx = faun.details;		//detail_fuel
	s->render.model.obj[15].tx = faun.opel_plane;	//plane3	
	return s;
}

static h_subsisp container;
static h_subsisp tank;
static h_subsisp donut;
static h_subsisp house;
static h_subsisp _faun;
static char buffer[256];
void static connect_player_bindings(void){
	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_W),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_W),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_E),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_E),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_A),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_A),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_S),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_S),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_D),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_D),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_LEFT),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_RIGHT),hv_player_control,donut);
	
	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_UP),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_DOWN),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_SPACE),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_SPACE),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PRESS,H_KEY_LEFTSHIFT),hv_player_control,donut);
	hv_sigconnect(MESG_KEY(H_RELEASE,H_KEY_LEFTSHIFT),hv_player_control,donut);

	hv_sigconnect(MESG_KEY(H_PTR_M,MSG_CODENULL),hv_player_control,donut);
}

void static contact(h_subsisp __s){
	hv_printf("%p-intrusion.\n",__s);
}
//static struct iu_btn test_button;
HV_INIT {
//	iu_initsome();
	_h_is.world.phy_rate = 100;
	_h_is.ct = &hct;
	
	h_ident(hct.trans);
	h_ident(hct.proj);
	_h_is._ctx = &hct;
	trn = h_terrain_test();

	container_texture				= tex_from_file("../../assets/container.ppm",TEX_RGB);
	air_conditioner_texture	= tex_from_file("../../assets/air_conditioner.ppm",TEX_RGB); 
	t90a_tank_texture				= tex_from_file("../../assets/t90a_tank.ppm",TEX_RGB);
	cursed_texture					= tex_from_file("../../assets/human2.ppm",TEX_RGB);
	house_texture						= tex_from_file("../../assets/house.ppm",TEX_RGB);
	plant_variety1					= tex_from_file("../../assets/plant_variety1.ppm",TEX_RGB);
	plant_variety2					= tex_from_file("../../assets/plant_variety2.ppm",TEX_RGB);

	load_faun();

	landscape = trn;
/*
	sd_com.container->objs[1].tx = container_texture;
	sd_com.container->objs[0].tx = air_conditioner_texture;	
	
	sd_com.t90a_tank->objs[0].tx = t90a_tank_texture;
	
	sd_com.donut->objs[0].tx = cursed_texture;

	sd_com.house->objs[0].tx = house_texture;
	sd_com.house->objs[1].tx = plant_variety1;
	sd_com.house->objs[2].tx = plant_variety2;
*/
	sd_com.ctx = &hct;
	
	container		= h_subsis();
	tank				= h_subsis();
	donut				= h_subsis();
	house				= h_subsis();

	container->flags |= SUBS_CONTACT_SUBS|SUBS_PHYSICS;
	container->share |= SUBS_SHARE_VEHICLE;
	donut->flags |= SUBS_CONTACT_SUBS|SUBS_PHYSICS;

	donut->mesg = contact;
	donut->player.x = 0;
	donut->player.y = 0;
	donut->player.z = 0;
	connect_player_bindings();

	container->data = h_static_subsis;
	tank->data			= h_static_subsis;
	donut->data			= h_static_subsis;
	house->data			= h_static_subsis;

	hv_subsis_static_model(container);
	hv_subsis_static_model(tank);
	hv_subsis_static_model(donut);
	hv_subsis_static_model(house);

	_16_u layout[] = {0,1};
	char const* container_comps[] = {"air_conditioning","container",NULL};
	struct hv_mdlpar container_conf = {
		.bits = HV_MDLFILE,
		.path = "../../assets/container.obj",
		.chosen = container_comps,
		.layout = layout
	};
	h_model_config(
		&container->render.model,
		&container_conf
	);

 	char const* donut_comps[] = {"Sphere",NULL};
	struct hv_mdlpar donut_conf = {
		.bits = HV_MDLFILE,
		.path = "../../assets/donut.obj",
		.chosen = donut_comps,
		.layout = layout
	};
	h_model_config(
		&donut->render.model,
		&donut_conf
	);

 	char const* tank_comps[] = {"Plane",NULL};
	struct hv_mdlpar tank_conf = {
		.bits = HV_MDLFILE,
		.path = "../../assets/t90a_tank.obj",
		.chosen = tank_comps,
		.layout = layout
	};
	h_model_config(
		&tank->render.model,
		&tank_conf
	);

	donut->render.model.obj[0].tx = cursed_texture;

	container->render.model.obj[0].tx = air_conditioner_texture;
	container->render.model.obj[1].tx = container_texture;

	tank->render.model.obj[0].tx = t90a_tank_texture;

	_faun = spawn_faun();
	_faun->flags |= SUBS_CONTACT_SUBS|SUBS_PHYSICS;
	_faun->share |= SUBS_SHARE_VEHICLE;
	_faun->phy.pos.y = -1000;
	h_subsis_link(_faun);
	h_subsis_link(container);
	h_subsis_link(tank);
	h_subsis_link(donut);
//	h_subsis_link(house);

	h_subs_view_attach(donut);
	h_subs_terrain_contact(donut,trn);
/*
	iu_btn_init(&test_button);
  test_button._w.w = 128;
  test_button._w.h = 45;
	iu_btn_with_text(&test_button,"yage");
*/
}

HV_DEINIT {
}

#include "../havoc/maths/dot.h"
#include "../havoc/maths/barycentric.h"

void render_commence(void);
# include "../maths.h"
void h_terrain(struct h_context *__ct, struct h_terrain *__trn);
#include "../flue/common.h"
_int_u static cntr = 0;
HV_TICK {
//	psp_fire_ray(&trn->psp,0,0,0,0,0,-1);
	return _rtexit;
}
/*
	this is called on render,
	
	HV_TICK and _h_draw work on diffrent threads
*/
#include "../citric/ct_font.h"
void _h_draw(void) {
	_int_u len = ffly_strf(buffer,0,"x: %d, y: %d, z: %d, fps: %u",
		(_int_s)donut->phy.pos.x,
		(_int_s)donut->phy.pos.y,
		(_int_s)donut->phy.pos.z,
		(_int_u)_h_is.cps
	);

//	ct_textdraw(buffer,len,0,0,1,0,0,1);

//	float vt[] = {0,0,512,512};
//	h_rect(_h_is._ctx,vt,1,0,0,1);

	ct_textdraw(buffer,len,0,0,1,0,0,1);

//	test_button._w.draw(&test_button);
}


void _iu_msg(void) {}
void _iu_tick(void) {}
void _iu_init(void) {}
void _iu_deinit(void) {}

