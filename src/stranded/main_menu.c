# include "stranded.h"
# include "../havoc/common.h"
# include "../havoc/renderer.h"
# include "../havoc/ui/btn.h"
# include "../m_alloc.h"
# include "../io.h"
# include "../havoc/landscape.h"
# include "../havoc/tex.h"
# include "../flue/common.h"
struct {
	struct ui_btn *exit, *play;
	ui_textp title;
	struct h_msgbucket *msgs;
} here;
#define D(__c) __c*(1./255.)
struct font_state static fs;
struct font_state static fsx;
struct font_state static fsy;
struct font_state static fsz;

static struct ui_btn_strc btn[] = {
{
	0.5, 0.5, 0.5, 0.07, 1, 1,
	{
		{1,1,1,1},
		{0,0,0,0}
	},
	{D(181), D(137), D(16), 1},
	"exit",
	sizeof("exit")-1
},
{
	0.5, 0.25, 0.5, 0.07, 1, 1,
	{
		{1,1,1,1},
		{0,0,0,0}
	},
	{D(181), D(137), D(16), 1},
	"play",
	sizeof("play")-1
}
};

enum {
	_play,
	_exit
};
# include "../havoc/body.h"
static h_bodyp player;
struct h_landscape ls;
struct h_camera cam;
# include "../maths.h"
void static on_entry(void) {
	player = h_body();
	player->pos.x = 0;
	player->pos.y = -1;
	player->pos.z = 0;
	h_ident(cam.ident);
	h_frustum(cam.ident, 0, 200, 0, 0);
	h_perspective(cam.ident, trad(45), 0);
	_h_rs.cam = &cam;
	ls.tx = tex_from_file("test.data");
	ls.t = h_terrain_test();
	here.msgs = h_mbkt_new();
	font_state_init(&fs);
	font_state_init(&fsx);
	font_state_init(&fsy);
	font_state_init(&fsz);
/*
	ui_textp title;
	here.title = title = text_creat(0);
	char const str[] = "stranded - v0.00";
	struct ui_text_struc set = {
		{str, sizeof(str)-1},
		_str.f,
		UI_TX_SETTEXT|UI_TX_SETFONT
	};

	title->bits = FNT_HALIGN;
	title->x = 0.5;
	title->y = 0.1;
	title->colour.r = 1;
	title->colour.g = 1;
	title->colour.b = 1;
	title->colour.a = 1;
	title->bg.r = 0;
	title->bg.g = 0;
	title->bg.b = 0;
	title->bg.a = 0.5;
	text_alter(title, &set);
	struct ui_btn *exit;
	here.exit = exit = ui_btn_new();
	ui_btn_load(exit, btn, _str.f);
	here.exit->bk = here.msgs;
	here.exit->value = _exit;

	struct ui_btn *play;
	here.play = play = ui_btn_new();
	ui_btn_load(play, btn+1, _str.f);
	here.play->bk = here.msgs;
	here.play->value = _play;
*/
	cam.zoom = 1;
}


void static on_exit(void) {
}

_flue_float static vtx[4] = {
	0, 0,
	1, 1,
};
static struct h_vec3 ch = {0,0,0};
static _8_s _rtexit = -1;
# include "../strf.h"
static _flue_float ii = 0;
# include "../havoc/things.h"
_8_s static tick(void) {
	if (!tarr_contact(ls.t, player)) {
//		ffly_fprintf(ffly_log, "contact.\n");
		ii+=0.01;

	}
//	ffly_fprintf(ffly_log, "%f, %f, %f.\n", player->pos.x, player->pos.y, player->pos.z);
	cam.pos.x = player->pos.x;
	cam.pos.y = player->pos.y-ii;
	cam.pos.z = player->pos.z;
	cam.at.x = 20;
	cam.at.y = 20;
	cam.at.z = 4096;
	ii-=0.01;
	h_camera_start(&cam);
	h_camera_rotate(&cam, &ch);
	h_translate(cam.m, cam.pos.x, cam.pos.y, cam.pos.z);
//	h_camera_lookat(&cam, cam.pos.x, cam.pos.y, cam.pos.z,
//		cam.at.x, cam.at.y, cam.at.z, 0, 1, 0);
	struct h_mbm *m, *bm;
	m = here.msgs->m;
	while(m != NULL) {
		m = (bm = m)->next;
		/*if (bm->id == UI_BTN) {
			switch(bm->code) {
				case UI_BTN_PRESS: {
					if (bm->value == _play) {

					} else if (bm->value = _exit) {
						printf("goodbye.\n");
				//		return 0;
					}
					break;
				}
			}
		}*/
		m_free(bm);
	}
	here.msgs->m = NULL;
	struct h_colour c;
	c.r = D(255);
	c.g = D(255);
	c.b = D(255);
	c.a = 1;
	h_crect(vtx, &c);

//	ui_btn_draw(here.exit);
//	ui_btn_draw(here.play);
//	ui_text_draw(here.title);

	char buf[128];
	*(_64_u*)buf = 0;
	*(_64_u*)(buf+8) = 0;
	*(_64_u*)(buf+16) = 0;
	*(_64_u*)(buf+24) = 0;
	*(_64_u*)(buf+32) = 0;
	*(_64_u*)(buf+40) = 0;
	_ffly_nds(buf, (_int_u)_h_is.cps);
	buf[2] = '-';
	buf[5] = '.';
	_ffly_nds(buf+3, _h_is.delay>>27);
	_ffly_nds(buf+6, (_h_is.delay&~(7<<27))>>(27-10));
#define XBUF (buf+16)
#define YBUF (buf+32)
#define ZBUF (buf+48)
	XBUF[0] = 'x';
	YBUF[0] = 'y';
	ZBUF[0] = 'z';
	XBUF[1] = player->pos.x<0?'-':' ';
	YBUF[1] = player->pos.y<0?'-':' ';
	ZBUF[1] = player->pos.z<0?'-':' ';

	_int_u _x, _y, _z;
	_x = (_int_u)(player->pos.x<0?-player->pos.x:player->pos.x);
	_y = (_int_u)(player->pos.y<0?-player->pos.y:player->pos.y);
	_z = (_int_u)(player->pos.z<0?-player->pos.z:player->pos.z);


	_ffly_nds(XBUF+2, _x);
	_ffly_nds(YBUF+2, _y);
	_ffly_nds(ZBUF+2, _z);

	struct h_colour a = {1,1,1,1}, b = {0,0,0,0};
	font_draw(_str.f, &fs, buf, 16, 0, 0, 0/*bits*/, 0.2, 0.03, &a, &b);

//coords
	font_draw(_str.f, &fsx, XBUF, 16, 0, 0.03, 0/*bits*/, 0.2, 0.03, &a, &b);
	font_draw(_str.f, &fsy, YBUF, 16, 0, 0.03*2, 0/*bits*/, 0.2, 0.03, &a, &b);
	font_draw(_str.f, &fsz, ZBUF, 16, 0, 0.03*3, 0/*bits*/, 0.2, 0.03, &a, &b);

	h_landscape(&ls);	

	flush();
	return _rtexit;
}

struct h_scene main_menu = {
	on_entry,
	on_exit,
	tick
};
