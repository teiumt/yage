# include "line.h"
# include "system/printf.h"
static char line[1024];
static char *cur = line;

# define cursorat \
	(line+((_int_u)(((_int)(cur-line))+l_cursor_pos)))

_int l_cursor_pos = 0;
char static temp;
void ffly_l_show(FF_FILE *__file) {
	temp = *cursorat;
	*cursorat = '#';
	*cur = ' ';
	ffly_fprintf(__file->rws, "\e[2K\r");
	*(cur+1) = '\0';

	ffly_fprintf(__file->rws, "~: %s", line);
	ffly_fprintf(__file->rws, "\e[%uD", ((cur-line)-(_int_u)(((_int)(cur-line))+l_cursor_pos))+1);
	ffly_fdrain(__file);
	*cursorat = temp;
}

void ffly_l_put(char __c) {
	char *p = cursorat;
	char *e = cur;
	while(e != p) {
		*e = *(e-1);
		e--;
	}

	*p = __c;
	cur++;
}

void ffly_l_del(void) {
	if (!(((_int)(cur-line))+l_cursor_pos))
		return;
	char *p = cursorat-1;
	char *e = cur;
	while(p != e) {
		*p = *(p+1);
		p++;
	}
	cur--;
}
# include "dep/mem_cpy.h"
_int_u
ffly_l_load(char *__dst) {
	_int_u l = cur-line;
	f_mem_cpy(__dst, line, l);
	return l;
}

void ffly_l_reset(void) {
	cur = line;
	l_cursor_pos = 0;
}
