# ifndef __ffly__bitfont__h
# define __ffly__bitfont__h
# include "y_int.h"
struct bf_info {
	_int_u n;
	_8_u *lm;
	_8_u *bm;
	_int_u width, height;
};

extern struct bf_info _bf_info;
void bfchar(_8_u);
# endif /*__ffly__bitfont__h*/
