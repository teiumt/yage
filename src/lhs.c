# include "lhs.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
# include "m_alloc.h"
# include "string.h"
void f_lhs_init(void) {

}

void f_lhs_de_init(void) {

}

struct f_lhs* f_lhs_alloc(_int_u __size) {
	struct f_lhs *lhs;
	lhs = (struct f_lhs*)m_alloc(sizeof(struct f_lhs));
	lhs->p = m_alloc(__size);
	return lhs;
}

void f_lhs_free(struct f_lhs *__lhs) {
	m_free(__lhs->p);
	m_free(__lhs);
}

void f_lhs_read(struct f_lhs *__lhs, void *__buf, _int_u __size, _32_u __offset) {
	mem_cpy(__buf, ((_8_u*)__lhs->p)+__offset, __size);
}

void f_lhs_write(struct f_lhs *__lhs, void *__buf, _int_u __size, _32_u __offset) {
	mem_cpy(((_8_u*)__lhs->p)+__offset, __buf, __size);
}
