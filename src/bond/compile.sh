rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd bond
ffly_objs="$ffly_objs slam.o pic.o blf.o map.o hash.o remf.o"
gcc $cc_flags -c map.c
gcc $cc_flags -c remf.c
gcc $cc_flags -c hash.c
gcc $cc_flags -c bond.c
gcc $cc_flags -c blf.c
gcc $cc_flags -c pic.c
gcc $cc_flags -c slam.c
gcc $cc_flags -o ybn main.c bond.o $ffly_objs -nostdlib
