# ifndef __ffly__bond__frag__h
# define __ffly__bond__frag__h
# include "../y_int.h"
struct frag {
	_int_u size;
	_8_u data[24];
	_8_u x_len;
	_32_u adr;
	_int_u f;
	_int m, bs;
	struct frag *next;
};

struct frag extern *fr_head;
struct frag *bond_fbn(_int_u);
struct frag* bond_fnew(_64_u, _int_u, _int_u);
void bond_fdestroy(struct frag*);
# endif /*__ffly__bond__frag__h*/
