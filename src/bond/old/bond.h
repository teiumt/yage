# ifndef __ffly__bond__h
# define __ffly__bond__h
# include "../y_int.h"
# include "../remf.h"
# include "frag.h"
#define imos(__off)\
	offset = __off
/*
	segments will be freed on output
*/
struct hash_entry {
	struct hash_entry *next;
	_8_u const *key;
	_int_u len;
	void *p;
};

typedef struct hash {
	struct hash_entry **table;
} *hashp;

typedef struct segment {
	struct segment *next;
	_8_u *p;
	_int_u offset;
	_int_u size;
	_int_u addr;
	_8_u type;
} *segmentp;

typedef struct hook* hookp;

/*
put in own region 'fragmentitized region'
*/
typedef struct region {
	struct region *next;
	_8_u type;
	_64_u offset;
	_32_u size;
	_int_u adr;
	_16_u fs, nf;
} *regionp;

typedef struct symbol {
	struct symbol *next;
	char const *name;
	_8_u type;
	_int_u loc;
	regionp *reg;
	_16_u f;
} *symbolp;

typedef struct relocate {
	struct relocate *next;
	_64_u offset;
	_16_u adr;
	// fragment of symbol/label
	_16_u to;
	// fragment reloc is in
	_16_u f;
	_8_u flags;
} *relocatep;

typedef struct hook {
	struct hook *next;
	_int_u offset;
	symbolp to;
	_16_u adr;
	_int_u f;
	_8_u flags;
} *hookp;

struct ffly_bond {


};

regionp extern curbin;
segmentp extern curseg;
_int_u extern offset;
_int_u extern adr;
int extern d;

void bond_oust(void*, _int_u);

void bond_hash_init(hashp);
void bond_hash_destroy(hashp);
void bond_hash_put(hashp, _8_u const*, _int_u, void*);
void* const bond_hash_get(hashp, _8_u const*, _int_u);
void bond(char const*, char const*);
void bond_output(remf_hdrp);
void bond_write(_64_u, void*, _int_u);
void bond_mapout(_64_u, _int_u);
void bond_read(_64_u, void*, _int_u);
# endif /*__ffly__bond__h*/
