#include "bond.h"
#include "../slam.h"
#include "../m_alloc.h"
#include "../io.h"
static struct slm_segment *sg;
void static _output(_ulonglong __ctx) {
	struct slam_hdr h;
	h.sg = f_bs.out.offset;
	h.ns = f_bs.tc;
	f_bs.out.pwrite(__ctx, sg, f_bs.tc*sizeof(struct slm_segment), h.sg);
	m_free(sg);
	f_bs.out.pwrite(__ctx, &h, sizeof(struct slam_hdr),0);
	printf("SLAM{n_segs: %u, placment: %x}\n",f_bs.tc,f_bs.out.offset);
}

/*
	tangs output are byte aligned
	so its up to the uploader or loader to ignore 
	the irrelevant parts
*/
void static 
tang(bn_tangp __t, _int_u __x) {
	struct slm_segment *s;
	s = sg+__x;
	s->off = __t->off;
	s->bytes = __t->size+__t->bsz;
	s->words = __t->words;
	s->adr = __t->adr+__t->k;
}

void static _prep(void) {
	f_bs.out.offset = sizeof(struct slam_hdr);
}

void static _post(void) {
	sg = m_alloc(f_bs.tc*sizeof(struct slm_segment));
}
struct bn_output bn_slam = {
	_output,
	tang,
	_prep,
	_post
};

