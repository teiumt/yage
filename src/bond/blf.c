#include "bond.h"
#include "../assert.h"
#include "../io.h"
#include "../blf.h"
#include "../tools.h"
#include "../string.h"
#include "../m_alloc.h"
struct frag_priv {
	struct bn_tang *t;	
};

struct priv_ctx {
	struct frag_priv *pfd;
	struct frag_ptr *f_top;
	struct blf_frag     *fr;
	struct blf_sym      *sy;
	_64_u addr;
	struct bn_section sects[64];
	struct bn_section *secttab[64];
	_int_u used_sects;
	_8_u *stt;
	struct bn_wa_block *wpt;
	struct bn_wa *wtbl;
	struct bn_tang *ttbl;
	struct bn_symb *sybtab[64];
	struct bn_smdata *smtab[0x100];
	struct blf_smdata *sm;
};

static struct bn_patch *pt;
static struct priv_ctx *ct;
//private fragment data
#define pfd (ct->pfd)
#define f_top (ct->f_top)
static _64_u bspace_top = 0x1fff;
void static read_symb(struct blf_sym *__sy, struct blf_smdata *__sm, _int_u __n) {
	struct blf_sym *sb;
	struct bn_symb *sy;
	struct blf_stent *se;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		sb = __sy+i;
		se = ct->stt+sb->name;
		char *str = ((_8_u*)se)+sizeof(struct blf_stent);
		printf("SYMB: '%s'%u NAME: %u.\n",str,se->len,sb->name);
		sy = bn_symbfor(str, se->len, &f_bs.symbols);
		ct->sybtab[i] = sy;
		struct blf_smdata *sm;
		struct bn_smdata *smd;
		_int_u i;
		i = 0;
		for(;i != sb->n;i++) {
			sm = __sm+sb->value+i;
			smd = sy->data+sm->x;
			assert(sm->x<16);
			printf("-SMD%u/x%u: domain: %u, flags: %u, value: %u.\n",sb->value+i,sm->x,sm->domain,sm->flags,sm->value);
			ct->smtab[sb->value+i] = smd;
			if (sm->domain == BLF_SYB_D_BSPACE) {
				bspace_top -= sm->value;
				smd->adr = bspace_top;	
			}else
			if (sm->flags&BLF_SYB_SECT) {
				if (!smd->sect) {
					ct->secttab[sm->sec] = smd->sect = ct->sects+sm->sec;
					if (sm->sec>ct->used_sects)
						ct->used_sects = sm->sec;
				}else{
					ct->secttab[sm->sec] = smd->sect;
				}
			}else {
				if (sm->flags&BLF_SYB_UD) {
					assert(ct->wpt != NULL);
					ct->wpt[sm->w].w = &smd->w;
				} else {
					smd->w = ct->wtbl+sm->w;
				}
			}
		}
	}
}
/*
	stays globaly defined
*/
static _64_u addr = 0;

void static read_frag(struct blf_frag *__fr, _int_u __n, _int_u __off) {
	struct blf_frag *f;
	struct bn_tang *t;
	_int_u i;
	i = 0;
	_64_u adr = addr;

	printf("BLF_FRAG: %u : %u.\n",adr,addr);
	for(;i != __n;i++) {
		f = __fr+i;
		printf("FRAG%u: words: %u\n", __off+i,f->words);
		t = ct->ttbl+i+__off;
		t->src = f->off;
		t->words = f->words;
		t->adr = adr;
		t->size = f->size;
		t->w = ct->wtbl+f->wa;
		t->nw = f->nw;
		struct bn_picfix *pf = t;
		pf->buf = f->over;
		pf->bts = f->nov;
		adr+=f->words;
		_int_u j;
		j = 0;
		for(;j != f->nw;j++) {
			struct bn_wa *w = ct->wtbl+j+f->wa;
			w->adr = t->adr;
			printf("|%cWA' addr=%u.\n", i+1 == __n?'_':' ',w->adr);
		}
	}
	addr = adr;
	printf("FEND: %u.\n", addr);
}

void static read_wa(struct blf_wa *__w, _int_u __n) {
	struct blf_wa *w;
	struct bn_wa *a;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		w = __w+i;
		a = ct->wtbl+i;
		a->adr = 0;
		a->plus = 0x41;
	}
}

void static
read_sections(struct blf_section *__sects, _int_u __n) {
	struct bn_section *sect;
	struct blf_section *s;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		s = __sects+i;
		sect = ct->secttab[s->idx];
		struct bn_wa *a;
		_int_u i;
		i = 0;
		for(;i != s->nw;i++) {
			a = ct->wtbl+i+s->wat;
			a->sect = sect;
		}
	}
}

void static read_reloc(struct blf_reloc *__rl, _int_u __n) {
	struct bn_rel *_r = (struct bn_rel*)m_alloc(sizeof(struct bn_rel)*__n), *r;

	struct blf_reloc *rl;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		rl = __rl+i;
		bn_tangp t = ct->ttbl+rl->f;
		r = _r+i;
		r->src = t;
		r->next = t->rl;
		t->rl = r;
		r->sm = NULL;		
		r->dst = NULL;
		r->flags = rl->flags;
		switch(rl->value) {
			case BLF_PIC_R_JMP:
				r->type = BLF_PIC_R_JMP;
			break;
			case BLF_PIC_R_CALL:
				r->type = BLF_PIC_R_CALL;
			break;
		}
		r->dis = rl->dis;
		r->where = rl->where;
		r->patch = pt+r->type;
		if (rl->value == BLF_PIC_R_BSPACE) {
			r->sm = ct->smtab[rl->sm];
		}else{
			/*
			r->dst is left unknown so...
		*/
		/*
			this type of reloc is only present once on a tang
		*/
		t->reloc = r;
		if (rl->flags&BLF_R_UD) {
			assert(ct->wpt != NULL);
			bn_wa_lk(&r->dst, ct->wpt+rl->w);
		}else
			r->dst = ct->wtbl+rl->w;
		
		}
		printf("RELOC' frag: %u, wa: %u, value: %u, where: %u\n", rl->f, rl->w, rl->value,rl->where);
	}
	struct bn_block *b;
	b = bn_block(&f_bs.rl);
	b->p = _r;
	b->n = __n;
}
#include "../tools.h"
/*
	
*/
struct frag_ptr {
	struct blf_frag *fr;
	_int_u n;
	struct frag_ptr *next;
};

struct frag_ptr static *fptr_find(struct blf_frag *__f) {
	struct frag_ptr *cur;
	cur = f_top;
	while(cur != NULL) {
		if (__f>=cur->fr&&__f<(cur->fr+cur->n)) {
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}
static struct blf_hdr *h;
/*
	strung together fragments
*/
struct frag_ptr static *fr_ptr(struct blf_frag *__start, _int_u __n) {
	struct frag_ptr *ptr = m_alloc(sizeof(struct frag_ptr));
	ptr->fr = __start;
	ptr->n = __n;
	ptr->next = f_top;
	f_top = ptr;
	return ptr;
}

void static splice(struct frag_ptr *__ptr, _int_u __start, _int_u __end) {
	__ptr->n = (ct->fr+__start)-__ptr->fr;
	struct frag_ptr *ptr;
	/*
		make sure we dont collect outofrange fragments
	*/
	if (__end<h->nfr)
		ptr = fr_ptr(ct->fr+__end,(h->nfr-__end));
}
/*

	build loading structure example

	looping over ignored fragments is wastful
	{F0,F1}->{F4}
	F0
	F1
	F2- IGNORE
	F3- IGNORE
	F4

	how...

	first we load the fragments and assume that start-end is the correct placement of things.

	later we splice the string into parts removing a fragment lets say
	then later we emit in the order that we need.
*/
#include "../tools.h"
void pic_fix(bn_tangp __t,struct bn_rel *rl);
void pic_em_fix(bn_tangp __t,struct bn_rel *rl);


void static pre_proc(struct f_bond_sf *sf) {
	printf("BLF.\n");
	ct = m_alloc(sizeof(struct priv_ctx));
	sf->priv = ct;
	f_top = NULL;
	h = sf->headr;

	switch (h->mech) {
		case BLF_M_PIC:
			pt = &pic_patch;	
			f_bs.fix = pic_fix;
		break;
		case BLF_M_PIC_EM:
			pt = &pic_em_patch;
			f_bs.fix = pic_em_fix;
		break;
	}

	mem_set(ct->sybtab,0,sizeof(void*)*64);

	/*
		load string table
	*/
	ct->stt = m_alloc(h->st_size);
	BN_pread(sf->ctx,ct->stt,h->st_size,h->st_ents);
	ffly_chrdump(ct->stt,h->st_size);


	/*
		fill out w and tang tables
	*/
	ct->wtbl = (struct bn_wa*)m_alloc(h->nw*sizeof(struct bn_wa));
	ct->ttbl = (struct bn_tang*)m_alloc(h->nfr*sizeof(struct bn_tang));
	if (h->w_m>0) {
		ct->wpt = (struct bn_wa_block*)m_alloc(h->w_m*sizeof(struct bn_wa_block));
		mem_set(ct->wpt,0,h->w_m*sizeof(struct bn_wa_block));
	} else
		ct->wpt = NULL;

	ct->used_sects = 0;
	/*
		initialize tang structures
	*/
	bn_tgint(ct->ttbl, h->nfr);

	struct blf_sym		*sy;
	struct blf_wa		*wa;
	struct blf_reloc	*rl;
	struct blf_section	*sects;
	struct blf_smdata	*sm;
	
	sm = m_alloc(h->smdn*sizeof(struct blf_smdata));
	BN_pread(sf->ctx,sm,h->smdn*sizeof(struct blf_smdata),h->smd);
	ct->sm = sm;
	/*
		loadin WA structures
	*/
	wa = m_alloc(h->wsz);
	BN_pread(sf->ctx,wa,h->wsz,h->wat);
	read_wa(wa, h->nw);
	m_free(wa);

	/*
		read symbol table
	*/
	sy = m_alloc(h->sytsz);
	BN_pread(sf->ctx,sy,h->sytsz,h->syt);
	read_symb(sy,sm,h->nsy);
	ct->sy = sy;
	/*
		the rest
	*/
	rl = m_alloc(h->rsz);
	BN_pread(sf->ctx,rl,h->rsz,h->rl);
	read_reloc(rl, h->nr);
	m_free(rl);
	
	sects = m_alloc(h->nsec*sizeof(struct blf_section));
	BN_pread(sf->ctx,sects,h->nsec*sizeof(struct blf_section),h->sec);
	read_sections(sects,h->nsec);
	m_free(sects);


	struct frag_ptr *ptr;

	pfd = m_alloc(h->nfr*sizeof(struct frag_priv));
	mem_set(pfd,0,h->nfr*sizeof(struct frag_priv));
	ct->fr = m_alloc(h->ftsz);
	BN_pread(sf->ctx,ct->fr,h->ftsz,h->ft);
	
	ptr = fr_ptr(ct->fr,h->nfr);
	if (h->entry != BLF_NULL) {
		struct blf_smdata *sy = ct->sm+h->entry;
		ptr = fptr_find(((_8_u*)ct->fr)+sy->f_start);
		if (!ptr) {
			printf("failed to find entry symbol.\n");
			return;
		}else{
			printf("found entry point symbol.\n");
		}

		/*
			remove entry routine
		*/

		splice(ptr,sy->f_is,sy->f_ie);
		addr = (ct->fr[sy->f_ie-1].adr+ct->fr[sy->f_ie-1].words)-ct->fr[sy->f_is].adr;
	}
}
void static proc_src(struct f_bond_sf *sf) {
	struct blf_hdr *h = sf->headr;
	ct = sf->priv;

	printf("BLF-HEADER: syt{%x:%u}, ft{%x:%u}, wat{%x:%u}, rl{%x:%u}.\n",
		h->syt, h->nsy, h->ft, h->nfr, h->wat, h->nw, h->rl, h->nr);

	struct f_bond_map *m;
	m = bn_map_new(0);
	_8_u *buf = m_alloc(h->size);
	BN_pread(sf->ctx,buf,h->size,h->start);
	printf("FRAGOUT.\n");
	ffly_hexdump(buf,h->size);


	f_bond_mapout(m, h->size);
	f_bond_write(m, buf, h->size, 0);
	m_free(buf);

	bn_tgclumpp clm;

	struct bn_link *l;
	l = (struct bn_link*)m_alloc(sizeof(struct bn_link));
	l->b = ct->wpt;
	l->n = h->w_m;
	l->next = f_bs.lk;
	f_bs.lk = l;

	_int_u nfrkn = 0;
	// add entry routine
	if (h->entry != BLF_NULL) {
		struct blf_smdata *sy = ct->sm+h->entry;
		printf("ENTRY: %u,%u.\n",sy->f_is,sy->f_ie);
		clm = bn_tgclump();

		clm->m = m;
		clm->tg = ct->ttbl+sy->f_is;
		clm->n = (sy->f_ie-sy->f_is);
		_64_u adrtmp = addr;
		addr = 0;
		read_frag(ct->fr+sy->f_is, clm->n,sy->f_is);
		addr = adrtmp;
		nfrkn+=clm->n;
		printf("END.\n");
	}

	struct frag_ptr *ptr;
	ptr = f_top;
	while(ptr != NULL) {
		printf("CLUMP: %u-N.\n",ptr->n);
		if (ptr->n>0) {
		_int_u s_idx = ptr->fr-ct->fr;
		read_frag(ptr->fr, ptr->n,s_idx);
		clm = bn_tgclump();
		clm->m = m;
		clm->tg = ct->ttbl+(ptr->fr-ct->fr);
		printf("PTR: start-end{%u->%u}:%u\n",s_idx,s_idx+ptr->n-1,ptr->n);
		clm->n = ptr->n;
		nfrkn+=clm->n;
		}
		ptr = ptr->next;
	}

	/*
		prevent forgetting fragments(DEBUG help)
	*/
	printf("known: %u - %u.\n",nfrkn,h->nfr);
	assert(nfrkn == h->nfr);
}
void bn_blf_output(_ulonglong __ctx) {

}
void bn_blf(void) {
	f_bs.proc_src = proc_src;
	f_bs.pre_proc = pre_proc;
}
