#include "bond.h"
#include "../types.h"
#include "../io.h"
#include "../m_alloc.h"
#include "../system/err.h"
#include "../opt.h"
#include "../depart.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../tools.h"
#include "../header.h"
#include "../version.h"
#include "../assert.h"
#include "../string.h"
struct file {
	int fd;
};

void static
_read(struct file *__ctx, void *__buf, _int_u __size) {
	read(__ctx->fd, __buf, __size);
}

void static
_pread(struct file *__ctx, void *__buf, _int_u __size, _64_u __offset) {
	pread(__ctx->fd, __buf, __size, __offset);
}

void static
_write(struct file *__ctx, void *__buf, _int_u __size) {
	write(__ctx->fd, __buf, __size);
	f_bs.out.offset+=__size;
}

void static
_pwrite(struct file *__ctx, void *__buf, _int_u __size, _64_u __offset) {
	pwrite(__ctx->fd, __buf, __size, __offset);
}

_y_err main(int __argc, char const *__argv[]) {
	f_bs.in.read = _read;
	f_bs.in.pread = _pread;
	f_bs.out.write = _write;
	f_bs.out.pwrite = _pwrite;
	ffoe_prep();
	char const *s = NULL;
	char const *d = NULL;
	char const *fmt = NULL;
	struct ffpcll pcl;
	pcl.cur = __argv+1;
	pcl.end = __argv+__argc;
	ffoe(ffoe_pcll, (void*)&pcl);

	s = ffoptval(ffoe_get("s"));
	d = ffoptval(ffoe_get("d"));
	fmt = ffoptval(ffoe_get("f"));
	ffoe_end();

	if (!s || !d || !fmt) {
		printf("please provide source and dest.\n");
		reterr;
	}
	struct f_bond_sf _sf[12];
	struct file sf[12], df, *_s, *cf;
	_s = sf;

	char *p, *bp;
	p = s;
	char c, b;
	char buf[128];
	bp = buf;
	_int_u i;
	i = 0;
	while(1) {
		c = *p++;
		if (c == ' ' || c == '\0') {
			*bp = '\0';
			bp = buf;
			_s->fd = open(buf, O_RDONLY, 0);
			printf("OPEN: %s.\n", buf);
			if (_s->fd<0) {
				printf("failed to open file.\n");
			}
			_sf[i].ctx = _s;
			//so not to effect position
			pread(_s->fd, _sf[i].headr, FH_BS, 0);
			struct f_fh *h = (struct f_fh*)_sf[i].headr;
			printf("FILE: id{%u}, vers{%u}.\n", h->id, h->vers);
			if (h->vers != ffly_version_int) {
				printf("WARNDING: version missmatch.\n");
			}
			_s++;
			i++;
			if (c == '\0')
				break;
		} else
			*(bp++) = c;
	}
	
	df.fd = open(d, O_WRONLY|O_CREAT|O_TRUNC, S_IRWXU);
	struct bn_fmt *fm;
	if (!str_cmp(fmt, "remf")) {
		fm = _bnf;
	}else
	if (!str_cmp(fmt, "blf")) {
		fm = _bnf+1;
	} else {
		assert(0 == !0);
	}
	bn_prep(fm);
	if (f_bs.out.offset>0) {
		printf("offset has been prematurely set, adjusting to %u\n", f_bs.out.offset);
		lseek(df.fd, f_bs.out.offset, SEEK_SET);
	}
	f_bond(_sf, (_ulonglong)&df, i);

	close(df.fd);
	cf = sf;
	for(;cf != _s;cf++) {
		close(cf->fd);
	}
	ffly_depart(NULL);
	retok;
}
