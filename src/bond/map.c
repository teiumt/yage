#include "bond.h"
#include "../m_alloc.h"
#include "../string.h"
/*
	file data memory ???
	where text,data sections to be stored in memory for tang output

	here we just dump everything into memory 
	then break off the parts we need as 
	its better then a dozen malloc calls
*/
struct f_bond_map* bn_map_new(
	_ulonglong __ctx)
{
	struct f_bond_map *m;
	m = (struct f_bond_map*)m_alloc(sizeof(struct f_bond_map));
	m->page_c = 1;
	m->pages = (_8_u**)m_alloc(sizeof(_8_u*));
	*m->pages = m_alloc(F_BNM_PAGE_SIZE);
	mem_set(*m->pages, 0x21, F_BNM_PAGE_SIZE);
	m->ctx = __ctx;
	m->off = 0;
	return m;
}

void f_bond_write(struct f_bond_map *__m, void *__buf, _int_u __size, _64_u __offset) {
	_int_u pg, pg_off;
	pg = __offset>>F_BNM_PAGE_SHFT;
	pg_off = __offset&F_BNM_PAGE_MASK;

	_8_u *src = (_8_u*)__buf;
	_8_u **pa = __m->pages+pg;
	if (pg_off>0) {
		_int_u rv = (F_BNM_PAGE_MASK^pg_off)+1;
		_8_u *d = (*(pa))+pg_off;
		if (__size>rv) {
			mem_cpy(d, src, rv);
			src+=rv;
			pa++;
		} else {
			mem_cpy(d, src, __size);
			return;
		}
	}

	_8_u **pe = __m->pages+((__offset+__size)>>F_BNM_PAGE_SHFT);	
	while(pa != pe) {
		mem_cpy(*pa, src, F_BNM_PAGE_SIZE);
		src+=F_BNM_PAGE_SIZE;
		pa++;
	}

	_int_u left = __size-(src-(_8_u*)__buf);
	if (left>0) {
		mem_cpy(*pa, src, left);
	}
}

_64_u f_bond_mapout(struct f_bond_map *__m, _int_u __size) {
	_int_u pgc;
	pgc = ((__size+__m->off)+F_BNM_PAGE_MASK)>>F_BNM_PAGE_SHFT;
	if (pgc>__m->page_c) {
		_int_u pg;
		pg = __m->page_c;
		__m->pages = (_8_u**)m_realloc(__m->pages, pgc*sizeof(_8_u*));
		while(pg != pgc) {
			*(__m->pages+(pg++)) = m_alloc(F_BNM_PAGE_SIZE);
		}
		__m->page_c = pgc;
	}
	_64_u r = __m->off;
	__m->off+=__size;
	return r;
}
