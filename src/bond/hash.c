#include "bond.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../crypt.h"
#include "../io.h"
void
f_bond_hash_init(hashp __hash) {
	__hash->table = (struct hash_entry**)m_alloc(0x100*sizeof(struct hash_entry*));
	struct hash_entry **p = __hash->table;
	struct hash_entry **end = p+0x100;
	while(p != end)
		*(p++) = NULL;
}

void static
free_chain(struct hash_entry *__top) {
	struct hash_entry *cur = __top, *bk;
	while(cur != NULL) {
		bk = cur;
		cur = cur->next;
		m_free((void*)bk->key);
		m_free(bk);
	}
}

void
f_bond_hash_destroy(hashp __hash) {
	struct hash_entry **p = __hash->table;
	struct hash_entry **end = p+0x100;
	while(p != end) {
		if (*p != NULL)
			free_chain(*p);
		p++;
	}
	m_free(__hash->table);
}

void
f_bond_hash_put(hashp __hash, _8_u const *__key, _int_u __len, void *__p) {
	_64_u sum = y_hash64(__key, __len);
	struct hash_entry *entry = (struct hash_entry*)m_alloc(sizeof(struct hash_entry));
	struct hash_entry **table = __hash->table+(sum&0xff);


	entry->next = *table;
	*table = entry;
	printf("hash_put: len: %u\n", __len);
	mem_dup((void**)&entry->key, (void*)__key, __len);
	entry->p = __p;
	entry->len = __len;
}

void* const
f_bond_hash_get(hashp __hash, _8_u const *__key, _int_u __len) {
	_64_u sum = y_hash64(__key, __len);
	struct hash_entry *cur = *(__hash->table+(sum&0xff));
	while(cur != NULL) {
		if (cur->len == __len) {
			if (!mem_cmp(cur->key, __key, __len))
				return cur->p;
		}
		cur = cur->next;
	}

	char buf[64];
	mem_cpy(buf,__key,__len);
	buf[__len] = '\0';
	printf("hash error, not fatal just cant locate entry nothing to panic about, %s.\n",buf);
	return NULL;
}
