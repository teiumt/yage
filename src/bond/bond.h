#ifndef __f__bond__bond__h
#define __f__bond__bond__h
#include "../y_int.h"
struct hash_entry {
	struct hash_entry *next;
	_8_u const *key;
	_int_u len;
	void *p;
};

typedef struct hash {
	struct hash_entry **table;
} *hashp;

//hash.c
void f_bond_hash_init(hashp);
void f_bond_hash_destroy(hashp);
void f_bond_hash_put(hashp, _8_u const*, _int_u, void*);
void* const f_bond_hash_get(hashp, _8_u const*, _int_u);

#define F_BN_REMF_ST_TANG 0x00

struct bn_wa_link {
	struct bn_wa **w;
	struct bn_wa_link *next;
};

struct bn_wa_block {
	struct bn_wa **w;
	struct bn_wa_link *l;
};

struct bn_link {
	struct bn_wa_block *b;
	_int_u n;
	struct bn_link *next;
};

struct bn_section;
struct bn_smdata {
	_8_u type;
	_64_u adr;
	union {
		struct bn_wa *w;
		struct bn_section *sect;
	};
};
struct bn_symb {
	struct bn_smdata data[16];
	_int_u n;
};

struct bn_wa {
	_64_u adr;
	_64_u plus;
	struct bn_section *sect;
};

struct bn_tb {
	void *p;
	_int_u size;
};

/*
	when a fix accurs on a tang other data will be needed
*/

struct bn_picfix {
	_64_u buf;
	_int_u bts;
};
struct bn_patch;
struct bn_fixup {
	_8_u adrty;
	_8_u level;
	struct bn_patch *pt;
};
struct f_bond_map;
struct bn_rel;
typedef struct bn_tang {
	struct f_bond_map *_m;
	_8_u userdata[64];
/*
	not the size but in words of mesurement
	size != words
*/
	_int_u words;
	_64_u adr;
/*
	where in the output file it was places
*/
	_64_u off;

	_int_u size, src;
	struct bn_tb *b;
	_int_u nb;

	struct bn_fixup fu;
	_64_u k, m,t;
	_64_s dis;
	_8_u buf[64];
	_int_u bsz;
	_8_u adrty;
	_8_u level;
	
	/*
		wa structs within this tang
	*/
	struct bn_wa *w;
	_int_u nw;

	struct bn_rel *rl;
	struct bn_rel *reloc;
	struct bn_wa *th[24];
	_int_u nt;
	struct bn_tang *next;
} *bn_tangp;
//tang clump
typedef struct bn_tgclump {
	struct f_bond_map *m;
	struct bn_tang *tg;
	_int_u n;
	struct bn_tgclump *next;

	struct bn_tang *pnd_tg[4];
	_int_u pnd_n;
} *bn_tgclumpp;

#define F_BN_R_16 0
#define F_BN_R_32 1
#define F_BN_R_14 2
struct bn_rel {
	void *patch;
	_8_u type;
	struct bn_wa *dst;
	struct bn_smdata *sm;
	bn_tangp src;
	struct bn_rel *next;
	_int_u where;
	_64_u flags;
	_32_s dis;
};

struct bn_section {
	_64_u vadr;
};

struct bn_block {
	void *p;
	_int_u n;
	struct bn_block *next;
};
/*
	i dont want seperit struct for this as its stupid

	NOTE:	
		re act apon relocation without data then when fragments are emited we construct that data
*/
struct bn_patch {
	//FIXME: should be array [m0,m1]
	/*
		inverse mask
*/
	_64_u m0, m1;
	/*
		number of words that the relocation takes up
*/
	_64_u w0, w1;
	/*
		size of data
*/
	_64_u s0, s1;
	
	/*
		the data formatting method
	*/
	void(*j0)(void);
	void(*j1)(void);
	_8_u *ib0;
	_8_u *ib1;
};

extern struct bn_patch pic_patch[2];
extern struct bn_patch pic_em_patch[2];

#define F_BNM_PAGE_SHFT 9
#define F_BNM_PAGE_SIZE (1<<F_BNM_PAGE_SHFT)
#define F_BNM_PAGE_MASK (F_BNM_PAGE_SIZE-1)

struct f_bond_cake {
	struct hash hm;
};
/*
	where everything is places programs crap
*/
struct f_bond_map {
	_8_u **pages;
	_32_u page_c;
	_ulonglong ctx;
	_32_u off;
};

struct bn_output {
	void(*out)(_ulonglong);
	void(*tang)(bn_tangp,_int_u);
	void(*prep)(void);
	void(*post)(void);
};

struct f_bond_map* bn_map_new(_ulonglong);
void f_bond_write(struct f_bond_map*, void*, _int_u, _64_u);
_64_u f_bond_mapout(struct f_bond_map*, _int_u);
struct f_bond {
	struct hash symbols;	
	struct {
		void(*read)(_ulonglong, void*, _int_u);
		void(*pread)(_ulonglong, void*, _int_u, _64_u);
	} in;
	struct {
		_64_u offset;
		void(*write)(_ulonglong, void*, _int_u);
		void(*pwrite)(_ulonglong, void*, _int_u, _64_u);
	} out;
	void(*proc_src)(struct f_bond_sf*);
	void(*pre_proc)(struct f_bond_sf*);
	void(*done)(void);
	void(*output)(_ulonglong);
	void(*fix)(bn_tangp,struct bn_rel*);

	//tang count
	_int_u tc;
	struct bn_output _out;
	bn_tgclumpp tg_srt, tg_end;
	struct bn_block *rl;
	struct bn_link *lk;
	_64_u adr;
};

struct bn_fmt {
	void(*prep)(void);
	void(*output)(_ulonglong);
	_64_u offset;
};

extern struct bn_fmt _bnf[2];

#define F_bond_read(__ctx, __buf, __size)\
	f_bs.in.read(__ctx, __buf, __size)
#define F_bond_pread(__ctx, __buf, __size, __offset)\
	f_bs.in.pread(__ctx, __buf, __size, __offset)
#define BN_pread(...) f_bs.in.pread(__VA_ARGS__);
#define BN_write(...) f_bs.in.write(__VA_ARGS__);
extern struct f_bond f_bs;


#define F_BN_OUT_REMF 0x00
struct f_bond_sf {
	_8_u headr[512];
	_ulonglong ctx;
	void *priv;
};

//output formats
struct bn_output bn_slam;
void bn_wa_lk(struct bn_wa**, struct bn_wa_block*);
struct bn_symb* bn_symbfor(_8_u*, _int_u, struct hash*);
void bn_prep(struct bn_fmt*);
void bn_remf_output(_ulonglong);
void bn_blf_output(_ulonglong);
bn_tgclumpp bn_tgclump(void);
void bn_tgint(struct bn_tang*, _int_u);
void f_bond_remf(void);
void bn_blf(void);
void bn_baout(void);
struct bn_block* bn_block(struct bn_block**);
void f_bond(struct f_bond_sf*, _ulonglong, _int_u);
#endif/*__f__bond__bond__h*/
