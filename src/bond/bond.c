#include "bond.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../ffly_def.h"
#include "../remf.h"
#include "../blf.h"
#include "../io.h"
#include "../assert.h"
/*
	NOTE: tang->adr is built apon

	for the real address

	its 

	t->adr+value

	who sets it? the driver/blf/remf etc
*/
struct bn_fmt _bnf[2] = {
	{f_bond_remf,bn_remf_output,remf_hdrsz},
	{bn_blf,bn_blf_output,blf_hdrsz}
};
struct f_bond f_bs = {
	.tg_srt = NULL,
	.tg_end = NULL,
	.lk = NULL,
	.rl = NULL,
	.adr = 0
};
/*
	program regions from each file are contained in a map of there very own
*/
struct bn_symb* bn_symbfor(_8_u *__ident, _int_u __len, struct hash *__hm) {
	struct bn_symb *s;
	s = (struct bn_symb*)f_bond_hash_get(__hm, __ident, __len);
	if (!s) {
		s = (struct bn_symb*)m_alloc(sizeof(struct bn_symb));
		f_bond_hash_put(&f_bs.symbols, __ident, __len, s);
		mem_set(s->data,0,16*sizeof(struct bn_smdata));
	}
	return s;
}

/*
	EXAMPLE
	test:
	call test = LINK-0
	call test = LINK-1
	call test = LINK-2

	BLOCK = test
*/
void
bn_wa_lk(struct bn_wa **__w, struct bn_wa_block *__b) {
	assert(__b->w != NULL);
	struct bn_wa_link *l;
	l = (struct bn_wa_link*)m_alloc(sizeof(struct bn_wa_link));
	l->w = __w;
	l->next = __b->l;
	__b->l = l;
}

void bn_prep(struct bn_fmt *__f) {
	f_bond_hash_init(&f_bs.symbols);
	__f->prep();
	f_bs.output = __f->output;
	f_bs._out = bn_slam;
	bn_slam.prep();
}
bn_tgclumpp bn_tgclump(void) {
	bn_tgclumpp clm;
	clm = m_alloc(sizeof(struct bn_tgclump));
	clm->next = NULL;
	clm->pnd_n = 0;
	if	(!f_bs.tg_srt)
		f_bs.tg_srt = clm;
	
	if (f_bs.tg_end != NULL)
		f_bs.tg_end->next = clm;
	f_bs.tg_end = clm;
	return clm;
}

struct bn_block* bn_block(struct bn_block **__base) {
	struct bn_block *b;
	b = (struct bn_block*)m_alloc(sizeof(struct bn_block));

	b->next = *__base;
	*__base = b;
	return b;
}

void bn_tgint(struct bn_tang *__t, _int_u __n) {
	struct bn_tang *t;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		t = __t+i;
		t->nb = 0;
		t->fu.pt = NULL;
		t->bsz = 0;
		t->reloc = NULL;
		t->k = 0;
		t->rl = NULL;
		t->level = 255;
		t->adr = 0x41/*dummy value debuggging i hope?*/;
		t->words = 0;
	}
}

void ypic(void) {
	struct bn_symb *s;
	s = bn_symbfor("data",4,&f_bs.symbols);
	s->data[0].sect = m_alloc(sizeof(struct bn_section));
	s->data[0].sect->vadr = 0x10;

	s = bn_symbfor("text",4,&f_bs.symbols);
	s->data[0].sect = m_alloc(sizeof(struct bn_section));
	s->data[0].sect->vadr = 0;
}

/*
	simply works like this.

	if displacement address overflows then a level shift happes
*/
_64_u static disc(bn_tangp t) {
	assert(t->reloc->dst != NULL);
	struct bn_rel *rl = t->reloc;
	_64_u dis = rl->dst->adr+rl->dst->plus+rl->dst->sect->vadr;
	printf("DISPLACMENT: dis: %u, addr: %u, adr+words: %u, plus: %u, section: %u\n", dis, rl->dst->adr, t->adr+t->words,rl->dst->plus,rl->dst->sect->vadr);
	t->dis = dis;
	struct bn_patch *pt;
	pt = t->fu.pt;
	if(dis<0)
		dis = -dis;
	_64_u w;
	if (!(dis&pt->m0)) {
		w = pt->w0;
		t->level = 0;
	}else
	if (!(dis&pt->m1)) {
		w = pt->w1;
		t->level = 1;
	} else {
		assert(1 == 0);
	}
	printf("WORDS-ADDED: %u, level: %u, dis: %u\n", w, t->level,dis);
	return w;
}
/*
	tropical linking
*/
static _64_u lk_ano = 0;
_64_u static linkit(struct bn_tang *__tg, _int_u __n, _64_u k) {
		struct bn_tang *t;
		_int_u i;
		i = 0;
		for(;i != __n;i++) {
			t = __tg+i;
			struct bn_wa *w;
			printf("TANG' wa_cnt: %u, adr: %u\n", t->nw,t->adr+t->k);
			
			/*
				fix up where abouts strucs
			*/
			_int_u j;
			j = 0;
			for(;j != t->nw;j++) {
				w = t->w+j;
				w->plus = k;
			}
			if (t->k != k){
				lk_ano = 1;
				t->k = k;
			}
			/*
				its more efficient to do this actively
			*/
			j = 0;
			if (t->reloc != NULL) {
			if (t->reloc->dst != NULL) {
				j = disc(t);
				k+=j;
			}
			}
		}
	return k;
}

void commence_linkage(void) {
	bn_tangp t, _t;
	bn_tgclumpp clm;
	_64_u k;
	lk_ano = 0;
_anoth:
	k = 0;
	clm = f_bs.tg_srt;
	while(clm != NULL) {
		k = linkit(clm->tg,clm->n,k);
		clm = clm->next;
	}

	if (lk_ano){
		printf("another sweep needed.\n");
		lk_ano = 0;
		goto _anoth;
	}
}

void static tangdo(struct f_bond_map *m,struct bn_tang *__tg, _int_u __n) {
		_int_u i;
		i = 0;
		struct bn_tang *t;
		for(;i != __n;i++) {
			t = __tg+i;
			printf("TANG' size: %u, src: %u, adr: %u, k: %u, words: %u, bsz: %u\n", t->size, t->src, t->adr, t->k, t->words, t->bsz);
			t->_m = m;
			if (!t->size) {
				t = t->next;
				continue;
			
			}
		
			_int_u nb;
			_64_u src = t->src, end;
			nb = (t->size+F_BNM_PAGE_MASK+(src&F_BNM_PAGE_MASK))>>F_BNM_PAGE_SHFT;
			struct bn_tb *b = (struct bn_tb*)m_alloc(nb*sizeof(struct bn_tb));
			printf("---> blockcount: %u.\n", nb);	
			_8_u **pg = m->pages+(src>>F_BNM_PAGE_SHFT);
			struct bn_tb *_b = b;
			_int_u pg_off;
			pg_off = src&F_BNM_PAGE_MASK;
			_8_u **pe;
			_64_u left;

			/*
				breakdown MAP into tang blocks that are easy to output
				and minipulate
			*/
			if (pg_off>0) {
				_int_u am = (F_BNM_PAGE_MASK^pg_off)+1;
				_b->p = (*pg)+pg_off;
				if (t->size>am) {
					_b->size = am;
					_b++;
					pg++;
				} else {
					_b->size = t->size;
					goto _sk;
				}
			}
			//aligned on page

			pe = m->pages+((end = (src+t->size))>>F_BNM_PAGE_SHFT);
			while(pg<pe) {
				_b->p = *pg;
				_b->size = F_BNM_PAGE_SIZE;
				_b++;
				pg++;
			}
			left = end&F_BNM_PAGE_MASK;
			if (left>0) {
				_b->p = *pg;
				_b->size = left;
				_b++;
			}
		_sk:
			t->b = b;
			t->nb = nb;
	}
}
#include "../tools.h"
# include "../assert.h"
void f_bond(struct f_bond_sf *__sf, _ulonglong __of, _int_u __nif) {
	ypic();
	_int_u i;
	struct f_bond_sf *_sf;
	i = 0;
	for(;i != __nif;i++) {
		_sf = __sf+i;
		printf("pre processing file: %x.\n", _sf->ctx);
		f_bs.pre_proc(_sf);
	}

	i = 0;
	for(;i != __nif;i++) {
		_sf = __sf+i;
		printf("processing file: %x.\n", _sf->ctx);
		f_bs.proc_src(_sf);
	}

	if (f_bs.tg_end != NULL) {
		f_bs.tg_end->next = NULL;
	}

	struct bn_link *l;
	l = f_bs.lk;
	while(l != NULL) {
		_int_u i;
		i = 0;
		
		struct bn_wa_block *b;
		printf("linking. %u\n", l->n);
/*		
			crossfile linkage
*/		
		for(;i != l->n;i++) {
			b = l->b+i;
			if (!b) continue;
			struct bn_wa *w;
			
			if (!b->w) continue;
			w = *b->w;
			if (!w)continue;
			printf("- %p:%u.\n", b->w, i);
			struct bn_wa_link *wl;
			wl = b->l;
			while(wl != NULL) {
				*wl->w = w;
				wl = wl->next;
			}
		}

		l = l->next;
	}

	bn_tangp t;
	bn_tgclumpp clm;
	/*
		do some linking
	*/
	struct bn_block *rl;
	rl = f_bs.rl;

	while(rl != NULL) {
		struct bn_rel *r;
		r = (struct bn_rel*)rl->p;
		_int_u i;
		i = 0;
		for(;i != rl->n;i++) {
			r->src->bsz = 0;
			switch(r->type) {
				case F_BN_R_16:
					r->src->bsz = 2;
				break;
				case F_BN_R_32:
				break;
			}
			struct bn_patch *pt;
//			assert(r->dst != NULL && r->sb != NULL);	
			/*
				other flavours of relocs are getting in the way
			*/
			if (r->dst != NULL)
				r->src->fu.pt = r->patch;
			printf("RELOC' adrtype: %u, %u\n", r->type,r->where);
			r->src->level = 0;

			/*
	TODO:
			if we jump to a label more then once allow label to backdraw

			if (r->src->adr>r->dst->adr) {
				r->dst->th[r->dst->nt++] = r->src;
			}
			*/
			r++;
		}
		rl = rl->next;
	}
	commence_linkage();
//	f_bs.done();
/*
	go over each tang and link to maps for ease of transport
*/
	struct f_bond_map *m;
	_int_u tc = 0;
	clm = f_bs.tg_srt;
	while(clm != NULL) {
		tc+=clm->n;
		tangdo(clm->m,clm->tg,clm->n);
		clm = clm->next;
	}

	f_bs.tc = tc;
	
	//slam file?
	f_bs._out.post();
/*

	i know we could just output above but
	if we want to change somthing then issues will occur
	well justs make shit harder
*/
	//output??
	_int_u k = 0;
	clm = f_bs.tg_srt;
	while(clm != NULL) {
		_int_u j;
		j = 0;
		for(;j != clm->n;j++) {
			t = clm->tg+j;
			t->off = f_bs.out.offset;
			printf("TANG' BLKS: %u, WORDS: %u, ADR: %x\n", t->nb,t->words,t->adr);
		
			/*
				where relocatable instructions are placed
				and generated
			*/

			struct bn_rel *rl;
			rl = t->rl;
			while(rl != NULL) {
				f_bs.fix(t,rl);
				rl = rl->next;
			}

			_int_u i;
			i = 0;
			struct bn_tb *b;
			for(;i != t->nb;i++) {
				b = t->b+i;
				printf("############################ %u.\n",b->size);
				f_bs.out.write(__of, b->p, b->size);
				ffly_hexdump(b->p, b->size);
			}

			printf("TANG short buffer, %u-bytes\n", t->bsz);
			f_bs.out.write(__of, t->buf, t->bsz);
			f_bs._out.tang(t,k+j);
		}
		k+=clm->n;
		clm = clm->next;
	}
	f_bs._out.out(__of);
}
