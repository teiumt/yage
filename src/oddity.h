# ifndef __ffly__oddity__h
# define __ffly__oddity__h
# include "y_int.h"
/*
	anything out of the ordinary
	errors/warnings/etc...
*/
extern _32_u oddity_count;
/*
	if odditys exceed x amount then inform the user or panic and begin shutdown procedures this will be done by bog
	so here we should notify by setting a bit high
*/
#define oddity_ASM \
"lock incl oddity_count(%rip)\n"
//"movq oddity_count(%rip), %rax\n"
//"cmpq $0x21299, %rax\n"
//"do jump"
#ifdef ignore_odditys
#define caught_oddity
#else
#define caught_oddity\
	__asm__(oddity_ASM)
#endif
# endif /*__ffly__oddity__h*/
