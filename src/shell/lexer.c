# include "lexer.h"
# include "input.h"
# include "memalloc.h"
# include "../string.h"
# define is_space(__c) (__c == ' ' || __c == '\t')
void *tok_data;
_int_u tok_sz;
_8_u tok_val;
void static
read_ident(void) {
	char buf[128];
	char *p = buf;
	char c = getc();
	while((c>='a'&&c<='z') || (c>='0'&&c<='9')) {
		*(p++) = c;
		if (at_eof()) break;
		c = getc();
	}
	if (!at_eof())
		ugetc(c);

	*p = '\0';
	_int_u l;

	l = p-buf;
	tok_data = mem_alloc(l+1);
	memcpy(tok_data, buf, l+1);
	tok_sz = l+1;
}

void static
read_str(void) {
	char buf[128];
	char *p = buf;
	char c;
	while((c = getc()) != '"')
		*(p++) = c;
	*p = '\0';
	_int_u l;

	l = p-buf;
	tok_data = mem_alloc(l+1);
	memcpy(tok_data, buf, l+1);
	tok_sz = l+1;
}

_8_u static buf = 0xff;

_8_u nexttok(void) {
	if (buf<0xff) {
		_8_u ret;

		ret = buf;
		buf = 0xff;
		return ret;
	}
	char c;
_again:
	if (at_eof())
		return _tok_eof;

	c = getc();
	if (is_space(c))
		goto _again;

	if (c>='a'&&c<='z') {
		ugetc(c);
		read_ident();
		return _tok_ident;
	} else if (c == '"') {
		read_str();		
		return _tok_str;
	} else {
		switch(c) {
			case '\n':
				return _tok_nl;
			case '*':
				tok_val = c;
				return _tok_keywd;
		}
	}
	return 0;
}

_8_u peektok(void) {
	buf = nexttok();
	return buf;
}

char const* tokstr(_8_u __tok) {
	switch(__tok) {
		case _tok_uk:
			return "unknown";
		case _tok_eof:
			return "eof";
		case _tok_ident:
			return "ident";
		case _tok_str:
			return "string";
		case _tok_keywd:
			return "keyword";
	}
	return "unknown";
}
