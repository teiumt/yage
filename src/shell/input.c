# include "input.h"
void(*get)(_int_u, _int_u, void*);
_8_u(*at_eof)(void);
_8_u static buf;
_8_i static pres = -1;
void ugetc(_8_u __b) {
	buf = __b;
	pres = 0;
}

_8_u getc(void) {
	if (!pres) {
		pres = -1;
		return buf;
	}
	_8_u ret;
	get(1, 0, &ret);
	return ret;
}
