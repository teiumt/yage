# include "exec.h"
# include "../dep/str_cmp.h"
# include "../version.h"
# include "../dep/mem_cmp.h"
# include "../system/util/common.h"
# include "../stdio.h"
# include "../string.h"
# include "../exec.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
/*
	TODO: 
	put into macros
*/
/*
	for larger segments just call a function
	so 
	jmp _example

	_example:
	myfunc();
	jmp _end

	its to keep things tidy its i like to keep small functions 
	grouped with eachother 
*/
static char const *help = "commands:\n"
	"	help\n"
	"	exit\n"
	"	f4 <enc,dec> <value>\n"
	"	exec <file>\n" "VERSION: " ffly_version_str "\n";

void _help();
void _exit();
void _info();
void static _f4(_int_u, struct arg_s**);
void _exec();
void _er();

struct _cmd {
	void *func;
	char const *name;
	_8_u flags;
};

#define LARGEFUNC 0x01
#define CMD(__func, __name, __flags)\
	{__func, __name, __flags}
static struct _cmd cmds[] = {
	CMD(_help, "help", 0x00),
	CMD(_exit, "exit", 0x00),
	CMD(_f4, "f4", LARGEFUNC),
	CMD(_info, "info", 0x00),
	CMD(_exec, "exec", 0x00),
	CMD(_er, "er", 0x00)
};

void ffsh_cmdput(struct hash *__hash, char const *__ident, _int_u __func) {
	hash_put(__hash, __ident, strlen(__ident), cmds+__func);
}

void* ffsh_cmdget(struct hash *__hash, char const *__ident) {
	return hash_get(__hash, __ident, strlen(__ident));
}

#define jmpend __asm__("jmp _end");

_8_i *ffsh_run = NULL;

/*
external routines
*/
void(*ffsh_er)(char const*, _int_u, char const*[]);

/*
	inbuilt commands
*/
void _f4(_int_u __n, struct arg_s **__args) {
	char buf[128];
	if (__n != 2) {
		printf("2 arguments needed.\n");
	} else {
		struct arg_s *mot = *__args;
		struct arg_s *val = *(__args+1);
		_int_u l;
		if (mot->l == 4) {
			if (!f_mem_cmp(mot->p, "enc", mot->l-1)) {
				l = f_f4_enc(val->p, buf, val->l-1);
				*(buf+l) = '\0';
				printf("enc, '%s':'%s':%u\n", val->p, buf, l);
			} else if (!f_mem_cmp(mot->p, "dec", mot->l-1)) {
				l = f_f4_dec(val->p, buf, val->l-1);
				*(buf+l) = '\0';
				printf("dec, '%s':'%s':%u\n", val->p, buf, l);
			} else {
				printf("unknown.\n");
			}
		} else
			printf("length error.\n");
	}
}

#define LABEL(__name) __asm__(__name ":\n\t");
#define jmpto(__func)\
	__asm__("jmp *%0" : : "r"(__func));
void ffsh_exec_cmd(void *__func, _int_u __n, struct arg_s **__args) {
	struct _cmd *cmd;
	cmd = (struct _cmd*)__func;

/*
	routine is quite large so no point in it being placed here
*/
	if (!(cmd->flags&LARGEFUNC)) {
		jmpto(cmd->func)
	} else {
		void(*func)(_int_u, struct arg_s**);
		func = cmd->func;
		func(__n, __args);
		jmpend
	}
	LABEL("_help")
		printf("%s", help);
	jmpend;

	LABEL("_exit")
		*ffsh_run = -1;
	jmpend;

	LABEL("_er") {
		char const *args[24];
		char const **cur;

		cur = args;
		_int_u i;
		i = 1;
		for(;i != __n;i++)
			*(cur++) = __args[i]->p;
		*cur = NULL;
		ffsh_er((char const*)(*__args)->p, __n, args);	
	}
	jmpend;

	LABEL("_info") {
		printf("firefly version: %u-%s\n", ffly_version_int, ffly_version_str);
	}
	jmpend;
	LABEL("_exec") {
		if (__n>0) {
			struct arg_s *file = *__args;
			ffexecf(file->p);
		} else
			printf("please provide file.\n");
	}
	jmpend;
	__asm__("_end:\n\t");
}
