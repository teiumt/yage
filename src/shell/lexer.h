# ifndef __ffly__shell__lexer__h
# define __ffly__shell__lexer__h
# include "../y_int.h"
_8_u nexttok(void);
_8_u peektok(void);
char const* tokstr(_8_u);
enum {
	_tok_uk,
	_tok_ident,
	_tok_keywd,
	_tok_str,
	_tok_eof,
	_tok_nl
};

extern void *tok_data;
extern _int_u tok_sz;
extern _8_u tok_val;
# endif /*__ffly__shell__lexer__h*/
