# ifndef __ffly__shell__input__h
# define __ffly__shell__input__h
# include "../y_int.h"
_8_u getc(void);
extern void(*get)(_int_u, _int_u, void*);
extern _8_u(*at_eof)(void);
void ugetc(_8_u);
# endif /*__ffly__shell__input__h*/
