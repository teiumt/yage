# include "memalloc.h"
# include "../malloc.h"
typedef struct hdr {
	struct hdr **bk, *next;
	void *p;
} *hdrp;

# define hdrsize sizeof(struct hdr)

static hdrp top = NULL;
void* mem_alloc(_int_u __c) {
	void *p = malloc(__c+hdrsize);
	hdrp h = (hdrp)p;
	h->bk = &top;
	h->next = top;
	if (top != NULL)
		top->bk = &h->next;
	top = h;
	return (h->p = (void*)((_8_u*)p+hdrsize));
}

void mem_free(void *__p) {
	hdrp h = (hdrp)((_8_u*)__p-hdrsize);
	*h->bk = h->next;
	if (h->next != NULL)
		h->next->bk = h->bk;
	free(h);
}

void mem_cleanup(void) {
	hdrp cur = top, bk;
	while(cur != NULL) {
		bk = cur;
		cur = cur->next;
		mem_free(bk->p);
	}
	top = NULL;
}
