# ifndef __f__ic__h
# define __f__ic__h
# include "y_int.h"
# include "types.h"
# include "linux/types.h"
# include "linux/socket.h"
# include "linux/in.h"
# include "in.h"
# include "linux/net.h"
# include "system/mutex.h"

/*

	what is ic????

	its a ping pong method of comms non holding
	sends and recves at an interval when there items to be s/r
*/
void f_ic_init(void);
void f_ic_de_init(void);
struct f_ic_cptcp {
	struct sockaddr *addr;
	socklen_t len;
};
#define F_IC_SHFT_BASE 1
#define F_IC_PKT_SHFT (7+F_IC_SHFT_BASE)
#define F_IC_PKT_SIZE (1<<F_IC_PKT_SHFT)
#define F_IC_PKT_MASK (F_IC_PKT_SIZE-1)
#define F_IC_PKT_CLUMP 4
/*
	we send in clumps why???
	because in the case the n*X packets in bytes
	is hard to divide why not just use ceil(X/SEGSIZE) ???
	usage of such a method is an idiots fantasy so we
	are going to use the fpu for every time we send shit????
	or we do it at compile time and use clumps
*/
#define F_IC_PKT_CLUMP_SZ (1<<F_IC_PKT_CLUMP)//16
typedef struct f_ic_pkt {
	_8_u *data;
	_int_u seq_n;
	_int_u size;//excluding padding
	struct f_ic_pkt *next;
} *f_ic_pktp;

struct f_ic_header {
	_8_u flags;
};

struct f_ic_shdr {
	_32_u seg;//seqno
	_32_u off;
};

struct f_ic_stack {
	mlock lock;
	_int_u ig_n, ig_bytes;
	_int_u npkt;
	//ingoings(in)
	struct f_ic_pkt *ig;
	//outcommings(out)/outgoings
	struct f_ic_pkt *og;
	void *priv;
};

#define F_IC_ENT_IGNORE 0x01
struct f_ic_ent {
	struct f_ic_stack stack;
	struct f_ic_ent *next, **bk;
	void *ctx;
	_8_u flags;
};

struct f_ic_cdet {
	union {
		struct f_ic_cptcp tcp;
	};
};

typedef struct f_ic_context *f_ic_contextp;
#define F_IC_FDEF(__retty, __name, ...) __retty(*__name)(__VA_ARGS__);
#define F_IC_CDTCP(__det) &(__det).tcp
struct f_ic_prot {
	F_IC_FDEF(void*, ctx_new, f_ic_contextp)
	F_IC_FDEF(void, ctx_destroy, void*)
	F_IC_FDEF(void, mkc, void*, void*)
	F_IC_FDEF(void, mksp, void*, void*)
	F_IC_FDEF(void, wic, void*, void*)
	F_IC_FDEF(void, close, void*)
	struct f_ic_ent *ent;
	mlock lock;
};

typedef struct f_ic_context {
	struct f_ic_prot *prot;
	struct f_ic_ent *ent;
	void *priv_ctx;
} *f_ic_contextp;

typedef struct f_ic_con {
	struct f_ic_context ctx;
} *f_ic_conp;
enum {
	_F_IC_CP_TCP
};

/*
	make connection
*/
f_ic_conp f_ic_mkc(void*, _8_u);
// wait for incomming connection
f_ic_conp f_ic_wic(f_ic_conp);
void f_ic_send(f_ic_conp, void*, _int_u);
void f_ic_recv(f_ic_conp, void*, _int_u);
# endif /*__f__ic__h*/
