# include "exec.h"
# include "../rosin.h"
static ffly_tapep tape;
_32_u static ip;

_8_u static
fetch_byte(_f_off_t __off) {
	_8_u rt;
	ffly_tape_get(tape, ip+__off, &rt, 1);
	return rt;
}

void static
ip_incr(_int_u __by) {
	ip+=__by;
}

_f_addr_t static
get_ip(void) {
	return ip;
}

void static
set_ip(_f_addr_t __to) {
	ip = __to;
}

static struct ffly_rosin ctx = {
	.stack_size = 700,
	.fetch_byte = fetch_byte,
	.ip_incr = ip_incr,
	.get_ip = get_ip,
	.set_ip = set_ip
};

void ffros_exec(ffly_tapep __tape) {
	ip = 0;
	tape = __tape;
	ff_rosin_init(&ctx);
	ff_rosin_run(&ctx);
	ff_rosin_de_init(&ctx);
}
//# define DEBUG
# ifdef DEBUG
_f_err_t ffmain(int __argc, char const *__argv[]) {
	ffly_tapep tape;

	tape = ffly_tape_new();
	_8_u code[] = {	
		0x08, 0x00, 0x00,
		0x07, 0x00, 0x00,
		0x09,
		0x0b, 0x00, 0x00,
		0x0c, 0x00, 0x00,
		0x00, 0x00, 0x18, 0x00,
		0x0d, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00,
		0x0a
	};

	ffly_tape_insert(tape, code, sizeof(code));
	ffros_exec(tape);

	ffly_tape_raze(tape);
}
# endif
