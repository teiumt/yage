# ifndef __evdev__h
# define __evdev__h
# include "../y_int.h"
# include "../linux/input.h"
#define EVDEVBUF_SZ 4096
struct evdev {
	struct input_id id;
	struct input_event buf[EVDEVBUF_SZ];
	struct input_event padding;/*just in case*/
	_int_u n, at;
	int fd;
};

_int_u evdev_bigdrop(struct evdev*, struct input_event*);
_8_s evdev_next(struct evdev*, struct input_event*);
struct evdev* evdev_from_fd(int);
void evdev_free(struct evdev*);
# endif /*__evdev__h*/
