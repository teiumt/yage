# include "evdev.h"
# include "../types.h"
# include "../m_alloc.h"
# include "../io.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../string.h"
# include "../linux/ioctl.h"
struct evdev* evdev_from_fd(int __fd) {
	struct evdev *d;
	d = m_alloc(sizeof(struct evdev));
	d->fd = __fd;
	d->n = 0;
	d->at = 0;
	int r;
	r = ioctl(__fd, EVIOCGID, &d->id);	
	if (r<0) {
		printf("failed to retrieve ids.\n");
	}
	printf("evdev{bustype: %u, vendor: %u, product: %u, version: %u}.\n", d->id.bustype, d->id.vendor, d->id.product, d->id.version);

	return d;
}
void evdev_free(struct evdev *__d) {
	m_free(__d);
}

_8_s evdev_next(struct evdev *__d, struct input_event *__e) {
	if (__d->at < __d->n) {
		*__e = __d->buf[__d->at++]; 
		return 0;
	}
	_32_s len;
	len = read(__d->fd, __d->buf, EVDEVBUF_SZ*sizeof(struct input_event));
	if (len<=0)
		return -1;
	__d->n = len/sizeof(struct input_event);
	__d->n--;
	*__e = __d->buf[0];
	__d->at = 1;
	return 0;
}

_int_u evdev_bigdrop(struct evdev *__d, struct input_event *__buf) {
	_32_s len;
	len = read(__d->fd, __buf, EVDEVBUF_SZ*sizeof(struct input_event));
	if (len<=0) {
		return 0;
	}

	return len/sizeof(struct input_event);
}
/*
#include "../time.h"
_err_t main(int __argc, char const *__argv[]) {
	int fd;
	fd = open(__argv[1], O_RDONLY, 0);
	if (fd<0) {
		printf("failed to open.\n");
		return -1;
	}

	struct evdev *d;
	d = evdev_from_fd(fd);

	struct input_event *ev;
	_8_s r;
	while(1) {
		mem_set(&ev, 0, sizeof(struct input_event));
		r = evdev_next(d, &ev);
		if (!r) {
			printf("TYPE: %u, CODE: %u, VALUE: %d.\n", ev.type, ev.code, ev.value);
			
		}
		doze(0,8);
		ffly_fdrain(_ffly_out);
	}

	evdev_free(d);
	close(fd);
}*/
