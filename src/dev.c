# include "dev.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "ffly_def.h"
# include "dev/man.h"
# include "tmu.h"
#define NLOAD 1


struct driver {
	struct f_dd d;
	_64_u interval;
	void(*load)(struct f_dd*);
};

static struct driver drv[] = {
	{.interval=TIME_FMS(200),.load=f_dv_imitate}
};

void f_dev_load(void) {
	struct driver *d;
	_int_u i;
	i = 0;
	for(;i != NLOAD;i++) {
		d = drv+i;
		d->load(&d->d);
	}
}

void static
_update(struct driver *__dv) {
//	__dv->d.update();
}

void f_dev_init(void) {
	f_dev_load();
return;
	struct driver *d;
	_int_u i;
	i = 0;
	for(;i != NLOAD;i++) {
		d = drv+i;
		f_dm_task((void*)_update, (_ulonglong)d, d->interval);
	}
}
