#include "opt.h"
#include "m_alloc.h"
#include "lib/hash.h"
#include "string.h"
#include "io.h"
void y_opts_prep(struct y_opt_comm *__com,struct y_opt *__opts,_int_u __opt_n) {
	__com->charmap = m_alloc(256);
	mem_set(__com->charmap,0xff,256);
	f_lhash_init(&__com->hm);
	_int_u i;
	i = 0;
	for(;i != __opt_n;i++){
		struct y_opt *opt;
		opt = __opts+i;

		if(opt->len == 1){
			__com->charmap[*opt->str] = i;
		}else{
			f_lhash_put(&__com->hm,opt->str,opt->len,opt);
		}
	}
	__com->opts = __opts;
	__com->ln = 0;
}

void y_opts(struct y_opt_comm *__com,char const *__argv[],_int_u __argc){
	_int_u i;
	i = 0;
	for(;i != __argc;i++){
		_int_u len;
		char const *arg;
		arg = __argv[i];
		
		len = str_len(arg+1);
		struct y_opt *opt;
		if(len == 1){
			opt = __com->opts+__com->charmap[arg[1]];
		}else{
			opt = f_lhash_get(&__com->hm,arg+1,len);
		}

		if(!opt){
			return;
		}
		opt->val = __argv[++i];
		if(opt->bits&OPT_FRONTLINE){
			__com->list[__com->ln++] = opt;
		}
	}
}
