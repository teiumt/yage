# ifndef __ffly__types__h
# define __ffly__types__h
# include "linux/types.h"
# include "y_int.h"

# ifdef __cplusplus
# ifdef __ffly_use_opencl
#   include <CL/cl.hpp>
# endif

# ifdef __ffly_use_cuda
#   include <cuda_runtime.h>
# endif
# endif

# ifdef  __ffly_source
typedef _8_s _err_t;
typedef _8_s _f_err_t;
typedef _8_u _f_byte_t;
typedef _16_u _f_addr_t;
typedef _32_u _f_off_t;
typedef _32_u _f_size_t;
typedef _32_s _f_ssize_t;
typedef _8_u _f_bool_t;
#ifdef __fflib
typedef __k_dev_t dev_t;
#endif
typedef _8_s _y_err;
typedef float _y_float;
// id
typedef _int_u _ff_id_t;
typedef _int_u __ff_id_t;
typedef _int_u* ff_id_t;

typedef _16_u _f_tid_t;

// atomic
typedef _64_u ff_atomic_u64_t;
typedef _64_u ff_atomic_u32_t;
typedef _64_u ff_atomic_u16_t;
typedef _64_u ff_atomic_u8_t;
typedef _64_u ff_atomic_uint_t;

typedef _8_u mlock;
typedef _8_u ff_flag_t;
typedef _32_u ff_fd_t;

typedef _8_u ff_cond_lock_t;
typedef _32_u ff_cache_t;

// resource
typedef _32_u _f_rs_t;

typedef float _f_32;
typedef double _f_64;

typedef const char* _const_str;
typedef _64_u _atom_64_u;
typedef _64_u _atom_32_u;
typedef _64_u _atom_16_u;
typedef _64_u _atom_8_u;
typedef _64_u _atom_int_u;
# endif


# ifdef __cplusplus
namespace ff {
typedef _f_err_t err_t;
typedef _f_byte_t byte_t;
typedef _f_off_t off_t;
typedef _f_addr_t addr_t;
typedef _f_size_t size_t;
typedef _f_ssize_t ssize_t;
typedef _f_bool_t bool_t;

# ifdef __ffly_use_opencl
typedef cl_int cl_err_t;
# endif
# ifdef __ffly_use_cuda
typedef cudaError_t cl_err_t;
# endif
}
# endif
# endif /*__ffly__types__h*/
