// command submission
# include "../amdgpu.h"
# include "../string.h"
# include "../m/alloca.h"
# include "../io.h"
_64_u amdgpu_cs_submit(struct amdgpu_ctx *__ctx, struct amdgpu_cs_request *__req) {

	union drm_amdgpu_cs cs;
	mem_set(&cs, 0, sizeof(union drm_amdgpu_cs));

	_int_u n;
	n = __req->ib_n;
	_64_u *cnk_arr;
	cnk_arr = (_64_u*)_alloca(n*sizeof(_64_u));
	struct drm_amdgpu_cs_chunk *chunks;
	struct drm_amdgpu_cs_chunk_data *cnk_data;
	chunks = (struct drm_amdgpu_cs_chunk*)_alloca(n*sizeof(struct drm_amdgpu_cs_chunk));
	cnk_data = (struct drm_amdgpu_cs_chunk_data*)_alloca(n*sizeof(struct drm_amdgpu_cs_chunk_data));

	_int_u i;
	i = 0;
	for(;i != n;i++) {
		struct amdgpu_cs_ib_info *ib;
		struct drm_amdgpu_cs_chunk *cnk;
		struct drm_amdgpu_cs_chunk_data *dat;
		ib = __req->ib+i;

		cnk_arr[i] = (_64_u)(cnk = (chunks+i));
		cnk->chunk_id = AMDGPU_CHUNK_ID_IB;
		cnk->length_dw = sizeof(struct drm_amdgpu_cs_chunk_ib)/4;
		cnk->chunk_data = (_64_u)(dat = (cnk_data+i));


		dat->ib_data._pad = 0;
		dat->ib_data.flags = ib->flags;
		dat->ib_data.va_start = ib->addr;
		dat->ib_data.ib_bytes = ib->size<<2;
		dat->ib_data.ip_type = __req->ip_type;
		dat->ib_data.ip_instance = __req->ip_inst;
		dat->ib_data.ring = __req->ring;
	}

	cs.in.ctx_id = __ctx->ctx_id;
	cs.in.num_chunks = n;
	cs.in.chunks = (_64_u)cnk_arr;
	cs.in.bo_list_handle = __req->list_handle;
	int r;

	printf("command submit.\n");
	r = drm_cmdreadwrite(__ctx->dev->fd, DRM_AMDGPU_CS, &cs, sizeof(union drm_amdgpu_cs));
	if (r == -1) {
		printf("command submittion failure.\n");
	}
	return cs.out.handle;
}

int amdgpu_cs_wait_fence(struct amdgpu_cs_fence *__fence, _int_u __n, _8_u __waitall, _64_u __timeout, _32_u *__status, _32_u *__first) {
	struct drm_amdgpu_fence *drm;
	drm = _alloca(sizeof(struct drm_amdgpu_fence)*__n);
	_int_u i;
	i = 0;
	struct drm_amdgpu_fence *to;
	struct amdgpu_cs_fence *from;
	for(;i != __n;i++) {
		to = drm+i;
		from = __fence+i;
		to->ctx_id = from->ctx->ctx_id;
		to->ip_type = from->ip_type;
		to->ip_inst = from->ip_inst;
		to->ring = from->ring;
		to->seq_no = from->seq_no;
	}
	union drm_amdgpu_wait_fences args;
	mem_set(&args, 0, sizeof(args));
	int r;
	args.in.fences = (_64_u)drm;
	args.in.fence_count = __n;
	args.in.wait_all = __waitall;
	args.in.timeout_ns = __timeout;
	r = drm_ioctl(__fence->ctx->dev->fd, DRM_IOCTL_AMDGPU_WAIT_FENCES, &args);
	if (r == -1) {
		printf("failed to wait for fences.\n");
		return -1;
	} else {
		printf("fence waited.\n");
	}

	*__status = args.out.status;
	*__first = args.out.first_signaled;
	return 0;
}
