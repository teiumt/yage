# include "../amdgpu.h"
# include "../drm.h"
# include "../m_alloc.h"
# include "../string.h"
# include "../io.h"

void amdgpu_showdevinfo(struct drm_amdgpu_info_device *__info) {
	ffly_fprintf(ffly_err,"device_id:\t\t%u\n"
			"chip_rev:\t\t%u\n"
			"external_rev:\t\t%u\n"
			"pci_rev:\t\t%u\n"
			"family:\t\t%u\n"
			"num_shader_engines:\t\t%u\n"
			"num_shader_arrays_per_engine:\t\t%u\n"
			"gpu_counter_freq:\t\t%u\n"
			"max_engine_clock:\t\t%u\n"
			"max_memory_clock:\t\t%u\n"
			"cu_active_number:\t\t%u\n"
			"enabled_rb_pipes_mask:\t\t%u\n"
			"num_rb_pipes:\t\t%u\n"
			"num_hw_gfx_contexts:\t\t%u\n"
			"ids_flags:\t\t%u\n"
			"virtual_address_offset:\t\t%lu\n"
			"virtual_address_max:\t\t%lu\n"
			"virtual_address_alignment:\t\t%lu\n"
			"pte_fragment_size:\t\t%u\n"
			"gart_page_size:\t\t%u\n"
			"ce_ram_size:\t\t%u\n"
			"vram_type:\t\t%u\n"
			"vram_bit_width:\t\t%u\n"
			"vce_harvest_config:\t\t%u\n"
			"gc_double_offchip_lds_buf:\t\t%u\n"
			"prim_buf_gpu_addr:\t\t%u\n"
			"pos_buf_gpu_addr:\t\t%u\n"
			"cntl_sb_buf_gpu_addr:\t\t%u\n"
			"param_buf_gpu_addr:\t\t%u\n"
			"prim_buf_size:\t\t%u\n"
			"pos_buf_size:\t\t%u\n"
			"cntl_sb_buf_size:\t\t%u\n"
			"param_buf_size:\t\t%u\n"
			"wave_front_size:\t\t%u\n"
			"num_shader_visible_vgprs:\t\t%u\n"
			"num_cu_per_sh:\t\t%u\n"
			"num_tcc_blocks:\t\t%u\n"
			"gs_vgt_table_depth:\t\t%u\n"
			"gs_prim_buffer_depth:\t\t%u\n"
			"max_gs_waves_per_vgt:\t\t%u\n"
			"high_va_offset:\t\t%u\n"
			"high_va_max:\t\t%u\n",
			__info->device_id,
			__info->chip_rev,
			__info->external_rev,
			__info->pci_rev,
			__info->family,
			__info->num_shader_engines,
			__info->num_shader_arrays_per_engine,
			__info->gpu_counter_freq,
			__info->max_engine_clock,
			__info->max_memory_clock,
			__info->cu_active_number,
			__info->enabled_rb_pipes_mask,
			__info->num_rb_pipes,
			__info->num_hw_gfx_contexts,
			__info->ids_flags,
			__info->virtual_address_offset,
			__info->virtual_address_max,
			__info->virtual_address_alignment,
			__info->pte_fragment_size,
			__info->gart_page_size,
			__info->ce_ram_size,
			__info->vram_type,
			__info->vram_bit_width,
			__info->vce_harvest_config,
			__info->gc_double_offchip_lds_buf,
			__info->prim_buf_gpu_addr,
			__info->pos_buf_gpu_addr,
			__info->cntl_sb_buf_gpu_addr,
			__info->param_buf_gpu_addr,
			__info->prim_buf_size,
			__info->pos_buf_size,
			__info->cntl_sb_buf_size,
			__info->param_buf_size,
			__info->wave_front_size,
			__info->num_shader_visible_vgprs,
			__info->num_cu_per_sh,
			__info->num_tcc_blocks,
			__info->gs_vgt_table_depth,
			__info->gs_prim_buffer_depth,
			__info->max_gs_waves_per_vgt,
			__info->high_va_offset,
			__info->high_va_max
	);
}


int amdgpu_query_dev_info(int __fd, struct drm_amdgpu_info_device *__info) {
	struct drm_amdgpu_info req;
	mem_set(&req, 0, sizeof(struct drm_amdgpu_info));
	req.ret_p = (_64_u)__info;
	req.ret_size = sizeof(struct drm_amdgpu_info_device);
	req.query = AMDGPU_INFO_DEV_INFO;
	int ret;
	ret = drm_cmdwrite(__fd, DRM_AMDGPU_INFO, &req, sizeof(struct drm_amdgpu_info));
	if (ret == -1) {
		printf("write cmd failed.\n");
		return -1;
	}
	return 0;
}

struct amdgpu_dev*
amdgpu_dev_init(int __fd) {
	struct amdgpu_dev *dev;
	dev = (struct amdgpu_dev*)m_alloc(sizeof(struct amdgpu_dev));

	dev->fd = __fd;
	// query is required! for memory and limits
	if(amdgpu_query_dev_info(__fd, &dev->info) == -1) {
		printf("failed to query device.\n");
		return NULL;
	}
	amdgpu_showdevinfo(&dev->info);	
	_64_u start;
	start = dev->info.virtual_address_offset;

	// dont care for now
	/*
		just to make sure higher 32-bits are zero
	*/

	_64_u max;
/*	if (dev->info.virtual_address_max>0x100000000)
		max = 0x100000000;
	else
		max = dev->info.virtual_address_max;
	*/
	/*
		this is as high a 32-bit address can go for now only 32-bit is working as yes.
	*/
	max = 0x100000000;
	ffly_fprintf(ffly_err,"MAX_ADDRESS: %lu, start: %lu\n", max,start);
	amdgpu_vam_init(&dev->vam,max, start);
	return dev;
}

void
amdgpu_dev_de_init(struct amdgpu_dev *__dev) {
	m_free(__dev);
}
