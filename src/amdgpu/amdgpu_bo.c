# include "../amdgpu.h"
# include "../m_alloc.h"
# include "../linux/mman.h"
# include "../io.h"
# include "../string.h"
# include "../system/errno.h"
struct amdgpu_bo_list*
amdgpu_bo_list(struct amdgpu_dev *__dev,
	_int_u __nbos, void *__list)
{
	union drm_amdgpu_bo_list args;
	mem_set(&args, 0, sizeof(args));
	args.in.operation = AMDGPU_BO_LIST_OP_CREATE;
	args.in.bo_number = __nbos;
	args.in.bo_info_size = sizeof(struct drm_amdgpu_bo_list_entry);
	args.in.bo_info_ptr = (_64_u)__list;
	int r;
	r = drm_cmdreadwrite(__dev->fd, DRM_AMDGPU_BO_LIST, &args, sizeof(args));
	if (r == -1) {
		printf("failed to create list, size: %u.\n", __nbos);
	}

	struct amdgpu_bo_list *list;
	list = m_alloc(sizeof(struct amdgpu_bo_list));
	list->handle = args.out.list_handle;
	return list;
}

void amdgpu_bo_list_destroy(struct amdgpu_dev *__dev, struct amdgpu_bo_list *__list) {
	union drm_amdgpu_bo_list args;
	mem_set(&args, 0, sizeof(args));
	args.in.operation = AMDGPU_BO_LIST_OP_DESTROY;
	args.in.list_handle = __list->handle;
	int r;
	r = drm_cmdreadwrite(__dev->fd, DRM_AMDGPU_BO_LIST, &args, sizeof(args));
	if (r<0) {
		printf("failed to destroy BO-list.\n");
	}
}

int amdgpu_bo_va_op(struct amdgpu_dev *__dev, struct amdgpu_bo *__bo, _32_u __op, _64_u __flags,
	_64_u __bo_offset, _64_u __addr, _64_u __size)
{
	struct drm_amdgpu_gem_va va;
	va.handle = __bo->handle;
	va.op = __op;
	va.flags = __flags;
	va.bo_offset = __bo_offset;
	va.va_addr = __addr;
	va.map_size = __size;
	int r;
	r = drm_cmdreadwrite(__dev->fd, DRM_AMDGPU_GEM_VA, &va, sizeof(struct drm_amdgpu_gem_va));
	if (r<0) {
		printf("va operation failure, %s, size: %u, dev: %u, buffer: %u, operation: %u, address: %x\n", strerror(errno),__size,__dev->fd,va.handle,__op,__addr);
	}else{
		printf("successfull va operation! with dev: %u, buffer: %u.\n",__dev->fd,va.handle);
	}
	return r;
}

void
amdgpu_bo_alloc(struct amdgpu_dev *__dev, struct amdgpu_bo *b, _64_u __size,
	_64_u __alignment, _64_u __domains, _64_u __flags)
{
	union drm_amdgpu_gem_create args;
	args.in.bo_size = __size;
	args.in.domains = __domains;
	args.in.domain_flags = __flags;
	args.in.alignment = __alignment;
	b->dev = __dev;
	b->size = __size;
	b->cpu_ptr = NULL;
	int r;
	r = drm_cmdreadwrite(__dev->fd, DRM_AMDGPU_GEM_CREATE,
		&args, sizeof(union drm_amdgpu_gem_create));
	if (r == -1) {
		printf("drm readwrite failue.\n");
		m_free(b);
		return NULL;
	}
	b->handle = args.out.handle;
	printf("AMDGPU------BUFFER OBJECT: %u.\n", b->handle);
	return b;
}

void static
kms_close(int __fd, _64_u __handle) {
	int r;
	struct drm_gem_close args;
	args.handle = __handle;
	r = drm_ioctl(__fd, DRM_IOCTL_GEM_CLOSE, &args);
	if (r == -1)
		printf("ioctl failure kms close.\n");
}

void amdgpu_bo_free(struct amdgpu_dev *__dev, struct amdgpu_bo *__bo) {
	if (__bo->cpu_ptr != NULL)
		amdgpu_bo_cpu_unmap(__bo);
	kms_close(__dev->fd, __bo->handle);
}

void amdgpu_bo_cpu_map(struct amdgpu_dev *__dev, struct amdgpu_bo *__bo) {
	union drm_amdgpu_gem_mmap args;
	args.in.handle = __bo->handle;
	int r;
	r = drm_cmdreadwrite(__dev->fd, DRM_AMDGPU_GEM_MMAP,
		&args, sizeof(union drm_amdgpu_gem_mmap));
	if (r == -1) {
		printf("command failure cpu map.\n");
	}

	void *ptr;
	ptr = mmap(NULL, __bo->size, PROT_READ|PROT_WRITE, MAP_SHARED, __dev->fd, args.out.addr_ptr);
	if (!ptr) {
		printf("failed to map.\n");
	}

	printf("AMDGPU: map-size: %u, cpu_ptr: %p\n", __bo->size,ptr);
	__bo->cpu_ptr = ptr;
}

void amdgpu_bo_export(struct amdgpu_dev *__dev, struct amdgpu_bo *__bo, _32_u __type, _32_u *__handle) {
	int r;
	switch(__type) {
		case amdgpu_bo_handle_dma_buf_fd:
			/*
				as CLOEXEC flags is here we dont realy have to do much in freeing resources
			*/
			drm_prime_handle_to_fd(__dev->fd, __handle, __bo->handle, DRM_CLOEXEC|DRM_RDWR);
			printf("EXPORT-%u\n",__bo->handle);
		break;
	}
}
#include "../assert.h"
void amdgpu_bo_import(struct amdgpu_dev *__dev, _32_u __type, _32_u __handle, struct amdgpu_bo_import_result *__res) {
	int r;
	if (__type == amdgpu_bo_handle_dma_buf_fd) {
		_32_u handle;
		r = drm_prime_fd_to_handle(__dev->fd, __handle, &handle);
		if (r<0) {
			assert(1 == 0);
			printf("failed to import.\n");
			return;//error
		}

		_32_u size;
		size = lseek(__handle,0,SEEK_END);
		lseek(__handle, 0, SEEK_SET);
		struct amdgpu_bo *bo = __res->bo;
		bo->handle = handle;
		bo->size = size;
		bo->dev = __dev;

		printf("IMPORTING fd %u to handle-%u.\n",__handle,handle);
	}
}

void amdgpu_bo_cpu_unmap(struct amdgpu_bo *__bo) {
	munmap(__bo->cpu_ptr, __bo->size);
	__bo->cpu_ptr = NULL;
}
