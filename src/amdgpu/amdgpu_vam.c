# include "../amdgpu.h"
# include "../m_alloc.h"
# include "../io.h"
#include "../string.h"
#include "../assert.h"
#include "../ffly_def.h"
#define hole amdgpu_va_hole



#define BOILER(__mgr,__addr)\
	(__mgr)->boiler[AMDGPU_HOLE_BANK(__addr)]
#define HOLEAT(__mgr,__addr,__size)\
	BOILER(__mgr,__addr).holes[boilerfor(__size)]
#define _link(__h, __mgr)\
	__h->h = HOLEAT(__mgr,__h->offset,__h->size);\
	if (HOLEAT(__mgr,__h->offset,__h->size) != NULL) {\
		HOLEAT(__mgr,__h->offset,__h->size)->bk = __h;\
	}\
	HOLEAT(__mgr,__h->offset,__h->size) = __h;\
	if (__h->offset>=BOILER(__mgr,__h->offset).topoff) {\
		BOILER(__mgr,__h->offset).top = BOILER(__mgr,__h->offset).holes+boilerfor(__h->size);\
	}

#define _delink(__h, __mgr)\
	if (HOLEAT(__mgr,__h->offset,__h->size) == __h) {\
		HOLEAT(__mgr,__h->offset,__h->size) = __h->h;\
		if (__h->h != NULL) {\
			__h->h->bk = NULL;\
		}\
	}else{\
	__h->bk->h = __h->h;\
	if (__h->h != NULL) {\
		__h->h->bk = __h->bk;\
	}\
	}
#define ALIGN(__val, __alignto)\
	((__val)&~(__alignto))

_64_u static boilerfor(_64_u __size) {
	if (!(__size&0xffffff00)){
		return 0;	
	}
	if (!(__size&0xffff0000)){
		return 2;	
	}
	if (!(__size&0xff000000)){
		return 3;	
	}
	return 4;
}
struct hole static*
hole_new(struct amdgpu_vam *__mgr) {
	struct hole *h;
	h = (struct hole*)m_alloc(sizeof(struct hole));
	h->magic = 'K';
	h->h = NULL;
	h->bk = NULL;
	__mgr->n++;
	return h;
}

void static
hole_destroy(struct hole *__h, struct amdgpu_vam *__mgr) {
	m_free(__h);
}

struct amdgpu_vam* amdgpu_vam_alloc(void) {
	
	return m_alloc(sizeof(struct amdgpu_vam));
}

void amdgpu_vam_init(struct amdgpu_vam *mgr, _64_u __start, _64_u __depth) {
	printf("AMDGPU_VAM_INIT.\n");
	mgr->offset = __start;
	mgr->stop = __depth;
	mgr->n = 0;
	mgr->boiler = m_alloc(sizeof(struct amdgpu_boiler)*AMDGPU_HOLE_BANK_AMOUNT);
	_int_u i;
	i = 0;
	for(;i != AMDGPU_HOLE_BANK_AMOUNT;i++) {
		struct amdgpu_boiler *b = mgr->boiler+i;
		mem_set(b->holes,0,sizeof(void*)*64);
		b->top = NULL;
	}

	struct hole *h;
	h = hole_new(mgr);
	h->offset = __start;
	h->size = __start-__depth;
	mgr->max = __start;
	ffly_fprintf(ffly_err, "max address: %u, %u.\n",__start, __depth);
	HOLEAT(mgr,h->offset,h->size) = h;
}

_8_s static 
murgeattempt(struct amdgpu_vam *__mgr, struct hole *hh, _64_u offset, _64_u size) {
	/*
		loop over each hole and try to remurge stuff
	*/
_tryit0:
	if (hh != NULL) {
		assert(hh->magic == 'K');
		/*
			if theres a hole below then murge the two
		*/
		if (offset == hh->offset-hh->size) {
			_delink(hh,__mgr);
			hh->size+=size;	
			_link(hh,__mgr);
			goto _fini;
		/*
			free hole from above
		*/
		} else if (hh->offset == offset-size) {
			/*
				as the offset is changing we need to delink it
			*/
			_delink(hh,__mgr);
			hh->offset = offset;
			hh->size+=size;
			_link(hh,__mgr);
			goto _fini;
		}
		hh = hh->h;
		goto _tryit0;
	}
	return -1;

_fini:
	return 0;
}

void amdgpu_va_free(struct amdgpu_vam *__mgr, struct amdgpu_va *__va) {
	struct hole *h;
	if (!__va)
		return;
	_64_u offset = __va->addr+__va->size;
	struct hole *hh;
	hh = HOLEAT(__mgr,offset,__va->size);
	if (!murgeattempt(__mgr,hh,offset,__va->size)) {
		goto _fini;
	}
	/*
		if are data crosses bank boundaries, then make the attempt to murge it.
	*/
	if (AMDGPU_HOLE_BANK(offset-__va->size) != AMDGPU_HOLE_BANK(offset)) {
		hh = *BOILER(__mgr,offset-__va->size).top;
		if (!murgeattempt(__mgr,hh,offset,__va->size)) {
//			printf("HIT!.\n");
			goto _fini;
		}
	}

	h = hole_new(__mgr);
	h->size = __va->size;
	h->offset = offset;
	_link(h, __mgr);
_fini:
	__mgr->alloced-=__va->size;
	m_free(__va);
}

void amdgpu_va_tree(struct amdgpu_vam *__mgr) {
	struct hole *h;
	_int_u i, j,k;
	i = 0;
	for(;i != AMDGPU_HOLE_BANK_AMOUNT;i++) {
		k = 0;
		for(;k != 64;k++) {
			h = __mgr->boiler[i].holes[k];
			j = 0;
			while(h != NULL) {
				printf("%u/%u/%u: hole: offset: %x, size: %u, %x\n", i, k,j++,h->offset, h->size,h->offset-h->size);
				h = h->h;
			}
		}
	}
}


void amdgpu_va_stat(struct amdgpu_vam *__mgr) {
	amdgpu_va_tree(__mgr);
}
/*
	should never happen

	TODO:
		schedule automatic declutter of boiler blocks.
		somehow?
*/
void static declutter(struct amdgpu_vam *__mgr) {
	printf("declutter.\n");
	_int_u i,j,k,m;
	i = 0;
	for(;i != AMDGPU_HOLE_BANK_AMOUNT;i++) {
		k = 0;
		for(;k != 64;k++) {
			struct hole *h;
			h = __mgr->boiler[i].holes[k];
			j = 0;
			for(;j != AMDGPU_HOLE_BANK_AMOUNT;j++) {
				m = 0;
				for(;m != 64;m++) {
					struct hole *hh;
					hh = __mgr->boiler[j].holes[m];
					while(hh != NULL) {
						if (hh != h) {
							assert(hh->magic == 'K');
							if (!murgeattempt(__mgr,h,hh->offset,hh->size)){
								_delink(hh, __mgr);
								m_free(hh);
								__mgr->n--;
								break;
							}
						}
						hh = hh->h;
					}
				}
			}
		}
	}
}

/*
	find a hole that the correct size or good enoth size.
	shink/squeeze that hole 
*/
static _64_u cntr = 0;
struct amdgpu_va* amdgpu_va_alloc(struct amdgpu_vam *__mgr, _64_u __size, _64_u __alignment) {	
	struct hole *h, *hh, *hn;
	struct amdgpu_va *va;
	_64_u size;

//	printf("left: %llu, holes: %llu\n", __mgr->holes->offset, __mgr->n);

	if (!__alignment)
		__alignment |= 1;
	_64_u align;
	align = (AMDGPU_GPU_PAGE_SIZE-1)|(__alignment-1);
	size = (__size+align)&(~align);
	_int_u j,i,k;
	_64_u addr, totsize;
_tryit:
	j = 0;
	i = 0;
	while(j != AMDGPU_HOLE_BANK_AMOUNT) {
		k = boilerfor(size);
		for(;k != 64;k++) {
		h = __mgr->boiler[j].holes[k];
		assert(j<0x100 && k<64);
		while(h != NULL) {
			if (h->size<size) {
//				printf("hole to small, by: %u-bytes, got %u need %u.\n", size-h->size, h->size, size);
				goto _sk;
			}
		//	printf("hole found.\n");
			addr = h->offset-size;
			/*
				align on page boundy
			*/
			addr &= ~(AMDGPU_GPU_PAGE_SIZE-1);
//			printf("%u: %lu, %lu, --- %lu\n", i, addr, h->offset-h->size, h->size);
			/*
				make sure this can be done
			*/
			if (addr>=h->offset-h->size) {
//				printf("hole is usable.\n");
				_delink(h, __mgr);
				h->size-=(totsize = (h->offset-addr));
				h->offset = addr;
				_link(h, __mgr);

				if (!h->size) {
				//	printf("hole no longer has mass to removeing.\n");
					_delink(h, __mgr);
					m_free(h);
					__mgr->n--;
				} 
		//		printf("----> %lu.\n", h->size);
				goto _sk0;
			}
		_sk:
			i++;
			h = h->h;
		}
		}
		j++;
	}
	amdgpu_va_stat(__mgr);
	printf("dident find any space, in VA address.\n");
	declutter(__mgr);
	goto _tryit;
_sk0:
	va = (struct amdgpu_va*)m_alloc(sizeof(struct amdgpu_va));
	va->addr = addr;
	va->size = totsize;
	/*
		ensure that we never give somthing that is larger then the MAX
	*/
	assert(!(va->addr&~(__mgr->max-1)));
	assert(totsize>0);
	va->mgr = __mgr;
#define NODEBUG
#ifndef NODEBUG
	if (cntr>0xffff) {
#endif
	printf("dished out address: %x - %x, total-holes: %u, %x\n", addr,addr+totsize,__mgr->n,addr>>24);
#ifndef NODEBUG
	cntr = 0;
	}else{
		cntr++;
	}
#endif
	__mgr->alloced+=totsize;
#ifndef NODEBUG
	ffly_fprintf(ffly_err, "AMDGPU alloced: %lx, max: %lx\n",__mgr->alloced,__mgr->max);
#endif
	return va;
}
