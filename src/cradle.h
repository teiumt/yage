# ifndef __f__cradle__h
# define __f__cradle__h
# include "y_int.h"
/*
	DO NOT INCLUDE (USER)

	what is this??

	a bed for all global

	i like static typing to a degree when it gets to this scale it becomes unmanageable
	so to fix it we place all global non local static defines here e.g. fsop struc

	the core structure of firefly
*/
#define __struconly
# include "system/fs.h"
# include "tmh.h"
# include "pcore.h"
#define F_CRADLE(__what) _f_cradle.__what
struct f_cradle {
	// place context here too
	struct {
		struct ffly_fsop *fs;
	} sys;

	struct f_tmh_s tmh;
	struct pcore pcores[7];
	_int_u npc;
};

extern struct f_cradle _f_cradle;
# endif /*__f__cradle__h*/
