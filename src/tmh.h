# ifndef __f__tmh__h
# define __f__tmh__h
# include "y_int.h"
# include "misc.h"
/*
	ohhhh take me home to the place, i belong

	i said many times i dont give a shit about naming
	as it can change over time.

	what is this?????

	say we store somthing at index 0 size: 100

	and define is such
	#define MY_IDX 0
*/
/*
	permentate storage
*/
#define TMH_PAGE_SHFT 4
#define TMH_PAGE_SIZE (1<<TMH_PAGE_SHFT)
#define TMH_PAGE_MASK (TMH_PAGE_SIZE-1)
typedef struct f_tmh_area {
	void *priv;
	_int_u size;
} *f_tmh_areap;

// record is visable
#define F_TMH_RVIS 0x01
// not mapped yet
#define F_TMH_NMY 0x02
#define F_TMH_WMAPPED F_ST_MAPPED
// working flags mask
/*
	for non local flags and apart of the ST flags domain
*/
#define F_TMG_WFM 0xf
typedef struct f_tmh_rec {
	void *priv;
	f_tmh_areap area;
	_8_u flags;
} *f_tmh_recp;

typedef struct f_tmh_pt {
	f_tmh_recp *records;
	_int_u page_c;
} *f_tmh_ptp;

struct f_tmh_recinfo {
	_int_u a_size;
	_int_u idx;
	void *a_priv;
};

// working structure
typedef struct f_tmh_wok {
	f_tmh_recp r;
	_8_u flags;
} *f_tmh_wokp;
// rename
// backgrow
typedef struct f_tmh_cap *f_tmh_capp;
typedef struct f_tmh_ctx *f_tmh_ctxp;
typedef struct f_tmh_bg {
	void(*rec_new)(f_tmh_capp, void*, struct f_tmh_recinfo*);
} *f_tmh_bgp;

typedef struct f_tmh_ctx {
	f_tmh_ptp pt;
	f_tmh_capp c;
	void *priv;
} *f_tmh_ctxp;

struct f_tmh_glob {
	void(*format)(void(*)(void*, _int_u));
};

typedef struct f_tmh_ops {
	void*(*ctx_new)(f_tmh_capp);
	void(*ctx_destory)(void*);
	void(*init)(void*);
	void(*deinit)(void*);

	void(*save)(void*);
	void(*load)(void*);
	void*(*rec_new)(void*, _int_u);
	void(*rec_destroy)(void*, void*);
	/*
		bound record to mapping
	*/
	void(*bound)(void*, void*, void*);

	void*(*map)(void*, _int_u);
	void(*unmap)(void*, void*);
	void(*write)(void*, void*, void*, _int_u, _int_u);
	void(*read)(void*, void*, void*, _int_u, _int_u);
	void(*test)(void*);
} *f_tmh_opsp;

typedef struct f_tmh_ag {
	_int_u idx, size;
	f_tmh_areap a;
} *f_tmh_agp;

typedef struct f_tmh_cap {
    f_tmh_bgp bg;
	struct f_tmh_ops ops;
	f_tmh_ctxp ctx;
} *f_tmh_capp;

typedef struct f_tmh_s {
	struct f_tmh_cap c;
} *f_tmh_sp;

extern struct f_tmh_glob f_tmh_file_glob;

void f_tmh_init(void);
void f_tmh_de_init(void);

f_tmh_wokp f_tmh_open(_64_u);
void f_tmh_close(f_tmh_wokp);
void f_tmh_map(f_tmh_wokp, _int_u);
void f_tmh_remap(f_tmh_wokp, _int_u);
void f_tmh_unmap(f_tmh_wokp);
void f_tmh_recat(f_tmh_agp, _int_u);
void f_tmh_add(f_tmh_agp, _int_u);
void f_tmh_write(f_tmh_wokp, void*, _int_u, _int_u);
void f_tmh_read(f_tmh_wokp, void*, _int_u, _int_u);
# endif /*__f__tmh__h*/
