# ifndef __ffly__mug__h
# define __ffly__mug__h
# include "y_int.h"
# include "types.h"
# include "net.h"

/*
	rename and redesign

	purpose:
		providing a means to access infomation regarding
		the underlying mechanics of protocols used and other additional information
		that may be useful to the client.

		short like http but text and low level just a text document that
		may be of use to the client to know like status, users.

		NOTE:
			only to be used for human infomation.
*/
enum {
	_ffly_mtp_rep_err	
};

enum {
	_ff_mtp_err_ptl,
	_ff_mtp_err_mst
};

typedef struct ffly_mtp_rep {
	_8_u type;
	_8_u err;
} *ffly_mtp_repp;

char const* ffly_mtp_errst(_8_u);

_f_err_t ffly_mtp_snd_report(FF_SOCKET*, ffly_mtp_repp);
_f_err_t ffly_mtp_rcv_report(FF_SOCKET*, ffly_mtp_repp);

_f_err_t ffly_mtp_snd(FF_SOCKET*, void*, _int_u);
void* ffly_mtp_rcv(FF_SOCKET*, _int_u*, _f_err_t*);

void* ffly_mtp_req(FF_SOCKET*, char const*, _int_u*);

_f_err_t ffly_mtpd_prepare(char const*);
_f_err_t ffly_mtpd_open();

_f_err_t ffly_mtpd_start();

_f_err_t ffly_mtpd_close();
_f_err_t ffly_mtpd_cleanup();
# endif /*__ffly__mtp__h*/
