#ifndef __sched__struc__h
#define __sched__struc__h
#include "y_int.h"
#include "mutex.h"
#include "list.h"
struct sched_entity;

struct sched_list{
	struct y_iwlist head;
	struct y_iwlist *cur;
	mlock lock;
};
struct sched_mann{
	struct sched_list list[2];
	_int_u ent_n;
};
typedef struct sched_core {
	struct sched_mann mann[4];
} *sched_corep;
struct pcore;
# include "pcore.h"
typedef struct sched_entity {
	// keep at top
	struct y_iwlist link;
	struct pcore *core;
	struct sched_list *list;
	struct sched_mann *mn;
	_32_u off, id;
	_8_s inuse;
	_64_u last_time;
	_8_s(*func)(_ulonglong);
	_ulonglong arg;
	_64_u iv;
	struct sched_entity *fd;
	_32_u type;
	_32_u flags;
} *sched_entityp;
#endif /*__sched__struc__h*/
