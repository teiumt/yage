# ifndef __f__chunkbuf__h
# define __f__chunkbuf__h
# include "y_int.h"
# include "ffly_def.h"
#define F_CB_CHUNK_SHFT 4
#define F_CB_CHUNK_SIZE (1<<F_CB_CHUNK_SHFT)
#define F_CB_CHUNK_MASK (F_CB_CHUNK_SIZE-1)
struct f_cb_chunk {
	char buf[F_CB_CHUNK_SIZE];
	struct f_cb_chunk *next;
};

struct f_chunkbuf {
	struct f_cb_chunk *c, *c_top;
	_int_u off;
};
#define f_cnkbuf_done(__cb)\
	(__cb)->c->next = NULL;

void f_cnkbuf_init(struct f_chunkbuf*);
void f_cnkbuf_pushout(struct f_chunkbuf*, void*, _int_u);
void f_cnkbuf_clean(struct f_chunkbuf*);
# endif /*__f__chunkbuf__h*/
