#include "file.h"
#include "linux/unistd.h"
#include "linux/fcntl.h"
#include "linux/stat.h"
#include "io.h"
#include "system/errno.h"
struct file_strc {
	int fd;
};

_8_s bopen(struct file *__f,char const *__path,_64_u __opt,_64_u __mode) {
	struct file_strc *s = __f;
	_64_u opt,mode;

	opt = 0;

	if (__opt&4) {
		switch(__opt&3) {
			case 0:
				opt = O_RDONLY;
			break;
			case 1:
				opt = O_WRONLY;
			break;
			case 2:
				opt = O_RDWR;
			break;
		}
	}
	if (__opt&_O_TRUNC)
		opt |= O_TRUNC;
	if (__opt&_O_CREAT)
		opt |= O_CREAT;

	mode = __mode;
	s->fd = open(__path,opt,mode);
	if (s->fd == -1) {
		printf("failed to open file.\n");
		return -1;
	}
	return 0;
}

void bwrite(struct file *__f,void *__buf,_int_u __size) {
	struct file_strc *s = __f;
	int r;

	r = write(s->fd,__buf,__size);
	if(r<0) {
		printf("failed to write: %u-bytes, with error: %s\n",__size,strerror(errno));
	}
}

void bread(struct file *__f,void *__buf,_int_u __size) {
	struct file_strc *s = __f;
	
	read(s->fd,__buf,__size);
}

void bpwrite(struct file *__f,void *__buf,_int_u __size,_64_u __offset) {
	struct file_strc *s = __f;
	pwrite(s->fd,__buf,__size,__offset);
}

void bpread(struct file *__f,void *__buf,_int_u __size,_64_u __offset) {
	struct file_strc *s = __f;
	pread(s->fd,__buf,__size,__offset);
}

void bclose(struct file *__f) {
	struct file_strc *s = __f;
	close(s->fd);
}

void bstat(struct file *__f,struct _stat *__st) {
	struct file_strc *s = __f;
	struct stat st;
	fstat(s->fd,&st);
	__st->st_size = st.st_size;
}
