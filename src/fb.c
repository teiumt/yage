# include <linux/fb.h>
# include <sys/mman.h>
# include <unistd.h>
# include <fcntl.h>
# include <signal.h>
# include <sys/ioctl.h>
# include "y_int.h"
# include <stdio.h>
# include <string.h>
# include <errno.h>
# include <sys/stat.h>
#define WIDTH 64
#define HEIGHT 64
/*
	used for displaying test.ppm
	as image viewers dont work over nfs well they do but dont update to change
	NOTE:
		using linux-fb for ease of use as requires less setup compared to interacting with radeon or amdgpu directly
		also this might be wrong as i havent looked into it a grate degree but as it works i dont realy care
*/
int main() {
	int fb, srcppm;
	fb = open("/dev/fb0", O_RDWR, 0);
	srcppm = open("test.data", O_RDWR, 0);
	if (srcppm<0) {
		printf("failed to load source ppm.\n");
		return -1;
	}
	struct fb_var_screeninfo info;
	ioctl(fb, FBIOGET_VSCREENINFO, &info);

	struct stat st;
	fstat(srcppm, &st);
	_int_u len, srclen;
	srclen = st.st_size;
	len = info.xres*info.yres*4;
	void *pixels, *src;
	pixels = mmap(NULL, len, PROT_READ|PROT_WRITE, MAP_SHARED, fb, 0);
	src = mmap(NULL, srclen, PROT_READ|PROT_WRITE, MAP_SHARED, srcppm, 0);

	_int_u x, y;
	_8_u *px, *_px;
	memset(pixels, 0, len);
	y = 0;
	for(;y != HEIGHT;y++) {
		x = 0;
		for(;x != WIDTH;x++) {
			px = ((_8_u*)pixels)+((x+(y*info.xres))*4);
			_px = ((_8_u*)src)+((x+(y*WIDTH))*4);
			*px = _px[2];
			px[1] = _px[1];
			px[2] = _px[0];
			px[3] = 255;
			//printf("%u,%u,%u\n", *_px, _px[1], _px[2]);
		}
	}
	
	printf("bye bye.\n");
	munmap(pixels, len);
	munmap(src, srclen);
	close (fb);
	close (srcppm);
}
