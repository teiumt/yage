#include "y.h"
#include "types.h"
#include "linux/unistd.h"

#include "m/allocr.h"
#include "io.h"
#include "thread.h"
#include "system/config.h"
#include "m/alloca.h"
#include "string.h"
#include "m_alloc.h"
#include "version.h"
#include "lib.h"
#include "system/error.h"

#include "linux/time.h"
#include "log.h"
#include "msg.h"
#include "bog.h"
#include "proc.h"
#include "lhs.h"
#include "lld.h"
#include "sched.h"
#include "env.h"
#include "strf.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_FIREFLY)
#define CFG_FILE "sys.bole"
_64_u y_bits = 0;
void __f_init_array(void);
void proc_init(void);
void ff_time_init(void);
void ff_sysconf_init(void);

typedef struct _ffopt {
	char const *name, *val;
	struct _ffopt *next;
} ffopt, *ffoptp;

#define ffopt_size sizeof(struct _ffopt)

ffopt static *optbed = NULL;
// rename
void evalopts(char const *__s) {
	char const *p = __s;
	char buf[128];
	char *bufp;
	ffoptp opt, end = NULL;
_again:
	bufp = buf;
	opt = (ffoptp)m_alloc(ffopt_size);
	if (!optbed) 
		optbed = opt;
	if (end != NULL)
		end->next = opt;
	end = opt;
	while(*p == ' ') p++;
	while((*p >= 'a' && *p <= 'z') || *p == '-')
		*(bufp++) = *(p++);
	*bufp = '\0';
	mem_dup((void**)&opt->name, buf, (bufp-buf)+1);
	bufp = buf;

	if (*p == ':') {
		p++;
		while(*p == ' ') p++;
		while(*p != ',' && *p != '\0')
			*(bufp++) = *(p++);
		*bufp = '\0';
		mem_dup((void**)&opt->val, buf, (bufp-buf)+1);
		if (*p == ',') {
			p++;
			goto _again;
		}
	} else
		opt->val = NULL;
	end->next = NULL;
}

/*
	alloca will use this until config is loaded.
*/
#define TMP_ALSSIZE 500

#include "config.h"
#include "system/config.h"
_f_err_t main(int, char const**);
char const static *by = "mrdoomlittle"; 
#include "mod.h"

#include "system/tls.h"
#include "piston.h"
#include "corrode.h"
#include "location.h"
//#include "pulse.h"
_8_i static
check_conf_version(void) {
	_8_i ret;
	char p[] = ffly_version;
	char const *vers;
	vers = *sysconf_get(version);
	if ((ret = mem_cmp(vers, p, ffly_version_len))<0) {
		MSG(WARN, "sysconf out of date.\n")
		_64_u a;
		y4_dec(vers, &a, ffly_version_len);
		if (a>ffly_version_int) {
			MSG(INFO, "config version too in date.\n")
		} else if (a<ffly_version_int) {
			MSG(INFO, "config version out of date.\n")
		}
	} else {
		MSG(INFO, "config in date.\n");
	}
	return ret;
}

#include "linux/stat.h"
#include "linux/fcntl.h"
#include "system/string.h"
#include "system/fs.h"
void ffly_string_init(void);
#include "msg.h"
#include "log.h"
#include "wharf.h"
#include <stdarg.h>
#include "m.h"
// are outputs
/*
	never write to __buf on this function
	also i always pass the length of are data even when theres no need for it(strings)
	as it may not be usfull when copying but when allocating space for it its good to have it
*/
#include "assert.h"
void static log_output(char *__buf, _int_u __len) {
	assert(__buf[__len] == '\0');
	ffly_fprintf(ffly_log, "%s", __buf);
}

void static msg_output(char *__buf, _int_u __len) {
	ffly_log_write(0, __buf, __len);
}
void blt_init(void);
void static _init0(void);
_f_err_t static
init(void) {
	_f_err_t err;

	err = ffly_tls_new();
	if (_err(err)) {
		_ret;
	}

	proc_init();
	proc_ctx_setup();


	err = ffly_ar_init();
	if (_err(err)) {
		_ret;
	}

	ffly_string_init();
	ffly_sysfs(FF_POSIX_FS);
	f_lld_init();
	io_init();

	ffly_msgout = msg_output;
	ffly_log_init(0, log_output);
	tinit();
//	ff_location_init();
#ifndef __ffly_crucial
//	f_whf_connect();
//	f_whf_place();

//	_init0();
//	__f_init_array();
	_init0();
	blt_init();
#endif

#ifndef __ffly_crucial
//	ffly_ss_prime();
	/*
		ignore only if the program goes rogue but usless if threads are too
	*/
/*	int fd;
	fd = open("pid", O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR|S_IWUSR);
	char buf[24];
	_int_u n;
	n = ffly_nots(getpid(), buf);
	write(fd, buf, n);
	close(fd);
*/
#endif
	ff_sysconf_init();
}

/*
	this is an issue
	i dont like the if statments but theres no other way 
	we could use a struct but thats a wast
	or exc memory ????? idk

	i was going to call a func
	example

	test() {
		if ((this) && enabled) {
			switch(routine) {
				init:
				de-init:
			}
		}
	}

	test(BOG, init)


	test(BOG, de-init)

	but now we are back where we started
	calling functions


*/
#define isenabled(__what)\
	if ((_ff_conf->ef&(__what))>0)
/*
	this may look ugly but its as good as its going to get

	it could be done in a more elaborate way but at a cost of speed
	i know it may not matter but it all adds up

	as if we replace the if statments with function calls
	we got from
	cmp 
	jmp {1 byte + extra}

	to 
	call {dword+extra}
	a load of shit

	TODO:
	move in to its own file
*/
//#ifndef __ffly_crucial

void _init0(void) {
	isenabled(BOG_ENB) {
		bog_start();
	}
	isenabled(PT_ENB) {
		piston();
	}
	isenabled(SCHED_ENB) {
		sched_init();
	}
}

void static _de_init0(void) {
	isenabled(SCHED_ENB) {
		sched_de_init();
	}
	isenabled(PT_ENB) {
		piston_stall();
	}
	isenabled(BOG_ENB) {
		bog_stop();
	}
}
//#endif
void static
prep(void) {
//#ifndef __ffly_crucial
//	void **p = ffly_alloca(sizeof(void*), NULL);
//	*p = (void*)by;

//	f_ss_chipp c;
//	ffly_ss_dir_creat("firefly", 7);
/*	ss_tun("info");
	ss_file_creat("created-by", NULL, _ffly_ss_def, 0);
	ss_bk;
*/
//	ffly_init_run();
//#endif
//	ff_mod_init();
//	ff_mod_handle();
//	ffly_mod();
}

void static
fini(void) {
#ifndef __ffly_crucial
	_de_init0();
//	ffly_ss_fin();
//	_de_init0();

//	f_whf_disconnect();
#endif
//	ff_mod_de_init();
	ffly_alloca_cleanup();
	ffly_thread_cleanup();
//	pr();
//	pf();
#ifndef __ffly_crucial
#endif
	proc_ctx_cleanup();
	io_closeup();
	f_lld_de_init();
//	f_creek_cleanup();
	ffly_ar_cleanup();
	ffly_tls_destroy();
#ifdef __ffly_debug
	/*
		if any leaks then say so.
	*/
	_int_u leak;//leak amount
	if ((leak = _f_m.used)>0) {
		int fd;
		fd = open("/dev/tty", O_WRONLY, 0);
		char buf[512];
		char *p = buf;
		p+=str_cpy(p, "memory leakage, ");

		// replace nots????
		p+=_ffly_nds(p,leak);
		p+=str_cpy(p, "-bytes.");
		*p = '\n';
		write(fd, buf, (p-buf)+1);
		close(fd);
	}
#endif
}

__asm__(".extern FF_LP");
void _ffstart(void) {
	init();
	int long long argc;
	char const **argv;
	char const **envp;
	__asm__("movq FF_LP(%%rip), %%rbx\t\n"
			"movq (%%rbx), %0\t\n"
			"movq %%rbx, %%rdi\n\t"
			"leaq 8(%%rbx), %%rdi\n\t"
			"movq %%rdi, %1\n\t"
			"movq (%%rbx), %%rax\n\t"
			"addq $8, %%rdi\n\t"
			"movq $8, %%rcx\n\t"
			"mulq %%rcx\n\t"
			"addq %%rax, %%rdi\n\t"
			"movq %%rdi, %2" : "=r"(argc), "=r"(argv), "=r"(envp) : : "rax", "rdi", "rcx", "rbx");
	char const **argp = argv;
	char const **end = argp+argc;

//	prep();
//	printf("%u - %s - %s\n", argc, *argv, *envp);
//	printf("%f\n", 0.00001234);
//	fini();
//	exit(0);

	envinit();
	envload(envp);
	
	{
		/*
			TODO:
				'FORCE_DRAIN'
		*/
		char const *printf_EN = envget("PRINTF_EN");
		if(!printf_EN) {//TODO: clean this up
			y_bits |= Y_PRINTF_DISABLE;
		}else{
			if (*printf_EN == '0') {
				y_bits |= Y_PRINTF_DISABLE;
			}else if(*printf_EN == '2'){
				y_bits |= Y_PRINTF_NO_PERMIT;
			}
		}
	}
	int exit_status;
#ifndef __ffly_crucial
	void *frame;
	void *tmp;

	tmp = m_alloc(TMP_ALSSIZE);
	ffly_alss(tmp, TMP_ALSSIZE);

	frame = ffly_frame();
	char const **argl = ffly_alloca(argc*sizeof(char const*), NULL);
	char const **arg = argl;
	*(arg++) = *(argp++); 

	/*
		move to its own function
	*/
	_8_i conf = -1;
	_8_i hatch = -1;
	if (argc>1) {
		if (!str_cmp(*argp, "-proc")) {
			evalopts(*(++argp));
			argp++;
			ffly_trim(2*sizeof(char const*)); // alloca stack trim
		}

		ffoptp cur = optbed;
		while(cur != NULL) {
			printf("name: '%s', val: '%s'\n", cur->name, !cur->val?"null":cur->val);
			if (!str_cmp(cur->name, "-sysconf")) {
				if (!cur->val) {
					printf("error.\n");
				}
				printf("loaded sysconfig.\n");
				ffly_ld_sysconf(cur->val);
				check_conf_version();
				conf = 0;
			} else if (!str_cmp(cur->name, "-mode")) {
				if (!str_cmp(cur->val, "debug"))
					ffset_mode(_ff_mod_debug);	
			} else if (!str_cmp(cur->name, "-hatch")) {
				if (!str_cmp(cur->val, "enable")) {
					printf("hatch enabled.\n");
					hatch = 0;
				}
			}
			cur = cur->next;
		}
		//printf("please provide sysconf.\n");
		//goto _end;
	}

	if (conf == -1) {
		if (!access(CFG_FILE, F_OK)) {
			printf("loading sysconfig.\n");
			ffly_ld_sysconf("sys.bole");
			check_conf_version();
			conf = 0;
		}
	}

	if (conf == -1)
		// load default builtin config if no file was provided
		ffly_ld_sysconf_def();
	/*
		alloca is giving pointer from the temp stack,
		so add them to the amend list for later.
	*/
	//f_sysconf_moveup();

	ffly_alad((void**)&argl);
	ffly_alad((void**)&arg);
	ffly_alad(&frame);
	// reload
	ffly_alrr();

	m_free(tmp);
	while(argp != end)
		*(arg++) = *(argp++);

	prep();
	/*
		TODO:
			show time since prog start as well
			not just ffmain time
	*/
	struct timespec start, finish;
	clock_gettime(CLOCK_MONOTONIC, &start);
	exit_status = main(arg-argl, argl);
	clock_gettime(CLOCK_MONOTONIC, &finish);
	MSG(INFO, "runtime: %u.%u {sec.ns}\n", finish.tv_sec-start.tv_sec, finish.tv_nsec-start.tv_nsec);
#else
	prep();
	exit_status = main(argc, argv);
#endif
#ifndef __ffly_crucial
	printf("\n\n");

	if (!conf)
		ffly_free_sysconf();
_end:
	{
		ffoptp cur = optbed, bk;
		while(cur != NULL) {
			bk = cur;
			cur = cur->next;
			m_free((void*)bk->name);
			if (bk->val != NULL)
				m_free((void*)bk->val);
			m_free(bk);
		}
	}
#ifdef __ffly_debug
	__ffmod_debug
		ffly_arstat();
#endif
	ffly_collapse(frame);
#endif // __ffly_crucial
//	pr();
//	pf();
	envcleanup();
	fini();
	exit(exit_status);

	/*
		function has no return address
		so if exit fails stay here to prevent fatal error occurring
	*/
	while(1);
}
