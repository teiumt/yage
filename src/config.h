# ifndef __ffly__conf__h
# define __ffly__conf__h
# include "y_int.h"
# include "inet.h"
#define F_CONF(__what) _ff_conf->__what
#define ffis_mode(__mode) (_ff_conf->mode == (__mode))
#define ffset_mode(__mode) _ff_conf->mode = (__mode)
#define __ffmod_debug \
	if (ffis_mode(_ff_mod_debug))
#define _ffmod_debugf(__func, ...)\
	__ffmod_debug{\
		__func(__VA_ARGS__);}
enum {
	_ff_mod_def,
	_ff_mod_debug,
};


#define BOG_ENB	1
#define PT_ENB	2
#define WH_ENB	4
#define SMS_ENB	8
#define SCHED_ENB	16
#define TMH_ENB	32
struct ff_config {
	_8_i mode;

	_64_u ef;
	struct {
		_32_u ip;
		_16_u port;
	} tc;
	// wharf server
	struct {
		_32_u ip;
		_16_u port;
	} whf;

	_int_u max_threads;
};

void ffly_cb(_64_u*);
extern struct ff_config *_ff_conf;
# endif /*__ffly__conf__h*/
