rm -f *.o
root_dir=$(realpath ../../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../../ && . ./compile.sh && cd em_pic/testsuite
ffly_objs="$ffly_objs"
gcc $cc_flags -o t_em0 t_em0.c $ffly_objs -nostdlib
