.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl _start
_start:
;stput
clrw
movlw $1
movwi j[$-1
;stput
;stpos
movlw $81
subwf $6
movwf $4
clrw
subfwb $7
movwf $5
;add off with
;push
;many save
movlb $0
movf $4
movwf $78
movf $5
movwf $79
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $120
;pop
;many retrieve
movlb $0
movf $78
movwf $4
movf $79
movwf $5
movf $120
;add off
movlb $4
movwf $32
clrf $33
movlw 8
movwf $40
movlw 1
movwf $41
clrf $48
clrf $49
call mul0w
call addfsr0
movlw $33
movwi $0
clrw
movwi $1
movwi $2
movwi $3
movwi $4
movwi $5
movwi $6
movwi $7
;addrof
;fsvar
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $82
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $80
call subfsr1w
movf $6
movwf $0
addfsr $1
movf $7
movwf $0
;stput
;deref
;bo
;stpos
movlw $2
call subfsr1w
movlb $1
call mov2w
movlb $1
movlw $16
addwf j[$48
clrw
addwfc j[$49
movlb $1
movf $48
movwf $4
movf $49
movwf $5
movf $0
movwi j[$-1
;cassign
;stpos
movlw $1
call subfsr1w
movf $1
movlb $255
movwf $24
;add16
movlw $84
addwf j[$6
clrw
addwfc j[$7
ret
