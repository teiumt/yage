#include "../../em_pic/picdebug.h"

void test(_8_u*,_8_u,_8_u);
void _start() {
	_8_u buf[2];
	buf[0] = 0x08;
	buf[1] = 0x08;
	_8_u *ptr;
	ptr = &buf;
	test(ptr,0x10,0x01);

	_8_u k;
	_8_u j;
	k = IO0;

	j = IO1;
	k = k+j;

	j = IO2;
	k = k+j;

	j = IO3;
	k = k+j;

	IO0 = k;
}
