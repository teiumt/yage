.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl t7
.globl t6
.globl t5
.globl t4
.globl t3
.globl t2
.globl t1
.globl t0
.globl _start
_start:
;stput
;funccall
;stput
clrw
movlw $25
movwi j[$-2
;stpos
movlw $1
call subfsr1w
call t0
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;cassign
movf $1
movlb $255
movwf $24
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t0:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t1
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t1:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t2
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t2:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t3
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t3:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t4
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t4:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t5
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t5:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t6
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t6:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stput
;funccall
;stput
;stfetch
moviw j[$0
movwi j[$-1
call t7
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
t7:
;stput
;bo
;stpos
movlw $1
call subfsr1w
movlw $1
addwf $1
movwf $1
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
;pop
;stash retrieve
movf $119
ret
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
