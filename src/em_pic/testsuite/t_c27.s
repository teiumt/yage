.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl _start
_start:
;stput
clrw
movlw $24
movwi j[$-12
;stput
clrw
clrw
movwi j[$-11
.L0
;eq
;stfetch
moviw j[$-11
sublw $10
btfsic $3,$2
goto .L1
;stput
;bo
;stfetch
moviw j[$-11
;push
;stash save
movwf $119
;stpos
movlw $12
call subfsr1w
;pop
;stash retrieve
movf $119
addwf $1
;push
;many save
movlb $0
movwf $63
;stpos
;add16
movlw $2
addwf $6
movwf $4
clrw
addwfc $7
movwf $5
;add off
;addfsr
;stfetch
moviw j[$1
addwf j[$4
clrw
addwfc j[$5
;pop
;many retrieve
movlb $0
movf $63
movwf $0
;stput
;bo
;stpos
addfsr j[$1
movlw $1
addwf $1
movwf $1
;stpos
addfsr j[$11
goto .L0
.L1
;cassign
;stpos
movlw $1
call subfsr1w
movf $1
movlb $255
movwf $24
;add16
movlw $1
addwf j[$6
clrw
addwfc j[$7
ret
