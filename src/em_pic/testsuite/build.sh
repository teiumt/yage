export CFM_DEBUG=off
_compile () {
	rm $1.s
	rm $1
	yc $1.c
	tas -i $1.s -o $1.o -f blf -p pic18em -g malk
}

link () {
	ybn -s "$1 lib.o" -d $2 -f blf
}

compile () {
	_compile $1
	link "$1.o" $1
}

tas -i "../../yc/playground/lib.s" -o lib.o -f blf -p pic18em -g malk

compile t_c0
compile t_c1
compile t_c2
compile t_c3
compile t_c4
compile t_c5
compile t_c6
compile t_c7
compile t_c8
compile t_c9
compile t_c10
compile t_c11
compile t_c12
compile t_c13
compile t_c14
compile t_c15
compile t_c16
compile t_c17
compile t_c18
compile t_c19
compile t_c20
compile t_c21
compile t_c22
compile t_c23
compile t_c24
compile t_c25
compile t_c26
compile t_c27
compile t_c28
compile t_c29
compile t_c30
compile t_c31
compile t_c32
compile t_c33
compile t_c34
compile t_c35
compile t_c36
compile t_c37
compile t_c38
compile t_c39
compile t_c40
compile t_c41
compile t_c42
compile t_c43
compile t_c44
compile t_c45
compile t_c46
compile t_c47
compile t_c48
compile t_c49
compile t_c50
compile t_c51
compile t_c52
compile t_c53
compile t_c54
compile t_c55
compile t_c56
compile t_c57
compile t_c58
compile t_c59
compile t_c60
compile t_c61
compile t_c62
compile t_c63
compile t_c64

_compile "t_c65"
_compile "t_c65#0"
link "t_c65.o t_c65#0.o" t_c65

compile t_c66
compile t_c67
compile t_c68
compile t_c69
compile t_c70
compile t_c71
dus runtest.dus

echo "###############################"
cat duslog
