.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl _start
.globl okay
okay:
;stput
clrw
clrw
movwi j[$-6
.L0
;eq
;stfetch
moviw j[$-6
;push
;stash save
movwf $119
;stpos
movlw $5
call subfsr1w
;pop
;stash retrieve
movf $119
subwf $1
btfsic $3,$2
goto .L1
;stput
;deref
;bo
;stpos
addfsr j[$1
movlb $1
call mov2w
;stpos
movlw $2
call subfsr1w
movlb $1
movf $6
movwf $4
movf $7
movwf $5
movlw 72
movwf $6
movlw 32
movwf $7
movf $0
movwf $1
addwf j[$48
clrw
addwfc j[$49
movf $4
movwf $6
movf $5
movwf $7
movlb $1
movf $48
movwf $4
movf $49
movwf $5
movf $0
movwi j[$-1
;bo
;stpos
addfsr j[$4
movlb $1
call mov2w
;stpos
movlw $4
call subfsr1w
movlb $1
movf $6
movwf $4
movf $7
movwf $5
movlw 72
movwf $6
movlw 32
movwf $7
movf $0
movwf $1
addwf j[$48
clrw
addwfc j[$49
movf $4
movwf $6
movf $5
movwf $7
movlb $1
movf $48
movwf $4
movf $49
movwf $5
;stpos
movlw $1
call subfsr1w
movf $1
movwf $0
;stput
;bo
;stpos
addfsr j[$1
movlw $1
addwf $1
movwf $1
;stpos
addfsr j[$6
goto .L0
.L1
;add16
movlw $5
addwf j[$6
clrw
addwfc j[$7
ret
_start:
;addrof
;fsvar
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $22
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $10
call subfsr1w
movf $6
movwf $0
addfsr $1
movf $7
movwf $0
;addrof
;fsvar
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $14
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $10
call subfsr1w
movf $6
movwf $0
addfsr $1
movf $7
movwf $0
;stput
clrw
movlw $33
movwi j[$4
;funccall
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $6
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $2
call subfsr1w
call mov01w
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $6
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $2
call subfsr1w
call mov01w
;stput
clrw
movlw $10
movwi j[$-5
call okay
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
;cassign
;stpos
addfsr j[$18
movf $1
movlb $255
movwf $24
;add16
movlw $6
addwf j[$6
clrw
addwfc j[$7
ret
