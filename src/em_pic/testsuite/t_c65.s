.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.extern test
.globl _start
_start:
;stput
clrw
movlw $8
movwi j[$-2
;stput
clrw
movlw $8
movwi j[$-1
;addrof
;fsvar
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $4
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $2
call subfsr1w
movf $6
movwf $0
addfsr $1
movf $7
movwf $0
;funccall
movf $6
movwf $4
movf $7
movwf $5
;stpos
;add16
movlw $6
subwf j[$4
clrw
subfwb j[$5
;stpos
movlw $2
call subfsr1w
call mov01w
;stput
clrw
movlw $16
movwi j[$-5
;stput
clrw
movlw $1
movwi j[$-6
;stpos
movlw $2
call subfsr1w
call test
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
;stput
movlb $255
movf $24
movwi j[$1
;stput
movlb $255
movf $25
movwf $1
;stput
;bo
;stfetch
moviw j[$0
;push
;stash save
movwf $119
;stpos
addfsr j[$1
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
;stput
movlb $255
movf $26
movwi j[$-1
;stput
;bo
;stfetch
moviw j[$-1
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
;stput
movlb $255
movf $27
movwi j[$-1
;stput
;bo
;stfetch
moviw j[$-1
;push
;stash save
movwf $119
;pop
;stash retrieve
movf $119
addwf $1
movwf $1
;cassign
movf $1
movlb $255
movwf $24
;add16
movlw $5
addwf j[$6
clrw
addwfc j[$7
ret
