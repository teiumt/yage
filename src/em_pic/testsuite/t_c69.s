.extern mov01w
.extern mov10w
.extern add0w
.extern sub0w
.extern mov0w
.extern or0w
.extern and0w
.extern add0d
.extern sub0d
.extern mov0d
.extern or0d
.extern and0d
.extern mov01d
.extern mul0w
.extern mov1w
.extern addfsr0
.extern addfsr1
.extern addfsri1
.extern addfsri0
.extern mov2w
.extern mov00w
.extern add00w
.extern add0q
.extern sub0q
.extern or0q
.extern and0q
.extern mov0q
.extern mov2d
.extern mov2q
.extern mov2iw
.extern mov2id
.extern mov2iq
.extern clr2q
.extern clr2d
.extern clr1q
.extern clr1d
.extern clr4q
.extern mov01q
.extern mov10d
.extern mov10q
.extern subfsr1w
.extern movfsr01
.extern movfsr10
.extern cmpw
.extern cmpd
.extern cmpq
.extern mov02d
.extern mov02q
.extern shld
.extern shlq
.extern shrhd
.extern shrhq
.extern shlw
.extern shld
.extern shlq
.extern shrw
.extern shrd
.extern shrq
.globl _start
_start:
;stput
clrw
movlw $173
movwi j[$-2
movlw $222
movwi j[$-1
;eq
;stpos
movlw $2
call subfsr1w
movlb $2
movlw 172
movwf $48
movlw 222
movwf $49
call sub0w
btfsic $3,$0
goto .L0
;eq
movlb $2
call mov2w
movlb $0
movlw 174
movwf $104
movlw 222
movwf $105
movf $6
movwf $4
movf $7
movwf $5
movlw 72
movwf $6
movlw 32
movwf $7
movlb $2
call sub0w
movf $4
movwf $6
movf $5
movwf $7
btfsic $3,$0
goto .L0
movlw $33
movlb $255
movwf $24
goto .L1
.L0
movlw $0
movlb $255
movwf $24
.L1
;add16
movlw $2
addwf j[$6
clrw
addwfc j[$7
ret
