#ifndef __em_pic__h
#define __em_pic__h
#include "../y_int.h"
#include "../types.h"
#include "../io.h"
#define _em_pic_inc	0
#define _em_pic_dec	1
#define _em_pic_movf	2
#define _em_pic_movlb	3
#define _em_pic_movlw	4
#define _em_pic_movwf	5
#define _em_pic_movlp	6
#define _em_pic_movwi	7
#define _em_pic_movwi0	8
#define _em_pic_moviw	9
#define _em_pic_moviw0	10
#define _em_pic_goto	11
#define _em_pic_nop		12
#define _em_pic_btfsic	13
#define _em_pic_btfsis	14
#define _em_pic_call	15
#define _em_pic_ret		16
#define _em_pic_sublw	17
#define _em_pic_subwf	18
#define _em_pic_subfwb	19
#define _em_pic_andlw	20
#define _em_pic_andwf	21
#define _em_pic_iorlw	22
#define _em_pic_iorwf	23
#define _em_pic_addwfc	24
#define _em_pic_addwf	25
#define _em_pic_addlw	26
#define _em_pic_addfsr	27
#define _em_pic_lsl		28
#define _em_pic_lsr		29
#define _em_pic_clrw	30
#define _em_pic_clrf	31
#define _em_pic_rlf		32
#define _em_pic_rrf		33
#define _em_pic_bcf		34
#define _em_pic_bsf		35
#define _em_pic_xorlw	36
#define _em_pic_xorwf	37	
#define _em_pic_incfsz	38
#define _em_pic_decfsz	39	
#define _em_pic_callw	40

#define EMPIC_IO0 0x7f98
#define EMPIC_IO1 0x7f99
#define EMPIC_IO2 0x7f9a
#define EMPIC_IO3 0x7f9b
#define EMPIC_IO4 0x7f9c
#define EMPIC_IO5 0x7f9d
#define EMPIC_IO6 0x7f9e
#define EMPIC_IO7 0x7f9f
#define emprintf(...) f_fprintf(emout,__VA_ARGS__) 
extern void *emout;
extern struct em_struc em_strc;
struct em_struc {
	_8_u *em_data;
	_int_u em_size;
};
_8_s em_main(int __argc, char const *__argv[]);
_8_s em_file(char const *__file);
_64_u em_pic18_ioget(_64_u);
void em_pic18_io(_64_u, _8_u);
void em_pic18_dis(_8_u *__buf,_int_u __words);
void em_pic18_init(void);
#endif /*__em_pic__h*/
