void out(_8_u __char) { 
    IO6 = 1; 
    IO7 = __char; 
    while(IO6#0); 
} 
 
void outbuf(_8_u *__buf,_8_u __n) { 
    _8_u word; 
_again: 
    __n = __n-1; 
    word = *(__buf+__n); 
    out(word); 
    if (__n != 0) { 
        goto _again; 
    } 
}
