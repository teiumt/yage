export CFM_DEBUG=off
yc test.c
#cd ../yc/playground
#yc tram.c
#yc delay.c
#tas -i tram.s -o tram.o -f blf -p pic18em -g malk
#tas -i delay.s -o delay.o -f blf -p pic18em -g malk
#cd ../../em_pic
tas -i test.s -o test.o -f blf -p pic18em -g malk
#../yc/playground/tram.o ../yc/playground/delay.o
ybn -s "test.o testsuite/lib.o" -d a.out -f blf
#../tools/slam_dump a.out
export EM_TEST=1
./empic a.out
