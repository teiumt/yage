#include "core.h"
#include "../assert.h"
#include "../io.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../mutex.h"
#include "../thread.h"
char const static *opname[] = {
	"INC",
	"DEC",
	"MOVF",
	"MOVLB",
	"MOVLW",
	"MOVWF",
	"MOVLP",
	"MOVWI",
	"MOVWI0",
	"MOVIW",
	"MOVIW0",
	"GOTO",
	"NOP",
	"BTFSIC",
	"BTFSIS",
	"CALL",
	"RET",
	"SUBLW",
	"SUBWF",
	"SUBFWB",
	"ANDLW",
	"ANDWF",
	"IORLW",
	"IORWF",
	"ADDWFC",
	"ADDWF",
	"ADDLW",
	"ADDFSR",
	"LSL",
	"LSR",
	"CLRW",
	"CLRF",
	"RLF",
	"RRF",
	"BCF",
	"BSF",
	"XORLW",
	"XORWF",
	"INCFSZ",
	"DECFSZ"
};

#define Z				4
#define CARRY_BIT		1
static mlock lock = MUTEX_INIT;

_64_u static ip = 0;
_8_u static *stack[256];
_8_u static *bank_vals[256];
#define status (global_bank[3])
static _8_u global_bank[128];

static _int_s offset = 0;
struct context {
	_8_u bank_n;
	_8_u *bank;
};

#define ADDROF(__bank,__file)\
	(0x2000+((__file)-0x20)+((__bank)*80))

static struct context mainctx;
#define incommonram(__file) (__file>=0x70 && __file<=0x7f)
void static selbank(struct context *__ctx, _int_u __bank) {
	_8_u **b;

	mt_lock(&lock);
	b = bank_vals+__bank;	
	if (!*b) {
		*b = m_alloc(128);
	}
	mt_unlock(&lock);
	
	__ctx->bank = *b;
	__ctx->bank_n = __bank;
}

_8_u static* stack_access(struct context *__ctx, _64_u __offset) {
	_int_u page = __offset>>8;
	_int_u offset = __offset&0xff;
	printf("PAGE_ACCESS{%x}: %u, %u\n", __offset, page,offset);
	_8_u **p;
	mt_lock(&lock);
	p = stack+page; 
	if (!*p) {
		*p = m_alloc(256);
		mem_set(*p,0,0x100);
	}
	mt_unlock(&lock);
	return (*p)+offset;
}
#define bank_load(...) _bank_load(&mainctx,__VA_ARGS__)
#define bank_store(...) _bank_store(&mainctx,__VA_ARGS__)
_8_u static _bank_load(struct context *__ctx, _int_u __file) {
	if (__file == 0){
		return *stack_access(__ctx,*(_16_u*)(global_bank+4)+offset);
	}else
	if (__file == 1) {	
		return *stack_access(__ctx,*(_16_u*)(global_bank+6)+offset);
	}else
	if (__file<=0x0b || incommonram(__file)) {
		return global_bank[__file];
	}else
	if (__file>=0x20 && __file<0x70) {
		_64_u adr;
		_64_u val = *stack_access(__ctx,adr = (0x2000+(__file-0x20)+(__ctx->bank_n*80)));
		printf("h'20:%x:%u{%x}: %u.\n",__file-0x20,__ctx->bank_n,adr,val);
		return val;
	}


	assert(__file<256);
	return __ctx->bank[__file];
}

void static _bank_store(struct context *__ctx, _int_u __file, _8_u __val) {
	printf("STORE: %u -> %u\n", __val,__file);
	if (__file == 0){
		*stack_access(__ctx,*(_16_u*)(global_bank+4)+offset) = __val;
		return;
	}else
	if (__file == 1) {
		*stack_access(__ctx,*(_16_u*)(global_bank+6)+offset) = __val;
		return;
	}else
	if (__file<=0x0b || incommonram(__file)) {
		global_bank[__file] = __val;
		return;
	}else
	if (__file>=0x20 && __file<0x70) {
		_64_u adr;
		*stack_access(__ctx,adr = (0x2000+(__file-0x20)+(__ctx->bank_n*80))) = __val;
		printf("h'20:%x:%u{%x}: %u.\n", __file-0x20,__ctx->bank_n,adr,__val);
		return;
	}

	assert(__file<256);
	__ctx->bank[__file] = __val;
}

static _16_u wreg;
void static op_incf(_8_u *__buf) {
	_8_u val;
	_8_u file = __buf[1];
	val = bank_load(file);
	val++;
	bank_store(file,val);
}

void static op_decf(_8_u *__buf) {
	_8_u val;
	_8_u file = __buf[1];
	val = bank_load(file);
	val--;
	bank_store(file,val);
	status &= ~Z;
	if (!val) {
		status |= Z;
	}
}

void static op_incfsz(_8_u *__buf) {
	_8_u val;
	_8_u file = __buf[1];
	val = bank_load(file);
	val++;
	bank_store(file,val);
	if (!val) {
		ip++;
	}
}

void static op_decfsz(_8_u *__buf) {
	_8_u val;
	_8_u file = __buf[1];
	val = bank_load(file);
	val--;
	bank_store(file,val);
	if (!val) {
		ip++;
	}
}



void static op_movf(_8_u *__buf) {
	wreg = bank_load(__buf[1]);
}

void static op_movlb(_8_u *__buf) {
	selbank(&mainctx,__buf[1]);
}

void static op_movlw(_8_u *__buf) {
	wreg = __buf[1];
	printf("WREG = %x.\n",__buf[1]);
}

void static op_movwf(_8_u *__buf) {
	bank_store(__buf[1],wreg);
}

void static op_moviw(_8_u *__buf) {
	_8_u indf = __buf[3];
	_8_s num = __buf[1];

	offset = num;
	wreg = bank_load(indf);	
	offset = 0;
	printf("MOVIW: %u : %d: %u, %u\n",wreg,num,indf,bank_load(indf));
}

void static op_movwi(_8_u *__buf) {
	_8_u indf = __buf[3];
	_8_s num = __buf[1];

	offset = num;
	bank_store(indf,wreg);
	offset = 0;
	printf("MOVWI: %u : %d: %u, %u\n",wreg,num,indf,bank_load(indf));

}

void static op_goto(_8_u *__buf) {
	_16_u adr = *(_16_u*)(__buf+1);
	ip = adr>>2;
	printf("JUMPING to %x.\n", ip);
}


void static stored(_8_u __bits, _8_u __file, _8_u __val) {
	if (__bits&1) {
		bank_store(__file,__val);
	}else{
		wreg = __val;
	}
}

_8_u static _sub(_16_s __accum, _16_s __val) {
	printf("SUB: %u-%u+%u.\n",__accum,__val,status&1);
	__accum = __accum-__val;

	status &= ~CARRY_BIT;
	status |= !((__accum>>8)&1);

	status &= ~Z;
	if (!__accum) {
		status |= Z;
	}
	printf("borrow? %u, %d\n", (__accum>>8)&1, __accum);
	return __accum&0xff;
}

void static op_subwf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u final;
	final = _sub(bank_load(file),wreg);
	stored(bits,file,final);
	printf("SUB{%u, %s}\n",final,(status&CARRY_BIT)?"with borrow":"no borrow");
}

void static op_subfwb(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u accum = bank_load(file);
	accum = _sub(accum,wreg+!(status&CARRY_BIT));
	stored(bits,file,accum);
}

_8_u static _add(_16_u __accum, _8_u __val) {
	__accum+=__val;
	_8_u val = __accum&0xff;
	status &= ~CARRY_BIT;
	status |= __accum>>8;
	return val;
}
void static op_addwf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u val;
	val = _add(bank_load(file),wreg);
	stored(bits,file,val);
	printf("ADDWF = %u+%u = %u, F%u, b%x\n",bank_load(file),wreg,val,file,bits);
}

void static op_addwfc(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_16_u accum  = bank_load(file);

	accum+=(status&CARRY_BIT);
	accum = _add(accum,wreg);	
	stored(bits,file,accum);
	printf("ADDWFC = %u+%u+%u = %u, F%u, b%x\n",bank_load(file),wreg,status&CARRY_BIT,accum,file,bits);
}

void static op_sublw(_8_u *__buf) {
	_8_u val = __buf[1];
	wreg = _sub(val,wreg);
	printf("######### WREG: %u: %u.\n",wreg,val);
}
void static op_addlw(_8_u *__buf) {
	_8_u val = __buf[1];
	wreg = _add(val,wreg);
}

void static op_clrw(_8_u *__buf) {
	wreg = 0;
}

void static op_btfsic(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bit_to_test = __buf[2];
	_8_u bits = bank_load(file);
	if (!(bits&(1<<bit_to_test))) {
		ip++;//skip next instruction
		printf("SKIPPING.\n");
	}
}

void static op_btfsis(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bit_to_test = __buf[2];
	_8_u bits = bank_load(file);
	if (bits&(1<<bit_to_test)) {
		ip++;
		printf("SKIPPING.\n");
	}
}

void static op_iorlw(_8_u *__buf) {
	_8_u val = __buf[1];
	wreg |= val;
}

void static op_iorwf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u val = bank_load(file);
	_8_u bits = __buf[3];
	val |= wreg;
	stored(bits,file,val);
}

void static op_addfsr(_8_u *__buf) {
	_8_u add = __buf[1];
	_8_u bits = __buf[3];
	_16_u *i;
	if (bits&1) {
		i = global_bank+6;
	} else {
		i = global_bank+4;
	}
	(*i)+=add;
	printf("ADDFSR: %u, %u.\n",bits,add);
}


void static op_andlw(_8_u *__buf) {
	wreg = wreg&__buf[1];
}

void static op_andwf(_8_u *__buf) {
	_8_u val;
	val = wreg&bank_load(__buf[1]);
	_8_u bits = __buf[3];
	stored(bits,__buf[1],val);
}

void static op_lsl(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u val;
	val = bank_load(file);
	status |= val>>7;
	val<<=1;
	stored(bits,file,val);
}

void static op_lsr(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u val;
	val = bank_load(file);
	status |= val&1;
	val>>=1;
	stored(bits,file,val);
}

void static op_rlf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u val;
	val = bank_load(file);

	status |= val&1;
	val>>=1;
	val |= (status&1)<<7;
	stored(bits,file,val);
}

void static op_rrf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u val;
	val = bank_load(file);
	status |= val>>7;
	val<<=1;
	val |= status&1;
	stored(bits,file,val);
}

void static op_bcf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bit = __buf[2];
	_8_u val;
	val = bank_load(file);
	val &= ~(1<<bit);
	bank_store(file,val);
}

void static op_bsf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bit = __buf[2];
	_8_u val;
	val = bank_load(file);
	val |= 1<<bit;
	bank_store(file,val);
	printf("BSF: %u, %u, %x\n",file,bit,val);
}

void static op_xorlw(_8_u *__buf) {
	_8_u val = __buf[1];
	wreg ^= val;
}
#define BANK(__x) (__x>>7)
#define FILE(__x) (__x&127)


#define TOSH	0x0fef
#define TOSL	0x0fee
#define STKPTR	0x0fed
void static op_xorwf(_8_u *__buf) {
	_8_u file = __buf[1];
	_8_u bits = __buf[3];
	_8_u val;
	val = bank_load(file);
	val = wreg^val;
	stored(bits,file,val);
}
void static op_clrf(_8_u *__buf) {
	bank_store(__buf[1],0);
}


#define TOSH_ADDR	ADDROF(BANK(TOSH),FILE(TOSH))
#define TOSL_ADDR	ADDROF(BANK(TOSL),FILE(TOSL))
#define STKPTR_ADDR	ADDROF(BANK(STKPTR),FILE(STKPTR))
_64_u static retstack[16];
static struct context tmrctx;
void static _call(_16_u adr) {
	_8_u *b = stack[STKPTR_ADDR>>8];
	_64_u curpos;
	curpos = b[STKPTR_ADDR&0xff];
	retstack[curpos] = b[TOSH_ADDR&0xff]<<8|b[TOSL_ADDR&0xff];	
	curpos++;
	b[STKPTR_ADDR&0xff] = curpos;
	b[TOSH_ADDR&0xff] = ip>>8;
	b[TOSL_ADDR&0xff] = ip&0xff;
	printf("TOSH: h%x, TOSL: h%x, raddr: h%x\n",b[TOSH_ADDR&0xff],b[TOSL_ADDR&0xff],ip);	
	printf("CALL to %x, from %x\n",adr,ip);
	ip = adr;
}
void static op_call(_8_u *__buf) {
	_16_u adr = *(_16_u*)(__buf+1);
	adr = adr>>2;
	_call(adr);
}

void static op_ret(_8_u *__buf) {
	_8_u *b = stack[STKPTR_ADDR>>8];

	_64_u curpos = b[STKPTR_ADDR&0xff];
	_16_u rip;
	rip = (_16_u)b[TOSH_ADDR&0xff]<<8|b[TOSL_ADDR&0xff];
	printf("TOSH: h%x, TOSL: h%x, raddr: h%x, curpos: h%x\n",b[TOSH_ADDR&0xff],b[TOSL_ADDR&0xff],rip,curpos);
	if (!curpos) {
		ip = ~0;
		return;
	}
	curpos--;

	b[STKPTR_ADDR&0xff] = curpos;
	b[TOSH_ADDR&0xff] = retstack[curpos]>>8;
	b[TOSL_ADDR&0xff] = retstack[curpos]&0xff;
	ip = rip;
	printf("RET to h%x, wreg: h%x\n",ip,wreg);
}

void static op_callw(_8_u *__buf) {
	_8_u pclath = global_bank[0x0a];
	_call((wreg>>2));
}

static void(*optab[])(_8_u*) = {
	op_incf,
	op_decf,
	op_movf,
	op_movlb,
	op_movlw,
	op_movwf,
	NULL,//movlp
	op_movwi,
	NULL,
	op_moviw,
	NULL,//MOVIW0
	op_goto,//GOTO
	NULL,//NOP
	op_btfsic,//BTFSIC
	op_btfsis,//BTFSIS
	op_call,//CALL
	op_ret,//RET
	op_sublw,//SUBLW
	op_subwf,//SUBWF
	op_subfwb,//SUBFWB
	op_andlw,//ANDLW
	op_andwf,//ANDWF
	op_iorlw,//IORLW
	op_iorwf,//IORWF
	op_addwfc,//ADDWFC
	op_addwf,//ADDWF
	op_addlw,//ADDLW
	op_addfsr,//ADDFSR
	op_lsl,//LSL
	op_lsr,//LSR
	op_clrw,
	op_clrf,
	op_rlf,//RLF
	op_rrf,//RRF
	op_bcf,//BCF
	op_bsf,//BSF
	op_xorlw,//XORLW
	op_xorwf,//XORWF
	op_incfsz,
	op_decfsz,
	op_callw//CALLW
};
void static showinfo(void) {
	printf(
		"INFO:\n"
		"| WREG: %u.\n"
		"| INDF0: %u\n"
		"| INDF1: %u\n"
		"| FSR0: %x\n"
		"| FSR0: %x\n"		
		"| FSR1: %x\n"
		"| FSR1: %x\n"
		"| BANK: %u\n",
		wreg,
		bank_load(0),
		bank_load(1),
		bank_load(4),
		bank_load(5),
		bank_load(6),
		bank_load(7),
		mainctx.bank_n
	);
}

#define LATC	0x1a
#define PORTC	0x0e
static struct context ourctx;
static void _bsel(_8_u __bank) {
	selbank(&ourctx,__bank);
}

static _8_u _bld(_8_u __file) {
	return _bank_load(&ourctx,__file);
}
static void _bst(_int_u __file, _8_u __val) {
	_bank_store(&ourctx,__file,__val);
}
#include "../tmu.h"
#include "../time.h"

struct tram_frame {
	_32_u addr;
};
_8_s tram_send(_8_u *__buf, _int_u __n) {
	printf("TRAM SENDING.\n");
	_bsel(BANK(LATC));

	_8_u v;
	if(!(v = _bld(FILE(LATC))&8)) {
		printf("LATC: %x, %u: %p\n",v, ourctx.bank_n, ourctx.bank);
		return -1;
	}
	_bsel(BANK(PORTC));
	_int_u i;
	_8_u word;
	for(;__n != 0;) {
		__n--;
		word = __buf[__n];
		i = 0;
		for(;i != 8;i++) {
			_8_u og;
			og = ((word>>i)&1)<<1;
			_bst(FILE(PORTC),og);

			doze(TIME_LSEC(0.001),0);
			_bst(FILE(PORTC),0xff);
			doze(TIME_LSEC(0.001),0);
		}
	}

	_bsel(BANK(LATC));
	while(_bld(FILE(LATC))&8);
	return 0;
}
static _8_s running;
void tram_recv(_8_u *__buf, _int_u __n) {
	_bsel(BANK(PORTC));
	_bst(FILE(PORTC),0);

	_bsel(BANK(LATC));
	_8_u val;
	_8_u word;
	_int_u i;
	for(;__n != 0;) {
		i = 0;
		word = 0;
		for(;i != 8;i++) {
			while(!(_bld(FILE(LATC))&4));
			val = _bld(FILE(LATC));
			word |= ((val>>3)&1)<<i;
			while(_bld(FILE(LATC))&4);	
		}
		emprintf("TRAM%u: %x.\n",__n, word);
		__n = __n-1;
		__buf[__n] = word;
	}
	_bsel(BANK(PORTC));
	_bst(FILE(PORTC),0xff);
	doze(TIME_LSEC(0.001),0);
}

static _64_u tmr0_val = 0;
static _8_s tmr0_init = -1;
#define TMR0H	0x59d
#define TMR0L	0x59c
#define T0CON0	0x59e
#define T0CON1	0x59f
#define PIR0	0x70c

static void tmr0(void) {
	struct context *ct = &tmrctx;

	selbank(ct,BANK(EMPIC_IO6));
	if (_bank_load(ct,FILE(EMPIC_IO6))&1) {
		emprintf("OUT: %x\n",_bank_load(ct,FILE(EMPIC_IO7)));
		_bank_store(ct,FILE(EMPIC_IO6),0);
	}

	if (!tmr0_init) {
		_64_u val = 0;
		selbank(ct,BANK(TMR0H));
		val |= _bank_load(ct,FILE(TMR0H))<<8;
		selbank(ct,BANK(TMR0L));
		val |= _bank_load(ct,FILE(TMR0L));
		if (tmr0_val>val) {	
			selbank(ct,BANK(PIR0));
			_bank_store(ct,FILE(PIR0),0xff);
			tmr0_init = -1;
			tmr0_val = 0;
			return;
		}
		tmr0_val++;
		return;
	}
	_8_u val;
	selbank(ct,BANK(T0CON0));
	val = _bank_load(ct,FILE(T0CON0));
	if (!(val&0x80)){tmr0_init = -1;return;}
	if (tmr0_init == -1) {
		tmr0_init = 0;
	}
}

static tstrucp th;
void static*
loop(void *__arg) {
	while(!running) {
		_8_u buf[16];
		_8_u v;
		_bsel(BANK(EMPIC_IO6));
		v = _bld(FILE(EMPIC_IO6));
		if (v&2) {
			tram_recv(buf,16);
			emprintf("TRAM: '%w'.\n",buf,16);
			_bsel(BANK(EMPIC_IO6));
			_bst(FILE(EMPIC_IO6),0);
		}
	}
}
void tram_init(void) {
	selbank(&mainctx,BANK(PORTC));
	bank_store(FILE(PORTC),0xff);
	
	selbank(&mainctx,BANK(LATC));
	bank_store(FILE(LATC),0);
}
static _8_u emmode = 1;
#include "../tools.h"
void em_pic18_dis(_8_u *__buf,_int_u __words) {
	stack_access(&tmrctx,STKPTR_ADDR);
	{
		_8_u *b = stack[STKPTR_ADDR>>8];
		b[STKPTR_ADDR&0xff] = 0;
	}
	_32_u sig;
	if (!emmode) {
		selbank(&mainctx,BANK(EMPIC_IO6));
		bank_store(FILE(EMPIC_IO6),0);
	
		tram_init();
		selbank(&mainctx,BANK(T0CON0));
		bank_store(FILE(T0CON0),0);

		selbank(&mainctx,BANK(PIR0));
		bank_store(FILE(PIR0),0);

		running = 0;
		tcreat(&th,loop,NULL);
		sig = th->sig;
	}
	
	selbank(&mainctx,BANK(T0CON0));
	bank_store(FILE(T0CON0),0);

	selbank(&mainctx,BANK(PIR0));
	bank_store(FILE(PIR0),0);
	for(;ip < __words>>2;) {
		_8_u op = __buf[ip<<2];
		if (op<_em_pic_decfsz+1) {
		//	printf("%u: %s : %u\n",ip,opname[op], op);
		}else{
			printf("ILLEGAL %x.\n",op);
			break;
		}
		ip++;
		if (optab[op] != NULL)
			optab[op](__buf+((ip-1)<<2));
		tmr0();
	}
	if (!emmode) {
		running = -1;
		twait(th,sig);
	}
	printf("BROKE OUT, ip{%x & %x}\n",ip,__words);
	printf("program with %u instructions.\n",__words/4);
	showinfo();
	_int_u i;
	i = 0;
	for(;i != 0x100;i++) {
		_8_u *page = *(stack+i);
		if (page != NULL) {
			printf("PAGE-%u{PRESENT}\n",i);
			ffly_hexdump(page,0x100);
		}
	}

	_64_u ioreg[] = {
		EMPIC_IO0,
		EMPIC_IO1,
		EMPIC_IO2,
		EMPIC_IO3,
		EMPIC_IO4,
		EMPIC_IO5,
		EMPIC_IO6,
		EMPIC_IO7
	};
	
	i = 0;
	for(;i != 8;i++) {
		_64_u reg = ioreg[i];
		selbank(&mainctx,reg>>7);
		_8_u v = mainctx.bank[reg&127];
		printf("IO%u: %x'%c\n",i,v,v);
	}
}

void em_pic18_io(_64_u __io, _8_u __value) {
	selbank(&mainctx,__io>>7);
	mainctx.bank[__io&127] = __value;
}

_64_u em_pic18_ioget(_64_u __io) {
	selbank(&mainctx,__io>>7);
	return mainctx.bank[__io&127];
}
#include "../env.h"
void em_pic18_init(void) {
	char const *s;
	s = envget("EM_TEST");
	if (s != NULL) {
		if (*s == '0') {
			emmode = 0;
		}
	}
	mem_set(stack,0,sizeof(_8_u*)*0x100);
	mem_set(bank_vals,0,sizeof(_8_u*)*0x100);
	selbank(&mainctx,0);
	ip = 0;
	status = 0;
	bank_store(6,239);
	bank_store(7,35);
}
