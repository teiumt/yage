#include "core.h"
#include "../io.h"
#include "../file.h"
#include "../m_alloc.h"
#include "../env.h"
#include "../slam.h"
#include "../tools.h"
#include "../strf.h"
#include "../lib.h"
struct em_struc em_strc;
void static readios(char *__st){
	_64_u ion = EMPIC_IO0;
	_int_u val;
	_int_u cnt;

	char buf[64];
_again:
	cnt = snumext(buf,__st);
	val = _ffly_dsn(buf,cnt);
	em_pic18_io(ion++,val);
	__st+=cnt;
	if (*__st == ',') {
		__st++;
		goto _again;
	}
	printf("NUM-IOS: %u.\n",ion);
}


_8_s em_file(char const *__file) {
	struct _stat st;
	struct file f;
	if (bopen(&f,__file,_O_RDONLY,0) == -1)
		return -1;
	bstat(&f,&st);
	if (!st.st_size)
		return -1;
	struct slam_hdr h;
	bread(&f,&h,sizeof(struct slam_hdr));


	struct slm_segment *segs = m_alloc(sizeof(struct slm_segment)*h.ns);
	bpread(&f,segs,sizeof(struct slm_segment)*h.ns,h.sg);
	_int_u i, total = 0;
	i = 0;
	for(;i != h.ns;i++) {
		struct slm_segment *s = segs+i;
		total+=s->words*4;
	}

	em_strc.em_data = m_alloc(total);
	i = 0;
	for(;i != h.ns;i++) {
		struct slm_segment *s = segs+i;
		_8_u *dst = ((_8_u*)em_strc.em_data)+s->adr*4;
		if (s->words>0) {
			bpread(&f,dst,s->words*4,s->off);
		}
	}
	bclose(&f);
	printf("EMPIC: SLAM-DATA, %u\n",total);
	ffly_hexdump(em_strc.em_data,total);
	em_strc.em_size = total;
	//em_pic18();
	em_pic18_dis(em_strc.em_data,total);
	m_free(em_strc.em_data);
	m_free(segs);
	return 0;
}
#include "../io.h"
#include "../rws.h"
#include "../ioc_struc.h"

void *emout;
extern struct f_rw_funcs rws_func;
_8_s em_main(int __argc, char const *__argv[]) {
	struct ffly_file *em_out;
	_8_s err;
	if (!(em_out = f_fopen("emlog",
		FF_O_WRONLY|FF_O_TRUNC|FF_O_CREAT,
		FF_S_IRUSR|FF_S_IWUSR, &err))) {
	}
	if (err == -1)
		return -1;
	ffly_fopt(em_out, FF_STREAM);
	static struct f_ioc_struc ioc_out;

	ioc_out.rws.funcs = &rws_func;
	ioc_out.rws.arg = (_ulonglong)em_out;
	em_out->rws = &ioc_out.rws;
	emout = &ioc_out;

	if (__argc<2) {
		printf("please provide executable.\n");
		return -1;
	}
	em_pic18_init();
	if (__argc == 3) {
		readios(__argv[2]);
	}
	_8_s r;
	r = em_file(__argv[1]);

	ffly_fclose(em_out);
	return r;
}
