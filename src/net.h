# ifndef __ffly__net__h
# define __ffly__net__h
# include "y_int.h"
# include "types.h"
# include "network/header.h"
# include "network/sock.h"
/*
	TODO:
		begin work on this

		clear up and remove sockaddr and any refrence to posix dog shit
		and only use for lower levels
*/

enum {
	_NET_PROT_TCP,
	_NET_PROT_UDP
};

_f_err_t ff_net_shutdown(FF_SOCKET*, int);
FF_SOCKET* ff_net_creat(_f_err_t*, _8_u);
_f_err_t ff_net_connect(FF_SOCKET*, struct f_netinfo*);
FF_SOCKET* ff_net_minc(FF_SOCKET*, _f_err_t*);
_f_size_t ff_net_send(FF_SOCKET*, void const*, _int_u, int, _f_err_t*);
_f_size_t ff_net_recv(FF_SOCKET*, void*, _int_u, int, _f_err_t*);

_f_err_t ff_net_bind(FF_SOCKET*, struct f_netinfo*);
_f_err_t ff_net_close(FF_SOCKET*);
# endif /*__ffly__net__h*/
