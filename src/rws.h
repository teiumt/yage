# ifndef __f__rws__h
# define __f__rws__h
# include "y_int.h"
/*
	FOR IO stuff(printf streams?) 


	this is to allow for creek bypass


	rws -> creek
		|
		-> raw file(defined by user)

	NOTE:
		this structure is to provide a means to do shit in fashion


		so its options one:
		define many structures for diffrent things(THE STUPED WAY)

		or a compact single one

		example

		lets say we use pik and bind it to a socket
		now lets say we have internal routines that use this
		socket to output stuff

		void outputstuff(...); <- are function(DEFINED in project somewhere)

		as said before 
		option a

		struct file {
			void *ctx;
			struct funcs
		}

		struct sock {
			void *ctx;
			struct funcs;
		}

		outputstuff_file(file)
		outputstuff_sock(sock)


		option b
		struct rws {
			void *ctx;
			struct funcs;
		}

		#define ID_FILE
		#define ID_SOCK
		outputstuff(rws, ID_FILE);
		outputstuff(rws, ID_SOCK);

		managing this struct is easer for the user as multi struct usage will be left for lower levels,
		we use option b because binding is easer & managing.
*/
/*
	primay usage in IO(printf,files)
*/
#define RWS_RDONLY		0
#define RWS_WRONLY		1
#define RWS_RDWR			2

#define RWS_IRWXU		0
#define RWS_IRUSR		1
#define RWS_IWUSR		2
#define RWS_IXUSR		3
struct f_rw_funcs;
struct rws_schematic{
	char const *path;
	struct f_rw_funcs *funcs;
};
struct rws_stat{
	_32_u st_size;
};

struct f_rw_funcs {
	_32_u(*pwrite)(_ulonglong, void*, _int_u, _64_u, _8_s*);
	_32_u(*pread)(_ulonglong, void*, _int_u, _64_u, _8_s*);
	_32_u(*read)(_ulonglong, void*, _int_u, _8_s*);
	_32_u(*write)(_ulonglong, void*, _int_u, _8_s*);
	_64_u(*seek)(_ulonglong, _64_u, _32_u);
	_ulonglong(*open)(struct rws_schematic*,_64_u,_64_u);
	void(*close)(_ulonglong);
	void(*stat)(_ulonglong,struct rws_stat*);
	_64_u opttab[8];
	_64_u modetab[8]
};

struct f_rw_struc {
	struct f_rw_funcs *funcs;
	_ulonglong arg;
	// undefined functionality
	void(*udf)(_8_u, _ulonglong);
};

extern struct f_rw_funcs rws_file;

#define f_rws(__rws, __func, ...)\
	(__rws)->funcs->__func(__VA_ARGS__)

#define f_rws_pwrite(__rws, ...)\
	f_rws(__rws, pwrite, (__rws)->arg, __VA_ARGS__)
#define f_rws_pread(__rws, ...)\
	f_rws(__rws, pread, (__rws)->arg, __VA_ARGS__)
#define f_rws_read(__rws, ...)\
	f_rws(__rws, read, (__rws)->arg, __VA_ARGS__)	
#define f_rws_write(__rws, ...)\
	f_rws(__rws, write, (__rws)->arg, __VA_ARGS__)
#define f_rws_listen(__rws)\
	f_rws(__rws, listen, (__rws)->arg)
# endif /*__f__rws__h*/
