#!/bin/bash
cp ../main main
export PRINTF_EN=1
x=0
while true;
do
	./main
	if [ "$?" -ne "0" ]; then
		echo "PROGRAM WAS ABORTED"
		break
	fi
	mv arout "$PWD/logs/arout$x"
	echo "log file saved at '$PWD/logs/arout$x'"
	x=$(( $x + 1 ))
done
