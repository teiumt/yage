#define __thread_internal
#include "thread.h"
#include "io.h"
#include "error.h"
#include "m_alloc.h"
#if !defined(__ffly_no_sysconf) && !defined(__ffly_crucial)
#define MAX_THREADS F_CONF(max_threads)
#else
#	define MAX_THREADS 20
#endif
#define PAGE_SIZE 4
#define UU_PAGE_SIZE 26
#define DSS (1000000*32)//32mbytes
#define DSS_PAD 16
#ifndef __ffly_no_sysconf
#	include "config.h"
#endif
# define T_USEABLE 0x1
#include "log.h"
#include "msg.h"
#include "proc.h"
#include "linux/mman.h"
#define MSG_BITS MSG_DOMAIN(_DOM_SYSTHREAD)
/*
	TODO:
		do somthing that does not involve {create and kill}
		have x amount of processes pending or use.
*/
/*
	why do i use pages ???
	because...
	A: less memory alloc calls,
	B: smaller realloc calls less copying of data
	C: if reloc takes place memory address changes big fucking anyance
*/
//# define debug
// no process linked
#define T_NOP 0x02
#define nextsig ((_tm.sig++)&0xffff)

#include "linux/signal.h"
#include "linux/types.h"
#include "linux/sched.h"
#include "linux/unistd.h"
#include "linux/wait.h"
#include "system/tls.h"
#include "assert.h"
static _64_u reg = 0;
#define regit(__bit)\
    __asm__("lock xorq %%rax, reg(%%rip)\n" : : "a"(__bit));

#define no_threads (_tm.next_joined)

_f_tid_t ffly_gettid(void) {
	return TTLS->id;
}

void tinit(void) {
	celr_init(&_tm._threads,BLKSIZE);
}

_8_s tstate(tstrucp __t, _32_u __sig, _8_u __state) {
	if (__t->sig != __sig) {
		MSG(WARN, "signatures dont match, thus indecating repossession.\n")
		return Y_FAIL;
	}

	if (tm_hget(__t->id) == TM_DEADEND) {
		MSG(WARN, "no such thread.\n")
		return Y_FAIL;
	}

	threadp t;
	t = (threadp)__t;
	
	if (__state == T_ALIVE) {
		return t->alive-1;
	} else {
		return (!t->alive)-1; 
	}
	return -1;
}
static mlock give = MUTEX_INIT;
static mlock want = MUTEX_INIT;
static threadp free_thread;
static _8_s free_aval = -1;

#define WINDDOWN 1
static _64_u bits = 0;


_8_s static trypost_availability(threadp __th) {
	if(!mt_trylock(&give)){
		free_thread = __th;
		free_aval = 0;
		while(!free_aval) {
			if (bits&WINDDOWN) {
				MSG(INFO, "%u: caught winddown signal.\n", __th->t.id)
				return -1;
			}
			doze(0,1);
		}
		mt_unlock(&give);
		return 0;
	}
	return -1;
}

void static
prox(void) {
	void *arg_p;
	__asm__("movq 8(%%rbp), %%rdi\n\t"
			"movq %%rdi, %0": "=m"(arg_p) : : "rdi");
	threadp thr;
	ffly_tls_init();
	ffly_process_prep();
	proc_ctx_setup();
	thr = (threadp)arg_p;
	regit(1<<thr->t.id);
	thr->lock = 0;
	MSG(INFO, "new thread %u : %p, PID: %u.\n", thr->t.id, tm_hget(thr->t.id), thr->t.pid)
	TTLS->id = thr->t.id;
_again:
	thr->routine(thr->arg_p);
	if(!trypost_availability(thr)) {
		goto _again;
	}

	thr->exit = 0;
	proc_ctx_cleanup();
	tm_del(thr);
	ffly_arhangi();
	/*
		tell every one im dead
	*/
	thr->flags |= T_USEABLE;	
	regit(1<<thr->t.id);
	exit(0);
}

_y_err tkill(tstrucp __t, _32_u __sig) {
	if (__t->sig != __sig) {
		MSG(WARN, "signatures dont match, thus indecating repossession.\n")
		return Y_FAIL;
	}

	if (tm_hget(__t->id) == TM_DEADEND) {
		MSG(WARN, "no such thread.\n")
		return Y_FAIL;
	}

	threadp t;
	t = (threadp)__t;
	if (kill(t->t.pid, SIGKILL) == -1) {
		MSG(ERROR, "failed to kill..\n")
		return Y_FAIL;
	}
	return FFLY_SUCCESS;
}

_8_s twait(tstrucp __t, _32_u __sig) {
	if (__t->sig != __sig) {
		MSG(WARN, "signatures dont match, thus indecating repossession.\n")
		return Y_FAIL;
	}

	if (tm_hget(__t->id) == TM_DEADEND) {
		MSG(WARN, "no such thread.\n")
		return Y_FAIL;
	}

	threadp t;
	t = (threadp)__t;
	MSG(INFO, "waiting for : %u.\n", __t->id)
	wait4(t->t.pid, NULL, __WALL|__WCLONE, NULL);
}

_8_s static attempt_to_want(tstrucp *__th, void*(*__p)(void*), void *__arg_p) {
	threadp th = NULL;
	if (!free_aval) {
		if (!mt_trylock(&want)){
			th = free_thread;
			th->arg_p = __arg_p;
			th->routine = __p;
			*__th = th;
			free_aval = -1;	
			mt_unlock(&want);
			MSG(INFO, "managed to snare thread-%u.\n",th->t.id)
			return 0;
		}
	}
	return -1;
}

void static thread_marrow(threadp thr){
	/*
	this is stupid as mmap also allows us to automaticly grow i think?
	*/ 
	thr->sp = (_8_u*)mmap(NULL, DSS+DSS_PAD, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);

	_64_u tls;
	tls = (_8_u*)mmap(NULL, TLS_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
	thr->tls = TLS_BASE(tls);
	assert(thr->sp != NULL);
	assert(thr->tls != NULL);
	*(void**)thr->tls = thr->tls;
}

_y_err tcreat(tstrucp *__th, void*(*__p)(void*), void *__arg_p) {
	/*
		dont just error out try on waiting for another thread to die
	*/
	if (no_threads == MAX_THREADS) {
		MSG(ERROR, "thread: only %u threads are allowed.\n", MAX_THREADS)
		return Y_FAIL;
	}
	if (!attempt_to_want(__th,__p,__arg_p)) {
		return Y_SUCC;
	}

	mt_lock(&_tm.lock);
	_32_u toff;

	_32_i h;
	if ((h = tm_obtain_hole()) == -1) {
		goto _fail;
	}

	threadp thr_reuse = NULL;
	if (!tm_unused_fetch(&thr_reuse,h)) {
		*__th = &thr_reuse->t;
		mt_unlock(&_tm.lock);
		goto _exec;
	}

	toff = _tm._threads.off++;
	MSG(INFO, "thread id: %u\n", h)
	mt_unlock(&_tm.lock);
	/*
		in the case we find to free space for thread structure
	*/
	goto _alloc;
_exec:
	{
//	printf("stage, exec.\n");

	threadp thr = tm_hget(h);
	mt_lock(&_tm.lock);
	_tm.joined[thr->idx = (--_tm.next_joined)] = thr->id;
	mt_unlock(&_tm.lock);
	/*
		we might of caught a thread thats still working, so wait.
		TODO:
			capture it and tame it.
	*/
	if (!(thr->flags&T_NOP)) {
		/*
			wait for release
		*/
		MSG(INFO, "waiting for thread %u to be released.\n", h)
		while(!(thr->flags&T_USEABLE));
		/*
			TODO:
				if the thread that we have here is on the edge of exiting
				pause it until be are done then tell it to jump to the start
		*/
		/*
			make sure thread is dead before use as the stack will be reused and 
			if we overwrite somthing important its going to cause havoc.
		*/
		wait4(thr->t.pid, NULL, __WALL|__WCLONE, NULL);
		MSG(INFO, "thread was released.\n")
	}

	if(thr_reuse != NULL) {
		thr->arg_p = __arg_p;
		thr->routine = __p;
	}else{
		thread_marrow(thr);
		thr->arg_p = __arg_p;
		thr->routine = __p;
	}

	thr->t.ident = 1<<thr->t.id;
	thr->t.sig = nextsig;
	thr->flags = 0x0;
	thr->alive = 0;
	thr->exit = -1;
	thr->lock = -1;
	_8_u *sp = (((_64_u)thr->sp))&~(_64_u)15;
	
	*(void**)(sp+(DSS-16)) = (void*)prox;
	*(void**)(sp+(DSS-8)) = (void*)thr;
	__linux_pid_t pid;
	if ((pid = clone(CLONE_VM|CLONE_SIGHAND|CLONE_FILES|CLONE_FS|SIGCHLD|CLONE_SETTLS,
		(_64_u)(sp+(DSS-16)), NULL, NULL, (_64_u)thr->tls)) == (__linux_pid_t)-1)
	{
		ffly_fprintf(ffly_err, "thread, failed to create.\n");
		return Y_FAIL;
	}

	thr->t.pid = pid;
	
	while(thr->lock == -1);
	
	}
	return FFLY_SUCCESS;

_alloc:
	mt_lock(&_tm.lock);
	tm_morealloc();
	{
		threadp t;
		t = ((threadp)_tm._threads.p[toff>>PAGE_SHIFT])+(toff&PAGE_MASK);
		/*
			NOTE:
				t->id0 and t->id1 are in a union with t->id
		*/
		t->id0 = toff&PAGE_MASK;
		t->id1 = toff>>PAGE_SHIFT;
		massert(t->id1 == (t->id>>16),"dont match, expected: %x but got: %x\n",t->id1,(t->id>>16));//sanity check
		massert(get_thrbyid(t->id) == t,"not same, %x{%x:%x}\n",t->id,t->id0,t->id1);
		*__th = &t->t;
		t->t.id = h;
		t->flags = T_USEABLE|T_NOP;
		tm_hset(h, t);
	}
	mt_unlock(&_tm.lock);
	goto _exec;
_fail:
	mt_unlock(&_tm.lock);
	return Y_FAIL;
}
#include "ffly_def.h"
void static thread_deadweight(threadp cur){
	if (cur->exit == -1) { // if exit was a not a success and has been forced to stop
		_64_u off = tls_getoff(pctx.ar);
		ffly_arhang(((_8_u*)cur->tls)+off);	
	}
	if (cur->sp != NULL) {
		MSG(INFO, "\t.- stack release.\n")
		munmap(cur->sp, DSS+DSS_PAD);
	}
	if (cur->tls != NULL) {
		MSG(INFO, "\t.- tls release.\n")
		munmap(TLS_REVERT(cur->tls), TLS_SIZE);
	}
}

#include "time.h"
_y_err ffly_thread_cleanup(void) {
	bits = WINDDOWN;
	threadp cur;
	MSG(INFO, "thread/s - shutdown&cleanup, phase{0}.\n")
	MSG(INFO, "waiting threads to finish up.\n")
	_64_u cnt = 0;
	while(reg != 0) {
		doze(0,4);

		if (cnt>4) {
			MSG(ERROR, "thread/s hasn't disengaged, signifying fault{%x}.\n",reg)
			MSG(ERROR, "sent kill signal to insure unresponsive process/s dont linger.\n")
			_int_u i;
			i = _tm.next_joined;
			while(i != 64) {
				_32_u id;
				id = _tm.joined[i++];
				cur = get_thrbyid(id);
				if (cur->exit == -1) {
					MSG(ERROR, "PID: %u, HOLE: %u, SIG: %u.\n", cur->t.pid, cur->t.id,cur->t.sig)
					kill(cur->t.pid, SIGKILL);
					thread_deadweight(cur);
				}
			}
			return -1;
		}
		cnt++;
		MSG(INFO, "waiting on %u, cnt: %u\n", reg,cnt)
	}
	MSG(INFO, "moving on.\n")
	_int_u i;
	i = _tm.next_joined;
	
	while(i != 64) {
		_32_u id;
		id = _tm.joined[i++];
		cur = get_thrbyid(id);
		MSG(INFO, "thread with id: %u:%u, cleaning.\n", id, cur->t.pid)

		wait4(cur->t.pid, NULL, __WALL|__WCLONE, NULL);
		thread_deadweight(cur);	
	}

	MSG(INFO, "thread free, phase{1}.\n")
	celr_free(&_tm._threads);
	MSG(INFO, "uu free, phase{2}.\n")
	_tm.uu.off = 0;
}
