# ifndef __ffly__resin__mm__h
# define __ffly__resin__mm__h
# include "../y_int.h"
void* ff_resin_mmap(_int_u);
void ff_resin_munmap(void*);
# endif /*__ffly__resin__mm__h*/
