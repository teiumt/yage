# include "tools.h"
# include "io.h"
# include "m_alloc.h"
# include "string.h"
#define newchunk ((struct chunk*)m_alloc(sizeof(struct chunk)))
#define ROW_SHIFT 5
#define ROW_LEN (1<<ROW_SHIFT)
#define ROW_MASK (ROW_LEN-1)
#define CHUNK_SHFT 4
#define CHUNK_SIZE (1<<CHUNK_SHFT)
#define CHUNK_MASK (CHUNK_SIZE-1)
#define RCSHFT (ROW_SHIFT+CHUNK_SHFT)
#define INBUFSZ (1<<RCSHFT)
#define OUTBUFSZ (CHUNK_SIZE+(1<<RCSHFT))
#define ischar(__c) ((__c>='a' && __c<='z') || (__c>='A' && __c<='Z') || (__c>='0'&&__c<='9'))
void static chrdump(_8_u *__dst, _8_u *__src, _int_u __size) {
	_8_u *d, *s = __src;
	d = __dst;
	_int_u re;

	char c;
	re = __size&~ROW_MASK;
	_8_u *e = s+re;
	while(s != e) {
		_8_u i;
		i = 0;
		for(;i != ROW_LEN;i++) {
			c = s[i];
			if (!ischar(c)) c = '.';
			*(d++) = c;
		}
		s+=ROW_LEN;
		*(d++) = '\n';
	}
	
	_8_u left = __size&ROW_MASK;
	if (left>0) {
		e = s+left;
		while(s != e) {
			c = *(s++);
			if (!ischar(c)) c = '.';
			*(d++) = c;
		}
		*(d++) = '\n';
	}
}

void static
_ct_init(struct context *__ct) {
}

void static
_ct_de_init(struct context *__ct) {
}
void _f_chrdump(struct f_dem *__dem, struct f_avs_page *__pgv, _int_u __np) {

}

void static
_dem_chrdump(struct f_dem_gwn *__dem) {
	struct f_gwn_stat st;
	void *buf;
	__dem->core.stat(__dem->core.in_arg, &st);
	if (!st.size)
		return;
	_8_u in_buf[INBUFSZ];
	_8_u out_buf[OUTBUFSZ];
	_int_u ce, off = 0;
	ce = st.size&~((1<<RCSHFT)-1);

	while(off != ce) {
		__dem->core.in(__dem->core.in_arg, in_buf, INBUFSZ, off);
		chrdump(out_buf, in_buf, INBUFSZ);
		__dem->core.out(__dem->core.out_arg, out_buf, OUTBUFSZ, off+(off>>CHUNK_SHFT));
		off+=CHUNK_SIZE;
	}

	_int_u left;
	left = st.size-off;
	if (left>0) {
		__dem->core.in(__dem->core.in_arg, in_buf, left, off);
		chrdump(out_buf, in_buf, left);

		/*
			nw = number of rows why?? because of the new line char
		*/
		_int_u nw = (left+ROW_MASK)>>ROW_SHIFT;
		__dem->core.out(__dem->core.out_arg, out_buf, left+nw, off+(off>>CHUNK_SHFT));
	}
}

struct f_dem_struc ds_chardump = {
	.dem = _dem_chrdump
};

void f_chrdump(struct f_iis *__iis, _8_u *__p, _int_u __size) {
	_int_u d_size;
	_int_u nw = (__size+ROW_MASK)>>ROW_SHIFT;
	d_size = __size+nw;
	_8_u *d;
	d = (_8_u*)m_alloc(d_size);
	chrdump(d, __p, __size);
	__iis->out(d, d_size);
	m_free(d);
}

void ffly_chrdump(_8_u *__p, _int_u __size) {
	f_chrdump(&_f_out, __p, __size);
}
