# include "m.h"
# include "m_alloc.h"
# include "io.h"
#include "msg.h"
struct mstruc _f_m = {
#ifdef __f_debug
	0, 0, 0, 0
#endif
};


void meminfo(struct meminfo *__info) {
#ifdef __ffly_debug
	__info->used = _f_m.used;
	__info->aa = _f_m.aa;
#endif
}
#define MSG_BITS MSG_DOMAIN(_DOM_NOWHERE)
void dmi(void) {
#ifdef __ffly_debug
	MSG(INFO,"memused: %u.%u-kilobytes.\n", _f_m.used>>10, _f_m.used&0x1023);
	MSG(INFO,"AA count: %u.\n", _f_m.aa);
#endif
}
