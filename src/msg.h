#ifndef __y__msg__h
#define __y__msg__h
#include "y_int.h"
#include <stdarg.h>
#include "domain.h"
//dug = debug
#define FF_DUG(__bits, ...)\
	MSG(__bits|DEBUG, __VA_ARGS__)
#define FFLY_MSG ffly_msg
#define MSG_DOMAIN(__dom) __dom
#define FFLY_MSG_DOMAIN(__domain) __domain
#define FFLY_MSG_DOM_BO	0
#define FFLY_MSG_DOM 0x00000000000000ff
#define FFLY_MSG_LEVEL (0x00000000000000ff<<FFLY_MSG_DOM_BO)
struct domain_s {
    char const *str;
    _int_u len;
};
#define SUBDOM(__name, __str, __len)\
	struct domain_s __name = {"/" __str,__len+1};
#define INFO	(0<<FFLY_MSG_DOM_BO)
#define WARN	(1<<FFLY_MSG_DOM_BO)
#define DEBUG	(2<<FFLY_MSG_DOM_BO)
#define ERROR	(3<<FFLY_MSG_DOM_BO)
#define CRIT	(4<<FFLY_MSG_DOM_BO)
#ifndef MSG_SUBDOM
#define MSG_SUBDOM (&dummy_domain)
#endif
#define N_DOMS 35
extern struct domain_s dummy_domain;
extern struct domain_s doms[N_DOMS];
#define MSG(__bits, ...)\
	ffly_msg(MSG_SUBDOM, (MSG_BITS), (__bits), __VA_ARGS__);
extern void(*ffly_msgout)(void*, _int_u);
void ffly_msg(struct domain_s*, struct domain_s*, _64_u, char const*, ...);
#endif /*__y__msg__h*/
