# ifndef __ffly__rosin__h
# define __ffly__rosin__h
# include "y_int.h"
# include "types.h"

/*
	unlike resin, rosin have complete access to the engine and is not within a sandbox environment
	rosin - calling of functions around engine
	resin - same but limited, only things diffrent is the flexibility e.g. no loops, no jumps, add, sub, div, etc
*/
typedef struct ffly_rosin {
	_int_u stack_size;
	_8_u *stack;

	_8_u(*fetch_byte)(_f_off_t);
	void(*ip_incr)(_int_u);
	_f_addr_t(*get_ip)(void);
	void(*set_ip)(_f_addr_t);
	_f_off_t ip_off;

} *ffly_rosinp;

struct rr_struc {
	/*
		sub operation
	*/
	_8_u op;
	/*
		args for operation
	*/
	void **args;
	// number of args
	_8_u n;
} __attribute__((packed));

void ff_rosin_init(ffly_rosinp);
void ff_rosin_de_init(ffly_rosinp);
void ff_rosin_run(ffly_rosinp);
# endif /*__ffly__rosin__h*/
