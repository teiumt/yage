# include "event.h"
# include "system/errno.h"
# include "system/error.h"
# include "system/io.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/event.h"
# include "msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_EVENT)
ff_eventp
ff_event_creat(_8_u __kind, _8_u __field, void *__data,
	_int_u __size, _f_err_t *__err)
{
	ff_eventp ret = ff_event_alloc(__err);
	ff_event_build(__kind, __field, __data, __size, ret);
	return ret;
}

_f_err_t ff_event_del(ff_eventp __event) {
	if (__event->data != NULL) {
		MSG(INFO, "event data freed.\n");
		__f_mem_free(__event->data);
	}
	ff_event_free(__event);
}

_8_i ff_event_poll(ff_eventp *__event) {
	if (!ffly_pending_event()) return -1;
	if (!ffly_event_pop(__event))
		return 0;
	return -1;
}

/*
	dont use struct ->next 
	this method may use more memory or wast memory but
	the speed is needed here.
*/
#define FAST_EVENTS 50
ffly_event_t static *events = NULL;
ffly_event_t static *fresh_event;
ffly_event_t static **free_events = NULL;
ffly_event_t static **next_free = NULL;
#define MAG0 'e'
#define MAG1 'v'
#define MAG2 'e'
struct ph {
	// might be removed later
	char ident[3];
};

ff_eventp ff_event_alloc(_f_err_t *__err) {
	*__err = FFLY_SUCCESS;
	if (!events) {
		if ((events = (ffly_event_t*)__f_mem_alloc(FAST_EVENTS*sizeof(ffly_event_t))) == NULL) {
			*__err = FFLY_FAILURE;
			return NULL;
		}
		fresh_event = events;
	}
	
	MSG(INFO, "event allocated.\n")
	if (next_free>free_events)
		return *(--next_free);
	
	/*
		if fast events are taken up then do a pure allocation
	*/
	if (fresh_event >= events+FAST_EVENTS) {
		MSG(INFO, "fast full, pure alloc.\n")
		_8_u *p;
		p = (_8_u*)__f_mem_alloc(sizeof(ffly_event_t)+sizeof(struct ph));
		struct ph *h;
		h = (struct ph*)p;
		h->ident[0] = MAG0;
		h->ident[1] = MAG1;
		h->ident[2] = MAG2;
		return (ffly_event_t*)(p+sizeof(struct ph));
	}
	return fresh_event++;
}

_f_err_t ff_event_free(ff_eventp __event) {
	if (!__event) return FFLY_SUCCESS;
	if (__event<events && __event >= events+FAST_EVENTS) {
		_8_u *p;
		p = (_8_u*)__event;

		struct ph *h;
		h = (struct ph*)p;
		if (h->ident[0] != MAG0) {
			MSG(ERROR, "mag0 is not correct")
			goto _err;
		}

		if (h->ident[1] != MAG1) {
			MSG(ERROR, "mag1 is not correct")
			goto _err;
		}

		if (h->ident[2] != MAG2) {
			MSG(ERROR, "mag2 is not correct")
			goto _err;	
		}

		__f_mem_free(p+sizeof(struct ph));
	} else {
		if (!free_events) {
			if ((free_events = (ffly_event_t**)__f_mem_alloc(FAST_EVENTS*sizeof(ffly_event_t*))) == NULL) {
				return FFLY_FAILURE;
			}
			next_free = free_events;
		}

		if (__event == fresh_event-1)
			fresh_event--;
		else {
			if (next_free >= free_events+FAST_EVENTS) {
				ffly_fprintf(ffly_err, "error.\n");
			}
			*(next_free++) = __event;
		}
	}
	MSG(INFO, "freed event.\n")
	return FFLY_SUCCESS;
_err:
	return FFLY_FAILURE;
}

void ff_event_cleanup(void) {
	if (events != NULL)
		__f_mem_free(events);
	if (free_events != NULL)
		__f_mem_free(free_events);
}
