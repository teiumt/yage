# include "mkfs.h"
# include "m/m.h"

void f_mkfs_m(void(*__out)(void*, void*, _int_u), void *__arg) {
	struct mfs_tract t;
	t.crnc = 0;
	t.crsc = 0;
	t.root = 0;
	t.cr = 0;
	t.sc = 0;
	t.slabs = 0;
	t.off = (sizeof(struct mfs_tract)+(MFS_SLAB_SIZE-1))>>MFS_SLAB_SHIFT;
	__out(__arg, &t, sizeof(struct mfs_tract));
}
