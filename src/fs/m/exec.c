# include "m.h"
# include "../../system/io.h"
# include "../../string.h"
// tobe renamed

/*
	used for node init/setup like loading dir sub nodes into hash table.
	why not just use structs?
		because its less flexible and its not a real file system.
*/
_8_u static *cc = NULL;
void loadin_node(struct mfs_node*, struct mfs_engrave*);
struct mfs_node* mfs_node_new(_32_u);
void static op0(void) {
/*
	load node
*/
	_32_u nn, ca;
	nn = *(_32_u*)cc;
	ca = *(_32_u*)(cc+4);
	printf("op0: %u, %u.\n", nn, ca);

	struct mfs_engrave eg;
	cr_read(&eg, sizeof(struct mfs_engrave), ca);
	loadin_node(mfs_node_new(nn), &eg);
}

struct mfs_node *mfs_getnode(_32_u);
/*
	place node into subnodes hash table & create new one if not there
*/
void static op1(void) {	
	struct mfs_node *dir, *sub;
	dir = mfs_getnode(*(_32_u*)cc);
	sub = mfs_getnode(*(_32_u*)(cc+4));
	printf("op1: %u, %u\n", dir, sub);

	if (!(dir->ntf&MFS_HHT)) {
		dir->h = mfs_hash_new();
		dir->ntf |= MFS_HHT;
	}

	mfs_hash_put(dir->h, sub->name, strlen(sub->name), sub->nn);
}

struct op_s {
	void(*op)(void);
	_int_u size;
};

static struct op_s op[] = {
	{op0, 8},
	{op1, 8}
};

# include "../../hexdump.h"
void mfs_exec(_8_u *__code, _int_u __n) {
	printf("exec code dump.\n");
	ffly_hexdump(__code, __n);
	_8_u *end;
	cc = __code;
	end = cc+__n;
	struct op_s *os;
	_8_u on;
	printf("mfs-exec. %u\n", __n);
	while(cc != end) {
		on = *(cc++);
		os = op+on;
		printf("op no: %u\n", on);
		os->op();
		cc+=op->size;
	}
}
