# include "m.h"
# include "../../ffly_def.h"
# include "../../system/io.h"
# include "../../dep/mem_cpy.h"
# include "../../dep/mem_set.h"
# include "../../linux/unistd.h"
# include "../../linux/fcntl.h"
# include "../../linux/stat.h"
# include "../../system/nanosleep.h"
#define SIZE 8192
_8_u static data[SIZE];
void static __write(void *__buf, _int_u __size, _64_u __offset) {
	ffly_nanosleep(0, 1000000); // 10ms
	if (__offset+__size>=SIZE) {
		printf("r/w error, %u:%u\n", __offset, __size);
		return;
	}
	f_mem_cpy(data+__offset, __buf, __size);
}

void static __read(void *__buf, _int_u __size, _64_u __offset) {
	ffly_nanosleep(0, 10000000);
	if (__offset+__size>SIZE) {
		printf("r/w error, %u:%u\n", __offset, __size);
		return;
	}

	f_mem_cpy(__buf, data+__offset, __size);
}

# include "../../fs.h"
# include "../../hexdump.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	f_mem_set(data, '@', SIZE);
	struct stat st;
	int fd;
	fd = open("test.bin", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
	fstat(fd, &st);
	read(fd, data, st.st_size);

	struct ffly_mfs _mfs = {
		.write = __write,
		.read = __read,
		.top = NULL, .bin = NULL,
		.off = 0
	};
	mfs = &_mfs;

	void(*fs_fin)(void);
	fs_fin = ffly_fs(_ff_mfs);

//	_32_u f;
	fs_mkdir("/test");
//	f = fs_open("/test/test.txt", MFS_CREAT);
//	fs_close(f);

	mfs_tree(mfs->root);
	fs_fin();
	pwrite(fd, data, SIZE, 0);

	close(fd);
}
