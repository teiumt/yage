# include "m.h"
# include "../../memory/mem_alloc.h"
# include "../../memory/mem_free.h"
# include "../../memory/mem_realloc.h"
# include "../../system/io.h"
# include "../../dep/str_cpy.h"
# include "../../dep/mem_set.h"
# include "../../dep/mem_cpy.h"
# include "../../hexdump.h"
#define MSG_BITS MSG_BITS_MFS
#define is_flag(__flags, __flag) \
	(((__flags)&(__flag)) == (__flag))
struct ffly_mfs *mfs;
struct mfs_cauldron mfs_cr = {.n = 0};
static _8_i cr_init = -1;
_32_u static
cr_alloc(_int_u __size) {
	mfs_cr.n+=__size;
	if (cr_init == -1) {
		mfs_cr.s = mfs_alloc(__size);	
		cr_init = 0;
		goto _sk;
	}
	mfs_resize(mfs_cr.s, mfs_cr.n);
_sk:
	return mfs_cr.n-__size;
}

void static commit_node(struct mfs_node*);
static struct mfs_node **nodes = NULL;
static _32_u nc = 0;

void loadin_node(struct mfs_node*, struct mfs_engrave*);
struct mfs_node *mfs_getnode(_32_u __n) {
	if ((__n+1)>nc) {
		_int_u prior;
		prior = nc;
		nc = __n+1;

		/*
			TODO use pages
				remove alloc
		*/

		if (!nodes)
			nodes = (struct mfs_node**)__f_mem_alloc(nc*sizeof(struct mfs_node*));
		else
			nodes = (struct mfs_node**)__f_mem_realloc(nodes, nc*sizeof(struct mfs_node*));

		_int_u n;
		n = prior;
		while(n != nc)
			*(nodes+(n++)) = NULL;
	}

	struct mfs_node *n, **np;
	np = nodes+__n;
	if (!(n = *np)) {
		n = (*np = (struct mfs_node*)__f_mem_alloc(sizeof(struct mfs_node)));
		f_mem_set(n, 0, sizeof(struct mfs_node));
		n->nn = __n;

		/*
			if node has not been loaded yet then say that is should be
		*/
		n->flags = MFS_NNL;

	}

	return n;
}
# define DEBUG
struct mfs_node static* mfs_nget(struct mfs_scope*, _32_u);
struct mfs_scope* mfs_build(_32_u*, _int_u);
void mfs_dmscope(struct mfs_scope*);

// write to scope
void mfs_swrite(struct mfs_scope *__sc, void *__buf,
	_int_u __size, _64_u __offset)
{
	MSG(INFO, "mds_write{size: %u, offset: %u}\n", __size, __offset)
	_int_u ss, offset;
	ss = (__offset>>MFS_SLAB_SHIFT);
	struct mfs_slab **s, *sb;
	s = __sc->slabs+ss;
	offset = __offset-(ss*MFS_SLAB_SIZE);

	struct mfs_slab **end;
	end = s+(__size>>MFS_SLAB_SHIFT);
	_8_u *p;

	p = (_8_u*)__buf;
	if (offset>0 && __size>=MFS_SLAB_SIZE) {
		_int_u sz;
		fsus_write(mfs->m, p, sz = (MFS_SLAB_SIZE-offset), ((*s)->off*MFS_SLAB_SIZE)+offset);
		s++;
		p+=sz;
		offset = 0;
	}

	if (offset+__size>MFS_SLAB_SIZE && __size<MFS_SLAB_SIZE) {
		_int_u a0, a1;
		a0 = MFS_SLAB_SIZE-offset;
		a1 = __size-a0;
		fsus_write(mfs->m, p, a0, ((*s)->off*MFS_SLAB_SIZE)+offset);
		fsus_write(mfs->m, p+a0, a1, (*(s+1))->off*MFS_SLAB_SIZE);
		return;
	}

	while(s<end) {
		sb = *(s++);
		fsus_write(mfs->m, p, MFS_SLAB_SIZE, sb->off*MFS_SLAB_SIZE);
		p+=MFS_SLAB_SIZE;
	}

	_int_u left;
	left = __size-(p-(_8_u*)__buf);
	if (left>0) {
		fsus_write(mfs->m, p, left, ((*s)->off*MFS_SLAB_SIZE)+offset);
	}
}

// read from scope
void mfs_sread(struct mfs_scope *__sc, void *__buf,
	_int_u __size, _64_u __offset)
{
	MSG(INFO, "mfs_sread{size: %u, offset: %u}\n", __size, __offset)
	_int_u ss, offset;
	ss = (__offset>>MFS_SLAB_SHIFT);
	struct mfs_slab **s, *sb;
	s = __sc->slabs+ss;
	offset = __offset-(ss*MFS_SLAB_SIZE);

	struct mfs_slab **end;
	end = s+(__size>>MFS_SLAB_SHIFT);
	_8_u *p;

//	printf("%u, %u, %u, %u\n", offset, ss, __offset, __size);
	p = (_8_u*)__buf;
	if (offset>0 && __size>=MFS_SLAB_SIZE) {
		_int_u sz;
		fsus_read(mfs->m, p, sz = (MFS_SLAB_SIZE-offset), ((*s)->off*MFS_SLAB_SIZE)+offset);
		s++;
		p+=sz;
		offset = 0;
	}

	if (offset+__size>MFS_SLAB_SIZE && __size<MFS_SLAB_SIZE) {
		_int_u a0, a1;
		a0 = MFS_SLAB_SIZE-offset;
		a1 = __size-a0;
		fsus_read(mfs->m, p, a0, ((*s)->off*MFS_SLAB_SIZE)+offset);
		fsus_read(mfs->m, p+a0, a1, (*(s+1))->off*MFS_SLAB_SIZE);
		return;
	}

	while(s<end) {
		sb = *(s++);
		fsus_read(mfs->m, p, MFS_SLAB_SIZE, sb->off*MFS_SLAB_SIZE);
		p+=MFS_SLAB_SIZE;
	}

	_int_u left;
	left = __size-(p-(_8_u*)__buf);
	if (left>0) {
		fsus_read(mfs->m, p, left, ((*s)->off*MFS_SLAB_SIZE)+offset);
	}
}

struct mfs_scope* mfs_alloc(_int_u);
void mfs_free(struct mfs_scope*);

struct mfs_node* mfs_node_new(_32_u __nn) {
	struct mfs_node *n;
	n = (struct mfs_node*)mfs_getnode(__nn);
	n->flags |= MFS_NNA|MFS_NDNA;
	n->cbs = NULL;
	n->cbc = 0;
	f_mem_set(n->name, 0, sizeof(n->name));
	return n;
}
/*
	move into 'commit_node' ???

	upload prep code to disk
*/
void static
unload_node(struct mfs_node *__n) {
	if (is_flag(__n->flags, MFS_PCP)) {
		_8_u *buf, *d;
		buf = (_8_u*)__f_mem_alloc(__n->pcs);
		d = buf;
		struct mfs_cb_struc *_cb, *_b;
		_cb = (struct mfs_cb_struc*)__f_mem_alloc(__n->cbc*sizeof(struct mfs_cb_struc));
		struct mfs_cb **cb, *b;
		cb = __n->cbs;
		_int_u i;
		i = 0;
		for(;i != __n->cbc;i++,cb++) {
			_b = _cb+i;
			b = *cb;
			_b->off = b->off;
			_b->size = b->size;
			f_mem_cpy(d, b->p, b->size);	
			d+=b->size;
		}

		__n->_cbs = mfs_valloc(__n->cbc*sizeof(struct mfs_cb_struc))<<MFS_SLAB_SHIFT;
		__n->pc = mfs_valloc(__n->pcs)<<MFS_SLAB_SHIFT;
		printf("unloadcode: %u, %u\n", __n->pcs, __n->pc);
		ffly_hexdump(buf, __n->pcs);
		fsus_write(mfs->m, buf, __n->pcs, __n->pc);
		fsus_write(mfs->m, _cb, __n->cbc*sizeof(struct mfs_cb_struc), __n->_cbs);
		__f_mem_free(buf);
	}
}

_16_u static
mfs_cbinsert(struct mfs_node *__n, _8_u *__code, _int_u __nn) {
	__n->cbc++;
	if (!__n->cbs) {
		__n->cbs = (struct mfs_cb**)__f_mem_alloc(__n->cbc*sizeof(struct mfs_cb*));
	} else {
		__n->cbs = (struct mfs_cb**)__f_mem_realloc(__n->cbs, __n->cbc*sizeof(struct mfs_cb*));
	}

	struct mfs_cb **cbp, *b;
	cbp = __n->cbs+(__n->cbc-1);
	b = *cbp = (struct mfs_cb*)__f_mem_alloc(sizeof(struct mfs_cb));
	b->p = __f_mem_alloc(__nn);
	f_mem_cpy(b->p, __code, __nn);
	b->size = __nn;
	MSG(INFO, "new code block-%u, size: %u.\n", __n->cbc-1, __nn)
	b->off = __n->pcs;
	__n->pcs+=__nn;
	return __n->cbc-1;
}

void static mfs_node_destroy(struct mfs_node *__n) {
	__f_mem_free(__n);
}

struct mfs_node* mfs_lookup(char const *__path) {
	char const *p;
	char buf[128];
	char *bufp;
	_32_u _n;
	struct mfs_node *n;
	_n = (n = mfs->root)->n;
	p = __path;
	_8_i fnd;
_again:
	bufp = buf;
	p++;//skip '/'
	while(*p != '/' && *p != '\0')
		*(bufp++) = *(p++);
	*bufp = '\0';
	_n = mfs_hash_get(n->h, buf, bufp-buf, &fnd);
	n = mfs_getnode(_n);

	if (fnd == -1) {
		printf("dead end at %s\n", buf);
		return NULL;
	}
	if (*p == '\0')
		goto _end;
	goto _again;
_end:
	return n;
}

void static __mfs_write(struct mfs_node *__n, void *__buf, _int_u __size, _64_u __offset) {
	if (__offset+__size >= __n->size) {
		_32_u apex;

		apex = __offset+__size;
		if (is_flag(__n->flags, MFS_NDNA)) {
			__n->s = mfs_alloc(apex);
			__n->flags ^= MFS_NDNA;
			goto _sk;		
		}

		mfs_resize(__n->s, apex);
	}
_sk:
	mfs_swrite(__n->s, __buf, __size, __offset);
}

void static __mfs_read(struct mfs_node *__n, void *__buf, _int_u __size, _64_u __offset) {
	mfs_sread(__n->s, __buf, __size, __offset);
}


void static mfs_pwrite(f_vmfs_nodep __n, void *__buf, _int_u __size, _64_u __offset) {
	__mfs_write(((struct mfs_file*)__n->f)->n, __buf, __size, __offset);
}

void static mfs_pread(f_vmfs_nodep __n, void *__buf, _int_u __size, _64_u __offset) {
	__mfs_read(((struct mfs_file*)__n->f)->n, __buf, __size, __offset);
}

// 0 = root
/*
	nn = node number
*/
_32_u static nn = 1;
void static
mfs_new(struct mfs_node *__dir, struct mfs_dent *__de) {
/*
	add node to directory hash table
*/
	_8_i fnd;
	struct mfs_node *n;
	_32_u _n;
	_n = mfs_hash_get(__dir->h, __de->name, __de->nlen, &fnd);
	if (fnd == -1) {
		mfs_hash_put(__dir->h, __de->name, __de->nlen, nn++);
		n = mfs_node_new(nn-1);
	} else {
		printf("'%s' already exists.\n", __de->name);
		return;
	}
 
	f_mem_cpy(n->name, __de->name, __de->nlen);
	__de->n = n;
}

/*
	insert node info directory/node
*/
void static
insert_node(struct mfs_node *__dir, struct mfs_node *__n) {
	_int_u siz;
	__dir->n+=sizeof(_32_u);

	siz = __dir->n;
	if (is_flag(__dir->flags, MFS_NDNA)) {
		__dir->s = mfs_alloc(siz);
		__dir->flags ^= MFS_NDNA;
		goto _sk;		
	}

	mfs_resize(__dir->s, siz);
_sk:
	{
		_8_u code[18];
		*code = 0x00;
		*(_32_u*)(code+1) = __n->nn;
		*(_32_u*)(code+5) = __n->ca;
		*(code+9) = 0x01;
		*(_32_u*)(code+10) = __dir->nn;
		*(_32_u*)(code+14) = __n->nn;

		printf("op0: %u, %u, op1: %u, %u.\n", __n->nn, __n->ca, __dir->nn, __n->nn);

		mfs_cbinsert(__dir, code, 18);
		__dir->flags |= MFS_PCP;
	}
	MSG(INFO, "insert_node: node_c: %u, slab_c: %u, slab_sum: %u, CA: %u.\n", __dir->n, __dir->s->slab_c, __dir->s->slab_c*MFS_SLAB_SIZE, __n->ca)
	mfs_swrite(__dir->s, &__n->ca, sizeof(_32_u), siz-sizeof(_32_u));
	commit_node(__dir);
	return;
}

_32_u mfs_balloc(void);
void
loadin_node(struct mfs_node *__n, struct mfs_engrave *__eg) {	
	MSG(INFO, "loadin_node.\n")
	__n->n = __eg->n;
	__n->h = __eg->h;
	__n->cbc = __eg->cbc;
	__n->_cbs = __eg->cbs;
	__n->mode = __eg->mode;
	__n->flags = __eg->flags;
	__n->pc = __eg->pc;
	__n->pcs = __eg->pcs;
	f_mem_cpy(__n->name, __eg->name, sizeof(__eg->name));
	if (!is_flag(__eg->flags, MFS_NDNA)) {
		_int_u slab_c;
		slab_c = (__eg->n+(MFS_SLAB_SIZE-1))>>MFS_SLAB_SHIFT;
		_32_u *slabs;
		slabs = (_32_u*)__f_mem_alloc((__eg->bale_c*MFS_BALE_SIZE)*sizeof(_32_u));
		f_mem_set(slabs, 0xff, (__eg->bale_c*MFS_BALE_SIZE)*sizeof(_32_u));
		_int_u i, ii;
		_32_u bs;
		bs = __eg->start;
#ifdef DEBUG
		MSG(INFO, "bale_count: %u, start: %u.\n", __eg->bale_c, __eg->start)
#endif
		if (__eg->bale_c>200) {

			return;
		}
		struct mfs_bale b;
		i = 0;
		for(;i != __eg->bale_c;i++) {
#ifdef DEBUG
			MSG(INFO, "load_bale: %u.\n", bs*MFS_SLAB_SIZE)
#endif
			if (bs == MFS_BALE_NULL) {
				printf("error.\n");
				break;
			}
				
			fsus_read(mfs->m, &b, sizeof(struct mfs_bale), bs*MFS_SLAB_SIZE);
			ii = 0;
			for(;ii != MFS_BALE_SIZE;ii++) {
				if ((i*MFS_BALE_SIZE)+ii >= slab_c)
					break;
				slabs[((__eg->bale_c*MFS_BALE_SIZE)-((i+1)*MFS_BALE_SIZE))+ii] = b.slabs[ii];
#ifdef DEBUG
				MSG(INFO, "load_slab: %u-< %u.\n", b.slabs[ii], ii)
#endif
			}

			bs = b.next;
		}
#ifdef DEBUG
		ffly_hexdump(slabs, (__eg->bale_c*MFS_BALE_SIZE)*sizeof(_32_u));
#endif
		__n->s = mfs_build(slabs, slab_c);
	}
	//run

	if (is_flag(__eg->flags, MFS_PCP)) {
		_8_u *buf;
		buf = (_8_u*)__f_mem_alloc(__eg->pcs);
		printf("PC %u, %u\n", __eg->pcs, __eg->pc);
		fsus_read(mfs->m, buf, __eg->pcs, __eg->pc);
		mfs_exec(buf, __eg->pcs);
	
		struct mfs_cb *b;
		struct mfs_cb_struc *cbs, *_b;
		cbs = (struct mfs_cb_struc*)__f_mem_alloc(__n->cbc*sizeof(struct mfs_cb_struc));
		fsus_read(mfs->m, cbs, __n->cbc*sizeof(struct mfs_cb_struc), __eg->cbs);
		__n->cbs = (struct mfs_cb**)__f_mem_alloc(__n->cbc*sizeof(struct mfs_cb*));
		_int_u i;
		i = 0;
		for(;i != __n->cbc;i++) {
			_b = cbs+i;
			b = *(__n->cbs+i) = __f_mem_alloc(sizeof(struct mfs_cb));
			b->off = _b->off;
			b->size = _b->size;
			b->p = __f_mem_alloc(_b->size);
			f_mem_cpy(b->p, buf+_b->off, _b->size);
		}
		__f_mem_free(buf);
	}
}

void
map_node(struct mfs_node *__n) {
	if (is_flag(__n->flags, MFS_NNA)) {
		__n->ca = cr_alloc(sizeof(struct mfs_engrave));
		__n->flags ^= MFS_NNA;
	}
}

void
commit_node(struct mfs_node *__n) {
	printf("commiting node: %u.\n", __n->nn);
	unload_node(__n);
	struct mfs_engrave eg;
	eg.nn = __n->nn;
	eg.n = __n->n;
	eg.h = __n->h;
	eg.pc = __n->pc;
	eg.pcs = __n->pcs;
	eg.cbc = __n->cbc;
	eg.cbs = __n->_cbs;
	eg.mode = __n->mode;
	MSG(INFO, "commit_node: %u.\n", eg.n);
	f_mem_cpy(eg.name, __n->name, sizeof(__n->name));
	if (!is_flag(__n->flags, MFS_NDNA)) {
		_int_u bale_c;
		struct mfs_scope *s;
		s = __n->s;

		bale_c = (s->slab_c+(MFS_BALE_SIZE-1))>>MFS_BALE_SHIFT;
		_32_u bs, bk;
		bk = MFS_BALE_NULL;
		struct mfs_bale b;
		_int_u i, ii;
		i = 0;
		
		for(;i != bale_c;i++) {
			// bale space
			bs = mfs_balloc();	
#ifdef DEBUG
			MSG(INFO, "store_bale: %u.\n", bs)
#endif
			ii = 0;
			f_mem_set(b.slabs, 255, MFS_BALE_SIZE*sizeof(_32_u));
			for(;ii != MFS_BALE_SIZE;ii++) {
				if ((i*MFS_BALE_SIZE)+ii >= s->slab_c)
					break;

				b.slabs[ii] = (*(s->slabs+(i*MFS_BALE_SIZE)+ii))->in;
#ifdef DEBUG
				MSG(INFO, "store_slab: %u-< %u.\n", b.slabs[ii], ii);
#endif
			}
#ifdef DEBUG
			f_hexdump(&_f_msg, b.slabs, MFS_BALE_SIZE*sizeof(_32_u));
#endif
			b.next = bk;
			bk = bs;
			fsus_write(mfs->m, &b, sizeof(struct mfs_bale), bs*MFS_SLAB_SIZE);
		}

		eg.start = bs;
		eg.bale_c = bale_c;
	}

	printf("node ca: %u\n", __n->ca);
	cr_write(&eg, sizeof(struct mfs_engrave), __n->ca);
}
/*
	create directory entry
*/
void static
mfs_creat(struct mfs_node *__dir, struct mfs_dent *__de) {
	mfs_new(__dir, __de);
	struct mfs_node *n;
	n = __de->n;
	n->mode = MFS_NREG;
	n->n = 0;
	map_node(n);
	commit_node(n);
	insert_node(__dir, n);
}

/*
	make directory
*/
void static
__mfs_mkdir(struct mfs_node *__dir, struct mfs_dent *__de) {
	mfs_new(__dir, __de);
	struct mfs_node *n;
	n = __de->n;
	n->h = mfs_hash_new();
	n->mode = MFS_NDIR;
	n->n = 0;
	map_node(n);
	commit_node(n);
	insert_node(__dir, n);
}

void static
mfs_mkdir(char const *__path) {
	struct mfs_node *d, *n;
	_8_i fnd;
	_32_u _n;
	char dir[1024];
	char buf[128];
	char *bp;
	char *dp = dir;
	char *pp = __path;
_again:
		pp++;
		bp = buf;
		while(*pp != '/' && *pp != '\0') {
			*(bp++) = *(pp++);
		}

		if (*pp == '\0') {
			*dp = '\0';	
			*bp = '\0';
			goto _esc;
		}

		*dp = '/';
		f_mem_cpy(dp+1, buf, bp-buf);
		dp+=(bp-buf)+1;
	goto _again;
_esc:
	printf("dir: %s, path: %s\n", dir, __path);

	d = dir == dp?mfs->root:mfs_lookup(dir);

	if (!d) {
		// error dir not found
		return NULL;
	}

	_n = mfs_hash_get(d->h, buf, bp-buf, &fnd);
	printf("---------> node: %u-%s\n", _n, fnd == -1?"VOID":"WORKING");
//	n = mfs_getnode(_n);


	if (fnd == -1) {
		printf("creating node.\n");
		MSG(INFO, "node not found, creating...\n");
		struct mfs_dent de;
		de.name = buf;
		de.nlen = bp-buf;
		__mfs_mkdir(d, &de);	
	}
}

struct mfs_fo file_op = {
	.write = mfs_pwrite,
	.read = mfs_pread
};

_8_s mfs_open(f_vmfs_nodep __n, char const *__path, _32_u __flags, _32_u __other) {
	struct mfs_file *f;
	f = mfs_file_new();
	f->op = &file_op;
	struct mfs_node *n, *fn;
	_32_u _n;
	_8_i fnd;
	char dir[1024];
	char buf[128];
	char *bp;
	char *dp = dir;
	char *pp = __path;
_again:
		pp++;
		bp = buf;
		while(*pp != '/' && *pp != '\0') {
			*(bp++) = *(pp++);
		}

		if (*pp == '\0') {
			*dp = '\0';	
			*bp = '\0';
			goto _esc;
		}

		*dp = '/';
		f_mem_cpy(dp+1, buf, bp-buf);
		dp+=(bp-buf)+1;
	goto _again;
_esc:
	printf("dir: %s\n", dir);

	n = dir == dp?mfs->root:mfs_lookup(dir);

	if (!n) {
		// error dir not found
		return -1;
	}

	if (n->mode != MFS_NDIR) {
		MSG(WARN, "this may be a big issue, or the user has used a non-directory in replace of a directory node.\nthis node is not a diectory please check the path.\n")
		return -1;
	}
	_n = mfs_hash_get(n->h, buf, bp-buf, &fnd);
	fn = mfs_getnode(_n);

	/*
		if node was not found & create flags is told then create a new node in said name
	*/
	if (fnd == -1 && is_flag(__flags, MFS_CREAT)) {
		/*
			create directory entry struct and then push to create entry
		*/
		struct mfs_dent de;
		de.name = buf;
		de.nlen = bp-buf;
		mfs_creat(n, &de);	
		f->n = de.n;
	} else if (!fnd) {	
		f->n = fn;
	} else 
		return -1;
	
	__n->f = f;
	return 0;
}

struct mfs_node* mfs_nget(struct mfs_scope *__s, _32_u __n) {
	struct mfs_node *n;
	_32_u ca;
	_32_u offset;
	struct mfs_engrave eg;
	offset = __n*sizeof(_32_u);

	mfs_sread(__s, &ca, sizeof(_32_u), offset);
	cr_read(&eg, sizeof(struct mfs_engrave), ca);
	printf("node_get: %u.\n", eg.nn);
	n = mfs_node_new(eg.nn);
	if (is_flag(n->flags, MFS_NNL)) {
		n->ca = ca;
#ifdef DEBUG
		printf("nget{ca: %u, offset: %u}\n", ca, offset);
#endif
		loadin_node(n, &eg);
		n->flags ^= MFS_NNL;
	}
	return n;
}

void static mfs_close(f_vmfs_nodep __n) {


}

void mfs_slabs_save(_int_u*, _32_u);
void mfs_slabs_load(_int_u, _32_u);
void ffly_mfs_init(void) {
	struct mfs_tract t;
	fsus_read(mfs->m, &t, sizeof(struct mfs_tract), 0);
	mfs->off = t.off;
	printf("mfs_init{cr: %u, crnc: %u, root: %u, crsc: %u, sc: %u, off: %u-slabs}\n", t.cr, t.crnc, t.root, t.crsc, t.sc, t.off);
	struct mfs_node *root;
	root = mfs_node_new(0);

	mfs->root = root;
	root->h = mfs_hash_new();
	root->ntf |= MFS_HHT;
	if (t.sc>0) {
		mfs_slabs_load(t.sc, t.slabs);
	}
	
	if (t.crsc>0 && t.crnc>0) {
		cr_init = 0;
		_32_u *sr_slabs;
		sr_slabs = (_32_u*)__f_mem_alloc(t.crsc*sizeof(_32_u));
		
		fsus_read(mfs->m, sr_slabs, t.crsc*sizeof(_32_u), t.cr);	
		_int_u i;
		for(i = 0;i != t.crsc;i++)
			printf("crsb: %u\n", sr_slabs[i]);
		mfs_cr.s = mfs_build(sr_slabs, t.crsc);
		mfs_cr.n = t.crnc;
		
		struct mfs_engrave eg;
		cr_read(&eg, sizeof(struct mfs_engrave), t.root);

		printf("root: h: %u, pc: %u, pcs: %u, mode: %u, flags: %u, nn: %u, n: %u, start: %u, bale_c: %u\n",
			eg.h, eg.pc, eg.pcs, eg.mode, eg.flags, eg.nn, eg.n, eg.start, eg.bale_c);
		loadin_node(root, &eg);
		__f_mem_free(sr_slabs);
		return;
	}

	map_node(root);
	commit_node(root);
}

void ffly_mfs_de_init(void) {
	struct mfs_tract t;
	_int_u i;

	t.root = mfs->root->ca;
	struct mfs_node *n;
	i = 0;
	while(i != nc) {
		n = mfs_getnode(i++);
		if (n != NULL)
			commit_node(n);
	}

	t.cr = mfs->off<<MFS_SLAB_SHIFT;
	t.crnc = mfs_cr.n;
	t.off = mfs->off;
	_32_u *cr_slabs;
	_int_u sc;

	sc = mfs_cr.s->slab_c;

	t.slabs = t.cr+(sc*sizeof(_32_u));
	t.crsc = sc;

	printf("mfs_de_init{cr: %u, crnc: %u, root: %u, crsc: %u}\n", t.cr, t.crnc, t.root, t.crsc);
	cr_slabs = (_32_u*)__f_mem_alloc(sc*sizeof(_32_u));

	i = 0;
	for(;i != sc;i++) {
		cr_slabs[i] = (*(mfs_cr.s->slabs+i))->in;
		printf("crsb: %u\n", cr_slabs[i]);
	}

	fsus_write(mfs->m, cr_slabs, sc*sizeof(_32_u), t.cr);
	__f_mem_free(cr_slabs);

	mfs_slabs_save(&t.sc, t.slabs);
	fsus_write(mfs->m, &t, sizeof(struct mfs_tract), 0);
}

#define PAD (pad+PADSP)
#define PADSP 2
void
mfs_tree(struct mfs_node *__n) {
	char static pad[64];
	char static *pd = pad;
	_int_u i;

	i = 0;
	for(;i != PADSP;i++)
		*(pd++) = ' ';

	*pd = '\0';
	_int_u n;
	n = __n->n/sizeof(_32_u);

	printf("%u or %u\n", n, __n->n);
	struct mfs_node *nn;
	i = 0;
	printf("number of nodes: %u.\n", n);
	if (n>20) {
		return;
	}
	for(;i != n;i++) {
		printf("%u ^ %u.\n", n, i);
		nn = mfs_nget(__n->s, i);
		if (nn != NULL) {
		if (nn->mode == MFS_NDIR) {
#ifdef DEBUG
			printf("-----> N: %u, H: %u, SZ: %u, CA: %u\n", nn->n, nn->h, nn->size, nn->ca);
#endif
			printf("%s\e[34m%s\e[0m\n", PAD, nn->name);
			mfs_tree(nn);
		} else {
			/*
				if not directory then file????
			*/
			printf("%s.%s\n", PAD, nn->name);
		}
		} else break;
	}

	pd-=PADSP;
	*pd = '\0';
}

# include "../../fs.h"

static struct ff_fso mfs_op = {
	.open = mfs_open,
	.close = mfs_close,
	.pwrite = mfs_pwrite,
	.pread = mfs_pread,
	.mkdir = mfs_mkdir
};

void ffly_mfs(void) {
	ffly_mfs_init();
	__ff_fso__ = &mfs_op;
}
