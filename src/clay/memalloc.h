# ifndef __ffly__clay__memalloc__h
# define __ffly__clay__memalloc__h
# include "../y_int.h"
void* clay_mem_alloc(_int_u);
void clay_mem_free(void*);
void clay_mem_cleanup(void);
# endif /*__ffly__clay__memalloc__h*/
