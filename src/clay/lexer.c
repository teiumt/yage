# include "lexer.h"
# include "input.h"
# include "memalloc.h"
# include "../string.h"
# include "../ffly_def.h"
# define is_space(__c) (__c == ' '|| __c == '\t' || __c == '\n')
void static
read_str(struct clay_token *tok) {
	char buf[128];
	char *bp;
	char c;

	bp = buf;
	while((c = clay_getc()) != '"') {
		*(bp++) = c;
	}

	*bp = '\0';
	tok->sz = (bp-buf)+1;
	tok->data = clay_mem_alloc(tok->sz);
	mem_cpy(tok->data, buf, tok->sz);
}

void static
read_no(struct clay_token *tok) {
	char buf[128];
	char *bp;
	char c;

	bp = buf;
	c = clay_getc();
	while((c >= '0' && c <= '9') || c == '.') {
		if (c == '.') {
			tok->bits |= 1;
		}
		*(bp++) = c;
		c = clay_getc();
	}
	clay_ugetc(c);

	*bp = '\0';
	tok->sz = (bp-buf)+1;
	tok->data = clay_mem_alloc(tok->sz);
	mem_cpy(tok->data, buf, tok->sz);
}

void static
read_ident(struct clay_token *tok) {
	char buf[128];
	char *bp;
	char c;

	bp = buf;
	c = clay_getc();
	while((c >= 'a' && c <= 'z') || c == '_' || (c>='0'&& c<='9')) {
		*(bp++) = c;
		c = clay_getc();
	}
	clay_ugetc(c);

	*bp = '\0';
	tok->sz = (bp-buf)+1;
	tok->data = clay_mem_alloc(tok->sz);
	mem_cpy(tok->data,buf,tok->sz);
}
static struct clay_token *tokbuf[20];
static struct clay_token **btok = tokbuf;

struct clay_token* clay_nexttok(clayp __clay) {
	if (btok>tokbuf) {
		struct clay_token *t;
		t = *--btok;
		return t;
	}
	struct clay_token *tok = clay_mem_alloc(sizeof(struct clay_token));
	tok->bits = 0;
	tok->val = 0;
	tok->kind = 0;
	char c;
_again:
	if (clay_at_eof())
		return NULL;
	c = clay_getc();

	if (is_space(c))
		goto _again;
	if (c>='0'&& c<='9') {
		clay_ugetc(c);
		read_no(tok);
		tok->kind = _clay_tok_no;
		} else if ((c>='a' && c<='z') || c == '_' || (c>='0'&& c<='9')) {
		clay_ugetc(c);
		read_ident(tok);
		tok->kind = _clay_tok_ident;
	} else {
		switch(c) {
			case '~':
				tok->val = _clay_tilde;
				tok->kind = _clay_tok_keywd;
				break;
			case '"':
				read_str(tok);
				tok->kind = _clay_tok_str;
				break;
			case '{':
				tok->val = _clay_l_brace;
				tok->kind = _clay_tok_keywd;
				break;
			case '}':
				tok->val = _clay_r_brace;
				tok->kind = _clay_tok_keywd;
			break;
			default:
				return NULL;
		}
	}
	return tok;
}
//retract token
void clay_rtok(struct clay_token *tok) {
		if (!tok)return;

	*(btok++) = tok;
}

struct clay_token* clay_peektok(clayp __clay) {
	struct clay_token *tok;
	tok = clay_nexttok(__clay);
	if (!tok)return NULL;
	clay_rtok(tok);
	return tok;
}
