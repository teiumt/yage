# include "../clay.h"
# include "parser.h"
# include "hash.h"
# include "memalloc.h"
# include "../ffly_def.h"
# include "../io.h"
# define solidify(__clay, __n) out[(__n)->type](__clay, __n)

/*
	rename

	meaning???

	pull everything together
*/

void static* out_val(clayp, clay_nodep);
void static* out_trove(clayp, clay_nodep);
static void*(*out[])(clayp, clay_nodep) = {
	out_val,
	out_trove
};

# define tsz(__t) (1<<(((__t)-_clay_8)+3))
static void *in;
static struct clay_hash *hash;
void*
out_val(clayp __clay, clay_nodep __n) {
//	struct clay_val *val;
//	val = in;
//	val->type = __n->val_id;

	if (__n->val_id == _clay_str) {
		*(char const**)in = __n->p;
		printf("string: %s.\n",__n->p);
	} else if (__n->val_id == _clay_float) {
		*(float*)in = *(float*)__n->val;
		printf("floating point value: %f.\n",*(float*)__n->val);
	} else {
		*(_64_u*)in = __n->val[0];
//		printf("int type: %u, %u, %u\n", __n->val_id, __n->val[0], tsz(__n->val_id));
	}
}
#include "../assert.h"
void*
out_trove(clayp __clay, clay_nodep __n) {
	struct clay_trove *t;
	t = (struct clay_trove*)clay_mem_alloc(sizeof(struct clay_trove));
	t->n = 0;
	printf("trove hm-%p, '%s'\n",hash,__n->name);
	clay_hash_put(hash, __n->name, __n->nlen-1, t);
	struct clay_hash *pres;//present
	pres = hash;
	hash = &t->table;
	clay_hash_init(hash);

	clay_nodep n;
	_int_u i = 0;
	while(i != __n->ns) {
		n = __n->sub+i;
		printf("SUBOBJECT-%u\n",n->type);
		in = t->array_vals+i;
		assert(n->type<2);
		solidify(__clay, n);
		i++;
	}
	hash = pres;
}

void clay_solidify(clayp __clay, clay_nodep __top) {
	clay_nodep n;
	n = __top;
	hash = &__clay->table;
	clay_hash_init(hash);
	while(n != NULL) {
		assert(n->type<2);
		solidify(__clay, n);
		n = n->next;
	}
}
