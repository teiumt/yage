# include "parser.h"
# include "lexer.h"
# include "input.h"
# include "../ffly_def.h"
# include "../stdio.h"
# include "memalloc.h"
# include "hash.h"
# include "../system/string.h"

# include "../string.h"
# define new_node ((clay_nodep)clay_mem_alloc(sizeof(struct clay_node)))
static struct clay_token *clay_tok;
void static parser_val(clayp __clay,clay_nodep n) {
	if (clay_tok->kind == _clay_tok_str) {
		n->type = _clay_val;
		n->val_id = _clay_str;
		n->sz = clay_tok->sz;
		n->p = clay_tok->data;
	} else if (clay_tok->kind == _clay_tok_no) {
		n->type = _clay_val;

		if (clay_tok->bits&1) {
			n->val_id = _clay_float;
			*(float*)n->val = ffly_stfloat(clay_tok->data);
			goto _end;
		}else {
		*(_64_u*)n->val = ffly_stno(clay_tok->data);
		}

		n->val_id = _clay_int;
	}else{
		while(1);
	}
_end:
	return;
}
static char depth[32] = {'\0'};
static _int_u dc = 0;
void static
val_or_trove(clayp __clay,clay_nodep n) {
	printf("%sTROVE,\n",depth);
	clay_tok = clay_nexttok(__clay);
	struct clay_token *nametok = clay_tok;
	char const *name;
	name = (char const*)clay_tok->data;
	n->name = name;
	n->nlen = clay_tok->sz;

	clay_tok = clay_nexttok(__clay);
	if (clay_tok->kind == _clay_tok_keywd && clay_tok->val == _clay_l_brace) {
		printf("%sname: %s, %p\n", depth,name,n);
		/*
			we have a trove
		*/
		n->type = _clay_trove;
		n->sub = clay_mem_alloc(sizeof(struct clay_node)*32);
		_int_u i = 0;
		depth[dc] = '\t';
		depth[++dc] = '\0';
	_again:
		if (clay_nexttokis(__clay, _clay_tok_keywd, _clay_r_brace) == -1) {
			val_or_trove(__clay,&n->sub[i++]);	
			goto _again;
		}

		depth[--dc] = '\0';
		n->ns = i;
	}else{
		clay_rtok(clay_tok);
		/*
			i like the interexchaing hands approch as parser level with globl token
		*/
		clay_tok = nametok;
		parser_val(__clay,n);
	}
_end:
	return;
}

clay_nodep clay_parser(clayp __clay) {
	clay_nodep top = NULL, end = NULL, n;
_again:
	clay_tok = clay_peektok(__clay);
	if (!clay_tok) goto _end;
	
	n = new_node;
	n->sub = clay_mem_alloc(sizeof(struct clay_node)*32);
	switch(clay_tok->kind) {
		case _clay_tok_str:
			val_or_trove(__clay,n);
		break;
	}

	if (!top)
		top = n;
	if (end != NULL) {
		end->next = n;
	}
	end = n;
	goto _again;
_end:
	if (end != NULL)
		end->next = NULL;
	return top;
}
