# include "../clay.h"
extern clayp _clay;
_8_u static buf;
_8_i static pres = -1;
void clay_ugetc(_8_u __b) {
	buf = __b;
	pres = 0;
}

_8_u clay_getc(void) {
	if (!pres) {
		pres = -1;
		return buf;
	}

	_8_u rt;
	rt = *(_clay->p+_clay->off);
	_clay->off++;
	return rt;
}
