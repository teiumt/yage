# ifndef __ffly__clay__parser__h
# define __ffly__clay__parser__h
# include "../clay.h"

typedef struct clay_node {
	struct clay_node *next;
	_8_u type;
	char const *name;
	_int_u nlen;

	_8_u val_id;
	_64_u val[8];

	_int_u sz;
	void *p;
	struct clay_node *sub;
	_int_u ns;
} *clay_nodep;

clay_nodep clay_parser(clayp);
# endif /*__ffly__clay__parser__h*/
