/*
	TODO:
		move away from rws structure as its too high level
		as i want to be able to pass other infomation other then controls
*/

#ifndef __y__io__h
#define __y__io__h
#include "y_int.h"
#include "types.h"
#include "system/file.h"
#ifdef __fflib
#include "rws.h"
#include "printf.h"
#else
#include <stdio.h>
#endif
#include "ffly_def.h"
#ifdef __fflib
#define _FFLY_OUT _ffly_out
#define _FFLY_IN _ffly_in
#define _FFLY_LOG _ffly_log
#define _FFLY_ERR _ffly_err
#else
#define _FFLY_OUT ffly_out
#define _FFLY_IN ffly_in
#define _FFLY_LOG ffly_log
#define _FFLY_ERR ffly_err
#endif

struct f_io_common {

};

extern struct f_io_common _f_io_comm;
#ifdef __fflib
extern struct f_rw_struc *ffly_out;
extern struct f_rw_struc *ffly_in;
extern struct f_rw_struc *ffly_log;
extern struct f_rw_struc *ffly_err;
#endif

extern FF_FILE *_FFLY_OUT;
extern FF_FILE *_FFLY_IN;
extern FF_FILE *_FFLY_LOG;
extern  FF_FILE *_FFLY_ERR;
_y_err io_init(void);
void io_closeup(void);
#ifdef __fflib
void putchar(char);
void ppad(char, _int_u);
_int_u ffly_rdline(void*, _int_u, struct f_rw_struc*);
#else
#define f_printf    printf
#define f_fprintf   ffly_fprintf
#define f_vfprintf  ffly_vfprintf
#define f_fprintfs  ffly_fprintfs
void ffly_fprintf(FF_FILE*, char const*, ...);
void printf(char const*, ...);
void ffly_fprintfs(FF_FILE*, char const*, ...);
void ffly_vfprintf(FF_FILE*, char const*, va_list);
#endif
#endif /*__y__io__h*/
