#ifndef __real__tree__h
#define __real__tree__h
#include "y_int.h"
#include "types.h"
struct rtnode{
	union{
		struct rtnode *next[0x100];
		_64_u data[0x100];
	};
};

struct rtnode* rtree_new(void);
void rtree_deinit(struct rtnode*);

_64_u *rtree_at(struct rtnode*,_64_u);

#endif/*__real__tree__h*/
