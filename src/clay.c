# include "clay.h"
# include "clay/input.h"
# include "clay/lexer.h"
# include "clay/memalloc.h"
# include "ffly_def.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
# include "clay/parser.h"
#include "io.h"
void clay_init(clayp __clay) {
	__clay->p = NULL;
	__clay->off = 0;
	__clay->end = 0;
	clay_hash_init(&__clay->table);
}

void clay_de_init(clayp __clay) {
	clay_mem_cleanup();
}

void clay_load(clayp __clay, char const *__file) {
	int fd;

	fd = open(__file, O_RDONLY, 0);

	struct stat st;
	fstat(fd, &st);

	__clay->p = clay_mem_alloc(st.st_size);	
	read(fd, __clay->p, st.st_size);
	__clay->end = st.st_size;
	close (fd);
}

clayp _clay;

_8_u clay_at_eof(void) {
	return (_clay->off>=_clay->end);
}

_8_s clay_nexttokis(clayp __clay, _8_u __type, _8_u __val) {
	struct clay_token *tok;
	tok = clay_nexttok(__clay);
	if (!tok) {
		printf("################ getting null token.\n");	
		return -2;
	}
	if (tok->kind == __type && tok->val == __val) {
		return 0;
	}

	clay_rtok(tok);
	return -1;
}

_8_u clay_reckontok(clayp __clay, _8_u __type, _8_u __val) {
	struct clay_token *tok;
	tok = clay_nexttok(__clay);
	if (!tok)return 0;
	return (tok->kind == __type && tok->val == __val);
}

# include "string.h"
void* clay_get(char const *__key, clayp __clay) {
	return clay_hash_get(&__clay->table, __key, str_len(__key));
}

void* clay_tget(char const *__key, void *__trove) {
	struct clay_trove *t;
	t = (struct clay_trove*)__trove;
	void *r;
	r = clay_hash_get(&t->table, __key, str_len(__key));
	if(!r){
		printf("failed to get '%s' from trove hashmap.\n",__key);
	}
	return r;
}

void *_clay_gettext(void *__val, _int_u __idx) {
	return ((struct clay_trove*)__val)->array_vals+__idx;
}
void* clay_array(void *__val) {
	return ((struct clay_trove*)__val)->array_vals;
}
# include "io.h"
void clay_solidify(clayp, clay_nodep);
void clay_read(clayp __clay) {
	_clay = __clay;

	printf("reading.\n");
	clay_nodep n;
	n = clay_parser(__clay);
	clay_solidify(__clay, n);
}

//# define DEBUG
# ifdef DEBUG
# include "types.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	struct clay clay;
	clay_init(&clay);

	clay_load(&clay, "test.clay");
	clay_read(&clay);

	void *p, *width, *height;
	p = clay_get("screen", &clay);
	width = clay_tget("width", p);
	height = clay_tget("height", p);

	
	printf("screen, width: %u, height: %u\n", clay_16(width), clay_16(height));
	clay_de_init(&clay);
}
# endif
