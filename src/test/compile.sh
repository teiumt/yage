rm -f *.o
gcc -c -std=gnu99 ../pci/device.c
gcc -c -std=gnu99 ../pci/init.c
gcc -c -std=gnu99 ../pci/linux_pci.c
gcc -c -std=gnu99 ../strf.c
gcc -c -std=gnu99 ../system/string.c
gcc -c -std=gnu99 ../amdgpu.c
gcc -c -std=gnu99 ../drm.c
gcc -c -std=gnu99 ../amdgpu/amdgpu_bo.c
gcc -c -std=gnu99 ../amdgpu/amdgpu_dev.c
gcc -c -std=gnu99 ../amdgpu/amdgpu_ctx.c
gcc -c -std=gnu99 ../amdgpu/amdgpu_cs.c
gcc -c -std=gnu99 ../amdgpu/amdgpu_vam.c

gcc -std=gnu99 -L/opt/amdgpu/lib/x86_64-linux-gnu -o main main.c amdgpu_vam.o device.o strf.o init.o linux_pci.o drm.o amdgpu_cs.o amdgpu_ctx.o amdgpu_bo.o amdgpu_dev.o amdgpu.o string.o -ldrm -lm
