# include <sys/stat.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdio.h>
# include <string.h>
# include <malloc.h>
# include <math.h>
# include "../../y_int.h"
_8_u *frame;
#define MUL 80
#define WIDTH (4*MUL)
#define HEIGHT (4*MUL)
#define FSZ (WIDTH*HEIGHT*3)

#define TXW 8
#define TXH 8
#define TX (TXW*TXH)

struct nt_vertex2 {
	_int x, y;
};
#define dot(__x0, __y0, __x1, __y1)\
	((__x0*__x1)+(__y0*__y1))
_8_i
static dim(_int __x, _int __y, struct nt_vertex2 *__a, struct nt_vertex2 *__b, struct nt_vertex2 *__c) {
	struct nt_vertex2 v0 = {
		.x=__b->x-__a->x,
		.y=__b->y-__a->y
	};

	struct nt_vertex2 v1 = {
		.x=__c->x-__a->x,
		.y=__c->y-__a->y
	};

	struct nt_vertex2 v2 = {
		.x=__x-__a->x,
		.y=__y-__a->y
	};

	float a, g, b;
	float d00, d01, d11, d02, d12;
	d00 = dot(v0.x, v0.y, v0.x, v0.y);
	d01 = dot(v0.x, v0.y, v1.x, v1.y);
	d11 = dot(v1.x, v1.y, v1.x, v1.y);
	d02 = dot(v0.x, v0.y, v2.x, v2.y);
	d12 = dot(v1.x, v1.y, v2.x, v2.y);
	float den;
	den = 1./(d00*d11-d01*d01);
//	den = ((v0.x*d00)*2)*v0.x;
//	a = (d11*d02-d01*d12);
//	b = (d12*d00-d02*d01);
//	g = ((1-(a/den))-(b/den));
//	if (a>=0 && b>=0 && g>=0)
//		return 0;

	if ((d02*d11-d12*d01) <= 0)
		return 0;
	if ((d02*d01-d12*d00) >= 0)
		return 0;
	if ((d12*d01)-(d01*d01) >= 0)
		return 0;

//	printf("%f\n", d00-d11);

	a = (d11*d02-d01*d12)*den;
	b = (d00*d12-d01*d02)*den;
	g = (1.-a)-b;

	if (a-b == (d12*d01)-(d01*d01))
		return -1;
	_8_i tes;
	tes = 0;
	if (a>=0) {
		tes |= 1;
		if (b>=0) {
			tes |= 2;
			if (g>=0) {
				tes |= 4;
				return tes;
			}
		//	return tes;
		}
	//	return tes;
	}
//	if (b>=0)
//		return 0;
/*
	float f0, f1, f2, f3, f4, f5;

	f0 = x*(__c->y-__a->y);
	f1 = (__c->x-__a->x)*y;

	f2 = x*(__a->y-__b->y);
	f3 = (__a->x-__b->x)*y;

	f4 = (v1.x-x)*(__c->y-__b->y);
	f5 = (__c->x-__b->x)*(v1.y-y);

	if ((f0-f1)*(f2-f3) >= 0 && (f2-f3)*(f4-f5) >= 0 && (f0-f1)*(f4-f5) >= 0)
		return 0;
*/
	return -1;
}

_8_u *tex;
#define setpix(__dst, __r, __g, __b)\
	*__dst = __r;\
	__dst[1] = __g;\
	__dst[2] = __b;

struct nt_vertex2 v0 = {0, 0};
struct nt_vertex2 v1 = {4*MUL, 2*MUL};
struct nt_vertex2 v2 = {2*MUL, 4*MUL};

void start(void) {
	_int_u x, y;
	y = 0;
	while(y != HEIGHT) {
		x = 0;
		while(x != WIDTH) {
			_8_u *d;
			_8_i res;
			d = frame+((x+(y*WIDTH))*3);
			if ((res = dim(x, y, &v0, &v1, &v2))>=0) {
				setpix(d, (res*0x01)*255, (res>>1&0x01)*255, (res>>2&0x01)*255);
			} else {
				setpix(d, ((x|y)&0x01)?128:255, ((x|y)&0x01)?128:255, ((x|y)&0x01)?128:255);
			}
			x++;
		}
		y++;
	}
}

int main() {
	char buf[128];
	int fd;
	fd = open("tex.data", O_RDONLY);
	struct stat st;
	fstat(fd, &st);
	tex = (_8_u*)malloc(st.st_size);
	read(fd, tex, st.st_size);
	close (fd);
	frame = malloc(FSZ);
	memset(frame, 0, FSZ);
	start();
	fd = open("test.ppm", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	_int_u size;
	size = sprintf(buf, "P6\n%u %u\n255\n", WIDTH, HEIGHT);
	write(fd, buf, size);
	write(fd, frame, FSZ);
	close (fd);
	free(frame);
	free(tex);
}
