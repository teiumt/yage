# include "../drm.h"
# include <stdio.h>
# include <errno.h>
# include <string.h>
# include <sys/ioctl.h>
# include "../amdgpu.h"
# include "../amdgpu_drm.h"
# include <sys/mman.h>
void show_meminfo(int __fd) {
	struct drm_amdgpu_memory_info info;
	bzero(&info, sizeof(struct drm_amdgpu_memory_info));
	if (ffly_amdgpu_memory_info(__fd, &info) == -1) {
		printf("failed to get memory info.\n");
	}

	struct drm_amdgpu_heap_info *heap;

	heap = &info.vram;
	printf("vram:\n");
	printf("\ttotal heap size: %u\n", heap->heap_usage);
	printf("\tusable heap size: %u\n", heap->heap_usage);
	printf("\theap usage: %u\n", heap->heap_usage);
	printf("\tmax allocation: %u\n", heap->heap_usage);

	heap = &info.cpu_accessible_vram;
	printf("cpu accessible vram:\n");
	printf("\ttotal heap size: %u\n", heap->heap_usage);
	printf("\tusable heap size: %u\n", heap->heap_usage);
	printf("\theap usage: %u\n", heap->heap_usage);
	printf("\tmax allocation: %u\n", heap->heap_usage);

	heap = &info.gtt;
	printf("gtt:\n");
	printf("\ttotal heap size: %u\n", heap->heap_usage);
	printf("\tusable heap size: %u\n", heap->heap_usage);
	printf("\theap usage: %u\n", heap->heap_usage);
	printf("\tmax allocation: %u\n", heap->heap_usage);
}

void show_devinfo(int __fd) {
	struct drm_amdgpu_info info_eq;
	struct drm_amdgpu_info_device dev_info;
	info_eq.ret_p = (_64_u)&dev_info;
	info_eq.ret_size = sizeof(struct drm_amdgpu_info_device);
	info_eq.query = AMDGPU_INFO_DEV_INFO;
	int r = drm_cmdreadwrite(__fd, DRM_AMDGPU_INFO, &info_eq, sizeof(struct drm_amdgpu_info));
	if (r == -1) {
		printf("failure.\n");
	}

	printf("virtual_address_offset: %u\n", dev_info.virtual_address_offset);
	printf("virtual_address_max: %u\n", dev_info.virtual_address_max);
	printf("virtual_address_alignment: %u\n", dev_info.virtual_address_alignment);

}
/*
	struct drm_amdgpu_gem_va va;
	memset(&va, 0, sizeof(struct drm_amdgpu_gem_va));
	va.handle = bo->handle;
	va.op = AMDGPU_VA_OP_MAP;
	va.flags = AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE;
	va.bo_offset = 0;
	va.va_addr = AMDGPU_GPU_PAGE_ALIGN(AMDGPU_VA_RESERVED_SIZE+(AMDGPU_GPU_PAGE_SIZE*4));
	va.map_size = AMDGPU_GPU_PAGE_ALIGN(bo->size);

	int r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_GEM_VA, &va, sizeof(struct drm_amdgpu_gem_va));
	if (r == -1) {
		printf("failed to map address, %s\n", strerror(errno));
	}

	show_devinfo(dev->fd);
	struct drm_amdgpu_bo_list_entry list[] = {
		{.bo_handle = bo->handle, .bo_priority = 0}
	};
	union drm_amdgpu_bo_list bolargs;

	memset(&bolargs, 0, sizeof(union drm_amdgpu_bo_list));
	bolargs.in.operation = AMDGPU_BO_LIST_OP_CREATE;
	bolargs.in.bo_number = 1;
	bolargs.in.bo_info_size = sizeof(struct drm_amdgpu_bo_list_entry);
	bolargs.in.bo_info_ptr = (_64_u)list;


	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_BO_LIST, &bolargs, sizeof(union drm_amdgpu_bo_list));
	if (r == -1) {
		printf("failed to create list.\n");
	}

	struct drm_amdgpu_cs_chunk_data chunk_data;
	memset(&chunk_data, 0, sizeof(struct drm_amdgpu_cs_chunk_data));
	chunk_data.ib_data.flags = 0;
	chunk_data.ib_data.va_start = va.va_addr;
	chunk_data.ib_data.ib_bytes = ib_size;
	chunk_data.ib_data.ip_type = AMDGPU_HW_IP_GFX;
	chunk_data.ib_data.ip_instance = 0;
	chunk_data.ib_data.ring = 0;

	struct drm_amdgpu_cs_chunk chunk;
	memset(&chunk, 0, sizeof(struct drm_amdgpu_cs_chunk));
	chunk.chunk_id = AMDGPU_CHUNK_ID_IB;
	chunk.length_dw = sizeof(struct drm_amdgpu_cs_chunk_ib)/4;
	chunk.chunk_data = (_64_u)&chunk_data;
	union drm_amdgpu_cs cs;

	memset(&cs, 0, sizeof(union drm_amdgpu_cs));
	_64_u chunk_array = (_64_u)&chunk;
	cs.in.ctx_id = ctx->ctx_id;
	cs.in.num_chunks = 1;
	cs.in.chunks = (_64_u)&chunk_array;
	cs.in.bo_list_handle = bolargs.out.list_handle;
	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_CS, &cs, sizeof(union drm_amdgpu_cs));
	if (r == -1) {
		printf("cs failure.\n");
	}

	union drm_amdgpu_wait_cs cswait;
	cswait.in.handle = cs.out.handle;
	cswait.in.ctx_id = ctx->ctx_id;
	cswait.in.ip_type = AMDGPU_HW_IP_GFX;
	cswait.in.ip_instance = 0;
	cswait.in.ring = 0;
	cswait.in.timeout = 0;
	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_WAIT_CS, &cswait, sizeof(union drm_amdgpu_wait_cs));
	if (r == -1) {
		printf("wait failure.\n");
	}

	printf("wait output: %u\n", cswait.out.status);

	_64_u list_hand;
	list_hand = bolargs.out.list_handle;
	memset(&bolargs, 0, sizeof(union drm_amdgpu_bo_list));
	bolargs.in.operation = AMDGPU_BO_LIST_OP_DESTROY;
	bolargs.in.list_handle = list_hand;
	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_BO_LIST, &bolargs, sizeof(union drm_amdgpu_bo_list));
	if (r == -1) {
		printf("failed to destory list.\n");
	}

	va.op = AMDGPU_VA_OP_UNMAP;
	va.flags = 0;
	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_GEM_VA, &va, sizeof(struct drm_amdgpu_gem_va));
	if (r == -1) {
		printf("failed to map address, %s\n", strerror(errno));
	}
*/
# include "../pci.h"
#define DRM_DIR "/dev/dri"
#define DRM_FILE "/dev/dri/card0"
# include <sys/stat.h>
# include <unistd.h>
# include <fcntl.h>
#include <sys/sysmacros.h>
#define DRM_MAJOR 226
#define DRM_DEV_DIRMODE     \
    (S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH)
#define DRM_DEV_MODE     (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP)
void static prep(void) {
	dev_t dev;
	struct stat st;
	if (stat(DRM_DIR, &st) == -1) {
		mkdir(DRM_DIR, DRM_DEV_DIRMODE);
		chmod(DRM_DIR, DRM_DEV_DIRMODE);
	}

	if (stat(DRM_FILE, &st) == -1) {
		dev = makedev(DRM_MAJOR, 0);
		mknod(DRM_FILE, S_IFCHR|DRM_DEV_MODE, dev);
	}
}
# include <stdlib.h>
void amdgpu_va_tree(struct amdgpu_vam*);
int main() {
{
/*
	int fd;
	fd = ffly_drm_open("/dev/dri/card0");
	struct amdgpu_dev *dev;
	dev = amdgpu_dev_init(fd);
#define N 20
	struct amdgpu_va *a0[N];
	_int_u i;
_again:
	for(i = 0;i != N;i++) {
		_64_u size;
		size = rand()%20;
		a0[i] = amdgpu_va_alloc(dev->vam, (size+1)*2096, 0);
	}
	amdgpu_va_tree(dev->vam);
	for(i = 0;i != N;i++) {
		amdgpu_va_free(dev->vam, a0[(N-1)-i]);
	}
	usleep(10000);
	goto _again;
*/
/*
	a1 = amdgpu_va_alloc(dev->vam, AMDGPU_GPU_PAGE_SIZE, 0);
	printf("of0: %u, of1: %u.\n", a0->addr, a1->addr);
	struct amdgpu_bo *bo;
	_64_u addr = a0->addr;
	bo = amdgpu_bo_alloc(dev, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_GTT, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	amdgpu_bo_va_op(bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE,
		0, addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));
	amdgpu_bo_va_op(bo, AMDGPU_VA_OP_UNMAP, 0, 0, addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));
*/
//	amdgpu_dev_de_init(dev);
//	close(fd);
}
//	return 0;
//	pci_system_init();
//	struct pci_dev *dev;
//	dev = pci_device_add(0, 0x1f, 0, 0);
//	_pci_sys->funcs->device_probe(dev);
//	pci_device_statechange(dev, PCI_DEV_DISABLE);

	int fd;
	fd = ffly_drm_open("/dev/dri/card0");

	struct amdgpu_info info;
	if (ffly_amdgpu_info(fd, &info) == -1) {
		printf("failed to get info.\n");
	}
	printf("gfx sclk: %u\n", info.gfx_sclk);
	printf("gfx mclk: %u\n", info.gfx_mclk);
	printf("temp: %f-degree\n", ((float)info.temp)*0.001);
	printf("load: %u%%\n", info.load);
	printf("power: %ld-watts\n", info.avg_power);


	show_meminfo(fd);

	struct amdgpu_dev *dev;
	struct amdgpu_bo *bo;
	struct amdgpu_ctx *ctx;
	dev = amdgpu_dev_init(fd);
	ctx = amdgpu_ctx_create(dev, 0);

	_int_u ib_size;

	ib_size = 8;
#define AMDGPU_VA_RESERVED_SIZE		 (1ULL << 20)
#define AMDGPU_GPU_PAGE_SIZE 4096
#define AMDGPU_GPU_PAGE_MASK (AMDGPU_GPU_PAGE_SIZE - 1)
#define AMDGPU_GPU_PAGE_SHIFT 12
#define AMDGPU_GPU_PAGE_ALIGN(a) (((a) + AMDGPU_GPU_PAGE_MASK) & ~AMDGPU_GPU_PAGE_MASK)
	bo = amdgpu_bo_alloc(dev, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_GTT, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);

	amdgpu_bo_cpu_map(bo);

	void *cpu_ptr;
	
	cpu_ptr = bo->cpu_ptr;

//	memset(cpu_ptr, 0xff, ib_size<<2);
	struct drm_amdgpu_bo_list_entry list[1];

	list->bo_handle = bo->handle;
	list->bo_priority = 0;
	union drm_amdgpu_bo_list args;
	memset(&args, 0, sizeof(args));
	args.in.operation = AMDGPU_BO_LIST_OP_CREATE;
	args.in.bo_number = 1;
	args.in.bo_info_size = sizeof(struct drm_amdgpu_bo_list_entry);
	args.in.bo_info_ptr = (_64_u)list;

	int r;
	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_BO_LIST, &args, sizeof(args));
	if (r == -1) {
		printf("failed to create list.\n");
	}
	_32_u *ptr;
	ptr = (_32_u*)cpu_ptr;

//	*ptr = (0x4e0c>>2)|(0<<16);
//	*(ptr+1) = 0;
#define N 32
	_int_u i = 0;
	while(i != N) {
		*(ptr+i) = 0xffff1000;//(0x10<<8)|(3<<30)|(0<<16);
		i++;
	}

//	i = 0;
//	for(;i != N;i++) {
//		printf("%x, %x\n", ptr[i], 0);
//	}
	_64_u addr;
//	addr = 1ULL<<20;
	addr = amdgpu_va_alloc(dev->vam, AMDGPU_GPU_PAGE_SIZE, 0)->addr;//AMDGPU_GPU_PAGE_ALIGN(AMDGPU_VA_RESERVED_SIZE+(AMDGPU_GPU_PAGE_SIZE*4));
	amdgpu_bo_va_op(bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE,
		0, addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));

	_64_u va;
	va = addr+(N<<2);
	ptr[8] = 0x30908>>2;
	ptr[9] = 4;
	ptr[12] = 0x2a<<8|3<<30|1;
	ptr[13] = 0;
	ptr[14] = 0x2f<<8|3<<30|1;
	ptr[15] = 0;
	ptr[16] = 0x27<<8|3<<30|1|4<<16;
	ptr[17] = 0;
	ptr[18] = va&0xffffffff;
	ptr[19] = va>>32;
	ptr[20] = 2;



	struct amdgpu_cs_request req;
	req.flags = 0;
	req.ip_type = AMDGPU_HW_IP_GFX;
	req.ip_inst = 0;
	req.ring = 0;
	req.ib_n = 1;
	req.list_handle = args.out.list_handle;

	struct amdgpu_cs_ib_info ibinfo;
	req.ib = &ibinfo;
	ibinfo.flags = 0;
	ibinfo.addr = addr;
	ibinfo.size = N;//ib_size; 

	_64_u handle = amdgpu_cs_submit(ctx, &req);
	union drm_amdgpu_wait_cs cswait;
	cswait.in.handle = handle;
	cswait.in.ctx_id = ctx->ctx_id;
	cswait.in.ip_type = AMDGPU_HW_IP_GFX;
	cswait.in.ip_instance = 0;
	cswait.in.ring = 0;
	cswait.in.timeout = AMDGPU_TIMEOUT_INFINITE;

	r = drm_cmdreadwrite(dev->fd, DRM_AMDGPU_WAIT_CS, &cswait, sizeof(union drm_amdgpu_wait_cs));
	if (r == -1) {
		printf("wait failure.\n");
	}
	printf("cswait: %u\n", cswait.out.status);

	amdgpu_bo_va_op(bo, AMDGPU_VA_OP_UNMAP, 0, 0, addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));

	_64_u list_handle;
	list_handle = args.out.list_handle;
	memset(&args, 0, sizeof(args));
	args.in.operation = AMDGPU_BO_LIST_OP_DESTROY;
	args.in.list_handle = list_handle;
	drm_cmdreadwrite(dev->fd, DRM_AMDGPU_BO_LIST, &args, sizeof(args));

	amdgpu_bo_cpu_unmap(bo);
	amdgpu_ctx_free(ctx);

	amdgpu_bo_free(bo);
	amdgpu_dev_de_init(dev);

	ffly_drm_close(fd);
}
