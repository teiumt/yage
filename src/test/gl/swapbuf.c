#include<stdio.h>
#include<stdlib.h>
#include<GL/glut.h>
#include<GL/glx.h>
#include<time.h>
int main(int argc, char** argv){
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB|GLUT_DEPTH);

  glutInitWindowPosition(80, 80);
  glutInitWindowSize(940, 940);
  glutCreateWindow("A Simple Triangle");
  glewInit();
	
	int x = 0;
	while(1){
		glClear(GL_COLOR_BUFFER_BIT);
		if(x&1){
			glClearColor(1,0,0,1);
		}else{
			glClearColor(1,0,0,0);
		}

		x++;
		struct timespec ts,te;
		clock_gettime(CLOCK_MONOTONIC,&ts);
		glFlush();
		clock_gettime(CLOCK_MONOTONIC,&te);
		double timetaken;
		timetaken = (te.tv_nsec-ts.tv_nsec)*0.001;
		timetaken+= (te.tv_sec-ts.tv_sec)*1000000;
		printf("timetaken: %lf-us.\n",timetaken);
		usleep(400000);


	}
}
