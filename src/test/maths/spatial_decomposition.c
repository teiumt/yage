#include "../../y_int.h"
#include "../../string.h"
#include "../../io.h"
#include "../../m_alloc.h"
struct point {
	_int_u x,y;
};

struct node {
	_int_u h,w;
	_int_u x,y;
	struct node *map;
};

#define WIDTH	128
#define HEIGHT	64
#define FSIZE	(WIDTH*HEIGHT)

/*
	this is here to help with conceptualizing the idea
*/
static const struct node node_init = {0,0,NULL};
static struct node ntop = node_init;
void static placepoint(struct point *__p) {
	struct node *n = &ntop;
	_int_u iix = WIDTH>>2;
	_int_u iiy = HEIGHT>>2;

	_int_u xc = WIDTH>>1,yc = HEIGHT>>1;
	_int_u x,y;
	x = __p->x;
	y = __p->y;

	_int_u i = 0;
	_int_u xx,yy;
	while(i != 7) {
		xx = (x>>(6-i))&1;
		yy = (y>>(6-i))&1;

		if(!n->map) {
			n->map = m_alloc(sizeof(struct node)*4);
			
			n->map[0] = node_init;
			n->map[1] = node_init;
			n->map[2] = node_init;
			n->map[3] = node_init;
		}

		n->x = xc;
		n->y = yc;
		_int_u dx,dy;
		dx = iix>>i;
		dy = iiy>>i;
		n->w = (iix<<1)>>i;
		n->h = (iiy<<1)>>i;
		if(xx == 0 && yy == 0) {
			xc-=dx;
			yc-=dy;
		}else if (xx == 1 && yy == 0) {
			xc+=dx;
			yc-=dy;
		}else if(xx == 0 && yy == 1) {
			xc-=dx;
			yc+=dy;
		}else{
			xc+=dx;
			yc+=dy;
		}

	//	printf("-- %u: %u.\n",xx,yy);
		n = n->map+xx+(yy<<1);
		i++;
	}

}
static char frame[(WIDTH+1)*HEIGHT];
void static hline(_int_u __from, _int_u __to, _int_u y) {
	_int_u i;
	i = __from;
	for(;i != __to;i++) {
		frame[i+y*(WIDTH+1)] = '-';
	}
}

void static vline(_int_u __from, _int_u __to, _int_u x) {
	_int_u i;
	i = __from;
	for(;i != __to;i++) {
		frame[x+i*(WIDTH+1)] = '|';
	}
}

void static plottree(struct node *n) {
	if(n->w<2 || n->h<2)return;
	hline(n->x-n->w,n->x+n->w,n->y);
	vline(n->y-n->h,n->y+n->h,n->x);
	frame[n->x+n->y*(WIDTH+1)] = '^';
	if(n->map != NULL) {
		plottree(n->map);
		plottree(n->map+1);
		plottree(n->map+2);
		plottree(n->map+3);
	}
}
#include "../../rand.h"
void spatial_decomposition(void) {
	mem_set(frame,'.',(WIDTH+1)*HEIGHT);

#define NPNTS 1
	struct point pnts[NPNTS];
	_int_u i;
	i = 0;
	for(;i != NPNTS;i++) {
		struct point *pnt = pnts+i;
		pnt->x = 64;
		pnt->y = 64;
		placepoint(pnt);
	}
	plottree(&ntop);

	_int_u x,y;
	y = 0;
	for(;y != HEIGHT;y++) {
		frame[WIDTH+y*(WIDTH+1)] = '\n';
	}
	frame[WIDTH+(HEIGHT-1)*(WIDTH+1)] = '\0';
	printf("%s",frame);
}
