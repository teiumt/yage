#include "io.h"
#include "m_alloc.h"
#include "resin.h"
#include "rdm.h"
#include "string.h"

#define MAX 0x38

/*
	redisign < move __p to static
*/
void static
op_exit(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_asb(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("to{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("val{%u}\n", *p);
}

void static
op_asw(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("to{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("val{%u}\n", *(_16_u*)p);
}

void static
op_asd(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("to{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("val{%u}\n", *(_32_u*)p);
}

void static
op_asq(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("to{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("val{%u}\n", *(_64_u*)p);
}

void static
op_jmp(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_st(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("src{%x}\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("dst{%x}\n", *(_f_addr_t*)p);
}

void static
op_ld(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("dst{%x}\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("src{%x}\n", *(_f_addr_t*)p);
}

void static
op_out(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_mov(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("src{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("dst{%x}\n", *(_f_addr_t*)p);
}

void static
op_rin(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("no{%x},\t\t", *(p++));
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_div(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("lhs{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("rhs{%x}\n", *(_f_addr_t*)p);
}

void static
op_mul(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("lhs{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("rhs{%x}\n", *(_f_addr_t*)p);
}

void static
op_sub(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("lhs{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("rhs{%x}\n", *(_f_addr_t*)p);
}

void static
op_add(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("lhs{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("rhs{%x}\n", *(_f_addr_t*)p);
}

void static
op_inc(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_dec(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_cmp(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("lhs{%x},\t\t", *(_f_addr_t*)p);
	p+=sizeof(_f_addr_t);
	printf("rhs{%x}\n", *(_f_addr_t*)p);
}

void static
op_cjmp(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("to{%x}\n", *(_f_addr_t*)p);
}

void static
op_call(_8_u *__p, char const *__ident) {
	_8_u *p = __p;
	printf("%s,\t\t", __ident);
	printf("adr{%x}\n", *(_f_addr_t*)p);
}

void static
op_ret(_8_u *__p, char const *__ident) {
	printf("%s\n", __ident);
}

static char const *ident[] = {
	"exit",
	"asb",
	"asw",
	"asd",
	"asq",
	"jmp",
	"stb",
	"stw",
	"std",
	"stq",
	"ldb",
	"ldw",
	"ldd",
	"ldq",
	"outb",
	"outw",
	"outd",
	"outq",
	"movb",
	"movw",
	"movd",
	"movq",
	"rin",
	"divb",
	"divw",
	"divd",
	"divq",
	"mulb",
	"mulw",
	"muld",
	"mulq",
	"subb",
	"subw",
	"subd",
	"subq",
	"addb",
	"addw",
	"addd",
	"addq",
	"incb",
	"incw",
	"incd",
	"incq",
	"decb",
	"decw",
	"decd",
	"decq",
	"cmpb",
	"cmpw",
	"cmpd",
	"cmpq",
	"je",
	"jne",
	"jg",
	"jl",
	"call",
	"ret"
};

static void(*out[])(_8_u*, char const*) = {
	op_exit,
	// assign
	op_asb,
	op_asw,
	op_asd,
	op_asq,
	op_jmp,
	// store
	op_st,
	op_st,
	op_st,
	op_st,
	// load
	op_ld,
	op_ld,
	op_ld,
	op_ld,
	// out
	op_out,
	op_out,
	op_out,
	op_out,
	// mov
	op_mov,
	op_mov,
	op_mov,
	op_mov,
	op_rin,
	// div
	op_div,
	op_div,
	op_div,
	op_div,
	// mul
	op_mul,
	op_mul,
	op_mul,
	op_mul,
	// sub
	op_sub,
	op_sub,
	op_sub,
	op_sub,
	// add
	op_add,
	op_add,
	op_add,
	op_add,
	// inc
	op_inc,
	op_inc,
	op_inc,
	op_inc,
	// dec
	op_dec,
	op_dec,
	op_dec,
	op_dec,
	// compare
	op_cmp,
	op_cmp,
	op_cmp,
	op_cmp,
	op_cjmp,
	op_cjmp,
	op_cjmp,
	op_cjmp,
	op_call,
	op_ret
};

/*
	TODO:
		__get() by chunks
*/
// used by rdm with file
void rdm(void(*__get)(_int_u, _int_u, void*), _32_u __offset, _32_u __end) {
	_8_u buf[64];

	_32_u cur = __offset;
	_8_u s;
	_int_u i = 0;
	while(cur-__offset < __end) {
		_8_u op;
		__get(cur, 1, &op);	
		if (op>MAX) {
			printf("error malformed opno, got{%u}\n", op);
			break;
		}
		cur++;

		__get(cur, s = ff_resin_ops(op), buf);
		printf("%u-%u:(%x)\t\t", i++, cur-__offset, op);
		out[op](buf, ident[op]);
		cur+=s;
	}
}

// rdm with memory pointer
void rdmp(_8_u *__p, _32_u __end) {
	_8_u *p = __p;
	_8_u *end = p+__end;
	_int_u i = 0;
	while(p != end) {
		_8_u op;
		if ((op = *(p++)) > MAX) {
			printf("error malformed opno, got{%u}\n", op);
			break;
		}
		printf("%u-%u:(%x)\t\t", i++, (p-__p)-1, op);
		out[op](p, ident[op]);
		p+=ff_resin_ops(op);
	}
}

# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
int static fd;
void static
get(_int_u __from, _int_u __size, void *__buf) {
	pread(fd, __buf, __size, __from);
}

void rdmf(char const *__file, _32_u __offset, _int_u __size) {
	if ((fd = open(__file, O_RDONLY, 0)) < 0) {
		f_fprintf(ffly_out, "failed to open file\n");
		return;
	}

	rdm(get, __offset, __size);
	close(fd);
}
