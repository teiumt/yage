#include <linux/fb.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/ioctl.h>
#include "y_int.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <malloc.h>
#include <sys/stat.h>
#define WIDTH 512
#define HEIGHT 512
#define dot(__x0, __x1, __y0, __y1)\
	((__x0*__y1)+(__x1*__y0))
int main() {
	void *impix;
	int fd, image;
	image = open("test.data", O_RDWR, 0);
	if (image<0) {

		return -1;
	}
	impix = malloc(WIDTH*HEIGHT*4);
	read(image,impix,WIDTH*HEIGHT*4);
	close(image);

	fd = open("/dev/fb0", O_RDWR, 0);
	if (fd<0) {
		printf("failed to open.\n");
		return -1;
	}
	_int_u fb_len;
	void *fb_pixels;
	struct fb_var_screeninfo fb_info;
	struct fb_fix_screeninfo fb_fix;

	ioctl(fd, FBIOGET_VSCREENINFO, &fb_info);
	ioctl(fd, FBIOGET_FSCREENINFO, &fb_fix);

	fb_len = fb_info.yres*fb_fix.line_length;
	fb_pixels = mmap(NULL, fb_len, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

	struct vertex { 
		double x,y;
	};


	struct vertex v0 = {0,0};
	struct vertex v1 = {256,0};
	struct vertex v2 = {0,256};
	struct vertex v3 = {512,512};
	while(v3.y > 20) {
	double i,j;
	_int_u x,y;
	y = 0;
	for(;y != HEIGHT;y++) {
		x = 0;
		j = (1./HEIGHT)*(double)y;
		for(;x != WIDTH;x++) {
			i = (1./WIDTH)*(double)x;
			_8_u *d = ((_8_u*)fb_pixels)+(x*4)+(y*fb_fix.line_length);
			*(_32_u*)d = 0;
			double _i = i*WIDTH,_j = j*HEIGHT;
			double k,m;
			k = _i+_j;
			
		
			if ((k-(256+256))<0) {
				d[0] = 255;
			}else {
				d[0] = 0;
			}
		}
	}
		v3.y-=20;
		printf("LOOK.\n");
		usleep(500000);
	}
	munmap(fb_pixels,fb_len);
	close(fd);
	free(impix);
}
