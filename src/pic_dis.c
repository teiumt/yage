#include "y_int.h"
#include "io.h"
struct in_strc {
	char const *name;
	_64_u valm;

};
#define _ {NULL,0}
struct in_strc in_name[0x100] = {
	{"NOP"},_,{"MOV-WF",0x7f},_,{"MOV-LW",0xff},{"MOV-LB",0x3f},_,_,{"SUB-WF",0x7f},_,_,_,{"DEC-F"},_,{"IOR-LW",0xff},_,//0
	{"IOR-WF",0x7f},_,_,_,{"AND-WF",0x7f},_,_,_,_,_,_,_,{"ADD-WF",0x7f},_,_,_,//1
	{"MOV-F",0x7f},_,_,_,_,_,_,_,{"INC-F"},_,_,_,_,_,_,_,//2
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//3
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//4
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//5
	{"BTFSIC"},_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//6
	{"BTFSIS"},_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//7
	{"CALL",0x7ff},_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//8
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//9
	{"GOTO",0x7ff},_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//10
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,//11
	{"MOV-LW",0xff},_,_,_,_,_,{"MOV-LP",0},_,_,_,_,_,_,_,_,_,//12
	_,_,_,_,{"LSL"},_,_,_,{"LSR"},_,_,_,_,_,_,_,//13
	_,_,_,_,{"AND-LW",0xff},_,_,_,_,_,_,_,{"SUB-FWB"},_,_,_,//14
	{"SUB-LW",0xff},_,_,_,{"ADD-WFC"},_,_,_,{"ADD-LW",0xff},_,_,_,_,_,_,_//15
};

struct in_strc static* find(_16_u __op) {
	struct in_strc *is;
	_int_u i;
	_64_u v = __op>>6;
	i = 0;
	//due to the nature of how opcodes are formed we can just do this
	//as theres restrictions
	for(;i != 8;i++) {
		is = in_name+(v&(0xff<<i));
		if (is->name != NULL)
			return is;
	}
	return NULL;
}

void m_pic_dis(_8_u *__buf, _int_u __n) {
	_64_u buf = 0;
	_int_u n = 64;
	_int_u i = 0;
	_int_u cnt = 0;
	for(;i != __n;i++) {
		buf = buf>>8|(_64_u)__buf[i]<<(64-8);
		n-=8;

		if (n<=(64-14)) {
			_16_u word = (buf>>n)&0x3fff;
			struct in_strc *is;
			is = find(word);
			if (is->name != NULL) {
				printf("%x# %s -%x, %x\n", cnt, is->name, (buf>>n)&is->valm, word);
			} else
				printf("UNKNOWN, %x\n", word);
			cnt++;
			n+=14;
		}
	}
}

