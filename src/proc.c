# include "proc.h"
# include "system/tls.h"
# include "io.h"
# include "m_alloc.h"
# include "string.h"
static _int_u ctx_tls;
void proc_init(void) {
}

#define PCT_MAG0 'C'
#define PCT_MAG1 'T'
#define PCT_MAG2 'X'

#define CTX(__name)\
	struct proc_context *__name;\
	__name = &pctx;
#define EH_DEPTH 4096

#define MAGIC\
	ctx->ident[0] = PCT_MAG0;\
	ctx->ident[1] = PCT_MAG1;\
	ctx->ident[2] = PCT_MAG2;

void proc_ctx_setup(void) {
	CTX(ctx)
	MAGIC

	mem_set(ctx->den, 0, PROC_DEN_MAX*sizeof(void*));
	ctx->rv = &ctx;
//	ctx->e_heap = f_eh_new(EH_DEPTH);
}

void proc_ctx_cleanup(void) {
	CTX(ctx)
	if (ctx->ident[0] != PCT_MAG0 || ctx->ident[1] != PCT_MAG1 || ctx->ident[2] != PCT_MAG2) {
//		printf("error, '%c', '%c', '%c'\n", ctx->ident[0], ctx->ident[1], ctx->ident[2]);
		while(1);
		return;
	}
//	f_eh_destroy(ctx->e_heap);

}
__thread struct proc_context pctx;
