# include "dus.h"
# include "../m_alloc.h"
# include "../io.h"
_int_u static used = 0;
/*
	should rename to area ???
*/
struct block {
	struct block **bk, *next;
	void *p;
	_int_u size;
};

static struct block *top = NULL;

# define hdrsize sizeof(struct block)
/*
	temporary
*/
void *dus_mmap(_int_u __size) {
	_8_u *p = m_alloc(hdrsize+__size);
	struct block *b = (struct block*)p;
	if (top != NULL)
		top->bk = &b->next;

	b->next = top;
	b->bk = &top;
	top = b;
	b->size = __size;
	used+=__size;
	return (b->p = (void*)(p+hdrsize));
}

void dus_munmap(void *__p) {
	struct block *b = (struct block*)((_8_u*)__p-hdrsize);
	if (b->next != NULL)
		b->next->bk = b->bk;
	*b->bk = b->next;
	used-=b->size;
	m_free(b);
}

void dus_mm_cleanup(void) {
	struct block *cur = top, *bk;
	printf("memory used: %u\n", used);
	while(cur != NULL) {
		bk = cur;
		cur = cur->next;
		dus_munmap(bk->p);
	}
}
