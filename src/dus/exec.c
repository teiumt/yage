# include "dus.h"
# include "../ffly_def.h"
# include "../io.h"
# include "../string.h"
# include "../m_alloc.h"
# include "../mman.h"
# include "../linux/sched.h"
# include "../unistd.h"
# include "../stat.h"
# include "../types.h"
# include "../linux/signal.h"
# include "../linux/wait.h"
# include "../system/errno.h"
# include "../env.h"
#include "../system/errno.h"
#include "../strf.h"
static struct f_lhash sy;
static dus_valuep reg[8];
static char const *opstr(_8_u __op) {
	switch(__op) {
		case _dus_op_copy:		return "copy";
		case _dus_op_assign:	return "assign";
		case _dus_op_push:		return "push";
		case _dus_op_pop:		return "pop";
		case _dus_op_fresh:		return "fresh";
		case _dus_op_free:		return "free";
		case _dus_op_out:		return "out";
	}
	return "unknown";
}

dus_objp static objs[20];
dus_objp static* top = objs;

void static
dupmapping(dus_valuep d,dus_valuep s){
	if(d->p != NULL){
		dus_munmap(d->p);
	}
	d->p = dus_mmap(s->size);
	d->size = s->size;
	mem_cpy(d->p,s->p,s->size);
}

void static
op_copy(dus_objp __obj) {
	_8_u *src = __obj->src->p;
	_8_u *dst = __obj->dst->p;
	mem_cpy(dst, src, __obj->len);
}

void static
op_fresh(dus_objp __obj) {
	printf("FRESH: %u.\n",__obj->size);
	__obj->to->size = __obj->size;
	__obj->to->p = dus_mmap(__obj->size);
}

void static
op_free(dus_objp __obj) {
	dus_munmap(__obj->p);
}

void static
op_push(dus_objp __obj) {
	*(top++) = __obj->p;
}

void static
op_pop(dus_objp __obj) {
	__obj->p = *(--top);
}

void static
op_assign(dus_objp __obj) {
	dus_valuep src;
	dus_valuep to;
	to = reg[__obj->reg[0]];
	src = reg[__obj->reg[1]];
	_8_u *dst;
	dst = to->p;
	mem_cpy(dst, src->p, src->len);
	to->type = src->type;
}

void static
op_out(dus_objp __obj) {
	dus_valuep src;
	src = reg[__obj->reg[0]];
	char *p = (char*)src->p;
	if (src->type == _dusc_str) {
		printf("!!! %s : %u for : %p\n", p,src->size,src);
		dus_printf("%s",p);
	} else {
		dus_printf("%u",src->in);
	}
}
#include "../assert.h"
// construct a string
void static
op_cas(dus_objp __obj) {
	dus_valuep dst,src;
	dst = reg[__obj->reg[0]];
	src = reg[__obj->reg[1]];
	printf("CAS: src: %u\n",src->size);
	dst->p = dus_mmap(1048);
	char *p = src->p;
	_8_u *dp = dst->p;
	char buf[128];
	char *bufp;
_again:
	if (*p == '$') {
		p++;
		bufp = buf;
		while(*p != ' ' && *p != '\0' && ((*p >= 'a' && *p <= 'z') || *p == '_'))
			*(bufp++) = *(p++);
		*bufp = '\0';
		printf("CAS_LOOKUP for symbol: %s\n",buf);
		dus_valuep v;
		v = (dus_valuep)f_lhash_get(&sy, buf, bufp-buf);
		if (!v) {
			printf("error, %s\n", buf);
		} else {
			printf("CAS_TYPE: %u.\n",v->type);
			if (v->type == _dusc_str) {
				printf("CAS_STRING.\n");
				printf("pointer : %p, '%w'\n", v->p,buf,bufp-buf);
				mem_cpy(dp, v->p, v->size);
				dp+=v->size-1;
			}else{
				printf("CAS_NUMBER.\n");
				dp+=_ffly_nds(dp,v->in);
			}
		}
	
	} else {
		while(*p != '$' && *p != '\0')
			*(dp++) = *(p++);
	}

	if (*p != '\0') {
		goto _again;
	}

	*dp = '\0';
	printf("CAS: %s.\n",dst->p);
	dst->size = (dp-(_8_u*)dst->p)+1;
	dst->type = _dusc_str;
}

void static
op_syput(dus_objp __obj) {
	dus_valuep v;
	v = reg[__obj->reg[0]];
	f_lhash_put(&sy, __obj->name,__obj->len,v);
	printf("put symbol, %w, %u\n", __obj->name,__obj->len,v->type);
}
extern dus_valuep dus_results;
extern dus_valuep dus_exit_code;
#include "../strf.h"
# include "../system/nanosleep.h"
void static
op_shell(dus_objp __obj) {
	dus_valuep env,path;
	env = reg[__obj->reg[0]];
	path = reg[__obj->reg[1]];
	char *p = path->p;
	char const *argv[100];
	char const **cur = argv;
	char *bed;
	_int_u l;

	char base[128];
	char *bp = base;
	while(*p != ' ' && *p != '\0')
		*(bp++) = *(p++);
	*bp = '\0';
	
	while(*p == ' ') p++;
	*(cur++) = base;

	printf("base: %s\n", base);
_again:
	bed = p;
	while(*p != ' ' && *p != '\0')
		p++;

	*cur = m_alloc((l = (p-bed))+1);
	mem_cpy(*cur, bed, l);
	*(char*)((*cur)+l) = '\0';
//	printf("--> %s\n", *cur);
	cur++;

	if (*p != '\0') {
		while(*p == ' ') p++;
		goto _again;
	}

	*cur = NULL;
	char envbuf[256];

	char const *envp[16] = {
		"PATH=/usr/bin",
	};

	_int_u eidx = 1;
	struct f_lhash_entry *ee;
	ee = env->hm->top;
	_int_u i;
	i = 0;
	
	while(ee != NULL){
		dus_valuep val;
		val = ee->p;
		char *bp;
		bp = envbuf+i;
		envp[eidx++] = bp;
		mem_cpy(bp,ee->key,ee->len);
		bp+=ee->len;
		*bp++ = '=';
//		printf("####: %s.\n",val->p);
		mem_cpy(bp,val->p,val->size);
		bp+=val->size-1;
		*bp = '\0';
		i+=2+ee->len+val->size-1;
		ee = ee->fd;
	}
	
	envp[eidx] = NULL;

	i = 0;
	for(;envp[i] != NULL;i++){
		printf("ENVP-%u{%s}.\n",i,envp[i]);
	}


	ffly_fdrain(_ffly_out);
	int fd;
	__linux_pid_t pid;
	pid = fork();
	if (pid == 0) {
		fd = open("stdout", O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
		dup2(fd,1);
		close(fd);
		_int_u i;
		i = 0;
		while(i != 0xffffff) {
			i++;
		}
		if (execve(base, argv, (char*const*)envp) == -1) {
			// failure
		}
		exit(0);
	}
	int status_code = 0;
	int ret;
	ret = wait4(pid, &status_code, __WALL, NULL);
	if (ret == -1) {
		printf("ERROR: %s.\n",strerror(errno));
	}

	fd = open("stdout", O_RDONLY, S_IRUSR|S_IWUSR);
	struct stat st;
	fstat(fd,&st);
	printf("reading results: %u.\n",st.st_size);
	if(dus_results->p != NULL){
		dus_munmap(dus_results->p);
	}

	dus_results->p = dus_mmap(dus_results->size = st.st_size+1);
	if(st.st_size>0){
		read(fd,dus_results->p,st.st_size);
		((char*)dus_results->p)[st.st_size] = '\0';
	}else{
	((char*)dus_results->p)[0] = '\0';
	}

	printf("RESULTS: {%s}.\n",dus_results->p);
	close(fd);
	_8_s exit_code = status_code>>8;
	if (ret == -1)
		exit_code = -1;
	printf("EXIT_CODE: %d, r: %d, %u\n",exit_code,ret,status_code&0xff);
	dus_exit_code->in = exit_code;
	cur = argv+1;
	while(*cur != NULL) {
		m_free(*cur);
		cur++;
	}
}

void static
op_set(dus_objp __obj) {
}

static void(*op[40])(dus_objp);
void static _exec(dus_objp __top) {
	dus_objp cur = __top;
	while(cur != NULL) {
		op[cur->op](cur);
//		printf("op: %s\n", opstr(cur->op));
		cur = cur->next;
	}
}

static _8_u status_reg;

void static
op_bo(dus_objp __obj) {
	dus_valuep l,r;
	l = reg[__obj->reg[0]];
	r = reg[__obj->reg[1]];
	status_reg = 0;
	if (l->type == _dusc_int) {
		printf("comparing %d, %d.\n",l->in,r->in);
		if (l->in == r->in)
			status_reg = 1;
	}else {
	printf("comparing: %u ~ %u, '%w' '%w'\n",l->size,r->size,l->p,l->size,r->p,r->size);
	if (l->size == r->size) {
		if (!mem_cmp(l->p,r->p,r->size)) {
			status_reg = 1;
		}
	}
	}
	if (__obj->op == _dus_bo_noteq) {
		status_reg = status_reg^1;
	}
}


void static
op_if(dus_objp __obj) {
	op_bo(__obj->cond);
	if (status_reg&1) {
		_exec(__obj->_if);
	}else{
		_exec(__obj->_else);
	}
}

void static
op_while(dus_objp __obj) {
_again:
	op_bo(__obj->cond);
	if (status_reg&1) {
		_exec(__obj->_if);
		goto _again;
	}
}

void static
op_call(dus_objp __obj) {
	_exec(__obj->func);
}

void static
op_add(dus_objp __obj) {
	dus_objp l = __obj->l;
	dus_objp r = __obj->r;
	__obj->in = l->in+r->in;
	printf("ADD: %u, %u = %u\n",l->in,r->in,__obj->in);
	__obj->type = _dusc_int;
}


void static
op_sub(dus_objp __obj) {
	dus_objp l = __obj->l;
	dus_objp r = __obj->r;
	
	__obj->in = l->in-r->in;
	__obj->type = _dusc_int;
}

void static
op_mov(dus_objp __obj) {
	dus_valuep dst,src;
	dst = reg[__obj->reg[0]];
	src = reg[__obj->reg[1]];
	dst->type = src->type;
	if(src->type == _dusc_int){
		dst->in = src->in;
	}else{
		dupmapping(dst,src);		
	}
	printf("OP_MOV: DST-%u{%u}, SRC-%u{%u}.\n",__obj->reg[0],dst->size,__obj->reg[1],src->size);
}

dus_valuep static arrelem(dus_valuep idx,dus_valuep arr){
	dus_valuep p;
	p = f_lhash_get(arr->hm,idx->p,idx->size-1);
	if(!p){
		p = m_alloc(sizeof(struct dus_value));
		f_lhash_put(arr->hm,idx->p,idx->size-1,p);
		p->p = NULL;
	}
	return p;
}
void static
op_deref(dus_objp __obj) {
	printf("OP_DEREF.\n");
	dus_valuep idx,arr;
	idx = reg[__obj->reg[0]];
	arr = reg[__obj->reg[1]];

	if(idx->type == _dusc_int){
	}else{
		reg[__obj->reg[2]] = arrelem(idx,arr);
	}
}

void static
op_load(dus_objp __obj){
	printf("OP_LOAD: location-%u with %p.\n",__obj->reg[0],__obj->val);
	reg[__obj->reg[0]] = __obj->val;
}

void static
op_movreg(dus_objp __obj){
	reg[__obj->reg[0]] = reg[__obj->reg[1]];
}
#include "../lib.h"
static char read_schar(char c){
	switch(c){
		case 'n':
			return '\n';
		case '0':
			return '\0';
	}
}

void static
cmd_rm(dus_valuep d,char *__args){
	char *s;
	s = d->p;
	s = s+d->size-2;
	char f;
	f = read_schar(__args[0]);
_again:
	if(*s == f){
		*s = '\0';
		s--;
		d->size--;
		goto _again;
	}
}

void static
cmd_pluck(dus_valuep d,char *__args){
	char e;
	e = __args[0];
	char *p;
	p = d->p;
	char *s;
	s = d->p;
	while(*s != e) s++;
	s++;
	_int_u i;
	i = 0;
	char c;
	while((c = *s++) != e){
		p[i++] = c;
	}
	p[i] = '\0';
	d->size = i+1;
}

void static
op_tr(dus_objp __obj){
	dus_valuep d,cmd;
	d = reg[__obj->reg[0]];
	cmd = reg[__obj->reg[1]];

	char *s;
	s = cmd->p;
	_int_u i;
	i = strchrl(s,' ')+1;
	if(!mem_cmp(s,"rm",2)){
		cmd_rm(d,s+i);
	}else
	if(!mem_cmp(s,"pluck",5)){
		cmd_pluck(d,s+i);
	}
}

static void(*op[40])(dus_objp) = {	
	op_copy,
	op_assign,
	op_push,
	op_pop,
	op_fresh,
	op_free,
	op_out,
	op_cas,
	op_syput,
	op_shell,
	op_set,
	op_if,
	op_call,
	op_add,
	op_sub,
	op_mov,
	op_deref,
	op_while,
	op_load,
	op_movreg,
	op_tr
};


void dus_run(dus_objp __top) {
	f_lhash_init(&sy);
	_exec(__top);
	f_lhash_destroy(&sy);
}
