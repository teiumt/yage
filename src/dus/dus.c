# include "dus.h"
# include "../string.h"
# include "../ffly_def.h"
# include "../m_alloc.h"
# include "../io.h"
#include "../rws.h"
#include "../ioc_struc.h"
#include "../system/file.h"
struct f_lhash dus_env;

void dus_init(void);
struct dus_clus_s dus_clus = {
	.init = dus_init
};
struct ffly_file *dus_out;
void *dusout;
extern struct f_rw_funcs rws_func;
void dus_init(void) {
	f_lhash_init(&dus_env);
	_8_s err;
    if (!(dus_out = f_fopen("duslog",
		FF_O_WRONLY|FF_O_TRUNC|FF_O_CREAT,
		FF_S_IRUSR|FF_S_IWUSR, &err))) {
	}
	ffly_fopt(dus_out, FF_STREAM);
	static struct f_ioc_struc ioc_out;

	ioc_out.rws.funcs = &rws_func;
	ioc_out.rws.arg = (_ulonglong)dus_out;
	dus_out->rws = &ioc_out.rws;
	dusout = &ioc_out;
}

_8_s
dus_nexttokis(_8_u __sort, _8_u __val) {
	dus_tokenp tok;
	_8_u res;
	if (!(tok = dus_nexttok()))
		return -1;

	res = (tok->sort == __sort && tok->val == __val);
	if (!res) {
		dus_ulex(tok);
		return -1;
	}
	return 0;
}

_8_s
dus_expect_token(_8_u __sort, _8_u __val) {
	dus_tokenp tok;
	_8_u res;
	if (!(tok = dus_nexttok()))
		return -1;
	res = (tok->sort == __sort && tok->val == __val);
	if (!res) {
		printf("expected %u:%u got: %u:%u.\n",__sort,__val,tok->sort,tok->val);
		return -1;
	}
	return 0;
}

#define to_keyword(__tok, __val) \
	__tok->sort = _dus_tok_keywd;\
	__tok->val = __val

dus_tokenp
dus_nexttok(void) {
	dus_tokenp tok;
	if (!(tok = dus_lex())) return NULL;
	if (tok->sort == _dus_tok_ident) {
		/*
			NOTE:
				dont remove {} from if statments as 
				'to_keyword' is a macro
		*/
		if (!str_cmp(tok->p, "out"))
		{//print somthing
			to_keyword(tok, _dus_keywd_out);
		} else if (!str_cmp(tok->p, "syt"))
		{//set symbol
			to_keyword(tok, _dus_keywd_syput);
		} else if (!str_cmp(tok->p, "sh"))
		{//shell exec
			to_keyword(tok, _dus_keywd_shell);
		} else if (!str_cmp(tok->p, "set"))
		{//set symbol?
			to_keyword(tok, _dus_keywd_set);
		}else
		if(!str_cmp(tok->p,"if"))
		{
			to_keyword(tok, _dus_keywd_if);
		}else
		if(!str_cmp(tok->p,"else"))
		{ 
			to_keyword(tok, _dus_keywd_else);
		}else
		if(!str_cmp(tok->p,"while"))
		{
			to_keyword(tok, _dus_keywd_while);
		}else
		if(!str_cmp(tok->p,"tr")){
			to_keyword(tok,_dus_keywd_tr);
		}
	}
	return tok;
}

/*
	FOR NOW! may not exist in the future
*/
static void *tf[2048];
static void **fresh = tf;
void dus_to_free(void *__p) {
	if (fresh-tf >= 2048) {
		printf("error overflow.\n");
	}
	*(fresh++) = __p;
}

dus_nodep dus_node_alloc(void) {
	dus_nodep ret;
	ret = (dus_nodep)m_alloc(sizeof(struct dus_node));
	dus_to_free(ret);
	return ret;
}

dus_objp dus_obj_alloc(void) {
	dus_objp ret;
	ret = (dus_objp)m_alloc(sizeof(struct dus_obj));
	dus_to_free(ret);
	return ret;
}

void dus_cleanup(void) {
	ffly_fclose(dus_out);
	
	f_lhash_destroy(&dus_env);
	void **cur = tf;
	while(cur != fresh)
		m_free(*(cur++));
	dus_lexer_cleanup();
	dus_mm_cleanup();
}
