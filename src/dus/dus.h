# ifndef __ffly__dus__h
# define __ffly__dus__h
# include "../y_int.h"
#include "../lib/hash.h"
# define new_node dus_node_alloc()
# define incrp dus_p++

extern _8_u *dus_p;
void dus_init(void);
void dus_cleanup(void);
_8_u dus_at_eof(void);

/*
	cluster of shit
	cluster structure
*/
struct dus_clus_s {
	void(*init)(void);
};
#define dus_printf(...) ffly_fprintf(dusout,__VA_ARGS__)
extern struct ffly_file *dus_out;

extern struct dus_clus_s dus_clus;
extern void *dusout;
typedef struct dus_value{
	void *p;
	_int_u size;
	_8_u type;
	_int_u len;
	_64_s in;
	struct f_lhash *hm;
} *dus_valuep;

typedef struct dus_obj {
	struct dus_obj *next;
	_8_u op;
	_32_u reg[8];
	_8_u type;
	dus_valuep val;
	_int_u size, len;
	// change to void
	struct f_lhash *hm;
	struct dus_obj *idx;

	struct dus_obj *to, *dst, *src, *l, *r;
	struct dus_obj *objpp;
	struct dus_obj *_if,*_else, *func, *cond;
	char *name;
	void *p;

	union {
	struct dus_obj *params[8], *args[8];
	};
	union{
	_int_u npar,narg;
	};
	struct dus_obj **arr;
	_64_s in;
} *dus_objp;
#define _dusc_int	0
#define _dusc_str	1
typedef struct dus_token {
	struct dus_token *next;
	_8_u sort, val;
	void *p;
	_int_u l;
	_8_s neg;
} *dus_tokenp;

struct dus_ndbean {
	struct dus_node *l, *r;
};

typedef struct dus_node {
	struct dus_node *next, *fd;
	_64_u intval;
	_8_u subop;
	void *p;
	struct dus_node *idx;
	_int_u len;
	_8_u kind;
	struct dus_node *cmd;
	struct dus_node *main, *_else, *cond;
	struct dus_node *var, *init;
	struct dus_node *l, *r;
	struct dus_node *src, *func;
	struct dus_node *_env;
	union {
		struct dus_node *params[8], *args[8];
	};
	union {
		_int_u npar, narg;
	};
	
	char *name;
	dus_objp _obj;
	dus_valuep val;
	struct dus_ndbean beans[24];
	_32_u loc;
	_int_u n_beans;
	_int_u arrlen;
	struct dus_node **array;
	struct dus_node *arr;
} *dus_nodep;
/*
	string fragment

	% used for args

	% = new fragment
	hello%test

*/
struct dus_frag {
	struct dus_frag *next;
	void *p;
	_int_u len;
};

extern struct f_lhash dus_env;

dus_tokenp dus_nexttok(void);

_8_s dus_nexttokis(_8_u, _8_u);
_8_s dus_expect_token(_8_u, _8_u);
void dus_lexer_cleanup(void);
dus_tokenp dus_peektok(void);
void dus_parse(dus_nodep*);
dus_objp dus_gen(dus_nodep);
void dus_run(dus_objp);

dus_nodep dus_exp(void);
void dus_ulex(dus_tokenp);
dus_tokenp dus_lex(void);

enum {
	_dus_op_copy,
	_dus_op_assign,
	_dus_op_push,
	_dus_op_pop,
	_dus_op_fresh,
	_dus_op_free,
	_dus_op_out,
	_dus_op_cas,
	_dus_op_syput,
	_dus_op_shell,
	_dus_op_set,
	_dus_op_if,
	_dus_op_call,
	_dus_op_add,
	_dus_op_sub,
	_dus_op_mov,
	_dus_op_deref,
	_dus_op_while,
	_dus_op_load,
	_dus_op_movreg,
	_dus_op_tr
};

enum {
	_dus_bo_equal,
	_dus_bo_noteq
};

enum {
	_dus_decl,
	_dus_var,
	_dus_str,
	_dus_int,
	_dus_assign,
	_dus_out,
	_dus_cas,
	_dus_syput,
	_dus_shell,
	_dus_set,
	_dus_if,
	_dus_func,
	_dus_func_call,
	_dus_array,
	_dus_deref,
	_dus_bo,
	_dus_while,
	_dus_tr
};

enum {
	_dus_grave,
	_dus_comma,
	_dus_eq,
	_dus_dollar,
	_dus_keywd_out,
	_dus_keywd_syput,
	_dus_keywd_shell,
	_dus_keywd_set,
	_dus_keywd_if,
	_dus_keywd_while,
	_dus_keywd_else,
	_dus_keywd_tr,
	_dus_l_bracket,
	_dus_r_bracket,
	_dus_l_brace,
	_dus_r_brace,
	_dus_eqeq,
	_dus_nteq,
	_dus_keywd_apos,
	_dus_l_paren,
	_dus_r_paren,
	_dus_plus,
	_dus_minus
};

/*
	TODO:
		add bits field to token 

		bits |= COMPOUND_TERMINATION = } or else or etc
*/
enum {
	_dus_tok_ident,
	_dus_tok_keywd,
	_dus_tok_str,
	_dus_tok_dec
};

dus_nodep dus_cas(void);
dus_tokenp dus_peektok(void);
//mm.c
void* dus_mmap(_int_u);
void dus_munmap(void*);
void dus_mm_cleanup(void);

dus_nodep dus_node_alloc(void);
dus_objp dus_obj_alloc(void);
void dus_to_free(void*);
# endif /*__ffly__dus__h*/
