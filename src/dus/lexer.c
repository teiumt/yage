# include "dus.h"
# include "../m_alloc.h"
# include "../io.h"
# include "../string.h"
#define nextc (*dus_p)
#define nextcadvance (*(dus_p+1))
_8_u is_space(char __c) {
	return __c == ' ' || __c == '\n' || __c == '\t';
}

void static
sk_white_space(void) {
	while(is_space(nextc)) incrp;
}

char static*
read_ident(_int_u *__l) {
	char buf[128];
	char *bufp = buf;
	char c = nextc;
	while((c>='a' && c<='z') || c == '_') {
		if ((bufp-buf)>=128) {
			// buffer overflow
		}
		*(bufp++) = c;
		incrp;
		c = nextc;
	}
	
	*bufp = '\0';
	_int_u l;
	char *p = (char*)m_alloc((l = (bufp-buf))+1);
	dus_to_free(p);
	mem_cpy(p, buf, l+1);
	*__l = l;
	return p;
}

char static*
read_dec(_int_u *__l) {
	char buf[128];
	char *bufp = buf;
	char c = nextc;
	while(c>='0' && c<='9') {
		if ((bufp-buf)>=128) {
			// buffer overflow
		}
		*(bufp++) = c;
		incrp;
		c = nextc;
	}

	*bufp = '\0';
	_int_u l;
	char *p = (char*)m_alloc((l = (bufp-buf))+1);
	dus_to_free(p);
	mem_cpy(p, buf, l+1);
	*__l = l;
	return p;
}

char static*
read_str(_int_u *__l) {
	char buf[1024];
	char *bufp = buf;
	char c = nextc;
	while(c != '"') {
		if ((bufp-buf)>=1024) {
			// buffer overflow
		}
		if (c == 0x5c) {
			incrp;
			switch(nextc) {
				case 'n':
					c = '\n';
				break;
				default:
					while(1);
			}
		}
		*(bufp++) = c;
		incrp;
		c = nextc;
	}

	*bufp = '\0';
	_int_u l;
	char *p = (char*)m_alloc((l = (bufp-buf))+1);
	dus_to_free(p);
	mem_cpy(p, buf, l+1);
	*__l = l;
	return p;
}

dus_tokenp dus_peektok(void) {
	dus_tokenp ret;
	dus_ulex(ret = dus_nexttok());
	return ret;
}

#define BUFSIZE 20
dus_tokenp static buf[BUFSIZE];
dus_tokenp static* end = buf;

void dus_ulex(dus_tokenp __p) {
	*(end++) = __p;
}

/*
	if any token issues occur this should be the first suspect

	after x tokens oldest one will be freed
*/
#define BACK 40
dus_tokenp static head = NULL;
dus_tokenp static next = NULL;
_int_u static len = 0;
static _8_s ignore = -1;
dus_tokenp dus_lex(void) {
	if (end>buf)
		return *(--end);
	if (len>BACK && head != NULL) {
		len--;
		dus_tokenp bk = head;
		head = head->next;
		m_free(bk);
	}

_bk:
	sk_white_space();
	if (dus_at_eof()) {
		return NULL;
	}

	if (*dus_p == '#') {
		while(*dus_p != '\n') {
			if (dus_at_eof())
				return NULL;
			incrp;
		}
		incrp;
		goto _bk;
	}

	dus_tokenp tok;
	tok = (dus_tokenp)m_alloc(sizeof(struct dus_token));
	tok->p = NULL;
	tok->next = NULL;
	if (!head)
		head = tok;
	if (next != NULL)
		next->next = tok;
	next = tok;

	_8_s neg = -1;
	char c = nextc;
	if (c == '-' && ignore == -1) {
		neg = 0;
		char k;
		k = nextcadvance;
		if (k>='0' && k<='9') {
			incrp;
			goto _num;
		}
	}
	ignore = -1;
	if ((c>='a' && c<='z') || c == '_') {
		tok->sort = _dus_tok_ident;
		tok->p = read_ident(&tok->l);
		if (nextc == '+' || nextc == '-') {
			ignore = 0;
		}

	} else if (c>='0' && c<='9') {
	_num:
		tok->sort = _dus_tok_dec;
		tok->p = read_dec(&tok->l);
		tok->neg = neg;
		if (nextc == '+' || nextc == '-') {
			ignore = 0;
		}
	} else {
		switch(c) {
			case 0x27:
				tok->sort = _dus_tok_keywd;
				tok->val = _dus_keywd_apos;
				incrp;
			break;
			case '!':
				tok->sort = _dus_tok_keywd;
				if (nextcadvance == '=') {
					tok->val = _dus_nteq;
					incrp;
				}
				incrp;
			break;
			case '$':
				tok->sort = _dus_tok_keywd;
				tok->val = _dus_dollar;
				incrp;
			break;
			case '"':
				incrp;
				tok->sort = _dus_tok_str;
				tok->p = read_str(&tok->l);
				incrp;
			break;
			case '=':
				tok->sort = _dus_tok_keywd;
				if (nextcadvance == '=') {
					tok->val = _dus_eqeq;
					incrp;
				}else{
					tok->val = _dus_eq;
				}
				incrp;				
			break;
			case ',':
				tok->sort = _dus_tok_keywd;
				tok->val = _dus_comma;
				incrp;
			break;
			case '`':
				tok->sort = _dus_tok_keywd;
				tok->val = _dus_grave;
				incrp;
			break;
			case '[':
			case ']':
				tok->sort = _dus_tok_keywd;
				tok->val = c == '['?_dus_l_bracket:_dus_r_bracket;
				incrp;
			break;
			case '{':
			case '}':
				tok->sort = _dus_tok_keywd;
				tok->val = c == '{'?_dus_l_brace:_dus_r_brace;
				incrp;
			break;
			case '(':
			case ')':
				tok->sort = _dus_tok_keywd;
				tok->val = c == '('?_dus_l_paren:_dus_r_paren;
				incrp;
			break;
			case '+':
				tok->sort = _dus_tok_keywd;
				tok->val = _dus_plus;
				incrp;
			break;
			case '-':
				tok->sort = _dus_tok_keywd;
				tok->val = _dus_minus;
				incrp;
			break;
			default:
				printf("unknown %c\n", c);
		}
	}
	len++;
	return tok;
}

void dus_lexer_cleanup(void) {
	dus_tokenp cur = head, tmp;
	while(cur != NULL) {
		tmp = cur->next;
		m_free(cur);
		cur = tmp;
	}
}
