# include "dus.h"
# include "../m_alloc.h"
# include "../io.h"
# include "../system/string.h"
# include "../string.h"
#include "../strf.h"
#include "../assert.h"
void static
exp_assign(dus_nodep *__expr) {
	if (!dus_nexttokis(_dus_tok_keywd, _dus_eq)) {
		dus_nodep exp = new_node;
		exp->r = dus_exp();
		exp->l = *__expr;
		printf("ASSIGN.\n");
		if (!exp->l || !exp->r) {
			printf("error.\n");
		}
		exp->kind = _dus_assign;
		*__expr = exp;
	}
}

_64_u static
read_int(char *__s) {
	return ffly_stno(__s);
}

dus_nodep static
exp_int(char *__s) {
	dus_nodep exp = new_node;
	exp->kind = _dus_int;
	exp->intval = read_int(__s);
	return exp;
}


struct f_lhash *dus_hm = NULL;
void static
exp_primary(dus_nodep *__expr) {
	dus_tokenp tok = dus_nexttok();
	if (!tok) {
		printf("null token.\n");
		return;
	}
	dus_nodep exp;
	if (tok->sort == _dus_tok_ident) {
		exp = f_lhash_get(&dus_env, tok->p, tok->l);
		if (!exp) {
			exp = f_lhash_get(dus_hm, tok->p, tok->l);
		}
		printf("lookup for '%s' result: %p.\n",tok->p,exp);
	} else {
		if (tok->sort == _dus_tok_dec) {
			exp = new_node;
			exp->kind = _dus_int;
			exp->intval = _ffly_dsn(tok->p,tok->l);	
			if (!tok->neg)
				exp->intval = -exp->intval;
		}else
		if (tok->sort == _dus_tok_str){// || tok->sort == _dec) {
	//		char c = *(char*)tok->p;
	//		if () {
	//			exp = exp_int((char*)tok->p);
			if (tok->sort == _dus_tok_str) {
				printf("string: %s.\n",tok->p);
				
				exp = new_node;
				exp->kind = _dus_str;
				exp->p = (char*)tok->p;
				exp->len = tok->l+1;
			
			}
		} else {
			if (tok->sort == _dus_tok_keywd && tok->val == _dus_dollar) {
				dus_tokenp n = dus_nexttok();	
				exp = new_node;
				exp->kind = _dus_str;
				exp->p = f_lhash_get(&dus_env, n->p, 1);
				exp->len = str_len(exp->p)+1;
			} else {
				dus_ulex(tok);
				return;
			}
		}
	}
	*__expr = exp;
}

void static
exp_numerical(dus_nodep *__expr) {
	dus_tokenp tok = dus_nexttok();
	if (!tok)return;
	if (tok->sort == _dus_tok_keywd) {
		_8_u op;
		switch(tok->val) {
			case _dus_plus:
				printf("OP_ADD.\n");
				op = _dus_op_add;
			break;
			case _dus_minus:
				printf("OP_SUB.\n");
				op = _dus_op_sub;
			break;
			default:
				dus_ulex(tok);
				return;
		}
		
		dus_nodep n;
		n = new_node;
		n->l = *__expr;
		*__expr = n;

		n->kind = _dus_bo;
		n->subop = op;
		n->r = dus_exp();
	}else{
		dus_ulex(tok);
	}
}

void static
exp_equality(dus_nodep *__expr) {
	dus_tokenp tok = dus_nexttok();
	if (!tok)return;
	
	if (tok->sort == _dus_tok_keywd) {
		_8_u op;
		switch(tok->val) {
			case _dus_eqeq:
				op = _dus_bo_equal;
			break;
			case _dus_nteq:
				op = _dus_bo_noteq;
			break;
			default:
			dus_ulex(tok);
			return;
		}

		dus_nodep n;
		n = new_node;
		n->l = *__expr;
		*__expr = n;

		n->subop = op;
		n->r = dus_exp();
	
	}else {
		dus_ulex(tok);
	}
}
_8_s static inarr = -1;
void static
exp_array(dus_nodep *__expr) {
	if (inarr == -1) {
		if (!dus_nexttokis(_dus_tok_keywd, _dus_grave)) {
		inarr = 0;
		dus_nodep n;
		n = new_node;
		*__expr = n;
		n->kind = _dus_array;
		dus_nodep *array;
		array = m_alloc(0x100*sizeof(dus_nodep));
		_int_u i;
		i = 0;
_again:
		array[i] = dus_exp();
		i++;
		if (!dus_nexttokis(_dus_tok_keywd, _dus_grave)) {
			goto _again;
		}
		n->array = array;
		n->arrlen = i;
		printf("ARRAY - %u.\n", i);
		inarr = -1;
		}
	}
}
void static
exp_arrelem(dus_nodep *__expr) {
	if (!dus_nexttokis(_dus_tok_keywd, _dus_l_bracket)) {
		dus_nodep n;
		n = new_node;
		n->arr = *__expr;
		*__expr = n;
		n->kind = _dus_deref;
		n->idx = dus_exp();
		dus_expect_token(_dus_tok_keywd,_dus_r_bracket);
	}
}

void static
exp_func_call(dus_nodep *__expr) {
	dus_nodep n;
	n = new_node;

	n->func = *__expr;
	*__expr = n;
	dus_nodep arg;
	n->narg = 0;
	n->kind = _dus_func_call;
	printf("FUNCTION_CALL.\n");
_again:
	printf("FUNC_ARG%u.\n",n->narg);
	arg = dus_exp();
	n->args[n->narg++] = arg;
	dus_tokenp tok = dus_peektok();
	printf("############### %u,%u.\n",tok->sort,tok->val);

	if (!dus_nexttokis(_dus_tok_keywd, _dus_comma)) {
		goto _again;
	}
	dus_expect_token(_dus_tok_keywd,_dus_r_paren);
}

void static
_exp(dus_nodep *exp) {
	exp_primary(exp);
	if (!dus_nexttokis(_dus_tok_keywd, _dus_l_paren)) {
		exp_func_call(exp);
	}else{
		exp_arrelem(exp);
		exp_assign(exp);
		exp_equality(exp);
		exp_numerical(exp);
	}
}


#include "../assert.h"
dus_nodep dus_exp(void) {
	dus_nodep exp = NULL, bk = NULL;
	int c = 0;
//_anoth:
	exp = NULL;
	_exp(&exp);
	assert(exp != NULL);
	exp->fd = bk;
//	if (!dus_nexttokis(_dus_tok_keywd, _dus_comma)) {
//		bk = exp;
//		c++;
//		goto _anoth;
//	}

	printf("EXP: %u.\n", c+1);
	return exp;
}

