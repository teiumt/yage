# include "dus.h"
# include "../types.h"
# include "../system/error.h"
# include "../unistd.h"
# include "../fcntl.h"
# include "../stat.h"
# include "../m_alloc.h"
# include "../io.h"
#include "../env.h"
_8_u *dus_p = NULL;
_8_u static *end;

_8_u dus_at_eof(void) {
	return dus_p>=end;
}

_y_err main(int __argc, char const **__argv) {
	if (__argc<2) {
		printf("please provide file.\n");
		reterr;
	}

	char const *file = __argv[1];
	_8_u *bed;

	int fd;
	fd = open(file, O_RDONLY, 0);
	if (fd == -1) {
		return -1;
	}
	struct stat st;
	fstat(fd, &st);
	dus_p = (_8_u*)m_alloc(st.st_size);
	bed = dus_p;
	end = dus_p+st.st_size-1;
	read(fd, dus_p, st.st_size);
	close(fd);

	dus_nodep top = NULL;
	dus_init();
	/*
		fill in with blanks
	*/
	static char const *blank = "0\0" "1\0" "2\0" "3\0" "4\0" "5\0" "6\0" "7\0" "8\0";
	char n = '0';
	_int_u i = 0;
	while(n != '9') {
		f_lhash_put(&dus_env,&n,1,blank+i);
		i+=2;
		n++;
	}

	char const **arg = __argv+1;
	n = '0';

	while(arg != __argv+__argc) {
		f_lhash_put(&dus_env, &n, 1, *arg);
		arg++;
		n++;
	}

	dus_parse(&top);
	dus_objp objs = dus_gen(top);
	dus_run(objs);
	dus_cleanup();
	m_free(bed);
}
