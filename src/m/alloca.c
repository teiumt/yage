# include "alloca.h"
# include "../m_alloc.h"
# include "../system/err.h"
# include "../config.h"
# include "../io.h"
# include "../system/config.h"
# include "../string.h"
/*
	note this is not alloca
	but a imitation of alloca
*/
static _8_u *p = NULL;
static _8_u *fresh = NULL;
static _8_u *end;

void static **amend[20];
void static ***bot = amend;
/*

	TODO:
		remove useage of allocr and use mmap
		as to reuse for pre allocr usage
*/
/*
	might fail if rdi is dirty

*/
__asm__(
		".text\n\t"
		".globl _alloca\n\t"
		"_alloca:\n\t"
		"pop %rcx\n\t"
		"movq $0x00000000ffffffff, %rax\n\t"
		"andq %rax, %rdi\n\t"
		"subq %rdi, %rsp\n\t"
		"andq $-15, %rsp\n\t"
		"movq %rsp, %rax\n\t"
		"push %rcx\n\t"
		"retq"
);
void ffly_alrr(void) {
	_8_u *tmp = p;
	_8_u *end = fresh;
	ffly_alrig();
	ffly_alsld(tmp, end-tmp);
	fresh+=end-tmp;
	void ***cur = amend;
	while(cur != bot) {
		printf("......\n");
		**cur = (void*)(p+(((_8_u*)**cur)-tmp));
		cur++;
	}
	bot = amend;
}

void ffly_alad(void **__p) {
	*(bot++) = __p;
}

_int_u ffly_alssz(void) {
	return fresh-p;
}

void ffly_alsst(void *__p) {
	mem_cpy(__p, p, fresh-p);
}

void ffly_alsld(void *__p, _int_u __size) {
	mem_cpy(p, __p, __size);
}

void ffly_alrig(void) {
	p = (_8_u*)m_alloc(*sysconf_get(alssize));
	fresh = p;
	end = p+*sysconf_get(alssize);
}

void ffly_alss(void *__p, _int_u __size) {
	p = (_8_u*)__p;
	fresh = p;
	end = p+__size;
}

void* ffly_alloca(_int_u __size, _f_err_t *__err) {
	if (fresh>=end) {
		ffly_errmsg("alloca failure.\n");
		return NULL;
	}
    void *ret = (void*)fresh;
	fresh+=__size;
	__ffmod_debug {
		if (fresh>=end)
			printf("warning: alloca, memory extends over end of stack, end extends: %u bytes.\n", fresh-end);
		printf("alloca, size: %u, off: %u\n", __size, fresh-p);	
	}
	return ret;
}

void ffly_trim(_int_u __n) {
	if (fresh-__n < p) {
		printf("warning: someone has tryed to trim to much.\n");
		fresh = p; // reset just in case
		return;
	}
	fresh-=__n;
}

void *ffly_frame(void) {
	return fresh;
}

void ffly_collapse(void *__frame) {
	fresh = __frame;
}

void ffly_alloca_cleanup(void) {
	if (p != NULL)
		m_free(p);
}
