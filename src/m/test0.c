#define N (512*64)
#define SIZE 1
#define VARIANCE (0xfff)

void ar_test0(void){
	void *ptrarr[N];
  void *ptr;
	_int_u k;
	k = 0;
	while(k != 10000){
  _int_u i;
  i = 0;
  for(;i != N;i++) {
    _int_u len;
  	len = 1<<15;

		//len = 16384;
		len = (1<<23)/512;
		ptrarr[i] = m_alloc(len);
		assert(ptrarr[i] != NULL);
//		mem_set(ptrarr[i],0,len);

  }
  i = 0;
  for(;i != N;i++) {
      m_free(ptrarr[i]);
  }
	k++;
	printf("%u percent complete.\n",k);
	}
}
#define TST_B
void ar_test(void){
	printf("commencing testing.\n");
	void *ptrarr[N];
	mem_set(ptrarr,0,sizeof(void*)*N);
	_int_u j = 0;
	while(j++ != 100){
	_int_u h = 0;
	while(h++ != 16){
	_64_u tmp;
	_int_u i;
	i = 0;
	for(;i != N;i++){
		if(!ptrarr[i]){
			_int_u len;
#ifdef TST_A
			len = (1<<23)/512;
			if(i>25 && i < 4096){
				len = 1<<15;
			}else if(i>8192 && i<16384){
				len = ((i-800)<<23)/4;
			}
#endif
#ifdef TST_B
			len = (ffly_rand()&VARIANCE)+SIZE;
#endif
			ptrarr[i] = m_alloc(len);
			if(!ptrarr[i]) {
				printf("got given a null pointer.\n");
				return;
			}
			//mem_set(ptrarr[i],0xff,len);
		}else{
		//	ptrarr[i] = m_realloc(ptrarr[i],((1<<23)/512)+10);
		}
	}

	tmp = ptrarr[0];
	i = 0;
	_int_u stop;
	stop = ffly_rand()%(N+1);
	while(1){
		if(i == stop)break;
		_int_u k;
		k = ffly_rand()%N;
		if(ptrarr[k] != NULL) {
			m_free(ptrarr[k]);
			ptrarr[k] = NULL;
			i++;
		}
	}
	}
#define TEST 1024
	void *test[TEST];
	_int_u i;
	i = 0;
	for(;i != TEST;i++){
		test[i] = m_alloc(1<<15);
	}
	i = 0;
	for(;i != TEST;i++){
		m_free(test[i]);
	}

	printf("%u percent complete.\n",j);
	}

	_int_u i;
	i = 0;
	for(;i != N;i++){
		if(ptrarr[i] != NULL){
			m_free(ptrarr[i]);
		}
	}

	printf("finished testing.\n");
}
