#include "ar_internal.h"
#include "../linux/mman.h"
#include "../linux/unistd.h"
#include "../linux/signal.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../system/util/checksum.h"
#include "../thread.h"
#include "../mutex.h"
#include "../config.h"
#include "../system/tls.h"
#include "../system/string.h"
#include "../string.h"
#include "../io.h"
#include "../m.h"
/*
  sysmore and systrim - is continuous memory region that 
  can be reduced or etc.
*/
void*
_ar_sysmore(_64_u __bc) {
  _64_u limit;
  void *ret;
  ret = ((_8_u*)_ar_ms.end)+_ar_ms.off;
  _ar_ms.off+=__bc;
  if(_ar_ms.off>_ar_ms.usage_peak){
		_ar_ms.usage_peak = _ar_ms.off;
	}
	_64_u top = _ar_ms.off;
  /*
    please for the love of god do somthing about this horid thing
    also NOTE: we need to be using the real page size of the system its self or mulables of it
  */
  limit = (top+MASK_OF(PAGE_SHIFT))&~MASK_OF(PAGE_SHIFT);
  if(limit>_ar_ms.limit){
    _ar_ms.limit = limit;
    ar_printf("brk value now at: %u.\n",limit);
		if(brk(_ar_ms.top = (void*)((_8_u*)_ar_ms.end+limit)) == (void*)-1) {
#ifdef __ffly_debug
      ar_printf("error: brk.\n");
#endif
      ar_abort();
    }
  }
  return ret;
}

void
_ar_systrim(_64_u __bc) {
    _ar_ms.off-=__bc;
		_64_u limit;
    limit = (_ar_ms.off+MASK_OF(PAGE_SHIFT))&~MASK_OF(PAGE_SHIFT);
    if ((limit+(3<<PAGE_SHIFT)) < _ar_ms.limit) {
      _ar_ms.limit = limit;
      if (brk(_ar_ms.top = (void*)((_8_u*)_ar_ms.end+limit)) == (void*)-1) {
#ifdef __ffly_debug
        ar_printf("error: brk.\n");
#endif
        ar_abort();
      }
    }
}
