#include "ar_internal.h"
# include "../linux/mman.h"
# include "../linux/unistd.h"
# include "../linux/signal.h"
# include "../linux/fcntl.h"
# include "../linux/stat.h"
# include "../system/util/checksum.h"
# include "../thread.h"
# include "../mutex.h"
# include "../config.h"
# include "../system/tls.h"
# include "../system/string.h"
# include "../string.h"
# include "../io.h"
# include "../m.h"

void encaseprintf(char *buf, char *buf0, _int_u len, _int_u len0) {
	char upper[2048];
	char lower[2048];
	_int_u ln;
	if(!buf0){
		ln = len;
	}else{
	if(len>len0) {
		ln = len;
		mem_set(buf0+len0,' ',len-len0);
		len0 = len;
	}else{
		ln = len0;
		if(len0>len) {
			mem_set(buf+len,' ',len0-len);
		}
		len = len0;
	}
	}

	mem_set(upper,'_',ln+4);
	mem_set(lower,'`',ln+4);
	upper[4+ln] = '\n';
	upper[4+ln+1] = '|';
	upper[4+ln+2] = ' ';
	lower[4+ln] = '\n';

	ar_printfb(upper,ln+4+3);

	if(buf0 != NULL){
		buf[ln] = ' ';
		buf[ln+1] = '|';
		buf[ln+2] = '\n';
		buf[ln+3] = '|';
		buf[ln+4] = ' ';

		ar_printfb(buf,ln+5);
	}else {
		buf[ln] = ' ';
		buf[ln+1] = '|';
		buf[ln+2] = '\n';
		ar_printfb(buf,ln+3);
	}
	if(buf0 != NULL){
		buf0[ln] = ' ';
		buf0[ln+1] = '|';
		buf0[ln+2] = '\n';
		ar_printfb(buf0,ln+3);
	}

	ar_printfb(lower,ln+4+1);
}


void pot_pr(potp p){
	/*
	_int_u i;
	i = 0;
	balep bale;
	bale = get_bale(p,p->top_bale);
	char buf[2048];
	char buf0[2048];
	_int_u len,len0;
_next:
	len = ar_printfit(buf,"BALE(%u) - size: %u, pot_offset: %x, remnants: %u, inuse: %s",
		i++,bale->size, bale_offset(bale), bale->pad, is_used(bale)?"yes":"no");
	len0 = ar_printfit(buf0,"prev: %x{%s}, next: %x{%s}, fd: %x{%s}, bk: %x{%s}",
		is_null(bale->prev)?0:bale->prev, is_null(bale->prev)?"dead":"alive", is_null(bale->next)?0:bale->next, is_null(bale->next)?"dead":"alive",
		is_null(bale->fd)?0:bale->fd, is_null(bale->fd)?"dead":"alive", is_null(bale->bk)?0:bale->bk, is_null(bale->bk)?"dead":"alive");
	encaseprintf(buf,buf0,len,len0);

	if (not_null(bale->next)) {
		bale = get_bale(p,bale->next);
		goto _next;
	}
	*/
}
/*
void pot_pr(potp __pot) {
	_int_u i;
	potp p = __pot;
	balep blk = get_bale(p, p->top_bale);
	_int_u static depth = 0;
	if (is_null(p->top_bale)) return;
	if (p->fd != NULL) {
		ar_printf("\ndepth, %u\n", depth++);
		pot_pr(p->fd);
		ar_printf("**end\n");
	}
	i = 0;
_next:
	ar_printf("/-----------------------\\\n");
	ar_printf("| %u - size: %u, off: %x, pad: %u, inuse: %s\n", i++,
		blk->size, bale_offset(blk), blk->pad, is_used(blk)?"yes":"no");

	ar_printf("| prev: %x{%s}, next: %x{%s}, fd: %x{%s}, bk: %x{%s}\n",
		is_null(blk->prev)?0:blk->prev, is_null(blk->prev)?"dead":"alive", is_null(blk->next)?0:blk->next, is_null(blk->next)?"dead":"alive",
		is_null(blk->fd)?0:blk->fd, is_null(blk->fd)?"dead":"alive", is_null(blk->bk)?0:blk->bk, is_null(blk->bk)?"dead":"alive");

	ar_printf("\\----------------------/\n");
	if (not_null(blk->next)) {
		blk = get_bale(p, blk->next);
		goto _next;
	}

}*/
# include "../maths/abs.h"
void lspot_pf(potp p) {
	if(p->bins[0] == AR_NULL)return;
	balep bale;
	bale = get_lsbale(p, p->bins[0]);
	_int_u len;
	char buf[2048];
_fwd:
	len = ar_printfit(buf,"size: %u,	0x%x, pad: %u",bale->size, lsbale_offset(bale),bale->pad);
	encaseprintf(buf,NULL,len,0);
	
	if(bale->fd != AR_NULL) {
		ar_assert(bale->fd != lsbale_offset(bale),"well this is no good, the bale forward is the same bale????\n");
		bale = get_lsbale(p,bale->fd);
		goto _fwd;
	}
}

void pot_pf(potp p){
	_int_u i;
	ar_off_t *bin = p->bins;
	balep bale,bk;
_next:
	bk = NULL;
	if (is_null(*bin)) {
		ar_printf("bin %u is empty.\n", bin-p->bins);
		goto _sk;
	}
	if (*bin >= p->off) {
		ar_printf("bin is messed up, at: %u, off: %u\n", bin-p->bins, *bin);
		goto _sk;
	}

	i = 0;
	bale = get_bale(p, *bin);
	char buf[2048];
_fwd:
	if (bk != NULL) {
		ar_printf("\\\n");
		ar_printf(" > %u-bytes\n", ffly_abs((_int)bale_offset(bk)-(_int)bale_offset(bale)));
		ar_printf("/\n");
	}
	/*
			______________
			| size = 200 |
			``````````````
	*/
	_int_u len;
	len = ar_printfit(buf,"size: %u,	0x%x, pad: %u",bale->size, bale_offset(bale),bale->pad);
	encaseprintf(buf,NULL,len,0);
	
	if (not_null(bale->fd)) {
		bk = bale;
		bale = get_bale(p, bale->fd);
		goto _fwd;
	}

_sk:
	if (bin != p->bins+(no_bins-1)) {
		bin++;
		goto _next;
	}

}
/*
void pot_pf(potp __pot) {
	potp p = __pot;
	_int_u i;
	ar_off_t *bin = p->bins;
	balep blk, bk;
	_int_u static depth = 0;
	if (p->fd != NULL) {
		if (!depth)
			ar_printf("**start\n");
		ar_printf("\ndepth, %u, %u\n", depth++, p->fd->off);
		pot_pf(p->fd);
		ar_printf("**end\n");
	}

_next:
	bk = NULL;
	if (is_null(*bin)) {
		ar_printf("bin %u is empty.\n", bin-p->bins);
		goto _sk;
	}
	if (*bin >= p->off) {
		ar_printf("bin is messed up, at: %u, off: %u\n", bin-p->bins, *bin);
		goto _sk;
	}

	i = 0;
	blk = get_bale(p, *bin);
_fwd:
	if (bk != NULL) {
		ar_printf("\\\n");
		ar_printf(" > %u-bytes\n", ffly_abs((_int)bale_offset(bk)-(_int)bale_offset(blk)));
		ar_printf("/\n");
	}
	ar_printf("/-----------------------\\\n");
	ar_printf("| size: %u\t\t| 0x%x, pad: %u\n", blk->size, bale_offset(blk), blk->pad);
	ar_printf("\\----------------------/\n");
	if (not_null(blk->fd)) {
		bk = blk;
		blk = get_bale(p, blk->fd);
		goto _fwd;
	}

_sk:
	if (bin != p->bins+(no_bins-1)) {
		bin++;
		goto _next;
	}

}*/

void ar_pot_pf(void) {
//	pot_pf(cur_arena);
//	pot_pr(cur_arena);
}

void ar_pot_pf_for(_64_u __addr) {
}

/*
void pr(void) {
	potp arena = cur_arena;
	ar_printf("\n**** all ****\n");
	rodp r = *rods;
	potp p = &main_pot;
	_int_u no = 0;
_next:
	if (p != NULL) {
		if (p == &main_pot)
			ar_printf("~: main pot, no{%u}\n", no);
		ar_printf("\npot, %u, used: %u, buried: %u, dead: %u, off: %u, pages: %u, bales: %u, total: %u\n",
			no++, _ar_used(p), _ar_buried(p), _ar_dead(p), p->off, p->limit>>PAGE_SHIFT, p->blk_c, p->total);
		pot_pr(p);
		if ((p = p->next) != NULL)
			goto _next;
	}

	if (p == &main_pot) {
		p = r->p;
		goto _next;
	}

	if ((r = r->next) != *rods) {
		p = r->p;
		goto _next;
	}
}
void pf(void) {
	potp arena = cur_arena;
	ar_printf("\n**** free ****\n");
	rodp r = *rods;
	potp p = &main_pot;
	_int_u no = 0;
_next:
	if (p != NULL) {
		ar_printf("\npot, %u, used: %u, buried: %u, dead: %u, off: %u, pages: %u, bales: %u, total: %u\n",
			no++, _ar_used(p), _ar_buried(p), _ar_dead(p), p->off, p->limit>>PAGE_SHIFT, p->blk_c, p->total);
		pot_pf(p);

		if ((p = p->next) != NULL)
			goto _next;
	}

	if (p == &main_pot) {
		p = r->p;
		goto _next;
	}

	if ((r = r->next) != *rods) {
		p = r->p;
		goto _next;
	}
}*/
