#include "../m_alloc.h"
#include "../mutex.h"
#define AM (4096*16)
#define N (4096)
#define _F ((rgl()&512)+32)
#define _A ((rgl()&512)+32)
#define LAG 4
static _8_s free_gate = -1;
static _8_s are_gate = -1;
void static *free_ptr[N];
void static *are_ptr[N];
_64_u static randgen(void) {
	_64_u num;
	num = ffgen_rand16l();
	return num;
}

#include "../atomic.h"
_64_u static rgl(void){
	_64_u num;
	num = randgen();
	num &= 0xffffff;
	num = num&(4096-1);
	return num+1;
}

_64_u __thread max_in_circ;
_64_u static rglx(void){
	_64_u num;
	num = rgl();
	
	if(num&32 && max_in_circ<128){
		num = 1<<15;


		max_in_circ++;
	}
	return num;
}
static _64_u a_cntr = 0;
static _64_u b_cntr = 0;
static void *personal_A[N];
static void *personal_B[N];

void static A(void) {
	max_in_circ = 0;
	_int_u i;
	_32_u idx;
	_32_u alloced = 0;
	_32_u freed = 0;
	_int_u j,u;
	j = 0;
	for(;j != LAG;j++){
	i = 0;
	u = _F;
	for(;i != u;i++){
		idx = (a_cntr++)&(N-1);
		void *fr;
		fr = free_ptr[idx];
		if(fr != NULL) {
			m_free(fr);
			freed++;
			free_ptr[idx] = NULL;
		}
		fr = personal_A[idx];
		if(fr != NULL){
			m_free(fr);
			personal_A[idx] = NULL;
		}
	}

	i = 0;
	u = _A;
	for(;i != u;i++){
		idx = (a_cntr++)&(N-1);
		void *are;
		are = are_ptr[idx];
		if(!are){
			alloced++;
			are_ptr[idx] = m_alloc(rglx()+1);
		}
		are = personal_A[idx];
		if(!are){
			personal_A[idx] = m_alloc(rglx()+1);
		}
	}
	}
	printf("(A-%u:%u).\n",alloced,freed);
}

void static B(void) {
	max_in_circ = 0;
	_int_u i;
	_32_u idx;
	_32_u alloced = 0;
	_32_u freed = 0;
  _int_u j,u;
  j = 0;
  for(;j != LAG;j++){
	i = 0;
	u = _F;
	for(;i != u;i++){
		idx = (b_cntr++)&(N-1);
		void *are;
		are = are_ptr[idx];
		if(are != NULL){
			m_free(are);
			freed++;
			are_ptr[idx] = NULL;
		}
		are = personal_B[idx];
		if(are != NULL){
			m_free(are);
			personal_B[idx] = NULL;
		}
	}

	i = 0;
	u = _A;
	for(;i != u;i++){
		idx = (b_cntr++)&(N-1);
		void *fr;
		fr = free_ptr[idx];
		if(!fr){
			alloced++;
			free_ptr[idx] = m_alloc(rglx()+1);
		}
		fr = personal_B[idx];
		if(!fr){
			personal_B[idx] = m_alloc(rglx()+1);
		}
	}
	}
	printf("(B-%u:%u).\n",alloced,freed);
}
static _8_s free_running = 0;
static _8_s ft_done = -1;
void static free_thread(void *__arg) {
	while(!free_running) {
		A();
	}
	ft_done = 0;
}

#include "../thread.h"
void ar_test(void){
	mem_set(personal_A,0,sizeof(void*)*N);
	mem_set(personal_B,0,sizeof(void*)*N);
	_int_u i;
	i = 0;
	for(;i != N;i++){
		free_ptr[i] = NULL;
		are_ptr[i] = NULL;
	}
	tstrucp th;
	tcreat(&th, free_thread, NULL);
	i = 0;
	while(i != AM){B();i++;}
	free_running = -1;
	while(ft_done == -1);
}
