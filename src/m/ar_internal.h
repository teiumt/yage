#ifndef __ar__internal__h
#define __ar__internal__h
#include "allocr.h"
#include "../proc.h"
#define ARMSG_BALE	1
#define ARMSG_POT		2
#define ARMSG_TBD	4//to be descided

//how much output is expected lowest is the highest
#define ARMSG_LV0	(_64u_(1)<<60)
#define ARMSG_LV1	(_64u_(1)<<61)
#define ARMSG_LV2	(_64u_(1)<<62)
#define ARMSG_LVK	(_64u_(1)<<63)

/*
	TODO?:
		writing to locations so far apart hits the caches hard, when dealing with an inactive pot 
		it may be a good ida to add a ID to the pot and use that with global memory to get infomation about the pot.

	- if the data we are dealing with is to do with allocation of bales then this is pot data.
	- if the data is for interaction between pots we should reference it.

	why? for example, lets say we are interacting with a pot in this case we want to lock it.

	we have two options.

	A)

		potp above = are_pot-POT_SIZE;
		mt_lock(&above->lock);

	B)
		pot_datap above = _ar_ms.private_data[are_pot->p-1];
		mt_lock(&above->lock);

		or 
		_ar_ms.private_data[are_pot->above_ref]

		or 
		_ar_ms.private_data[_ar_ms.private_data[are_pot->us]->above]

	the two options have there down fall (A) is slower as its most likly that we are going to be working directly with memory(caches are of no use)
	(B) in the case we have non pot data we may wast memory, however as of the close proximity to oneanother it promotes the use of cache.

	for example the distance between pots is 2^23 or 8MB, where as the total size of (B) might be 65Kbytes well within rage where cahes can take effect, with a distance of a magnatude of under 128 bytes.

	the meaning behind data keeping.
	_______________________________
	lets say in a drastic case where lesser pots take 100ms to init the pot structure,
	and greater pots 200ms. it would be in this case a must to keep the initing of pots to a low/min.


	NOTE:
		POTFIT allocation should never be larger then a greater pot, the only case this is acceptable
		is when two or more POTFIT allocations are freed and then murged making a larger void space.
		we can either turn it into POTS or leave for any allocations larger then the greater pot size cap on the POTFIT size.
	
		allocation of POTS:
			when allocating lesser or greater pots we should always attempt to allocate then next to one of there own,
			greater pot next to existing greater pot, same for lesser pots. this will reduce splicing of linear extents(meaning if we 
			have a linear region made up of pots with a greater pot in the middle we dont cut it in half as will cause performance issues).

		lesser pot merger with greater pot, if it happens we have 4 lesser pots with a small amount of bales within and dont have any 
		free greater pot. we then transfer the exiting bale in all lesser pots into a greater pot(ONLY FOR DRASTIC MESSURES to avoid at all costs extending memory mappings).
      
			LESSER POTS(FREE)            POTFIT extent
		 ________  ________  ___________________________________
		|        ||        ||                                   |
		|        ||        ||                                   |
		|        ||        ||                                   |
		|________||________||___________________________________|
								<-------- EXPAND sideways(to avoid allocting the extent by other means like using pots greater or lesser)

		this applys to all, greater pots, lesser pots and potfits
		this promotes the congregation of like minded things.
		what we want to avoid is lesser greater and potfits being randomly placed.

	thoughts:
		. when extending a POTFIT into free greater pots exess pots may remain
		when extending lesser POTS into a free greater pot this will cause that greater pot to be dismantled(depends on what side it came from top or bottom of greater pot).
		when extending greater POTS into free lesser pots we will need to know how many lesser pots in that direction are free.

	how we are going to deal with greater pots in free space?

	what we will do is add greater pots to the lesser pot lot landscape, meaning greater pots will be placed in with
	the lesser lots. however greater pots contain uninited pots that are invalid as pots, the way we will deal with this
	is we will have a list with an ID that will be added to the greater pots free adjcent pots. This list
	will consist of an int counting the number of uninited pots below or above the adjcent pots. when a pot is allocated
	this list will have to be advanced and the pot will need removing. this will also be a branch of a lot.

	TODO:
		for greater pots insted of decommissioning due to the intire space being unused we retain them until someone comes along to take them.
		so for lesser pots we give the pots up, for greater pots we keep until taken away from us.

	TODO:
		pot->underneath if 'pot' is the last in line then point to itself at a displacment so that pot->somthing is not pot->correct
		or somthing else????

	Malfunctions and would be causes:

	#asserts because of POT_LAST_IN_LINE flag
	- below_flags() may not be clear or contains junk data.


	NOTES:
	- when dealing with the spantree take note that it may do a lesser allocation so
	be wary of that fact as it might get caught in a lock trap if extra precaution arnt taken


	TODO:
		shorting: keep track of most out of place bales i.e there adjcent bales being very diffrent in size.
		do brute force sort on those ones or try to get them as close to others that have an identical physique.
*/
/*
	locking approch, bale shall not have locks.
	the pots themselfs are locked, only one thread may access a pot at one time.

	locking is expansive on a large scale such as locking the bales themselfs.
	
	look when allocation memory it will be directed to pots that are apart of that threads arena,
	the only issue is when freeing the memory. this is where another thread may free a piece of memory
	allocated by another thread.


	we are going to go about this by the 'giving model'
	if somone lacks somthing then give them that somthing


	FIXME?
		remove BALE_BINNED bit somehow, the issue is that we have no way to tell if the bale has been binned.

		the issue is that if there is only one bale in a bin then both 'fd' and 'bk' are AR_NULL meaning 
		we cant presume that the bale is within the bins reach is either are not NULL.

		i was thinking we could just loop the bales in the bins in a ring, thus assuring that 'fd and bk' are always not NULL,
		but would mean we would have to check the bin every time and compare agenst are bale to check if we are at the head,or tail of the list.


				 bk<          >fd
		NULL -> X -> X -> X -> NULL
		front                  rear

		BIN[Y] -> X -> X -> X -> BIN[Y]->bk
		front                      rear

		by doing thiese checks it would become a performance issue.
		as if we try to sort the bales in the bin we need to make sure that
		the front is highest order and rear is lowest order of sizes.

		meaning checking the rear by using the front.
		meaning.

		front = get_bale(__ref,__pot->bin[Y])
		rear = front->bk

		where as if we could just use fd = AR_NULL means we are at the front
		and bk = AR_NULL means we are at the rear

		is it okay to use a flag for this??? could we have diffrent types of bins and try to make use of it more????

		we do however have extra bits;

		#####################

		fd,bk,and size and other ints within the bale structure could be aligned by 
		some amount by one or somthing ???
		meaning we could use the first bit in the size int as a flag?? for this??

		#####################

	CHANGE-OF-PLANES:
		lesser pot size is now 2^23 so 8MB so 838,8608
		larger pot sizes may be the same but handled in a diffrent way?
		
		this change in size means extream bale management

		how did i select this size??

		worst case is 1/4 of the pot is just free bales.


		so # where average bale size 400-bytes 50 64-bit ints
		2^23/(28+400) = 19,599 - max'ish amount of bales

		so lets just say that

		19,599/4 = number-of-bales that might be free in a worst case.
		so 4,899 < this is a nice manageable number

		also means we have an extra 32-23 = 9-bits

	not using this until finished
  as it may hide issues.

  also not fully done so using wont do much but mess up alot
*/
#define ALIGN 1
#define MASK_OF(__bit) ((((_64_u)1)<<(__bit))-1)
#define ALIGN_TO(__no,__to) (((__no)+((__to)-1))&~((__to)-1))
#define align_to(__no, __to)(((__no)+((__to)-1))&((~(__to))+1))
#define is_aligned_to(__no, __align)(((__no&__align) == __align)||!(__no&__align))

//CHANGE:
#define PAGE_SHIFT 23
#define PAGE_SIZE (1<<PAGE_SHIFT)
#define LARGER_SET 1
/*
	complete sprs to retain
*/
#define CSPRTR (SPROUT_MAX-3)
/*
	greater pot resurrection

	NOTE: this holds greater pots capative
*/
//#define GPRESUR 64

#define GTTT 64

typedef _32_u ar_off_t;
typedef _32_u ar_size_t;
typedef _32_u ar_uint_t;
typedef _32_s ar_int_t;

#define AR_HWRD _16_u
#define AR_DWRD _32_u
#define AR_QWRD _64_u

#define AR_BALE_OFFSET AR_OFFSET
#define AR_OFFSET _32_u
#define AR_INT_U    _32_u
#define AR_INT_S    _32_s
#define AR_INT      _32_s
#define AR_SIZE   _32_u
/* recycle old spots
*/
#ifndef NULL
# define NULL ((void*)0)
#endif

// bale flags
#define BALE_USED 0x02
#define BALE_MMAPED 0x04
#define BALE_LINKED 0x08
#define BALE_SHAM		0x10
#define BALE_UBIN	0x20

//changed fomr x30 to x40
#define BALE_BINNED 0x40
/*
  POT flags
	nofurther
*/
#define POT_FREE	0x01
#define POT_LAST_IN_LINE 0x02
#define POT_APART 0x04
#define POT_TAKE 0x08
/*
	this flag propagates throughout pots in a upwards manner.
	the meaning for using this method is that we cant have a sprout structure for every greater pot(lots of memory)
	but the issue that comes along with this method is that the pots that make up the greater pot dont know if 
	the sprout is valid or not. this is where we resort to telling are adjcent pots that this sprout is indeed valid.

	we could tell all the pots that make up the greater pot that the sprout is valid, however we want things to happen in a specific order.
	if we did that then the sprout would need to contain placeholders for each subpot

	the issue with this is that we would need to lock every pot in the greater pot to prevent it from access a sprout if it where to be removed.
	else we would need someway to prevent all the pots from accessing the sprout.
    
		POTS
	 _______
	|       |
	|       | gtp -> TABLE -|
	|_______|               |
	|       |               |
	|       | gtp -> TABLE -|
	|_______|               | -> SPROUT
	|       |               |
	|       | gtp -> TABLE -|
	|_______|               |
	|       |               |
	|       | gtp -> TABLE -|
	|_______|

	overall the issue is at the TABLE location, if we where to make it invalid how would we make sure know one expect areselfs 
	are using the sprout? meaning someone else might get an invalid sprout thats in the process of being freed.

	the method we are using is a propagation method.
	why? because it means we use the locks within the pots themselfs.

	the only things im sceptical on is the POT_SPROUT flags copying.

	TODO:
		could merge methods. make it like a viral infection. where not only the base of the greater pot is the spreader?.
		where a pot can only join itself if its be incontact with another infected pot? this will mean no locking at a table level.
*/
#define POT_SPROUT 0x10
/*
	this is nothing but a debug flag,
	it is to make sure there is no interference
	from the lesser pot dishing process;
	the _balloc function. 

	its to make sure that that the pot isent somehow 
	making its way back into the lesser pot dishing process.
*/
#define POT_INTERFERE 0x20
#define POT_GTRESTRICT 0x100
/*
	if the pot for example like a greater pot has inner pots that however stay uninited(overwritten with dirty data)
	when decommissioning them theres no need to init them howver we we have sure only the flags have been preped 
	to let us know if initing of the pot is needed. as in the case the greater pot is pulled back to the job of greater pot
	then we dont wast are time initing it for nothing so we keep it small.
*/
#define POT_INCHOATE 0x200
#define POT_GREATER 0x40
#define POT_GREMNANTS 0x80
#define POT_STAG 0x400
#define PT_GREATER (1<<15)

#define ARENA_ISSUED 0x01

//fate after death
//no longer at rest
//remanate
#define PT_FAC0_MASK (0xf)
#define PT_FAC1_MASK (0xf<<4)
#define PT_FAC2_MASK (0xf<<8)
#define PT_FAC0(__x) ((__x)<<0)
#define PT_FAC1(__x) ((__x)<<4)
#define PT_FAC2(__x) ((__x)<<8)
/*
	0000 0000 0000 000 0
	^^^^ ^^^^ ^^^^ ^^^ ^- type
	|    tag0/tag1 misc-bits
	|
	|
	directive
	
*/

//uncertain
#define PT_FAC0_UNCER		PT_FAC0(0)
#define PT_FAC0_BIRTH		PT_FAC0(1)
#define PT_FAC0_FATE		PT_FAC0(2)
#define PT_FAC0_LESSER_BIRTH		PT_FAC0(3)
#define PT_FAC0_LESSER_FATE			PT_FAC0(4)
#define PT_FAC0_LESSER_SPANNED		PT_FAC0(5)

#define PT_FAC1_BIRTH_NLA					PT_FAC1(0)
#define PT_FAC1_BIRTH_PURE				PT_FAC1(1)
#define PT_FAC1_BIRTH_PARTLY			PT_FAC1(2)
#define PT_FAC1_BIRTH_COMPLETE		PT_FAC1(3)
#define PT_FAC1_BIRTH_SPROUT			PT_FAC1(4)

#define PT_FAC1_FATE_RESURRECTED	PT_FAC1(0)
#define PT_FAC1_FATE_DECOMPOSED		PT_FAC1(1)
#define PT_FAC1_FATE_ADOPTED			PT_FAC1(2)

#define PT_FAC1_LESSER_BIRTH_SALVAGE		PT_FAC1(0)
#define PT_FAC1_LESSER_BIRTH_SPROUT			PT_FAC1(1)
//GREATER POT TAKINGS
#define PT_FAC1_LESSER_BIRTH_GPT				PT_FAC1(2)
#define PT_FAC1_LESSER_BIRTH_PURE				PT_FAC1(4)

#define PT_FAC1_LESSER_DEATH_SPROUT					PT_FAC1(0)
#define PT_FAC1_LESSER_DEATH_REPOSSESSED		PT_FAC1(1)
#define PT_FAC1_LESSER_DEATH_REJOIN					PT_FAC1(2)

#define PT_FAC1_LESSER_SPANNED_ALONE					PT_FAC1(0)
#define PT_FAC1_LESSER_SPANNED_CONJOINED			PT_FAC1(1)
#define PT_FAC1_LESSER_SPANNED_AFFILIATED		PT_FAC1(2)


#define PT_FAC2_BIRTH_NOTHING			PT_FAC2(0)
#define PT_FAC2_FATE_NOTHING			PT_FAC2(0)

#define PT_FAC2_BIRTH_ACEND				PT_FAC2(1)
#define PT_FAC2_BIRTH_SUCCESSOR		PT_FAC2(2)
#define PT_FAC2_FATE_RECOMBINED	PT_FAC2(1)

#define PT_FAC0_EXTR(__tags) ((__tags)&PT_FAC0_MASK)
#define PT_FAC1_EXTR(__tags) (((__tags)&PT_FAC1_MASK)>>4)
#define PT_FAC2_EXTR(__tags) (((__tags)&PT_FAC2_MASK)>>8)

#define PT_FAC(__tags,__x,__y)\
	__tags = (((__tags)&~(__x))|(__y))
//plunk
//transferable mask
#define POT_TFB (POT_LAST_IN_LINE)
/*
  min about that can be trimed from oversized bale
*/
#define TRIM_MIN 0x80
// MAX SHRINKAGE
#define MAX_SH (0x40)
// MAX GROWTHAGE
#define MAX_GR (0x40)

#define MIN_PAD_TBGM (0x10)

#define bale_offset(__bale)\
  (((_64_u)__bale)&MASK_OF(POT_SIZE_SHIFT))
#define lsbale_offset(__bale)\
  (((_64_u)__bale)&MASK_OF(LGRPOT_SIZE_SHIFT))
#define balem_offset(__bale,__m)\
  (((_64_u)__bale)&__m)


#define bale_end(__bale)\
	(bale_offset(__bale)+((__bale)->size+bale_size))
#define balem_end(__bale,__m)\
	(balem_offset(__bale,__m)+((__bale)->size+bale_size))
#define lsbale_end(__bale)\
	(lsbale_offset(__bale)+((__bale)->size+bale_size))

#define get_pot(__b)\
  ((potp)((((_64_u)__b)|MASK_OF(POT_SIZE_SHIFT))-sizeof(struct pot)))
#define get_lspot(__b)\
  ((potp)((((_64_u)__b)|MASK_OF(LGRPOT_SIZE_SHIFT))-sizeof(struct pot)))

#define is_flag(__flags, __flag)\
  (((__flags)&(__flag))==(__flag))

#define bale_flag(__bale,__flag)\
	is_flag((__bale)->flags,__flag)
#define is_free(__b) \
  !is_flag((__b)->flags,BALE_USED)
#define is_used(__b) \
  is_flag((__b)->flags,BALE_USED)
/*
  __ref - any pointer within the range of the pot we are working within
*/
#define get_bale(__ref,__off)\
	((balep)((((_64_u)__ref)&~MASK_OF(POT_SIZE_SHIFT))|(__off)))
#define get_lsbale(__ref,__off) \
  ((balep)((((_64_u)__ref)&~MASK_OF(LGRPOT_SIZE_SHIFT))|(__off)))
#define get_balem(__ref,__off,__mask) \
  ((balep)((((_64_u)__ref)&~__mask)|(__off)))


#define bale_next(__b) get_bale(__b,(__b)->next)
#define bale_prev(__b) get_bale(__b,(__b)->prev)
#define lsbale_next(__b) get_lsbale(__b,(__b)->next)
#define lsbale_prev(__b) get_lsbale(__b,(__b)->prev)

#define balem_next(__b,__m) get_balem(__b,(__b)->next,__m)
#define balem_prev(__b,__m) get_balem(__b,(__b)->prev,__m)


#define AR_NULL (~(AR_OFFSET)0)
#define is_null(__p) ((__p)==AR_NULL) 
#define not_null(__p) ((__p)!=AR_NULL)

#define POT_SIZE_SHIFT 23
#define POT_SIZE (1<<POT_SIZE_SHIFT)
#define POT_MASK (POT_SIZE-1)
#define POT_REAL_SIZE (POT_MASK^sizeof(struct pot))
/*
	this tells us if a bale qualifies to be placed in a POT,
	without bale header(using pot header as supplement)

	here we can assume that the pointer given to the user is POT aligned meaning 
	the first 23-bits are all zero(thus telling us its a bale of this kind)
	meaning we sould fetch the pot header.
*/
#define BALE_POTFITEX (POT_SIZE/8)
#define BALE_POTFIT (POT_SIZE-(POT_SIZE/8))
#define lkpot(__pot) \
  mt_lock(&(__pot)->inuse_lock)
#define ulpot(__pot) \
  mt_unlock(&(__pot)->inuse_lock)

#define _64u_(__x) ((_64_u)(__x))
#define _32u_(__x) ((_32_u)(__x))
#define no_bins 81
#define lsbin_no(__bc)\
	(((__bc)>>15)&63)
#define bin_no(__bc)\
(_32u_(__bc)>>4 > 31? 31+ (_32u_(__bc)>>9 > 31? 31+( (_32u_(__bc)>>14)&0x3 ) : _32u_(__bc)>>9  ) : _32u_(__bc)>>4)
#define bin_for(__pot,__bn)\
	((__pot)->bins+(__bn))
#define get_bin_for(__pot,__bn)\
	(*bin_for(__pot,__bn))
#define get_bin(__pot, __bc) \
  (*((__pot)->bins+bin_no(__bc)))
#define bin_at(__pot, __bc) \
  ((__pot)->bins+bin_no(__bc))

#define bin_bit_zero(__pot,__bn)\
	(__pot)->binmap[(__bn)>>3] &= ~(((_8_u)1)<<((__bn)&0x7));
#define bin_bit_one(__pot,__bn)\
	(__pot)->binmap[(__bn)>>3] |= (((_8_u)1)<<((__bn)&0x7));
#define bin_bit(__pot,__bn)\
	(((__pot)->binmap[(__bn)>>3]>>((__bn)&0x7))&1)
/*
  rename???? soup???? stew??
  POSIBLE:
    concoction
    cauldron
    soup
    stew
    boiler
    crucible
    cistern
    kiln
    pot

	NOTE:	the more bales in a pot the morelikly its going to get caught on the pot_lock(if threads)
	we try to cap it at 400'ish bales per pot max'ish
*/
#define UBIN_MAX 0
#define ubin_indx(__size)\
	((((__size)>>4)&0x3f))
#define ubin_bindx(__size)\
	(((__size)>>9)&0x3f)
#define ubin_aindx(__bx,__size)\
	__bx

#define lsconsistency(__pot,__blk)\
	((((_64_u)__pot)&~MASK_OF(LGRPOT_SIZE_SHIFT)) == (((_64_u)__blk)&~MASK_OF(LGRPOT_SIZE_SHIFT)))
/*
	NOTE:
		for the sizes of lesser pots we assume a cap on the size,
		meaning the size of a single bale shall never be larger then
		1/x of the total pot size, in this case 1/512 of 2^23 even if there is 
		we assume its not many to begin with.
*/
struct chain_struc {
	_64_u pad;//cauctions
	struct pot *fd, *bk;	
};
typedef struct pot {
	_64_u pad;//cauctions
	struct pot *fd, *bk;
	
	//TODO: remove??
	AR_INT_U  limit;
	//TODO: rename to bale_c or bcnt
  AR_INT_U  blk_c;

	/*
		starts from zero, pot structure is at off = POT_SIZE-sizeof(struct pot)
		this is because it ignores having to set off = sizeof(struct pot) 
	*/
 
	/*
 		the two things you see before you, are not unitable.

		if we where to use 'off' to get the end bale then we would need to know the size of the bale.
		if we set off the end_bale we could need the size of the bale to get to the end of the pot.

		so in all it makes no sence to unite these. for each allocation that not recycled, we check 
		if 'off'+'new bale size+header' > MAX_POT_SIZE. if we used the other method we would end up with

		if 'off'+'current bale size+header'+'new bale size+header' > MAX_POT_SIZE

		look its better to cache these things like this.

		but also end_bale might not always be the real end bale.
		meaning
		that 
	
		the bale located at off-(its size+header) might not always be equal to end_bale.

		/-----> top
		|
		|
		|
		|-----> end_bale
		|###### > remnants of bale
		|######
		\-----> 'off'
	*/
	AR_OFFSET off;
	AR_OFFSET end_bale;

	_32_u flags;
 
 	/*
		UBINS are a special type of bin. meaning they only contain one of there kind.
		meaning there cant be more then one bale with X size.

		decisive sorting
	
		the thing here is we want quick lookup but not too much memory for header.

		0000 0000 0000 XXXX - ignore(assume zero) 
		|_____||_____|
		|				|>>u_binsdwn[0-63]
		|>UBIN[0-63]
	
		NOTE:	ubins can steal from the main bins, to replenish somthing thats not there.
		meaning when we are searching for what we want, if the allocation was unsuccfull then
		we check if the ubins might want it.
	*/

	_64_u ubin_bits;
	AR_BALE_OFFSET ubins[64];
	/*
		u_binsdwn tells us if a given size is in a ubin link list
	*/
	_64_u u_binsdwn[64];
	/*
		we use an 16-bit int a max amount of bales in a POT would be 
		POT_REAL_SIZE/(BALE_SIZE+1) = 2500 ish?
	*/
	_32_u bincnt[no_bins];

	/*
		the first bale in linked list for bins
	*/
 	AR_BALE_OFFSET bins[no_bins];
	_8_u binmap[16];
 //TODO: remove some of these
 struct pot *next;
  struct pot *previous;
  AR_INT_U buried;
	mlock lock;
	mlock inuse_lock;
	_32_u lot;
	/*
		this is the mark for what heap this pot is within,
		NOTE: cant be used for for greater pots, why?

		because this means in each heap we will have to have X amount pre allocated so
		we can check.

		are_ptr
		heap_ptr

		(are_ptr-heap_ptr)>>POT_SIZE_SHIFT = offset in pots

		# this would need preallocation
		heap->array[offset in pots]

		however this wont work.
		why?

		because in the larger pot we dish out pointers within that larger pot.
		this means inorder to know what heap we are in we would need to store this in 
		the bale itself.

		as we wont have access to the POT. until we know if its a greater or lesser POT.
		as if we align by lesser POT size and get the pot from there it will be invalid
		as its memory thats dished out by the greater pot.

		so to get the POT structure from anywhere within a greater POT we need to know 
		if its a greater or lesser POT.
	*/
	_32_u heap;
	_32_u underneath;
	_32_u gr;
	/*
		used to describe past and present states.
		used in debug to help understand what operations the pot might of undergone
		to get where it is now.
	*/
	_16_u tags[16];
	_16_u tag_ptr;
	_64_u valid_tags;
	//the arena that accommodates this pot
	_32_u acm;

	_32_u b2;
	/* this works because only one may manage a pot.
		okay atomics are slow, so lets do this,
		_adjcent_flags[0] = flags from below pot
		_adjcent_flags[1] = flags from above pot
	*/
	_16_u _adjcent_flags[2];
	_16_u anti_mask;
	/*
		this data is apart its surrounding  pots;
	*/
	struct {
		/*
			this lock is for the pot below us(its there lock not ares)
			we dont init this data, or overwrite and only though certain 
			proceedures
		*/
		mlock lock;
	//appendage
	} apd;

	_32_u tan;
	_16_u real_flags;
}*potp;

/*
	do we have jurisdiction over a POT? is the pot under are control
	or does someone else?
*/
#define has_jurisdiction(__pot) ((__pot)->acm == TLS->arena)
//halfling
#define above_flags(__pot) (__pot)->_adjcent_flags[1]
#define below_flags(__pot) (__pot)->_adjcent_flags[0]
#define pot_newtag(__pot) {(__pot)->tag_ptr = (((__pot)->tag_ptr+1)&0xf);(__pot)->tags[(__pot)->tag_ptr] = 0;(__pot)->valid_tags |= (1<<(__pot)->tag_ptr);}
#define pot_curtag(__pot) (__pot)->tags[(__pot)->tag_ptr]
#define off2ptr(__pot,__off)\
	((_64u_(__pot)&~(_64_u)POT_MASK)|(__off))
#define gtoff2ptr(__pot,__off)\
	((_64u_(__pot)&~(_64_u)LGRPOT_MASK)|(__off))
#define abpot(p) ((potp)(_64u_(p)-POT_SIZE))
#define blpot(p) ((potp)(_64u_(p)+POT_SIZE))

#define abgpot(p) ((potp)(_64u_(p)-LGRPOT_SIZE))
#define blgpot(p) ((potp)(_64u_(p)+LGRPOT_SIZE))
#define ab0pot(p) ((potp)((_64u_(p)-1)^sizeof(struct pot)))

//higher order bits
#define hob(__pot)\
	(_64u_(__pot)&~(_64_u)POT_MASK)
#define lot_table(__x) _ar_ms.lot_table[__x]
#define get_lot(__x) (_ar_ms.lots+_ar_ms.lot_table[__x])
/*
	OLD consept RODs
*/
/*
  dont use pointers when we can use offsets to where the bale is located
  as it will take of less space in the header.
*/
/*
  keep packed as we want the header to be as small as possible,
  if any alignement is needed we will do it are selfs as having the compiler do
  it might cause irregularity and inconsistencys
*/

/*
  is was called block before name has changes to 'bale' in haybale
*/
typedef struct bale {
  /*
    we may see that im using offsets and not pure pointers,
    look i had a bad experance with pointers and so it ended up like this.
  	TODO:
		allow for bales to be tagged/marked
	*/
  AR_OFFSET prev;
  AR_OFFSET next;
  AR_OFFSET fd;
  AR_OFFSET bk;

	/*
		are bale size is 37-bytes,
		we have 3-bytes free.

		why not just keep it small? glibc-malloc has a 40-byte chunk header size.
		as we are using offset and not pointers(like malloc) we have excess space. (why not make use of it?)
		if glibc-malloc has a 40-byte we will cap are selfs at that as it works for this why not for us?

		bale_size = 6*AR_OFFSET + 1*_64_u + 1*AR_SIZE + 1
		bale_size = 37-bytes

		meaning we have 40-37 of excess space compared to malloc.
		meaning 3-bytes, round to pow of 4 then we have 8-bytes or 1 32-bit int free for us to use
		'pad' is here as well, this is because pad is only used on non-free/in-use bales
		meaning if the bale is free then its going unused.

	*/
	union {
		struct {
			AR_OFFSET front,rear;
			_64_u other;
		};
		struct {
			/*
				junk part of bale that wont be used.
				but can be used.

				zeroed when freed.
			*/
			//the remnant part
			AR_INT_U pad;//surplus/appendage
		};
	};
  AR_SIZE size;//total size, without bale header
	_16_u flags;
} __attribute__((packed)) *balep;

#define pot_size sizeof(struct pot)
#define bale_size sizeof(struct bale)

#define lot_index(__lot) ((__lot)-_ar_ms.lots)

#define LGRPOT_MASK (LGRPOT_SIZE-1)
#define LGRPOT_REAL_SIZE (LGRPOT_MASK^sizeof(struct pot))
#define LGRPOT_SIZE_SHIFT 26
#define LGRPOT_SIZE (1<<LGRPOT_SIZE_SHIFT)
#define LGRPOT_QSIZE (LGRPOT_SIZE>>2) 
#define LGRPOT_ILVBITS (MASK_OF(LGRPOT_SIZE_SHIFT)^MASK_OF(POT_SIZE_SHIFT))
#define potbinno_for(__size) 0
//	((__size)>=(POT_SIZE*16)?1:0)

/*
	the issue we face; if two adjcent pots are free we want to collect them under one thing.
	
	a large linear range.

	the issue is that the pot that contains the infomation, like the lot structure does. if its gets taken
	for use, we are going to have to copy the data. but also we would have to maintain two sets of this data
	at both the top pot and bottom pot.

	one issue is we dont have access to some common structure for the large linear extent(from the brk system call or other).
	this means we cant maintain a chain of pots like we do with bales as we wont be able to determan and keep track of the bottom and top pots.

	however pots a very large so there wont be many of them around, meaning copying some index for a structure that incompasses the linear range that is
	free is a possible idea.

	using this we wont need to copy the data and we can maintain two sets without duplicating the data.
	we also wont be using pointers as that is slow.

	if we have the top of a piece of memory we are better off using offsets to spots on that memory then using pointers.
	we only use pointers when this option is not viable.
*/

/*
	why is this done like this? if we desided to use a list approch
	we would face issues regarding direction(expanding from below or above) but also lists are slow.

	why is direction an issue in lists?
	- if means we have to change the base pointer of the list.

	this has its downsides however list are more expansive,
	the downside to this approch is that if we collide with another lot
	that has more then a single span then we would have to copy over them using an slow routine involving a loop.
	however its more cost saving when we deal with single lot collisions. the only expensive thing here is the 
	defer pointers('n_defer' and 'next') and having to increment/dec them. upside to this is these values can be stored in
	registers so making it overall the best option other them lists.
*/
#define LOT_LEECH 1
#define LOT_BOOKED 2
struct lot{
	/*
		NOTE:
			nowhere do we store any pointer to the edges of the lot.
	*/
	_64_u size_in_pots;
	_8_s issued;
	_int_u isucnt;
	mlock lock;
	_16_u flags;
	_16_u bcs_idx;
	potp leech;

	_32_u acm;
	potp pot;
	struct lot *next_free,**prev_next_free;
};

/* TODO:
struct heap {
  void *top;
  void *end;
  _64_u off;
  _64_u limit;
};*/
#define to_lpts(__x)\
	((__x)<<POT_SIZE_SHIFT)
#define in_lpts(__x)\
	((__x)>>POT_SIZE_SHIFT)
#define spanlgn(__lot)\
	_ar_ms.span_lgn[__lot]
#define get_sprout(__b) ((potp)((((_64_u)__b)|MASK_OF(LGRPOT_SIZE_SHIFT))-sizeof(struct pot)-sizeof(struct sprout)))
struct sprout {
	_32_u bits;
	potp p;
	_32_u finalize;
	mlock lock;
	_32_u acm;
	_32_u spr_idx;
};
#define SPROUT_MAX_INDX (8*8)
#define SPROUT_MAX 4
/*
	TODO: if the arena has ecess of lets say pots then sould be placed in global space to other arenas can snatch up.
*/
struct func_stamp{
	char const *str;
};

/*
	this structure is apart of the arena, its not contained in a pot
	is pointed to. this is because not all pots need this structure.
*/
struct bin_bay_dish{
	_32_u bins[no_bins];
	_32_u cnt;
};

struct bin_bay{
	potp pots[32];
	_32_u dish[32];
	_int_u n;
};
/*
	make size of a potfit should be 8*POT_SIZE
*/
struct furnish {
	_32_u pot[16];//16 just in case
};

struct fitpot{
	struct furnish furn;
	struct pot pot;
};

/*
  like its name, we are sieveing the bales.
  grouping them by lower order bits.

  so for every pow of 2^12 we group the lower order of 6-bits
  into one 2^12


  2^12 + 0*2^6
  2^12 + 1*2^6
  ...
  ...
  2^12 + 63*2^6

  all come under one binned bale.
  
  so we therfor have a lack of bales in the bins
  with the lower order bits.(faster lookup times)
*/
struct sieve_struc{
	_int_u sieve_reset;
	_int_u sieve_replace[64];
	AR_BALE_OFFSET sieve_table[512];
};
#define STAG_SIZE 4
//REMINDER: swap out '8' for '16' to remove assert failure of non null
#define subsid(an,lr,i) (an)->subsidiary_frees[(lr*16)+i] 
/*
	a arena has its own private data, however sometimes we may want arenas to use the same free data.
	this would mean dedicating an whole arena to that, or we can just do this.
*/
struct accommodation {
	/*
		TODO:
			have many free lists.
			this would improve threading as it will mean we wont need to wait for them to remove from list
	*/
	struct lot*_free_pots[16];
	mlock free_pots_lock;	

	/*
		look if the number of bookedcased lots exceed a set amount it means that,
		greater pots are very spread out(BAD!)
	*/
#define BOOKCASE_SIZE 4
	struct lot* bookcase[BOOKCASE_SIZE];
	_32_u bc_free_x[BOOKCASE_SIZE];
	_32_u bc_free[BOOKCASE_SIZE];
	_int_u bcs_n;
	mlock bookcase_lock;	

	// greater pot alignment critear
	_64_u gpac_miss;
	_64_u gb_insufficient;
	_64_u outlanders;
};

struct arena {
	mlock debug_lock;
	_64_u subsidiary_frees_cnt[8];
	_64_u subsidiary_drains_cnt[8];

	/*
	 0-1,			#0
	 2-3,			#1
	 4-7,			#2
	 8-15,		#3
	 16-31		#4
	 32-63,		#5
	 64-127		#6
	 (and so on) -> #13
	*/
	balep frontbale[14];
	/*
		if a front line bale slot needs stocking(like a shelf at convenience store)
	*/
	_32_u stock[14];
	_int_u next_stock;
	mlock stock_lock;
	/*
		before accessing arena all shunted bale must be taken are of.
	*/
	balep shunted_bale;
	/*
		what we are going to try to do is accumulate then free.
		meaning if another person owns the arena then collect 
		free bales for that arena in are arena then when we finaly get the 
		lock for the other arena flush everything we have.
	
		width is 16wide
	*/
	void *subsidiary_frees[8*16];
	_int_u subf_cnt[8];
	_32_u subf_n;
	_32_u subfs[8];
	_32_u subfsc[8];
	mlock subf_lock;

	struct func_stamp *stamp;

	struct bin_bay bays[no_bins];

	_32_u free_dishes[8*no_bins];
	_32_u next_dish;

	_32_u flags;

	potp top;
	/*
		the accumulation of count of number of bales that fit criteria of there corresponding bin

		this gives us an full picture of whats available for us, without checking each POT
	
		NOTE: nothing to do with whats below this
	*/
	_32_u bincnt[no_bins];

	/*
		surface no linked
	
		pots with highest count
	*/
	potp bins[no_bins*8];
	/*
		the POT with the largest non-used segment(POT_REAL_SIZE-pot->off).	
		the pot with the smallest offset = largest chasm
	
		00 = off<2^13
		01 = off>1*2^13
		02 = off>2*2^13
		..
	*/
	potp largest_chasm[8];
	potp grs_partly[4];
	_int_u grsp_cnt;

	/*
		okay after a bit of thinking, we come up with this.


		if we happen when a pot_free is called the pot in question is greater pot aligned,
		we mark it and dont free, and we let adjcent pots clump them selfs together to this pot.
		this means if a a adjcent pot is freed we join areselfs to it, then this means that the adjcent of that pot
		can join its self to that pot and so one.

		its this method or, we brand each pot with a number associated with the greater pot its within.
		this means we will have to set an int in the pot, and greater pot id for lesser pots.

		this method would be slower, as it means for each pot we would need to keep track of the greater pot we are within.
		another means of doing this is to have the base pointer for the memory map at hand and subtract it from the pot pointer
		and then do some bit magic to get the greater pot number, however this as its own problems.
	*/

	struct sprout *spr_inuse[SPROUT_MAX];
	_32_u spr_next;

	/*
		if the sprout is complete then a referance to it will be placed here,
		the meaning behind it is because we would have to resetup a sprout if a pot
		where to be removed leaving it incomplete. but also the sprout structure should #
		realy be let-go by the one whom created it.
	*/
	struct sprout *spr_complete[SPROUT_MAX];
	_int_u spr_cnt;
	mlock spr_lock;

	_32_u grp[64];
	_32_u grp_free[64];
	_32_u grp_next;

	_32_u take[64];
  _int_u next_take;
  _32_u table_take[64];
  _int_u next_table;

	struct pot *potbin[2];
	mlock potbin_lock;

	mlock access_lock;

	struct sieve_struc sieve;
	potp stagnation[STAG_SIZE];
	_int_u stg_n;
	struct accommodation *acm;

	_32_u lpsal_miss;
	_32_u gpsal_miss;
	_32_u gpbc_miss;
	_64_u lots_acum;
};
#define more_uptr ((void*)(_64u_(_ar_ms.end)+_ar_ms.off))
/*
	one tihing to be aware of down here is that
	we have large arrays of sprouts,dishes and such.
	this is because having them contained within the arena
	means we cant give to those in need. what i means is if an arena
	is not using its share we can give it to another.

	if it was contained in an arena then we would need to IDs the arena and the index of are data(here only one is needed).
*/
#define linkidx(__x) ((in_lpts(__x)>15)?0:in_lpts(__x))
struct main_struc {
  void *top;
  void *end;
  _64_u off;
  _64_u limit;
	mlock more_lock;
	potp indulge;
	mlock arena_alloc_lock;
	mlock lock;
	_64_u subsidiary_frees;
	_64_u subsidiary_flunks;
	_64_u subsidiary_drains;
	_64_u subsidiary_drains_by_outlander;
	_64_u subsidiary_frees_by_outlander;

	_64_u sfb_hits[no_bins];
	_64_u sfb_misses[no_bins];
	_64_u sfb_deadends[no_bins];

	_64_u cspr_harvests;
	_64_u bin_hits;
	_64_u bin_misses;

	_64_u pot_hits;
	_64_u pot_misses;
	
	_64_u pot_alloced;
	_64_u pot_freed;

	_64_u lpot_alloced;
	_64_u lpot_freed;
	_64_u lpot_peak;

	_64_u abc_zero_case;
	_64_u abc_hits;
	_64_u abc_misses;

	_64_u pot_peak;
	_64_u poted_bales_alloced;
	_64_u poted_bales_freed;

	_64_u bin_hits_by_bin[no_bins];
	_64_u bin_misses_by_bin[no_bins];
	_64_u bin_deadends_by_bin[no_bins];

	_64_u abc_hits_by_bin[no_bins];
	_64_u abc_misses_by_bin[no_bins];
	_64_u abc_deadends_by_bin[no_bins];

	_64_u sizesort_hits;
	_64_u ubin_hits;
	_64_u ubin_misses;
	_64_u ubin_snaffles;
	_64_u ubin_scrubed_snaffles;
	_64_u chasm_hits;
	_64_u chasm_misses;

	_64_u usage_peak;

	_64_u gyre_steps;

	//another word for clump
	_64_u gnarl_hits;
	_64_u gnarl_misses;
	_64_u gnarl_formed;

	_64_u sieve_rinses;
	_64_u rinse_peak;
	_64_u gnarl_complete_cycles;

	struct lot lots[512];
	/*
		this table allows us not to need to traverse the pots inorder to update the 'lot'
	*/
	//FIXME: use 16_bit
	_32_u lot_table[512];
	_64_u span_lgn[512];
	struct arena arenas[8];
	_32_u free_arenas[8];
	_int_u arena_next;

	struct bin_bay_dish dishes[8*no_bins*8];
	_32_u spill_lot_strc[64];
	_32_u spill_lot_table[64];
	mlock spill_lock_strc;
	mlock spill_lock_table;
	_32_u spill_lsn;
	_32_u spill_ltn;
	struct accommodation acm[8];
};
#define acm_at(__x) (_ar_ms.acm+(__x))
#define acm_idx(__acm) ((__acm)-_ar_ms.acm)
//#define public_domain_acm (_ar_ms.acm+8)

#define get_potrel(__p,__x) ((potp)(_64u_(__p)-((__x)<<POT_SIZE_SHIFT)))
#define arena_distinct TLS->arena
#define arena_at(__x) (_ar_ms.arenas+__x)
#define myarena arena_at(TLS->arena)
#define TLS ((struct tls_storage*)proc_ctx_fetch()->ar)
struct tls_storage {
	_32_u arena;
  _32_u real;
	potp tmp;
};
//bale two user pointer
#define bale2up(__bale)\
	((void*)(((_8_u*)__bale)+bale_size))
#define up2bale(__ptr)\
	((balep)(((_8_u*)__ptr)-bale_size))



#define more_lock()			mt_lock(&_ar_ms.more_lock)
#define more_unlock()		mt_unlock(&_ar_ms.more_lock)

#define MESG_FILTER (ARMSG_POT|ARMSG_TBD|ARMSG_LV2|ARMSG_LVK)
#define NO_PRINT



void _ar_printf(char const*,...);
#ifdef NO_PRINT
#define ar_printf(...)
#define AR_MESG(...)
#else
#define ar_printf _ar_printf
#define AR_MESG(...) _ar_mesg(__VA_ARGS__);
#endif
#define cur_arena TLS->p
extern struct main_struc _ar_ms;
void ar_abort(void);
_64_u _ar_dead(potp);
_64_u _ar_used(potp);
_64_u _ar_buried(potp);
void* _ar_sysmore(_64_u);
void _ar_systrim(_64_u);
_int_u ar_printfit(char *buf,char const *__format, ...);
void ar_printfb(char *buf,_int_u len);
#define STRNG(__s) #__s
#define _STRNG(__s) STRNG(__s)
#define ar_assert(__expr,...)\
  if(!(__expr)){_ar_printf("assert failure at" __FILE__ " : line-" _STRNG(__LINE__) " message: " __VA_ARGS__);ar_abort();}

#endif/*__ar__internal__h*/
