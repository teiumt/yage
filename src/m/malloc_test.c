#include "../y_int.h"
#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<assert.h>
#include<string.h>
#include<time.h>
FILE *graph;
void doplot(_32_u __x, _32_u __y) {
	fprintf(graph,"%u %u\n",__x,__y);
}
#define apply_errorcorrection(__delta)\
	__delta -= __delta<40?__delta:40;
static _int_u cnt = 0;
void static* do_malloc(_int_u __size){
  struct timespec ts,te;
  void *r;
  clock_gettime(CLOCK_MONOTONIC,&ts);
	r = malloc(__size);
  clock_gettime(CLOCK_MONOTONIC,&te);

	_64_u delta;
  delta = (te.tv_sec-ts.tv_sec)*1000000000+(te.tv_nsec-ts.tv_nsec);
  apply_errorcorrection(delta);
	doplot(cnt++,delta);
  return r;
}

void static do_free(void *__ptr){
  struct timespec ts,te;
  clock_gettime(CLOCK_MONOTONIC,&ts);
	free(__ptr);
  clock_gettime(CLOCK_MONOTONIC,&te);
 	_64_u delta;
  delta = (te.tv_sec-ts.tv_sec)*1000000000+(te.tv_nsec-ts.tv_nsec);
  apply_errorcorrection(delta);
	doplot(cnt++,delta);
}

void graphp(void){
	graph = fopen("graph.plt","w");
}

void graph_finish(void){
	fclose(graph);
}
#include "../rand.h"
#define mem_set memset
#define m_free free
#define m_alloc malloc
#include "test0.c"

int main() {
	graphp();
	ar_test();
	graph_finish();
}
