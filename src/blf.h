#ifndef __blf__h
#define __blf__h
#include "header.h"
#define blf_hdrsz sizeof(struct blf_hdr)
/*
	primaryly used for crossfile linking
	source file linking done thrugh wa structs
	done by name(hash lookup) otherwise
*/
#define BLF_NULL ~0
/*
	put into local domain
*/
#define BLF_SYB_D_LOCL 0
/*
	put into globlal domain
*/
#define BLF_SYB_D_GLBL 1


#define BLF_SYB_D_BSPACE 2
#define BLF_SYB_D_NULL 45
/*
	undetermand/unresolved 
*/
#define BLF_SYB_UD  2
#define BLF_SYB_SECT	4
struct blf_smdata {
	QWORD w;
	QWORD domain;
	QWORD flags;
	QWORD sec;
	QWORD value;
/*
	i think haveing this like this is more efficent.. maybe?
	jk is is better i hope so.
	why?
	because it means no multiply instruction and is just a defanate offset/displacment
*/
	/*
		real addressing
	*/
	QWORD f_start;
	QWORD f_end;

	/*
		index addressing
	*/
	QWORD f_is;
	QWORD f_ie;

//span = f_end-f_start?
/*
	why is this here? so we can copy symbols to diffrent locations
*/
	QWORD x;
};

struct blf_sym {
	QWORD name;
	QWORD value;
	QWORD n;
};

/*
	labels/syms

	test:		<- this would be a WA
		ret

	test:
	.L0		=	test+1
	.L1		=	test+2
	.L2		=	test+3
		ret
*/
struct blf_wa {
/*
	where in fragment are be located
*/
	QWORD adr;
};

struct blf_frag {
//real addressing
/*
	where is this fragment within the memory map
	a region where all progbits are stored
*/
	QWORD off;
	QWORD size;
//in exact words
	QWORD adr;
/*
	FRAGS contain real data 
	the only bits that need looking at is nov/over
*/
	QWORD words;
	QWORD over;
	QWORD nov;
//counterpart
//ther wa strucs that are using this tang as a pivot	
	QWORD wa;
	WORD nw;	
};

#define BLF_R_UD 2
#define BLF_R_PART 4
#define BLF_PIC_R_JMP	0
#define BLF_PIC_R_CALL	1
#define BLF_PIC_R_BSPACE 2
#define BLF_PIC_R_DK		3
#define BLF_R_8		8
#define BLF_R_16	16
#define BLF_R_32	32
#define BLF_R_64	64

struct blf_reloc {
	WORD value;
	QWORD f;
	union {
		QWORD w;
		QWORD sm;
	};
	QWORD w0;
	QWORD flags;
	QWORD where;
	_32_u dis;
};

/*
	next = offset+sizeof(struct blf_stent)+len
*/
struct blf_stent {
	HWORD len;
} __attribute__((packed));


struct blf_section {
	QWORD idx;
	//frag table
	QWORD ft;
	QWORD nfr;

	//wa table
	QWORD wat;
	QWORD nw;
	
	//reloc table
	QWORD rl;
	QWORD nr;
};

#define BLF_M_PIC 0
#define BLF_M_PIC_EM 1
struct blf_hdr {
	struct f_fh h;
	//machinery idk
	QWORD mech;

	QWORD syt, sytsz;
	QWORD nsy;

	//section table
	QWORD sec;
	QWORD nsec;

	//frag table
	QWORD ft;
	QWORD ftsz;
	QWORD nfr;

	//wa table
	QWORD wat;
	QWORD wsz;
	QWORD nw;
	
	//reloc table
	QWORD rl;
	QWORD rsz;
	QWORD nr;

	QWORD start;
	QWORD size;
	
	//string table
	QWORD st_ents;
	QWORD st_n;
	QWORD st_size;

	QWORD smd;
	QWORD smdn;

	QWORD w_m;
	QWORD entry;
} __attribute__((aligned(FH_BS)));
#endif /*__blf__h*/
