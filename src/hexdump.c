#include "tools.h"
#include "io.h"
#include "string.h"
#include "m_alloc.h"
#define ROW_SHIFT 3
#define ROW_LEN (1<<ROW_SHIFT)
#define ROW_MASK (ROW_LEN-1)
static _int_u tohex(char *__out, _8_u __val) {
	_int_u i;
	i = 0;
	_8_u val;
	char *o = __out+1;
	char c;
	for(;i != 2;i++) {
		val = __val&0x0f;
		if (val>=0 && val<10)
			c = '0'+val;
		else if (val>=10 && val<=15)
			c = 'a'+(val-10);
		*(o--) = c;
		__val>>=4;
	}
	return 2;
}

_int_u static
hexdump(_8_u *__dst, _8_u *__src, _int_u __size) {
	_8_u *s = __src, *d = __dst;
	_8_u *e = s+(__size&~ROW_MASK);
	while(s != e) {
		_8_u i;
		i = 0;
		for(;i != ROW_LEN;i++) {
			d+=tohex(d, s[i]);
			*(_16_u*)d = 0x92c;
			d+=2;
		}
		s+=ROW_LEN;
		*(d++) = '\n';
	}

	_int_u left;
	left = __size&ROW_MASK;
	if (left>0) {
		e = s+left;
		while(s != e) {
			d+=tohex(d, *(s++));
			*(_16_u*)d = 0x92c;
			d+=2;
		}
		*(d++) = '\n';
	}
	return (d-__dst);
}

void f_hexdump(struct f_iis *__iss, _8_u *__p, _int_u __size) {
	_8_u *d;
	_int_u d_size;
	/*
		rough estimate of how much to allocate to make sure we stay within 
		memory area

		d_size = (number of rows(rounded up))*33
	*/
	d_size = ((__size+ROW_MASK)>>ROW_SHIFT)*((ROW_LEN*4)+1);

	d = (_8_u*)m_alloc(d_size);
	_int_u l;
	l = hexdump(d, __p, __size);

	__iss->out(d, l);
	m_free(d);

}

void ffly_hexdump(_8_u *__p, _int_u __size) {
	f_hexdump(&_f_out, __p, __size);
}
