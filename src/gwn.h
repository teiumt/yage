# ifndef __f__gwn__h
# define __f__gwn__h
# include "y_int.h"
# include "rws.h"
# include "dem.h"
/*
	ground work node


	to help us unify some crap
	example:
	hexdump

	example

	lets say we want to hex dump a file into another file??
	so the user will need to go thrugh a lenthy process to get there right??
	so its as easy as setting gwn_src, and dst to a file io(RWS)
	src: file0, dst: file1:



*/
struct f_gwn_io_rws {
	struct f_rws_struc *rws;
};

struct f_gwn_stat {
	_int_u size;
};
struct f_gwn_io {
	void(*io)(_ulonglong, void*, _int_u, _64_u);
	_ulonglong arg;
	void(*stat)(_ulonglong, struct f_gwn_stat*);
};
struct f_gwn {
	union {
		struct {
			struct f_gwn_io *src, *dst;
			union {
				struct f_dem_gwn *dem;
			};
		};
	}
};

#define F_GWN_IO_RWS_IN 0x00
#define F_GWN_IO_RWS_OUT 0x01
void f_gwn_io(_8_u, struct f_gwn_io*, void*);
# endif /*__f__gwn__h*/
