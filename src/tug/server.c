#include "tug.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../types.h"
_y_err main(int __argc, char const **__argv) {
	struct tug_conn con;
	if(tug_connect(&con) == -1) {
		printf("failed to connect.\n");
		return -1;
	}
	tug_establish(&con,1);
	tug_hello(&con);
	_8_u buf[128];
	struct tug_pkt pkt;
	pkt.to = 0;
	pkt.from = 1;
	pkt.size = 8;
	_64_u *blank = buf;
	*blank = 21299;
	tug_pktsnd(&con,&pkt,buf);

	tug_pktrcv(&con,&pkt,buf);
	printf("blank: %x. from: %u, to: %u, size: %u\n",*blank,pkt.from,pkt.to,pkt.size);

	tug_lullaby(&con);
	tug_disconnect(&con);
}
