#ifndef __tug__h
#define __tug__h
#include "../y_int.h"
# include "../linux/in.h"
#include "../linux/un.h"

#define TUG_CM_SERVER 0
#define TUG_CM_CLIENT0 1
#define TUG_CM_CLIENT1 2


#define TUG_OUT		fd0[1]
#define TUG_IN		fd[0]
enum tug_msg_type {
	TUG_INLET,
	TUG_OUTLET,
	TUG_HELLO,
	TUG_LULLABY,
	TUG_DONE,
/*
	exec code
*/
	TUG_EXEC
};
struct tug_pkt {
	_int_u from;
	_int_u to;
	_int_u size;
};

struct tug_msghdr {
	_int_u type;
	_int_u size;
	_64_u bn;
	_int_u in,out;
};

struct tug_bay {
	int fd_in,fd_out;	
	_int_u tx;
	_8_s lullabyed;
	struct tug_bay *next, **bk;
};
struct tug_conn {
/*
	fd0 = read buffer
	fd1 = write buffer
*/
	int u_fd, fd[2],fd0[2];
	struct sockaddr_un u_adr;
	_64_u bay;
	_int_u tx;
};
void tug_pktrcv(struct tug_conn*,struct tug_pkt*,void*);
void tug_pktsnd(struct tug_conn*,struct tug_pkt*,void*);

void tug_pkt_sndto(struct tug_conn*,_32_u,void*,_32_u);


int tug_msgsnd(struct tug_msghdr*,int,int);
int tug_msgrcv(struct tug_msghdr*,int,int*);
int tug_read(int,void*,_int_u);
int tug_write(int,void*,_int_u);
//client.c
_8_s tug_poll(struct tug_conn *__con);
int tug_hello(struct tug_conn *__conn);
int tug_lullaby(struct tug_conn *__con);
int tug_connect(struct tug_conn *__conn);
int tug_disconnect(struct tug_conn*);
void tug_establish(struct tug_conn *__conn,_64_u);
void tug_start(void);
#endif/*__tug__h*/
