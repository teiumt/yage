# include "copy.h"
# include "y_int.h"
# include "system/error.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
# include "system/io.h"
# define CHUNK_SHIFT 8//256
# define CHUNK_SIZE (1<<CHUNK_SHIFT)
# include "msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_NOWHERE)
/*
	NOTE:
		change chunk size depending on total size or not	
		and also should map file using mmap and copy?????? and sync ??? 
		or ioctl operation ioctl_file_clone??


	also realloc the file
*/
_f_err_t
ff_fcopy(char const *__src, char const *__dst) {
	MSG(INFO, "fcopy %s, %s.\n", __src, __dst);
	_8_u *buf = (_8_u*)__f_mem_alloc(CHUNK_SIZE);

	struct stat st;
	int s, d;

	if ((s = open(__src, O_RDONLY, 0)) == -1) {
		ffly_fprintf(ffly_err, "failed to open file: %s\n", __dst);
		reterr;
	}

	if ((d = open(__dst, O_WRONLY|O_CREAT|O_TRUNC,
		S_IRUSR|S_IWUSR)) == -1) {
		ffly_fprintf(ffly_err, "failed to open file: %s\n", __dst);
		close(s);
		reterr;
	}
	
	fstat(s, &st);

	_int_u cc = st.st_size>>CHUNK_SHIFT;
	_int_u left = st.st_size-(cc*CHUNK_SIZE);
	if (left>0) {
		read(s, buf, left);
		write(d, buf, left);
	}
_again:
	if (cc>0) {
		read(s, buf, CHUNK_SIZE);
		write(d, buf, CHUNK_SIZE);
		goto _again;
	}

	close(s);
	close(d);
	__f_mem_free(buf);
}
