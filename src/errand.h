#ifndef __y__errand
#define __y__errand
#include "cradle.h"
struct y_errand {
	void(*func)(struct y_errand*);
	void *priv;
};

void y_errand_submit(struct y_errand*);
void y_errand_tick(struct pcore*);
#endif/*__y__errand*/
