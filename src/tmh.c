# include "tmh.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "memory/mem_realloc.h"
# include "dep/mem_set.h"
# include "cradle.h"
# include "system/io.h"
#define TMH F_CRADLE(tmh)
void f_tmh_file(f_tmh_opsp);
f_tmh_areap static
_map(f_tmh_ctxp __ctx, _int_u __size) {
	f_tmh_areap area;
	area = (f_tmh_areap)__f_mem_alloc(sizeof(struct f_tmh_area));
	area->size = __size;
	area->priv = __ctx->c->ops.map(__ctx->priv, __size);
	return area;
}

void static
_initpage(f_tmh_recp __recs) {
	_int_u i;
	i = 0;
	f_tmh_recp r;
	for(;i != TMH_PAGE_SIZE;i++) {
		r = __recs+i;
		r->flags = F_TMH_NMY;
	}
}

f_tmh_recp static
_getrec(f_tmh_ctxp __ctx, _int_u __idx) {
	_int_u pg, pg_off;
	pg = __idx>>TMH_PAGE_SHFT;
	pg_off = __idx&TMH_PAGE_MASK;

	f_tmh_ptp pt = __ctx->pt;
	if (pg>=pt->page_c) {
		_int_u _pg = pt->page_c;
		pt->page_c = pg+1;
		pt->records = (f_tmh_recp*)__f_mem_realloc(pt->records, pt->page_c*sizeof(f_tmh_recp));
		while(_pg != pt->page_c) {
			_initpage(*(pt->records+_pg) = (f_tmh_recp)__f_mem_alloc(TMH_PAGE_SIZE*sizeof(struct f_tmh_rec)));
			_pg++;
		}
	}

	return pt->records[pg]+pg_off;
}

static void _rec_new(f_tmh_capp __c, void *__priv, struct f_tmh_recinfo *__ri) {
	f_tmh_recp r;
	r = _getrec(__c->ctx, __ri->idx);

	r->area = (f_tmh_areap)__f_mem_alloc(sizeof(struct f_tmh_area));
	r->area->priv = __ri->a_priv;
	r->area->size = __ri->a_size;
	r->flags |= F_TMH_RVIS;
	r->flags &= ~F_TMH_NMY;
	r->priv = __priv;
}

void static rec_new(f_tmh_capp __c, f_tmh_recp __r, _int_u __idx) {
	void *priv;
	priv = __c->ops.rec_new(__c->ctx->priv, __idx);
	__r->priv = priv;
	__r->flags |= F_TMH_RVIS;
	__r->area = NULL;
}

static struct f_tmh_bg bg = {
	.rec_new = _rec_new
};

void static
prep(void) {
	TMH.c.ctx = (f_tmh_ctxp)__f_mem_alloc(sizeof(struct f_tmh_ctx));
	TMH.c.ctx->c = &TMH.c;
	f_tmh_file(&TMH.c.ops);
	TMH.c.ctx->priv = TMH.c.ops.ctx_new(&TMH.c);
	TMH.c.ctx->pt = (f_tmh_ptp)__f_mem_alloc(sizeof(struct f_tmh_pt));
}

void f_tmh_init(void) {
	TMH.c.bg = &bg;
	prep();

	f_tmh_ptp pt = TMH.c.ctx->pt;
	pt->page_c = 1;
	pt->records = (f_tmh_recp*)__f_mem_alloc(sizeof(f_tmh_recp));
	_initpage(*pt->records = (f_tmh_recp)__f_mem_alloc(TMH_PAGE_SIZE*sizeof(struct f_tmh_rec)));
	TMH.c.ops.init(TMH.c.ctx->priv);

	TMH.c.ops.load(TMH.c.ctx->priv);
}

void f_tmh_de_init(void) {
	TMH.c.ops.test(TMH.c.ctx->priv);

	TMH.c.ops.save(TMH.c.ctx->priv);
	TMH.c.ops.deinit(TMH.c.ctx->priv);

	f_tmh_ptp pt = TMH.c.ctx->pt;

	f_tmh_recp *pg, *pg_e;
	pg = pt->records;
	pg_e = pg+pt->page_c;
	while(pg != pg_e) {
		__f_mem_free(*pg);
		pg++;
	}
	__f_mem_free(pt->records);
	__f_mem_free(pt);
}

void f_tmh_recat(f_tmh_agp __ag, _int_u __n) {
	f_tmh_agp ag;
	_int_u i;
	i = 0;
	f_tmh_recp r;
	for(;i != __n;i++) {
		ag = __ag+i;
		r = _getrec(TMH.c.ctx, ag->idx);
		if (!(r->flags&F_TMH_RVIS)) {
			/*
				error
			*/
			ag->a = NULL;
		} else
			ag->a = r->area;
	}
}

void f_tmh_add(f_tmh_agp __ag, _int_u __n) {
	_int_u i;
	i = 0;
	f_tmh_agp ag;
	f_tmh_recp r;
	for(;i != __n;i++) {
		ag = __ag+i;
		r = _getrec(TMH.c.ctx, ag->idx);
		if (!(r->flags&F_TMH_RVIS)) {
			rec_new(&TMH.c, r, ag->idx);
		}
		ag->a = r->area;
	}

}

f_tmh_wokp f_tmh_open(_64_u __id) {
	f_tmh_wokp w;
	w = (f_tmh_wokp)__f_mem_alloc(sizeof(struct f_tmh_wok));
	w->flags = 0x00;
	f_tmh_recp r;
	r = _getrec(TMH.c.ctx, __id);
	/*
		we may have the record but to the lower level stuff they have no idear this record 
		exists so we have to tell them about it so its saves to disk.
	*/
	if (!(r->flags&F_TMH_RVIS)) {
		rec_new(&TMH.c, r, __id);
	}
	if (!(r->flags&F_TMH_NMY)) {
		printf("record is mapped.\n");
		w->flags = F_TMH_WMAPPED;
	} else
		printf("record is not mapped.\n");
	w->r = r;
	return w;
}


void f_tmh_close(f_tmh_wokp __w) {

	__f_mem_free(__w);
}

void f_tmh_map(f_tmh_wokp __w, _int_u __size) {
	f_tmh_areap area;
	area = _map(TMH.c.ctx, __size);
	__w->r->area = area;
	__w->r->flags ^= F_TMH_NMY;
	TMH.c.ops.bound(TMH.c.ctx->priv, area->priv, __w->r->priv);
}

void f_tmh_remap(f_tmh_wokp __w, _int_u __size) {

}

void f_tmh_unmap(f_tmh_wokp __w) {

}

void f_tmh_write(f_tmh_wokp __w, void *__buf, _int_u __size, _int_u __offset) {
	TMH.c.ops.write(TMH.c.ctx->priv, __w->r->area->priv, __buf, __size, __offset);
}

void f_tmh_read(f_tmh_wokp __w, void *__buf, _int_u __size, _int_u __offset) {
	TMH.c.ops.read(TMH.c.ctx->priv, __w->r->area->priv, __buf, __size, __offset);
}

