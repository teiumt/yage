# include "compactor.h"
# include "system/io.h"
# include "linux/fcntl.h"
# include "linux/unistd.h"
# include "linux/stat.h"
# include "opt.h"
# include "depart.h"

/*
	need to work on this as for now it will do
*/
# ifndef __ffly_compactor_bin
void prb(_8_u __v) {
	_8_u i = 0;
	while(i != 8) {
		printf("%u", __v>>(7-i)&0x1);
		i++;
	}
	printf("\n");
}

_8_u static _getbyte(f_compactorp __com) {
	_8_u byte;
	__com->get(&byte, 1);
}


void static _putbyte(f_compactorp __com, _8_u __byte) {
	__com->put(&__byte, 1);
}

void static
bit_put(f_compactorp __com, _8_u __bits, _8_u __n) {
	__bits &= 0xff>>(8-__n);
	if (!__com->left) {
		__com->left = 8;
		_putbyte(__com, __com->buf);
		__com->buf = 0x00;
	}

	if (__com->left<__n) {
		_8_u ext = __n-__com->left;
		__com->buf = __com->buf>>__com->left|((__bits&(0xff>>(8-__com->left)))<<(8-__com->left));
		_putbyte(__com, __com->buf);
		__com->left = 8;
		__com->buf = 0x00;
		bit_put(__com, __bits>>(__n-ext), ext);
		return;
	}

	__com->buf = __com->buf>>__n|((__bits&(0xff>>(8-__n)))<<(8-__n));
	__com->left-=__n;
}

_8_u static
bit_get(f_compactorp __com, _8_u __n) {
	_8_u ret;
	if (!__com->left) {
		__com->left = 8;
		__com->buf = _getbyte(__com);
	}

	if (__com->left<__n) {
		_8_u ext = __n-__com->left;
		ret = __com->buf;
		__com->left = 8;
		__com->buf = _getbyte(__com);
		ret = ret|(bit_get(__com, ext)<<(__n-ext));
		return ret;
	}

	ret = __com->buf&(0xff>>(8-__n));
	__com->buf>>=__n;
	__com->left-=__n;
	return ret;
}

void static
bit_flush(f_compactorp __com) {
	if (__com->left != 8)
		_putbyte(__com, __com->buf>>__com->left);
}

void f_compact(f_compactorp __com) {
	__com->left = 8;
	__com->buf = 0x00;
	_8_u prior = _getbyte(__com);
	_8_u past = 0;
	_putbyte(__com, prior);
	_8_u tmp, val, l, bits;
	_8_u i;
	while(__com->at_eof() == -1) {
		tmp = _getbyte(__com);
		val = prior^tmp;
	_bk:
		prior = tmp;
		l = 0;
		while(!((val>>(7-l))&0x01) && l<8)
			l++;
		bits = 8-l;

		i = 0;
		if (val == past) {
			bit_put(__com, 1, 1);
		_again:
			tmp = _getbyte(__com);
			if ((val = (prior^tmp)) != past || i > 2) {
				bit_put(__com, i, 2);
				goto _bk;
			}
			prior = tmp;
			i++;
			goto _again;
		}
		bit_put(__com, 0, 1);
		bit_put(__com, bits, 4);
		bit_put(__com, val, bits);
		past = val;
	}
	bit_flush(__com);
}

void f_decompact(f_compactorp __com) {
	__com->left = 0;
	__com->buf = 0x00;
	_8_u prior = _getbyte(__com);
	_8_u past = 0;
	_putbyte(__com, prior);
	while(__com->at_eof() == -1) {
		_8_u sk = bit_get(__com, 1);
		_8_u tmp;
		_8_u b;
		if (!sk) {
			_8_u bits = bit_get(__com, 4);
			b = bit_get(__com, bits);
			tmp = b;
		} else {
			_8_u i;
			i = bit_get(__com, 2);
		_again:
			_putbyte(__com, prior = (prior^past));
			if (i>0) {
				i--;
				goto _again;
			}
			continue;
		}
		b = prior^b;
		prior = b;
		past = tmp;
		_putbyte(__com, b);
	}
}
# endif
# ifdef __ffly_compactor_bin
int unsigned static in_off = 0, in_size = 0;
int static in;
int static out;

void put(void *__buf, _int_u __size) {
	write(out, __buf, __size);
}

void get(void *__buf, _int_u __size) {
	read(in, __buf, __size);
	in_off+=__size;
}

_8_i at_eof(void) {
	if (in_off>=in_size)
		return 0;
	return -1;	
}

void reset() {
	in_off = 0;
}

# include "compactor.h"
# include "dep/str_cmp.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	char const *method = NULL;
	char const *infile = NULL, *outfile = NULL;
	ffoe_prep();
	struct ffpcll pcl;
	pcl.cur = __argv+1;
	pcl.end = __argv+__argc;
	ffoe(ffoe_pcll, (void*)&pcl);
	method = ffoptval(ffoe_get("m"));
	infile = ffoptval(ffoe_get("i"));
	outfile = ffoptval(ffoe_get("o"));
	ffoe_end();

	if (!method || !infile || !outfile) {
		printf("usage: ./ -m <method{c, d}> -i <input> -o <output>");
		return -1;
	}

	struct f_compactor com;
	com.put = put;
	com.get = get;
	com.at_eof = at_eof;
	com.reset = reset;
	in = open(infile, O_RDONLY, 0);
	struct stat st;
	fstat(in, &st);
	in_size = st.st_size;
	out = open(outfile, O_WRONLY|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR);
	if (!f_str_cmp(method, "d"))
		f_decompact(&com);
	else if (!f_str_cmp(method, "c"))
		f_compact(&com);
	else {
		printf("unknown method.\n");
	}
	close(in);
	close(out);
	ffly_depart(NULL); 
}
# endif
