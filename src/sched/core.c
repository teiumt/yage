# include "../sched.h"
# include "../cradle.h"
# include "../msg.h"
# include "../ffly_def.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSSCHED)
struct pcore* sched_preferred(void){
	struct pcore *p;
	struct pcore *pref;
	_int_u i;

	pref = _f_cradle.pcores;
	MSG(INFO, "looking for suitable core for entity.\n")
	i = 1;
	for(;i<_f_cradle.npc;i++) {
		p = _f_cradle.pcores+i;
		MSG(INFO, "core-%u{%u}, core-%u{%u}.\n ", p->id, p->delay, pref->id, pref->delay)
		if (p->delay<pref->delay) {
			MSG(INFO, "changed dest core from %u to core-%u.\n", pref->id, p->id)
			pref = p;
		}
	}

	return pref;
}

_8_s static linkup(struct sched_mann *mn,sched_entityp __ent,struct pcore *pref,struct sched_list *ls){
	if(mt_trylock(&ls->lock) == -1)
		return -1;
	y_iwlist_link(ls->cur,&__ent->link);
	ls->cur = &__ent->link;
	mn->ent_n++;
	__ent->core = pref;
	__ent->mn = mn;
	__ent->list = ls;
	mt_unlock(&ls->lock);
	return 0;
}
#define NO_VERBOSE

void entity_detach_nolock(sched_entityp __ent) {
	struct sched_list *ls;
	ls = __ent->list;
	
	if(!__ent->link.next){
		ls->cur = __ent->link.prev;	
		ls->cur->next = NULL;
	}else{
		y_iwlist_unlink(&__ent->link);
	}
#ifndef NO_VERBOSE
	MSG(INFO, "entity-%u removed from core-%u.\n", __ent->id, __ent->core->id)
#endif
}
#include "../proc.h"


void sched_migrate(sched_entityp __ent,struct pcore *__to){
#ifndef NO_VERBOSE
	MSG(INFO,"migrating (%u) from core-%u to core-%u, type: %u\n",__ent->id,__ent->core->id,__to->id,__ent->type)
#endif
	entity_detach_nolock(__ent);
	sched_entity_attach(__ent,__to);
}

void sched_entity_attach(sched_entityp __ent,struct pcore *pref) {
	struct sched_core *s;
	s = &pref->sched;

	struct sched_mann *mn;
	mn = s->mann+__ent->type;

	_int_u i;
	i = 0;
_again:
#ifndef NO_VERBOSE
	MSG(INFO,"linking (%u) to core-%u, c: %u\n",__ent->id,pref->id,i)
#endif
	if(linkup(mn,__ent,pref,&mn->list[i&1]) == -1){
		i++;
		goto _again;
	}
#ifndef NO_VERBOSE
	MSG(INFO, "entity-%u added to core-%u.\n", __ent->id, pref->id)
#endif
}

void sched_entity_detach(sched_entityp __ent) {
	mt_lock(&__ent->list->lock);
	entity_detach_nolock(__ent);
	mt_unlock(&__ent->list->lock);
}

