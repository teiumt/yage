# ifndef __f__ioc__struc__h
# define __f__ioc__struc__h
# include "rws.h"
/*
	short for io control structure
*/
struct f_ioc_struc {
	struct f_rw_struc rws;
};

# endif /*__f__ioc__struc__h*/
