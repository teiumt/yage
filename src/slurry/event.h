# ifndef __ffly__slurry__event__h
# define __ffly__slurry__event__h
# include "../y_int.h"
struct s_button {
	_int x, y;
};

struct s_key {
	_int_u code;	
};

typedef struct s_event {
	_8_u type;
	struct s_button button;
	struct s_key key;
} *s_eventp;

enum {
	_s_event_msg,
	_s_button_press,
	_s_button_release,
	_s_event_unknown,
	_s_key_press,
	_s_key_release
};

# endif /*__ffly_slurry__event__h*/
