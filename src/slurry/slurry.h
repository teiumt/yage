# ifndef __ffly__slurry__h
# define __ffly__slurry__h
# include "../y_int.h"
# include "proto.h"
# ifndef __slurry_client
# include "window.h"
# include <X11/Xlib.h>
# include <GL/glx.h>
# endif

#define EVBS 20
#define OV_PORTN	0

# include "event.h"

# ifndef __slurry_client
struct s_window {
	struct s_window *next;
	_8_u *frame_buff;
	Display *d;
	_int_u width, height;
	Window w;
	GLXContext glx_ct;
};
# endif


/*
operational variables
*/
extern void *s_ov[];

typedef struct s_tape {
	void *text;
	_int_u len;
} *s_tapep;

typedef struct s_buff {
	void *data;
	_int_u len;
} *s_buffp;

_32_s s__send(long long, void*, _int_u, _32_u);
_32_s s__recv(long long, void*, _int_u, _32_u);
_32_s(*s_io_send)(void*, _int_u, long long);
_32_s(*s_io_recv)(void*, _int_u, long long);

s_tapep s_tape_new(_int_u);
void s_tape_destroy(s_tapep);
void s_confload(void);
void s_flush(long long);
# ifndef __slurry_client
void sse_open(void);
void sse_run(void);
void sse_close(void);

# endif
# endif /*__ffly__slurry__h*/
