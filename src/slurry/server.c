# include "slurry.h"
# include <sys/types.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <sys/time.h>
# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <signal.h>
# include <errno.h>
# include <fcntl.h>
/*
	TODO:
		lock address within stack that a pointer may be stored or plate them like /memory/plate
		to avoid giving the user the pointer or dont.... as i dont realy care.

	TODO:
		for operations that cause sock read/write - create struct and push it onto a stack
		and after tape has finished or operation calling for stack to be flushed.
*/
static struct sockaddr_in adr;
int static sock;
void sse_open(void) {
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		printf("error failed to create socket.\n");
		return;
	}
	adr.sin_family = AF_INET;
	adr.sin_addr.s_addr = htons(INADDR_ANY);
	adr.sin_port = htons(*(_int_u*)s_ov[OV_PORTN]);
	int val = 1;		
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(int));

	fcntl(sock, F_GETFL);

	if (bind(sock, (struct sockaddr*)&adr, sizeof(struct sockaddr_in)) == -1) {
		printf("failed to bind socket to address.");
		return;
	}	
}


_8_u static *p;
_8_i static run;

static struct s_event evbuf[EVBS];
struct {int sock;} client;


void static sig(int __sig) {
	run = -1;
	shutdown(sock, SHUT_RDWR);
}

_8_u static stack[512];
# define stackat(__ad) \
	(stack+(__ad))
void static
window_new(void) {
	printf("new window, %u, %u\n", *(_16_u*)p, *(_16_u*)(p+2));
	_16_u dst;
	dst = *(_16_u*)p;
	*(struct s_window**)stackat(dst) = __s_window_new(*(Display**)(stackat(*(_16_u*)(p+2))));
}

void static
_read(void) {
	printf("read.\n");
	_16_u src, n;
	src = *(_16_u*)p;
	n = *(_16_u*)(p+2);
	s_send(stackat(src), n, client.sock);
}

void static
_write(void) {
	printf("write.\n");
	_16_u dst, n;
	dst = *(_16_u*)p;
	n = *(_16_u*)(p+2);
	s_recv(stackat(dst), n, client.sock);
}

void static
window_init(void) {
	printf("window init.\n");
	_16_u wd;
	_16_u width, height;
	_16_u title;

	wd = *(_16_u*)p;
	width = *(_16_u*)(p+2);
	height = *(_16_u*)(p+4);
	title = *(_16_u*)(p+6);

	struct s_window *w;
	w = *(struct s_window**)stackat(wd);
	__s_window_init(w, width, height, (char const*)stackat(title));
}

void static
window_display(void) {
	printf("window display.\n");
	struct s_window *wd;

	wd = *(struct s_window**)stackat(*(_16_u*)p);
	__s_window_display(wd);
}

void static
window_frame(void) {
	printf("window frame.\n");
	struct s_window *wd;

	wd = *(struct s_window**)stackat(*(_16_u*)p);
	
	_8_u *f;
	f = wd->frame_buff;

	s_recv(f, wd->width*wd->height*4, client.sock);
}

void static
window_destroy(void) {
	printf("window destroy.\n");
	_16_u wd;
	wd = *(_16_u*)p;

	struct s_window *w;
	w = *(struct s_window**)stackat(wd);
	__s_window_destroy(w);
}

void static
display_open(void) {
	printf("display open, %u\n", *(_16_u*)p);
	*(Display**)stackat(*(_16_u*)p) = XOpenDisplay(NULL);
}

void static
display_close(void) {
	printf("display close.\n");
	Display *d = *(Display**)stackat(*(_16_u*)p);
	XSetCloseDownMode(d, DestroyAll);
	XCloseDisplay(d);
}

void static
event(void) {
	printf("event.\n");
	Display *d = *(Display**)stackat(*(_16_u*)p);
	XEvent event;
	_8_u *t;
	s_eventp ev;
	ev = evbuf;
_again:
	t = &ev->type;
	if (XPending(d) > 0 && ev<(evbuf+EVBS)) {
		XNextEvent(d, &event);
		switch(event.type) {
			case ClientMessage:
				*t = _s_event_msg;
			break;
			case ButtonPress: case ButtonRelease:
				*t = (event.type == ButtonPress?_s_button_press:_s_button_release);
				
				ev->button.x = event.xbutton.x;
				ev->button.y = event.xbutton.y;
			break;
			case KeyPress: case KeyRelease:
				*t = (event.type == KeyPress?_s_key_press:_s_key_release);
				ev->key.code = event.xkey.keycode;
			break;
			default:
				*t = _s_event_unknown;
		}
		ev++;
		goto _again;
	}
	
	_int_u n;
	n = ev-evbuf;
	printf("number: %u\n", n);
	s_send(&n, sizeof(_int_u), client.sock);
	if (n>0)
		s_send(evbuf, n*sizeof(struct s_event), client.sock);
}

void static
pending(void) {
	printf("pending.\n");
	Display *d = *(Display**)stackat(*(_16_u*)p);
	_8_i res;
	res = -1;
	if (XPending(d)>0) {
		res = 0;
	}
	s_send(&res, 1, client.sock);
}

_8_i static dc;
void disconnect();
__asm__("disconnect:\n\t"
		"movb $0, dc(%rip)\n\t"
		"ret");

static void(*op[])(void) = {
	window_new,
	_read,
	_write,
	window_init,
	window_destroy,
	disconnect,
	window_display,
	window_frame,
	display_open,
	display_close,
	event,
	pending
};

void ox(void *__op) {
	((void(*)(void))__op)();
}

_8_u static osz[] = {
	4,		//window_new
	4,		//write
	4,		//read
	8,		//window_init
	2,		//window_destroy
	0,		//disconnect
	2,		//window_display
	2,		//window_frame
	2,		//display_open
	2,		//display_close
	2,		//event
	2		//pending
};

void texec(s_tapep __t) {
	p = (_8_u*)__t->text;
	_8_u *end;
	end = p+__t->len;
	_8_u on;
_again:
	if (p>=end)
		return;
	on = *(p++);
	printf("operation : %u : %u\n", on, osz[on]);
	ox(op[on]);
	p+=osz[on];
	goto _again;
}

void sse_run(void) {
	struct sigaction sa;
	bzero(&sa, sizeof(struct sigaction));
	sa.sa_handler = sig;
	sigaction(SIGINT, &sa, NULL);
	struct sockaddr_in cli;
	run = 0;
	_int_u len;
	s_tapep t;
	while(!run) {
		dc = -1;
		if (listen(sock, 1)<0) {
			printf("failed to listen.\n");
			break;
		}
		len = sizeof(struct sockaddr_in);
		if ((client.sock = accept(sock, (struct sockaddr*)&cli, &len)) <0) {
			printf("failed to accept.\n");
			break;
		}
		printf("client connected.\n");
		_int_u tsz;
	_again:
		if (run == -1) break;
		if (s_recv(&tsz, sizeof(_int_u), client.sock)<=0) {
			printf("failed to recv. %s:%d\n", strerror(errno), errno);
			break;
		}
		printf("got size: %u\n", tsz);
		printf("recved tape size.\n");
		if (tsz>4088) {
			return;
		}
		t = s_tape_new(tsz);
		printf("waiting for tape text.\n");

		if (s_recv(t->text, tsz, client.sock)<=0) {
			printf("failed to recv. %s:%d\n", strerror(errno), errno);
			break;
		}
		printf("exec.\n");
		texec(t);
		s_tape_destroy(t);
		printf("\n\n");

		if (!dc) {
			printf("client disconnected.\n");
			close(client.sock);
		} else
			goto _again;
	}
	printf("\ngoodbye, sad to see you go :(\n");
}

void sse_close(void) {
	close(sock);
}
