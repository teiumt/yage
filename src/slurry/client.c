# define __slurry_client
# include "slurry.h"
# include "connection.h"
# include "o_sheet.h"
# ifndef __fflib
# include <unistd.h>
# include <string.h>
# include <stdio.h>
# else
# include "../linux/unistd.h"
# include "../string.h"
# include "../linux/socket.h"
# include "../stdio.h"
# endif
#define tape(__conn, __code, __len) \
{	\
	_int_u len; \
	len = __len; \
	s_send(&len, sizeof(_int_u), __conn->sock); \
	s_send(__code, __len, __conn->sock);\
}

/*
	TODO:
		build tape then allow user to run it not like ....
*/

_16_u static stack = 0;
_16_u s_window_new(s_connp __conn, _16_u __d) {
	_16_u w;
	w = stack;
	stack+=8;

	_8_u code[5];

	*code = _op_window_new;
	*(_16_u*)(code+1) = w;
	*(_16_u*)(code+3) = __d;
	tape(__conn, code, 5);
	return w;
}

void s_read(s_connp __conn, _16_u __adr, _int_u __n, void *__buf) {
	_8_u code[5];
	*code = _op_read;
	*(_16_u*)(code+1) = __adr;
	*(_16_u*)(code+3) = __n;
	tape(__conn, code, 5);
	s_recv(__buf, __n, __conn->sock);
}

void s_write(s_connp __conn, _16_u __adr, _int_u __n, void *__buf) {
	_8_u code[5];
	*code = _op_write;
	*(_16_u*)(code+1) = __adr;
	*(_16_u*)(code+3) = __n;
	tape(__conn, code, 5);
	s_send(__buf, __n, __conn->sock);
}

void s_window_init(s_connp __conn, _16_u __wd, 
	_16_u __width, _16_u __height, _16_u __title)
{
	_8_u code[9];
	*code = _op_window_init;
	*(_16_u*)(code+1) = __wd;
	*(_16_u*)(code+3) = __width;
	*(_16_u*)(code+5) = __height;
	*(_16_u*)(code+7) = __title;
	tape(__conn, code, 9);
}

_16_u s_display_open(s_connp __conn) {
	_16_u d;
	d = stack;
	stack+=8;
	_8_u code[3];
	*code = _op_display_open;
	*(_16_u*)(code+1) = d;
	tape(__conn, code, 3);
	return d;
}

void s_display_close(s_connp __conn, _16_u __d) {
	_8_u code[3];
	*code = _op_display_close;
	*(_16_u*)(code+1) = __d;
	tape(__conn, code, 3);
}

void s_window_destroy(s_connp __conn, _16_u __wd) {
	_8_u code[3];
	*code = _op_window_destroy;
	*(_16_u*)(code+1) = __wd;
	tape(__conn, code, 3);
}

void s_window_display(s_connp __conn, _16_u __wd) {
	_8_u code[3];
	*code = _op_window_display;
	*(_16_u*)(code+1) = __wd;
	tape(__conn, code, 3);
}

void s_window_frame(s_connp __conn, _16_u __wd, _8_u *__frame, _int_u __width, _int_u __height) {
	_8_u code[3];
	*code = _op_window_frame;
	*(_16_u*)(code+1) = __wd;
	tape(__conn, code, 3);
	/*
		using tcp for this is bad
		TODO:
			change
			keep extra udp socket open for this?????
	*/
	s_send(__frame, __width*__height*4, __conn->sock);
}

static struct s_event evbuf[EVBS];
static _int_u ev = 0;
static _int_u nev = 0;

void s_event(s_connp __conn, _16_u __d, struct s_event **__dst) {
	if (ev<nev) {
	_up:
		*__dst = evbuf+(ev++);
	} else {
		printf("new events.\n");
		_8_u code[3];
		*code = _op_event;
		*(_16_u*)(code+1) = __d;
		tape(__conn, code, 3);

		_int_u n;
		s_recv(&n, sizeof(_int_u), __conn->sock);
		nev = n;
		printf("----> %u\n", n);
		ev = 0;
		if (n>0)
			s_recv(evbuf, n*sizeof(struct s_event), __conn->sock);
		else {
			*__dst = NULL;
			return;
		}
		goto _up;
	}
}

_8_i s_pending(s_connp __conn, _16_u __d) {
	_8_i res;
	if (ev<nev) {
		res = 0;
	} else {
		_8_u code[3];
		*code = _op_pending;
		*(_16_u*)(code+1) = __d;
		tape(__conn, code, 3);
		s_recv(&res, 1, __conn->sock);
	}
	return res;
}

_16_u s_rtn(_int_u __sz) {
	_16_u r;
	r = stack;
	stack+=__sz;
	return r;
}

void s_disconnect(s_connp __conn) {
	_8_u code;
	code = _op_disconnect;
	tape(__conn, &code, 1);
}
/*
//# ifdef __fflib
# include "../system/nanosleep.h"
# include <string.h>
# include <time.h>
static _8_u buf[600*600*4];
void s_test(void) {
	memset(buf, 255, 600*600*4);
	stack = 0;
	s_connp con;
	con = s_open();
	s_connect(con, 10198, "127.0.0.1");

	_16_u d;
	d = s_display_open(con);
	_16_u wd;
	wd = s_window_new(con, d);
	char const *tstr = "MEH";

	_16_u title;
	title = s_rtn(strlen(tstr)+1);
	s_write(con, title, strlen(tstr)+1, tstr);
	s_window_init(con, wd, 600, 600, title);

	struct timespec start, stop;
	_int_u i;
	struct s_event *fe;
	for(i = 0;i!= 400;i++) {
		clock_gettime(CLOCK_MONOTONIC, &start);
		s_window_frame(con, wd, buf, 600, 600);
		clock_gettime(CLOCK_MONOTONIC, &stop);
		printf("time taken sec: %u, ns: %u\n", stop.tv_sec-start.tv_sec, stop.tv_nsec-start.tv_nsec);
		s_window_display(con, wd);
		if (!s_pending(con, 0)) {
			s_event(con, d, &ev);
			printf("event is pending.\n");
			break;
		}
	}

	ffly_nanosleep(5, 0);
	s_window_destroy(con, wd);
	s_display_close(con, d);

	s_disconnect(con);
	s_close(con);
}

int main() {
	s_test();
}
//# endif
*/
