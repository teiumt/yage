# ifndef __ffly__slurry__connection__h
# define __ffly__slurry__connection__h
# include "../y_int.h"
# include "event.h"
# ifndef __fflib
# include <netinet/in.h>
# else
# include "../linux/in.h"
# endif
typedef struct s_conn {
	int sock;
	struct sockaddr_in addr;
} *s_connp;

s_connp s_open(void);
void s_connect(s_connp, _16_u, char const*);
void s_close(s_connp);
void s_test(void);
_16_u s_window_new(s_connp, _16_u);
void s_read(s_connp, _16_u, _int_u, void*);
void s_write(s_connp, _16_u, _int_u, void*);
void s_window_init(s_connp, _16_u, _16_u, _16_u, _16_u);
_16_u s_display_open(s_connp);
void s_display_close(s_connp, _16_u);
void s_window_destroy(s_connp, _16_u);
void s_window_display(s_connp, _16_u);
void s_window_frame(s_connp, _16_u, _8_u*, _int_u, _int_u);
void s_disconnect(s_connp);
void s_event(s_connp, _16_u, struct s_event**);
_8_i s_pending(s_connp, _16_u);
_16_u s_rtn(_int_u);
# endif /*__ffly__slurry__connection__h*/
