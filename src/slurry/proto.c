# include "proto.h"
#define PKTSHFT 11
#define PKTSIZE (1<<PKTSHFT)
void s_flush(long long);
_32_s s_send(void *__buf, _int_u __size, long long __arg) {
	if (s_io_send(__buf, __size, __arg)<0)
		return -1;
/*
	f_u8_t *p;
	_int_u n;
	n = __size>>PKTSHFT;

	p = (_8_u*)__buf;
	_int_u i;
	i = 0;
	while(i != n) {
		s_io_send(p, PKTSIZE, __arg);
		p+=PKTSIZE;
		i++;
	}

	_int_u left;
	left = __size-(n<<PKTSHFT);
	if (left>0) {
		s_io_send(p, left, __arg);
	}
	s_flush(__arg);
*/
	return __size;
}

_32_s s_recv(void *__buf, _int_u __size, long long __arg) {
	if (s_io_recv(__buf, __size, __arg)<0)
		return -1;
	return __size;
/*
	_8_u *p;
	_int_u n;
	n = __size>>PKTSHFT;

	p = (_8_u*)__buf;
	_int_u i;
	i = 0;
	while(i != n) {
		s_io_recv(p, PKTSIZE, __arg);
		p+=PKTSIZE;
		i++;
	}

	_int_u left;
	left = __size-(n<<PKTSHFT);
	if (left>0) {
		s_io_recv(p, left, __arg);
	}
*/
}
