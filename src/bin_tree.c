#include "bin_tree.h"
#include "m_alloc.h"
#include "ffly_def.h"
#include "string.h"
#include "assert.h"
struct bt_node static* newnode(struct bin_tree *__tree);
void bt_init(struct bin_tree *__tree) {
	__tree->top = NULL;
	__tree->root = newnode(__tree);
	__tree->root->l = newnode(__tree);
	__tree->root->r = newnode(__tree);
	__tree->root->alive = -1;
	__tree->root->null = 0;
	__tree->root->key = 0x41;
}
struct bt_node static* newnode(struct bin_tree *__tree) {
	struct bt_node *n = m_alloc(sizeof(struct bt_node));

	n->l = NULL;
	n->r = NULL;
	n->key = 0x41;
	n->alive = -1;
	n->null = 0;

	if (__tree->top != NULL)
		__tree->top->bw = &n->fw;
	n->bw = &__tree->top;
	n->fw = __tree->top;
	__tree->top = n;
	
	return n;
}

void static freenode(struct bt_node *__n) {
	*__n->bw = __n->fw;
	if (__n->fw != NULL) {
		__n->fw->bw = __n->bw;
	}
	if(__n->l != NULL)
		freenode(__n->l);
	if (__n->r != NULL)
		freenode(__n->r);
	m_free(__n);
}

struct bt_node static* _bt_get(struct bin_tree *__tree, struct bt_node *__start, _64_u __key) {
	struct bt_node *n = __start, *back, **r;
_trek:
	if (!n){
		n = newnode(__tree);
		n->l = newnode(__tree);
		n->r = newnode(__tree);
		n->null = -1;
		n->l->b = n;
		n->r->b = n;
	
		n->l->bb = &n->l;
		n->r->bb = &n->r;

		n->key = __key;
		if (n->key>__key) {
			back->r = n;	
		}else if (n->key<__key) {
			back->l = n;
		}
		return n;
	}

	if (!n->null) {
		n->l = newnode(__tree);
		n->r = newnode(__tree);
		n->l->b = n;
		n->r->b = n;
	
		n->null = -1;
		n->key = __key;
		return n;
	}
	if (n->key == __key && !n->alive) {
		return n;
	}
	back = n;
	if (n->key>__key) {
		n = n->r;
	}else if (n->key<__key) {
		n = n->l;
	}
	goto _trek;

}

void* bt_find(struct bin_tree *__tree, _64_u __key) {
	struct bt_node *n = __tree->root;
_trek:
	if (n->key == __key && n->null == -1) {
		return n;
	}
	if (n->key>__key) {
		n = n->r;
	}else if (n->key<__key) {
		n = n->l;
	}
	if (!n->null) {
	//	printf("couldent find key assoasated with node: %x.\n",__key);
		return NULL;
	}
	goto _trek;

}
static char space[128] = {'\0'};
static _int_u sp = 0;
void static addspace(void) {
	space[sp++] = '\t';
	space[sp++] = '|';
	space[sp] = '\0';	
}

void static subspace(void) {
	space[sp-2] = '\0';
	sp-=2;
}
void bt_show(struct bt_node *__n) {
	if (!__n)return;
	if (!__n->null)return;
	printf("%s: node-%u\n",space,__n->key);
	addspace();
	printf("%sLEFT%u:.\n",space,__n->key);
	bt_show(__n->l);
	printf("%sRIGHT%u:.\n",space,__n->key);
	bt_show(__n->r);
	subspace();
}
void bt_erase(struct bin_tree *__tree, struct bt_node *__n) {
	assert(__n != NULL);
	if (__tree->root == __n) {
		if (__n->l->null == -1) {
			__tree->root = __n->l;
			if (__n->r->null == -1) {
				struct bt_node *cur;
				cur = __n->l;
				while(1) {
					if (!cur->r->null) {
						break;
					}
					cur = cur->r;
				}
				cur->r = __n->r;
				__n->r->b = cur;
			}
		}else if (__n->r->null == -1) {
			__tree->root = __n->r;
		}else{
			__tree->root = NULL;
		}
	
		return;
	}
	struct bt_node *n = NULL;
	if (!__n->r->null && !__n->l->null) {
		__n->null = 0;
		return;
	}
	if(__n->b->key>__n->key) {
		/*
			if are right side happens so be dead in the water,
			then replace the BACK node right with the left side
			we can place R/or L in to the BACK node right 
			as ether is grater then the key.

					10(H)		G
			       /  \
				  1   20(T)		L
				  	 /	\
					17	100
					(L)	(R)
						/  \
					   24   415
			NOTE:
				all nodes attached to branch T keys are grater then H node key,
				this rule is followed.
				if we want to remove node T then we can replace it with any of the sub nodes
				with minor modifications, namely murging both sub branches into one.

				NOTE:
				another rule we can use the edges to guide us.
				example:
					the L branch of node T if murged with R then we 
					find the lowest value by keeping left and going down.
					we can then attach to the last NODE.

					PROOF:
					L of T will nether be larger then any of the values of R
					R will allways be bigger then any values from L of T

					so we can attach L to the lowest value in the R branch of T

					we can get the lowest by following the left hand side of all nodes starting from T..
				
					10(H)		G
				   /  \
				  1    100(T)	L
				       /
					  24		L
					  /
					 17	
		*/
		if(!__n->r->null) {
			__n->null = 0;
			__n->b->r = __n->l;
			__n->l->b = __n->b;
			return;
		}
		__n->b->r = n = __n->r;
		__n->r->b = __n->b;
		struct bt_node *cur;
		cur = n;
		while(1) {
			if (!cur->l->null) {
				break;
			}
			cur = cur->l;
		}
		freenode(cur->l);
		cur->l = __n->l;
		__n->l->b = cur;
	}else
	if(__n->b->key<__n->key) {
		if(!__n->l->null) {
			__n->null = 0;
			__n->b->l = __n->r;
			__n->r->b = __n->b;
			return;
		}
		__n->b->l = n = __n->l;
		__n->r->b = __n->b;
		struct bt_node *cur;
		cur = n;
		while(1) {
			if (!cur->r->null) {
				break;
			}
			cur = cur->r;
		}
		freenode(cur->r);
		cur->r = __n->r;
		__n->r->b = cur;
	}
	assert(n != NULL);
	__n->key = ~0;
	n->b = __n->b;
}

void* bt_embed(struct bin_tree *__tree, _64_u __key) {
	struct bt_node *n;
	n = _bt_get(__tree,__tree->root,__key);
	n->alive = 0;
	return n;
}
/*
	nested freeing can only get us so far before be run out
*/
void bt_cleanup(struct bin_tree *__tree) {
	struct bt_node *n = __tree->top, *bk;
	while(n != NULL) {
		n = (bk = n)->fw;
		m_free(bk);
	}
}
