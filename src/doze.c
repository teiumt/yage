# include "tmu.h"
# include "io.h"
# include "linux/time.h"
/*
	1 click = 0.125 second
	1 seconds = 7-clicks = subclicks CLICK_BASE
	2 seconds = 8-clicks
	3 seconds = 15-clicks

	subclicks = 0-CLICK_BASE7
*/
void doze(_64_u __lower, _64_u __higher) {
	_64_u sec = (__higher>>3)<<1;
	long double lower;
	lower = ((__higher&7)*(CLICK_BASE/7))+__lower;

	long double nsec;
	nsec = lower*(1000000000./CLICK_BASE);
	_64_u nsec_i;
	nsec_i = (_64_u)nsec;
	struct timespec ts;
	ts.tv_sec = sec;
	ts.tv_nsec = nsec_i;

	nanosleep(&ts, NULL);
}
