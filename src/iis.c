# include "iis.h"
# include "rws.h"
# include "io.h"
void f_msgraw(void*, _int_u);
void f_io_outlet(struct f_rw_struc*, void*, _int_u);
void static
_msgout(void *__buf, _int_u __size) {
	f_msgraw(__buf, __size);
}

void static
_fout(void *__buf, _int_u __size) {
	f_io_outlet(ffly_out, __buf, __size);
}
struct f_iis _f_msg = {_msgout};
struct f_iis _f_out = {_fout};
