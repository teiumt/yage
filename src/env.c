# include "env.h"
# include "m_alloc.h"
# include "string.h"
#include "crypt.h"
# include "io.h"
char static **env = NULL;

char const **getenv(void) {
	return env;
}

_int_u static off = 0;
struct entry {
	_8_u const *key;
	_int_u len;
	char *p;
	_int_u vl;
	_int_u env, ol;
	struct entry *fd, *next;
};

static struct entry **table;
static struct entry *top = NULL;

void envinit(void) {
	table = (struct entry**)m_alloc(0x100*sizeof(struct entry*));
	struct entry **cur = table;
	while(cur != table+0x100)
		*(cur++) = NULL;
}

void envcleanup(void) {
	if (table != NULL)
		m_free(table);

	struct entry *cur = top, *bk;
	while(cur != NULL) {
		bk = cur;
		cur = cur->fd;
		m_free(bk->key);
		m_free(bk);
	}

	char **p = env;
	while(p != env+off) {
		//printf("%s\n", *p);
		m_free(*(p++));
	}

	if (env != NULL)
		m_free(env);
}

void envput(char const*, char const*);
void envload(char const **__envp) {
	char const **cur = __envp;
	char const *p;
	char buf[128];
	char *bufp;
	while(*cur != NULL) {
		p = *cur;
		bufp = buf;
		while(*p != '=')
			*(bufp++) = *(p++);
		*bufp = '\0';
		p++;
		envput(buf, p);
		cur++;
	}
}

static struct entry*
lookup(char const *__key) {
	_int_u len = str_len(__key);
	_64_u sum = y_hash64(__key, len);
	struct entry *cur = *(table+(sum&0xff));
	while(cur != NULL) {
		if (cur->len == len) {
			if (!mem_cmp(cur->key, __key, len))
				return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

char const *envget(char const *__key) {
	struct entry *ent = lookup(__key);
	if (ent != NULL)
		return ent->p;
	return NULL;
}

_8_i
envset(char const *__key, char const *__p) {
	struct entry *ent;
	_int_u len = str_len(__p);
	if (!(ent = lookup(__key)))
		return -1;

	char **e = env+ent->env;
	if (len != ent->vl) {
		_int dif = (_int)len-(_int)ent->vl;
		ent->vl = len;
		_int_u voff = ent->p-*e;
		ent->ol = ((_int)ent->ol)+dif;
		*e = (char**)m_realloc(*e, ent->ol+2);
		ent->p = (*e)+voff;
	}
	mem_cpy(ent->p, __p, len+1);
	return 0;
}

void
envput(char const *__key, char const *__p) {
	if (!envset(__key, __p))
		return;
		
//	printf("env, key: %s, val: %s\n", __key, __p);
	if (!env) {
		env = (char**)m_alloc(sizeof(char*));	
		off++;
	} else
		env = (char**)m_realloc(env, (++off)*sizeof(char*));

	struct entry *ent;
	_int_u len = str_len(__key);
	_64_u sum = y_hash64(__key, len);	
		
	ent = (struct entry*)m_alloc(sizeof(struct entry));
	struct entry **tbl = table+(sum&0xff);
	ent->next = *tbl;
	*tbl = ent;
	mem_dup((void**)&ent->key, (void*)__key, len);
	
	ent->vl = str_len(__p);
	char *p;
	p = (*(env+(off-1)) = (char*)m_alloc(ent->vl+len+2));

	mem_cpy(p, __key, len);
	p+=len;
	*(p++) = '=';
	ent->p = p;
	mem_cpy(p, __p, ent->vl);
	p+=ent->vl;
	*p = '\0';

	ent->len = len;
	ent->fd = top;
	ent->env = off-1;
	ent->ol = ent->vl+len+1;
	top = ent;

}

