# include "exec.h"
# include "linux/unistd.h"
# include "linux/stat.h"
# include "linux/fcntl.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/io.h"
# include "system/nanosleep.h"
# include "dep/mem_cpy.h"
# include "header.h"
int static fd;

void f_exc_su(struct f_exc_ctx *__ctx, _int_u __size) {
	__ctx->p = (_8_u*)__f_mem_alloc(__size);
	__ctx->end = __size;
}

void f_exc_ldsegs(struct f_exc_ctx *__ctx, f_exc_segmentp __segs, _int_u __n) {
	f_exc_segmentp s;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		s = __segs+i;
		f_printf("reading segment at %u:%u.\n", s->offset, s->size);
		if (pread(__ctx->fd, __ctx->p+s->adr, s->size, s->offset)<=0) {
			f_printf("read error.\n");
		}
	}
}

void f_exc_read(struct f_exc_ctx *__ctx, void *__buf, _int_u __size, _64_u __from) {
	pread(__ctx->fd, __buf, __size, __from);
}

void f_excf(char const *__file) {
	if ((fd = open(__file, O_RDONLY, 0)) == -1) {
		ffly_fprintf(ffly_err, "failed to open file, %s\n", __file);
		return;
	}
	struct f_exc_ctx ct;
	struct stat st;
	fstat(fd, &st);

	_8_u block[FH_BS];
	read(fd, block, FH_BS);
	ct.hdr = block;
	ct.fd = fd;

	struct f_fh *h;
	h = (struct f_fh*)block;

	if (*h->ident != FH_MAG0) {
		f_printf("FEXC' mag0 corrupted.\n");
		goto _error;
	}

	if (h->ident[1] != FH_MAG1) {
		f_printf("FEXC' mag1 corrupted.\n");
		goto _error;
	}

	if (h->ident[2] != FH_MAG2) {
		f_printf("FEXC' mag2 corrupted.\n");
		goto _error;
	}

	if (h->ident[3] != FH_MAG3) {
		f_printf("FEXC' mag3 corrupted.\n");
		goto _error;
	}

	if (h->ident[3] != FH_MAG4) {
		f_printf("FEXC' mag4 corrupted.\n");
		goto _error;
	}

	if (h->vers != FH_VERS) {
		f_printf("FEXC' version missmatch.\n");
		goto _error;
	}

	f_remf_exec(&ct);
_error:
	close(fd);
}

void ffexecf(char const *__file) {
	f_excf(__file);
}
