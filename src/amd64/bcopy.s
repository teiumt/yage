	.globl bcopy
	.text
/*
*	rdi - dst 
*	rsi - src
*	rdx - no
*/
bcopy:
.L0:
	movb (%rsi), %al
	movb %al, (%rdi)
	inc %rdi
	inc %rsi
	dec %rdx
	cmpq $0, %rdx
	ja .L0
	ret
