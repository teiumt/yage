#include "syscall.h"
	.globl open
	.globl close
	.globl read
	.globl write
	.globl exit
	.globl brk
	.globl mremap
	.globl mmap
	.globl munmap
	.globl arch_prctl
	.globl stat
	.globl fcntl
	.globl clone
	.globl wait4
	.globl nanosleep
	.globl kill
	.globl pread
	.globl pwrite
	.globl lseek
	.globl creat
	.globl access
	.globl getpid 
	.globl getppid
	.globl fsync
	.globl ioctl
	.globl fstat
	.globl clock_gettime
	.globl setsockopt
	.globl bind
	.globl accept
	.globl sendto
	.globl recvfrom
	.globl socket
	.globl listen
	.globl connect
	.globl shutdown
	.globl dup2
	.globl execve
	.globl fork
	.globl sendmsg
	.globl recvmsg
	.globl unlink
	.globl chmod
	.globl rt_sigaction
	.globl pipe
	.globl tee
	.globl poll
	.globl splice
	.globl futex
	.globl shmctl
	.globl shmget
	.globl shmat
	.globl shmdt
	.text
shmctl:
	movq $sys_shmctl,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
shmget:
	movq $sys_shmget,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
shmat:
	movq $sys_shmat,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
shmdt:
	movq $sys_shmdt,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
futex:
	movq %rcx,%r10
	movq $sys_futex,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
splice:
	movq %rcx,%r10
	movq $sys_splice,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
poll:
	movq $sys_poll,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
tee:
	movq %rcx,%r10
	movq $sys_tee,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
pipe:
	movq $sys_pipe,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
rt_sigaction:
	movq %rcx,%r10
	movq $sys_rt_sigaction,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
open:
	movq $sys_open,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
close:
	movq $sys_close,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
read:
	movq $sys_read,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
write:
	movq $sys_write,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
exit:
	movq $sys_exit,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
brk:
	movq $sys_brk,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
mremap:
	movq %rcx,%r10
	movq $sys_mremap,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
mmap:
	movq %rcx,%r10
	movq $sys_mmap,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
munmap:
	movq $sys_munmap,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
arch_prctl:
	movq $sys_arch_prctl,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
stat:
	movq $sys_stat,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
fcntl:
	movq $sys_fcntl,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
clone:
	movq %rcx,%r10
	movq $sys_clone,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
wait4:
	movq %rcx,%r10
	movq $sys_wait4,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
nanosleep:
	movq $sys_nanosleep,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
kill:
	movq $sys_kill,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
pread:
	movq %rcx,%r10
	movq $sys_pread64,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
pwrite:
	movq %rcx,%r10
	movq $sys_pwrite64,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
lseek:
	movq $sys_lseek,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
creat:
	movq $sys_creat,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
access:
	movq $sys_access,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
getpid:
	movq $sys_getpid,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
getppid:
	movq $sys_getppid,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
fsync:
	movq $sys_fsync,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
ioctl:
	movq $sys_ioctl,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
fstat:
	movq $sys_fstat,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
clock_gettime:
	movq $sys_clock_gettime,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
setsockopt:
	movq $sys_setsockopt,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
bind:
	movq $sys_bind,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
accept:
	movq $sys_accept,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
sendto:
	movq %rcx,%r10
	movq $sys_sendto,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
recvfrom:
	movq %rcx,%r10
	movq $sys_recvfrom,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
listen:
	movq $sys_listen,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
socket:
	movq $sys_socket,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
connect:
	movq $sys_connect,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
shutdown:
	movq $sys_shutdown,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
dup2:
	movq $sys_dup2,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
execve:
	movq $sys_execve,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
fork:
	movq $sys_fork,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
sendmsg:
	movq $sys_sndmsg,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
recvmsg:
	movq $sys_rcvmsg,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
unlink:
	movq $sys_unlink,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
chmod:
	movq $sys_chmod,%rax
	syscall
	cmpq $-MAX_ERRNO,%rax
	jae _fault
	ret
.globl __set_errno
_fault:
	call __set_errno
	ret
