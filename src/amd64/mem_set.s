	.globl mem_set
	.text
/*
*	rdi - dst
*	sil - src
*	rdx - no
*/
mem_set:
.L0:
	movb %sil, (%rdi)
	incq %rdi
	decq %rdx
	cmpq $0, %rdx
	ja .L0
	ret
