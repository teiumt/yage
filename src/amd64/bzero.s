	.globl bzero
	.text
/*
*	rsi - length
*	rdi - ptr to data
*/
bzero:
.L0:
	movb $0x00, (%rdi)
	inc %rdi
	dec %rsi
	cmpq $0, %rsi
	ja .L0
	ret
