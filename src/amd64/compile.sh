#!/bin/sh
as -o $dst_dir/bcopy.o $root_dir/bcopy.s
as -o $dst_dir/bzero.o $root_dir/bzero.s
as -o $dst_dir/mem_cpy.o $root_dir/mem_cpy.s
as -o $dst_dir/mem_cmp.o $root_dir/mem_cmp.s
as -o $dst_dir/str_len.o $root_dir/str_len.s
as -o $dst_dir/str_cmp.o $root_dir/str_cmp.s
as -o $dst_dir/str_cpy.o $root_dir/str_cpy.s
as -o $dst_dir/mem_dup.o $root_dir/mem_dup.s
as -o $dst_dir/str_dup.o $root_dir/str_dup.s
as -o $dst_dir/mem_set.o $root_dir/mem_set.s
as -o $dst_dir/maths.o $root_dir/maths.s
as -o $dst_dir/mutex.o $root_dir/mutex.s
cpp $root_dir/posix/syscall.s -o $root_dir/syscall.S
as -o $dst_dir/syscall.o $root_dir/syscall.S
export ffly_objs="$dst_dir/bcopy.o \
$dst_dir/bzero.o \
$dst_dir/mem_cpy.o \
$dst_dir/mem_cmp.o \
$dst_dir/str_len.o \
$dst_dir/str_cmp.o \
$dst_dir/str_cpy.o \
$dst_dir/mem_dup.o \
$dst_dir/str_dup.o \
$dst_dir/mem_set.o \
$dst_dir/maths.o \
$dst_dir/mutex.o \
$dst_dir/syscall.o"
