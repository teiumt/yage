	.globl mt_lock
	.globl mt_unlock
	.globl mt_trylock
	.text
mt_lock:
0:
	movb (%rdi),%al
	cmpb $1,%al
	je 0b

	movb $1,%al
	lock xchg %al,(%rdi)
	cmpb $0,%al
	jne 0b
	ret
mt_unlock:
	movb (%rdi),%al
	lock xor %al,(%rdi)
	ret
mt_trylock:
	movb $1,%al
	lock xchg %al,(%rdi)
	cmpb $0,%al
	je 0f
	movb $-1,%al
	ret
0:
	movb $0,%al
	ret
