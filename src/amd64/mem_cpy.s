	.globl mem_cpy
	//mem_cpy 'be weary'
	.globl mem_cpyw
	.text
/*
*	rdi - dst
*	rsi - src
*	rdx - count
*/
mem_cpy:
	cmpq $0,%rdx
	je .L1
.L0:
/*
* non zero copy - expects nonzero for count
*/
mem_cpyw:
	movb (%rsi), %al
	movb %al, (%rdi)
	incq %rdi
	incq %rsi
	decq %rdx
	cmpq $0, %rdx
	ja .L0
.L1:
	ret
