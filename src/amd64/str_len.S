# 1 "str_len.s"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "str_len.s"
 .globl str_len
 .text




str_len:
 movq %rdi, %rax
.L0:
 incq %rdi
 cmpb $0, -1(%rdi)
 jne .L0
 decq %rdi

 subq %rax, %rdi
 movq %rdi, %rax
 ret
