# include "pt.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
struct f_pts _f_pts = {
	.n = 0
};

struct f_pt* f_pt_add(void(*__func)(_ulonglong), _ulonglong __arg) {
	struct f_pt *t;
	t = (struct f_pt*)__f_mem_alloc(sizeof(struct f_pt));
	t->func = __func;
	t->arg = __arg;
	_f_pts.t[_f_pts.n++] = t;
	return t;
}

void f_pt_run(void) {
	struct f_pt *t;
	_int_u i;
	i = 0;
	for(;i != _f_pts.n;i++) {
		t = _f_pts.t[i];
		t->func(t->arg);
	}
}
