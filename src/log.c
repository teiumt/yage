# include "log.h"
# include "io.h"
# include "string.h"
# include "m_alloc.h"
# include "mutex.h"
#include "y.h"
struct ffly_file *vk_out;
void *vkout;
extern struct f_rw_funcs rws_func;

struct log_struc logs[LOG_MAX];
#define LOG(__name, __n)\
	struct log_struc *__name;\
	__name = logs+(__n);

void y_log_file(struct log_struc *__log,char const *__path) {
    _8_s err;
    if (!(__log->_out = f_fopen(__path,
        FF_O_WRONLY|FF_O_TRUNC|FF_O_CREAT,
        FF_S_IRUSR|FF_S_IWUSR, &err))) {
    }
    ffly_fopt(__log->_out, FF_STREAM);
		__log->ioc_out.rws.funcs = &rws_func;
    __log->ioc_out.rws.arg = (_ulonglong)__log->_out;
    __log->_out->rws = &__log->ioc_out.rws;
}

void log_printf(struct log_struc *__log, char const *__fmt,...){
	if(y_bits&Y_PRINTF_PERMIT){
		return;
	}
	va_list args;

  va_start(args, __fmt);
  ffly_vfprintf(&(__log)->ioc_out,__fmt,args);
  va_end(args);
}

void y_log_fileend(struct log_struc *__log) {
	ffly_fclose(__log->_out);
}

void ffly_log_init(_16_u __log, void(*__out)(char*, _int_u)) {
	LOG(log, __log)

	log->out = __out;
	log->level = 0;
}

static char *buf;
static _int_u off;
void static _prep(_int_u __size) {
	buf = (char*)m_alloc(__size+1);
	off = 0;
}

void static _write(void *__buf, _int_u __size) {
	mem_cpy(buf+off, __buf, __size);
	off+=__size;
}

void ffly_log_write(_16_u __log, void *__buf, _int_u __size) {
	LOG(log, __log);
	log->out(__buf, __size);
}

mlock static lock = FFLY_MUTEX_INIT;
void static _log(_16_u __log, _64_u __info, char const *__format, va_list __args) {
	mt_lock(&lock);
	_64_u level;
	level = __info&LOG_LEVEL_BITS;
	LOG(log, __log);
	
	if (log->level != level) {
		return;
	}

	ffly_printin(__format, __args, _write, _prep);
	buf[off] = '\0';
	log->out(buf, off+1);
	m_free(buf);

	mt_unlock(&lock);
}

void static _ff_logv(_16_u __log, _64_u __info, char const *__format, va_list __args) {
	_log(__log, __info, __format, __args);
}

void _ff_log(_16_u __log, _64_u __info, char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	_log(__log, __info, __format, args);	
	va_end(args);
}
