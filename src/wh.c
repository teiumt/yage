#include "wh.h"
#include "ffly_def.h"
#include "m_alloc.h"
#include "slab_dealer.h"
#include "file.h"
#include "real_tree.h"
static struct file data;
static struct slab_dealer dealer;
static struct rtnode *stump;
void static _write_data(_ulonglong arg,_8_u *__buf,_64_u __size,_64_u __offset){
	bpwrite(&data,__buf,__size,__offset);
}
void static _read_data(_ulonglong arg,_8_u *__buf,_64_u __size,_64_u __offset){
	bpread(&data,__buf,__size,__offset);
}

static struct wh_file *file_head = NULL;
static _int_u nfiles = 0;

struct wh_file* wh_creat(_ulonglong __id){
	struct wh_file *a;
	a = m_alloc(sizeof(struct wh_file));
	*(void**)rtree_at(stump,__id) = a;
	a->info = WH_EXISTS;
	a->key = __id;
	a->next = file_head;
	a->sda = NULL;
	file_head = a;
	nfiles++;
	return a;
}

void static load(struct wh_hdr *hdr){
	dealer.offset = hdr->offset;
	if(hdr->nfree>0){
		_64_u *slabs;
		_int_u size;
		slabs = m_alloc(size= (hdr->nfree*sizeof(_64_u)));
		bpread(&data,slabs,size,hdr->free_slabs);
		_int_u i;
		i = 0;
		for(;i != hdr->nfree;i++){
			printf("free_slab%u: %x.\n",i,slabs[i]);
			sb_free(&dealer,slabs[i]);
		}
		m_free(slabs);
	}

	if(hdr->nfiles>0){
		struct wh_file_fossil *files;
		_int_u size;
		files = m_alloc(size = (hdr->nfiles*sizeof(struct wh_file_fossil)));
		bpread(&data,files,size,hdr->files);
		_int_u i;
		i = 0;
		for(;i != hdr->nfiles;i++){
			struct wh_file_fossil *fos;
			fos = files+i;
			printf("load file: info: %x, slabs: %u.\n",fos->info,fos->nslabs);
			struct wh_file *f;
			f = wh_creat(fos->key);
			f->info = fos->info;

			if(fos->nslabs>0){
				_64_u *slablocs;
				slablocs = m_alloc(size = (fos->nslabs*sizeof(_64_u)));
				bpread(&data,slablocs,size,fos->slabs);
				struct sd_area *area;
				area = m_alloc(sizeof(struct sd_area));
				area->slabs = slablocs;
				area->n_slabs = fos->nslabs;
				area->dealer = &dealer;
				printf("file backing, with %u-slabs.\n",area->n_slabs);
				f->sda = area;
			}
		}
		m_free(files);
	}
}


void wh_init(void){
	stump = rtree_new();
	dealer.read = _read_data;
	dealer.write = _write_data;
	sd_init(&dealer);
	dealer.offset = sizeof(struct wh_hdr);
	bopen(&data,"wh.data",_O_RDWR,_S_IRUSR|_S_IWUSR);

	struct wh_hdr hdr;
	bpread(&data,&hdr,sizeof(struct wh_hdr),0);
	if(hdr.bits&1){
		load(&hdr);
	}
}



void static store(void){
	struct wh_hdr hdr;
	hdr.offset = dealer.offset;
	hdr.nfree = dealer.n_inbin;
	hdr.nfiles = nfiles;
	_int_u write_size;
	_64_u end;
	end = dealer.offset;
	if(hdr.nfree>0){
		_64_u *free_slabs;
		free_slabs = m_alloc(hdr.nfree*sizeof(_64_u));
		struct sd_slab *sb;
		sb = dealer.sb_bin;
		_int_u i;
		i = 0;
		while(sb != NULL){
			free_slabs[i] = sb->offset;	
			i++;
			sb = sb->next;	
		}

		hdr.free_slabs = end;
		bpwrite(&data,free_slabs,write_size = (i*sizeof(_64_u)),end);
		end+=write_size;
		m_free(free_slabs);
	}

	if(nfiles>0){
		struct wh_file_fossil *files;
	
		files = m_alloc(nfiles*sizeof(struct wh_file_fossil));
		struct wh_file *f;
		f = file_head;
		_int_u idx = 0;
		while(f != NULL){
			struct wh_file_fossil *fos;
			fos = files+idx++;
			if(!f->sda){
				fos->nslabs = 0;
			}else{
				fos->nslabs = f->sda->n_slabs;
			}
			fos->key = f->key;
			fos->slabs = end;
			fos->info = f->info;
			if(f->sda != NULL){
				bpwrite(&data,f->sda->slabs,write_size = (fos->nslabs*sizeof(_64_u)),end);
				end+=write_size;
			}
			f = f->next;
		}

		hdr.files = end;
		bpwrite(&data,files,nfiles*sizeof(struct wh_file_fossil),end);
		m_free(files);
	}
	hdr.bits = 1;
	bpwrite(&data,&hdr,sizeof(struct wh_hdr),0);
}


void wh_deinit(void){
	store();
	bclose(&data);
}
void wh_stat(_ulonglong __id,_64_u *__stat){
	struct wh_file *a;
	a = *(void**)rtree_at(stump,__id);
	if(!a){
		*__stat = 0;
		return;
	}
	*__stat = a->info;
}

struct wh_file* wh_open(_ulonglong __id){
	struct wh_file *a;
	a = *(void**)rtree_at(stump,__id);
	if(a != NULL)
		return a;
	return wh_creat(__id);
}

void wh_backing(struct wh_file *a,_64_u __size){
	if(a->info&WH_BACKING){
		sd_realloc(a->sda,__size);	
	}else{
		a->sda = sd_alloc(&dealer,__size);
		a->info |= WH_BACKING;
	}
}

void wh_close(struct wh_file *a){
}

void wh_read(struct wh_file *a,void *__buf,_64_u __size,_64_u __offset){
	sd_read(a->sda,__buf,__size,__offset);
}
void wh_write(struct wh_file *a,void *__buf,_64_u __size,_64_u __offset){
	sd_write(a->sda,__buf,__size,__offset);
}
