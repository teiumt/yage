#define MSG_SUBDOM &frdom
#include "freight.h"
#include "mutex.h"
#include "ffly_def.h"
#include "thread.h"
#include "io.h"
#include "msg.h"
#include "clock.h"
#include "time.h"
#include "vat.h"
//#include "haze.h"
#include "tmu.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_CORE)
SUBDOM(frdom, "freight", 7)
#define INACTIVE 1
struct fr_struc {
	_64_u n;
	tstrucp t;
	_32_u sig;
	_8_u bits;
	_64_u d;
	float usage;
	struct fr_struc *next, **bk;
};

#define CO0 (main+1)
#define CO1 (main+2)
static struct fr_struc main[4];
struct {
	_64_u sig;
    _32_u n_act;
	_8_s kill;
	struct fr_struc *top;
	double maxuge;
	_8_u bits[8];
	_64_u at;
}control = {
	0,0,-1,NULL,0,{},0
};

#define MAXDE ((_64_u)1<<27)
#define IDENT (3)
/*
	add pass the pasel system
*/
static mlock lock = MUTEX_INIT;
#define regit(__bit)\
    __asm__("lock xorq %%rax, control(%%rip)\n" : : "a"(__bit));
void static
tick(struct fr_struc *__fr) {
	printf("hello freight thread.\n");
	struct y_timespec start, stop;
	struct y_vat *v;
	v = vt_new();
	regit(1<<__fr->n);
	y_clock_gettime(&start);
	while(control.kill == -1) {
		while(__fr->bits&INACTIVE) {
			doze(0, 8);	
			if (!control.kill) goto _exit;
		}
		y_clock_gettime(&stop);
		_64_u d;
		d = (stop.lower|(stop.higher<<27))-(start.lower|(start.higher<<27));
		y_clock_gettime(&start);
//		haze_tick(0);
//		vt_tick(v);
		doze((_64_u)(((double)CTSB/100.)*(7./(float)control.maxuge)), 0);

		__fr->usage = (float)d/(float)MAXDE;
		__fr->d = d;
	}
_exit:
	regit(1<<__fr->n);
	vt_destroy(v);

	printf("goodbye freight thread.\n");
}

#define attach(__fr)\
	if (control.top != NULL) {\
		control.top->bk = &__fr->next;\
	}\
	__fr->bk = &control.top;\
	__fr->next = control.top;\
	control.top = __fr;
#define deattch(__fr)\
	if (__fr->next != NULL) {\
		__fr->next->bk = __fr->bk;\
	}\
	*__fr->bk = __fr->next;
void static fr_new(struct fr_struc *__fr) {
	__fr->usage = 0;
	__asm__("leaq control(%%rip),%%rax\n"
			"movq $1,%%rbx\n"
			"lock xaddq %%rbx,8(%%rax)\n"
			"movq %%rbx,(%0)"
			: : "r"(__fr) : "rax","rbx");
	attach(__fr);
	tcreat(&__fr->t, (void*(*)(void*))tick, __fr);
	__fr->sig = __fr->t->sig;
}
static _8_s inited = -1;
void freight_init(void) {
	control.maxuge = 1;
	CO0->bits = INACTIVE;
	CO1->bits = INACTIVE;
	fr_new(main);
	fr_new(CO0);

	printf("FREIGHT: %u, %u.\n", main->sig, CO0->sig);
//	fr_new(CO1);
	MSG(INFO, "waiting on freights.\n")
	while(control.sig != IDENT);
	inited = 0;
}

void freight_de_init(void) {
	control.kill = 0;
	while(control.sig != 0) {
		doze(0, 2);
		printf("WAITING for %u to reach ZERO\n", control.sig);
	}
}

void freight_man(void) {
	if (inited == -1)return;
	struct fr_struc *cur;
	cur = control.top;
	float totuage = 0;	
	while(cur != NULL) {
		totuage+=cur->usage;
		cur = cur->next;
	}


	MSG(INFO, "USAGE: %f.\n", totuage*100)
	if (totuage>(control.maxuge*0.75)) {
		MSG(INFO, "FREIGHT: OVERLOAD.\n")
		// to activate
		control.maxuge++;
		struct fr_struc *tavt = &main[1]+(control.at++);
		tavt->bits^=INACTIVE;
	}
}
