# ifndef __y__mutex__h
# define __y__mutex__h
# include "types.h"
#define F_MUTEX_INIT 0
#define FFLY_MUTEX_INIT 0
#define MUTEX_INIT 0
#define MUTEX_LOCKED 1
#define F_DEF_MUTEX(__name)\
	mlock __name = FFLY_MUTEX_INIT;
void mt_lock(mlock*);
void mt_unlock(mlock*);
/*
	return

	0 means it locked
	-1 means failure
*/
_8_s mt_trylock(mlock*);
//void mt_refuse(mlock*);
# endif /*__y__mutex__h*/
