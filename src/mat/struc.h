#ifndef __y__mat__struc__h
#define __y__mat__struc__h
#include "../y_int.h"

struct f_mat_astrc { 
	// who is wanted to use
	_32_u clutch;
	_8_u r, g, b;
	_int_u pad_left;
	_int_u pad_top;
};

#endif /*__y__mat__struc__h*/
