#include "../y_int.h"
#include "mf.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../string.h"
#include "struc.h"
#include "mat.h"
#include "screen.h"
#define is_bit(__bits, __bit) \
	((__bits)&(__bit)==(__bit))

static _8_u *ptr;

static _8_u stack[4096];
struct f_mat_astrc astrc[NSEGS_A];
struct op {
	void(*func)(void);
	_8_u len;
};

struct frag {
	char *p;
	_int_u size;
	_int_u off;
	struct frag *link;
};

static _int_u block_c = 0;
struct block {
	struct frag *f;
	_int_u size;
	_int_u n;
	struct f_mat_attr *attr;
	struct block *next;
	struct block *bk;
};

static struct block *top = NULL, *end = NULL;
static struct block *curblock = NULL;
struct block static*
_new_block(void) {
	struct block *b;
	b = (struct block*)malloc(sizeof(struct block));
	if (!top) {
		top = b;
	}

	if (end != NULL) {
		end->next = b;
	}

	end = b;
	b->attr = (struct f_mat_attr*)malloc(sizeof(struct f_mat_attr));
	b->attr->bits = 0x00;
	b->next = NULL;
	b->size = 0;
	b->f = NULL;
	b->n = block_c;
	block_c++;
	return b;
}

void static _start(void) {
	_16_u vial;
	printf("start, vial: %u\n", vial = *(_16_u*)ptr);

	struct f_mat_astrc *s;
	s = astrc+vial;
	struct block *b;
	b = _new_block();
	b->bk = curblock;

	switch(s->clutch) {
		case _f_mat_ctc_pos:
			b->attr->pad_left = s->pad_left;
			b->attr->pad_top = s->pad_top;
			b->attr->bits |= _f_mat_at_pos;
		break;
		case _f_mat_ctc_colour:
			b->attr->r = s->r;
			b->attr->g = s->g;
			b->attr->b = s->b;
			b->attr->bits |= _f_mat_at_colour;
		break;
	}

	struct block *parent;
	parent = curblock;
	curblock = b;
	if (parent != NULL) {
		_8_u bits;
		bits = parent->attr->bits&~b->attr->bits;
		if (is_bit(bits, _f_mat_at_pos)) {
			b->attr->pad_left = parent->attr->pad_left;
			b->attr->pad_top = parent->attr->pad_top;
		}
		if (is_bit(bits, _f_mat_at_colour)) {
			b->attr->r = parent->attr->r;
			b->attr->g = parent->attr->g;
			b->attr->b = parent->attr->b;
		}
	}

	
	printf("r: %u, g: %u, b: %u, pad_left: %u, pad_top: %u\n", s->r, s->g, s->b, s->pad_left, s->pad_top);

}

void static _end(void) {
	if (!curblock) {
		printf("oh good fucking lord.\n");
		return;
	}
	curblock = curblock->bk;
}

void static _out(void) {
	if (!curblock) {
		printf("fuck off.\n");
		return;
	}
	char *p;
	p = (char*)(stack+*(_16_u*)ptr);

	struct frag *f;
	f = (struct frag*)malloc(sizeof(struct frag));

	f->link = curblock->f;
	curblock->f = f;

	_int_u len;
	len = *(_16_u*)(ptr+2);
	f->p = (char*)malloc(len);
	memcpy(f->p, p, len);
	f->size = len;
	f->off = curblock->size;
	curblock->size+=len;
}
#define MAX 2
static struct op ops[] = {
	{_start, 2},
	{_end, 0},
	{_out, 4}
};

void static _exec(_8_u *__ptr, _int_u __size) {
	_8_u *end;
	_8_u on;
	ptr = __ptr;
	end = ptr+__size;
	struct op *opp;
	while(ptr != end) {
		on = *(ptr++);
		if (on>MAX) {
			printf("illegal instruction, %x\n", on);
			return;
		}
		opp = ops+on;
		opp->func();

		ptr+=opp->len;
	}
}

void static _loadstrca(_int_u __dst, _8_u *__src, struct f_mf_astrc *__table) {
	struct f_mat_astrc *dst = astrc+__dst;

	dst->clutch = 	*(_32_u*)(__src+__table->fill[_f_mf_as_clutch]);
	dst->r =		*(_32_u*)(__src+__table->fill[_f_mf_as_r]);
	dst->g =		*(_32_u*)(__src+__table->fill[_f_mf_as_g]);
	dst->b =		*(_32_u*)(__src+__table->fill[_f_mf_as_b]);	
	dst->pad_left =	*(_32_u*)(__src+__table->fill[_f_mf_as_pad_left]);
	dst->pad_top =	*(_32_u*)(__src+__table->fill[_f_mf_as_pad_top]);
}


void static _finish(void) {
	if (!block_c)
		return;
	struct f_mat_block *blks;

	blks = (struct f_mat_block*)malloc(block_c*sizeof(struct f_mat_block));
	struct f_mat_block *d;
	struct block *b, *next;
	b = top;
	while(b != NULL) {
		d = blks+b->n;
		d->size = b->size;
		if (b->size>0) {
			d->p = (char*)malloc(b->size);
			d->attr = b->attr;
			struct frag *f;
			f = b->f;
			while(f != NULL) {
				if (f->size>0)
					memcpy(d->p+f->off, f->p, f->size);
				free(f->p);
				f = f->link;
			}
		}

		next = b->next;
		free(b);
		b = next;
	}

	f_mat_scrn_place(blks, block_c);

}

# include "../hexdump.h"
#define showhdr(__h)\
	printf("HEADER: src: %u, len: %u, vial: %u, n_vial: %u, n_seg: %u\n", __h->src, __h->len, __h->vial, __h->n_vial, __h->n_seg);
void f_mat_exec(_8_u *__ptr, _int_u __size) {
	struct f_mf_hdr *h;
	h = (struct f_mf_hdr*)__ptr;
	showhdr(h)
	struct f_mf_vial *se;
	se = (struct f_mf_vial*)(__ptr+h->vial);
	_int_u i;
	i = 0;
	for(;i != h->n_vial;i++){
		printf("vial: N: %u, src: %u, size: %u.\n", se->n, se->src, se->size);
		_loadstrca(se->n, __ptr, &se->as);
		se++;
	}

	struct f_mf_seg *seg;
	seg = (struct f_mf_seg*)(__ptr+h->seg);
	i = 0;
	for(;i != h->n_seg;i++) {
		printf("dst: %u, src: %u, size: %u.\n", seg->dst, seg->src, seg->size);
		if (seg->size>0) {
			memcpy(stack+seg->dst, __ptr+seg->src, seg->size);
		}
		seg++;
	}

	ffly_hexdump(__ptr+h->src, h->len);
	_exec(__ptr+h->src, h->len);
	_finish();
}
