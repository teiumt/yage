# ifndef __mat__mf__h
# define __mat__mf__h
# include "../y_int.h"
# include "mat.h"
enum {
	_f_mf_as_clutch,
	_f_mf_as_r,
	_f_mf_as_g,
	_f_mf_as_b,
	_f_mf_as_pad_left,
	_f_mf_as_pad_top
};

struct f_mf_astrc {
	_32_u fill[SEGA_EC];
};

struct f_mf_vial {
	_32_u src, size;
	_16_u n;
	struct f_mf_astrc as;
};

struct f_mf_seg {
	_32_u src, size;
	_32_u dst;
};

struct f_mf_hdr {
	_32_u src, len;
	_32_u vial, n_vial;
	_32_u seg, n_seg;
};

# endif /*__mat__mf__h*/
