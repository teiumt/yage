#include "../string.h"
#include "../m_alloc.h"
# include "mas.h"
# include "mat.h"
_8_u static outbuf[4096];
_int_u static ob_ptr = 0;
char const static *p, *end;
static _ulonglong _iaf(_8_u __what, _ulonglong __arg) {
	return p>=end;
}


static void _wto(void *__buf, _int_u __size, _32_u __offset) {
	memcpy(outbuf+__offset, __buf, __size);
}
static _int_u _in(void *__buf, _int_u __size) {
	_int_u left;
	left = end-p;
	if (!left) {
		return 0;
	}

	if (__size>left) {
		memcpy(__buf, p, left);
		p = end;
		return left;
	}
	
	memcpy(__buf, p, __size);
	p+=__size;
	return __size;
}

static _int_u _out(void *__buf, _int_u __size) {
	_8_u *d;
	d = outbuf+ob_ptr;
	ob_ptr+=__size;
	memcpy(d, __buf, __size);
	return 0;
}

# include "../hexdump.h"
# include "../chrdump.h"
# include "../linux/unistd.h"
# include "../linux/stat.h"
# include "../linux/fcntl.h"
# include "screen.h"
void f_lmn_exec(_8_u*, _int_u);
_f_err_t ffmain(int __argc, char const *__argv[]) {
	f_mat_scrn_init();

	int fd;
	fd = open("mat.out", O_RDONLY, 0);

	struct stat st;
	fstat(fd, &st);
	void *bed;
	p = bed = malloc(st.st_size);
	read(fd, bed, st.st_size);
	printf("filesize: %u.\n", st.st_size);

	end = ((char const*)bed)+st.st_size-1;
	_f_mas_asc.ptr = &ob_ptr;
	_f_mas_asc.wto = _wto;
	_f_mas_asc.in = _in;
	_f_mas_asc.out = _out;
	_f_mas_asc.iaf = _iaf;
//	struct f_mc_af af = {
//		_in,
//		_out,
//		_iaf
//	};

//	struct f_mc_common mcc = {
//		.funcs = &af	
//	};
//	f_mat_compile(&mcc);
	f_mas_init();
	f_mat_as();
	f_mas_de_init();
	ffly_hexdump(outbuf, ob_ptr);
	ffly_chrdump(outbuf, ob_ptr);
	f_mat_exec(outbuf, ob_ptr);
	f_mat_scrn_show();
	free(bed);
	close(fd);

//	fd = open("mat.out", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
//	write(fd, outbuf, ob_ptr);
//	close(fd);

}

