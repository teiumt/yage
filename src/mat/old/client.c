# include "../mtp.h"
# include "../mat.h"
# include "../net.h"
# include "../inet.h"
# include "../dep/bzero.h"
# include "../memory/mem_free.h"
# include "../system/err.h"
# include "../system/io.h"
# include "../dep/str_len.h"

void* ffly_mtp_req(FF_SOCKET *__sock, char const *__file, _int_u *__size) {
	_int_u len;
	_f_err_t err;
	len = f_str_len(__file);

	_8_u code[10];
	*code = 0x01;
	*(_16_u*)(code+1) = 0;
	*(_16_u*)(code+3) = len;
	*(code+5) = 0x00;
	*(_16_u*)(code+6) = 0;
	*(_16_u*)(code+8) = len;

	_32_u tsz;
	tsz = 10;
	ff_net_send(__sock, &tsz, sizeof(_32_u), 0, &err);
	ff_net_send(__sock, code, 10, 0, &err);

	ff_net_send(__sock, __file, len, 0, &err);
	return ffly_mtp_rcv(__sock, __size, &err);
}

_f_err_t ffmain(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("please provide file.\n");
		retok;
	}

	_f_err_t err;
	FF_SOCKET *sock = ff_net_creat(&err, _NET_PROT_TCP);
	if (_err(err)) {
		ffly_fprintf(ffly_err, "failed to crate socket.\n");
		_ret;
	}

	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	addr.sin_family = AF_INET;
	addr.sin_port = htons(21299);
	if (_err(err = ff_net_connect(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)))) {
		ffly_fprintf(ffly_err, "failed to connect.\n");
		goto _end;
	}

	void *p;
	_int_u size;
	
	p = ffly_mtp_req(sock, __argv[1], &size);

	struct ffly_mat mat;
	mat.p = p;
	mat.end = mat.p+size;

	ffly_matact(&mat);

	__f_mem_free(p);	
_end:
	ff_net_close(sock);
	_ret;
}
