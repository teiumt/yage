# ifndef __ffly__mat__h
# define __ffly__mat__h
# include "../../y_int.h"
# include "../../ffly_def.h"
# include "../../types.h"
typedef struct ffly_mat {
	char const *p, *end;
} *ffly_matp;

void ffly_matact(ffly_matp);
# endif /*__ffly__mat__h*/
