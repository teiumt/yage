# ifndef __f__mat__screen__h
# define __f__mat__screen__h
#include "../y_int.h"
#include "mat.h"
void f_mat_scrn_cleanup(void);
void f_mat_scrn_show(void);
void f_mat_scrn_init(void);
void f_mat_scrn_place(struct f_mat_block*, _int_u);
# endif /*__f__mat__screen__h*/
