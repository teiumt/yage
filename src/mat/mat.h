#ifndef __mat__h
#define __mat__h
#include "../y_int.h"
#define NSEGS_A 1
#define SEGA_EC 6
#define NCTC 2
enum {
	_f_mat_ctc_pos,
	_f_mat_ctc_colour
};

enum {
	_f_mc_tk_keychr,
	_f_mc_tk_ident,
	_f_mc_tk_num,
	_f_mc_tk_text
};

enum {
	_f_mc_tv_lt,
	_f_mc_tv_gt,
	_f_mc_tv_colon,
	_f_mc_tv_slash,
	_f_mc_tv_comma
};

enum {
	_f_mc_n_tag,
	_f_mc_n_tag_end
};

struct f_mc_tag_elem {
	char const *attr_name;
	_int_u attr_nlen;
	char const *attr_value;
	_int_u attr_vlen;
};

typedef struct f_mc_token {
	_8_u kind;
	_8_u val;
	void *p;
	_int_u len;
} *f_mc_tokenp;

typedef struct f_mc_node {
	_8_u id;
	struct f_mc_tag_elem tag_elems[26];
	_int_u tag_ec;
	struct f_mc_node *next;
	char const *name;
	_int_u nlen;
	// after text
	char *at;
	_int_u at_len;
} *f_mc_nodep;

struct f_mc_af {
	_int_u(*in)(void*, _int_u);
	_int_u(*out)(void*, _int_u);
	_ulonglong(*iaf)(_8_u, _ulonglong);
};

#define _f_mat_at_pos 1
#define _f_mat_at_colour 2 
struct f_mat_attr {
	_8_u r, g, b;
	_8_u bits;
	_int_u pad_left;
	_int_u pad_top;
};

struct f_mat_block {
	char *p;
	_int_u size;
	struct f_mat_attr *attr;
};

struct f_mc_common {
	struct f_mc_af *funcs;
};
void f_mc_ulex(f_mc_tokenp);
void f_mat_compile(struct f_mc_common*);
f_mc_nodep f_mc_parser(struct f_mc_common*);
void f_mc_emit(struct f_mc_common*, f_mc_nodep);
f_mc_tokenp f_mc_nexttok(struct f_mc_common*);
_8_s f_mc_tokexpect(struct f_mc_common*, _8_u, _8_u);
_8_s f_mc_nexttokis(struct f_mc_common*, _8_u, _8_u);
#endif /*__mat__h*/
