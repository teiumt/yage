# include "mat.h"
# include "../mort/malloc.h"
# include "../mort/stdio.h"
# include "../mort/string.h"
static char cbuf[64];
static char *cbp = cbuf;
char f_mc_getchr(struct f_mc_common *__comm) {
	char c;
	if (cbp>cbuf) {
		c = *(--cbp);
		return c;
	}
	if (__comm->funcs->in(&c, 1) != 1) {
		printf("error.\n");
		return 0;
	}
	return c;
}

void f_mc_retract(char __c) {
	*(cbp++) = __c;
}

char const static*
read_ident(struct f_mc_common *__comm, _int_u *__l) {
	char buf[128];
	char *bufp = buf;
	char c = f_mc_getchr(__comm);
	while(c>='a'&&c<='z') {
		*(bufp++) = c;
		c = f_mc_getchr(__comm);
	}
	f_mc_retract(c);
	*bufp = '\0';
	*__l = bufp-buf;
	char const *ret = strdup(buf);
	return ret;
}

char const static*
read_no(struct f_mc_common *__comm, _int_u *__l) {
	char buf[128];
	char *bufp = buf;
	char c = f_mc_getchr(__comm);
	while(c>='0'&&c<='9') {
		*(bufp++) = c;
		c = f_mc_getchr(__comm);
	}
	f_mc_retract(c);
	*bufp = '\0';
	*__l = bufp-buf;
	char const *ret = strdup(buf);
	return ret;
}

static f_mc_tokenp tokbuf[24];
static f_mc_tokenp *nxttok = tokbuf;

void
f_mc_ulex(f_mc_tokenp __tok) {
	*(nxttok++) = __tok;
}
#define emptyspace(__c) (__c == ' ' || __c == '\t' || __c == '\n')
void static _rtext(struct f_mc_common*);

f_mc_tokenp f_mc_lex(struct f_mc_common *__comm) {
	if (nxttok>tokbuf)
		return *(--nxttok);

	f_mc_tokenp tok;
	char c;
	tok = (f_mc_tokenp)malloc(sizeof(struct f_mc_token));
	
_skipc:
	if (__comm->funcs->iaf(0, 0) && cbp == cbuf)
		return NULL;

	c = f_mc_getchr(__comm);
	if (emptyspace(c))
		goto _skipc;


	switch(c) {
		case '<':
			tok->kind = _f_mc_tk_keychr;
			tok->val = _f_mc_tv_lt;
		break;
		case '>':
			tok->kind = _f_mc_tk_keychr;
			tok->val = _f_mc_tv_gt;
			_rtext(__comm);
		break;
		case ':':
			tok->kind = _f_mc_tk_keychr;
			tok->val = _f_mc_tv_colon;
		break;
		case '/':
			tok->kind = _f_mc_tk_keychr;
			tok->val = _f_mc_tv_slash;
		break;
		case ',':
			tok->kind = _f_mc_tk_keychr;
			tok->val = _f_mc_tv_comma;
		break;
		default:
		f_mc_retract(c);
		if (c>='a'&&c<='z') {
			tok->kind = _f_mc_tk_ident;
			tok->p = (void*)read_ident(__comm, &tok->len);
		} else if (c>='0'&&c<='9') {
			tok->kind = _f_mc_tk_num;
			tok->p = (void*)read_no(__comm, &tok->len);
		}
	}

	return tok;

}

f_mc_tokenp f_mc_nexttok(struct f_mc_common *__comm) {
	f_mc_tokenp tok;
	tok = f_mc_lex(__comm);

	return tok;
}

_8_s f_mc_tokexpect(struct f_mc_common *__comm, _8_u __kind, _8_u __value) {
	f_mc_tokenp tok;
	if (!(tok = f_mc_nexttok(__comm))) {
		return -1;
	}
	if (tok->kind == __kind && tok->val == __value) {
		return 0;
	}
	f_mc_ulex(tok);
	return -1;

}

_8_s f_mc_nexttokis(struct f_mc_common *__comm, _8_u __kind, _8_u __value) {
	f_mc_tokenp tok;
	if (!(tok = f_mc_nexttok(__comm))) {
		return -1;
	}
	if (tok->kind == __kind && tok->val == __value) {
		return 0;
	}
	f_mc_ulex(tok);
	return -1;


}

void
_rtext(struct f_mc_common *__comm) {
	char buf[1024];
	char *p;
	p = buf;
	
	char c;
_again:
	if (__comm->funcs->iaf(0, 0) && cbp == cbuf)
		return;
	c = f_mc_getchr(__comm);
	if (c != '<') {
		*(p++) = c;
		goto _again;
	}
	f_mc_retract(c);

	_int_u l;
	l = p-buf;

	if (l>0) {
		f_mc_tokenp tok;
		tok = (f_mc_tokenp)malloc(sizeof(struct f_mc_token));
		tok->kind = _f_mc_tk_text;
		tok->len = l;
		char *r;
		memdup(&r, buf, l);
		tok->p = r;
		*(nxttok++) = tok;
	}
}

void f_mat_compile(struct f_mc_common *__comm) {
	f_mc_nodep n;
	n = f_mc_parser(__comm);
	if (!n){
		return;
	}
	f_mc_emit(__comm, n);

}
