rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd mat
ffly_objs="$ffly_objs mas.o screen.o mas_exp.o mas_op.o test.o m_exc.o mc_emit.o mc_parser.o mc_reader.o"
gcc $cc_flags -c -o mas.o mas.c
gcc $cc_flags -c -o mas_exp.o mas_exp.c
gcc $cc_flags -c -o test.o test.c
gcc $cc_flags -c -o mas_op.o mas_op.c
gcc $cc_flags -c -o m_exc.o m_exc.c
gcc $cc_flags -c -o mc_emit.o mc_emit.c
gcc $cc_flags -c -o mc_parser.o mc_parser.c
gcc $cc_flags -c -o mc_reader.o mc_reader.c
gcc $cc_flags -c -o screen.o screen.c
gcc $cc_flags -o ffmat $ffly_objs -nostdlib
