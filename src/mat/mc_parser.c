#include "mat.h"
#include "../m_alloc.h"
#include "../io.h"

f_mc_nodep static
_new_node(void) {
	return (f_mc_nodep)malloc(sizeof(struct f_mc_node));	
}

void static
parser_tag_element(struct f_mc_common *__comm, f_mc_tokenp __tok, struct f_mc_tag_elem *__elem) {
	if (__tok->kind == _f_mc_tk_ident) {
		if (f_mc_tokexpect(__comm, _f_mc_tk_keychr, _f_mc_tv_colon) == -1) {
			// error
			printf("expected colon.\n");
		}

		f_mc_tokenp val;
		val = f_mc_nexttok(__comm);
		
		__elem->attr_name = __tok->p;
		__elem->attr_nlen = __tok->len;
		__elem->attr_value = val->p;
		__elem->attr_vlen = val->len;
	}
}

f_mc_nodep static
parser_tag(struct f_mc_common *__comm) {
	f_mc_tokenp tok, drtv;
	f_mc_nodep n;
	n = _new_node();

	if (!f_mc_nexttokis(__comm, _f_mc_tk_keychr, _f_mc_tv_slash)) {
		if (f_mc_tokexpect(__comm, _f_mc_tk_keychr, _f_mc_tv_gt) == -1) {
			printf("expected tag end.\n");
			return NULL;
		}
		n->id = _f_mc_n_tag_end;
		return n;
	}

	drtv = f_mc_nexttok(__comm);
	n->name = drtv->p;
	n->nlen = drtv->len;
	_int_u i;
	i = 0;
_again:
	tok = f_mc_nexttok(__comm);
	if (!tok) {
		while(1);
	}
	if (!(tok->kind == _f_mc_tk_keychr && tok->val == _f_mc_tv_gt)) {
		parser_tag_element(__comm, tok, n->tag_elems+i);
		

		if (f_mc_nexttokis(__comm, _f_mc_tk_keychr, _f_mc_tv_comma) == -1) {
			printf("dident get comma.\n");
		}
		i++;
		goto _again;
	}
	n->tag_ec = i;
	n->id = _f_mc_n_tag;
	return n;
}

f_mc_nodep f_mc_parser(struct f_mc_common *__comm) {
	f_mc_tokenp tok;
/*
_a:
	tok = f_mc_nexttok(__comm);

	if (tok != NULL) {
		printf("%u, %u.\n", tok->kind, tok->val);
		goto _a;
	}
	
return NULL;
*/
	f_mc_nodep n, top = NULL, end = NULL;
_again:
	if (!(tok = f_mc_nexttok(__comm)))
		return top;
	if (tok->kind == _f_mc_tk_keychr && tok->kind == _f_mc_tv_lt) {
		n = parser_tag(__comm);
		n->at_len = 0;
		tok = f_mc_nexttok(__comm);
		if (tok != NULL) {
			if (tok->kind == _f_mc_tk_text) {
				n->at_len = tok->len;
				n->at = tok->p;
			} else {
				f_mc_ulex(tok);
			}
		}
	}

	if (!n) {
		printf("error.\n");
		return NULL;
	}
	n->next = NULL;
	if (!top)
		top = n;
	if (end != NULL)
		end->next = n;
	end = n;

	if (!__comm->funcs->iaf(0, 0)) {
		goto _again;
	}
	return top;
}
