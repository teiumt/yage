# include "screen.h"
# include "../mort/stdio.h"
# include "../mort/malloc.h"
# include "../mort/string.h"
#define is_bit(__bits, __bit) \
	((__bits)&(__bit)==(__bit))


struct frag {
	char *p;
	_int_u l;
	_int_u off;
};

struct line {
	struct frag *frags[20];
	struct frag **p;
	_int_u off;
};

# define new_frag \
{					   \
	struct line *ln;	\
	ln = *(frame+row);  \
	f = (*(ln->p++) = (struct frag*)malloc(sizeof(struct frag)));   \
}

# define line_save \
{					   \
	struct line *ln;	\
	ln = *(frame+row);  \
	new_frag			\
	if ((f->l = col) > 0) {			 \
		memdup((void*)&f->p, buf, f->l);	\
		f->off = ln->off+dis;			   \
		ln->off+=f->l;					  \
	}									   \
}
static struct frag *f;

#define clr memset(buf, '.', 1024);
# define HEIGHT 60
static struct line *frame[HEIGHT];
static char buf[1024];
static _int_u col = 0, row = 0;
static _int_u dis = 0;
void f_mat_scrn_place(struct f_mat_block *__blks, _int_u __n) {
	struct f_mat_block *b, *e;
	struct f_mat_attr *attr;
	b = __blks;
	e = b+__n;
	while(b != e) {
	if (!b->size) {
		b++;
		continue;
	}
	attr = b->attr;

	if (is_bit(attr->bits, _f_mat_at_pos)) {
		row = attr->pad_top;
		col = attr->pad_left;


	}

	_8_s rc = -1;
	if ((attr->bits&_f_mat_at_colour)>0) {
		rc = 0;
		char *p;
		p = buf+col;
		p+=f_str_cpy(p, "\e[38;2;");
		p+=ffly_nots(attr->r, p);
		*(p++) = ';';
		p+=ffly_nots(attr->g, p);
		*(p++) = ';';
		p+=ffly_nots(attr->b, p);
		*(p++) = 'm';
		col+=p-(buf+col);//too lazy
	}


	_int_u i;
	i = 0;
	char c;
	for(;i != b->size;i++) {
		c = b->p[i];
		if (c == ';') {
			line_save;
			row++;
			col = 0;
			clr
			continue;
		}

		*(buf+col++) = c;
	}

	if (!rc) {
		memcpy(buf+col, "\e[0m", 4);
		col+=4;
	}
	line_save
	col = 0;
	clr
	dis = 0;
	b++;
	}
}

void f_mat_scrn_show(void) {
	struct line **p = frame;
	struct line **e = p+HEIGHT;
	_int_u off;
	struct line *ln;
	while(p != e) {
		ln = *(p++);
		off = 0;
		if (*ln->frags != NULL) {
			struct frag **cur = ln->frags;
			while(*cur != NULL) {
				struct frag *f = *cur;
				if (f->l>0) {
					_int_u bias;
					bias = off-f->off;
					_int_u l = f->l-bias;
					char *p = f->p+bias;
					if (l>1024) {
						printf("error.\n");
					} else
						ffly_fwrite(_ffly_out, p, l);
					off+=l;
				}
				cur++;
			}
		}
		printf("\n");
	}
}

void
f_mat_scrn_cleanup(void) {
	struct line **p = frame;
	struct line **e = p+HEIGHT;
	struct line *ln;
	while(p != e) {
		ln = *(p++);
		struct frag **cur = ln->frags;
		while(*cur != NULL) {
			struct frag *f = *cur;
			if (f->l>0)
				free(f->p);
			free(f);
			cur++;
		}
		free(ln);
	}
}

void f_mat_scrn_init(void) {
	clr
	struct line **p = frame;
	struct line **e = p+HEIGHT;
	struct line *ln;
	while(p != e) {
		ln = (*(p++) = (struct line*)malloc(sizeof(struct line)));
		memset(ln->frags, 0, sizeof(ln->frags));
		ln->p = ln->frags;
		ln->off = 0;
	}

}
