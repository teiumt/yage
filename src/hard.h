#ifndef __y__hard__h
#define __y__hard__h
#include "y_int.h"
struct y_hard {
	_8_u *p;
};

struct y_hard* hard_alloc(_64_u);
void hard_free(struct y_hard*);
void hard_read(struct y_hard*, void*, _32_u, _64_u);
void hard_write(struct y_hard*, void*, _32_u, _64_u);
#endif /*__y__hard__h*/
