#ifndef __graph__h
#define __graph__h
#include "file.h"

struct graph{
	struct file f;
	_64_u x;
};
void graph_ploty(struct graph*,_64_u);
void graph_plot(struct graph*,_64_u,_64_u);
void graph_open(struct graph*,char const*);
void graph_close(struct graph*);
#endif/*__graph__h*/
