# include "fs.h"
# include "fs/m/m.h"
# include "ffly_def.h"
# include "fs/mkfs.h"
# include "system/io.h"
struct ff_fso *__ff_fso__;
# include "fs_sm.h"
static struct f_fsus_m m = {
	.flags = 0x00
};
struct ffly_mfs _mfs = {
	.m = &m,
	.top = NULL, .bin = NULL,
	.off = 0
};

void static
_out(void * __arg, void *__buf, _int_u __size) {
	((f_fsus_mp)__arg)->f->write(((f_fsus_mp)__arg)->ctx, __buf, __size, 0);
}

void static
_mfs_(void) {
	f_fs_sm(&m, 0);
	if ((m.flags&F_FSUS_FN)>0) {
		printf("FS formatting needed.\n");
		f_mkfs_m(_out, (void*)&m);
	}
	mfs = &_mfs;
	ffly_mfs();
}

static void(*load[])(void) = {
	_mfs_
};

static void(*de_init[])(void) = {
	ffly_mfs_de_init
};

# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
f_vmfs_nodep f_vmfs_open(char const *__path, _32_u __flags, _32_u __mode) {
	f_vmfs_nodep n;
	n = (f_vmfs_nodep)__f_mem_alloc(sizeof(struct f_vmfs_node));

	struct ff_fso *fso = __ff_fso__;
	fso->open(n, __path, __flags, __mode);
	n->fso = fso;
	return n;
}

void f_vmfs_close(f_vmfs_nodep __n) {
	__n->fso->close(__n);
	__f_mem_free(__n);
}
/*
	function that returns a function pointer
*/
void(*ffly_fs(_8_u __what))(void) {
	load[__what]();
	return de_init[__what];
}
