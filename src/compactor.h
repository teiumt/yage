# ifndef __f__compactor__h
# define __f__compactor__h
# include "y_int.h"
# include "types.h"
typedef struct f_compactor {
	void(*put)(void*, _int_u);
	void(*get)(void*, _int_u);

	void(*reset)(void);
	_8_i(*at_eof)(void);

	_8_u buf;
	_8_u left;
} *f_compactorp;

void f_compact(f_compactorp);
void f_decompact(f_compactorp);
# endif /*__f__compactor__h*/
