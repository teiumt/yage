# ifndef __yc__h
# define __yc__h
# include "../y_int.h"
# include "../codes.h"
typedef struct fc_sym {
	union {
		struct {
			char *name;
			_int_u len;
			_64_u off;
		};
		// numeric ident
		_int_u ident;
	};
	_8_u local;
} *fc_symp;

typedef struct fc_opi {
	char const *s;
	_int_u size;
	_int_u len;
} *fc_opip;
#include "cfm/it.h"
#include "cfm/cfm.h"
#include "clang/clang.h"

#define FC_STR_MAX 24
#define FC_SA_PAGE_SHFT 4
#define FC_SA_PAGE_SIZE (1<<FC_SA_PAGE_SHFT)
#define FC_SA_PAGE_MASK (FC_SA_PAGE_SIZE-1)
#define FC_SA_FPS (FC_STR_MAX*FC_SA_PAGE_SIZE)
# include "../bbs.h"
struct yc_file {
	_64_u ptr, limit;
	int fd;
	_8_u state;
	struct bbs_ops *ops;
};
#define LL_EXISTING 1
struct yc_core {
	char *ll_buf;
	_int_u llbn;
	_64_u llbits;
	_64_u remnants;

	char ll_buf0[0x100];
	_int_u ll_bufi;
};

#define YC_OPBS 0x100
struct _fc_struc {
	void(*open)(struct yc_file*, char const*);
	void(*close)(struct yc_file*);
	struct bbs_ops ops;
	struct yc_file *file;

	struct {
		_64_u ptr;
		int fd;
		_8_u buf[YC_OPBS];
		_int_u bp;
		_ulonglong arg;
	} out;

	// string allocator
	struct {
		_8_i inited;
		_64_u off, page_c;
		void **pages;
	} saloc;

	struct f_clg_c c;
	struct f_cfm cfm;
	_32_u lid;
	

	struct yc_core core;
};

struct yc_opt {
	char const *str;
	_int_u len;
};

#define _ll_tok_ident	0
#define _ll_tok_str		1
#define _ll_tok_dir		2
#define _ll_tok_keywd	3
struct ll_token {
	_8_u kind;
	_8_u id;

	union {
		struct {
			char *s;
			_int_u len;
		};
		char c;
	};
};
/*
	TWO-OUTPUTS

	_yc_write			(NON-BUFFERD)
	yc_write			(BUFFERD)
*/
#define clg_struc fc_struc.c
#define cfm_struc fc_struc.cfm
extern struct _fc_struc fc_struc;
#define yc_pwrite		fc_struc.ops.out.pwrite
_8_u yc_read(struct yc_file*, void*, _int_u);
//void yc_pwrite(_ulonglong, void*, _int_u, _64_u);
//void _yc_write(_ulonglong, void*, _int_u);
struct ll_token* ll_lex(void);
void ll_lex_end(void);
struct yc_file* yc_open(char const*);
void yc_close(struct yc_file*);
void _yc_write(_ulonglong, void*, _int_u);
void yc_write(void*, _int_u);
void yc_seek(_ulonglong, _64_u);
void yc_drain(void);
void yc_init(void);
void yc_cleanup(void);
fc_symp fc_sym_new(char const*, _int_u);
void yc_fileout(char const*);
# endif /*__yc__h*/
