# include "vec.h"
# include "../io.h"
# include "../m_alloc.h"
# include "../string.h"
#define PAGE_SHIFT 4
#define PAGE_SIZE (1<<PAGE_SHIFT)
#define PAGE_MASK (PAGE_SIZE-1)
yc_vecp yc_vec_new(_int_u __blksz) {
	yc_vecp v;
	v = (yc_vecp)m_alloc(sizeof(struct yc_vec));
	v->page_c = 1;
	v->pages = (void**)m_alloc(sizeof(void*));
	*v->pages = m_alloc(PAGE_SIZE*__blksz);
	v->off = 0;
	v->blksz = __blksz;
	return v;
}

void yc_vec_destroy(yc_vecp __v) {
	if (__v->page_c>0) {
		_int_u i;
		i = 0;
		for(;i != __v->page_c;i++) {
			m_free(*(__v->pages+i));
		}
	
		m_free(__v->pages);
	}
	m_free(__v);
}

void *yc_vec_getat(yc_vecp __v, _int_u __where) {
	_int_u pg, pg_off;
	pg = __where>>PAGE_SHIFT;
	pg_off = __where&PAGE_MASK;
	return ((_8_u*)*(__v->pages+pg))+(pg_off*__v->blksz);

}
_int_u yc_vecsize(yc_vecp __v) {
	return __v->off;
}

void yc_vec_push(yc_vecp __v, void **__data) {
	_int_u pg, pg_off;
	void *d;
	pg = __v->off>>PAGE_SHIFT;
	pg_off = __v->off&PAGE_MASK;

	if (pg>=__v->page_c) {
		__v->pages = (void*)m_realloc(__v->pages, (++__v->page_c)*sizeof(void*));
	} else 
		goto _sk;

	*(__v->pages+pg) = m_alloc(PAGE_SIZE*__v->blksz);
_sk:
	d = ((_8_u*)*(__v->pages+pg))+(pg_off*__v->blksz);
	*__data = d;
	__v->off++;
}

void yc_vec_pop(yc_vecp __v, void *__data) {
	void *s;
	_int_u pg, pg_off;
	pg = __v->off>>PAGE_SHIFT;
	pg_off = __v->off&PAGE_MASK;

	s = ((_8_u*)*(__v->pages+pg))+(pg_off*__v->blksz);
	mem_cpy(__data, s, __v->blksz);
	if (pg<__v->page_c-1 && __v->page_c>1) {
		m_free(*(__v->pages+pg));
		__v->pages = (void*)m_realloc(__v->pages, (--__v->page_c)*sizeof(void*));
	}
	__v->off--;
}

