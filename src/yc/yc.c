# include "yc.h"
# include "../m_alloc.h"
# include "../string.h"
#include "../io.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../linux/stat.h"
void static*
_alloc(void) {
	if (fc_struc.saloc.inited == -1) {
		fc_struc.saloc.pages = (void**)m_alloc(sizeof(void*));
		*fc_struc.saloc.pages = m_alloc(FC_SA_FPS);
		fc_struc.saloc.inited = 0;
	}
	_int_u pg, pg_off;
	pg = fc_struc.saloc.off>>FC_SA_PAGE_SHFT;
	pg_off = fc_struc.saloc.off&FC_SA_PAGE_MASK;

	if (pg>=fc_struc.saloc.page_c) {
		fc_struc.saloc.pages = (void**)m_realloc(fc_struc.saloc.pages, (++fc_struc.saloc.page_c)*sizeof(void*));
		*(fc_struc.saloc.pages+pg) = m_alloc(FC_SA_FPS);
	}

	fc_struc.saloc.off++;
	return ((_8_u*)*(fc_struc.saloc.pages+pg))+(pg_off*FC_STR_MAX);
}

fc_symp fc_sym_new(char const *__str, _int_u __len) {
	fc_symp s;
	s = (fc_symp)m_alloc(sizeof(struct fc_sym));
	s->local = 0;
	if (__str != NULL) {
		s->off = fc_struc.saloc.off*FC_STR_MAX;
		s->name = _alloc();
		mem_cpy(s->name, __str, s->len = __len);
	}
	return s;
}

void static _read(struct yc_file *__f, void *__buf, _int_u __size) {
    read(__f->fd, __buf, __size);
}

static struct bbs_ops file_ops = {
	_read
};

struct yc_file*
yc_open(char const *__path) {
	struct yc_file *f;
	f = (struct yc_file*)m_alloc(sizeof(struct yc_file));
	f->state = 0x00;
	fc_struc.open(f, __path);
	f->ops = &file_ops;
	return f;
}

void yc_close(struct yc_file *__f) {
	fc_struc.close(__f);
	m_free(__f);
}

void static _open(struct yc_file *__f, char const *__path) {
	__f->fd = open(__path, O_RDONLY, 0);
	__f->ptr = 0;

	struct stat st;
	fstat(__f->fd, &st);
	__f->limit = st.st_size;
	printf("opening file-'%s' with size of: %u-bytes.\n", __path, st.st_size);
}

void static _close(struct yc_file *__f) {
	close(__f->fd);
}

void static _seek(_ulonglong __arg, _64_u __offset) {
	lseek(fc_struc.out.fd, __offset, SEEK_SET);
}

void static _write(_ulonglong __arg, void *__buf, _int_u __size) {
	write(fc_struc.out.fd, __buf, __size);
}

void static _pwrite(_ulonglong __arg, void *__buf, _int_u __size, _64_u __offset) {
	pwrite(fc_struc.out.fd, __buf, __size, __offset);
}

void yc_init(void) {
	fc_struc.lid = 0;
	fc_struc.core.remnants = 0;
	fc_struc.core.llbn = 0;
	fc_struc.core.llbits = 0;
	fc_struc.core.ll_bufi = 0;
}
void yc_fileout(char const *__file) {
	//are output file
	fc_struc.out.fd = open(__file, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	fc_struc.out.bp = 0;
}

#define drainresidual\
	if (fc_struc.out.bp>0) {\
		fc_struc.ops.out.write(fc_struc.out.arg, fc_struc.out.buf, fc_struc.out.bp);\
		fc_struc.out.bp = 0;\
	}
void yc_drain(void) {
	drainresidual
}

void yc_cleanup(void) {
	drainresidual
	close(fc_struc.out.fd);
}

void static
drain(void) {
	fc_struc.ops.out.write(fc_struc.out.arg, fc_struc.out.buf, YC_OPBS);
	fc_struc.out.bp = 0;
}

_8_u yc_read(struct yc_file *__f, void *__buf, _int_u __size) {
	_8_u r;
	r = 0x00;
	if (__f->state&Y_ENDOF)
		return Y_ENDOF;
    if (__f->ptr+__size>=__f->limit) {
        if (__f->ptr>=__f->limit){
			fc_struc.core.remnants = 0;
		}else {
			fc_struc.core.remnants = __f->limit-__f->ptr;
			fc_struc.ops.in.read(__f, __buf, fc_struc.core.remnants);
		}
		r |= Y_ENDOF;
		__f->state = r;
		return r;
    }
    fc_struc.ops.in.read(__f, __buf, __size);
    __f->ptr+=__size;
	return r;
}

void _yc_write(_ulonglong __arg, void *__buf, _int_u __size) {
	fc_struc.ops.out.write(__arg, __buf, __size);
	fc_struc.out.ptr+=__size;
}

void yc_write(void *__buf, _int_u __size) {
	_8_u *src, *dst;
	src = (_8_u*)__buf;
	dst = fc_struc.out.buf+fc_struc.out.bp;
	_int_u left;
	fc_struc.out.ptr+=__size;
_again:
	left = YC_OPBS-fc_struc.out.bp;
	if (left>0) {
		if (__size<left) {
			left = __size;
		}
		mem_cpy(dst, src, left);
		__size-=left;
		src+=left;
		fc_struc.out.bp+=left;
	}

	if (__size>0) {
		drain();
		dst = fc_struc.out.buf;
		goto _again;
	}
}

void yc_seek(_ulonglong __arg, _64_u __offset) {
	drainresidual
	fc_struc.ops.seek(__arg, __offset);
	fc_struc.out.ptr = __offset;
}
struct _fc_struc fc_struc = {
	.open = _open,
	.close = _close,
	.ops = {
		_read,
		_write,
		_pwrite,
		_seek
	},
	.saloc = {
		.inited = -1,
		.page_c = 1,
		.off = 0,
		.pages = NULL
	}
};
