# include "yc.h"
# include "../y_int.h"
# include "../types.h"
# include "../io.h"
#include "../string.h"
# include "../bbs.h"
# include "../as/as.h"
#include "../lib.h"
static int fd;

_f_err_t main(int __argc, char const *__argv[]) {	
//	struct bbs_node *n;
//	n = bbsn_new();
//	fc_struc.ops.out = ith.out;
//	fc_struc.out.arg = n;
	if (__argc<2) {
		printf("please provide source file.\n");
		return -1;
	}

	char outfile[128];
	char *p,c;
	_int_u i;
	i = 0;
	p = (char*)__argv[1];
	while((c = *(p++)) != '.') {
		outfile[i++] = c;
	}

	mem_cpy(outfile+i,".s",3);
	yc_fileout(outfile);

	yc_init();
	fc_struc.file = yc_open(__argv[1]);
	fc_struc.ops.in = fc_struc.file->ops->in;
	f_clg_init();

	_8_u dgs_out = -1;
	char const *infile  = NULL;
	if (__argc>1) {
		i = 1;
		for(;i != __argc;i++) {
			char const *s = __argv[i];
			if (s[0] == '-') {
				if (s[1] == 'D') {
					char buf[64];
					_int_u len;
					len = sextract(buf,s+2);
					yc_hash_put(clg_struc.defines,buf,len,(void*)-1);
					printf("got define: '%w'.\n",buf,len);
				}else if(s[1] == 'o') {
					i++;
					dgs_out = 0;
					yc_fileout(__argv[i]);
				}
			} else {
				infile = s;
			}
		}
	}

	if (!infile) {
		printf("no input file provided.\n");
		return -1;
	}
	if (dgs_out == -1) {
		char outfile[128];
		char *p,c;
		_int_u i;
		i = 0;
		p = (char*)__argv[1];
		while((c = *(p++)) != '.') {
			outfile[i++] = c;
		}

		mem_cpy(outfile+i,".s",3);
		yc_fileout(outfile);
	}
	
	f_clg_parser();
	yc_close(fc_struc.file);
	yc_cleanup();

//	printf("%s.\n", n->src, fc_struc.out.ptr);
//	bbsn_destroy(n);
}
