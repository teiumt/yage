.extern mov01
.extern mov10
.globl test
test:
;sub16
movlw $4
subwf j[$6
movlw $0
subfwb j[$7
moviw j[$2
movwf $4
moviw j[$3
movwf $5
addfsr $10
;many save
movlb $0
movf $4
movwf $78
movf $5
movwf $79
moviw j[$2
movwf $4
moviw j[$3
movwf $5
addfsr $10
;many retrieve
movlb $0
movf $78
movwf $6
movf $79
movwf $7
call mov01
;add16
movlw $4
addwf j[$6
movlw $0
addwfc j[$7
ret
