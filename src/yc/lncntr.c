#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <malloc.h>
#include "../y_int.h"
int main(int __argc, char const *__argv[]) {
	if (__argc < 2) {
		fprintf(stderr, "please provide a file.\n");
		return -1;
	}

	int fd;
	if ((fd = open(__argv[1], O_RDONLY)) == -1) {
		fprintf(stderr, "failed to open.\n");
		return -1;
	}
	struct stat st;
	fstat(fd, &st);
	char *buf = malloc(st.st_size);
	read(fd,buf,st.st_size);
	close(fd);

	_int_u cnt = 0;
	_int_u i;
	i = 0;
	char c, k;
	while(i != st.st_size) {
		c = buf[i];
		if (c == '\n') {
			k = buf[i+1];
			if ((k>='a'&&k<='z')||(k>='A'&&k<='Z'))
				cnt++;
			if (k == ';') {
				i+=2;
				while(buf[i++] != '\n');
			}
		}
		i++;
	}
	printf("%u",cnt);
}
