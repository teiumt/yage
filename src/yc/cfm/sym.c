# include "../yc.h"
# include "../../m_alloc.h"
# include "../../string.h"
f_cfm_symp f_cfm_sym_new(fc_symp __s) {
	f_cfm_symp s;
	s = (f_cfm_symp)m_alloc(sizeof(struct f_cfm_sym));
	s->s = __s;
	s->y = cfm_eminit;
	s->next = cfm_struc.sytop;
	cfm_struc.sytop = s;
	return s;
}
