# include "../../yc.h"
#include "../cfm.h"
# include "../../../string.h"
# include "../../../io.h"
# include "../../../m_alloc.h"
# include "../../../strf.h"
# include "../frag.h"
struct ins {
	char const *name;
	_8_u len;
};

typedef struct contr {
	_8_u op;
	it_fragp fr;
	fc_symp sy;
	_int_u size, len;
	struct contr *link;
} *contrp;

enum {
	_c_lp,//label placment
	_c_vp//var placment
};

static contrp con_top = NULL;
contrp static newcon(void) {
	contrp c;
	c = (contrp)m_alloc(sizeof(struct contr));
	c->link = con_top;
	con_top = c;
	return c;
}


struct ins in_amd64[] = {
	{"mov", 3},
	{"jmp", 3},
	{"add", 3},
	{"sub", 3},
	{"call", 4},
	{"ret", 3},
	{"and", 3},
	{"lea", 3},
	{"cmp", 3},
	{"jne", 3},
	{"je", 2},
	{"xor", 3},
	{"movz", 4},
	{"inc", 3},
	{"dec", 3}
};

struct ins in_pic[] = {
	{"movlb",5},
	{"movlp",5},
	{"movlw",5},
	{"movwf",5},
	{"btfsis",6},
	{"ret",3},
	{"goto",4},
	{"call",4},
	{"btfsic",6},
	{"movf", 4},
	{"sublw", 5},
	{"subwf", 5},
	{"subfwb", 6},
	{"andlw",5},
	{"andwf",5},
	{"iorlw",5},
	{"iorwf",5},
	{"addlw",5},
	{"addwf",5},
	{"addwfc",6},
	{"lsl",3},
	{"lsr",3},
	{"decf",4},
	{"incf",4},
	{"addfsr",6},
	{"moviw",5},
	{"movwi",5},
	{"clrf",4},
	{"clrw",4},
	{"rlf",3},
	{"rrf",3},
	{"bcf",3},
	{"bsf",3},
	{"xorlw",5},
	{"xorwf",5}
};

char static inpic_mods[2] = {
	'j', 'k'
};

static struct ins *ins_;
struct reg {
	char const *name;
	_int_u n;
};

struct reg regs[] = {
	{"%rax", 4},
	{"%eax", 4},
	{"%ax", 3},
	{"%al", 3},
	{"%rbp", 4},
	{"%rip", 4},
	{"%rbx", 4},
	{"%ebx", 4},
	{"%bx", 3},
	{"%bl", 3}
};

char suffix_table[] = "bwlqbwblbqwlwq";

_8_u static int_sz(_64_u __o) {
	switch(__o) {
		case _f_it_8:
			return 1;
		case _f_it_16:
			return 2;
		case _f_it_32:
			return 4;
		case _f_it_64:
			return 8;
	}
	return 0;
}


void static _yolk(it_yolkp __yk) {
	struct it_cf *cf;
	
	_int_u i;
	i = 0;
	char buf[256], *p;
	char eib[24], *ep;
	ep = eib;
	printf("IT_YOLK: %u.\n", __yk->n_cf);
	while(i != __yk->n_cf) {
		_int_u register pg, pg_off, pan;
		_int_u register off = i+__yk->cf;
		pan = off>>IT_PANPAGE;
		pg = (off&IT_PANPAGEMK)>>F_IT_PAGE_SHFT;
		pg_off = off&F_IT_PAGE_MASK;
		cf = cfm_struc.pt_cf.p[pan]->ptrs[pg];
		cf+=pg_off;
		_int_u d;
		d = 0;
		for(;d != cf->ndsc;d++) {
			if (cf->dlen[d]>0) {
				it_frag_induct(cf->desc[d], cf->dlen[d]);
			}
		}
		printf("IT_CF: %u, %u.\n", off, cf->op);
		struct ins *in = ins_+cf->op;

		mem_cpy(buf, in->name, in->len);
		p = buf+in->len;
		_8_u s;
		s = getsize(cf->info);
		if (s != _f_it_none) {
			if ((s&0x08)>0) {
				*(p++) = suffix_table[(s&0x07)+4];
				*(p++) = suffix_table[(s&0x07)+5];
			} else
				*(p++) = suffix_table[s&0x07];
		}

		if (cf->n_opds>0) {
			*(p++) = ' ';
			_int_u j, k;
			j = 0;
			struct it_oa_em *ot;
			struct it_oa *oa;
			_64_u info;
			_64_u sign;
			for(;j != cf->n_opds;j++) {
				oa = cf->operands+j;
				printf("OPERAND: %u.\n", j);
			_re:
				k = 0;
				for(;k != oa->ns;k++) {
					ot = oa->oaem+k;
					info = ot->info;
					sign = (info&_f_it_s)==_f_it_s;
					if ((ot->bits&IT_MODEN)>0) {
						*(p++) = inpic_mods[_IT_MODS(ot->bits)];
						if (ot->x>0) {
							*(p++) = '0'+(ot->x-1);
						}
						*(p++) = '[';
					}
					if ((ot->bits&IT_OAEM_PPD)>>2) {
						*(p++) = '$';
					}
					if ((info&(_f_it_int|_f_it_dis))>0) {
						_8_u size;
						size = int_sz(info&_f_it_int);
						_64_u val;
						_64_u neg;
						// could just use 64bit with x padding for lower bitx
						val = ot->oa;
						char outbuf[24];
						char *ob;
						ob = outbuf;
	
						if (sign) {
							if (!(neg = (info&_f_it_p))) {
								*(ob++) = '-';
							}
						}
						printf("value: %u.\n", val);	
						_int_u l;
						l = _ffly_nds(ob, val);
						mem_cpy(p, outbuf, l = (l+(ob-outbuf)));
						p+=l;
	
					} else if ((info&_f_it_sym)>0) {
						fc_symp s;
						s = (fc_symp)ot->oa;
						if (!s->local) {			
							mem_cpy(p, s->name, s->len);
							p+=s->len;
							if (ot->bits&IT_OAEM_accost_low) {
								mem_cpy(p,"`l",2);
								p+=2;
							}else if (ot->bits&IT_OAEM_accost_high) {
								mem_cpy(p,"`h",2);
								p+=2;
							}
							if (ot->dis>0) {
								*p = '+';
								p++;
								_int_u l;
								l = _ffly_nds(p, ot->dis);
								p+=l;
							}
						} else {
							*(p++) = '.';
							*(p++) = 'L';
							p+=_ffly_nds(p, s->ident);
						}
					} else if ((info&_f_it_reg)>0) {
						struct reg *r;
						r = regs+ot->oa;
						mem_cpy(p, r->name, r->n);
						p+=r->n;
					} else if ((info&_f_it_abr)>0) {
						*(p++) = '(';	
						*(ep++) = ')';
						oa = (struct it_oa*)ot->oa;
						goto _re;
					} else if ((info&_f_it_arth)>0) {
						_int_u l;
						l = _ffly_nds(p, ot->oa);
						p+=l;
						*(p++) = '+';
					} else if ((info&_it_slip)>0) {
						mem_cpy(p, (void*)ot->oa, ot->oa0);
						p+=ot->oa0;
					}
					/*
						TODO:
							to allow extra with out eib fucking shit up pass bit 
							to say "next-bypass-eib"
					*/
					if (ot->bits&IT_OAEM_PPS) {
						*(p++) = ',';
					}
					if ((ot->bits&IT_OAEM_ANT)>>1) {
						continue;
					}
					if (ep>eib) {
						_int_u l;
						l = ep-eib;
						mem_cpy(p, eib, l);
						p+=l;
						ep = eib;
					}
				}
				if (j != cf->n_opds-1)
					*(p++) = ',';
			}
		}
		*p = '\n';
		it_frag_induct(buf, (p-buf)+1);
		i++;
	}
}

void static po_pic(void) {

}
void static po_amd64(void) {

}
static void(*dopo)(void);

static char const enter_func[] = "push %rbp\nmovq %rsp, %rbp\n";
static char const exit_func[] = "movq %rbp, %rsp\npop %rbp\nret\n";
/*
	emit yolks as whole pages in one function
*/
static void(*track_proc)(struct it_track*);


static it_yolkp getyk(struct it_track *__trk, it_superyolkp sy, _int_u __i) {
	_int_u register off = __i+sy->yk, pan;
	_int_u register pg, pg_off;
	pan = off>>IT_PANPAGE;
	pg = (off&IT_PANPAGEMK)>>F_IT_PAGE_SHFT;
	pg_off = off&F_IT_PAGE_MASK;

	it_yolkp yk;
	yk = __trk->p[pan]->ptrs[pg];
	yk+=pg_off;
	return yk;
}


void static _track_proc_pic(struct it_track *__trk){
	it_superyolkp sy;

	it_ypop po;
	_int_u i, j;
	i = 0;
	while(i != __trk->n_yk) {
		sy = __trk->sy+i;
		po = sy->po;
		while(po != NULL) {
			printf("IT SUPER-PO.\n");
			switch(po->op) {
				/*
					beginning of new label
				*/
				case _it_yk_ss:{
					contrp c;
					c = newcon();
					c->op = _c_lp;
					c->sy = po->sy;
					c->fr = it_cur_frag;
					it_frag_new();
				}
				break;
			}
			po = po->link;
		}


		j = 0;
        while(j != sy->n) {
			it_yolkp yk;
			yk = getyk(__trk, sy, j);
			po = yk->po;

			while(po != NULL) {
				printf("IT_PO: %u.\n", po->op);
				switch(po->op) {

					case _it_yk_ll: {
						char buf[256];
						*buf = '.';
						*(buf+1) = 'L';
						_int_u l;
						l = _ffly_nds(buf+2, po->sy->ident);
						*(buf+2+l) = '\n';
						it_frag_induct(buf, l+3);
					}
					break;
					case _it_yk_aso:
						it_frag_induct(po->pi->s, po->pi->len);
					break;
				}
				po = po->link;
			}
			_yolk(yk);

			j++;
		}

		if (sy->pop&IT_POP_STACK) {
			it_frag_induct("ret\n", 4);
		}
		i++;
	}		
}

void static _track_proc(struct it_track *__trk) {
	it_superyolkp sy;

	it_ypop po;
	_int_u i, j;
	i = 0;
	while(i != __trk->n_yk) {
		sy = __trk->sy+i;
		
		printf("SUPER-YOLK, %u\n", sy->pop);
		po = sy->po;
		while(po != NULL) {
			printf("IT SUPER-PO.\n");
			switch(po->op) {
				/*
					beginning of new label
				*/
				case _it_yk_ss:{
					contrp c;
					c = newcon();
					c->op = _c_lp;
					c->sy = po->sy;
					c->fr = it_cur_frag;
					it_frag_new();
				}
				break;

				case _it_yk_var: {
					contrp c;
					c = newcon();
					c->op = _c_vp;
					c->sy = po->sy;
					c->fr = it_cur_frag;
					c->len = po->pi->len;
					c->size = po->pi->size;
				}
				break;
			}
			po = po->link;
		}

		_8_u exfunc = 0;
		if (sy->pop&IT_POP_STACK) {
			it_frag_induct(enter_func, sizeof(enter_func)-1);
			if (sy->space>0) {
				char buf[128];
				char *p;
				p = buf;
				mem_cpy(p, "subq $", 6);
				p+=6;
				_int_u l;
				l = _ffly_nds(p, sy->space);
				p+=l;
				mem_cpy(p, ",%rsp\n", 6);
				p+=6;
				it_frag_induct(buf, p-buf);
			}
			exfunc = 1;
		}


		j = 0;
		while(j != sy->n) {
			_int_u register off = j+sy->yk, pan;
			_int_u register pg, pg_off;
			pan = off>>IT_PANPAGE;
			pg = (off&IT_PANPAGEMK)>>F_IT_PAGE_SHFT;
			pg_off = off&F_IT_PAGE_MASK;

			it_yolkp yk;
			yk = __trk->p[pan]->ptrs[pg];
			yk+=pg_off;
			po = yk->po;
			/*
				what is this for????
				we have two options for
				this 
				manual
				or 
				blockerize it

				to improve proformance


				why?
					because instructions are emited in a linerar fashion
					so passing a exit_func instruction wont work because of the fixed syntax
					thats why be use yolks 

					ops to trigger setup
					pre setup
					ins
					post setup
			*/
			while(po != NULL) {
				printf("IT_PO: %u.\n", po->op);
				switch(po->op) {
					case _it_yk_ll: {
						char buf[256];
						*buf = '.';
						*(buf+1) = 'L';
						_int_u l;
						l = _ffly_nds(buf+2, po->sy->ident);
						*(buf+2+l) = ':';
						*(buf+3+l) = '\n';
						it_frag_induct(buf, l+4);
					}
					break;
					case _it_yk_aso:
						it_frag_induct(po->pi->s, po->pi->len);
					break;
				}
				po = po->link;
			}
			_yolk(yk);

			if ((yk->pop&IT_POP_BRKLOC)>0) {
				_track_proc((struct it_track*)yk->_0);
				char buf[128];
				_int_u bsz;

				*buf = '.';
				buf[1] = 'L';
				bsz = _ffly_nds(buf+2, yk->_1);
				buf[2+bsz] = ':';
				buf[3+bsz] = '\n';

				it_frag_induct(buf, 4+bsz);
			}
			j++;
		}

		if (exfunc) {
			it_frag_induct(exit_func, sizeof(exit_func)-1);
		}
		i++;
	}


}

it_moundp static data;
void static extern_array(void **__syms, _int_u __n) {
	_int_u i;
	i = 0;
	char buf[128];
	for(;i != __n;i++) {
		fc_symp sy = __syms[i];
		mem_cpy(buf,".extern ",8);
		mem_cpy(buf+8,sy->name,sy->len);
		buf[8+sy->len] = '\n';
		it_mound_pack(data, buf, 9+sy->len);
	}
}

void static global_array(void **__syms, _int_u __n) {
	_int_u i;
	i = 0;
	char buf[128];
	for(;i != __n;i++) {
		fc_symp sy = __syms[i];
		mem_cpy(buf,".globl ",7);
		mem_cpy(buf+7,sy->name,sy->len);
		buf[7+sy->len] = '\n';
		it_mound_pack(data, buf, 8+sy->len);
	}
}


static void _ins_entry(struct it_wing *__w) {
	track_proc(__w->trk);
}

struct wing {
	void(*entry)(struct it_wing*);
};

static struct wing wings[] = {
	{_ins_entry}
};

struct sizespec {
	char const *name;
	_int_u len;
};


static struct sizespec ss[] = {
	{".byte", 5},
	{".word", 5},
	{".long", 5},
	{".quad", 5}
};

void static 
emit_label(contrp __c) {
	printf("writing label to fragment.\n");
	char buf[128];
	char *p;
	p = buf;
	mem_cpy(p, ".globl ", 7);
	mem_cpy(p+7, __c->sy->name, __c->sy->len);
	p+=__c->sy->len+7;
	*(p++) = '\n';
	it_mound_pack(data, buf, p-buf);
	p = buf;
	mem_cpy(p, __c->sy->name, __c->sy->len);
	p+=__c->sy->len;
	*(p++) = ':';
	*(p++) = '\n';
	it_frag_write(__c->fr, buf, p-buf, __c->fr->off);
	__c->fr->off+=p-buf;
}

void static
_emit_var(f_cfm_symp __sy) {
	char buf[128];
	char *bp;
	bp = buf;
	mem_cpy(bp, ".comm ", 6);
	bp+=6;

	mem_cpy(bp, __sy->s->name, __sy->s->len);
	bp+=__sy->s->len;

	*(bp++) = ',';

	bp+=_ffly_nds(bp, __sy->y.wide);
	*(bp++) = '\n';

	it_mound_pack(data, buf, bp-buf);
}

void static
emit_var(contrp __c) {
	_emit_var(__c->sy);
}

void static 
emit_label_pic(contrp __c) {
	printf("writing label to fragment.\n");
	char buf[128];
	char *p;
	p = buf;
	mem_cpy(p, __c->sy->name, __c->sy->len);
	p+=__c->sy->len;
	*(p++) = ':';
	*(p++) = '\n';
	it_frag_write(__c->fr, buf, p-buf, __c->fr->off);
	__c->fr->off+=p-buf;
}

void static var_array(void **__syms, _int_u __n) {
	_int_u i;
	i = 0;
    for(;i != __n;i++) {
  	 	f_cfm_symp sy = __syms[i];
		_emit_var(sy);
	}
}

static void(*c_f[])(contrp) = {
	emit_label,
	emit_var
};


void it_amd64_gas_proc(it_comp __c) {
	_int_u i;
	i = 0;
	for(;i!=__c->n_wgs;i++){
		wings[__c->w[i].id].entry(__c->w);
	}

	contrp c;
	c = con_top;
	while(c != NULL) {
		c_f[c->op](c);
		c = c->link;
	}
}

void it_amd64_gas_out(void) {
	yc_write(".text\n", 6);
	it_moundout(data);
	yc_drain();
	it_fragout();//text
}

void it_pic_out(void) {
	it_moundout(data);
	yc_drain();
	it_fragout();//text
}


void it_pic(void) {
	data = it_mound_new();
	ins_ = in_pic;
	c_f[0] = emit_label_pic;
	track_proc = _track_proc_pic;
	cfm_extern = extern_array;
	cfm_global = global_array;
	cfm_var = var_array;
}

void it_amd64_gas(void) {
	data = it_mound_new();
	ins_ = in_amd64;
	track_proc = _track_proc;
	cfm_extern = extern_array;
	cfm_global = global_array;
	cfm_var = var_array;

}
