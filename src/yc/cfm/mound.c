# include "mound.h"
# include "../yc.h"
# include "../../string.h"
# include "../../m_alloc.h"
#define PAGE_SHFT 3
#define PAGE_SIZE (1<<PAGE_SHFT)
#define PAGE_MASK (PAGE_SIZE-1)

it_moundp it_mound_new(void) {
	it_moundp m;
	m = (it_moundp)m_alloc(sizeof(struct it_mound));

	m->p = (char**)m_alloc(sizeof(char*));
	*m->p = (char*)m_alloc(PAGE_SIZE);
	m->page_c = 1;
	m->off = 0;
	return m;
}

void it_mound_pack(it_moundp __m, char *__buf, _int_u __len) {
	_int_u pg, pg_off;
	pg = __m->off>>PAGE_SHFT;
	pg_off = __m->off&PAGE_MASK;

	_int_u pgc;
	pgc = (__m->off+__len+(PAGE_SIZE-1))>>PAGE_SHFT;
	if (pgc>__m->page_c) {
		_int_u pge;
		pge = __m->page_c;
		__m->page_c = pgc;
		__m->p = (char**)m_realloc(__m->p, pgc*sizeof(char*));
		while(pge != pgc) {
			*(__m->p+pge) = (char*)m_alloc(PAGE_SIZE);
			pge++;
		}
	}

	_int_u e;
	e = (__len+__m->off)>>PAGE_SHFT;
	char *p;
	p = __buf;
	_int_u left;
	if (pg_off>0) {
		_int_u fill;
		if (__len>=(fill = (PAGE_SIZE-pg_off))) {
			mem_cpy((*(__m->p+pg))+pg_off, p, fill);
			p+=fill;
			pg++;
			pg_off = 0;
		} else {
			mem_cpy((*(__m->p+pg))+pg_off, __buf, __len);
			goto _sk;
		}
	}

	while(pg<e) {
		mem_cpy(*(__m->p+pg), p, PAGE_SIZE);
		p+=PAGE_SIZE;
		pg++;
	}

	left = __len-(p-__buf);
	if (left>0) {
		mem_cpy(*(__m->p+pg), p, left);
	}
_sk:
	__m->off+=__len;
}

void it_moundout(it_moundp __m) {
	_int_u pgc;
	pgc = __m->off>>PAGE_SHFT;
	_int_u i;
	i = 0;
	for(;i != pgc;i++) {
		yc_write(*(__m->p+i), PAGE_SIZE);
	}

	_int_u left;
	left = __m->off-(pgc<<PAGE_SHFT);
	if (left>0) {
		yc_write(*(__m->p+i), left);
	}
}
