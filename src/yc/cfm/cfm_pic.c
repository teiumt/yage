#include "../yc.h"
#include "../../m_alloc.h"
#include "../../io.h"
#include "../../string.h"
#include "../../assert.h"
#include "pic.h"
#define trk_cur (cfm_struc.trk_cur)
#define trk_top (cfm_struc.trk_top)

/*
	for goto statments that need to be filled in later as stack address of label might still be left wn	

	ISSUE:
		movft16
	lets say we need to temporarily store a 16bit value. inorder to conserve program space static functions will need to be put in place
	so movft16 will not move for example non dynamic that dest or src cant be desided?


	NOTE:
		some instructions can do diffrent things

		ie 

		addwf = add W to FILE

		meaning loading only happens to one of the operands and the other is handeld by the main function

		void func{
			load(left hand side)

			do operation on right hand side

			- no load for right hand side that is handled here
		}

	int *x;

	(x+10)[10] = 0;

	(x+10+10)[0] = (x+10)[10];

	deref_intconst

	x+10 = binary operation


	okay i want to make somthing clear,
	everything is in its explicit form.


	why? because it allows us to more optimize the code output.
*/

struct pbs_break {
	struct pbs *pbs_head, *pbs_tail;
	struct pbs_break *next;
};
struct pbs;
typedef struct it_yolk_h {
	struct pbs_break *brk_head, *brk_tail;
} *it_yolk_hp;
#define TMP0 (0x70+8)
#define TMP1 (0x70+8+1)
#define TMP2 (0x70+8+2)
#define TMP3 (0x70+8+3)
#define TMP4 (0x70+8+4)
#define TMP5 (0x70+8+5)
#define TMP6 (0x70+8+6)
#define TMP7 (0x70+8+7)

#define FSR0_ (4|(5<<8))
#define FSR1_ (6|(7<<8))
#define FSR0 0x04
#define FSR1 0x06
#define _FSR0 4,5
#define _FSR1 6,7
#define _fsr0 (5|(4<<3))
#define _fsr1 ((7|(6<<3))|(1<<6))

#define gpr_bankaddr(__bank,__file) (0x2000+(__bank*80)+(__file-0x20))
struct cfm_lds cfm_pic_sp[64];
/*
	shifting a mem location contents thats >8 bits wide

	int n;			#amount we shift

	int i;

	i = i<<n;

	n&3		= how much to shift 
	n>>3;	= how many bytes we need to clear

	for(n>>3) {
		i[x] = 0;
		x++;
	}

*/

#define _imm8 _f_it_8
struct pbs { 
	_8_u op;
	_64_u info[2];
	_64_u opand[2];
	_64_u opand0[2];
	_64_u bits[2];
	_64_u x[2];
	_32_s dis[2];
	_8_u n;
	char const *desc[8];
	_int_u dlen[8];
	_int_u ndsc;
	struct pbs *next;
};
struct jb;
struct min {
	struct jb *op[4];
	_64_u info[4];
	_64_u opand[2*4];
	_64_u opand0[2*4];
	_8_u ig[4];
	//ig = ignore
};

struct addrstrc {
	_16_u bank;
	_16_u file;
};

struct desc {
	char const *desc;
	_int_u len;
};

struct {
	_64_u bank;
} selreg;
/*
	first and last place are accum
*/

struct pbs_break *_brk = NULL;
//desc selector
static struct desc *ds[8] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};
static _int_u ndh = 0;
//snapoff???
void static pbs_breakoff(void) {
	struct pbs_break *b;
	b = m_alloc(sizeof(struct pbs_break));
	it_yolk_hp h = (it_yolk_hp)trk_cur->yk_top;
	_brk->next = b;
	b->next = NULL;
	b->pbs_head = NULL;
	b->pbs_tail = NULL;
	_brk = b;
}

struct pbs static* pbs_new(void) {
	struct pbs *s = m_alloc(sizeof(struct pbs));
	struct pbs_break *b = _brk;
	if (!b->pbs_head)
		b->pbs_head = s;
	if (b->pbs_tail != NULL)
		b->pbs_tail->next = s;
	b->pbs_tail = s;
	s->next = NULL;
	_int_u d;
	d = 0;
	for(;d != ndh;d++) {
		s->desc[d] = ds[d]->desc;
		s->dlen[d] = ds[d]->len;
	}
	s->x[0] = 0;
	s->x[1] = 0;
	s->dis[0] = 0;
	s->dis[1] = 0;
	s->ndsc = ndh;
	ndh = 0;
	return s;

}

static struct min _m;
struct jb {
	_64_u info;
	_64_u opand[2];
	_64_u opand0[2];
};
#define _ {.info=~0}
static struct jb jbtab[10] = {
	_,_,_,_,_,_,_,_,_,_
};

void cfm_lp(void) {
	mem_set(jbtab, 0xff, sizeof(struct jb)*10);
}

struct {
	_8_u u;
} wreg = {0};
#define add16_fsr1(__c0, __c1) add16(__c0,__c1,6,7);
#define sub16_fsr1(__c0, __c1) sub16(__c0,__c1,6,7);

#define dsc_push	";push\n"
#define dsc_pop		";pop\n"
#define dsc_add16	";add16\n"
#define dsc_sub16	";sub16\n"
#define dsc_stpos	";stpos\n"
#define dsc_stfetch	";stfetch\n"
#define dsc_stput	";stput\n"
#define dsc_bo		";bo\n"
#define dsc_addrof	";addrof\n"
#define dsc_assignp	";assignp\n"
#define dsc_funccall		";funccall\n"
#define dsc_assign			";assign\n"
#define dsc_assign0			";assign0\n"
#define dsc_deref			";deref\n"
#define dsc_toreg			";toreg\n"
#define dsc_cassign			";cassign\n"
#define dsc_dran			";dran\n"
#define dsc_fsvar			";fsvar\n"
#define dsc_assignl			";assignl\n"
#define dsc_deref_regfit	";deref regfit\n"
#define dsc_deref_enl		";deref enl\n"
#define dsc_putreg			";putreg\n"
#define dsc_assign_deref	";assign deref\n"
#define dsc_eqo				";eq\n"
#define dsc_deref_cram		";deref cram\n"
#define dsc_addfsr			";addfsr\n"
#define dsc_rmfsr			";rmfsr\n"
#define dsc_stashsv			";stash save\n"
#define dsc_stashrt			";stash retrieve\n"
#define dsc_addoff			";add off\n"
#define dsc_addoffwith			";add off with\n"
#define dsc_manysv			";many save\n"
#define dsc_manyrt			";many retrieve\n"

//use macro
static struct desc _dsc[] = {
	{dsc_push, sizeof(dsc_push)-1},
	{dsc_pop, sizeof(dsc_pop)-1},
	{dsc_add16, sizeof(dsc_add16)-1},
	{dsc_sub16, sizeof(dsc_sub16)-1},
	{dsc_stpos, sizeof(dsc_stpos)-1},
	{dsc_stfetch, sizeof(dsc_stfetch)-1},
	{dsc_stput, sizeof(dsc_stput)-1},
	{dsc_bo, sizeof(dsc_bo)-1},
	{dsc_addrof, sizeof(dsc_addrof)-1},
	{dsc_assignp, sizeof(dsc_assignp)-1},
	{dsc_funccall, sizeof(dsc_funccall)-1},
	{dsc_assign, sizeof(dsc_assign)-1},
	{dsc_assign0, sizeof(dsc_assign0)-1},
	{dsc_deref, sizeof(dsc_deref)-1},
	{dsc_toreg, sizeof(dsc_toreg)-1},
	{dsc_cassign, sizeof(dsc_cassign)-1},
	{dsc_dran, sizeof(dsc_dran)-1},
	{dsc_fsvar, sizeof(dsc_fsvar)-1},
	{dsc_assignl, sizeof(dsc_assignl)-1},
	{dsc_deref_regfit, sizeof(dsc_deref_regfit)-1},
	{dsc_deref_enl, sizeof(dsc_deref_enl)-1},
	{dsc_putreg, sizeof(dsc_putreg)-1},
	{dsc_assign_deref, sizeof(dsc_assign_deref)-1},
	{dsc_eqo, sizeof(dsc_eqo)-1},
	{dsc_deref_cram, sizeof(dsc_deref_cram)-1},
	{dsc_addfsr, sizeof(dsc_addfsr)-1},
	{dsc_rmfsr, sizeof(dsc_rmfsr)-1},
	{dsc_stashsv, sizeof(dsc_stashsv)-1},
	{dsc_stashrt, sizeof(dsc_stashrt)-1},
	{dsc_addoff, sizeof(dsc_addoff)-1},
	{dsc_addoffwith, sizeof(dsc_addoffwith)-1},
	{dsc_manysv, sizeof(dsc_manysv)-1},
	{dsc_manyrt, sizeof(dsc_manyrt)-1}

};
#define dsc_PUSH	(_dsc)
#define dsc_POP		(_dsc+1)
#define dsc_ADD16	(_dsc+2)
#define dsc_SUB16	(_dsc+3)
#define dsc_STPOS	(_dsc+4)
#define dsc_STFETCH	(_dsc+5)
#define dsc_STPUT	(_dsc+6)
#define dsc_BO		(_dsc+7)
#define dsc_ADDROF		(_dsc+8)
#define dsc_ASSIGNP	(_dsc+9)
#define dsc_FUNCCALL	(_dsc+10)
#define dsc_ASSIGN		(_dsc+11)
#define dsc_ASSIGN0		(_dsc+12)
#define dsc_DEREF	 (_dsc+13)
#define dsc_TOREG		(_dsc+14)
#define dsc_CASSIGN		(_dsc+15)
#define dsc_DRAN		(_dsc+16)
#define dsc_FSVAR		(_dsc+17)
#define dsc_ASSIGNL		(_dsc+18)
#define dsc_DEREF_REGFIT	(_dsc+19)
#define dsc_DEREF_ENL		(_dsc+20)
#define dsc_PUTREG	   (_dsc+21)
#define dsc_ASSIGN_DEREF	(_dsc+22)
#define dsc_EQO				(_dsc+23)
#define dsc_DEREF_CRAM		(_dsc+24)
#define dsc_ADDFSR			(_dsc+25)
#define dsc_RMFSR			(_dsc+26)
#define dsc_STASHSV			(_dsc+27)
#define dsc_STASHRT			(_dsc+28)
#define dsc_ADDOFF			(_dsc+29)
#define dsc_ADDOFFWITH		(_dsc+30)
#define dsc_MANYSV			(_dsc+31)
#define dsc_MANYRT		(_dsc+32)


#define setdesc(__dsc)\
	if(ndh<8){ds[ndh] = __dsc;\
	ndh++;}
void load16(_64_u);	
static fc_symp _addfsr0, _addfsr1, _addfsri1, _addfsri0, _clr2q,_clr2d,_clr1q,_clr1d,_clr4q;

static fc_symp _subfsr1w,_movfsr01,_movfsr10;

struct binop {
	fc_symp _0[64];
};
#define __bop_0		0
#define __bop_00	1

#define __add	0
#define __sub	2
#define __or	3
#define __and	4
#define bop_at(__size,__n,__what) (bops[__size]._0[__n+__what])
#define _add0(__size) (bops[__size]._0[0])
#define _add00(__size) (bops[__size]._0[1])

#define _sub0(__size) (bops[__size]._0[2])
#define _or0(__size) (bops[__size]._0[3])
#define _and0(__size) (bops[__size]._0[4])
#define _mul0(__size) (bops[__size]._0[5])

#define _mov0(__size) (bops[__size]._0[6])

#define _mov01(__size) (bops[__size]._0[7])

#define _mov1(__size) (bops[__size]._0[8])
#define _mov2(__size) (bops[__size]._0[9])
#define _mov00(__size) (bops[__size]._0[10])
#define _mov2i(__size) (bops[__size]._0[11])
#define _mov10(__size) (bops[__size]._0[12])
#define _cmp0(__size) (bops[__size]._0[13])
#define _mov02(__size) (bops[__size]._0[14])
#define _shlh(__size) (bops[__size]._0[15])
#define _shrh(__size) (bops[__size]._0[16])

#define _shl(__size) (bops[__size]._0[17])
#define _shr(__size) (bops[__size]._0[18])




static struct binop bops[9] = {
	{{}},
	{{
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	}},
	{{
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	}},
	{{
	
	}},
	{{
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	
	}},
	{{
	
	}},
	{{
	
	}},
	{{
	
	}},
	{{
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
	}}
};
#define bop_look(__op,__size) (bops[__size]._0[__op])

void static emitsingle(_8_u op) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = op;
	pb->n = 0;
}
void static
emitop(_8_u op, _64_u __info, _64_u __opand, _64_u __bits) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = op;
	pb->info[0] = __info;
	pb->opand[0] = __opand;
	pb->bits[0] = __bits;
	pb->n = 1;
}

void static save_wreg(_16_u __where) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __where;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

}
void static load_wreg(_16_u __from) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = __from;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

void static callthis(fc_symp __sy) {
	assert(__sy != NULL);
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_call;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = __sy;
	pb->bits[0] = 0;
}

void stfetch(_16_s);
void static addwreg_tofsr(_16_u __fsr) {
	_64_u bits;
	_64_u fsr0, fsr1;
	bits = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
	fsr0 = __fsr&0xff;
	fsr1 = __fsr>>8;

	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_addwf;
	pb->info[0] = _imm8;
	pb->opand[0] = fsr0;
	pb->bits[0] = bits;
	pb->n = 1;
	
	pb = pbs_new();
	pb->op = _pic_clrw;
	pb->info[0] = 0;
	pb->opand[0] = 0;
	pb->bits[0] = 0;
	pb->n = 0;

	pb = pbs_new();
	pb->op = _pic_addwfc;
	pb->info[0] = _imm8;
	pb->opand[0] = fsr1;
	pb->bits[0] = bits;
	pb->n = 1;
}

void static subwreg_fromfsr(_16_u __fsr) {
	_64_u bits;
	_64_u fsr0, fsr1;
	bits = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
	fsr0 = __fsr&0xff;
	fsr1 = __fsr>>8;

	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_subwf;
	pb->info[0] = _imm8;
	pb->opand[0] = fsr0;
	pb->bits[0] = bits;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_clrw;
	pb->info[0] = 0;
	pb->opand[0] = 0;
	pb->bits[0] = 0;
	pb->n = 0;


	pb = pbs_new();
	pb->op = _pic_subfwb;
	pb->info[0] = _imm8;
	pb->opand[0] = fsr1;
	pb->bits[0] = bits;
	pb->n = 1;
}

struct many_store;
static _64_u stash_bits = 0;
struct stash {
	_int_u n;
	_64_u val[16];
	_64_u bank[16];
	_8_u type;
	f_cfm_emp em;
	_64_u *uloc;	
	struct many_store *store;
	struct stash *next;
};
/*
	NOTE:
		other 8-bytes are used for fixed temp storage
*/
/*

	we have 16-bytes of common ram 
	so this needs to be dynamic. say we dish out 1 byte
	then a request for 8-bytes. how do we deal with overlap?
	we need to rewrite that 1 bytes value/address so it does not overlap with are 8-bytes?
	or not?


	procure
*/

struct ram_store {
	_64_u free_ram[16];
	_int_u nfreeram;
};

struct many_store {
	_64_u b0[80];
	_64_u b0_free;
	_64_u bank;
	_8_s is_accum;
};

struct ram_store ram = {
	{
	0x70,
	0x70+1,
	0x70+2,
	0x70+3,
	0x70+4,
	0x70+5,
	0x70+6,
	0x70+7
	},8

};

#define GPR_HWORD	(&many[0])
#define GPR_WORD	(&many[2])
#define GPR_DWORD	(&many[4])
#define GPR_QUAD	(&many[6])
#define BANK_GPR 80
#define GPR(__x) (0x20+__x)
struct many_store many[8] = {
	//HWORDS
	{
		{
			GPR(0),GPR(1),GPR(2),GPR(3),GPR(4),GPR(5),GPR(6),GPR(7),
			GPR(8),GPR(9),GPR(10),GPR(11),GPR(12),GPR(13),GPR(14),GPR(15),
			GPR(16),GPR(17),GPR(18),GPR(19),GPR(20),GPR(21),GPR(22),GPR(23),
			GPR(24),GPR(25),GPR(26),GPR(27),GPR(28),GPR(29),GPR(30),GPR(31)
		},
		32,
		0
	},
	{
		{
		},
		0,
		1,
		-1
	},
	//WORDS
	{
		{
			GPR(32),GPR(34),GPR(36),GPR(38),GPR(40),GPR(42),GPR(44),GPR(46)
		},
		8,
		0,
		-1
	},
	{
		{

		},
		0,
		1,
		-1
	},
	//DWORDS
	{
		{
			GPR(48),GPR(52),GPR(56),GPR(60)
		},
		4,
		0,
		-1
	},
	{
		{

		},
		0,
		1,
		-1
	},
	//QUADS
	{
		{
			GPR(64),GPR(72)
		},
		2,
		0,
		-1
	},
	{
		{

		},
		0,
		1,
		-1
	}
};
#define REG_END (~(_64_u)0)
struct many_store accum_regs[] = {
	{
		{
			GPR(16),REG_END
		},
		1,
		1,
		0
	},
	{
		{
			GPR(16),REG_END
		},
		1,
		2,
		0
	},
	{
		{
			GPR(16),REG_END
		},
		1,
		3,
		0
	},
	{
		{
			GPR(16),REG_END
		},
		1,
		5,
		0
	},
	{
		{
			GPR(16),REG_END
		},
		1,
		6,
		0
	}
};
#define GPR_ACCUM (accum_regs)
struct stash *accum;
struct stash *stash_top = NULL, *stash_tail = NULL;
struct stash static*
stash_alloc(_int_u __n) {
	struct stash *st = m_alloc(sizeof(struct stash));
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		assert(ram.nfreeram>0);
		ram.nfreeram--;
		st->val[i] = ram.free_ram[ram.nfreeram];
	}

	st->n = __n;
	return st;
}

void static
stash_free(struct stash *__st) {
	_int_u i;
	i = 0;
	for(;i != __st->n;i++) {
		ram.free_ram[ram.nfreeram] = __st->val[i];
		ram.nfreeram++;
	}
	m_free(__st);
}

struct stash static*
many_alloc(struct many_store *m, _int_u __n) {
	while(!m->b0_free){m++;}
	struct stash *st = m_alloc(sizeof(struct stash));
	_int_u i;
	i = 0;

	for(;i != __n;i++) {
		m->b0_free--;
		st->val[i] = m->b0[m->b0_free];
	}
	st->uloc = m->b0+m->b0_free;
	
	st->n = __n;
	st->store = m;
	return st;
}

static void many_free(struct stash *__st) {
	struct many_store *m = __st->store;
	_int_u i;
	i = 0;
	for(;i != __st->n;i++) {
		*__st->uloc = m->b0[m->b0_free];
		m->b0[m->b0_free] = __st->val[i];
		m->b0_free++;
	}

	m_free(__st);
}
void static movft16(_int_u __dst, _int_u __src) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = __src;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __dst;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = __src+1;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __dst+1;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

}

void static move8(_int_u __dst, _int_u __src) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = __src;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __dst;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}


void static clrw(void) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_clrw;
	pb->info[0] = 0;
	pb->opand[0] = 0;
	pb->bits[0] = 0;
	pb->n = 0;
}

void static clrf(_8_u __file) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_clrf;
	pb->info[0] = _imm8;
	pb->opand[0] = __file;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

void static
stash_wreg(struct stash *__st) {
	setdesc(dsc_STASHSV)
	save_wreg(__st->val[0]);
}

void static
retrieve_wreg(struct stash *__st) {
	setdesc(dsc_STASHRT)
	load_wreg(__st->val[0]);
}

void static bankset(_64_u __bank) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movlb;
	pb->info[0] = _imm8;
	pb->opand[0] = __bank;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

void many_save(struct stash *__st, _64_u __what) {
	setdesc(dsc_MANYSV)
	bankset(__st->store->bank);
	movft16(__st->val[0],__what);
}

void many_retrieve(struct stash *__st, _64_u __what) {
	setdesc(dsc_MANYRT)
	bankset(__st->store->bank);
	movft16(__what,__st->val[0]);
}

void many_save_wreg(struct stash *__st) {
	setdesc(dsc_MANYSV)
	bankset(__st->store->bank);
	save_wreg(__st->val[0]);
}
void many_wreg_retrieve(struct stash *__st) {
	setdesc(dsc_MANYRT)
	bankset(__st->store->bank);
	load_wreg(__st->val[0]);
}

struct stash *stack_stash[16];
static _int_u stackpush_num = 0;

struct stash *stack_stashx[16];
static _int_u stackpush_numx = 0;

/*
	weak no nested things but quick
*/
void static push(void) {
	setdesc(dsc_PUSH)
	struct stash *st;
	st = stash_alloc(1);
	stash_wreg(st);
	stack_stash[stackpush_num++] = st;
}

void static pop(void) {
	setdesc(dsc_POP)
	struct stash *st;
	assert(stackpush_num>0);
	st = stack_stash[--stackpush_num];
	retrieve_wreg(st);
	stash_free(st);
}

void static pushx(void) {
	setdesc(dsc_PUSH)
	struct stash *st;
	st = many_alloc(GPR_HWORD,1);
	many_save_wreg(st);
	stack_stashx[stackpush_numx++] = st;
}

void static popx(void) {
	setdesc(dsc_POP)
	struct stash *st;
	assert(stackpush_numx>0);
	st = stack_stashx[--stackpush_numx];
	many_wreg_retrieve(st);
	many_free(st);
}

void static pushx16(_64_u __where) {
	setdesc(dsc_PUSH)
	struct stash *st;
	st = many_alloc(GPR_WORD,1);
	many_save(st,__where);
	stack_stashx[stackpush_numx++] = st;
}

void static popx16(_64_u __where) {
	setdesc(dsc_POP)
	struct stash *st;
	st = stack_stashx[--stackpush_numx];
	many_retrieve(st,__where);
	many_free(st);
}

union {
	struct {
		_8_u fsr0;
		_8_u fsr1;
	};
	_16_u fsr;
} stpos_ = {{6,7}};

void static imm_wreg(_8_u __i) {
	if (!__i) {
		clrw();
		return;
	}
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movlw;
	pb->info[0] = _imm8;
	pb->opand[0] = __i;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

/*
	when using fetch or put banksel must be cleard or at zero0
*/
void static _add_sub16r(_8_u op, _8_u op0, _8_u __c0, _8_u __c1, _8_u __f0, _8_u __f1, _8_u __f0r, _8_u __f1r) {
	if (op == _pic_addwf)
		setdesc(dsc_ADD16)
	else
		setdesc(dsc_SUB16)

	struct pbs *pb;
	imm_wreg(__c0);
	pb = pbs_new();
	pb->op = op;
	pb->info[0] = _imm8;
	pb->opand[0] = __f0;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	save_wreg(__f0r);
	
	imm_wreg(__c1);

	pb = pbs_new();
	pb->op = op0;
	pb->info[0] = _imm8;
	pb->opand[0] = __f1;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
	
	save_wreg(__f1r);
}
#define add16r(...) _add_sub16r(_pic_addwf,_pic_addwfc,__VA_ARGS__)
#define sub16r(...) _add_sub16r(_pic_subwf,_pic_subfwb,__VA_ARGS__)


#define add16(...) _add_sub16(_pic_addwf,_pic_addwfc,__VA_ARGS__)
#define sub16(...) _add_sub16(_pic_subwf,_pic_subfwb,__VA_ARGS__)

void static _add_sub16(_8_u op, _8_u op0, _8_u __c0, _8_u __c1, _8_u __f0, _8_u __f1) {
	setdesc(dsc_ADD16)
	struct pbs *pb;
	imm_wreg(__c0);

	pb = pbs_new();
	pb->op = op;
	pb->info[0] = _imm8;
	pb->opand[0] = __f0;
	pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
	pb->n = 1;

	imm_wreg(__c1);

	pb = pbs_new();
	pb->op = op0;
	pb->info[0] = _imm8;
	pb->opand[0] = __f1;
	pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
	pb->n = 1;
}

struct pfw {
	_16_s _ladadr;
};
#define ladadr (_w._ladadr)
static struct pfw _w;

struct savepnt {
	struct pfw _w;
};
static struct savepnt sp[24];
static _int_u cursp = 0;
void *cfm_savepoint(void) {
	struct savepnt *s = sp+(cursp++);
	s->_w = _w;
	return s;
}
//silent
#define stposs(__adr) _stposs(__adr,stpos_.fsr0,stpos_.fsr1)
void static _stposs(_16_s __adr, _8_u __f0, _8_u __f1) {
	if (__adr == ladadr)
		return;
	setdesc(dsc_STPOS)
	_16_s dif;
	struct pbs *pb;
	if (__adr>ladadr) {
		dif = __adr-ladadr;
		if (dif<64) {
			pb = pbs_new();
			pb->op = _pic_addfsr;
			pb->info[0] = _imm8;
			pb->opand[0] = dif;
			pb->bits[0] = IT_OAEM_PPD;
			pb->n = 1;
			if (__f0 == 6) {
				pb->bits[0] |= IT_MODEN|IT_MOD_J;
			}
		} else {
			add16(dif&0xff, dif>>8,__f0,__f1);
		}
	} else {
		dif = ladadr-__adr;
		if (dif<0x100 && __f0 == FSR1) {
			imm_wreg(dif&0xff);
			callthis(_subfsr1w);
		}else{
			sub16(dif&0xff, dif>>8,__f0,__f1);
		}
	}

}

void static _stpossr(_16_s __adr, _8_u __f0, _8_u __f1, _8_u __f0r, _8_u __f1r) {
	setdesc(dsc_STPOS)
	_16_s dif;
	struct pbs *pb;
	if (__adr>ladadr) {
		dif = __adr-ladadr;
		add16r(dif&0xff, dif>>8,__f0,__f1,__f0r,__f1r);
	} else {
		dif = ladadr-__adr;
		sub16r(dif&0xff, dif>>8,__f0,__f1,__f0r,__f1r);
	}
}

void static stpos(_16_s __adr) {
	stposs(__adr);
	ladadr = __adr;
}

void loadimm(_64_u __int, _int_u __sz, _int_u __off) {
	struct pbs *pb;
	_int_u i;
	_int_u val;
	i = 0;
	while(i != __sz) {
		val = __int&0xff;
		if (!val){
			clrf(__off+i);
		}else {
			pb = pbs_new();
			pb->n = 1;
			pb->op = _pic_movlw;
			pb->info[0] = _imm8;
			pb->opand[0] = val;
			pb->bits[0] = 0;
			save_wreg(__off+i);
		}
		__int>>=8;
		i++;
	}
}
void static
moveover(_64_u __dst, _64_u __src, _int_u __size) {
	_int_u i;
	i = 0;
	while(i != __size) {
		move8(__dst+i,__src+i);
		i++;
	}
}

void static
swapit(_64_u __dst, _64_u __src, _int_u __size) {
	_int_u i;
	i = 0;
	while(i != __size) {
		move8(TMP0+i,__src+i);
		move8(__src+i,__dst+i);
		move8(__dst+i,TMP0+i);
		i++;
	}
}

#define MULreg 40
static _int_u idx_mode;
static struct cfm_props *props_addfsr = NULL;
/*
	ISSUE:
		some routines are identical for ADD-DIS and ADD-OFF

	ADD frame var to FSR
*/
#define R0 0x20
#define R1 0x28
#define R2 0x30
#define R3 0x38
void static addfstofsr(struct f_cfm_em *__em, _16_u __fsr) {
	/*
		__fsr = fsr we are adding to
	*/
	setdesc(dsc_ADDFSR)
	assert(props_addfsr != NULL);	
	if (props_addfsr->nc_off_v->u == 1) {
		/*
			in the case __fsr is set to 1 we assume that FSR1 has been moved to FSR0
			so we do this.
		*/
		if (__fsr == FSR1_)
			stpos_.fsr = (4|(5<<8));
		stfetch(__em->pm.off);
		if (__fsr == FSR1_)
			stpos_.fsr = (6|(7<<8));

		if (idx_mode == 1) {
			addwreg_tofsr(__fsr);
		}else{
			/*
				FIXME
			*/
			bankset(4);
			save_wreg(R0);
			loadimm(0,1,R0+1);
			loadimm(0,2,R2);
		
			loadimm(idx_mode|(1<<8),2,R1);
			callthis(_mul0(2));
			load_wreg(R2);
			addwreg_tofsr(__fsr);
		}
	}else {
		if (idx_mode == 1) {
			if (__fsr == FSR1_) {
				_stposs(__em->pm.off,_FSR0);
				callthis(_addfsri1);
			}else{
				stpos(__em->pm.off);
				callthis(_addfsri0);
			}
		}else{
			/*
				here we dont realy have much wiggle room,
				so we are forced to just correct it.
			
				SWAP fsr0 and fsr1 around
			
				NOTE:
					ether one fsr0 or fsr1 could store frame pointer
					if we are to add to FSR1 then we assume the frame pointer
					has been temporarily stored in the other.
				TODO:
					note that FSR1 could be the stack pointer
			
				HERE we can only add to FSR0 and not FSR1
				only working with FSR0
			*/
			if (__fsr == FSR1_) {
				swapit(FSR0,FSR1,2);	
			}
			stpos(__em->pm.off);
		
			bankset(4);
			/*
				mov are index to R0
			*/
			callthis(_mov0(2));
			loadimm(idx_mode|(1<<8),2,R1);
			loadimm(0,2,R2);
			callthis(_mul0(2));

			callthis(_addfsr0);
			/*
				restore original
			*/
			if (__fsr == FSR1_) {
				swapit(FSR1,FSR0,2);
			}
		}
	}
}
void static rmfsfromfsr(struct f_cfm_em *__em, _16_u __fsr) {
	setdesc(dsc_RMFSR)
	stfetch(__em->pm.off);
	subwreg_fromfsr(__fsr);
}

void static addwtofsr(struct f_cfm_em *__em, _16_u __fsr) {
	if (idx_mode == 1) {
		addwreg_tofsr(__fsr);
	}else{
		bankset(4);
		save_wreg(R0);
		loadimm(0,1,R0+1);
		loadimm(idx_mode|(1<<8),2,R1);
		loadimm(0,2,R2);
		callthis(_mul0(2));
		callthis(_addfsr0);
	}
}

void static (*addoff[8])(struct f_cfm_em*,_16_u) = {
	NULL,
	NULL,
	addfstofsr,
	NULL,
	addwtofsr
};
void static (*rmoff[8])(struct f_cfm_em*,_16_u) = {
	NULL,
	NULL,
	rmfsfromfsr
};

void static (*adddis[8])(struct f_cfm_em*,_16_u) = {
	NULL,
	NULL,
	addfstofsr
};

void stfetch(_16_s __adr) {
	setdesc(dsc_STFETCH)
	_64_s dif = __adr-ladadr;
	if (dif>=-32 && dif<=31) {
		struct pbs *pb;
		pb = pbs_new();
		pb->op = _pic_moviw;
		pb->info[0] = _f_it_s|_imm8|_f_it_p;
		if (dif<0) {
			dif = -dif;
			pb->info[0] &= ~_f_it_p;
		}
		pb->opand[0] = dif;
		pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
		pb->n = 1;
	} else {
		stpos(__adr);
		struct pbs *pb;
		pb = pbs_new();
		pb->op = _pic_movf;
		pb->info[0] = _imm8;
		pb->opand[0] = 1;
		pb->bits[0] = IT_OAEM_PPD;
		pb->n = 1;
	}
}

#define move10(__sz) callthis(_mov10(__sz))
#define move01(__sz) callthis(_mov01(__sz))
/*
	NOTE: copy does not use stack 
*/
void static copy(_64_u __sz) {
	struct pbs *pb;
	if (__sz == 1) {
		pb = pbs_new();
		pb->op = _pic_movf;
		pb->info[0] = _imm8;
		pb->opand[0] = 1;
		pb->bits[0] = IT_OAEM_PPD;
		pb->n = 1;
		pb = pbs_new();
		pb->op = _pic_movwf;
		pb->info[0] = _imm8;
		pb->opand[0] = 0;
		pb->bits[0] = IT_OAEM_PPD;
		pb->n = 1;	
		return;
	}
	move01(__sz);	
}

void static relpf(_8_u __op, _16_s __dis, _64_u __bits) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = __op;
	pb->info[0] = _f_it_s|_imm8|_f_it_p;
	if (__dis<0) {	
		__dis = -__dis;
		pb->info[0] &= ~_f_it_p;
	}
	pb->opand[0] = __dis;
	pb->bits[0] = IT_OAEM_PPD|__bits;
	pb->n = 1;	
}


void static store_int(_64_u i, _int_u __fsr, _int_u __sz, _16_s __dis) {
	if (__fsr == 1) {
		clrw();
		if (__sz == 8) {
			if (!(i&0xffffffffffffff00)) {
				stpos(ladadr+__dis);
				callthis(_clr1d);
				callthis(_clr4q);
				__sz = 1;
				__dis = 0;
			}else
			if (!(i&0xffffffffffff0000)) {
				stpos(ladadr+__dis);
				callthis(_clr2d);
				callthis(_clr4q);
				__sz = 2;
				__dis = 0;
			}else
			if (!(i&0xffffffff00000000) && __dis>0) {
				stpos(ladadr+__dis);
				callthis(_clr4q);	
				__sz = 4;
			}
		}else
		if (__sz == 4) {
			if (!(i&0xffffffffffffff00) && __dis>0) {
				stpos(ladadr+__dis);
				callthis(_clr1d);
				__sz = 1;
				__dis = 0;
			}else
			if (!(i&0xffffffffffff0000) && !__dis) {
				callthis(_clr2d);
				__sz = 2;
			}
		}
	}
	_64_u s = 0;
	struct pbs *pb;
	_64_u bits = __fsr==1?(IT_MODEN|IT_MOD_J):0;
	_64_u j;
	_16_s wreg = -1;
_next:
	j = i&0xff;
	if (j != wreg) {
		wreg = j;
		if (!j) {
			clrw();
		}else{
			pb = pbs_new();
			pb->op = _pic_movlw;
			pb->info[0] = _imm8;
			pb->opand[0] = j;
			pb->bits[0] = IT_OAEM_PPD;
			pb->n = 1;
		}
	}
	relpf(_pic_movwi,s+__dis,bits); 
	if (s != __sz-1){
		s++;
		i>>=8;
		goto _next;
	}
}

void static enl_load(_16_u file, _16_u bank) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movlb;
	pb->info[0] = _imm8;
	pb->opand[0] = bank;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = file;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

}
void static 
fetchit(f_cfm_emp __em, f_cfm_nodep __off, _64_u __offi) {
	if (!__off) {
		stfetch(__em->pm.off+__offi);
		return;
	}
	_stpossr(__em->pm.off+__offi,_FSR1,_FSR0);
	__off->rake.fsr = FSR0_;
	__off->f(__off);
	load_wreg(0);
}
void static
addtofsr(_64_u __n, _8_u __bits);
/*
	add to FSR
*/
void static add_off(struct cfm_rake *__a) {
	setdesc(dsc_ADDOFF)
/*
	if are source address is in FSR1 move it over to FSR0

	NOTE: without this addoff wont work correctly
*/
	if (__a->fsr == FSR1_){
		movft16(FSR0,FSR1);
	}
	props_addfsr = &__a->props;
	addoff[__a->props.nc_off_ac&0xf](__a->props.nc_off,__a->fsr);
}

/*
	add to FSR with

	binary operation or ?
*/
void static add_off_with(struct cfm_rake *__a) {
	setdesc(dsc_ADDOFFWITH)
	pushx16(__a->fsr == FSR0_?FSR0:FSR1);
	if (__a->fsr == FSR1_) {
		movft16(FSR1,FSR0);
	}
	__a->with->f(__a->with);
	save_wreg(TMP0);
	popx16(FSR0);
	load_wreg(TMP0);
	__a->fsr = FSR0_;
	add_off(__a);
}

void static store_intit(_64_u i, _int_u __fsr, _int_u __sz, _16_s __dis, f_cfm_nodep __off) {
	if (!__off) {
		printf("DISP: %d.\n",__dis);	
		store_int(i,__fsr,__sz,__dis);
		return;
	}
	__off->rake.fsr = !__fsr?FSR0_:FSR1_;
	__off->f(__off);
	store_int(i,__fsr,__sz,0);
}

/*
	this function only exists because _func_call
	need to assign stuff to the stack
*/
#define WITHINRANGE(dis,by) if(dis>=-(32-by) && dis<=(31-by))
/*
	8bit assignment with exeption of INT-CONST assignment
*/

void static
altered(f_cfm_emp __em) {
	if (!__em->saved) {
		__em->saved = -1;
		many_free(__em->stash);	
	}
}

_8_s static
save_retrieve(f_cfm_emp __em) {
	if (!__em->saved) {
		many_wreg_retrieve(__em->stash);
		return 0;
	}
	return -1;
}

void static
savethis(f_cfm_emp __em) {
	struct stash *st;
	return;
	if (!GPR_HWORD->b0_free) {
		(st = stash_top)->em->saved = -1;
		stash_top = st->next;
		stash_free(st);
	}

	__em->stash = st = many_alloc(GPR_HWORD,1);
	many_save_wreg(__em->stash);
	__em->saved = 0;
	st->em = __em;
	if (!stash_top)
		stash_top = st;
	st->next = NULL;
	if (stash_tail != NULL)
		stash_tail->next = st;
	stash_tail = st;
}

void static stput_intconst(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_STPUT)
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);
	_16_s adr = __lhs->pm.off+p0->off;
	_64_s dis = adr-ladadr;

	_16_u fsr = FSR1_;
	if (p0->nc_off_n != NULL) {
		fsr = FSR0_;
		_stpossr(adr,_FSR1,_FSR0);
	}
	idx_mode = p0->v->u;
	store_intit(__rhs->i,fsr == FSR1_?1:0,p0->v->u,dis,p0->nc_off_n);		
}
//diffrent formalities/flavors
/*
	LHS is frame spec

	8,16,32,64b = 8b
*/
static _8_u reg_mode;
void static
stput(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	printf("PUT: pm_off: %d, off: %u, ladadr: %u.\n", __lhs->pm.off,p0->off,ladadr);
	_16_s __adr = __lhs->pm.off+p0->off;
	assert(p1->vac>0);
	setdesc(dsc_STPUT)
	struct pbs *pb;
	altered(__lhs);
	_64_s dis;
	if (p1->n != NULL) {
		reg_mode = 0;
		__a->prop = p1;
		__a->loadin(__rhs,__a);
	}else
	/*
		if right side is FS and of byte size then fetch it into WREG
	*/
	if (p1->vac == _cfm_ac_sym_fs) {
		if (__rhs->saved == -1) {
			idx_mode = p1->v->u;
			fetchit(__rhs,p1->nc_off_n,p1->off);
			if (!p1->nc_off_n)
				savethis(__rhs);
		}else {
			//goto _somewhere;
		}
	}

	if (p0->nc_off_n != NULL) {
		goto _j0;
	}
	dis = __adr-ladadr;
	WITHINRANGE(dis,8){
		
	} else {
		/*
			here we have a WREG i think, stpos uses WREG so save data to stack and pop it after routine is finished
		*/
		if ((p1->vac&(_cfm_acb_reg|_cfm_acb_sym_fs))>0 && __rhs->saved == -1) {
			push();
		}
		dis = 0;
		stpos(__adr);

		if ((p1->vac&(_cfm_acb_reg|_cfm_acb_sym_fs))>0 && __rhs->saved == -1)
			pop();
	}

	if (!__rhs->saved) {
		many_wreg_retrieve(__rhs->stash);
	}
_j0:
	/*
		these can easy be loaded into wreg
	*/
	if(p1->vac == _cfm_ac_enl) {
		enl_load(__rhs->i&127,__rhs->i>>7);
	}else
	if(__rhs->bits&CFM_EMB_INJECT) {
		struct pbs *pb;
		pb = pbs_new();
		pb->op = _pic_movlw;
		pb->bits[0] = 0;
		pb->n = 1;

		pb->info[1] = _it_slip;
		pb->opand[2] = __rhs->str.s;
		pb->opand0[2] = __rhs->str.len;	
	}

	if (p0->nc_off_n != NULL) {
		idx_mode = p0->v->u;
		pushx();
		_stpossr(__adr,_FSR1,_FSR0);
		p0->nc_off_n->rake.fsr = FSR0_;
		p0->nc_off_n->f(p0->nc_off_n);
		popx();
		save_wreg(0);
		return;
	}

	if (dis != 0) {
		relpf(_pic_movwi,dis,IT_MODEN|IT_MOD_J);
	}else{
		pb = pbs_new();
		pb->op = _pic_movwf;
		pb->info[0] = _imm8;
		pb->opand[0] = 1;
		pb->bits[0] = IT_OAEM_PPD;
		pb->n = 1;	
	}

	/*
		save WREG to somewhere for fast retrevle
	
		if we get somthing thats not the correct size then zero it out
		if dest is bigger then source size.
	*/
	if (p1->vac == _cfm_ac_reg && p0->v->u>1) {
		store_int(0,1,p0->v->u-1,dis+1);
	}
}

void static fsrload(_16_u __adr, _8_u __fsr) {
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movlw;
	pb->info[0] = _imm8;
	pb->opand[0] = __adr>>8;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __fsr&7;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movlw;
	pb->info[0] = _imm8;
	pb->opand[0] = __adr&0xff;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = (__fsr>>3)&7;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

_64_u static loadin_off;
void static
loadin(f_cfm_emp __em, struct cfm_props *p) {
	struct pbs *pb;

	if (__em->bits&CFM_EMB_INJECT) {
		assert(__em->str.len<0x100);
		pb = pbs_new();
		pb->op = _pic_movlw;
		pb->info[0] = _it_slip;
		pb->opand[0] = __em->str.s;
		pb->opand0[0] = __em->str.len;
		pb->bits[0] = 0;
		pb->n = 1;
		return;
	}


	if (p->vac == _cfm_ac_int) {
		pb = pbs_new();
		pb->op = _pic_movlw;
		pb->info[0] = _imm8;
		pb->opand[0] = __em->i;
		pb->bits[0] = IT_OAEM_PPD;
		pb->n = 1;	
	}else
	if (p->vac == _cfm_ac_sym_fs) {
		stfetch(__em->pm.off+loadin_off);
	}else
	if (p->vac == _cfm_ac_enl) {
		_16_u bank;
		_16_u file;
		bank = __em->i>>7;
		file = __em->i&127;
		enl_load(file,bank);
	}
}

_8_u static mtb[8];
_8_u static mtb_tmp[8] = {0,1,2,3,4,5,6,7,8};
void static
prune(void) {
	_int_u j;
	j = 0;
	for(;j != ~0;j++) {
		_8_u i;
		i = mtb[j];
		if (i == 255)
			break;
		struct jb *j = _m.op[i];
		_8_u ig = 0xff;
		if (j->info == _m.info[i]) {
			if (j->opand[0] == _m.opand[i*2]) {
				ig ^= 1;	
			}
			if (j->opand[1] == _m.opand[i*2+1]) {
				ig ^= 2;
			}
			if (j->opand0[0] == _m.opand0[i*2]) {
				ig ^= 4;
			}
			if (j->opand0[1] == _m.opand0[i*2+1]) {
				ig ^= 8;
			}
		}
		_m.ig[i] = 0xff;
		j->info = _m.info[i];
		j->opand[0] = _m.opand[i*2];
		j->opand[1] = _m.opand[i*2+1];
		j->opand0[0] = _m.opand0[i*2];
		j->opand0[1] = _m.opand0[i*2+1];
	}
}

#define _towreg(__fsr) load_wreg(__fsr)
void static _towreg0(f_cfm_emp __em, struct cfm_rake *__a) {
	setdesc(dsc_TOREG)
	__a->loadin(__em, __a);
	load_wreg(0);
}


void static
casg(_64_u __dst) {
	struct pbs *pb;
	_64_u val = __dst;
	_16_u bank = val>>7;
	_16_u file = val&127;
	pb = pbs_new();
	pb->op = _pic_movlb;
	pb->info[0] = _imm8;
	pb->opand[0] = bank;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
	
	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = file;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

void static _cassign0(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movlw;
	pb->bits[0] = 0;
	pb->n = 1;

	if (__rhs->bits&CFM_EMB_INJECT) {
		pb->info[0] = _it_slip;
		pb->opand[0] = __rhs->str.s;
		pb->opand0[0] = __rhs->str.len;
	} else if (p1->vac == _cfm_ac_int) {
		pb->info[0] = _imm8;
		pb->opand[0] = __rhs->i;
		pb->bits[0] = IT_OAEM_PPD;
	}

	casg(__lhs->i);	
}

void static
enl_derefreg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p = CFM_PROPS1(__a);
	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	accum = acum;

	p->n->f(p->n);
	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);
	load_wreg(0);

	casg(__lhs->i);
	many_free(acum);
}

/*
	we expect p->n->f to set WREG
*/
void static
enl_putreg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p = CFM_PROPS1(__a);	
	p->n->f(p->n);
	casg(__lhs->i);
}


/*
	get premuture state of things
	the min'imalist view of things

			/- 16,32,64b
	PORTC = a;
*/
void static
_cassign(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {	
	setdesc(dsc_CASSIGN)
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	stpos(__rhs->pm.off+p1->off);
	_towreg(1);
	casg(__lhs->i);
}

/*
	PORTC = *a;
*/
void static
_cassign_toreg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {	
	setdesc(dsc_CASSIGN)

	__a->prop = CFM_PROPS1(__a);
	__a->loadin(__rhs,__a);
	
	_towreg(0);	
	casg(__lhs->i);
}

void static
func_ret(void) {
	//make sure stpos is where it was left by the calling function
	printf("LADADR: %d, FUNCMASS: %u, PARMASS: %u\n", ladadr,cfm_struc.dw->mass,cfm_struc.dw->size);
	if (ladadr<=0) {	
		printf("%d >= %d\n",ladadr, -(cfm_struc.dw->mass+cfm_struc.dw->size));
	}

	_16_u correct = -ladadr;
	add16_fsr1(correct&0xff, correct>>8);	
}

void static
_func_def(f_cfm_nodep __n) {
	cfm_struc.dw = __n->d;
	cfm_lp();
	it_superyolkp sy;
	sy = trk_cur->sy-1;
	sy->pop = IT_POP_STACK;
	it_ypop po;
	po = cfm_it_syo(sy);
	po->op = _it_yk_ss;
	po->sy = __n->sym->s;

	struct pfw orig;
	orig = _w;

	ladadr = 0;
	if (__n->trk != NULL) {
		cfm_outfck(__n->trk);
	}
	func_ret();
	_w = orig;
	printf("func end.\n");
	sy = cfm_it_superyolk_new();
	cfm_it_yolk_new();
	cfm_struc.dw = NULL;
}

struct goto_strc {
	f_cfm_nodep n;
	struct pbs_break *brk;
	struct goto_strc *next;
	struct pfw w;
};

static struct goto_strc *goto_top = NULL;

void static
_goto(f_cfm_nodep __n) {
	struct goto_strc *gt;
	gt = m_alloc(sizeof(struct goto_strc));
	gt->next = goto_top;
	goto_top = gt;
	gt->n = __n;
	gt->w = _w;
	gt->brk = _brk;
	pbs_breakoff();
}

void static
goto_out(void) {
	struct goto_strc *gt;
	gt = goto_top;
	while(gt != NULL) {
		struct pbs *pb;
		struct savepnt *s;
		s = gt->n->sym->sp;
		/*
			break where pbs struct will be injected into
		*/
		_brk = gt->brk;
		_w = gt->w;
	/*
		ensure stack is correct for the label
		NOTE: slilently sets it does not effect ladadr(read_only)
	*/

		stposs(s->_w._ladadr);

		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_goto;
		pb->info[0] = _f_it_sym;
		pb->opand[0] = gt->n->sym->s;
		pb->bits[0] = 0;

		gt = gt->next;
	}
}
void static
_agrer(struct cfm_rake*);

/*
	TODO:
		
		void test(_8_u *__ptr) {

		}

		_8_u *p;
		test(p);	<- arg gets interpreted as the base type i.e. 8-bit INT
*/
void static
_func_call(f_cfm_nodep __n) {
	f_cfm_dwelp func;
	func = __n->func;
	setdesc(dsc_FUNCCALL)
	cfm_lp();
	struct pbs *pb;
	_int_u i;
	_int_s off;
	_int_s crest = -(cfm_struc.dw->size+cfm_struc.dw->mass);
	i = 0;
	for(;i != 5;i++) {
		struct many_store *m = accum_regs+i;
		_64_u reg;
		_int_u j;
		j = m->b0_free;
		if (m->b0[j] == REG_END)
			continue;
		bankset(m->bank);
		while((reg = m->b0[j]) != REG_END) {
			crest-=8;
			stpos(crest);
			/*
				for NOW!
				TODO: FSR TO FSR copy not this!
			*/
			callthis(_mov2i(8));
			j++;
		}
	}
	
	off = crest;
	i = 0;
	if (__n->n_par != __n->func->n_args) {
		printf("function call - n_params inadequate for function %u#%u.\n",__n->n_par, __n->func->n_args);
	}
	assert(__n->n_par == __n->func->n_args);
	assert(__n->n_par<=8);
	for(;i !=__n->n_par;i++) {
		f_cfm_emp arg = __n->func->args+i;
		f_cfm_nodep n = __n->params[i];
		off-=arg->wide;
		struct cfm_vsp v;
		struct cfm_props *p0,*p1;
		p0 = CFM_PROPS0(n);
		p1 = CFM_PROPS1(n);
		v.u = v._u = arg->wide;
		struct f_cfm_em dst;
		dst.pm.off = off;
		dst.bits = 0;
		if (n->f0 == _agrer) {
			p0->paw = 0;
			p1->paw = 0;
			p0->y = &dst;
			p0->v = &v;
			p0->vac = _cfm_ac_sym_fs;	
		} else {
			assert(1 == 0);
//		n->dst = &dst;
//		n->v0 = dst.vs;
//		n->v0_ac = _cfm_ac_sym_fs;	
		
		}
		printf("################# %u <- %u, %u : %u\n",p0->vac, p1->vac,p0->y->bits,p1->y->bits);
		n->f0(n);
	}
	/*
		its the routines job to put it back to its origiganal value
	*/

	stpos(crest);

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_call;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = __n->sym->s;
	pb->bits[0] = 0;
	if (func->u == 1) {
		push();
	}
	i = 0;
	for(;i != 5;i++) {
		struct many_store *m = accum_regs+i;
		_64_u reg;
		_int_u j;
		j = m->b0_free;
		if (m->b0[j] == REG_END)
			continue;
		bankset(m->bank);
		while((reg = m->b0[j]) != REG_END) {
			stpos(crest);
			crest+=8;
			/*
				for NOW!
				TODO: FSR TO FSR copy not this!
			*/
			callthis(_mov2(8));
			j++;
		}
	}	
	if (func->u>1) {
		if (!reg_mode) {
		//	bankset(4);
		//	load_wreg(R2);
			selreg.bank = 4;
		}
	}else{
		pop();
	}
}
fc_symp static cmp_sy;
void static ifw(f_cfm_nodep __n);
void static
_while(f_cfm_nodep __n) {
	struct savepnt *s = __n->sym->sp;
	fc_symp sy;
	sy = fc_sym_new(NULL, 0);

	cmp_sy = sy;
	_16_u _rst0 = ladadr;
	ifw(__n);

	_16_s _rst = ladadr;

	struct pbs *pb;
	it_ypop po;	
	_64_u jt = cfm_struc.here->jt;
	if (__n->trk != NULL) {
		cfm_outfck(__n->trk);
	}

	stposs(_rst0);

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_goto;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = jt;
	pb->bits[0] = 0;	

	po = cfm_it_ypo(cfm_it_yolk_new());
	po->op = _it_yk_ll;
	po->sy = sy;

	sy->local = 1;
	sy->ident = fc_struc.lid++;
	//wet to where were due to jump if condition doesent fit
	ladadr = _rst;
}
void static
_bittests(f_cfm_nodep __n) {
	struct cfm_props *p;
	p = CFM_PROPS0(__n);
	struct pbs *pb;
	_16_u bank, file;
	if(p->vac == _cfm_ac_sym_fs) {
		stpos(p->y->pm.off+p->off);
	}else
	if(p->vac == _cfm_ac_enl) {
		_64_u val = p->y->i;
		bankset(val>>7);
		file = val&127;
	}else{
		assert(1 == 0);
	}


	pb = pbs_new();
	pb->n = 2;
	pb->op = __n->a;
	pb->info[0] = _imm8;
	pb->opand[0] = file;
	pb->bits[0] = IT_OAEM_PPD;
	
	pb->info[1] = _imm8;
	pb->opand[1] = __n->_0;
	pb->bits[1] = IT_OAEM_PPD;
}
/*
	take FSR and plop into stack
	a = *a;
	moving 1 byte
	8bit destanation
*/
void static
_assign_deref(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_ASSIGN_DEREF);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	__a->prop = p1;
	__a->loadin(__rhs,__a);

	struct pbs *pb;
	stpos(__lhs->pm.off+p0->off);
	pb = pbs_new();
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = 0;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;

}

/*
	8-bit stack 
	_8_u a;
	a = a;
	a = 0;
	simple move assign, etc

	8/16/32/64-bits
	mov $10,%wreg

	8-bit only and not to much specal
	mov $x,$x

	using WREG


	NOTE:

		it is faster to use _assign then _assignp for move operations
		for 8bit operations

		a = a;
		EXAMPLE:
		_assign{
			STPOS(__src)
			LOAD to WREG
			STORE in temp location			
			STPOS(__dst)
			LOAD temp location into WREG	
			MOVE WREG to FSR1				
		}

		_assignp{
			COPY FSR1 TO FSR0		4-instructions
			STPOS(__src)
			STPOS(__dst, FSR0)
			COPY DATA (FSR1 -> FSR0)
		}

		also intconst assignment using _assign is faster aswell
		as no "COPY FSR1 TO FSR0" takes place

	_assignp expects FSR0 to be used as dest while FSR1 as source
*/
void static _assign(f_cfm_nodep __n) {
/*
	setdesc(dsc_ASSIGN)
	struct cfm_rake a;
	a.off = __n->off0;
	a.em = __n->rake.src;	
	a.size = __n->rake.dst->vs->u;
	__n->loadin(__n->rake.dst->pm.off+__n->off, &a);
*/
}

#define STATUS 3
#define Z 2
#define CARRY 0
void static _eqoand(f_cfm_nodep __n);
void ifw(f_cfm_nodep __n) {
	struct pbs *pb;
	_int_u i;
	i = 0;

	for(;i != __n->nt;i++) {
		_eqoand(__n->trickle[i]);
	}

}
void static gotothis(fc_symp __sy) {
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_goto;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = __sy;
	pb->bits[0] = 0;
}

void static
_if(f_cfm_nodep __n) {
	fc_symp sy,el;
	sy = fc_sym_new(NULL, 0);
	cmp_sy = sy;	
	struct pbs *pb;
	ifw(__n);
	_16_s _rst = ladadr;

	it_ypop po;
	if (__n->trk != NULL) {
		cfm_outfck(__n->trk);
	}
	
	stposs(_rst); 

	/*
		walkover else statment if it exists
	*/
	if (__n->_else != NULL) {
		el = fc_sym_new(NULL, 0);
		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_goto;
		pb->info[0] = _f_it_sym;
		pb->opand[0] = el;
		pb->bits[0] = 0;
	}

	po = cfm_it_ypo(cfm_it_yolk_new());
	po->op = _it_yk_ll;
	po->sy = sy;

	sy->local = 1;
	sy->ident = fc_struc.lid++;

	ladadr = _rst;

	if(__n->_else != NULL) {
		cfm_outfck(__n->_else);
		stposs(_rst);

		po = cfm_it_ypo(cfm_it_yolk_new());
		po->op = _it_yk_ll;
		po->sy = el;

		el->local = 1;
		el->ident = fc_struc.lid++;
	}

	ladadr = _rst;
}
_8_u static bo_arth[8] = {
	_pic_addlw,
	_pic_sublw,
	_pic_andlw,
	_pic_iorlw,
	_pic_lsl,
	_pic_lsr,
	255,
	255
};

void static
loadit(f_cfm_emp __em, struct cfm_rake *__a, _64_u __bits) {
	struct cfm_lds *l;
	l = &cfm_pic_sp[CFM_DUB(__a->prop->vac,__bits)];
	__a->loadin = l->loadin;
	l->f(__em,__a);
}

void static _load(struct cfm_props *p) {
}
void static _cmp(_8_u __op) {
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 2;
	_8_u op;
	switch(__op) {
		case _f_cfm_eqt:
			op = _pic_btfsis;
		break;
		case _f_cfm_net:
			op = _pic_btfsic;
		break;
		case _cfm_gt:
			op = _pic_btfsic;
			goto _j1;
		break;
		case _cfm_lt:
			op = _pic_btfsis;
			goto _j1;
		break;
	}


	pb->opand[1] = Z;
	goto _j0;
_j1:
	pb->opand[1] = 0;

_j0:
	pb->op = op;

	pb->info[0] = _imm8;
	pb->opand[0] = STATUS;
	pb->bits[0] = IT_OAEM_PPD;

	pb->info[1] = _imm8;
	pb->bits[1] = IT_OAEM_PPD;	
	gotothis(cmp_sy);

}

void static gtltcmp(void) {
		struct pbs *pb;
		pb = pbs_new();
		pb->n = 2;
		pb->opand[1] = 0;
		pb->op = _pic_btfsic;
		pb->info[0] = _imm8;
		pb->opand[0] = STATUS;
		pb->bits[0] = IT_OAEM_PPD;

		pb->info[1] = _imm8;
		pb->bits[1] = IT_OAEM_PPD;	
		gotothis(cmp_sy);
		
}
_16_u static reduct(_16_u __lhs, _16_u __rhs);
static struct stash *acm;

void static
loadregfsr(struct cfm_props *p, struct stash *acm) {
	accum = acm;
	p->n->f(p->n);
	movft16(FSR0,FSR1);
	if (p->bits&CFM_DEREF) {
		bankset(acm->store->bank);
		movft16(FSR1,R2);
	}else{
		_64_u adr = gpr_bankaddr(acm->store->bank,acm->val[0]);
		loadimm(adr,2,FSR1);	
	}
}

void static
loadr2(_8_u u, struct cfm_props *p, struct stash *acm) {
	if (p->vac == _cfm_ac_reg) {
		accum = acm;
		p->n->f(p->n);
		bankset(acm->store->bank);
		if (p->bits&CFM_DEREF) {
			movft16(FSR0,R2);
			callthis(_mov02(p->v->u));
		}
	}else{
		bankset(acm->store->bank);
		stpos(p->y->pm.off+p->off);
		callthis(_mov2(u));
	}
}

void static
cmp_fsint(_8_u op, struct cfm_props *p0, struct cfm_props *p1) {
	if (p0->vac == _cfm_ac_reg) {
		loadregfsr(p0,acm);
	}else
		stpos(p0->y->pm.off+p0->off);
	
	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	bankset(acum->store->bank);
	loadimm(p1->y->i,p0->v->u,R2);
	callthis(_cmp0(p0->v->u));
	if (p0->vac == _cfm_ac_reg) {
		movft16(FSR1,FSR0);
	}
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 2;
	pb->opand[1] = 0;
	pb->op = op == _f_cfm_eqt?_pic_btfsic:_pic_btfsis;
	pb->info[0] = _imm8;
	pb->opand[0] = R3;
	pb->bits[0] = IT_OAEM_PPD;

	pb->info[1] = _imm8;
	pb->bits[1] = IT_OAEM_PPD;	
	gotothis(cmp_sy);

	many_free(acum);
}

void static
cmp_fs(_8_u op, _8_u u, struct cfm_props *p0, struct cfm_props *p1) {
	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	loadr2(u,p0,acum);

	if (p1->vac == _cfm_ac_reg) {
		loadregfsr(p1,acm);
		bankset(acum->store->bank);
	}else{
		stpos(p1->y->pm.off+p1->off);
		if (p0->vac == _cfm_ac_reg)
			bankset(acum->store->bank);
	}
	callthis(_cmp0(u));
	if (p1->vac == _cfm_ac_reg) {
		movft16(FSR1,FSR0);
	}
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 2;
	pb->opand[1] = 0;
	pb->op = op == _f_cfm_eqt?_pic_btfsic:_pic_btfsis;
	pb->info[0] = _imm8;
	pb->opand[0] = R3;
	pb->bits[0] = IT_OAEM_PPD;

	pb->info[1] = _imm8;
	pb->bits[1] = IT_OAEM_PPD;	
	gotothis(cmp_sy);
	many_free(acum);
}
//we expect p0 to be of fs origins
void static
cmpglt_fsint(_8_u op, struct cfm_props *p0, struct cfm_props *p1) {
	if (op == _f_cfm_eqt || op == _f_cfm_net) {
		cmp_fsint(op,p0,p1);
		return;
	}

	struct stash *acum, *acum0;
	acum = many_alloc(GPR_ACCUM,1);
	
	acum0 = many_alloc(GPR_QUAD,1);
	_64_u in = (_64_u)p1->y->i;
		
	_int_u i;
	i = 0;
	if (op == _cfm_gt) {
		if (p0->vac == _cfm_ac_reg) {
			loadregfsr(p0,acm);
		}else{
			stpos(p0->y->pm.off+p0->off);
		}
		bankset(acum->store->bank);
		loadimm(in,p0->v->u,R2);
		callthis(_sub0(p0->v->u));
		if (p0->vac == _cfm_ac_reg) {
			movft16(FSR1,FSR0);
		}
	}else{
		/*
			there is no getting around this.

			ISSUE:
				is we dont do this it would be <= and not just <
		*/
		loadr2(p0->v->u,p0,acum);

		bankset(acum0->store->bank);	
		loadimm(in,p0->v->u,acum0->val[0]);
		_64_u adr = gpr_bankaddr(acum0->store->bank,acum0->val[0]);
		movft16(FSR0,FSR1);
		loadimm(adr,2,FSR1);

		bankset(acum->store->bank);
		callthis(_sub0(p0->v->u));
		movft16(FSR1,FSR0);
	}
	gtltcmp();	
	many_free(acum);
	many_free(acum0);		
}

void _cmpfsfs(_8_u u, struct stash *acum, struct cfm_props *p0, struct cfm_props *p1) {
	loadr2(u,p1,acum);
	if (p0->vac == _cfm_ac_reg) {
		loadregfsr(p0,acm);
		bankset(acum->store->bank);
	}else{
		stpos(p0->y->pm.off+p0->off);
		if (p1->vac == _cfm_ac_reg)
			bankset(acum->store->bank);
	}
	callthis(_sub0(u));

	if (p0->vac == _cfm_ac_reg) {
		movft16(FSR1,FSR0);
	}
}

void cmpfs(_8_u op, _8_u u, struct cfm_props *p0, struct cfm_props *p1) {
	if (op == _f_cfm_eqt || op == _f_cfm_net) {
		cmp_fs(op,u,p0,p1);
		return;
	}
	struct stash *acum, *acum0;
	acum = many_alloc(GPR_ACCUM,1);
	if (op == _cfm_gt) {
		_cmpfsfs(u,acum,p0,p1);
	}else{
		_cmpfsfs(u,acum,p1,p0);
	}

	gtltcmp();
	many_free(acum);
}

void static _eqo(f_cfm_nodep __n) {
	setdesc(dsc_EQO);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__n);
	p1 = CFM_PROPS1(__n);

	struct pbs *pb;

	__n->rake.prop = p0;
	//dst = p0
	//src = p1;
	_8_u u = reduct(p0->v->u,p1->v->u);

	if (u>1) {
		acm = many_alloc(GPR_ACCUM,1);

		if (p1->vac&(_cfm_ac_sym_fs|_cfm_ac_reg) && p0->vac == _cfm_ac_int) {
			cmpglt_fsint(__n->op,p1,p0);
		}else
		if (p1->vac == _cfm_ac_int && p0->vac&(_cfm_ac_sym_fs|_cfm_ac_reg)) {
			cmpglt_fsint(__n->op,p0,p1);	
		}else{
			/*
				its best to have any REG on the left hand side
			*/
			if (p0->vac == _cfm_ac_sym_fs && p1->vac == _cfm_ac_reg) {
				cmpfs(__n->op,u,p1,p0);
			}else
				cmpfs(__n->op,u,p0,p1);
		}
		many_free(acm);
		return;
	}
	assert(p0->vac != _cfm_ac_reg);
	if (p1->vac == _cfm_ac_int) {
		loadit(p0->y,__n,p0->bits);

		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_sublw;
		pb->info[0] = _imm8;
		pb->opand[0] = p1->y->i;
		pb->bits[0] = IT_OAEM_PPD;
	}else
	if (p1->vac == _cfm_ac_sym_fs) {
		loadit(p0->y,__n,p0->bits);

		if (p0->bits&CFM_DEREF) {
			push();
			load16(p1->y->pm.off+p1->off);
			pop();
	
			pb = pbs_new();
			pb->n = 1;
			pb->op = _pic_subwf;
			pb->info[0] = _imm8;
			pb->opand[0] = 0;
			pb->bits[0] = IT_OAEM_PPD;
		
		} else {
			push();
			stpos(p1->y->pm.off+p1->off);	
			pop();

			pb = pbs_new();
			pb->n = 1;
			pb->op = _pic_subwf;
			pb->info[0] = _imm8;
			pb->opand[0] = 1;
			pb->bits[0] = IT_OAEM_PPD;
		
		}
	} else if (p1->vac == _cfm_ac_reg){
		p1->n->f(p1->n);
		save_wreg(TMP0);
		loadit(p0->y,__n,p0->bits);
		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_subwf;
		pb->info[0] = _imm8;
		pb->opand[0] = TMP0;
		pb->bits[0] = IT_OAEM_PPD;
	}else
	if(p1->vac == _cfm_ac_enl) {
		enl_load(p1->y->i&127,p1->y->i>>7);
		save_wreg(TMP0);
		loadit(p0->y,__n,p0->bits);

		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_subwf;
		pb->info[0] = _imm8;
		pb->opand[0] = TMP0;
		pb->bits[0] = IT_OAEM_PPD;	
	}

	_cmp(__n->op);
}

void _eqoand(f_cfm_nodep __n) {
	__n->f(__n);
}


void static _shftint(f_cfm_nodep __n, _8_u op) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__n);
	p1 = CFM_PROPS1(__n);

	struct pbs *pb;
	loadin(p0->y,p0);
	/*
		place dest into WREG then move into FILE reg and do operation finishing with exporting FILE to WREG
	*/
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP0;
	pb->bits[0] = IT_OAEM_PPD;
	
	_int_u n = p1->y->i;
	_int_u i;
	i = 0;
	for(;i != n;i++) {
		pb = pbs_new();
		pb->n = 1;
		pb->op = op;
		pb->info[0] = _imm8;
		pb->opand[0] = TMP0;
		pb->bits[0] = IT_OAEM_PPD;
		if (i != n-1) {
			pb->bits[0] |= IT_MODEN|IT_MOD_J; 
		}
	}
}

void static
_shftby(f_cfm_nodep __n, _8_u op) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__n);
	p1 = CFM_PROPS1(__n);

	struct pbs *pb;
	loadin(p0->y,p0);//LHS

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP1;
	pb->bits[0] = IT_OAEM_PPD;

	//load RHS
	loadin(p1->y,p1);

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP0;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_incf;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP0;
	pb->bits[0] = IT_MODEN|IT_MOD_J;
	fc_symp sy;
	it_ypop po;
	sy = fc_sym_new(NULL, 0);
	po = cfm_it_ypo(cfm_it_yolk_new());
	po->op = _it_yk_ll;
	po->sy = sy;

	sy->local = 1;
	sy->ident = fc_struc.lid++;

	fc_symp sy0;
	sy0 = fc_sym_new(NULL, 0);
	sy0->local = 1;
	sy0->ident = fc_struc.lid++;
	
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_decf;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP0;
	pb->bits[0] = IT_MODEN|IT_MOD_J;

	pb = pbs_new();
	pb->n = 2;
	pb->op = _pic_btfsic;
	pb->info[0] = _imm8;
	pb->opand[0] = STATUS;
	pb->bits[0] = IT_OAEM_PPD;

	pb->info[1] = _imm8;
	pb->opand[1] = Z;
	pb->bits[1] = IT_OAEM_PPD;
	
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_goto;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = sy0;
	pb->bits[0] = 0;

	pb = pbs_new();
	pb->n = 1;
	pb->op = op;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP1;
	pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
	
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_goto;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = sy;
	pb->bits[0] = 0;

	po = cfm_it_ypo(cfm_it_yolk_new());
	po->op = _it_yk_ll;
	po->sy = sy0;
	
	/*
		finaly move result into WREG 
	*/
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = TMP1;
	pb->bits[0] = 0;
}
// binary operation, add,sub,mul,shift etc

/*
	thinking session

	a = a[10]+a

	the first one a[] can be FSR0 yes

	a = a[10]+a[10]
	a = FSR0+(FSR1->WREG)
	movf
	addwf


	now

	a = func(0)+a[10];

	WREG = func()
	addwf

*/
/*
	if prop-1 is of the int variety then 
	we load prop-0 into WREG
*/

/*
	k = constant literal
	w = wreg
	f = file

	things to be aware of

	ADD has no order requirements

	SUBLW	= 	k	-	w
	SUBWF	= 	w	-	f

	ANDLW	= 	w	&	k
	IORLW	= 	w	|	k
	ANDWF	= 	w	&	f
	IORWF	= 	w	|	f

	bo0_arth_and_logic

	load p0 into wreg and p1 into what ever it is

	as we AND* needs the left hand side loaded with WREG
	we just switch p0 with p1.
*/
void static bo0_arth_and_logic(struct cfm_props *p0, struct cfm_props *p1, _8_u op) {
	struct pbs *pb;

	/*
		a = a+b+c+etc	
	*/
	if (p1->vac == _cfm_ac_reg) {
		p1->n->f(p1->n);
		op++;
	}else

	/*
		P1 is naturally loaded into WREG
	*/
	if (p1->nc_off_n != NULL) {
		stpos(p1->y->pm.off+p1->off);
		movft16(FSR0,FSR1);
		p1->nc_off_n->f(p1->nc_off_n);
		load_wreg(0);
	}else
	if (p1->vac == _cfm_ac_sym_fs) {
		loadin_off = p1->off;
		loadin(p1->y,p1);
	}else goto _j0;
	push();
_j0:
	if (p0->nc_off_n != NULL) {
		op++;
		stpos(p0->y->pm.off+p0->off);
		movft16(FSR0,FSR1);
		p0->nc_off_n->f(p0->nc_off_n);
	}else
	if (p0->vac == _cfm_ac_sym_fs) {
		op++;
		stpos(p0->y->pm.off+p0->off);
	}

	if (p1->vac == _cfm_ac_int || p1->vac == _cfm_ac_enl) {
		loadin(p1->y,p1);
	}else {
		pop();
	}
	
	if(p0->vac == _cfm_ac_enl) {
		bankset(p0->y->i>>7);
	}

	pb = pbs_new();
	pb->n = 1;
	pb->op = op;
	pb->info[0] = _imm8;
	pb->bits[0] = IT_OAEM_PPD;
	if (p0->nc_off_n != NULL) {
		pb->opand[0] = 0;
	}else
	if (p0->vac == _cfm_ac_int) {
		pb->opand[0] = p0->y->i;
	}else
	if (p0->vac == _cfm_ac_enl) {
		pb->opand[0] = p0->y->i&127;
		pb->op++;
	}else
	if (p0->vac == _cfm_ac_sym_fs) {
		pb->opand[0] = 1;//INDF1
	}else if (p0->vac == _cfm_ac_reg){
		pb->opand[0] = TMP0;
	}else{
		printf("WHAT THE FUCK!.\n");
		assert(1 == 0);		
	}

}

void static _bo0(f_cfm_nodep __n) {
	setdesc(dsc_BO);
	struct pbs *pb;
	fc_symp sy;
	if (__n->op == _cfm_mul) {
		return;
	}
	_8_u op;
	op = bo_arth[__n->op&7];
	assert(op != 255);
	_int_u n = 1;
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__n);
	p1 = CFM_PROPS1(__n);

	// a = 1+1; this scenario is impermissible	
	assert((p0->vac|(p1->vac<<4)) != CFM_DUB(_cfm_ac_int,_cfm_ac_int));

	if ((__n->op&((_cfm_shl|_cfm_shr)&_cfm_som))>0) {
		if (p1->vac == _cfm_ac_int) {
			_shftint(__n, op);
		} else if (p1->vac == _cfm_ac_sym_fs) {		
			_shftby(__n,op);
		}
		return;
	}

//END
	if ((__n->op&((_f_cfm_add|_f_cfm_sub)&_cfm_som))>0) {
		bo0_arth_and_logic(p0,p1,op);
	}else{
		bo0_arth_and_logic(p1,p0,op);
	}
}

/*
	load value from stack into fsr register
	FSR(0)
*/
void load16(_64_u __off) {	
	struct pbs *pb;
	_64_s dis = __off-ladadr;
/*
		EXAMPLE
		a[21+i] = 42;

		a+=i;
		relpf(21)
*/
	WITHINRANGE(dis,2) {
		relpf(_pic_moviw,dis,IT_MODEN|IT_MOD_J);
		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_movwf;
		pb->info[0] = _imm8;
		pb->opand[0] = 4;
		pb->bits[0] = IT_OAEM_PPD;
	
		relpf(_pic_moviw,dis+1,IT_MODEN|IT_MOD_J);
		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_movwf;
		pb->info[0] = _imm8;
		pb->opand[0] = 5;
		pb->bits[0] = IT_OAEM_PPD;
		return;
	}
/*
		a+=i;
		stpos(21)
*/
	//fetch from stack
	stpos(__off);
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_OAEM_PPD;


	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 4;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->op = _pic_addfsr;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
	pb->n = 1;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 5;
	pb->bits[0] = IT_OAEM_PPD;
	ladadr++;
}

/*
	complex move operations using memory NOT registers
	could involve many diffrent instructions
	a = *a; - deref rhs and store in lhs
	mem to mem
*/
_16_u reduct(_16_u __lhs, _16_u __rhs) {
	if (__lhs<__rhs)
		return __lhs;
	return __rhs;
}

void static take_addr(void);

void static 
numstoretmp(_64_u __i, _64_u __sz) {
	bankset(accum->store->bank);
	_64_u s = 4;
	_64_u n = accum->val[0];
	_64_u i =__i;
	struct pbs *pb;
_next:
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movlw;
	pb->info[0] = _imm8;
	pb->opand[0] = i&0xff;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = n;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movlw;
	pb->info[0] = _imm8;
	pb->opand[0] = (i>>8)&0xff;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = n+1;
	pb->bits[0] = IT_OAEM_PPD;
	if (s != 1<<__sz) {
		s<<=2;
		i>>=16;
		n+=2;
		goto _next;
	}	
}

static _64_u bo_op;
static _8_u bop_op;


void static
_logiconreg(_64_s op, f_cfm_emp __em, struct cfm_rake *__a, _int_u __size) {
	struct cfm_props *p0;
	p0 = __a->prop;
	bankset(accum->store->bank);
	struct pbs *pb;
	if (p0->vac == _cfm_ac_int) {
		/*
			cant do nothing about this
		*/
		_64_u s = 2;
		_64_u i;
		_64_u n = 0;
		i = __em->i;
	_next:
		pb = pbs_new();
		pb->n = 1;
		pb->op = _pic_movlw;
		pb->info[0] = _imm8;
		pb->opand[0] = i&0xff;
		pb->bits[0] = IT_OAEM_PPD;
		pb = pbs_new();
		pb->op = op;
		pb->info[0] = _imm8;
		pb->opand[0] = accum->val[0]+n;
		pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
		pb->n = 1;
		if (s != 1<<__size) {
			s<<=1;
			n++;
			i>>=8;
			goto _next;
		}

	} else {
		callthis(bop_at(__size,bop_op,__bop_0));
	}
}

/*
	do add,sub on register
*/
void static
_shiftonreg(_64_s op, f_cfm_emp __em, struct cfm_rake *__a, _int_u __size) {
	struct cfm_props *p0;
	p0 = __a->prop;

	struct pbs *pb;
	/*
		this big shit here is as good as its going to get for int shifts x<<21
	
		TODO:
		 if we have a shift size bigger then the widness then 
		 just clrf the whole thing
	
		for now just dont allow it.
	*/

	if (p0->vac == _cfm_ac_int) {
		assert(__size<<3 != __em->i);
		bankset(accum->store->bank);
		_8_u file = accum->val[0];
		_int_u i,j;
	if (op == _pic_lsl) goto _lsl;
	if (__em->i>>3 && !(__em->i&7) && __size != 2) {
		i = 0;
		for(;i != __em->i>>3;i++) {
			callthis(_shrh(__size));
			clrf(file+__size-1);
		}
	}else
	/*
		in the case we are operating on a 16b table we dont use a lib routine,
		WHY? because
		we only have two instructions
	*/
	if (__em->i == 8 && __size == 2) {
		load_wreg(file+1);
		save_wreg(file);
		clrf(file+1);
	} else{
		j = 0;
		for(;j != __em->i;j++) {
			callthis(_shr(__size));	
		}
	}
	return;
_lsl:
	if (__em->i>>3 && !(__em->i&7) && __size != 2) {
		i = 0;
		for(;i != __em->i>>3;i++) {
			clrf(file);
			callthis(_shlh(__size));
		}
	}else
	if (__em->i == 8 && __size == 2) {
		i = 0;
		for(;i != __size-1;i++) {
			load_wreg(file+i);
			save_wreg(file+i+1);
		}
		clrf(file);
	}else{
		j = 0;
		for(;j != __em->i;j++) {
			callthis(_shl(__size));
		}
	}
	}else if (p0->vac == _cfm_ac_sym_fs) {

	}
}
void static
_arthonreg(_64_s op, f_cfm_emp __em, struct cfm_rake *__a, _int_u __size) {
	struct cfm_props *p0;
	p0 = __a->prop;
	struct cfm_props *p1;
	p1 = CFM_PROPS0(__a);

	struct pbs *pb;	
	if (p0->vac == _cfm_ac_int) {
		_64_u i = __em->i;
		if (p1->y->bits&CFM_PTR) {
			i*=p1->v_ptr->u;
		}

		bankset(accum->store->bank);
		imm_wreg(i&0xff);

		pb = pbs_new();
		pb->op = op;
		pb->info[0] = _imm8;
		pb->opand[0] = accum->val[0];
		pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
		pb->n = 1;

		op++;
		_64_u s = 4;
		_64_u n = 1;
		i>>=8;
	_next:
		imm_wreg(i&0xff);

		pb = pbs_new();
		pb->op = op;
		pb->info[0] = _imm8;
		pb->opand[0] = accum->val[0]+n;
		pb->bits[0] = IT_OAEM_PPD|IT_MODEN|IT_MOD_J;
		pb->n = 1;
		if (s != 1<<__size) {
			s<<=1;
			n++;
			i>>=8;
			goto _next;
		}

	} else {
		_64_u fsr = 1;
		fc_symp mov = _mov0(2);

		if (p0->vac == _cfm_ac_reg) {
			if (p0->n->f == _bo0) {
				bankset(accum->store->bank);
				p0->n->f(p0->n);
				save_wreg(R0);
				loadimm(0,__size-1,R0+1);
				if (p1->y->bits&CFM_PTR && p1->v_ptr->u>1) {
					loadimm(p1->v_ptr->u|(1<<8),2,R1);
					callthis(_mul0(2));
				}else {
					fc_symp sy = bop_at(__size,bop_op,__bop_00);
					assert(sy != NULL);
					callthis(sy);
				}
				return;
			}
			/*
				in this case we have another bo
			*/
			struct stash *acum;
			_64_u adr;
			if (p0->paw == CFM_PAW_SELREG) {
				p0->n->f(p0->n);
				adr = gpr_bankaddr(selreg.bank,R2);
			}else{
				struct stash *orig;
				acum = many_alloc(GPR_ACCUM,1);
				orig = accum;
				accum = acum;
				p0->n->f(p0->n);
				accum = orig;
				adr = gpr_bankaddr(acum->store->bank,R2);
			}
			movft16(FSR0,FSR1);
			loadimm(adr,2,FSR1);

			fsr = 0;
			mov = _mov00(2);
			if (p0->paw != CFM_PAW_SELREG) {
				many_free(acum);
			}
		}else {
			stpos(__em->pm.off+p0->off);
		}
		bankset(accum->store->bank);
		/*
			we expect FSR stored value
			from ether REG or FS
		*/
		if (p1->y->bits&CFM_PTR && p1->v_ptr->u>1) {
			if (p0->v->u == 1) {
				assert(p0->vac != _cfm_ac_reg);
				move8(R0,fsr);
				loadimm(0,1,R0+1);
			}else{
				callthis(mov);
			}
	
			loadimm(p1->v_ptr->u|(1<<8),2,R1);
			callthis(_mul0(2));
		}else {
			struct stash *tmp;
			tmp = many_alloc(GPR_QUAD,1);
			if (__size != p0->v->u) {
				_64_u adr = gpr_bankaddr(tmp->store->bank,tmp->val[0]);
				movft16(FSR0,FSR1);
				loadimm(adr,2,FSR1);
				if (p0->v->u == 1) {
					move8(1,0);
				}else{
					move10(p0->v->u);
				}
			}
			if (__size == 2 && p0->v->u == 1) {		
				emitop(op,_imm8,R2,IT_OAEM_PPD|IT_MODEN|IT_MOD_J);
				clrw();
				emitop(op+1,_imm8,R2+1,IT_OAEM_PPD|IT_MODEN|IT_MOD_J);
			}else {
			
				clrw();
				if (__size != p0->v->u) {
				if (__size == 4 && p0->v->u == 1) {
					callthis(_clr1d);
				}else
				if (__size == 8 && p0->v->u == 1) {
					callthis(_clr1q);
				}else
				if (__size == 4 && p0->v->u == 2) {
					callthis(_clr2d);
				}else
				if (__size == 8 && p0->v->u == 2) {
					callthis(_clr2q);
				}else
				if (__size == 8 && p0->v->u == 4) {
					callthis(_clr4q);
				}else{
					printf("S%u ~ S%u.\n",__size,p0->v->u);
					assert(1 == 0);
				}
				}
				callthis(bop_at(__size,bop_op,__bop_0));
				if (p0->vac == _cfm_ac_reg) {
					movft16(FSR1,FSR0);
				}
			}
			if (__size != p0->v->u) {
				movft16(FSR1,FSR0);
			}
			many_free(tmp);
		}
	}
}

void static
_bo(f_cfm_nodep __n) {
	setdesc(dsc_BO);
	_8_u op;
	bo_op = __n->op;
	struct pbs *pb;
	op = bo_arth[__n->op&7];
	assert(op != 255);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__n);
	p1 = CFM_PROPS1(__n);

	_64_u u;
	u = p1->v->u;
	if (p0->v->u>u)
		u = p0->v->u;
	printf("-----> BOOP: %u.\n", op);
	/*
		NOTE:
			only the right hand side can be a _cfm_reg when it comes to bo+bo
			exemption func call
	*/
	if (p0->vac == _cfm_ac_int) {
		_64_u i;
		i = p0->y->i;
		if (p1->y->bits&CFM_PTR) {
			i*=p1->v_ptr->u;
		}
		/*
			we cant do nothing about this
			unless its somthing like all zeros.
		*/
		numstoretmp(i, u);
	} else {
		if (p0->vac == _cfm_ac_reg) {
			if (p0->paw == CFM_PAW_SELREG) {
				p0->n->f(p0->n);			
				callthis(_movfsr01);
				loadimm(gpr_bankaddr(selreg.bank,R2),2,FSR1);
				bankset(accum->store->bank);
				callthis(_mov2(p0->v->u));
				callthis(_movfsr10);
			}
		}else{
		stpos(p0->y->pm.off+p0->off);
		bankset(accum->store->bank);
		if (p1->y->bits&CFM_PTR && p1->v_ptr->u>0) {
			if (p0->v->u == 1) {
				move8(R0,1);
				loadimm(0,1,R0+1);
			}else{
				callthis(_mov0(2));
			}
			loadimm(p1->v_ptr->u|(1<<8),2,R1);
			loadimm(0,2,R2);
			callthis(_mul0(2));
		}else {
			if (p0->y->bits&CFM_PTR && p0->v_ptr->u>1) {
				callthis(_mov2(2));
			}else {
				callthis(_mov2(p0->v->u));
			}
		}
		}
	}

	__n->rake.prop = p1;
	_8_u bop = 255;
	switch(bo_op) {
		case _f_cfm_add:
		bop = __add;
		break;
		case _f_cfm_sub:
		bop = __sub;
		break;
		case _cfm_or:
		bop = __or;
		break;	
		case _cfm_and:
		bop = __and;
		break;
	}
	bop_op = bop;

	if ((__n->op&((_f_cfm_add|_f_cfm_sub)&_cfm_som))>0)
		_arthonreg(op+1,p1->y,&__n->rake,u);
	else if((__n->op&((_cfm_and|_cfm_or)&_cfm_som))>0)
		_logiconreg(op+1,p1->y,&__n->rake,u);
	else if((__n->op&((_cfm_shl|_cfm_shr)&_cfm_som))>0)
		_shiftonreg(op,p1->y,&__n->rake,u);
	else {
		assert(1 == 0);
	}
	/*
		OUTPUT -> TMP0-TMP7
	*/
}

void
addtofsr(_64_u __n, _8_u __bits){
	assert(__n<64);
	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_addfsr;
	pb->info[0] = _imm8;
	pb->opand[0] = __n;
	pb->bits[0] = IT_OAEM_PPD|__bits;
	pb->n = 1;
}

#define ADDTOFSR0 0
#define ADDTOFSR1 (IT_MODEN|IT_MOD_J)
/*
	dont know a good name for this
	bref:
		fill in all zeros 
		unfold to size of dest
*/
void static unfurl(_16_u u, _16_u _u, _16_u __fsr) {
	if (u != _u) {
		store_int(0,__fsr,_u-u,u);
	}
}

/*
	take addr of FSR1 and put in FSR0 location
*/
void take_addr(void) {
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = 6;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 0;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->op = _pic_addfsr;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
	
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = 7;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 0;
	pb->bits[0] = IT_OAEM_PPD;
}

void static
store_adr(f_cfm_emp __em, struct cfm_props *__p) {
	stpos(__em->pm.off+__p->off);	
	take_addr();
}

void static
_deref_assign_addrof_reg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p = CFM_PROPS0(__a);

	struct stash *acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	p->n->f(p->n);

	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);

	store_adr(__rhs,CFM_PROPS1(__a));
	many_free(acum);
}

/*
	a = &a;
	*a = &a;
*/
void static
_deref_assign_addrof(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_ADDROF);

	__a->prop = CFM_PROPS0(__a);
	__a->loadin(__lhs, __a);
/*
	position stack ptr to correct place to where the data we
	are taking the address of is.
*/
	store_adr(__rhs,CFM_PROPS1(__a));
}

char const static *ac_name[] = {
	"NULL",
	"SYMBOL STRAY",
	"SYMBOL FRAME SPECIFIC",
	"INT",
	"REG",
	"ENL"
};

void(*cfm_pic_f(struct cfm_rake *__a, _64_u __lhs, _64_u __rhs, _64_u __bits0, _64_u __bits1, _64_u __lhsu, _64_u __rhsu, _64_u __paw0, _64_u __paw1))(void*,void*,void*) {
	printf("PICf: %u, %u, %u, %u - %u,%u\n",__lhsu, __rhsu,__bits0, __bits1,__lhs,__rhs);
	assert((__lhs&0xf)<6 && (__rhs&0xf)<6);
	printf("PICf: %s, %s.\n", ac_name[__lhs&0xf], ac_name[__rhs&0xf]);

	struct cfm_nts **nts_p = cfm_optab[CFM_DUB((_64_u)__lhs,(_64_u)__rhs)];
	struct cfm_nts *nts;
//	assert(1 == 0);
	assert(nts_p != NULL);
	nts = nts_p[CFM_DUB0(__bits0,__bits1)];
	assert(nts != NULL);
	_8_u u = __lhsu|(__rhsu)<<4;
	printf("MASK: %x, %x.\n",nts->bits[0], nts->bits[1]);
	void(*load)(void*,void*,void*);
	if (nts->bits[0]&(u&0xf0) && nts->bits[0]&(u&0x0f)) {
		__a->loadin = nts->loadin[0];
		load = nts->f[0+__paw1];
	}else
	if (nts->bits[1]&(u&0xf0) && nts->bits[1]&(u&0x0f)) {
		__a->loadin = nts->loadin[1];
		load = nts->f[1+__paw1];
	}else
	if (nts->bits[2]&(u&0xf0) && nts->bits[2]&(u&0x0f)) {
		__a->loadin = nts->loadin[2];
		load = nts->f[2+__paw1];
	}else{
		printf("U: %x.\n", u);
		assert(1 == 0);
	}
	ffly_fdrain(_ffly_out);
	return load;
}
/*
	issue function params/args assignment


	func(21299)

	var = 21299;
	this happens here not at clang or other level

	meaning: ag rerouting
*/
void
_agrer(struct cfm_rake *__rk) {	
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__rk);
	p1 = CFM_PROPS1(__rk);
	_8_u u0;
	_8_u u1;
	/*
		when deref we dont realy care about the size of the pointer but 
		we do care about the wideness of the data we are derefing
	*/
	u0 = p0->bits&CFM_DEREF?p0->v_ptr->u:p0->v->u;
	u1 = p1->bits&CFM_DEREF?p1->v_ptr->u:p1->v->u;

	void(*load)(void*,void*,void*);
	load = cfm_pic_f(__rk,p0->vac,p1->vac,p0->bits,p1->bits,u0,u1,p0->paw,p1->paw);
	load(p0->y,p1->y,__rk);
}

void static
_func_call_arg_func_call(f_cfm_nodep __n) {
/*
	_n->f(__n);
	struct cfm_rake r;
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(&r);
	p1 = CFM_PROPS1(&r);	
	p0->bits = 0;
	p1->bits = 0;

	r.nc_off = NULL;
	r.nc_off0 = NULL;
	r.nc_dis = NULL;
	r.nc_dis0 = NULL;
	r.off = 0;
	r.off0 = 0;
	r.dis = 0;
	r.dis0 = 0;

	r.dst = __n->dst;
	r.v0 = __n->v0;
	r.v0_ac = __n->v0_ac;

	r.src = f_cfm_regm;
	r.v1 = f_cfm_regm->vs;
	r.v1_ac = _cfm_ac_reg;
	r.v1_n = NULL;
	_agrer(&r);
*/
}
void static
_deref_deref(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a);
void(*cfm_pic_func[64])(f_cfm_nodep) = {
	_func_def,_assign,NULL,_bo0,NULL,NULL,
	_func_call,NULL,_goto,_if,_while,NULL,
	NULL,_cassign,_bittests,_bo,
	_deref_deref,_deref_assign_addrof,
	NULL,_cassign_toreg,_agrer,_eqo,_eqoand,_func_call_arg_func_call,add_off,add_off_with
};
/*
	put value of reg into mem location

	stput_reg_ would be better to put bank into selbank?
	ISSUE:
		pX-n->f()  does not preserve selbank
*/
void static
stput_reg_(_64_u __bank, struct cfm_props *p0) {
	stpos(p0->y->pm.off+p0->off);
	bankset(__bank);
	callthis(_mov2i(p0->v->u));
}

void static
stput_selreg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_PUTREG)
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	p1->n->f(p1->n);
	stput_reg_(selreg.bank,p0);
}

void static
stput_reg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_PUTREG)
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	/*
		why not just feed it the real dest location?
		if we stuff FSR0 with the dest
		A) we would have to preserve it, so we would have to store it in a temp location so it can be used else where
		B) addwf does not include a offset so FSR would need to be incremented each time 'addfsr'
	*/
	
	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	p1->n->f(p1->n);
	stput_reg_(acum->store->bank,p0);
	many_free(acum);
}

/*
	int *a;

	a[X] = Y;

	the pointer is comming from the stack
*/
void static apply_off(struct cfm_rake *__a) {
	if (__a->prop->nc_off_n != NULL) {
		/*
			in the case we get 

			a[X+Y] = Y;
		*/

		/*
			add offset,

			add_off,add_off_with
		*/
		__a->prop->nc_off_n->rake.fsr = FSR0_;
		__a->prop->nc_off_n->f(__a->prop->nc_off_n);	
	}
	if (__a->prop->off>0){
		addtofsr(__a->prop->off,ADDTOFSR0);
	}
}

void static
stput_deref(f_cfm_emp __em, struct cfm_rake *__a) {
	setdesc(dsc_DEREF)
	load16(__em->pm.off);
	apply_off(__a);
}
/*

	when adding offset to FSR1 thats not known at compile time
	it required that its removed
	EXAMPLE

	add-offset

	do stuff that requires offset to be there

	remove-offset

	EXAMPLE
	stray symbol deref
*/
void static
ss_deref(f_cfm_emp __em, struct cfm_rake *__a) {
/*
	where symbol = address from 0x2000 to -> 0x2fef

	where off is added to address of symbol and dis is added to the pointer contaned within
	FSR0_L = off(symbol_L)
	FSR0_H = off(symbol_H)
	FSR0+=dis	

	to add a offset to a symbol first the address of the symbol must be loaded
	into FSR1 temporarily the a add operation is done on FSR1 after witch
	contents of location is dumpt into FSR0 and the displacment is added

	NOTE:
		this is only for the unknown INT-CONST displacments can be incorparated into the instruction and done at linking time
		EXAMPLE
			movlw -4(symbol) - move contents of symbol-4 into WREG

*/
}

/*
	a = *(a+10)

	fs = reg+DEREF
*/
void static
reg_deref(f_cfm_emp __em, struct cfm_rake *__a) {
	setdesc(dsc_DEREF)

	struct stash *acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	__a->prop->n->f(__a->prop->n);
	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);
	load_wreg(0);
	many_free(acum);
}
/*
	(a+10)[0] = 0;
*/
void static
_reg_deref(f_cfm_emp __em, struct cfm_rake *__a) {
	setdesc(dsc_DEREF)
	struct stash *acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	__a->prop->n->f(__a->prop->n);//POINTER = REG = TMP0
	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);
	many_free(acum);
	apply_off(__a);
}

void static
asg0(f_cfm_emp __em, struct cfm_rake *__a) {
	setdesc(dsc_ASSIGN0);
	__a->loadin(__em, __a);
}

void static
_extract_addr(f_cfm_emp __em, struct cfm_rake *__a) {
	load16(__em->pm.off+__a->prop->off);
}
/*
	non-8b move operations 
	a = a;

	simply setting both FSR's and moving data between them
*/
void static
__assign(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	movft16(FSR0,FSR1);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	_stposs(__lhs->pm.off+p0->off,_FSR0);
	stpos(__rhs->pm.off+p1->off);

	_int_u u;
	u = reduct(p0->v->u,p1->v->u);

	move01(u);
	unfurl(u,p0->v->u,0);
}
/*
	16b+
	a = *a;

	note LHS is not a pointer
	and assured a FS, the only side we thats unknown is the RHS
*/
void static
_assign0(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) { 	
	setdesc(dsc_ASSIGNP);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	__a->prop = p1;
	asg0(__rhs,__a);

	stpos(__lhs->pm.off+p0->off);
	_16_u u;
	u = reduct(p0->v->u, p1->v_ptr->u);
	
	move10(u);
	unfurl(u,p0->v->u,1);
}

/*
	16b+
	*a = a;
*/
void static
_assign1(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_ASSIGNL);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	__a->prop = p0;
	asg0(__lhs,__a);

	stpos(__rhs->pm.off+p1->off);
	_16_u u;
	u = reduct(p0->v_ptr->u,p1->v->u);

	move01(u);
	unfurl(u,p0->v_ptr->u,0);
}
/*
	frame-specific
	mem(16,32,64) = *(8-bit)
*/
void static 
_deref_regfit(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_DEREF_REGFIT);
	struct cfm_props *p;
	p = CFM_PROPS0(__a);

	__a->prop = CFM_PROPS1(__a);
	asg0(__rhs,__a);
	//place derefrenced 8bit var to dest
	stpos(__lhs->pm.off+p->off);	
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movf;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_OAEM_PPD;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 0;
	pb->bits[0] = IT_OAEM_PPD;
}

/*
	int *a;
	a[X] = INTCONST'0;
*/
void static
_deref_intconst(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	__a->prop = CFM_PROPS0(__a);
	asg0(__lhs,__a);

	struct cfm_props *p;
	p = CFM_PROPS0(__a);

	//cram int into FSR0
	//NOTE: must be same size as DEST
	store_int(__rhs->i,0,p->v_ptr->u,0);
}

void static
stry_load(f_cfm_symp __sy, _64_u __fsr0, _64_u __fsr1, _32_s __dis) {
	struct pbs *pb;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movlw;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = __sy->s;
	pb->bits[0] = IT_OAEM_accost_low;
	pb->dis[0] = __dis;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __fsr0;
	pb->bits[0] = 0;
		
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movlw;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = __sy->s;
	pb->bits[0] = IT_OAEM_accost_high;
	pb->dis[0] = __dis;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = __fsr1;
	pb->bits[0] = 0;	

}

//loadimm8
void static
_stry_int(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	stry_load(__lhs->sym,_FSR0,p0->off);
	store_int(__rhs->i,0,p0->v->u,0);
}

void static
_stry_enl(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);


	stry_load(__lhs->sym,_FSR0,p0->off);
	enl_load(__rhs->i&127,__rhs->i>>7);
	save_wreg(0);
}

void static
_enl_stry(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	stry_load(__rhs->sym,_FSR0,p1->off);
	load_wreg(0);
	casg(__lhs->i);
}


/*
	stry symbols dont follow strict confinement rules

	ie we can easy load in to any FSR we want and dont 
	need to copy around stuff.
*/
void static
_stry_stry(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	pushx16(FSR1);
	stry_load(__lhs->sym,_FSR0,p0->off);
	stry_load(__rhs->sym,_FSR1,p1->off);
	_16_u u;
	u = reduct(p0->v->u,p1->v->u);
	
	if (p0->v->u == 1) {
		move8(0,1);
	}else{
		move01(u);
	}
	popx16(FSR1);
}

void static
_stry_fs(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	stry_load(__lhs->sym,_FSR0,p0->off);
	stpos(__rhs->pm.off+p1->off);
	_16_u u;
	u = reduct(p0->v->u,p1->v->u);
	
	if (p0->v->u == 1) {
		move8(0,1);
	}else{
		move01(u);
	}

}

void static
_fs_stry(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	stry_load(__rhs->sym,_FSR0,p1->off);
	stpos(__lhs->pm.off+p0->off);
	_16_u u;
	u = reduct(p0->v->u,p1->v->u);
	if (p0->v->u == 1) {
		move8(1,0);
	}else{
		assert(1 == 0);
		move10(u);
	}
}

void static
stry_takeaddr(struct cfm_props *p) {
	struct pbs *pb;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movlw;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = p->y->sym->s;
	pb->bits[0] = IT_OAEM_accost_low;
	pb->dis[0] = p->off;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwi;
	pb->info[0] = _imm8;
	pb->opand[0] = 0;
	pb->bits[0] = IT_MODEN|IT_MOD_J;
	
	
	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movlw;
	pb->info[0] = _f_it_sym;
	pb->opand[0] = p->y->sym->s;
	pb->bits[0] = IT_OAEM_accost_high;
	pb->dis[0] = p->off;

	pb = pbs_new();
	pb->n = 1;
	pb->op = _pic_movwi;
	pb->info[0] = _imm8;
	pb->opand[0] = 1;
	pb->bits[0] = IT_MODEN|IT_MOD_J;	
}

void static
_fs_stry_addr(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	stpos(__lhs->pm.off+p0->off);

	stry_takeaddr(p1);
}
/*
	*a = PORTC;
*/
void static
_deref_enl(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_DEREF_ENL);
	load16(__lhs->pm.off);
	enl_load(__rhs->i&127,__rhs->i>>7);
	save_wreg(0);
}

/*
	a = 8bit
	*a = 8/16/32/64
*/
void static
_deref_cram(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	setdesc(dsc_DEREF_CRAM);
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	load16(__lhs->pm.off);
	__a->prop = p0;
	apply_off(__a);
	stpos(__rhs->pm.off+p1->off);
	move8(0,1);
}

/*
	*(a+10) = *(a+10)
*/
void static
_deref_deref_reg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	/*
		acum0 = LHS
		acum1 = RHS
	*/
	struct stash *acum0,*acum1;
	acum0 = many_alloc(GPR_ACCUM,1);
	accum = acum0;
	p0->n->f(p0->n);

	/*
		we assume are address is stored in are stashreg
	*/

	acum1 = many_alloc(GPR_ACCUM,1);
	accum = acum1;	
	p1->n->f(p1->n);

	pushx16(FSR1);
	many_retrieve(acum0,FSR0);
	many_retrieve(acum1,FSR1);

	_16_u u;
	u = reduct(p0->v_ptr->u,p1->v_ptr->u);
	
	if (p0->v_ptr->u == 1) {
		//move01 involves routines calls
		move8(FSR0,FSR1);
	}else {
		move01(u);
	}
	popx16(FSR1);
	many_free(acum0);
	many_free(acum1);
}

//here we make use of both FSR's
/*
	non 8b
*/
void
_deref_deref(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	_16_u u;
	u = reduct(p0->v_ptr->u,p1->v_ptr->u);

	load16(__rhs->pm.off);
	__a->prop = CFM_PROPS1(__a);
	apply_off(__a);

	/*
		most effect way is just to move around the FSR itsself and not the value contained at that address 
	*/
	pushx16(FSR0);
	load16(__lhs->pm.off);
	__a->prop = CFM_PROPS0(__a);
	apply_off(__a);

	popx16(FSR1);

	copy(u);
	unfurl(u, p0->v_ptr->u,0);
}

void static
_deref_reg_reg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0,*p1;
	p0 = CFM_PROPS0(__a);
	p1 = CFM_PROPS1(__a);

	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	p0->n->f(p0->n);

	p1->n->f(p1->n);
	save_wreg(TMP0);
	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);
	load_wreg(TMP0);
	save_wreg(0);
	many_free(acum);
}

/*
	*a = *a; <- both 8-bit both FS
	a[10] = a[10]

	here we push 8
*/
void static
_deref_deref0(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	
	load16(__rhs->pm.off);
	__a->prop = CFM_PROPS1(__a);
	apply_off(__a);

	/*
		move IND0 to wreg
	*/
	load_wreg(0);
	/*
		we use pushx not push why?
		because apply_off would be nested
		and push is limited to 8-bytes while 
		80-byte limit on pushx
	*/
	pushx();
	load16(__lhs->pm.off);
	__a->prop = CFM_PROPS0(__a);
	apply_off(__a);
	popx();

	struct pbs *pb;
	pb = pbs_new();
	pb->op = _pic_movwf;
	pb->info[0] = _imm8;
	pb->opand[0] = 0;
	pb->bits[0] = IT_OAEM_PPD;
	pb->n = 1;
}

void static
_enl_to_enl(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	enl_load(__rhs->i&127,__rhs->i>>7);
	casg(__lhs->i);
}

void static
_dereg_reg_enl(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0 = CFM_PROPS0(__a);
	struct cfm_props *p1 = CFM_PROPS1(__a);

	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	p0->n->f(p0->n);
	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);
	
	enl_load(__rhs->i&127,__rhs->i>>7);
	save_wreg(0);
	many_free(acum);
}

void deref_reg_fs(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0 = CFM_PROPS0(__a);
	struct cfm_props *p1 = CFM_PROPS1(__a);

	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	p0->n->f(p0->n);
	bankset(acum->store->bank);
	movft16(FSR0,acum->val[0]);
	
	stpos(__rhs->pm.off+p1->off);
	load_wreg(1);
	save_wreg(0);
	many_free(acum);
}

void deref_fs_reg(f_cfm_emp __lhs, f_cfm_emp __rhs, struct cfm_rake *__a) {
	struct cfm_props *p0 = CFM_PROPS0(__a);
	struct cfm_props *p1 = CFM_PROPS1(__a);
	p1->n->f(p1->n);
	push();
	load16(__lhs->pm.off);
	__a->prop = p0;
	apply_off(__a);
	pop();
	save_wreg(0);
}

void
_fsvar(f_cfm_emp __em, struct cfm_rake *__a) {
	setdesc(dsc_FSVAR);
	movft16(FSR0,FSR1);
	_stposs(__em->pm.off+__a->prop->off,_FSR0);
}


void static
wreg_node(f_cfm_emp __em, struct cfm_rake *__a) {
	__a->prop->n->f(__a->prop->n);
}

void static
bo_node(f_cfm_emp __em, struct cfm_rake *__a) {
	struct stash *acum;
	acum = many_alloc(GPR_ACCUM,1);
	accum = acum;
	__a->prop->n->f(__a->prop->n);
	retrieve_wreg(acum);
	many_free(acum);

}

void static func_fils(f_cfm_nodep __n) {
	f_cfm_blockp b;
	b = __n->block;
	_int_u mass = 0, parmass = 0;

	if (__n->d->n_args>0) {
		_int_u i;
		_int_s off = 0;
		i = 0;
		for(;i != __n->d->n_args;i++) {
			f_cfm_emp y;
			y = __n->d->args+i;
			assert(y->wide != 0);
			off-=y->wide;
			y->pm.off = off;
			parmass+=y->wide;
		}
	}

	if (b->n>0) {
		_int_u i;
		_int_s off = -parmass;
		i = 0;
		for(;i != b->n;i++) {
			f_cfm_emp y;
			y = b->y[i];
			assert(y->wide != 0);
			off-=y->wide;
			y->pm.off = off;
			mass+=y->wide;
		}
	}

	__n->d->size = parmass;	
	__n->d->mass = mass;
}

void cfm_pic_ret(f_cfm_flockp __f) {
	struct cfm_props *p = &__f->ret;
	if (p->v->u == 1) {
		loadin(__f->ret.y,&__f->ret);
	}else{
		bankset(4);
		if (p->vac == _cfm_ac_int) {
			loadimm(p->y->i,p->v->u,R2);
		}else
		if(p->vac == _cfm_ac_sym_fs) {
			stpos(p->y->pm.off+p->off);
			callthis(_mov2(p->v->u));
		}
	}
	push();
	func_ret();
	pop();
	struct pbs *pb;
	pb = pbs_new();
	pb->n = 0;
	pb->op = _pic_ret;
}

static fc_symp local_symb(char const *__name) {
	fc_symp s;
	s = fc_sym_new(__name,str_len(__name));
	s->local = 0;
	return s;
}

_64_u core_bits = 1;
void static crap(void) {
	_mov10(2) = local_symb("mov10w");
	_mov10(4) = local_symb("mov10d");
	_mov10(8) = local_symb("mov10q");
	
	_add0(2) = local_symb("add0w");
	_sub0(2) = local_symb("sub0w");
	_mov0(2) = local_symb("mov0w");
	_or0(2) = local_symb("or0w");
	_and0(2) = local_symb("and0w");
	_mul0(2) = local_symb("mul0w");

	_add0(4) = local_symb("add0d");
	_add0(8) = local_symb("add0q");

	_sub0(4) = local_symb("sub0d");
	_sub0(8) = local_symb("sub0q");
	
	_mov0(4) = local_symb("mov0d");
	
	_or0(4) = local_symb("or0d");
	_or0(8) = local_symb("or0q");
	
	_and0(4) = local_symb("and0d");
	_and0(8) = local_symb("and0q");

	_mov01(2) = local_symb("mov01w");
	_mov01(4) = local_symb("mov01d");
	_mov01(8) = local_symb("mov01q");
	
	_mov1(2)	=local_symb("mov1w");
	_mov2(2)	=local_symb("mov2w");
	_mov2(4)	=local_symb("mov2d");
	_mov2(8)	=local_symb("mov2q");

	_mov00(2)	= local_symb("mov00w");
	_add00(2)	= local_symb("add00w");
	_mov0(8) = local_symb("mov0q");

	_addfsr0 	= local_symb("addfsr0");
	_addfsr1 	= local_symb("addfsr1");
	_addfsri1	= local_symb("addfsri1");
	_addfsri0	= local_symb("addfsri0");

	_mov2i(2)	=local_symb("mov2iw");
	_mov2i(4)	=local_symb("mov2id");
	_mov2i(8)	=local_symb("mov2iq");

	_clr2q = local_symb("clr2q");
	_clr2d = local_symb("clr2d");
	_clr1q = local_symb("clr1q");
	_clr1d = local_symb("clr1d");
	_clr4q = local_symb("clr4q");
	
	_subfsr1w = local_symb("subfsr1w");
	_movfsr01 = local_symb("movfsr01");
	_movfsr10 = local_symb("movfsr10");
	_cmp0(2)	= local_symb("cmpw");
	_cmp0(4)	= local_symb("cmpd");
	_cmp0(8)	= local_symb("cmpq");
	_mov02(4)	= local_symb("mov02d");
	_mov02(8)	= local_symb("mov02q");


	_shlh(4) = local_symb("shld");
	_shlh(8) = local_symb("shlq");

	_shrh(4) = local_symb("shrhd");
	_shrh(8) = local_symb("shrhq");



	_shl(2) = local_symb("shlw");
	_shl(4) = local_symb("shld");
	_shl(8) = local_symb("shlq");

	_shr(2) = local_symb("shrw");
	_shr(4) = local_symb("shrd");
	_shr(8) = local_symb("shrq");
	
	void *ext[64];

	ext[0] = _mov01(2);
	ext[1] = _mov10(2);
	ext[2] = _add0(2);
	ext[3] = _sub0(2);
	ext[4] = _mov0(2);
	ext[5] = _or0(2);
	ext[6] = _and0(2);

	ext[7] = _add0(4);
	ext[8] = _sub0(4);
	ext[9] = _mov0(4);
	ext[10] = _or0(4);
	ext[11] = _and0(4);
	ext[12] = _mov01(4);
	ext[13] = _mul0(2);
	ext[14] = _mov1(2);

	ext[15] = _addfsr0;
	ext[16] = _addfsr1;
	ext[17] = _addfsri1;
	ext[18] = _addfsri0;
	ext[19] = _mov2(2);
	ext[20] = _mov00(2);
	ext[21] = _add00(2);
	ext[22] = _add0(8);
	ext[23] = _sub0(8);
	ext[24] = _or0(8);
	ext[25] = _and0(8);
	ext[26] = _mov0(8);
	ext[27] = _mov2(4);
	ext[28] = _mov2(8);
	ext[29] = _mov2i(2);
	ext[30] = _mov2i(4);
	ext[31] = _mov2i(8);
	ext[32] = _clr2q;
	ext[33] = _clr2d;
	ext[34] = _clr1q;
	ext[35] = _clr1d;
	ext[36] = _clr4q;

	ext[37] = _mov01(8);
	ext[38] = _mov10(4);
	ext[39] = _mov10(8);
	ext[40] = _subfsr1w;
	ext[41] = _movfsr01;
	ext[42] = _movfsr10;
	ext[43] = _cmp0(2);
	ext[44] = _cmp0(4);
	ext[45] = _cmp0(8);
	ext[46] = _mov02(4);
	ext[47] = _mov02(8);
	ext[48] = _shlh(4);
	ext[49] = _shlh(8);
	ext[50] = _shrh(4);
	ext[51] = _shrh(8);
	ext[52] = _shl(2);
	ext[53] = _shl(4);
	ext[54] = _shl(8);
	ext[55] = _shr(2);
	ext[56] = _shr(4);
	ext[57] = _shr(8);

	if (core_bits&1) {
		cfm_extern(ext,58);
	}


}
/*
	USAGE of this because of size diffrence 

	so lets say we have 
	global var
	local var
	ptr var
	thats 
	4 bits taken up so 16 combinations

	global var = local var

	etc.
	now sizes

	there are 8,16,32,64
	so now we are at 4 bits per side

	so 256 combinations

	see the issue that means high memory usage
	or a large function pointer table
*/
static struct cfm_nts nts_tab[] = {
	{//0
		.bits = {
			0x1e,
			0xee,
			0xf1
		},
		.f = {
			_deref_regfit,
			_assign0,
			_assign_deref
		},
		.loadin = {
			stput_deref,
			stput_deref,
			stput_deref,
		}
	},
	{//1
		.bits = {
			0xff
		},
		.f = {
			stput_intconst
		},
		.loadin = {
		
		}

	},
	{//2
		.bits = {
			0xff
		},
		.f = {
			_deref_intconst
		},
		.loadin = {
			stput_deref
		}
	},
	{//3
		.bits = {
			0xee,
			0x11
		},
		.f = {
			_deref_deref,
			_deref_deref0
		},
		.loadin = {
			stput_deref
		}
	},
	{//4
		.bits = {
			0xee,
			0x1f
		},
		.f = {
			__assign,
			stput
		},
		.loadin = {
		
		}
	},
	{//5
		.bits = {
			0xee,
			0x11
		},
		.f = {
			_assign1,
			_deref_cram
		},
		{
			stput_deref
		}
	},
	{//6
		.bits = {
			0xff
		},
		.f = {
			_deref_enl
		},
		{
			stput_deref
		}
	},
	{//7
		.bits = {
			0xff
		},
		.f = {
			stput
		},
		{
		
		}
	},
	{//8
		.bits = {
			0xff
		},
		.f = {
			_cassign_toreg
		},
		{
			stput_deref
		}
	},
	{//9
		.bits = {
			0x11,
			0xe1,
			0xee
		},
		.f = {
			stput,
			stput,
			stput_reg,
			stput_selreg
		},
		{
			wreg_node,
			bo_node
		}
	
	},
	{//10
		.bits = {
			0xff
		},
		.f = {
			_deref_assign_addrof
		},
		{
			_fsvar	
		}
	
	},
	{//11
		.bits = {
			0xff
		},
		.f = {
			_cassign0
		},
		{
			
		}
	
	},
	{//12
		.bits = {
			0xff
		},
		.f = {
			_cassign
		},
		{
			
		}
	
	},
	{//13
		.bits = {
			0xff
		},
		.f = {
			enl_putreg	
		},
		{
			
		}
	
	},
	{//14
		.bits = {
			0xff
		},
		.f = {
			_deref_intconst	
		},
		{
			_reg_deref
		}
	
	},
	{//15
		.bits = {
			0xff
		},
		.f = {
			_deref_deref_reg	
		},
		{
			_reg_deref
		}
	
	},
	{//16
		.bits = {
			0xff
		},
		.f = {
			_deref_assign_addrof
		},
		{
			_extract_addr
		}
	
	},
	{//17
		.bits = {
			0xff
		},
		.f = {
			_deref_assign_addrof_reg
		},
		{

		}
	
	},
	{//18
		.bits = {
			0xff
		},
		.f = {
			enl_derefreg
		},
		{

		}
	
	},
	{//19
		.bits = {
			0xff
		},
		.f = {
			_enl_to_enl
		},
		{

		}
	
	},
	{//20
		.bits = {
			0xff
		},
		.f = {
			_deref_reg_reg
		},
		{

		}
	
	},
	{//21
		.bits = {
			0xff
		},
		.f = {
			stput
		},
		{
			reg_deref
		}
	},
	{//22
		.bits = {
			0xff
		},
		.f = {
			_dereg_reg_enl
		},
		{

		}
	},
	{//23
		.bits = {
			0xff
		},
		.f = {
			deref_reg_fs
		},
		{

		}
	},
	{//24
		.bits = {
			0x11
		},
		.f = {
			deref_fs_reg
		},
		{

		}
	},
	{//25
		.bits = {
			0xff
		},
		.f = {
			_stry_int
		},
		{

		}
	},
	{//26
		.bits = {
			0xff
		},
		.f = {
			_stry_enl
		},
		{

		}
	},
	{//27
		.bits = {
			0xff
		},
		.f = {
			_stry_stry
		},
		{

		}
	},
	{//28
		.bits = {
			0xff
		},
		.f = {
			_stry_fs
		},
		{

		}
	},
	{//29
		.bits = {
			0xff
		},
		.f = {
			_fs_stry
		},
		{

		}
	},
	{//30
		.bits = {
			0xff
		},
		.f = {
			_enl_stry
		},
		{

		}
	},
	{//31
		.bits = {
			0xff
		},
		.f = {
			_fs_stry_addr
		},
		{

		}
	}


};
void static*ptrtab[16*32] = {

};

void static loadin_prox(f_cfm_emp __em, struct cfm_rake *__a) {
	loadin(__em,__a->prop);
}

void static dummy(void *__0, void *__1) {}
void cfm_pic_prep(void) {
	struct cfm_stuff *stf;
	stf = cfm_stf+1;
	stf->pawtab[CFM_PAW_FUNCC] = CFM_PAW_SELREG;
	mem_set(ptrtab, 0, sizeof(ptrtab));
	cfm_optab[CFM_DUB(_cfm_ac_sym_fs, _cfm_ac_sym_fs)] = ptrtab; 
	ptrtab[CFM_DUB0(0,0)] = nts_tab+4;
	ptrtab[CFM_DUB0(0,CFM_DEREF)] = nts_tab;
	ptrtab[CFM_DUB0(0,CFM_ADDROF)] = nts_tab+10;
	ptrtab[CFM_DUB0(CFM_DEREF,CFM_DEREF)] = nts_tab+3;
	ptrtab[CFM_DUB0(CFM_DEREF,0)] = nts_tab+5;
	ptrtab[CFM_DUB0(CFM_DEREF,CFM_ADDROF)] = nts_tab+16;


	cfm_optab[CFM_DUB(_cfm_ac_sym_fs, _cfm_ac_int)] = ptrtab+0x10;
	ptrtab[CFM_DUB0(0,0)+0x10] = nts_tab+1;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0x10] = nts_tab+2;

	cfm_optab[CFM_DUB(_cfm_ac_sym_fs, _cfm_ac_enl)] = ptrtab+0x20;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0x20] = nts_tab+6;
	ptrtab[CFM_DUB0(0,0)+0x20] = nts_tab+7;

	cfm_optab[CFM_DUB(_cfm_ac_enl, _cfm_ac_sym_fs)] = ptrtab+0x30;
	ptrtab[CFM_DUB0(0,CFM_DEREF)+0x30] = nts_tab+8;
	ptrtab[CFM_DUB0(0,0)+0x30] = nts_tab+12;

	cfm_optab[CFM_DUB(_cfm_ac_sym_fs, _cfm_ac_reg)] = ptrtab+0x40;
	ptrtab[CFM_DUB0(0,0)+0x40] = nts_tab+9;
	ptrtab[CFM_DUB0(0,CFM_DEREF)+0x40] = nts_tab+21;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0x40] = nts_tab+24;

	cfm_optab[CFM_DUB(_cfm_ac_enl, _cfm_ac_int)] = ptrtab+0x50;
	ptrtab[CFM_DUB0(0,0)+0x50] = nts_tab+11;

	cfm_optab[CFM_DUB(_cfm_ac_enl, _cfm_ac_reg)] = ptrtab+0x60;
	ptrtab[CFM_DUB0(0,0)+0x60] = nts_tab+13;
	ptrtab[CFM_DUB0(0,CFM_DEREF)+0x60] = nts_tab+18;

	cfm_optab[CFM_DUB(_cfm_ac_reg, _cfm_ac_int)] = ptrtab+0x70;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0x70] = nts_tab+14;

	cfm_optab[CFM_DUB(_cfm_ac_reg, _cfm_ac_reg)] = ptrtab+0x80;	
	ptrtab[CFM_DUB0(CFM_DEREF,CFM_DEREF)+0x80] = nts_tab+15;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0x80] = nts_tab+20;

	cfm_optab[CFM_DUB(_cfm_ac_reg, _cfm_ac_sym_fs)] = ptrtab+0x90;
	ptrtab[CFM_DUB0(CFM_DEREF,CFM_ADDROF)+0x90] = nts_tab+17;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0x90] = nts_tab+23;


	cfm_optab[CFM_DUB(_cfm_ac_enl,_cfm_ac_enl)] = ptrtab+0xa0;
	ptrtab[CFM_DUB0(0,0)+0xa0] = nts_tab+19;

	cfm_optab[CFM_DUB(_cfm_ac_reg,_cfm_ac_enl)] = ptrtab+0xb0;
	ptrtab[CFM_DUB0(CFM_DEREF,0)+0xb0] = nts_tab+22;

	cfm_optab[CFM_DUB(_cfm_ac_sym_stry,_cfm_ac_int)] = ptrtab+0xc0;
	ptrtab[CFM_DUB0(0,0)+0xc0] = nts_tab+25;
	
	cfm_optab[CFM_DUB(_cfm_ac_sym_stry,_cfm_ac_enl)] = ptrtab+0xd0;
	ptrtab[CFM_DUB0(0,0)+0xd0] = nts_tab+26;
	
	cfm_optab[CFM_DUB(_cfm_ac_sym_stry,_cfm_ac_sym_stry)] = ptrtab+0xe0;
	ptrtab[CFM_DUB0(0,0)+0xe0] = nts_tab+27;
	
	cfm_optab[CFM_DUB(_cfm_ac_sym_stry,_cfm_ac_sym_fs)] = ptrtab+0xf0;
	ptrtab[CFM_DUB0(0,0)+0xf0] = nts_tab+28;
	
	cfm_optab[CFM_DUB(_cfm_ac_sym_fs,_cfm_ac_sym_stry)] = ptrtab+0xf1;
	ptrtab[CFM_DUB0(0,0)+0xf1] = nts_tab+29;
	ptrtab[CFM_DUB0(0,CFM_ADDROF)+0xf1] = nts_tab+31;

	cfm_optab[CFM_DUB(_cfm_ac_enl,_cfm_ac_sym_stry)] = ptrtab+0xf2;
	ptrtab[CFM_DUB0(0,0)+0xf2] = nts_tab+30;
	
	struct f_cfm_node n;
	assert(&n.rake == &n);


	struct cfm_lds *l;
	l = &cfm_pic_sp[CFM_DUB(_cfm_ac_sym_fs,CFM_DEREF)];
	l->f = _towreg0;
	l->loadin = stput_deref;

	l = &cfm_pic_sp[CFM_DUB(_cfm_ac_sym_fs,0)];
	l->f = loadin_prox;

	l = &cfm_pic_sp[CFM_DUB(_cfm_ac_int,0)];
	l->f = loadin_prox;

	l = &cfm_pic_sp[CFM_DUB(_cfm_ac_reg,0)];
	l->f = dummy;


}
#include "../../env.h"
static struct it_yolk_h *last_h = NULL;
void static
yk_prep(it_yolkp __yk) {
	char const *s;
	s = envget("CFM_DEBUG");

	if (s != NULL) {
		if (!str_cmp(s,"on")) {
			core_bits &= ~1;
		}
	}
	printf("yolk prep.\n");
	if (last_h != NULL) {
		last_h->brk_tail = _brk;
		_brk->next = NULL;
	}
	struct it_yolk_h *h;
	h = __yk;

	h->brk_head = m_alloc(sizeof(struct pbs_break));
	h->brk_head->pbs_head = NULL;
	h->brk_head->pbs_tail = NULL;
	_brk = h->brk_head;
	last_h = h;
}

void cfm_pic(f_cfm_trackp __trk, struct it_com *cm) {
	_int_u i;
	i = 0;
	for(;i != cfm_struc.nff;i++) {
		func_fils(cfm_struc.func_fils[i]);
	}

	cfm_struc.yk_prep = yk_prep;
	*(struct it_yolk_h*)cfm_struc.it_ykh = (struct it_yolk_h){NULL,NULL};
	trk_cur = cfm_it_track_new();
	cfm_it_superyolk_new();
	cfm_it_yolk_new();

	crap();

	cfm_outfck(__trk);
	last_h->brk_tail = _brk;
	_brk->next = NULL;
	/*
	fill in goto/s
	*/
	goto_out();

	cfm_it_yolk_hp cy;
	cfm_it_trkp trk;
	trk = trk_top;
	struct it_cf *cf;
	while(trk != NULL) {
		cy = trk->yk_top;
		it_yolk_hp h;
		while(cy != NULL) {
			h = (it_yolk_hp)cy;
			cy->yk->cf = cfm_struc.pt_cf.off;
			struct pbs_break *b;
			b = h->brk_head;
			while(b != NULL) {
				printf("-----> BREAK.\n");
				struct pbs *pbs_cur = b->pbs_head;
				while(pbs_cur != NULL) {
					cf = cfm_it_cf_new();
					cf->op = pbs_cur->op;
					cf->n_opds = pbs_cur->n;
					cf->info = _f_it_none;
					cf->desc = pbs_cur->desc;
					cf->dlen = pbs_cur->dlen;
					cf->ndsc = pbs_cur->ndsc; 
					printf("-----> CF: OP: %u.\n", cf->op);
					_int_u i;
					i = 0;
					for(;i != pbs_cur->n;i++) {
						cf->operands[i].ns = 1;
						cf->operands[i].oaem[0].oa = pbs_cur->opand[i];
						cf->operands[i].oaem[0].oa0 = pbs_cur->opand0[i];
						cf->operands[i].oaem[0].info = pbs_cur->info[i];
						cf->operands[i].oaem[0].bits = pbs_cur->bits[i];
						cf->operands[i].oaem[0].dis = pbs_cur->dis[i];
						cf->operands[i].oaem[0].x = pbs_cur->x[i];
					}
				_sk:
					pbs_cur = pbs_cur->next;
				}
				b = b->next;
			}
			cy->yk->n_cf = cfm_struc.pt_cf.off-cy->yk->cf;
			cy = cy->link;
		}
		trk = trk->next;
	}

	static struct it_wing w;
	w.trk = &trk_cur->trk;
	w.trk->sy = trk_cur->sbuf;
	w.trk->n_yk = trk_cur->sy-trk_cur->sbuf;
	w.trk->p = trk_cur->pt_yolks.p;
	w.id = _f_it_ins;

	cm->w = &w;
	cm->n_wgs = 1;
}
