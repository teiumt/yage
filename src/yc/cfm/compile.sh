rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd cfm
ffly_objs="$ffly_objs shs.o cfm.o sym.o it.o it_sym.o it_amd64.o"
gcc -c $cc_flags cfm.c
gcc -c $cc_flags sym.c
gcc $cc_flags -c ../shs.c
gcc $cc_flags -c ../it/it.c -o it.o
gcc $cc_flags -c ../it/sym.c -o it_sym.o
gcc $cc_flags -c ../it/output/amd64.c -o it_amd64.o

gcc $cc_flags -o cfm main.c $ffly_objs -nostdlib
