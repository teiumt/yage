# include "frag.h"
# include "../yc.h"
# include "../../m_alloc.h"
# include "../../io.h"
# include "../../string.h"
it_fragp it_fr_head = NULL;
it_fragp it_cur_frag = NULL;
#define PAGE_SHFT 3
#define PAGE_SIZE (1<<PAGE_SHFT)
#define PAGE_MASK (PAGE_SIZE-1)
it_fragp it_frag_new(void) {
	it_fragp f;

	f = (it_fragp)m_alloc(sizeof(struct it_frag));

	f->p = (char**)m_alloc(sizeof(char*));
	*f->p = (char*)m_alloc(PAGE_SIZE);
	f->page_c = 1;
	f->m = it_mound_new();
	f->off = 0;
	if (!it_fr_head)
		it_fr_head = f;
	if (it_cur_frag != NULL)
		it_cur_frag->next = f;
	it_cur_frag = f;
	f->next = NULL;
	return f;
}

void it_frag_lay(it_fragp __f, void *__buf, _int_u __size) {
	it_mound_pack(__f->m, __buf, __size);
}

void it_frag_write(it_fragp __f, void *__buf, _int_u __size, _64_u __off) {
	printf("writing to: %u.\n", __off);
	_int_u pg_c;

	pg_c = ((__size+__off)+(PAGE_SIZE-1))>>PAGE_SHFT;

	if (pg_c>__f->page_c) {
		_int_u pg;
		pg = __f->page_c;
		__f->page_c = pg_c;
		__f->p = (char**)m_realloc(__f->p, __f->page_c*sizeof(char*));
		while(pg != pg_c) {
			mem_set(*(__f->p+pg++) = (char*)m_alloc(PAGE_SIZE), '#', PAGE_SIZE);
		}
		printf("new pages %u, %u\n", pg_c, __f->off);
	}

	char *p;
	p = (void*)__buf;
	_int_u pg, pg_off;
	pg = __off>>PAGE_SHFT;
	pg_off = __off&PAGE_MASK;
	_int_u e, npg;
	npg = __size>>PAGE_SHFT;
	e = (__off+__size)>>PAGE_SHFT;
	_int_u fill;
	if (pg_off>0) {
		if (__size>=(fill = (PAGE_SIZE-pg_off))) {
			mem_cpy((*(__f->p+pg))+pg_off, p, fill);
			p+=fill;
			pg++;
			pg_off = 0;
		} else {
			mem_cpy((*(__f->p+pg))+pg_off, p, __size);
			return;
		}
	}

	while(pg<e) {
		mem_cpy(*(__f->p+pg), p, PAGE_SIZE);
		pg++;
		p+=PAGE_SIZE;
	}

	_int_u left;
	left = __size-(p-(char*)__buf);
	if (left>0) {
		mem_cpy(*(__f->p+pg), p, left);
	}

}

void it_frag_read(it_fragp __f, void *__buf, _int_u __size, _64_u __off) {
	char *p;
	p = (void*)__buf;
	_int_u pg, pg_off;
	pg = __off>>PAGE_SHFT;
	pg_off = __off&PAGE_MASK;
	_int_u e, npg;
	npg = __size>>PAGE_SHFT;
	e = (__off+__size)>>PAGE_SHFT;
	_int_u fill;
	if (pg_off>0) {
		if (__size>=(fill = (PAGE_SIZE-pg_off))) {
			mem_cpy(p, (*(__f->p+pg))+pg_off, fill);
			p+=fill;
			pg++;
			pg_off = 0;
		} else {
			mem_cpy(p, (*(__f->p+pg))+pg_off, fill);
			return;
		}

	}

	while(pg<e) {
		mem_cpy(p, *(__f->p+pg), PAGE_SIZE);
		pg++;
		p+=PAGE_SIZE;
	}

	_int_u left;
	left = __size-(p-(char*)__buf);
	if (left>0) {
		mem_cpy(p, *(__f->p+pg), left);
	}
}

void it_frag_induct(char *__buf, _int_u __len) {
	it_frag_write(it_cur_frag, __buf, __len, it_cur_frag->off);
	it_cur_frag->off+=__len;
}

void it_frag_destroy(it_fragp __f) {
}

// output
void it_fragout(void) {
	it_fragp f;
	f = it_fr_head;
	while(f != NULL) {
		it_moundout(f->m);
		if (f->off>0) {
			printf("offset: %u.\n", f->off);
			char **page;
			page = f->p;
			char buf[2048];
			char **e;
			_int_u t_pg;
			e = page+(t_pg = (f->off>>PAGE_SHFT));
			printf("whole pages: %u.\n", t_pg);
			while(page != e) {
				mem_cpy(buf, *page, PAGE_SIZE);
				buf[PAGE_SIZE] = '\0';
				printf("page{%s}\n", buf);
				_yc_write(fc_struc.out.arg, *page, PAGE_SIZE);
				page++;
			}

			_int_u left;
			left = f->off-(t_pg<<PAGE_SHFT);
			if (left>0) {
				printf("left: %u.\n", left);
				if (page<(f->p+f->page_c)) {
					mem_cpy(buf, *page, left);
					buf[left] = '\0';
					printf("page{%s}\n", buf);
					_yc_write(fc_struc.out.arg, *page, left);
				} else {
					printf("i dont know why this is happening but it shouldent.\n");			
				}
			}
		}
		f = f->next;
	}
}
