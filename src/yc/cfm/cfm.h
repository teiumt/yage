#ifndef __f__cfm__cfm__h
#define __f__cfm__cfm__h
#include "../../y_int.h"
#include "../../types.h"
/*
	NOTE:
		if we have an assignment operation example: X = Z;
		then following Y = Z. clang might not know that z MIGHT be stored in a register 
		so

		REG = Z;
		X = REG;
		Y = REG;

		issue?
		on the first issue X = Z the function pointer for the next one along will have to change?

		because 
		assign_var_REG
		and not assign_var_var?

		NOTE:
		cant be done at clang level due to interference like using of said register EXAMPLE: pic only was a single register for usage 

	NOTE:
		register selection are cfm specific and only known by cfm
		register selection cant be forced for things like arth,+,-,*,etc
		among other things

	FUNCTION-ROUTINES

	W(structs)
	var = ptr array
	params = linear array 
*/
// short for conforming = cfm
enum {
	_f_cfm_func_def,
/*
	EIMT: mov instruction
*/
/*
	single assign
*/
	_f_cfm_assign,
	_f_cfm_setreg,
	_f_cfm_bo0,
	_f_cfm_cpyreg,
	_f_cfm_ioregreg,
	_f_cfm_func_call,
	_f_cfm_deref,
	_f_cfm_goto,
	_f_cfm_if,
	_cfm_while,
	_f_cfm_for,
	_f_cfm_idc,
	_f_cfm_cassign,
	_cfm_bittests,
	_cfm_bo,
//*a = *a;
	_cfm_deref_deref,
	_cfm_assign_addrof,
	_cfm_assign_deref,	
	_cfm_cassign_toreg,
	_cfm_agrer,
	_cfm_eqo,
	_cfm_eqoand,
	_cfm_func_call_arg_func_call,
	_cfm_add_off,
	_cfm_add_off_with
};
#define _cfm_val		0
#define _cfm_deref		1
#define _cfm_deref0      4
#define _cfm_addrof		2
#define _cfm_reg		3
#define _cfm_assign0	5
#define _cfm_fsvar		6
#define _cfm_cassign0	7
//toreg	8
#define _cfm_assign1	9
#define _cfm_deref_regfit	10
#define _cfm_deref_reg		11
#define _cfm_intconst		12
#define _cfm_enl			13
#define _f_cfm_add		(1<<3)
#define _f_cfm_sub		((1<<4)|1)
#define _cfm_and		((1<<5)|2)
#define _cfm_or			((1<<6)|3)
#define _cfm_shl		((1<<7)|4)
#define _cfm_shr		((1<<8)|5)
#define _cfm_mul		((1<<9)|6)
#define _cfm_div		((1<<10)|7)
#define _cfm_som		(0xff<<3)
enum {
	_f_cfm_eqt,
	_f_cfm_net,
	_cfm_gt,
	_cfm_lt
};

#define F_CFM_COND 0x00
#define CFM_SY_FUNC 0x04
#define CFM_FRAMESPEC 0x02
#define CFM_SY_NOT 0x01
#define CFM_SY_IMM 0x08
typedef struct f_cfm_em *f_cfm_emp;
typedef struct f_cfm_sym *f_cfm_symp;
/*
	rename-

	what?
	say we have a function we have two choices in defining the sub symbols

	we parce a sub command in e.g.
	decl x
	decl y
	or
	decl x,y, 2?

	or 
	block {
		x{int}, y{int}
	}

	func {start, nn, sym, block} <- args to function
*/
enum {
	_f_cfm_int_8,
	_f_cfm_int_16,
	_f_cfm_int_32,
	_f_cfm_int_64
};

#define _f_cfm_sign 1
#define _f_cfm_tsm (0xf<<1)
#define _f_cfm_b	_f_cfm_int_8
#define _f_cfm_w	(_f_cfm_int_16<<1)
#define _f_cfm_d	(_f_cfm_int_32<<1)
#define _f_cfm_q	(_f_cfm_int_64<<1)
#define _f_cfm_int8		_f_cfm_int_8
#define _f_cfm_int16	(_f_cfm_int_16<<1)
#define _f_cfm_int32	(_f_cfm_int_32<<1)
#define _f_cfm_int64	(_f_cfm_int_64<<1)
/*
	type specs/var specs?
*/
typedef struct cfm_vsp {
	_8_u info;

	//rename to wideness?
	_int_u u,_u;
} *f_cfm_vspp;

struct cfm_placement {
	_64_s off;
};

typedef struct f_cfm_block {
	f_cfm_emp *y;
	_int_u n;
} *f_cfm_blockp;

/*
	note not register in what you think they are TOBE renamed? <- think of pointers
*/

// moving registers
struct f_cfm_em;
extern struct f_cfm_em *f_cfm_regm;
typedef struct f_cfm_superflock *f_cfm_superflockp;
typedef struct f_cfm_flock *f_cfm_flockp;

/*
	place for situation
*/
typedef struct f_cfm_node *f_cfm_nodep;
/*
	node working structure

	so:
		a node is more or less just a trigger passing info to do shit,
		dwel is contained within the node to surve as a catalist for connecten between nodes without 
		directly connecting them
*/
// function related crap
/*
	stack,par,args interlinkage
*/
typedef struct f_cfm_dwel {
	_int_u pad, size, mass;
	f_cfm_nodep n;
	_int_u sp;
	f_cfm_emp args;
	_int_u n_args;

	_64_u u;
} *f_cfm_dwelp;

typedef struct f_cfm_track {
	f_cfm_superflockp sf;
	_int_u n;
} *f_cfm_trackp;
//nothing to say

struct cfm_nts {
	_8_u bits[4];
	void(*f[4])(f_cfm_nodep);
	void(*loadin[4])(void*,void*);
};
#define CFM_PAW_FUNCC 0
/*
	selective register
*/
#define CFM_PAW_SELREG 1

//left hand side
#define CFM_PROPS0(__rk)((struct cfm_props*)(__rk))
//right hand side
#define CFM_PROPS1(__rk)((struct cfm_props*)(((_64_u*)(__rk))+1))
struct cfm_props {
	_64_s off,						_0;
	_64_u bits,						_1;
	struct f_cfm_em *nc_off,		*_2;
	_64_u nc_off_ac,				_3;
	struct cfm_vsp *nc_off_v,		*_4;
	struct f_cfm_node *nc_off_n,	*_5;
	_64_u vac,						_6;
	struct cfm_vsp *v,				*_7;
	_64_u vac_ptr,					_8;
	struct cfm_vsp *v_ptr,			*_9;
	struct f_cfm_node *n,			*_10;
	struct f_cfm_em *y,				*_11;
	_64_u paw,_12;
};

struct cfm_rake {
	struct cfm_props props;
	struct cfm_props *prop;
	/*
		int-consts

		a[a+21+(1<<2)] = a[a+(4+21)] = a[a+25];

		to avoid adding 

		ie 
			24+a = REG

		as we want
			PTR
			PTR+=24
			PTR+=a
			PTR+=REG0
			PTR+=REG1
			try avoiding using REG
	*/
	/*
		stack specific
	*/

	_int_u size;
	void(*loadin)(void*,void*);
	struct f_cfm_node *with;
	_64_u fsr;
};

#define F_CFM_PAGE_SHFT 4
#define F_CFM_PAGE_SIZE (1<<F_CFM_PAGE_SHFT)
#define F_CFM_PAGE_MASK (F_CFM_PAGE_SIZE-1)

enum {
	_f_cfm_po_not,
	_f_cfm_po_var,
	_f_cfm_po_jmp,
	_f_cfm_po_ll,
	_f_cfm_po_aso,
	_f_cfm_po_jt,
	_f_cfm_po_def,
	_cfm_sp
};
//prepoperations
typedef struct f_cfm_fpo {
	_8_u op;
	f_cfm_symp sy;
	struct fc_opi pi;
	char *n;
	_int_u nlen;
	char *v;
	_int_u vlen;
	struct f_cfm_fpo *link;
} *f_cfm_fpop;

typedef struct f_cfm_flock *f_cfm_flockp;
typedef struct f_cfm_superflock {
	f_cfm_flockp fck;
	_int_u n;
} *f_cfm_superflockp;
struct cfm_here {
	f_cfm_symp jt;
};



typedef struct f_cfm_flock {
	struct f_cfm_fpo *po;
	/*
		pages of nodes non pointer memory direct
	*/
	struct f_cfm_node **n;
	_int_u pg_off;
	_int_u nn;
	struct cfm_here here;
	struct cfm_props ret;
	void(*end)(struct f_cfm_flock*);
} *f_cfm_flockp;

typedef struct f_cfm_renb {
	void *priv;
	struct f_cfm_eby *reg;
} *f_cfm_renbp;

// absolute classifier
// absolute classafacation
// symbol stationary(e.g. void test(){})
/*
	fixed symbol e.g. function,label
*/
#define _cfm_acb_sym_stry		(1<<8)
#define _cfm_acb_sym_fs			(1<<9)
#define _cfm_acb_int			(1<<10)
#define _cfm_acb_reg			(1<<11)
#define _cfm_acb_enl			(1<<16)
#define _cfm_acb_reg0			(1<<17)



#define _cfm_ac_sym_stry		(1|(1<<8))
// symbol frame specific(e.g. dis(%rbp))
/*
	a symbol thats address is govend by the frame that it resides in
	ie function routine.
*/
#define _cfm_ac_sym_fs			(2|(1<<9))
// interger constant(e.g. $0)
#define _cfm_ac_int				(3|(1<<10))

#define _cfm_ac_reg				(4|(1<<11))

#define _cfm_ac_enl				(5|(1<<16))
#define _cfm_ac_reg0			(6|(1<<17))

#define CFM_DUB(__x,__y)(((__x)|((__y)<<4))&0xff)
#define CFM_DUB0(__x,__y)((__x)|((__y)<<2))
/*
	NOTE:

	int x[10];
	int *y;

	x[10] = 0; is not the same as y[10] = 0;


	x[10] = 0;	'CFM_DEREF is not present
	y[10] = 0;	'CFM_DEREF is set

	CFM_DEREF = "does the thing we where given FS or reg need to be derefed?"
*/
#define CFM_DEREF	1
#define CFM_ADDROF	2
#define _f_cfm_ag_sign		(1<<5)
#define _f_cfm_ag_pos		(1<<6)
//enlighten me
/*
	short for enigmatic why?
	because is cant find a name for it 
	so it will be left as an unknown
*/
typedef struct f_cfm_em *f_cfm_emp;
#define CFM_EMB_INJECT 0x01
#define CFM_PTR			0x02
/*
	in structure terms

	pm is attached to the structure placement
	and node const offset is used.

	core struct for all infomation
	const,int,float,stack var, etc
*/

typedef struct f_cfm_em {
	union {
		/*
			stack placement
		*/
		struct cfm_placement pm;	
		f_cfm_symp sym;
		
		_64_u i;
		void *ioreg;
		/*
			a = aso "PORTC";
			= movlw PORTC
			etc...
			slips
		*/
		struct {
			char *s;
			_int_u len;
		}str;
	};
	void *stash;
	_8_s saved;
	_8_u bits;
	_64_u wide;
} *f_cfm_emp;

typedef struct f_cfm_node {
	struct cfm_rake rake;

	void(*f)(f_cfm_nodep);

	void(*f0)(f_cfm_nodep);
	struct f_cfm_node *trickle[8];
	_int_u nt;

	struct f_cfm_em *dst;
	_64_u v0_ac;
	struct cfm_vsp *v0;
	_16_u op;

	_8_u a;
	void(*load[7])(void*,void*);
	void(*load0[7])(void*,void*);

	struct f_cfm_node *params[7];
	struct f_cfm_em *gub;
	_int_u n_par;
	f_cfm_symp sym;
	f_cfm_blockp block;
	struct f_cfm_track *trk, *_else;
	struct f_cfm_dwel *n, *func;
	f_cfm_dwelp d;
	struct cfm_vsp vs;
	_64_u _0;
	_8_u od;
	struct f_cfm_em y;
} *f_cfm_nodep;


typedef struct f_cfm_sym {
	void *sp;
	fc_symp s;
	_8_u flags;
	//only to avoid having to allocate a new one
	struct f_cfm_em y;
	struct f_cfm_sym *next;
	_64_u val;
} *f_cfm_symp;


// pan table
struct cfm_pant {
	struct it_pan **p;
	_int_u pgc;
	_int_u off;
	_int_u pans;
	_int_u pgsize;
};

struct cfm_pbs {

};
struct f_cfm_em cfm_eminit;
typedef struct cfm_it_yolk_h {
	_8_u userdata[64];
	it_yolkp yk;
	struct cfm_it_yolk_h *link;
} *cfm_it_yolk_hp;

typedef struct cfm_it_track {
    struct it_track trk;
    struct it_superyolk sbuf[128];
    struct it_superyolk *sy;
    cfm_it_yolk_hp yk_top;
    struct cfm_it_track *next;
/*
	pantry :|
*/
	struct cfm_pant pt_yolks;
} *cfm_it_trkp;

struct f_cfm {
	struct fc_st *st;
	f_cfm_symp sytop;
	f_cfm_symp jt;
	f_cfm_dwelp dw;
	cfm_it_trkp trk_top;
	cfm_it_trkp trk_cur;

	struct cfm_here *here;
	struct cfm_pant pt_cf;

	_8_u it_ykh[64];
	void **func_fils;
	_int_u nff;
	void(*yk_prep)(it_yolkp);
};

struct cfm_lds {
	void(*f)(void*,void*);
	void(*loadin)(void*,void*);
};

struct cfm_stuff {
	_8_u atab[64];
	_64_u pawtab[64];
	void(*ret)(f_cfm_flockp);
	void(**func)(f_cfm_nodep);
	struct cfm_lds *sp;
	void*(*f)(f_cfm_nodep,void*,void*,_64_u,_64_u);
};
extern struct cfm_stuff cfm_stf[2];
void* cfm_savepoint(void);
void f_cfm_reg_alloc(f_cfm_renbp*, _int_u);
void f_cfm_reg_free(f_cfm_renbp*, _int_u);
struct cfm_stage;
void f_cfm_init(struct cfm_stage*);
struct f_cfm_em *f_cfm_em_new(void);

f_cfm_symp f_cfm_sym_new(fc_symp);
//cfm.c
it_yolkp cfm_it_yolk_new(void);
it_superyolkp cfm_it_superyolk_new(void);

//cfm_amd64.c
struct it_cf*
cfm_it_cf_new(void);
struct it_cf* cfm_it_cf_new(void);
cfm_it_trkp cfm_it_track_new(void);

//cfm_pic.c
void cfm_pic(f_cfm_trackp,struct it_com*);


void cfm_amd64(f_cfm_trackp,struct it_com*);
struct cfm_stage {
	void(*prep)(void);
	void(*func)(f_cfm_trackp,struct it_com*);	
};
extern void(*cfm_proc)(f_cfm_nodep, _int_u);
void cfm_outfck(f_cfm_trackp);

it_ypop cfm_it_syo(it_superyolkp);
it_ypop cfm_it_ypo(it_yolkp);
extern struct cfm_nts **cfm_optab[0x1000];
void cfm_init(f_cfm_trackp,struct cfm_stage*);

extern void(*cfm_extern)(void**,_int_u);
extern void(*cfm_global)(void**,_int_u);
extern void(*cfm_var)(void**,_int_u);
//extern void(*cfm_optab[0x1000])(void*,void*);
extern struct cfm_stage cfm_st[4];
void cfm_process(void);
#endif /*__f__cfm__cfm__h*/
