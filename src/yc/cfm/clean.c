# include "../yc.h"
# include "../../m_alloc.h"
/*
	cleaning routines
	for user to set
*/
void(*cr_tbl[24])(void*, void*);

it_cleanp f_it_clean(_8_u __id, void *__nes, void *__ptr) {
	it_cleanp p;
	p = (it_cleanp)m_alloc(sizeof(struct it_clean));
	p->ptr = __ptr;
	p->func = cr_tbl+__id;
	p->nes = __nes;
	return p;
}

static void *ptr[128];
static void **n_ptr = ptr;
void f_it_pclean(void *__ptr) {
	*(n_ptr++) = __ptr;
}
