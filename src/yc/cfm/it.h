# ifndef __f__it__it__h
# define __f__it__it__h
# include "../../y_int.h"
#define _f_it_ins 0

#define _f_it_mov 0x00
#define _f_it_jmp 0x01
#define _f_it_add 0x02
#define _f_it_sub 0x03
#define _f_it_call 0x04
#define _f_it_ret 0x05
#define _f_it_and 0x06
#define _f_it_lea 0x07
#define _f_it_cmp 0x08
#define _f_it_jne	(0x08+1)
#define _f_it_je	(0x08+2)
#define _f_it_xor	(0x08+3)
#define _f_it_movzx (0x08+4)
#define _f_it_b 0
#define _f_it_w 1
#define _f_it_d 2
#define _f_it_q 3
#define _f_it_bw (0|8)
#define _f_it_bd (2|8)
#define _f_it_bq (4|8)
#define _f_it_wd (6|8)
#define _f_it_none 0xf

#define _f_it_int (_f_it_8|_f_it_16|_f_it_32|_f_it_64)
#define _f_it_dis (_f_it_dis8|_f_it_dis16|_f_it_dis32|_f_it_dis64)
#define _f_it_s 1//is signed
#define _f_it_8 2
#define _f_it_16 4
#define _f_it_32 8
#define _f_it_64 16
#define _f_it_sym 32
#define _f_it_reg 64
#define _f_it_dis8 128
#define _f_it_dis16 256
#define _f_it_dis32 512
#define _f_it_dis64 1024
#define _f_it_abr 2048
#define _f_it_arth 4096
#define _f_it_p 8192
#define _it_slip 0x4000
#define IT_SY_TYPE 0x03

enum {
	_f_it_sy_label,
	_f_it_sy_labelvar
};
#define getsize(__x) ((__x)&0x0f)
#define operandid(__x) ((__x)&0xff)

#define operand_iset(__x, __n) ((__x&0xff)<<(__n*8))
#define operand_iget(__x, __n) ((__x>>(__n*8))&0xff)

#define F_IT_PAGE_SHFT 4
#define F_IT_PAGE_SIZE (1<<F_IT_PAGE_SHFT)
#define F_IT_PAGE_MASK (F_IT_PAGE_SIZE-1)
#define IT_POS ((_64_u)1<<63)
struct it_cf;
#define IT_PANPAGE (IT_PAN_SHFT+F_IT_PAGE_SHFT)
#define IT_PANPAGESZ (1<<IT_PANPAGE)
#define IT_PANPAGEMK (IT_PANPAGESZ-1)

#define IT_PAN_SHFT 6 
#define IT_PAN_SIZE (1<<IT_PAN_SHFT)
#define IT_PAN_MASK (IT_PAN_SIZE-1)
struct it_pan {
	void *ptrs[IT_PAN_SIZE];
};


/*
	i know this name is stuped but i cant be fucking 
	dealing with naming shit.
*/
enum {
	_it_yk_not,
	_it_yk_ss,
	_it_yk_var,
	_it_yk_ll,
	_it_yk_aso
};

typedef struct it_ypo {
	_8_u op;
	fc_symp sy;
	fc_opip pi;
	struct it_ypo *link;
} *it_ypop;
#define IT_POP_STACK 0x01
#define IT_POP_BRKLOC 0x01
/*
	TODO:
	 new struct called frame????
	so to contain stuff for fuction setup like var mass ie 'space to allocate on stack'
	 first what is a yolk?

	 its like a fragment
	 so if a new function is created a new yolk will be created to contain 
	 the function contents
*/
typedef struct it_yolk *it_yolkp;
typedef struct it_superyolk {
	_64_u yk;
	_int_u n;
	it_ypop po;
	_8_u pop;
	_int_u space;
	_64_u _0;
} *it_superyolkp;

typedef struct it_yolk {
	it_ypop po;
	_8_u pop;
	_ulonglong _0;
	_ulonglong _1;
	_int_u n_cf;
	_int_u n_opds;
	_64_u cf;
} *it_yolkp;

struct it_track {
	struct it_pan **p;
	_int_u n_yk;
	struct it_superyolk *sy;
	_64_u yk;
};

struct it_wing {
	_8_u id;
	
	struct it_track *trk;
};

typedef struct it_com {
	struct it_wing *w;
	_int_u n_wgs;

} *it_comp;

typedef struct it_clean {
	void *ptr, *nes;
	void(*func)(void*, void*);
} *it_cleanp;

typedef struct it_mblock {
	struct it_mblock *next;
} *it_mblockp;

it_cleanp f_it_clean(_8_u, void*, void*);
void f_it_pclean(void*);
void *it_malloc(_int_u);
extern struct it_mblock *it_mb;
// postpend seperator
#define IT_OAEM_PPS 0x01
// prepend dollar
#define IT_OAEM_PPD 0x04
#define IT_OAEM_ANT 0x02
#define IT_OAEM_accost_low (1<<8)
#define IT_OAEM_accost_high (1<<9)

#define IT_MOD_J (0<<4)
#define IT_MOD_K (1<<4)
#define IT_MODEN (1<<3)
#define _IT_MODS(__bits) ((__bits>>4)&0xf)
struct it_oa_em {
	_64_u oa;
	_64_u oa0;
	_64_u info;
	_64_u bits;
	_8_u cb;
	_8_u x;
	_32_s dis;
};

struct it_oa {
	_int_u ns;
	struct it_oa_em oaem[7];
};

// compound frame
struct it_cf {
	_8_u op;

	_8_u n_opds;

	/*
		no negative signed number should be placed here,

		if we have a negative number say -22 we should 
		place 22 posative and say that its to be writen 
		as a negative. this is done by setting the end bit to zero
	*/

	struct it_oa operands[7];
	_8_u info;

	//descriptive infomation
	// ie comments
	char const **desc;
	_int_u *dlen;
	_int_u ndsc;
};

struct it_outstruc {
	char const *codename, *_for;
	void(*prep)(void);
	void(*process)(it_comp);
	void(*out)(void);
	char const *desc;
};

#define F_IT_N 4

struct f_it {
	struct it_outstruc out[F_IT_N];
};

extern struct f_it it_struc;

#define IT_GAS_AMD64 (it_struc.out+0)
#define IT_FBD_AMD64 (it_struc.out+1)
#define IT_TAS_PIC (it_struc.out+2)

void it_process(it_comp, struct it_outstruc*);
# endif /*__f__it__it__h*/
