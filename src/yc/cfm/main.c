# include "../y_int.h"
# include "../types.h"
# include "cfm.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../linux/stat.h"
# include "../mort/stdio.h"
# include "../mort/malloc.h"
# include "../mort/string.h"

static struct f_cfm_node nodes[] = {
	{.kind=_f_cfm_func_def, .start=1, .n_space=1},
	{.kind=_f_cfm_assign}
};

void static _write(void *__buf, _int_u __len) {
}
static struct f_cfm_fcst fc = {
	.write = _write
};

_f_err_t ffmain(int __argc, char const *__argv[]) {
	struct f_cfm cfm;
	cfm.fc = &fc;
	f_cfm_symp s;
	s = f_cfm_sym_new(f_shs_sym_new("test", 4));
	nodes[0].sym = s;
	
	f_cfm_init();
	struct f_cfm_eby *y0, *y1, *y2;
	y0 = f_cfm_em_new(_f_ht_sym);
	y1 = f_cfm_em_new(_f_ht_int);
	y2 = f_cfm_em_new(_f_ht_sym);

	y0->em->sym = s;

	y1->em->i = 21;
	y1->em->info = _f_ht_int8|_f_cfm_hei_is;

	y2->em->sym = s;

	nodes[1].src = y1;
	nodes[1].dst = y2; 

	f_cfm_process(&cfm, 1, 0);
}
