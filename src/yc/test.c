TMR0H#  0x59d
TMR0L#  0x59c
T0CON0# 0x59e
T0CON1# 0x59f
PORTC#  0x0e
TRISC#  0x14
ANSELC# 0x1f4e
PIE0#   0x716
PIR0#   0x70c
LATC#   0x1a
INTCON# 0x00b
SSP1BUF#	0x18c
SSP1STAT#	0x18f

TRISA#		0x12
PORTA#		0x0c
ANSELA#		0x1f38
LATA#		0x18
PIR3#		0x70f
SSP1CON2#	0x191
SSP1CLKPPS#	0x1ec5
SSP1DATPPS#	0x1ec6
RA0PPS#		0x1f10
RA1PPS#		0x1f11
WPUC#		0x1f4f

RX1PPS#		0x1ecb
CK1PPS#		0x1ecc

RA2PPS#		0x1f12
RA4PPS#		0x1f14
RA5PPS#		0x1f15
RC4PPS#		0x1f24
RC5PPS#		0x1f25

RC1REG#		0x0119
TX1REG#		0x011a

RC1STA#		0x011d
TX1STA#		0x011e

void delay();
void ioext();
/*
	while(!PORTC#2); loop if high
*/
/*
void _start() {
	_8_u buf[10];
	_8_u *bfp;
	bfp = &buf;
	
	PORTA = 0;
	ANSELA = 0;
	TRISA = 3;
	PORTC = 0;
	ANSELC = 0;
	TRISC = 0;

	aso "call icinit\n";
	LATC = 1;
	SSP1CLKPPS = 1;
	SSP1DATPPS = 0;
	RA0PPS = 20;
	RA1PPS = 19;
	delay(127);
	buf[0] = 0;
	buf[1] = 0;
	ioext(bfp);
	_8_u i;
	i = 0;
_again:
	LATC = 1;
	buf[0] = 0x09;
	buf[1] = i;
	ioext(bfp);
	LATC = 0;
	i+=1;
	i&=7;
	delay(127);
	delay(127);
	goto _again;
}

*/
/*
void _start() {
	while(PIR0#5);
	PORTC = 0;
	ANSELC = 0;
	TRISC = 0;
	LATC = 0;
	_8_u i;
	_8_u j;
	i = 0;
_again:
	j = 1<<i;
	LATC = j;
	i+=1;
	i&=3;
	delay(127);
	delay(127);
	goto _again;
}
*/
/*
void _start() {
	PORTC = 0;
	ANSELC = 0;
	TRISC = 0;
	LATC = 0;

	_8_u i;
	i = 0;
_again:
	LATC = i;
	i+=1;
	delay(127);
	delay(127);
	goto _again;

}
*/
/*
test void();
void start() {
	PORTC = 0;
	ANSELC = 0;
	TRISC = 0;
	LATC = 0;

	_8_u i;
_again:
	i = 0;

	LATC = 1;
	while(i != 100) {
		delay(1);
		i+=1;
	}
	LATC = 0;
	i = 0;
	while(i != 100) {
		delay(1);
		i+=1;
	}

	goto _again;
}
*/

send void();
recv void();
putc void();
getc void();
command void();
void _start() {
	PORTC = 0;
	ANSELC = 0;
	TRISC = 3;
	LATC = 0;
	WPUC = 0;

	PORTA = 0;  
	ANSELA = 0;
	TRISA = 3;
	LATA = 0;
/*
	aso "call uartinit\n";
	RA4PPS = 0x0f;
	RX1PPS = 5;
*/

	aso "call icinit\n";	
	SSP1CLKPPS = 1;
	SSP1DATPPS = 0;
	RA0PPS = 20;
	RA1PPS = 19;
	_8_u buf[10];
	_8_u *bfp;
	bfp = &buf;
	delay(127);
	buf[0] = 0;
	buf[1] = 0;
	ioext(bfp);
	
	buf[0] = 0x09;
	buf[1] = 0x30;
	ioext(bfp);
	command();
	
	delay(30);

	command();
	delay(30);

	command();
	delay(30);

	buf[0] = 0x09;
	buf[1] = 0x38;
	ioext(bfp);
	command();
	
	buf[0] = 0x09;
	buf[1] = 0x10;
	ioext(bfp);
	command();
	
	buf[0] = 0x09;
	buf[1] = 0x0c;
	ioext(bfp);
	command();

	buf[0] = 0x09;
	buf[1] = 0x06;
	ioext(bfp);

	command();
	_8_u i;
	i = 48;
	LATA |= 32;
_again:
	if (i == 0x3a) {
		i = 48;
	}
	buf[0] = 0x09;
	buf[1] = i;
	ioext(bfp);

	command();
//	i = getc();
//	putc('Y');
//	delay(1);
//	delay(20);
//	send(21);
//	i+=1;
//	delay(127);
//	delay(127);
//	i = recv();
	delay(127);
	delay(127);
	LATA &= 0xdf;
	buf[0] = 0x09;
	buf[1] = 1;
	ioext(bfp);
	command();
	LATA |= 32;
	delay(127);
	i+=1;
	goto _again;
}
void command() {
	LATA |= 4;
	delay(1);
	LATA &= 0xfb;
	delay(1);
}

void putc(_8_u __c) {
	TX1REG = __c;	
	while(TX1STA#1);
}

void getc() {
	_8_u c;
	while(PIR3#5);
	PIR3 = 0;
	c = RC1REG;
	ret c;
}
void ioext(_8_u *p) {
	PIR3 = 0;
	SSP1CON2 |= 1;
	while(PIR3#0);
	
	PIR3 = 0;
	SSP1BUF = 0x40;
	while(PIR3#0);
	
	PIR3 = 0;
	SSP1BUF = p[0];
	while(PIR3#0);

	PIR3 = 0;
	SSP1BUF = p[1];
	while(PIR3#0);

	PIR3 = 0;
	SSP1CON2 |= 4;
	while(PIR3#0);
	delay(1);
}

tmr0_strt void();
tmr0_reset void();
tmr0_stop void();
void recv() {
	_8_u i;
	_8_u word;
	_8_u t;
	word = 0;
	i = 0;
	tmr0_strt();
	LATC |= 8;
	while(i != 8) {
		tmr0_reset();
		while(!PORTC#0) {
			if (!PIR0#5){
				ret 0;
			}
		}
		t = PORTC;
		t = t>>1;
		t &= 1;
		t = t<<i;
		word |= t;
		tmr0_reset();
		while(PORTC#0) {
			if (!PIR0#5) {
				ret 0;
			}
		}
		i+=1;
	}

	LATC &= 0xf7;
	tmr0_stop();
	ret word;
}

void send(_8_u __word) {
	_8_u i;
	_8_u word;
	_8_u t;

	i = 0;
	word = __word;	
	t = 0;

	while(PORTC#1);
	while(i != 8) {
		t = word>>i;
		t &= 1;
		t = t<<3;
		LATC |= t;
		LATC |= 4;
		delay(1);
		LATC &= 0xf3;
		delay(1);
		i+=1;
	}

	while(!PORTC#1);
}


void tmr0_strt() {
	TMR0H = 0;
	TMR0L = 0;
	T0CON1 = aso "2<5";
	T0CON0 = aso "1<7'1<4|''0x10|'";
}

void tmr0_reset() {
	PIR0 = 0;
	TMR0H = 0;
	TMR0L = 0;
}

void tmr0_stop() {
	T0CON0 = 0;
	PIR0 = 0;
}

void delay_us(_8_u us) {
	TMR0H = 0;
	TMR0L = 0;
	T0CON1 = aso "2<5";
	T0CON0 = aso "1<7'1<4|''0x10|'"

}

void delay(_8_u ms) {
	_8_u i;
	_8_u l;

	if (ms>7) {
		l = ms>>3;
		l-=1;
	
		TMR0H = 0;
		TMR0L = 0;
		T0CON1 = aso "2<5";
		T0CON0 = l;
		T0CON0 |= aso "1<7'1<4|''0x00|'";
		while(PIR0#5);
		T0CON0 = 0;
		PIR0 = 0;
	}

	l = ms&7;
	if (l>0) {
		i = 0;
		while(i != l) {
			TMR0H = 0xea;
			TMR0L = 0x79;
			T0CON1 = aso "2<5";
			T0CON0 = aso "1<7'1<4|''0x00|'";
			while(PIR0#5);
			T0CON0 = 0;
			PIR0 = 0;

			i+=1;
		}
		
	}
}
