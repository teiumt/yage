# ifndef __yc__vec__h
# define __yc__vec__h
# include "../y_int.h"

typedef struct yc_vec {
	void **pages;
	_int_u off;
	_int_u blksz;
	_int_u page_c;
} *yc_vecp;

yc_vecp yc_vec_new(_int_u);
void yc_vec_destroy(yc_vecp);
void yc_vec_push(yc_vecp, void**);
void yc_vec_pop(yc_vecp, void*);
void *yc_vec_getat(yc_vecp, _int_u);
_int_u yc_vecsize(yc_vecp);
# endif /*__yc__vec__h*/
