;FSR0 = dst
;FSR1 = src
;COMM first 8-bytes = N*
0x20	#define R0
0x28	#define R1
0x30	#define R2
0x38	#define R3
0x04	#define FSR0L
0x05	#define FSR0H
0x06	#define FSR1L
0x07	#define FSR1H
mul0w:
; R0 - source
; R1 - numx
; R2 - dest
; R3 - tmp storage
.L0
	movf R1
	movwf R3
.L1
	movf R0
	addwf j[R2
	movf R0+1
	addwfc j[R2+1

	decfsz j[R3
	goto .L1

	decfsz j[R1+1
	goto .L0
	ret
movfsr01:
	movf FSR1L
	movwf FSR0L
	movf FSR1H
	movwf FSR0H
	ret
movfsr10:
	movf FSR0L
	movwf FSR1L
	movf FSR0H
	movwf FSR1H
	ret
subfsr1w:
	subwf j[FSR1L 
	clrw
	subfwb j[FSR1H
	ret
; 16b FSR addition for dst register
addfsr0:
	movf R2
	addwf j[4
	movf R2+1
	addwfc j[5
	ret
addfsr1:
	movf R2
	addwf j[6
	movf R2+1
	addwfc j[7
	ret
; add to fsr-0 with indirect-1
addfsri0:
	movf 1
	addwf j[4
	moviw j[1
	addwfc j[5
	ret
; add to fsr-1 with indirect-0
addfsri1:
	movf 0
	addwf j[6
	moviw 1
	addwfc j[7
	ret
; moving data from FSR to bank spec
; are form of register space
mov00w:
	movf 0
	movwf R0
	moviw 1
	movwf R0+1
	ret
mov0w:
	movf 1
	movwf R0
	moviw j[1
	movwf R0+1
	ret
mov1w:
	movf 1
	movwf R1
	moviw j[1
	movwf R1+1
	ret
mov2q:
	call mov2d
	moviw j[4
	movwf R2+4
	moviw j[5
	movwf R2+5
	moviw j[6
	movwf R2+6
	moviw j[7
	movwf R2+7
	ret
mov2d:
	movf 1
	movwf R2
	moviw j[1
	movwf R2+1
	moviw j[2
	movwf R2+2
	moviw j[3
	movwf R2+3
	ret
mov2w:
	movf 1
	movwf R2
	moviw j[1
	movwf R2+1
	ret
mov2iq:
	call mov2id
	movf R2+4
	movwi j[4
	movf R2+5
	movwi j[5
	movf R2+6
	movwi j[6
	movf R2+7
	movwi j[7
	ret
mov2id:
	movf R2
	movwf 1
	movf R2+1
	movwi j[1
	movf R2+2
	movwi j[2
	movf R2+3
	movwi j[3
	ret
mov2iw:
	movf R2
	movwf 1
	movf R2+1
	movwi j[1
	ret
mul8:
	movf $1
	addwf $0
	decf 0x70
	ret
mov01w:
	movf $1
	movwf $0
	moviw j[$1
	movwi $1
	ret
mov01q:
	call mov01d
	moviw j[$4
	movwi $4
	moviw j[$5
	movwi $5
	moviw j[$6
	movwi $6
	moviw j[$7
	movwi $7
mov01d:
	movf $1
	movwf $0
	moviw j[$1
	movwi $1
	moviw j[$2
	movwi $2
	moviw j[$3
	movwi $3	
	ret
mov10w:
	movf $0
	movwf $1
	moviw $1
	movwi j[$1
	ret
mov10q:
	call mov10d
	moviw $4
	movwi j[$4
	moviw $5
	movwi j[$5
	moviw $6
	movwi j[$6
	moviw $7
	movwi j[$7
mov10d:
	movf $0
	movwf $1
	moviw $1
	movwi j[$1
	moviw $2
	movwi j[$2
	moviw $3
	movwi j[$3
	ret
add00w:
	movf R0
	addwf j[R2
	movf R0+1
	addwfc j[R2+1
	ret
add0w:
	movf 1
	addwf j[R2
	moviw j[1
	addwfc j[R2+1
	ret
sub0w:
	movf 1
	subwf j[R2
	moviw j[1
	subfwb j[R2+1
	ret
or0w:
	movf 1
	iorwf j[R2
	moviw j[1
	iorwf j[R2+1
	ret
and0w:
	movf 1
	andwf j[R2
	moviw j[1
	andwf j[R2+1
	ret
mov0q:
	call mov0d
	moviw j[4
	movwf R2+4
	moviw j[5
	movwf R2+5
	moviw j[6
	movwf R2+6
	moviw j[7
	movwf R2+7
	ret
mov0d:
	movf 1
	movwf R2
	moviw j[1
	movwf R2+1
	moviw j[2
	movwf R2+2
	moviw j[3
	movwf R2+3	
	ret
mov02q:
	call mov0d
	moviw 4
	movwf R2+4
	moviw 5
	movwf R2+5
	moviw 6
	movwf R2+6
	moviw 7
	movwf R2+7
	ret
mov02d:
	movf 0
	movwf R2
	moviw 1
	movwf R2+1
	moviw 2
	movwf R2+2
	moviw 3
	movwf R2+3	
	ret
clr2q:
	call clr4q
clr2d:
	movwi j[2
	movwi j[3
	ret
clr1q:
	call clr4q
clr1d:
	movwi j[1
	movwi j[2
	movwi j[3
	ret
clr4q:
	movwi j[4
	movwi j[5
	movwi j[6
	movwi j[7	
	ret
; FSR R addition
add0q:
	call add0d
	moviw j[4
	addwfc j[R2+4
	moviw j[5
	addwfc j[R2+5
	moviw j[6
	addwfc j[R2+6
	moviw j[7
	addwfc j[R2+7
	ret
add0d:
	movf 1
	addwf j[R2	
	moviw j[1
	addwfc j[R2+1
	moviw j[2
	addwfc j[R2+2
	moviw j[3
	addwfc j[R2+3	
	ret
; subtraction
sub0q:
	call sub0d
	moviw j[4
	subfwb j[R2+4
	moviw j[5
	subfwb j[R2+5
	moviw j[6
	subfwb j[R2+6
	moviw j[7
	subfwb j[R2+7
	ret
sub0d:
	movf 1
	subwf j[R2
	moviw j[1
	subfwb j[R2+1
	moviw j[2
	subfwb j[R2+2
	moviw j[3
	subfwb j[R2+3
	ret
; inclusive or
or0q:
	call or0d
	moviw j[4
	iorwf j[R2+4
	moviw j[5
	iorwf j[R2+5
	moviw j[6
	iorwf j[R2+6
	moviw j[7
	iorwf j[R2+7
	ret
or0d:
	movf 1
	iorwf j[R2
	moviw j[1
	iorwf j[R2+1
	moviw j[2
	iorwf j[R2+2
	moviw j[3
	iorwf j[R2+3	
	ret
; and logic
and0q:
	call and0d
	moviw j[4
	andwf j[R2+4
	moviw j[5
	andwf j[R2+5
	moviw j[6
	andwf j[R2+6
	moviw j[7
	andwf j[R2+7
	ret
and0d:
	movf 1
	andwf j[R2
	moviw j[1
	andwf j[R2+1
	moviw j[2
	andwf j[R2+2
	moviw j[3
	andwf j[R2+3
	ret
cmpw:
	bsf R3,0
	movf 1
	subwf R2
	btfsis $3,$2
	ret
	moviw j[1
	subwf R2+1
	btfsis $3,2
	ret
	bcf R3,0
	ret
cmpq:
	call cmpd
	btfsis R3,$0
	ret
	bsf R3,0
	moviw j[4
	subwf R2+4
	btfsis $3,$2
	ret
	moviw j[5
	subwf R2+5
	btfsis $3,$2
	ret
	moviw j[6
	subwf R2+6
	btfsis $3,$2
	ret
	moviw j[7
	subwf R2+7
	btfsis $3,$2
	ret
	bcf R3,0
	ret
cmpd:
	bsf R3,0
	movf 1
	subwf R2
	btfsis $3,$2
	ret
	moviw j[1
	subwf R2+1
	btfsis $3,$2
	ret
	moviw j[2
	subwf R2+2
	btfsis $3,$2
	ret
	moviw j[3
	subwf R2+3
	btfsis $3,$2
	ret
	bcf R3,0
	ret
; shift a half to the right
shrhd:
	movf R2+1
	movwf R2
	movf R2+2
	movwf R2+1
	movf R2+3
	movwf R2+2
	ret
shrhq:
	call shrhd
	movf R2+4
	movwf R2+3
	movf R2+5
	movwf R2+4
	movf R2+6
	movwf R2+5
	movf R2+7
	movwf R2+6
	ret
shlhd:
	movf R2+2
	movwf R2+3
	movf R2+1
	movwf R2+2
	movf R2
	movwf R2+1
	ret
shlhq:
	movf R2+6
	movwf R2+7
	movf R2+5
	movwf R2+6
	movf R2+4
	movwf R2+5
	movf R2+3
	movwf R2+4
	call shlhd
	ret
shld:
	lsl j[R2+3
	btfsic R2+2,7
	bsf R2+3,0

	lsl j[R2+2
	btfsic R2+1,7
	bsf R2+2,0
shlw:
	lsl j[R2+1
	btfsic R2,7
	bsf R2+1,0

	lsl j[R2
	ret
shlq:
	lsl j[R2+7
	btfsic R2+6,7
	bsf R2+7,0
	
	lsl j[R2+6
	btfsic R2+5,7
	bsf R2+6,0

	lsl j[R2+5
	btfsic R2+4,7
	bsf R2+5,0

	lsl j[R2+4
	btfsic R2+3,7
	bsf R2+4,0
	call shld
	ret
shrw:
	lsr j[R2
	btfsic R2+1,0
	bsf R2,7

	lsr j[R2+1
	ret
shrd:
	call shrw
	btfsic R2+2,0
	bsf R2+1,7

	lsr j[R2+2
	btfsic R2+3,0
	bsf R2+2,7

	lsr R2+3
	ret
shrq:
	call shrd
	btfsic R2+4,0
	bsf R2+3,7

	lsr j[R2+4
	btfsic R2+5,0
	bsf R2+4,7

	lsr j[R2+5
	btfsic R2+6,0
	bsf R2+5,7

	lsr j[R2+6
	btfsic R2+7,0
	bsf R2+6,7

	lsr R2+7
	ret
