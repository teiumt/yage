#"header.h"
void putc(_8_u __c) {
	TX1REG = __c;
	while(TX1STA#1);
}

void getc() {
	_8_u c;
	while(PIR3#5);
	PIR3 = 0;
	c = RC1REG;
	ret c;
}

/*
	UART:
	pins
		-TX: RA4
		-RX: RA5
*/

void init() {
	PORTC = 0;
	ANSELC = 0;
	TRISC = 3;
	LATC = 0;
	WPUC = 0;

	aso "call uartinit\n";
	RA4PPS = 0x0f;
	RX1PPS = 5;

	PORTA = 0;
	ANSELA = 0;
	TRISA = 0;
	LATA = 0;
}

void main() {
	init();
_again:
	LATC = 4;
	delay(10);
	LATC = 0;
	delay(10);
	goto _again;
}
