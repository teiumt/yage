#include "header.h"
void delay(_8_u ms) {
	_8_u i;
	_8_u l;

	if (ms>7) {
		l = ms>>3;
		l-=1;

		TMR0H = 0;
		TMR0L = 0;
		T0CON1 = 0x40;
		T0CON0 = l;
		T0CON0 |= 0x90;
		while(!PIR0#5);
		T0CON0 = 0;
		PIR0 = 0;
	}

	l = ms&7;
	if (l>0) {
		i = 0;
		while(i != l) {
			TMR0H = 0xea;
			TMR0L = 0x79;
			T0CON1 = 0x40;
			T0CON0 = 0x90;
			while(!PIR0#5);
			T0CON0 = 0;
			PIR0 = 0;
		
			i+=1;
		}
	}
}
