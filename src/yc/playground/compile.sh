rm *.o
../yc main.c
../yc delay.c
../yc io.c
../yc timer.c
../../as/tas -i main.s -o main.o -p non -g malk -f blf
../../as/tas -i delay.s -o delay.o -p non -g malk -f blf
../../as/tas -i io.s -o io.o -p non -g malk -f blf
../../as/tas -i timer.s -o timer.o -p non -g malk -f blf
../../as/tas -i comm.s -o comm.o -p non -g malk -f blf
../../as/tas -i lib.s -o lib.o -p non -g malk -f blf
../../as/tas -i start.s -o start.o -p non -g malk -f blf
../../bond/ybn -s "start.o main.o delay.o io.o timer.o comm.o lib.o" -d a.out -f blf
../../tools/slam_dump a.out
../../tools/pic_dis slam.out
