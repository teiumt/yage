.extern mov01
.extern mov10
.extern delay
.extern send
.extern recv
.extern uartinit
.extern icinit
.globl tmr0_stop
.globl tmr0_reset
.globl tmr0_strt
tmr0_strt:
;sub16
movlw $0
subwf j[$6
movlw $0
subfwb j[$7
movlw $0
movlb $11
movwf $29
movlw $0
movlb $11
movwf $28
movlw 2<5
movlb $11
movwf $31
movlw 1<7'1<4|''0x10|'
movlb $11
movwf $30
;add16
movlw $0
addwf j[$6
movlw $0
addwfc j[$7
ret
tmr0_reset:
;sub16
movlw $0
subwf j[$6
movlw $0
subfwb j[$7
movlw $0
movlb $14
movwf $12
movlw $0
movlb $11
movwf $29
movlw $0
movlb $11
movwf $28
;add16
movlw $0
addwf j[$6
movlw $0
addwfc j[$7
ret
tmr0_stop:
;sub16
movlw $0
subwf j[$6
movlw $0
subfwb j[$7
movlw $0
movlb $11
movwf $30
movlw $0
movlb $14
movwf $12
;add16
movlw $0
addwf j[$6
movlw $0
addwfc j[$7
ret
