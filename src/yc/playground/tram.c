#include "tram.h"
#include "header.h"
void tram_recv(_8_u *__buf, _8_u __n) {
	_8_u i;
	_8_u word;
	_8_u t;
_again:
	word = 0;
	i = 0;
	LATC |= 8;
	while(i != 8) {
		while(PORTC#0);
		t = PORTC;
		t = t>>1;
		t &= 1;
		t = t<<i;
		word |= t;
		while(!PORTC#0);
		i+=1;
	}

	LATC &= 0xf7;
	__n = __n-1;
	*(__buf+__n) = word;
	if (__n != 0) {
		goto _again;
	}
}

void tram_send(_8_u *__buf, _8_u __n) {
	_8_u i;
	_8_u word;
	_8_u t;

	while(PORTC#1);
_again:
	__n = __n-1;
	word = *(__buf+__n);
	i = 0;
	while(i != 8) {
		t = word>>i;
		t &= 1;
		t = t<<3;
		LATC |= t;
		LATC |= 4;
		LATC &= 0xf3;
		i+=1;
	}

	if (__n != 0) {
		goto _again;
	}

	while(!PORTC#1);
}
