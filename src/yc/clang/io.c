# include "../yc.h"
#include "../../io.h"
#include "../../assert.h"
char static linebuf[0x100];
#define linebuf clg_struc.file.linebuf
#define line_length clg_struc.file.line_length
#define line_pos clg_struc.file.line_pos
void clg_directive(char*, _int_u);
char static
nextchr(void) {
	char c;
_miss:
	if (fc_struc.core.ll_bufi != fc_struc.core.llbn) {
		
		c = fc_struc.core.ll_buf[fc_struc.core.ll_bufi++];
		printf("CHAR: '%c'.\n",c);
		if (c == '#') {
			if (fc_struc.core.ll_bufi+1 < fc_struc.core.llbn) {
				char c;
				c = fc_struc.core.ll_buf[fc_struc.core.ll_bufi+1];
				if (c>='a'&&c<='z') {
					clg_directive(fc_struc.core.ll_buf+fc_struc.core.ll_bufi,fc_struc.core.llbn-fc_struc.core.ll_bufi);
					goto _miss;
				}
			}
		}
		return c;
	} else
	if (line_pos == line_length) {
		if (fc_struc.file->state == Y_ENDOF) {
			printf("at end of line.\n");	
			return '\0';
		}
		_int_u i;
		_8_u state;
	_back:
		i = 0;
	_again:
		state = yc_read(fc_struc.file, &c, 1);
		linebuf[i] = c;
		printf("LINE: ;%c;.\n", c);
		if (state != Y_ENDOF) {
			i++;
			if (c != '\n')
				goto _again;
		}
		if (!i && state != Y_ENDOF) {
			goto _back;
		}

		if (linebuf[0] == '#') {
			line_length = 0x100;
			line_pos = 0x100;
			clg_directive(linebuf+1,i-1);
			goto _miss;
		}
		fc_struc.file->state = state;
		if (!i)
			return '\0';
		line_length = i;
		line_pos = 0;
	}
	printf("CHAR: '%c': %u~%u.\n", linebuf[line_pos], line_pos, line_length);
	return linebuf[line_pos++];
}

char f_clg_getchr(void) {
	if (clg_struc.file.cb>clg_struc.file.cbuf) {
		return *(--clg_struc.file.cb);
	}
	return nextchr();
}
// take peek - next char
char f_clg_tpnc(void) {
	if (clg_struc.file.cb>clg_struc.file.cbuf)
		return *(clg_struc.file.cb-1);
	char c;
	c = nextchr();
	*(clg_struc.file.cb++) = c;
	return c;
}

// compare next match move on
_8_s f_clg_cnmmo(char __c) {
	char c;
	if (clg_struc.file.cb>clg_struc.file.cbuf) {
		c = *(clg_struc.file.cb-1);
		if (c == __c) {
			clg_struc.file.cb--;
			return 0;
		}
		return -1;
	}
	c = nextchr();
	if (c == __c) {
		return 0;
	}

	*(clg_struc.file.cb++) = c;
	return -1;
}

void f_clg_ugetc(char __c) {
	*(clg_struc.file.cb++) = __c;
}

