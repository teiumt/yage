#include "../yc.h"
#include "../../ffly_def.h"
#include "../../m_alloc.h"
#include "../../string.h"
#include "../../io.h"
#include "../../assert.h"
#define GETCHR f_clg_getchr()
#define CNMMO f_clg_cnmmo
#define isspace(__c) (__c == ' ' || __c == '\0' || __c == '\n' || __c == '\t')
_int_u static read_ident(char **__dst) {
	char buf[128];
	char *cp;
	cp = buf;

	char c;
_again:
	c = GETCHR;
	if ((c>='a' && c<='z') || c == '_' || (c>='0' && c<='9') || (c>='A'&&c<='Z')) {
		*(cp++) = c;
		goto _again;
	} else {
		f_clg_ugetc(c);
	}

	*cp = '\0';
	_int_u len;
	char *p;
	mem_dup(&p, buf, (len = (cp-buf))+1);
	*__dst = p;
	return len;
}

char static escape_chr(char __c) {
	switch(__c) {
		case 'n': return '\n';
		case 't': return '\t';
	}
	return '\0';
}

_int_u static read_str(char **__dst) {
	char buf[128];
	char *cp;
	cp = buf;

	char c;
_again:
	c = GETCHR;
	if (c != 0x22) {
		if (c == '\\') {
			c = GETCHR;
			c = escape_chr(c);
		}
		*(cp++) = c;
		goto _again;
	}

	_int_u len;
	char *p;
	mem_dup(&p, buf, (len = (cp-buf))+1);
	*__dst = p;
	return len;
}


_int_u static read_num(char **__dst) {
	char buf[128];
	char *cp;
	cp = buf;

	char c;
_again:
	c = GETCHR;
	if ((c>='0' && c<='9') || c == 'x' || (c>='a' && c<='f')) {
		*(cp++) = c;
		goto _again;
	} else {
		f_clg_ugetc(c);
	}

	*cp = '\0';
	_int_u len;
	char *p;
	mem_dup(&p, buf, (len = (cp-buf))+1);
	*__dst = p;
	return len;
}

char static skspace(char c) {
_j1:
	if (!(fc_struc.file->state&Y_ENDOF) || clg_struc.file.cb > clg_struc.file.cbuf || clg_struc.file.line_pos != clg_struc.file.line_length || fc_struc.core.ll_bufi != fc_struc.core.llbn) {
		if (isspace(c)) {	
			c = GETCHR;
			goto _j1;
		}
	}

	if (c == '/') {
		char d;
		d = GETCHR;
		if (d == '/') {
			while(GETCHR != '\n');
			c = GETCHR;
			goto _j1;
		}else
		if (d == '*') {
			while(1) {
				d = GETCHR;
			_j0:
				if (d == '*') {
					d = GETCHR;
					if (d == '/') {
						c = GETCHR;
						goto _j1;
					} else
						goto _j0;
				}
			}
		} else
			f_clg_ugetc(c);
	}
	if (isspace(c)) {
		printf("failed to ignore escape.\n");
		c = '\0';
	}
	return c;
}
f_clg_tokenp clg_lex(void) {
	f_clg_tokenp tok;
	if (clg_struc.file.tp>clg_struc.file.tokbuf) {
		tok = *(--clg_struc.file.tp);

		if (!tok) {
			printf("this should not be null.\n");
		}
		return tok;
	}

	if (clg_struc.file.tp0>clg_struc.file.tokbuf0) {
		tok = *(--clg_struc.file.tp0);

		if (!tok) {
			printf("this should not be null.\n");
		}
		return tok;
	}

	char c;
	if (fc_struc.file->state&Y_ENDOF && clg_struc.file.cb == clg_struc.file.cbuf && clg_struc.file.line_pos == clg_struc.file.line_length && fc_struc.core.ll_bufi == fc_struc.core.llbn) {
		printf("CHAR: !!!'%c'\n", GETCHR);
		return NULL;

	}
	c = GETCHR;
	c = skspace(c);

//	if (c == '\0')return NULL;
	
	tok = (f_clg_tokenp)m_alloc(sizeof(struct f_clg_token));
	tok->kind = _clg_tok_keywd0;
	tok->id = 0xff;
	tok->eof = -1;
	tok->res = 0;
	tok->other = 0;
	tok->sum = 0;
	tok->sum0 = 0;
	_64_u pre = 0;
	/*
		yes i know i over did this a bit :|.
	*/
	char sv = c;
	_64_u k;
	tok->sum |= (pre = (clg_struc.leadmap+0x600)[c]);
	if (pre != 0) {
		c = GETCHR;
		c = skspace(c);
		tok->sum |= (pre = (clg_struc.leadmap+0x600)[c])<<32;
		if (c == '!' || c == 0x27) {
			char sv;
			sv = c;
			c = GETCHR;
			if (c != '=')
				goto _outof;
			f_clg_ugetc(c);
			c = sv;
		}
		if (pre != 0) {
			char cc;
			cc = c;
			_64_u k;
			k = (clg_struc.leadmap)[sv];
			k |= (clg_struc.leadmap+0x100)[c];
			c = GETCHR;
			c = skspace(c);
			k = (clg_struc.leadmap+0x200)[k];

			pre = (clg_struc.leadmap+0x300)[c];
			tok->sum0 = (clg_struc.leadmap+0x600)[c];
			tok->res = (clg_struc.leadmap+0x700)[k|pre];
			if (pre != 0) {
			} else {
				f_clg_ugetc(c);
			}
			printf("############ DOUBLE: '%c%c', RES: %u, sum{%x, %x},%u, %u, %u, sum0: %u\n", sv, cc, tok->res, tok->sum,tok->sum>>32,k,(clg_struc.leadmap)[sv],(clg_struc.leadmap)[c],tok->sum0);

			return tok;
		} else {
			// this is stupid by its the only way other then more MAPS
			tok->res = clg_struc.leadmap[clg_struc.leadmap[clg_struc.leadmap[sv]+0x200]+0x700];
		}
	}
_outof:

/*
		issue below there is an unget char routine that causes issues when ending the file
*/
	if (fc_struc.file->state&Y_ENDOF && clg_struc.file.cb == clg_struc.file.cbuf && clg_struc.file.line_pos == clg_struc.file.line_length && fc_struc.core.ll_bufi == fc_struc.core.llbn) 
		return tok;

	c = skspace(c);
	if (((c>='a'&&c<='z') || c == 0x5c) && tok->sum&MUG(__clg_quote,__clg_quote)) {
		if (c == 0x5c) {
			c = escape_chr(GETCHR);
		}
		tok->s = (void*)c;
		tok->kind = _clg_tok_chr;

		GETCHR;
	}else
//	printf("TOK: %c->%u ! %c.\n", sv, tok->flags, c);	
	if ((c>='a'&&c<='z') || c == '_' || (c>='A'&&c<='Z')) {
		f_clg_ugetc(c);
		tok->kind = _clg_tok_ident;
		tok->size = read_ident(&tok->s);
	} else if (c>='0' && c<='9') {
		f_clg_ugetc(c);
		tok->kind = _clg_tok_num;
		tok->size = read_num(&tok->s);
	} else if (c == 0x22){
		tok->kind = _clg_tok_str;
		tok->size = read_str(&tok->s);
	}
	c = GETCHR;
	tok->other |= (pre = (clg_struc.leadmap+0x400)[c]);
	if (!pre)
		f_clg_ugetc(c);
	c = GETCHR;
	f_clg_ugetc(skspace(c));
	if (fc_struc.file->state&Y_ENDOF && clg_struc.file.cb == clg_struc.file.cbuf && clg_struc.file.line_pos == clg_struc.file.line_length && fc_struc.core.ll_bufi == fc_struc.core.llbn) {
		tok->eof = 0;
	}
	return tok;
}


void clg_ulex(f_clg_tokenp __tok) {
	if (!__tok) {
		printf("UNLEX a null token?\n");
		return;
	}

	*(clg_struc.file.tp++) = __tok;
}
