# ifndef __f__clang__clang__h
# define __f__clang__clang__h
# include "../../y_int.h"
# include "../vec.h"
# include "../hash.h"
enum {
    _clg_dl_var
};


typedef struct f_clg_sym *f_clg_symp;

struct f_clg_env {
	yc_hashp hm;
	_int_u ns;
	f_clg_symp sym;
	_8_u glob;
	
	yc_vecp dl[4];
};
enum {
	_clg_tok_ident,
	_clg_tok_keywd,
	_clg_tok_keywd0,
	_clg_tok_num,
	_clg_tok_str,
	_clg_tok_chr
};

enum {
	_clg_k_void,
	_clg_k_8u,
	_clg_k_16u,
	_clg_k_32u,
	_clg_k_64u,
	_clg_k_goto,
	_clg_k_aso,
	_clg_k_if,
	_clg_k_while,
	_clg_k_for,
	_clg_k_switch,
	_clg_k_ret,
	_clg_k_else,
	_clg_k_struct	
};
/*
	NOTE:
		NUMBER sign is a complement and is not used to resolve things
		if (NUMBER)
*/
#define _CLG_NUMBER 		1
#define _CLG_EXCLAMATION	2
#define _CLG_COLON			4
#define _lhs_mask	(0xf0f)
#define _rhs_mask	(0xf0f<<4)

#define _LHS_mask ((_64_u)0xffffffff) 
#define _RHS_mask (((_64_u)0xffffffff)<<32)
#define _SR(__v) (__v<<4)
#define _SR16(__v) (((_64_u)__v)<<32)
#define _lrm		(_lhs_mask|_rhs_mask)
//flags??
/*
	this is how this works:

	lhs['&'] = 1;
	rhs['&'] = 1<<8;
	_64_u val = rhs['&']|lhs['&'];

	val = (lhs['&']|rhs['&'])

	for now lhs and rhs are the same lookup table
*/
/*
	apart of tok->sum
*/
#define __clg_l_paren    ((_64_u)1<<1)
#define __clg_r_paren    ((_64_u)1<<2)
#define __clg_l_brace    ((_64_u)1<<3)
#define __clg_r_brace    ((_64_u)1<<4)
#define __clg_l_bracket  ((_64_u)1<<5)
#define __clg_r_bracket  ((_64_u)1<<6)
#define __clg_semicolon ((_64_u)1<<7)
#define __clg_equal		((_64_u)1<<8)
#define __clg_astrisk	((_64_u)1<<9)
#define __clg_exclamation		((_64_u)1<<10)
#define __clg_ampersand ((_64_u)1<<11)
#define __clg_gt	((_64_u)1<<12)
#define __clg_lt	((_64_u)1<<13)
#define __clg_comma	((_64_u)1<<14)
#define __clg_quote	((_64_u)1<<15)
#define __clg_plus	((_64_u)1<<16)
#define __clg_minus	((_64_u)1<<17)
#define __clg_verticalbar	((_64_u)1<<18)
#define __clg_dot			((_64_u)1<<19)

#define _clg_plus0		1
#define _clg_minus0		2
#define _clg_l_paren0	3
#define _clg_r_paren0	4
#define _clg_equal0		5
#define _clg_ampersand0	6
#define _clg_verticalbar0	7
#define _clg_lt0	8
#define _clg_gt0	9
#define _clg_l_bracket0	10
#define _clg_r_bracket0	11
#define _clg_null0	12


#define _clg_plus1			1
#define _clg_minus1			2
#define _clg_ampersand1		3
#define _clg_verticalbar1	4
#define _clg_lt1			5
#define _clg_gt1			6
#define _clg_and1			7


#define doub(__v0,__v1)(__v0|(__v1<<4))
#define MUG(__v0, __v1) (((_64_u)(__v0))|(((_64_u)__v1)<<32))
//operative mask
#define clg_opmk			(7<<6)
#define clg_isop			(1<<5)
#define _clg_plus			((0<<6)|clg_isop)
#define _clg_minus			((1<<6)|clg_isop)
#define _clg_ampersand		((2<<6)|clg_isop)
#define _clg_verticalbar	((3<<6)|clg_isop)
#define _clg_lt2			((4<<6)|clg_isop)
#define _clg_gt2			((5<<6)|clg_isop)
#define _clg_and			(7<<5)
#define _clg_mul			((6<<6)|clg_isop)
typedef struct f_clg_token {
	_8_u kind;
	_64_u id;
	_int_u size;
	/*
		a = a+b;

		we need a numerical version of sum
		like this....

		+	-0
		-	-1
		/	-2
		*	-3
	*/
	_64_u res;
	/*
		this is for 

		# in this case does not exist and complements its partner 
		TEST#	0xff
		and statment(TEST#0)
	*/
	_64_u other;
	/*
		if we need to check if ether X or Y is present in the token

		like this..

		if (sum&l_brace || sum&r_brace) {

		}

		sum0 = 3rd charicter

		<<= = sum0 = __clg_verticalbar 
	*/
	_64_u sum, sum0;
	char *s;
	_8_s eof;
} *f_clg_tokenp;
typedef struct clg_type *clg_typep;
/*
	expression passing struct
*/
typedef struct f_clg_crude {
	clg_typep ty, ty0;
	f_clg_symp s;
	_8_u flags;
	struct f_cfm_em *y;
	struct f_cfm_em _y;
	struct cfm_vsp *vs;
	struct cfm_vsp *v0;
	_64_u vs_ac;
	_64_u v0_ac;
} *f_clg_crudep;

#define _clg_ptr 1
typedef struct clg_type {
	/*
		int,float,struct,union,etc
	*/
	_8_u kind;

	struct cfm_vsp *v;
	_8_u szdx;
	_int_u len;
	_8_u bits;	
	_64_u offset;
	yc_hashp members;
	struct cfm_vsp _v;
} *clg_typep;


#define CLG_CRDF_COMP 0x01
typedef struct f_clg_sym {
/*
	could break because of move?
*/
	struct f_clg_crude d;

	char name[24];
	_int_u len;
	struct clg_type ty0;
	struct clg_type *ty;
	f_cfm_dwelp dw;

	f_cfm_symp s;
/*
	global symbol chain
*/
	struct f_clg_sym *link;
/*
	local symbol chain in respect with its surroundings
*/
	struct f_clg_sym *next;
	struct f_clg_node *n;
	void *priv;
	_8_u flags;

	/*
		we assume that the symbols an extern
	*/
	struct f_clg_sym *ur, **ur_bk;
} *f_clg_symp;
/*
	within function sight
*/
#define CLG_WIFS	0x01

struct clg_file {
	char *linebuf;
	_int_u line_length;
	_int_u line_pos;

	char cbuf[24];
	char *cb;
	f_clg_tokenp tokbuf[24];
	f_clg_tokenp *tp;
	f_clg_tokenp tokbuf0[24];
	f_clg_tokenp *tp0;
};

typedef struct f_clg_c {
	struct f_cfm cfm;
	f_clg_symp sy;
	
	struct f_clg_env genv;
	struct f_clg_env *tenv; //the env
	struct f_cc_fcst *fc;

	_int_u ur_cnt;
	_int_u gb_cnt;
	struct f_clg_sym *ur, *gb;

	_8_u bits;
	_64_u *leadmap;
	_8_u *land;
	struct clg_file file;

	yc_hashp types, defines;
} *f_clg_cp;

/*
	ws flags
*/
#define CLG_COMP 0x01
#define CLG_INV 0x02
#define CLG_DEREF 0x04
#define CLG_ADDROF	0x08
#define CLG_REG		0x10
//working struc
typedef struct clg_wkstrc {
	f_clg_crudep d;//keept at the top
	_64_u flags;
	_64_u flags0;
	_64_u off;
	_64_u dis;
	_64_u nc_off_ac;
	_64_u paw;
	struct f_cfm_em *nc_off;
	f_cfm_nodep nc_off_n;
	
	struct f_cfm_em *nc_dis;
	f_clg_tokenp tok;
	f_cfm_nodep n;
} *clg_wkstrcp;

extern struct f_clg_c clg_struc;
void clg_showtok(f_clg_tokenp);

_8_i clg_expecttok(_8_u, _64_u);
_8_i clg_nexttokis(_8_u, _64_u);

_8_s f_clg_cnmmo(char);
f_clg_tokenp clg_peektok0(void);
void f_clg_cfm();
f_clg_tokenp clg_peektok(void);
void clg_ulex(f_clg_tokenp);
f_clg_tokenp clg_nexttok(void);
f_clg_tokenp clg_lex(void);
char f_clg_getchr(void);
void f_clg_init(void);
void f_clg_ugetc(char);
void f_clg_parser(void);
char f_clg_tpnc(void);
# endif /*__f__clang__clang__h*/
