/*
	FIX:
	encasment expressions

	a = (10+10)+(10+10)+10		=	false
	a = (10+10)+(10+10)+(10+10)	=	false
	a = (10+10)+(10+10)			=	true
*/
# include "../yc.h"
# include "../../io.h"
# include "../../string.h"
# include "../../m_alloc.h"
# include "../../ffly_def.h"
# include "../../strf.h"
# include "../../assert.h"
#define NA_PAGE_SHFT F_CFM_PAGE_SHFT
#define NA_PAGE_SIZE (1<<NA_PAGE_SHFT)
#define NA_PAGE_MASK (NA_PAGE_SIZE-1)
// node allocator
#define FUNCFOR(__n, __x) (__n->f = (stf->func[__x]));
#define SPFOR(__n,__x) (__n->loadin = (stf->sp[__x]));
#define PAWFOR(__x) stf->pawtab[__x]
static struct cfm_stuff *stf;
/*
	TODO:
		direct with CFM
		not middle part ie. whats awaits below.
*/
_8_u static within_func_call = -1;
static struct {
	struct f_cfm_node **nodes;
	_int_u page_c;
	_int_u off;
} na;

struct flock {
	void(*end)(f_cfm_flockp);
	_int_u off;
	_int_u n;
	_int_u placment;
	struct cfm_here here;
	f_cfm_fpop po;
	struct cfm_props ret;
	struct flock *next;
};

struct superflock {
	_int_u n;
	_int_u placment;
	struct flock *link;
	struct superflock *next;
};

struct track {
	struct f_cfm_track trk;
	_int_u i;
	struct superflock *link;
	struct track *next;
};

static struct track *curtrack = NULL;
static struct track *t_head = NULL, *t_tail = NULL;
static void *func_fils[64];
static _int_u nff = 0;

_64_u static _read_no(char*, _int_u);
static struct superflock*
new_superflock(void) {
	printf("NEW SUPERFLOCK.\n");
	struct superflock *sf;
	sf = (struct superflock*)m_alloc(sizeof(struct superflock));
	sf->n = 0;
	sf->link = NULL;
	sf->placment = curtrack->i++;
	sf->next = curtrack->link;
	curtrack->link = sf;
	return sf;
}

static struct flock *new_flock(void) {
	printf("NEW FLOCK.\n");
	struct flock *f;
	f = (struct flock*)m_alloc(sizeof(struct flock));
	f->off = na.off;
	f->n = 0;
	f->placment = curtrack->link->n++;
	f->po = NULL;
	f->end = NULL;
	f->next = curtrack->link->link;
	curtrack->link->link = f;
	return f;
}

static struct track *new_track(void) {
	printf("NEW TRACK.\n");
	struct track *t;
	t = (struct track*)m_alloc(sizeof(struct track));
	t->i = 0;
	t->trk.sf = NULL;
	t->link = NULL;
	if (t_tail != NULL)
		t_tail->next = t;
	t_tail = t;
	return t;
}
#define curflock (curtrack->link->link)
f_cfm_fpop new_cfm_fpo(struct flock *__f) {
	f_cfm_fpop op;
	op = (f_cfm_fpop)m_alloc(sizeof(struct f_cfm_fpo));
	op->link = __f->po;
	__f->po = op;
	return op;
}

f_cfm_nodep new_cfm_node(void) {
	f_cfm_nodep n;
	_int_u pg, pg_off;
	pg = na.off>>NA_PAGE_SHFT;
	pg_off = na.off&NA_PAGE_MASK;
	if (pg>=na.page_c) {
		na.nodes = (f_cfm_nodep*)m_realloc(na.nodes, (++na.page_c)*sizeof(f_cfm_nodep));
		na.nodes[pg] = (f_cfm_nodep)m_alloc(NA_PAGE_SIZE*sizeof(struct f_cfm_node));
	}

	na.off++;
	n = na.nodes[pg]+pg_off;
	curtrack->link->link->n++;
	return n;
}

static _8_s atbase = 0;
f_cfm_nodep newnode(void) {
	f_cfm_nodep n;
	if (!atbase){
		n = new_cfm_node();
		atbase = -1;
		return n;
	}

	return m_alloc(sizeof(struct f_cfm_node));
}


#define newcrude ((f_clg_crudep)m_alloc(sizeof(struct f_clg_crude)))
#define newsym ((f_clg_symp)m_alloc(sizeof(struct f_clg_sym)))
#define newtype ((clg_typep)m_alloc(sizeof(struct clg_type)))

void static _dlink(void *__ptr, _8_u __link) {
	void *p;
	yc_vec_push(clg_struc.tenv->dl[__link], (void*)&p);
	*(void**)p = __ptr;
}
static struct cfm_vsp vs_in8 = {_f_cfm_int8,1,1};
static struct cfm_vsp vs_in16 = {_f_cfm_int16,2,2};
static struct cfm_vsp vs_in32 = {_f_cfm_int32,4,4};
static struct cfm_vsp vs_in64 = {_f_cfm_int64,8,8};
static struct clg_type *_int8 = &(struct clg_type){_f_cfm_int_8, &vs_in8, 0, 1, 0,0};
static struct clg_type *_int16 = &(struct clg_type){_f_cfm_int_16, &vs_in16, 1, 1, 0,0};
static struct clg_type *_int32 = &(struct clg_type){_f_cfm_int_32, &vs_in32, 2, 1,0, 0};
static struct clg_type *_int64 = &(struct clg_type){_f_cfm_int_64, &vs_in64, 3, 1, 0,0};
_8_s static expectfix(_64_u __bit) {
	f_clg_tokenp tok;
	tok = clg_nexttok();
	printf("EXPECTFIX: %u, %u, %x\n", tok->kind,tok->id,tok->sum);
	/*
		the compount statment requires this, so to let it know where it gets off at.
		IT NEEDS THE TOKEN that encases the RHS brace,

		EXAMPLE:

		{ <- STATR

			_8_u test;
		} <- STOP

		the end token look somthing like this

		;}

		and we want to unlex the token so compound can grab it.
		other things like nested compound statment handle them selfs i hope :|
	*/

	/*
		if we get a

		;*		then we need to unlex
		or any other combination 
		~*
	*/
	if ((tok->sum&MUG(__clg_r_brace,__clg_r_brace)) || tok->kind != _clg_tok_keywd0 || tok->sum&_SR16(__clg_astrisk) || tok->sum&_SR16(__clg_l_paren)) {
		printf("TOKEN is found to be necessary!, %u, %u, %u, - %u\n",(tok->sum&MUG(__clg_r_brace,__clg_r_brace)),tok->kind != _clg_tok_keywd0,tok->sum&_SR16(__clg_astrisk), tok->kind);
		clg_ulex(tok);
	} else {
		printf("NO expect to FIX.\n");
	}

	/*
		token could be intangled with another

		so we could get

		EXAMPLE


		_8_u a[100];

		];			<- this


		OR

		_8_u a;

		;}			<- this
	*/
	if (tok->sum&MUG(__bit,__bit)) {
		
		return 0;
	}

	return -1;
}

_8_i static is_func(void) {
	_8_s res = 0;
	f_clg_tokenp tokbuf[24];
	f_clg_tokenp tok;
	tokbuf[0] = clg_nexttok();
	if (!tokbuf[0])
		return -1;
	if (tokbuf[0]->kind != _clg_tok_keywd) {
		printf("dident get keyword, %u\n",tokbuf[0]->kind);
		goto _r0;
	}
	tokbuf[1] = clg_nexttok();
	if (!tokbuf[1])
		goto _r0;
	if (tokbuf[1]->kind != _clg_tok_ident) {
		printf("dident get ident, %u.\n",tokbuf[1]->kind);
		goto _r1;
	}

	tokbuf[2] = clg_nexttok();
	if (!tokbuf[2])
		goto _r1;

	printf("TOK0: %u, %u, TOK1: %u, %u, TOK2: %u, %u.\n",
		tokbuf[0]->kind, tokbuf[0]->id,
		tokbuf[1]->kind, tokbuf[1]->id,
		tokbuf[2]->kind, tokbuf[2]->id);
	/*
		if we get 

		void test() {

		}

		or
		void test();
	*/
	if (!(tokbuf[2]->sum&MUG(__clg_l_paren,__clg_l_paren))) {
		res = 0;
		goto _end;
	}
	if (tokbuf[2]->sum == MUG(__clg_l_paren,__clg_r_paren)){
		tokbuf[3] = clg_nexttok();
		assert(tokbuf[3] != NULL);
		if (tokbuf[3]->sum&MUG(__clg_l_brace,__clg_l_brace))
			res = 1;
		else
			res = 2;
		clg_ulex(tokbuf[3]);
	} else {
		f_clg_tokenp buf[16];
		_int_u i;
		i = 0;
	_roll:
		printf("is_func: checking token-%u.\n",i);
		buf[i] = clg_nexttok();
		if (buf[i] != NULL) {
		/*
			void test( _8_u test
			if we get an ')' or ',' on the fourth token then its a func_decl
		*/
			if (buf[i]->kind != _clg_tok_ident) {
				if (buf[i]->sum&MUG(__clg_r_paren|__clg_comma,__clg_r_paren|__clg_comma)) {
					// this is a func decl
					res = 2;
				}else {
					i++;
					goto _roll;
				}
			}else{
				// this is a func def
				res = 1;
			}
		} else {
			printf("NULL lex.\n");
		}
		i++;
/*
		start to unroll
*/
		while(i != 0) {
			i--;
			clg_ulex(buf[i]);
			printf("unrolling token: %u.\n", i);
		}
	}
_end:
	printf("is_func: R2.\n");
	clg_ulex(tokbuf[2]);
_r1:
	printf("is_func: R1%s\n",!tokbuf[2]?"(NULL)":"");
	clg_ulex(tokbuf[1]);
_r0:
	printf("is_func: R0%s\n",!tokbuf[1]?"(NULL)":"");
	clg_ulex(tokbuf[0]);
	printf("is_func: RES:%d.\n",res-1);
	return res-1;

}
//consume
//declaration specifiers

//_8_s static is_typequalifier()
//const/etc

_8_i static is_type(f_clg_tokenp __tok) {
	_8_i res;
	if (__tok->kind != _clg_tok_keywd) {
		printf("is_type: nope not a keyword.\n");
		return -1;
	}
	res = (__tok->id == _clg_k_void)|(__tok->id == _clg_k_8u)|(__tok->id == _clg_k_16u)|(__tok->id == _clg_k_32u)|(__tok->id == _clg_k_64u);
	return res-1;
}

f_clg_symp static new_symbol(char const *__name, _int_u __len) {
	if (!__len) {
		while(1) {
			f_printf("error symb len is zero????.\n");
		}
	}
	f_clg_symp s = newsym;

	mem_cpy(s->name, __name, __len);
	s->len = __len;

	s->link = clg_struc.sy;
	clg_struc.sy = s;
	s->flags = 0x00;
	s->s = f_cfm_sym_new(fc_sym_new(__name, __len));
/*	s->s->flags = 0x00;
	s->ty = NULL;
	s->d.s = s;
	s->d.flags = 0x00;
	s->s->w = &s->w;
	s->w.sy = s->s;
	s->w.type = 0;

	s->w.vs.ac = _cfm_ac_sym_stry;
	s->w.vs.info = 0;
*/
	s->next = clg_struc.tenv->sym;
	clg_struc.tenv->sym = s;
	clg_struc.tenv->ns++;
	return s;
}

void static
parser_decl_spec(f_clg_tokenp __tok, clg_typep *__ty) {
	printf("got %u, want : %u.\n", __tok->kind, _clg_tok_keywd);
	if (__tok->kind == _clg_tok_ident) {
		printf("got ident: %c.\n", *__tok->s);
	}
	assert(__tok->kind == _clg_tok_keywd);
	if (__tok->id == _clg_k_void) {
		*__ty = _int8;
	} else if (__tok->id == _clg_k_8u) {
		*__ty = _int8;
	} else if (__tok->id == _clg_k_16u) {
		*__ty = _int16;
	} else if (__tok->id == _clg_k_32u) {
		*__ty = _int32;
	} else if (__tok->id == _clg_k_64u) {
		*__ty = _int64;
	} else if (__tok->id == _clg_k_struct) {
		f_clg_tokenp tok;
		tok = clg_nexttok();
		*__ty = &((f_clg_symp)yc_hash_get(clg_struc.types,tok->s,tok->size))->ty0;
		assert(*__ty != NULL);
	}else{
		assert(1 == 0);
	}
}
_8_u static parser_stmt(void);
void static parser_expr(clg_wkstrcp);
void static parser_decl(void);

static struct f_clg_crude tlc = {.ty = NULL};

static struct f_clg_crude *tlc_;


void static
parser_compound_stmt(void) {
	f_clg_tokenp tok, tok0, tk;
	tok = clg_nexttok();
	if (!tok) {
		printf("compound null.\n");
		return;
	}
	assert((tok->sum&MUG(__clg_l_brace,__clg_l_brace))>0);


/*
	if we get

	void test() {}

	where
	() is whole
	and {} is whole

	so {} is its own token


	are first escape plan
*/
	if (tok->sum == MUG(__clg_l_brace,__clg_r_brace)) {	
		return;
	}

	tok0 = clg_nexttok();
	if (!tok0) {
		printf("compound null2, %u.\n", tok->sum);
		return;
	}
	/*
		second escape plan

		void test(_8_u a){}

					token-number
		void		-0
		test		-1
		(_8_u		-2
		a			-3
		){			-4
		}			-5
	*/
	if (((tok->sum&_RHS_mask)|(tok0->sum&_LHS_mask)) == MUG(__clg_r_brace,__clg_l_brace)){
		printf("second escape.\n");
//		tok0->flags &= ~_lhs_mask;
		tok0->sum &= ~__clg_r_brace;
		clg_ulex(tok0);
		return;
	}
	printf("working compound.\n");
	/*
		put next token(tok0) to tok
	*/
	tk = tok;
	if (tok->kind == _clg_tok_keywd0) {
		tok = tok0;
	}
	clg_ulex(tok0);
	if (tk->kind != _clg_tok_keywd0)
		clg_ulex(tk);

	printf("TOKCOMP: %u, %u.\n", tok->kind, tok->id);
	goto _j0;
_again:
	if (!(tok = clg_peektok())) {
		printf("NULL token at end of compound ????.\n");
		goto _done;
	}

/*

	could end with,
	}<- are one		LHS
					
	}<- other		RHS

	or.

	;<- other		LHS
	}<- are one		RHS

	using tok->sum as bit checking is faster
	and make it alot easier
*/

	/*
		if we get a right brace anywhere
	*/
	if (tok->sum&MUG(__clg_r_brace,__clg_r_brace)) {
		/*
			if we get a double }}
		*/
		if (tok->sum == MUG(__clg_r_brace,__clg_r_brace)) {
			/*
				removal of this is required as above will think there is a right brace where there is non
			*/
			tok->sum &= ~__clg_r_brace;
		/*
			if we get '}X'
			then make sure then token is visable by not consumeing it

			example issue

				while(1);	<- ");" is a whole token thus '}' is now on its self on the left hand side if this occurs then token must pass on to 
				what evers makeing use of the RHS of thinks?

				so '}void' when we read this token we need to get the VOID type
			}
			void test()
		*/
		} else if (tok->sum&_SR16(__clg_r_brace)){
			/*
				this means are brace is on the right hand side and means token can be consumed

				token is no longer with so downstream there will be no interaction with it
				ie.
					like the others removal of the 'right brace' is not strictly required
			*/
			clg_nexttok();	
		} else {
			/*
				example:

				if{
					if{
						if{
							i = 0;		token
						}				->;}
					}					->}'i' <- if '}' presence is not removed it will invoke a second compound exit down the line
					i = 0;				->;}
				}

			
			*/
			tok->sum &= ~__clg_r_brace;
		}

		goto _done;
	}
_j0:
	atbase = 0;
	tlc_ = &tlc;
		if (tok->id == _clg_k_struct) {
			parser_struct_decl();
	}else
	if (!is_type(tok)) {
		parser_decl();
	} else {
		if (parser_stmt()) {
			struct clg_wkstrc ws;
			parser_expr(&ws);
			tok = clg_peektok();
//			printf("OKOK: %u,%u, %u\n", tok->kind, tok->id, tok->flags);

			if (expectfix(__clg_semicolon) == -1){
				printf("failedure to expect semicolon.\n");
				goto _done;
			}
		} else {
			//all good		
		}
	}
	goto _again;
_done:
	printf("compound END, initiated by token: %p, %u, %u, %x\n", tok, tok->kind, tok->id, tok->sum);
	return;
}

struct dtr_strc {
	char *name;
	_int_u len;
};

void static
parser_decor(clg_typep *__ty, clg_typep __base) {
	clg_typep ty = __base;
	
_again:
	if (!clg_nexttokis(_clg_tok_keywd0, __clg_l_bracket)) {
		ty = newtype;
		ty->bits = 0;
		f_clg_tokenp tok;
		tok = clg_nexttok();


		struct cfm_vsp *v = m_alloc(sizeof(struct cfm_vsp));
		ty->v = v;
		*v = *__base->v;
		ty->len = _read_no(tok->s, tok->size);
		v->_u = v->_u*ty->len;
		if (clg_expecttok(_clg_tok_keywd0, __clg_r_bracket) == -1) {
			printf("parser_decor.\n");
		}
	} else {
		*__ty = ty;
		return;
	}
	goto _again;
}

void static
_declar(clg_typep *__ty, clg_typep __base) {
	clg_typep ty;
	ty = __base;
	f_clg_tokenp tok;
	tok = clg_nexttok();

	if (tok->sum&MUG(__clg_astrisk,__clg_astrisk)) {
		ty = newtype;
		*ty = *_int16;
		ty->bits = _clg_ptr;
	}
	*__ty = ty;
	clg_ulex(tok);
}

void static
parser_declarator(clg_typep *__ty, clg_typep __base, struct dtr_strc *__dt) {
	clg_typep ty;
	ty = __base;
	f_clg_tokenp tok;
	tok = clg_nexttok();

	if (tok->sum&MUG(__clg_astrisk,__clg_astrisk)) {
		ty = newtype;
		*ty = *_int16;
		ty->bits = _clg_ptr;
	}

	if (!tok) {

		return;
	}

	assert(tok->kind == _clg_tok_ident);
	__dt->name = tok->s;
	__dt->len = tok->size;
	parser_decor(__ty, ty);
}

void static
parser_decl_body(clg_typep ty, clg_typep base, struct dtr_strc dt) {
	f_clg_symp sy;
	f_cfm_fpop po;
	struct f_cfm_em *y;
_sametype:
	sy = new_symbol(dt.name, dt.len);
	f_clg_crudep d = &sy->d;
	y = &sy->s->y;
	y->saved = -1;
	y->wide = ty->v->_u;
	if (ty->bits&_clg_ptr) {
		y->bits = CFM_PTR;
	}
	d->vs = ty->v;
	d->v0 = base->v;
	if (clg_struc.bits&CLG_WIFS) {
		d->vs_ac = _cfm_ac_sym_fs;
		d->v0_ac = _cfm_ac_sym_fs;
	}else{
		d->vs_ac = _cfm_ac_sym_stry;
		d->v0_ac = _cfm_ac_sym_stry;
		y->sym = sy->s;
	}
	sy->ty = ty;
	printf("decl ptrtyp: %u: %u, typ: %u: %u\n", base->v->u, base->len, ty->v->u, ty->len);

	d->ty = ty;
	d->ty0 = base;
	d->y = y;
	if (!(clg_struc.bits&CLG_WIFS)) {
		po = new_cfm_fpo(curflock);
		po->op = _f_cfm_po_var;
		po->sy = sy->s;
		po->pi.size = ty->szdx;
		po->pi.len = ty->len;
	}
	yc_hash_put(clg_struc.tenv->hm, dt.name, dt.len, sy);

	_dlink(sy, _clg_dl_var);
	if (!clg_nexttokis(_clg_tok_keywd0, __clg_comma)) {
		printf("CAUGHT SAME TYPE VAR.\n");
		f_clg_tokenp tok;
		tok = clg_nexttok();
		dt.name = tok->s;
		dt.len = tok->size;
		goto _sametype;
	}

	if (expectfix(__clg_semicolon) == -1){
		printf("expeced semicolon at end of decl.\n");
	}
}

void
parser_decl(void) {
	clg_typep ty, base;
	ty = NULL;
	parser_decl_spec(clg_nexttok(), &ty);
	struct dtr_strc dt;
	base = ty;
	parser_declarator(&ty, ty, &dt);
	parser_decl_body(ty,base,dt);
}

void parser_sizeof_expr(clg_wkstrcp __wk) {

}

	
void parser_struct_decl(void) {
	f_clg_tokenp t0, t1, tok;
	t0 = clg_nexttok();
	t1 = clg_nexttok();
	tok = clg_nexttok();
	if (tok->kind == _clg_tok_ident) {
		clg_ulex(tok);
		clg_ulex(t1);
		clg_ulex(t0);
		parser_decl();
		return;
	}
	f_clg_symp s = m_alloc(sizeof(struct f_clg_sym)),sy;
	clg_typep strc = &s->ty0;
	strc->v = &strc->_v;
	strc->members = yc_hash_new();
	yc_hash_put(clg_struc.types, t1->s, t1->size,s);

	clg_typep ty,base;
	if (tok->sum == MUG(__clg_l_brace,__clg_r_brace)) {
		goto _end;
	}
	_64_u offset = 0;
	struct dtr_strc dt;
	struct f_cfm_em *y;
_again:
	sy = m_alloc(sizeof(struct f_clg_sym));
	y = &sy->s->y;
	parser_decl_spec(tok,&ty);
	base = ty;
	parser_declarator(&ty, ty, &dt);
	printf("STRUCTURE member : %s.\n",dt.name);
	/*
		here we are waiting for token match of ';}' this means we are at the end of the structure
	*/
	yc_hash_put(strc->members,dt.name,dt.len,sy);
	sy->ty0 = *ty;
	ty = &sy->ty0;
	ty->offset = offset;
	offset+=ty->v->u*ty->len;
	sy->d.ty = ty;
	sy->d.ty0 = base;
	sy->d.y = y;
	sy->d.vs = ty->v;
	sy->d.v0 = base->v;
	sy->d.vs_ac = _cfm_ac_sym_fs;
	sy->d.v0_ac = _cfm_ac_sym_fs;
	printf("SIZE: %u, %u.\n",ty->v->u,ty->v->_u);
	tok = clg_nexttok();
	if (tok->sum == MUG(0,__clg_semicolon))
		tok = clg_nexttok();
	if (tok->sum == MUG(__clg_semicolon,__clg_r_brace)) {
		goto _end;
	}
	goto _again;
_end:
	strc->_v.u = offset;
	strc->_v._u = offset;
	strc->len = 1;
	if (expectfix(__clg_semicolon) == -1){
		printf("expeced semicolon at end of struct decl.\n");
	}
	return;
}

void static
int_type(clg_wkstrcp __ws, clg_typep __ty, _64_u __int) {
	f_clg_crudep d;
	d = newcrude;
	d->s = NULL;
	__ws->d = d;
	d->ty = newtype;
	d->ty0 = d->ty;
	*d->ty = *__ty;

	f_cfm_emp y;
	d->y = y = f_cfm_em_new();
	d->vs = __ty->v;
	d->v0 = __ty->v;
	d->vs_ac = _cfm_ac_int;
	d->v0_ac = _cfm_ac_int;
	y->i = __int;
}

_64_u _read_no(char *__s, _int_u __len) {
	_64_u num;
	if (__len>1) {
		if (__s[1] == 'x') {
			num = _ffly_hxti(__s+2, __len-2);
			goto _sk;
		}
	}
	num = _ffly_dsn(__s, __len);
_sk:
	return num;
}
void static
read_no(clg_wkstrcp __ws, char *__s, _int_u __len) {
	_64_u num;
	clg_typep ty;
	num = _read_no(__s, __len);
	if (!(num&0xffffffffffffff00))
		ty = _int8;
	else if (!(num&0xffffffffffff0000))
		ty = _int16;
	else if (!(num&0xffffffffff000000))
		ty = _int32;
	else
		ty = _int64;
	int_type(__ws, ty, num);
}

static _8_s incase = -1;
void static
parser_primary_expr(clg_wkstrcp __ws) {
	f_clg_tokenp tok;
	tok = clg_nexttok();
	if (!tok) {
		return;
	}

	if (tok->sum&MUG(__clg_l_paren,__clg_l_paren)) {
		if (tok->kind != _clg_tok_keywd0)
			clg_ulex(tok);
		tok->sum &= ~MUG(__clg_l_paren,__clg_l_paren);
		printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX incasement.\n");
		incase = 0;
		_64_u flags = __ws->flags;
		parser_expr(__ws);
		__ws->flags |= flags;
		incase = -1;
		//incasement expression
		f_clg_tokenp tok;
		tok = clg_peektok();
		assert(tok->sum&MUG(__clg_r_paren,__clg_r_paren));
		tok->sum &= ~__clg_r_paren;
		/*
			we are assuming that we are not alone
			only case
			is the CAST expression

			x = (a);
			fun((x),x)
			never alone?
		*/
		printf("ENCASE end.\n");
		return;	
	}
	
	if (tok->kind == _clg_tok_ident) {
	
		char buf[24];
		mem_cpy(buf, tok->s, tok->size);
		buf[tok->size] = '\0';
		printf("attemting to locate '%s':%u in hash table-%p.\n", buf, tok->size,clg_struc.tenv->hm);
		ffly_fdrain(_ffly_out);
		f_clg_symp sy;
		sy = (f_clg_symp)yc_hash_get(clg_struc.tenv->hm, tok->s, tok->size);
			
		if (!sy) {
			/*
				if envirment is local then look in the global env for the thing were looking for
			*/
			f_printf("looking in globlal table.\n");
			if (!clg_struc.tenv->glob) {
				sy = (f_clg_symp)yc_hash_get(clg_struc.genv.hm, tok->s, tok->size);
				if (sy != NULL)
					goto _sk;
			}
			printf("error hash key not found.\n");
			assert(1 == 0);
			return;
		}
	_sk:
		printf("SUCCESS, %p\n", &sy->d);
		__ws->d = &sy->d;
		if (tok->other&_CLG_NUMBER)
			__ws->flags0 |= CLG_COMP;
		if (tok->sum&MUG(__clg_exclamation,__clg_exclamation))
			__ws->flags0 |= CLG_INV;
		printf("---FLAGS: %u.\n", __ws->flags0);
	} else if (tok->kind == _clg_tok_num) {
		assert(tok->kind == _clg_tok_num);
		read_no(__ws, tok->s, tok->size);
	}else
	if(tok->kind == _clg_tok_chr) {
		int_type(__ws, _int8, (_8_u)tok->s);	
	} else {
	_end:
		clg_ulex(tok);
		return;
	}
}

void static
parser_assign_shorthand(clg_wkstrcp __ws, clg_wkstrcp __wkon, _16_u __op) {
	struct f_clg_crude tlc0, *_tlc;
	_tlc = tlc_;
	tlc_ = &tlc0;
	struct clg_wkstrc ws;
	parser_expr(&ws);

	f_cfm_nodep n;
	n = m_alloc(sizeof(struct f_cfm_node));
	FUNCFOR(n,_f_cfm_bo0)
	n->op = __op;
	n->od = -1;
	struct cfm_props *p0 = CFM_PROPS0(n);
	struct cfm_props *p1 = CFM_PROPS1(n);
	p1->y = ws.d->y;
	p0->y = __wkon->d->y;
	p1->n = ws.n;
	p0->n = __wkon->n;
	p1->off = ws.off;
	p0->off = __wkon->off;
	p1->nc_off_n = ws.nc_off_n;
	p0->nc_off_n = __wkon->nc_off_n;
	p1->v = ws.d->vs;
	p0->v = __wkon->d->vs;
	p1->vac = ws.d->vs_ac;
	p0->vac = __wkon->d->vs_ac;
	p0->paw = 0;
	p1->paw = 0;

	__ws->n = n;
	__ws->nc_off = 0;
	__ws->nc_off_n = NULL;
	__ws->off = 0;
	__ws->flags = 0;
	_tlc->y = &n->y;

	*_tlc->y = *f_cfm_regm;
	_tlc->vs = p0->v;
	_tlc->v0 = p0->v;
	_tlc->vs_ac = _cfm_ac_reg;
	_tlc->v0_ac = _cfm_ac_reg;
	__ws->d = _tlc;
	__ws->paw = 0;
}

static _16_u ne_tab[] = {
	_f_cfm_add,
	_f_cfm_sub,
	_cfm_and,
	_cfm_or,
	_cfm_shl,
	_cfm_shr,
	_cfm_mul,
	_cfm_div
};

void static
parser_assign_expr(clg_wkstrcp __ws) {	
	if (__ws->d != NULL && incase == -1) {
		f_clg_crudep _d = __ws->d;
		f_clg_tokenp tok;
		struct f_clg_crude tlc0;
		struct clg_wkstrc ws;
		tok = clg_nexttok();
		f_cfm_nodep n;
	
		if (!tok) goto _sk;//stepover
		printf("ISASSIGNMENT: %x, %x.\n",tok->sum,tok->kind);
		if (tok->res&clg_isop && (tok->sum|_SR16(tok->sum0))&_SR16(__clg_equal)) {
			printf("SHORTHAND: RES : %u.\n", tok->res);	
			n = newnode();
			tlc_ = &tlc0;
			parser_assign_shorthand(&ws, __ws, ne_tab[(tok->res&clg_opmk)>>6]);	
			goto _j0;

		}else
		if (tok->sum&MUG(__clg_equal,__clg_equal) && tok->sum != MUG(__clg_equal,__clg_equal) && !(tok->sum&__clg_exclamation) && tok->sum != MUG(__clg_r_bracket,__clg_equal) &&
		/*
			if we get

			[sum: ')='] + [sum0: '=']
			if (*(X) == test) {

			}
		*/
		!(tok->sum0&__clg_equal)
		){
			printf("##########£££££.\n");
			/*
				we could get

				]=

				or just

				=

				or 

				=&
				=*
			*/
			if (tok->kind != _clg_tok_keywd0 || tok->sum&_SR16(__clg_ampersand) || tok->sum&_SR16(__clg_astrisk)) {
				clg_ulex(tok);
			}
		/*
			not removing this could invoke another assign expression
		*/
			tok->sum &= ~MUG(__clg_equal,__clg_equal);
			f_clg_tokenp tok;
			tok = clg_peektok();
			n = newnode();
			tlc_ = &tlc0;
			parser_expr(&ws);

			assert(ws.d != NULL);
		_j0:
			FUNCFOR(n,_cfm_agrer)

			struct cfm_props *p0,*p1;
			p0 = CFM_PROPS0(n);
			p1 = CFM_PROPS1(n);
			p0->bits = __ws->flags;
			p1->bits = ws.flags;
			p0->nc_off = __ws->nc_off;
			p1->nc_off = ws.nc_off;
			p0->nc_off_ac = __ws->nc_off_ac;
			p1->nc_off_ac = ws.nc_off_ac;

			p0->nc_off_n = __ws->nc_off_n;
			p1->nc_off_n = ws.nc_off_n;

			p1->off = ws.off;
			p0->y = _d->y;
			p0->off = __ws->off;
			p1->y = ws.d->y;

			p0->vac = __ws->d->vs_ac;
			p0->v = __ws->d->vs;
			
			p0->vac_ptr = __ws->d->v0_ac;
			p0->v_ptr = __ws->d->v0;

			p1->vac = ws.d->vs_ac;				
			p1->v = ws.d->vs;

			p1->vac_ptr = ws.d->v0_ac;
			p1->v_ptr = ws.d->v0;
		
			p0->n = __ws->n;
			p1->n = ws.n;
			p0->paw = __ws->paw;
			p1->paw = ws.paw;
			return;
		}
	_sk:
		clg_ulex(tok);
	}

}

void static
parser_numerical_expr(clg_wkstrcp __ws) {
	if (__ws->d != NULL) {
		f_clg_tokenp tok;
		tok = !__ws->tok?clg_peektok():__ws->tok;
		if (!tok)
			return;
		if (tok->sum&__clg_r_bracket)
			return;
		printf("NUMERICAL: %u, :%u : %x\n", (tok->res&clg_opmk)>>6, (tok->res&clg_isop)>0,tok->sum);
		/*
			if we have this expression 


			(10+10)+10
		
			tok->res is set if eather 

			)+
			or just 
			+

			so encasement expression wont break correctly
			and it starts encroaching on the part of the expression
			its not ment to.
		*/
		if (incase == -1) {
			/*
				if we get 

				func(func(100)+100);
			
				)+
			*/
			if (tok->sum&__clg_r_paren) {
				return;
			}
		}
		if ((tok->res&clg_isop)>0) {
			_16_u op;
			assert(((tok->res&clg_opmk)>>6)<8);
			op = ne_tab[(tok->res&clg_opmk)>>6];
			printf("OPERATION: %u.\n",op);

			ffly_fdrain(_ffly_out);
			/*
				we might get

				)+		= _clg_tok_keywd0

				or 

				+10		
			*/
			if (tok->kind == _clg_tok_keywd0){
				clg_nexttok();
			} else {
				tok->res = 0;
			}
			struct clg_wkstrc ws;
			struct f_clg_crude tlc0, *_tlc = tlc_;
			tlc_ = &tlc0;
			parser_expr(&ws);
			f_cfm_nodep n;
			n = m_alloc(sizeof(struct f_cfm_node));	
			_64_u u;
			u = __ws->d->vs->u;
			if (ws.d->vs->u>u)  {
				u = ws.d->vs->u;
			}
			if (u>1) {
				FUNCFOR(n,_cfm_bo)
			} else {
				FUNCFOR(n,_f_cfm_bo0)
			}
			assert(ws.d != NULL);
			assert(__ws->d != NULL);

			struct cfm_props *p0,*p1;
			p0 = CFM_PROPS0(n);
			p1 = CFM_PROPS1(n);

			n->op = op;
			n->od = -1;
			p1->n = ws.n;
			p0->n = __ws->n;
			p1->y = ws.d->y;
			p0->y = __ws->d->y;
			p0->off = __ws->off;
			p1->off = ws.off;
			p1->nc_off_n = ws.nc_off_n;
			p0->nc_off_n = __ws->nc_off_n;
			
			p1->vac = ws.d->vs_ac;
			p0->vac = __ws->d->vs_ac;
			p1->v = ws.d->vs;
			p0->v = __ws->d->vs;

			p0->vac_ptr = __ws->d->v0_ac;
			p0->v_ptr = __ws->d->v0;
			p1->vac_ptr = ws.d->v0_ac;
			p1->v_ptr = ws.d->v0;	
			p0->paw = __ws->paw;
			p1->paw = ws.paw;
			__ws->n = n;
			__ws->paw = 0;
			_tlc->y = &n->y;
			if(__ws->d->ty->bits&_clg_ptr) {
				*_tlc->y = *__ws->d->y;
				_tlc->vs = __ws->d->v0;
				_tlc->v0 = __ws->d->vs;
			}else if(ws.d->ty->bits&_clg_ptr) {
				*_tlc->y = *ws.d->y;
				_tlc->vs = ws.d->v0;
				_tlc->v0 = ws.d->vs;
			}else {
				*_tlc->y = *f_cfm_regm;
				if (__ws->d->vs->u>ws.d->vs->u) {
					_tlc->vs = __ws->d->vs;
				}else {
					_tlc->vs = ws.d->vs;			
				}
				_tlc->v0 = _tlc->vs;
			}
			_tlc->vs_ac = _cfm_ac_reg;
			_tlc->v0_ac = _cfm_ac_reg;
			static struct clg_type dummy = {.bits = 0};
			__ws->d = _tlc;
			__ws->d->ty  = &dummy;
			
		}
	}
}

void static
parser_goto(void) {
	f_clg_tokenp tok;
	tok = clg_nexttok();
	f_clg_symp sy;
	sy = (f_clg_symp)yc_hash_get(clg_struc.genv.hm, tok->s, tok->size);
	if (!sy) {
		sy = new_symbol(tok->s, tok->size);
		yc_hash_put(clg_struc.genv.hm, tok->s, tok->size, sy);
	}

	f_cfm_nodep n;
	n = new_cfm_node();
	FUNCFOR(n,_f_cfm_goto)
	n->sym = sy->s;
}

void static
parser_label(char const *__name, _int_u __len) {
	f_clg_symp sy;
	sy = (f_clg_symp)yc_hash_get(clg_struc.genv.hm, __name, __len);
	if (!sy) {
		sy = new_symbol(__name, __len);
		yc_hash_put(clg_struc.genv.hm, __name, __len, sy);
	}
	f_printf("----------------------- NEWFLOCK.\n");
	new_flock();
	f_cfm_fpop po;
	po = new_cfm_fpo(curflock);
	po->op = _f_cfm_po_ll;
	po->sy = sy->s;
	sy->s->s->local = 1;
	sy->s->s->ident = fc_struc.lid++;
}


/*
	a location to where we can jump to

	just call it a LABEL 
*/
_32_u static
genjtloc(void) {
	struct flock *f;
	f = new_flock();
	_32_u lid;
	f_cfm_symp sy;
	sy = f_cfm_sym_new(fc_sym_new(NULL, 0));
	sy->s->local = 1;
	sy->s->ident = lid = fc_struc.lid++;
	sy->flags = 0x00;
	f_cfm_fpop po;
	po = new_cfm_fpo(f);
	po->op = _f_cfm_po_jt;
	po->sy = sy;
	return lid;
}
static f_cfm_nodep trickle_list[8];
static _int_u tl_i;

void static
read_ifw_head(f_cfm_nodep n) {
	struct clg_wkstrc ws;
	f_clg_tokenp tok;
	tok = clg_peektok();
	if (!(tok->sum&MUG(__clg_l_paren,__clg_l_paren))) {
		printf("failed for if statment.\n");
	}else{
		if (!(tok->sum&__clg_l_paren)) {
			tok->sum &= ~_SR16(__clg_l_paren);
		}else
			tok->sum &= ~__clg_l_paren;
	}
	
	tl_i = 0;
	parser_expr(&ws);
	tok = clg_peektok();
	if (!(tok->sum&MUG(__clg_r_paren,__clg_r_paren))) {
		printf("failed for if statment.\n");
	}else{
		if (!(tok->sum&MUG(__clg_l_brace,__clg_l_brace))) {		
			clg_nexttok();
		}
	}

	mem_cpy(n->trickle,trickle_list,sizeof(f_cfm_nodep)*tl_i);
	n->nt = tl_i;
}
void static
parser_if_stmt(void) {	
	f_cfm_nodep n;
	n = newnode();
	read_ifw_head(n);

	FUNCFOR(n,_f_cfm_if)
	struct track *t_orig, *t;
	t_orig = curtrack;
	n->trk = &(t = new_track())->trk;
	curtrack = t;
	new_superflock();
	new_flock();
	
	parser_compound_stmt();
	
	curtrack = t_orig;
	new_flock();
	
	n->_else = NULL;
	if (!clg_nexttokis(_clg_tok_keywd, _clg_k_else)) {
		t_orig = curtrack;
		n->_else = &(t = new_track())->trk;
		curtrack = t;
		new_superflock();
		new_flock();
	
		parser_compound_stmt();
	
		curtrack = t_orig;
		new_flock();
	}
}

void static
parser_aso(void) {
	f_clg_tokenp tok;
	tok = clg_nexttok();
	new_flock();
	f_cfm_fpop po;
	po = new_cfm_fpo(curflock);
	po->op = _f_cfm_po_aso;
	po->pi.s = tok->s;
	po->pi.len = tok->size;
	
}

void static
parser_while_stmt(void) {
	new_flock();
	f_cfm_symp sy;
	sy = f_cfm_sym_new(fc_sym_new(NULL, 0));
	f_cfm_fpop po;
	po = new_cfm_fpo(curflock);
	po->op = _f_cfm_po_ll;
	po->sy = sy;
	sy->s->local = 1;
	sy->s->ident = fc_struc.lid++;
	curflock->here.jt = sy->s;	
	
	f_cfm_nodep n;
	n = newnode();
	read_ifw_head(n);
	n->sym = sy;
	FUNCFOR(n,_cfm_while)
	n->trk = NULL;
	f_clg_tokenp tok;
	tok = clg_peektok();
	if (tok->sum&_SR16(__clg_l_brace)) {
		struct track *t_orig, *t;
		t_orig = curtrack;
		n->trk = &(t = new_track())->trk;
		curtrack = t;
		new_superflock();
		new_flock();
		parser_compound_stmt();
   		curtrack = t_orig;
   		new_flock();
	} else {
		clg_nexttok();
	}
}

void static
parser_ret_stmt(void) {
	struct clg_wkstrc w;
	parser_expr(&w);
	if (!w.d) {
		printf("return statment has not argument or somthing has gone terribly wrong.\n");
	}
	assert(w.d != NULL);
	struct flock *f;
	f = curflock;
	f->end = stf->ret;
	f->ret.y = w.d->y;
	f->ret.v = w.d->vs;
	f->ret.vac = w.d->vs_ac;
	new_flock();
}

_8_u
parser_stmt(void) {
	f_clg_tokenp tok;
	tok = clg_nexttok();
	printf("statment checking: %u, %u.\n", tok->kind, tok->id);
	if (tok->kind == _clg_tok_keywd) {
		if (tok->id == _clg_k_goto) {
			parser_goto();
		}else
		if (tok->id == _clg_k_aso) {
			parser_aso();
		}else
		if (tok->id == _clg_k_if) {
			printf("working if statment.\n");
			parser_if_stmt();
		}else
		if (tok->id == _clg_k_while) {
			parser_while_stmt();	
		}else
		if (tok->id == _clg_k_for) {
	
		}else
		if (tok->id == _clg_k_switch) {
	
		}else
		if (tok->id == _clg_k_ret){
			parser_ret_stmt();
		}else{
			printf("found nothing.\n");
			goto _j0;
		}
	} else if (tok->kind == _clg_tok_ident && tok->other&_CLG_COLON) {
		parser_label(tok->s, tok->size);
	} else {
	_j0:
		clg_ulex(tok);
		return 1;
	}
	return 0;
}


void static
parser_equality_expr(clg_wkstrcp __ws) {
	f_clg_tokenp tok;
	tok = clg_peektok();

	if (!tok)
		return;
	if (tok->sum>0 && incase == -1) {
		f_cfm_nodep n;
		_8_u op;
		struct clg_wkstrc ws;
		f_clg_crudep d;
		_64_u sum = tok->sum;
		if (tok->sum&_SR16(__clg_equal|__clg_exclamation) && !(tok->sum&__clg_equal) && tok->sum0>0) {
			sum>>=32;
			sum |= tok->sum0<<32;
		}

		switch(sum) {
			case MUG(__clg_equal,__clg_equal):
				op = _f_cfm_eqt;
			break;
			case MUG(__clg_exclamation,__clg_equal):
				op = _f_cfm_net;
			break;
			default:
				if(tok->sum&MUG(__clg_gt,__clg_gt)) {
					op = _cfm_gt;
				} else if (tok->sum&MUG(__clg_lt,__clg_lt)) {
					op = _cfm_lt;
				}else {
					goto _sk;
				}
		}
		printf("equality operation.\n");
		/*
			if (a == 0) {

			}


			== # counts as its own token
		*/
		if (tok->kind == _clg_tok_keywd0)
			clg_nexttok();
		else {
			/*
				we dont want it steping over itself

				when a EXPR is called to read the RHS its going to come back to this function
				TBH: we are better setting prevetative bits and not clearing sum
			*/
			tok->sum = 0;
		}
	
		n = m_alloc(sizeof(struct f_cfm_node));
		trickle_list[tl_i++] = n;
		n->f = stf->func[_cfm_eqo];
		n->op = op;
		struct f_clg_crude tlc0, *_tlc = tlc_;
		tlc_ = &tlc0;
		parser_expr(&ws);

		assert(ws.d != NULL);
		assert(__ws->d != NULL);
		struct cfm_props *p0,*p1;
		p0 = CFM_PROPS0(n);
		p1 = CFM_PROPS1(n);


		p1->y = ws.d->y;
		p0->y = __ws->d->y;
		p0->bits = __ws->flags;
		p1->bits = ws.flags;
		p0->off = __ws->off;
		p1->off = ws.off;
		p0->n = __ws->n;
		p1->n = ws.n;
		p0->v = __ws->d->vs;
		p1->v = ws.d->vs;
		p0->vac = __ws->d->vs_ac;
		p1->vac = ws.d->vs_ac;
		n->od = F_CFM_COND;
		n->gub = f_cfm_regm;
		__ws->d = _tlc;
		_tlc->y = f_cfm_regm;
		__ws->n = n;
		tok = clg_peektok();
		struct f_clg_crude tlc1;
		if (tok->sum == MUG(__clg_ampersand,__clg_ampersand)) {
			/*
				TODO:
					allow for 
	
					&&x dual tokens
			*/
			clg_nexttok();
			tlc_ = &tlc1;
			parser_primary_expr(__ws);
			parser_equality_expr(__ws);
		}


		return;
	}
_sk:
	return;
}

void static
parser_func_call(clg_wkstrcp __ws) {
	assert(__ws->d != NULL);
	yc_vecp params;
	struct f_clg_crude *_tlc = tlc_;
	_int_u i;	
	params = yc_vec_new(sizeof(f_cfm_nodep));
	f_clg_tokenp tok;
	_64_u orig = within_func_call;
	within_func_call = 0;
	tok = clg_nexttok();
	f_cfm_nodep n;
	n = newnode();

	if (tok->sum != MUG(__clg_l_paren,__clg_r_paren)) {
		assert(tok->sum&MUG(__clg_l_paren,__clg_l_paren));
		i = 0;
		clg_ulex(tok);
		/*
			we dont want EXPR invoking another call to this same function
		*/
		tok->sum &= ~_LHS_mask;

		struct clg_wkstrc ws;
		f_cfm_nodep param;
		struct f_clg_crude tlc0;
		void *pp;
_again:		
		tlc_ = &tlc0;
		parser_expr(&ws);
		param = ws.n;
		i++;
		if (!ws.n) {
			param = m_alloc(sizeof(struct f_cfm_node));

			struct cfm_rake *r = param;
			struct cfm_props *p0,*p1;
			p0 = CFM_PROPS0(r);
			p1 = CFM_PROPS1(r);


			p0->nc_off = 0;
			p1->nc_off = 0;
			p0->nc_off_n = NULL;
			p1->nc_off_n = NULL;
			p0->n = NULL;
			p1->n = NULL;
			p0->off = 0;
			p1->off = 0;
			p0->bits = 0;
			p1->bits = ws.flags;
			p1->y = ws.d->y;
			p1->v = ws.d->vs;
			p1->vac = ws.d->vs_ac;
			param->f0 = stf->func[_cfm_agrer];
		}else{
			param->f0 = stf->func[_cfm_func_call_arg_func_call];
		}
		assert(param != NULL);
		yc_vec_push(params, (void*)&pp);
		*(void**)pp = param;
		if (!clg_nexttokis(_clg_tok_keywd0, __clg_comma)) {
			goto _again;
		}

		if (!clg_expecttok(_clg_tok_keywd0, __clg_r_paren)) {

		}
	}
	within_func_call = orig;
//	while(1);

	__ws->n = n;
	_int_u vsz;
	vsz = yc_vecsize(params);
	struct f_cfm_node *_params[7];
	if (vsz>0) {
		_int_u i;
		i = 0;
		for(;i != vsz;i++) {
			f_cfm_nodep param;
			param = *(f_cfm_nodep*)yc_vec_getat(params, i);
			
			_params[i] = param;
		}
	}
	f_clg_symp s;
	s = __ws->d->s;

	assert(s != NULL);

	FUNCFOR(n,_f_cfm_func_call)

	n->sym = s->s;
	f_printf("))))))))))))))))))))))) %p.\n", s->dw);
	n->func = s->dw;
	n->n_par = vsz;
	assert(sizeof(n->params)>=7*sizeof(f_cfm_nodep));
	mem_cpy(n->params, _params, 7*sizeof(f_cfm_nodep));
	__ws->d = _tlc;
	_tlc->y = f_cfm_regm;
	assert(s->d.ty != NULL);
	_tlc->ty = s->d.ty;
	_tlc->vs = s->d.ty->v;
	_tlc->v0 = _tlc->vs;
	_tlc->vs_ac = _cfm_ac_reg;
	_tlc->v0_ac = _cfm_ac_reg;
	__ws->paw = PAWFOR(CFM_PAW_FUNCC);
}

static struct f_cfm_em ss_em = {.saved = -1,.bits = 0};
void static
parser_subscript_expr(clg_wkstrcp __ws) {
	struct clg_wkstrc w;
	_8_s orig = atbase;
	atbase = -1;
	struct f_clg_crude tlc0, *origtlc;
	origtlc = tlc_;
	tlc_ = &tlc0;
	parser_expr(&w);
	tlc_ = origtlc;
	atbase = orig;

	/*
		if we get somthing like a symbol then this...
	*/
	if (!w.n) {
		if (w.d->vs_ac == _cfm_ac_int) {
			__ws->off = w.d->y->i*__ws->d->ty->v->u;
		}else{
			f_cfm_nodep n;
			n = m_alloc(sizeof(struct f_cfm_node));
			n->f = stf->func[_cfm_add_off];
			struct cfm_props *p = CFM_PROPS0(n);
			p->nc_off = w.d->y;			
			p->nc_off_ac = w.d->vs_ac;
			p->nc_off_v = w.d->ty->v;
			__ws->nc_off_n = n;
		}
	}else{//if we get a secondary operation	like a binary-op,etc
		f_cfm_nodep n;
		n = m_alloc(sizeof(struct f_cfm_node));
		n->f = stf->func[_cfm_add_off_with];
		
		struct cfm_props *p = CFM_PROPS0(n);

		p->nc_off = w.d->y;
		p->nc_off_ac = w.d->vs_ac;
		p->nc_off_v = w.d->ty->v;
		n->rake.with = w.n;
		__ws->nc_off_n = n;
		
	}
//	ss_em.vs = __ws->d->y->vs;
//	ss_em.v0 = __ws->d->y->v0;
//	__ws->d= &tlc;
//	tlc.y = &ss_em;
	if (__ws->d->vs_ac == _cfm_ac_reg) {
		__ws->flags |= CFM_DEREF;
	} else {

	if (__ws->d->ty->bits&_clg_ptr) {
		__ws->flags |= CFM_DEREF;
	} 
	}

	if(clg_expecttok(_clg_tok_keywd0, __clg_r_bracket) == -1) {
		printf("expected rhs bracket.\n");
	}
}

void static
struct_field(clg_wkstrcp __ws, f_clg_tokenp __field) {
	clg_typep t = __ws->d->ty0;
	struct f_clg_crude *_tlc = tlc_;
	f_clg_symp field;
	field = yc_hash_get(t->members,__field->s,__field->size);
	f_clg_tokenp tok;
	tok = clg_peektok();
	struct clg_wkstrc ws;
	ws.d = &field->d;
	ws.off = 0;
	ws.nc_off_n = NULL;
	if (tok->sum&MUG(__clg_l_bracket,__clg_l_bracket)) {
		tok->sum &= ~MUG(__clg_l_bracket,__clg_l_bracket);
		parser_subscript_expr(&ws);
		__ws->nc_off_n = ws.nc_off_n;
	}
	
	assert(field != NULL);
	*_tlc = *__ws->d;
	__ws->off = field->d.ty->offset+ws.off;
	_tlc->vs = field->d.vs;
	_tlc->v0 = field->d.v0;
	__ws->d = _tlc;
}

void static
parser_postfix_expr(clg_wkstrcp __ws) {
	f_clg_tokenp tok;
	tok = clg_peektok();
	if (!tok)
		return;
	if (tok->sum&MUG(__clg_l_bracket,__clg_l_bracket) && !(tok->sum&__clg_r_paren)) {
		printf("SUBSCRIPT.\n");
		if (tok->sum&_SR16(__clg_l_bracket)) {
			clg_nexttok();
		}
		tok->sum &= ~MUG(__clg_l_bracket,__clg_l_bracket);
		parser_subscript_expr(__ws);
	}

	if (tok->sum == MUG(__clg_minus,__clg_gt)) {
		clg_nexttok();
		struct_field(__ws,clg_nexttok());
		__ws->flags |= CFM_DEREF;
	}
	switch(tok->sum&_LHS_mask) {
		case __clg_l_paren:
			parser_func_call(__ws);
		break;
		case __clg_dot:
			clg_nexttok();
			tok->sum &= ~__clg_dot;
			struct_field(__ws,tok);
		break;
		default:
			goto _sk;
	}
	return;
_sk:
	printf("no postfix.\n");
	return;
}
/*
	aso is in a complete form 
	
	it read all the token all the way to a
	
	;

	but! as of theway we are working with tokens others dont know about this
	as they think we have the next expression

	;a = 0;

	so it thinks we are doing another assignment as it ignores the ';' part

	in some cases the linear nature of this is used but with an ASO expression
	we read everthing fron start to finish with out using it.

	EXAMPLE

	a = 0;

	first primary_expr is used to fetch the 'a'
	then we use assign_expr for the assignment
	all in one parser_expr call
*/
static struct cfm_vsp aso_vs = {.u = 1};
_8_s static
parser_aso_expr(clg_wkstrcp __ws) {
	if(!clg_nexttokis(_clg_tok_keywd, _clg_k_aso)) {
		f_clg_tokenp tok;
		tok = clg_nexttok();
		printf("ASOUT: %u:%c.\n", tok->size, *tok->s);
		static struct f_clg_crude tmp;
		mem_set(&tmp,0,sizeof(struct f_clg_crude));
		tmp.y = f_cfm_em_new();
		tmp.vs = &aso_vs;
		tmp.v0 = tmp.vs;
		tmp.y->bits = CFM_EMB_INJECT;
		tmp.y->str.s = tok->s;
		tmp.y->str.len = tok->size;
		__ws->d = &tmp;
		return 0;
	}
	return -1;
}

void static
parser_bittests(clg_wkstrcp __ws) {
	if (__ws->flags0&CLG_COMP) {
		f_clg_tokenp tok;
		tok = clg_nexttok();
		if (!tok)
			return;
		if (tok->kind == _clg_tok_num) {
			f_cfm_nodep n;
			n = m_alloc(sizeof(struct f_cfm_node));
			FUNCFOR(n,_cfm_bittests)
			struct cfm_props *p = CFM_PROPS0(n);
			p->y = __ws->d->y;
			p->vac = __ws->d->vs_ac;
			p->off = __ws->off;
			n->_0 = _read_no(tok->s, tok->size); 
			printf("BIT TESTING.\n");
			if (__ws->flags0&CLG_INV) {
				n->a = stf->atab[0];
			}else{
				n->a = stf->atab[1];
			}
			trickle_list[tl_i++] = n;
			return;
		}
		clg_ulex(tok);
	}
}

void static
parser_unary_expr(clg_wkstrcp __ws) {
	f_clg_tokenp tok;
	tok = clg_peektok();
	if (tok->sum&MUG(__clg_ampersand,__clg_ampersand)) {
		if (tok->sum&_SR16(__clg_ampersand))
			clg_nexttok();
		__ws->flags |= CFM_ADDROF;
	}
}
void
parser_expr(clg_wkstrcp __ws) {
	__ws->nc_off = NULL;
	__ws->nc_dis = NULL;
	__ws->nc_off_n = NULL;
	__ws->flags = 0;
	__ws->flags0 = 0;
	__ws->n = NULL;
	__ws->d = NULL;
	__ws->off = 0;
	__ws->paw = 0;
	__ws->dis = 0;
	__ws->tok = NULL;
	f_clg_tokenp tok;
	tok = clg_peektok();
	if (!tok)
		return;
	/*
		we dont know yet if this realy is a deref but!
		we assume it is until known otherwise
	*/

	if (tok->sum&MUG(__clg_astrisk,__clg_astrisk)) {
		if (tok->sum&_SR16(__clg_astrisk)) {
			clg_nexttok();
		}
		__ws->flags |= CFM_DEREF;
		__ws->tok = tok;
	}

	if (!parser_aso_expr(__ws))
		return;
	parser_unary_expr(__ws);
	parser_primary_expr(__ws);
	parser_bittests(__ws);

	parser_postfix_expr(__ws);

	parser_assign_expr(__ws);

	parser_numerical_expr(__ws);
	parser_equality_expr(__ws);
}

void static
parser_func_def(void) {
	f_clg_symp sy;
	f_cfm_nodep n;
	n = new_cfm_node();
	func_fils[nff++] = n;
	yc_hashp hm;
	hm = yc_hash_new();
	printf("FUNCTION-HASHMAP: %p.\n",hm);
	clg_typep ty;
	parser_decl_spec(clg_nexttok(), &ty);
	struct dtr_strc dt;
	parser_declarator(&ty, ty, &dt);	
	void *syp;
	syp = yc_hash_get(clg_struc.genv.hm, dt.name, dt.len);
	f_cfm_dwelp d;
	if (!syp) {
		sy = new_symbol(dt.name, dt.len);
		f_printf("SYMBOL_FUNCTION: %u, %c --> %p\n", dt.len, dt.name[0], clg_struc.tenv);
		n->d = (f_cfm_dwelp)m_alloc(sizeof(struct f_cfm_dwel));
		sy->dw = n->d;
		sy->dw->u = ty->v->u;
		sy->d.s = sy;
		sy->d.ty = ty;
		f_printf("SYMB_FUNCTION: DW: %p.\n", sy->dw);


		yc_hash_put(clg_struc.genv.hm, dt.name, dt.len, sy);
	} else {
		sy = syp;
		/*
			remove extern linkage
		*/
		clg_struc.ur_cnt--;
		*sy->ur_bk = sy->ur;
		if (sy->ur != NULL)
			sy->ur->ur_bk = sy->ur_bk;
		n->d = sy->dw;
	}
	d = n->d;
	
	/*
		add global linkage
	*/
	clg_struc.gb_cnt++;
	sy->ur_bk = &clg_struc.gb;
	sy->ur = clg_struc.gb;
	if (clg_struc.gb != NULL)
		clg_struc.gb->ur_bk = &sy->ur;
	clg_struc.gb = sy;

	sy->s->flags = CFM_SY_FUNC;
	n->sym = sy->s;

	f_clg_tokenp tk0;
	tk0 = clg_nexttok();
	/*
		in this case we have no arguments
	*/
	f_cfm_emp arg_ems;
	_int_u nargs = 0;
	if (tk0->sum == MUG(__clg_l_paren,__clg_r_paren)) {
		if (tk0->kind != _clg_tok_keywd0) {
			clg_ulex(tk0);
		}
		goto _j0;
	} else
		clg_ulex(tk0);
	/*
		this is a wast of memory but better.
	*/
	arg_ems = m_alloc(sizeof(struct f_cfm_em)*16);
_again:
	if (clg_nexttokis(_clg_tok_keywd0, __clg_r_paren) == -1) {
		struct f_cfm_em *y;
		f_clg_symp sy;
		clg_typep ty,base;
		parser_decl_spec(clg_nexttok(), &ty);
		base = ty;
		struct dtr_strc dt;
		parser_declarator(&ty, ty, &dt);
	_sametype:
		sy = new_symbol(dt.name, dt.len);
		y = arg_ems+nargs++;
		f_clg_crudep d = &sy->d;
		d->y = y;
		*y = cfm_eminit;
		y->wide = ty->v->_u;
		y->bits = 0;
		d->vs_ac = _cfm_ac_sym_fs;
		d->v0_ac = _cfm_ac_sym_fs;
		d->vs = ty->v;
		d->v0 = base->v; 
		d->ty = ty;
		d->ty0 = base;
		yc_hash_put(hm, dt.name, dt.len, sy);
		if (!clg_nexttokis(_clg_tok_keywd0, __clg_quote)) {
			printf("CAUGHT SAME TYPE VAR.\n");
			f_clg_tokenp tok;
			tok = clg_nexttok();
			dt.name = tok->s;
			dt.len = tok->size;
			goto _sametype;
		}
		goto _again;
	}
_j0:
{
	d->args = arg_ems;
	d->n_args = nargs;
	struct f_clg_env env;
	struct f_clg_env *orig;
	orig = clg_struc.tenv;
	clg_struc.tenv = &env;
   	env.hm = hm;
	env.ns = 0;
	env.dl[0] = yc_vec_new(sizeof(void*));
	env.glob = 0;

	struct track *t_orig, *t;
	t_orig = curtrack;
	n->trk = &(t = new_track())->trk;
	curtrack = t;
	new_superflock();
	new_flock();
	clg_struc.bits |= CLG_WIFS;
	parser_compound_stmt();
	clg_struc.bits ^= CLG_WIFS;
	curtrack = t_orig;
	clg_struc.tenv = &env;

	f_cfm_blockp cfm_blk;
	_8_u *p;
	p = (_8_u*)m_alloc(sizeof(struct f_cfm_block)+(sizeof(f_cfm_emp)*env.ns));
	cfm_blk = (f_cfm_blockp)p;
	cfm_blk->y = (f_cfm_emp)(p+sizeof(struct f_cfm_block));

	f_clg_symp cur;
	_int_u i = 0;
	yc_vecp vars;
	vars = env.dl[_clg_dl_var];
	_int_u nv;
	nv = yc_vecsize(vars);

	while(i != nv) {
		cur = *(f_clg_symp*)yc_vec_getat(vars, i);
		f_cfm_emp y;
		y = (*(cfm_blk->y+i) = &cur->s->y);
		cur = cur->next;
		i++;
	}
	n->d->n = n;
	FUNCFOR(n,_f_cfm_func_def);
	cfm_blk->n = i;
	n->block = cfm_blk;
	new_flock();	
}
}

void static
parser_func_decl(void) {
	clg_typep ty;
	parser_decl_spec(clg_nexttok(), &ty);

	f_clg_tokenp name;
	name = clg_nexttok();

	f_cfm_emp arg_ems;
	_int_u nargs = 0;
	f_cfm_emp y;
	arg_ems = m_alloc(sizeof(struct f_cfm_em)*16);

	f_clg_symp sy;
	sy = new_symbol(name->s, name->size);
	clg_struc.ur_cnt++;
	sy->ur = clg_struc.ur;
	sy->ur_bk = &clg_struc.ur;
	if (clg_struc.ur != NULL)
		clg_struc.ur->ur_bk = &sy->ur;
	clg_struc.ur = sy;
	f_cfm_dwelp d;
	d = (f_cfm_dwelp)m_alloc(sizeof(struct f_cfm_dwel));
	d->n_args = 0;
	d->mass = 0;
	d->size = 0;
	sy->dw = d;
	sy->dw->u = ty->v->u;
	sy->d.s = sy;
	sy->d.ty = ty;
	printf("SYMB_FUNCTION: DW: %p.\n", sy->dw);
	
	f_clg_tokenp tok;
	if (
		(tok = clg_nexttok())->sum != MUG(__clg_l_paren,__clg_r_paren)
	) {
		clg_ulex(tok);
		if (!clg_expecttok(_clg_tok_keywd0, __clg_l_paren)) {
		}
		if (clg_nexttokis(_clg_tok_keywd0, __clg_r_paren) == -1) {
			clg_typep ty,base;
		_again:
			parser_decl_spec(clg_nexttok(), &ty);
			base = ty;
			struct dtr_strc dt;
			_declar(&ty, ty);
			y = arg_ems+nargs++;
			y->wide = ty->v->_u;
			printf("ARG%u: %u-wide.\n",nargs-1,y->wide);
			d->size+=y->wide;
			tok = clg_nexttok();
			if (tok->sum&__clg_comma) {
				clg_ulex(tok);
				goto _again;
			}
			if (tok->sum&_SR16(__clg_comma)) {
				goto _again;
			}

			if (tok->sum&__clg_r_paren){
				clg_ulex(tok);
			}
		}
	} else
		clg_ulex(tok);

	d->args = arg_ems;
	d->n_args = nargs;
	yc_hash_put(clg_struc.genv.hm, name->s, name->size, sy);
	clg_expecttok(_clg_tok_keywd0, __clg_semicolon);
}
void clg_parser_parse(void) {
	f_clg_tokenp tok;
	while((tok = clg_peektok()) != NULL) {
		atbase = 0;
		tlc_ = &tlc;
		//if (!tok->eof) break;
		_8_u r;
		if (tok->id == _clg_k_struct && tok->kind == _clg_tok_keywd) { 
			parser_struct_decl();	
		}else	
		if (!(r = is_func())) {
			parser_func_def();
		}else
		if (r == 1) {
			parser_func_decl();
		}else{
			if (!is_type(tok)) {
				parser_decl();
			} else {
				if (tok->kind == _clg_tok_ident && tok->other == _CLG_NUMBER) {
					tok = clg_nexttok();
					f_clg_tokenp tok0;
					tok0 = clg_nexttok();
					f_clg_symp sy;
					printf(">>## %s.\n",tok->s);
					struct f_cfm_em *y;
					sy = new_symbol(tok->s, tok->size);
					sy->s->flags |= CFM_SY_IMM;
					sy->d.flags |= CLG_CRDF_COMP;
					yc_hash_put(clg_struc.genv.hm, tok->s, tok->size, sy);
					y = &sy->s->y;
					*y = cfm_eminit;
					sy->d.vs_ac = _cfm_ac_enl;
					sy->d.v0_ac = _cfm_ac_enl;
					y->sym = sy->s;
					sy->d.vs = &vs_in8;
					sy->d.v0 = &vs_in8;
					sy->d.ty = &sy->ty0;
					sy->d.ty0 = sy->d.ty;
					sy->ty0.bits = 0;
					sy->ty0.v = &vs_in8;
					y->i = _read_no(tok0->s, tok0->size);
					sy->d.y = y;
				
					/*new_flock();
					f_cfm_fpop po;
					po = new_cfm_fpo(curflock);
					po->op = _f_cfm_po_def;
					po->n = tok->s;
					po->nlen = tok->size;
					po->v = tok0->s;
					po->vlen = tok0->size;
			*/
				}else {
				struct clg_wkstrc ws;
				printf("FINALS.\n");
				ws.d = NULL;
				ws.flags = 0x00;
				parser_expr(&ws);
				clg_expecttok(_clg_tok_keywd0, __clg_semicolon);
				
				}
			}
		}
	}
}

void f_clg_parser(void) {
	stf = cfm_stf+1;
	na.nodes = (struct f_cfm_node**)m_alloc(sizeof(struct f_cfm_node*));
	*na.nodes = (struct f_cfm_node*)m_alloc(NA_PAGE_SIZE*sizeof(struct f_cfm_node));
	na.page_c = 1;
	na.off = 0;
	curtrack = new_track();
	t_head = curtrack;
	t_tail = curtrack;
	new_superflock();
	new_flock();
	f_cfm_init(cfm_st+1);
	assert(PAWFOR(CFM_PAW_FUNCC) == 1);
	clg_parser_parse();
	t_tail->next = NULL;

	struct superflock *sfck;
	struct flock *fck;
	struct track *t;
	t = t_head;
	while(t != NULL) {
		printf("TRACK, i: %u\n", t->i);
		f_cfm_flockp f;
		f_cfm_superflockp sf;
		if (t->i>0) {
			f_cfm_flockp _f;
			f_cfm_superflockp _sf;
			sf = (f_cfm_superflockp)m_alloc(t->i*sizeof(struct f_cfm_superflock));

			sfck = t->link;
			while(sfck != NULL) {
				printf("SUPERFLOCK, placment: %u, n: %u\n", sfck->placment, sfck->n);
				_sf = sf+sfck->placment;
				if (sfck->n>0) {
					f_printf("flock N #### %u.\n", sfck->n);
					f = (f_cfm_flockp)m_alloc(sfck->n*sizeof(struct f_cfm_flock));
					fck = sfck->link;
					while(fck != NULL) {
						printf("FLOCK, placment: %u, n: %u\n", fck->placment, fck->n);
						_f = f+fck->placment;
						_f->po = fck->po;
						_f->here = fck->here;
						_f->n = na.nodes+(fck->off>>NA_PAGE_SHFT);
						_f->pg_off = fck->off&NA_PAGE_MASK;
						_f->nn = fck->n;
						_f->end = fck->end;
						_f->ret = fck->ret;
						printf("NN: %u, POF: %u, PG: %u.\n", fck->n, _f->pg_off, fck->off>>NA_PAGE_SHFT);
						fck = fck->next;
					}
				} else
					f = NULL;
				_sf->fck = f;
				_sf->n = sfck->n;
				sfck = sfck->next;
			}
		} else
			sf = NULL;
		t->trk.sf = sf;
		t->trk.n = t->i;
		t = t->next;
	}
	cfm_struc.func_fils = func_fils;
	cfm_struc.nff = nff;
	struct f_clg_sym *sy;
	void **esyt;
	esyt = m_alloc(sizeof(void*)*clg_struc.ur_cnt);
	sy = clg_struc.ur;
	_int_u i = 0;
	while(sy != NULL) {
		esyt[i++] = sy->s->s;
		sy = sy->ur;
	}

	sy = clg_struc.gb;
	i = 0;
	void **gsyt;
	gsyt = m_alloc(sizeof(void*)*clg_struc.gb_cnt);
	while(sy != NULL) {
		gsyt[i++] = sy->s->s;
		sy = sy->ur;
	}

	cfm_init(&t_head->trk,cfm_st+1);
	cfm_extern(esyt,clg_struc.ur_cnt);
	cfm_global(gsyt,clg_struc.gb_cnt);
	i = 0;
	yc_vecp v = clg_struc.genv.dl[_clg_dl_var];
	while(i != yc_vecsize(v)) {
		void **p = yc_vec_getat(v,i);
		f_clg_symp s = *p;
		void *x[] = {s->s};
		cfm_var(x,1);
		i++;
	}
	cfm_process();
}
