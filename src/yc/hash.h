# ifndef __yc__hash__h
# define __yc__hash__h
# include "../y_int.h"
#define FC_HT_SHIFT 8
#define FC_HT_SIZE (1<<FC_HT_SHIFT)
#define FC_HT_MASK (FC_HT_SIZE-1)
struct yc_hash_entry;
typedef struct yc_hash {
	struct yc_hash_entry **table;
	struct yc_hash_entry *top;
} *yc_hashp;

typedef struct yc_hash_entry {
	struct yc_hash_entry *next, *link;
	_8_u const *key;
	_int_u len;
	void *p;
} *yc_hash_entp;

yc_hashp yc_hash_new(void);
void yc_hash_destroy(yc_hashp);
void yc_hash_put(yc_hashp, _8_u const*, _int_u, void*);
void* yc_hash_get(yc_hashp, _8_u const*, _int_u);
# endif /*__yc__hash__h*/
