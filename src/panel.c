# include "panel.h"
# include "system/pipe.h"
# include "system/io.h"
_f_err_t static
sndop(_8_u __op, _int_u __pipe) {
	_f_err_t ret;
	ret = ffly_pipe_wr8l(__op, __pipe);
	return ret;
}

_int_u static pipe;
void ff_p_connect(_int_u __shm_id) {
	_f_err_t err;
	pipe = ffly_pipe(8, FF_PIPE_SHMM, __shm_id, &err);
	ffly_pipe_connect(pipe);
}

void ff_p_disconnect(void) {
	sndop(_ffly_po_disconnect, pipe);
	ffly_pipe_close(pipe);
}

void ff_p_meminfo(struct ffly_meminfo *__info) {
	sndop(_ffly_po_meminfo, pipe);
	ffly_pipe_read(__info, sizeof(struct ffly_meminfo), pipe);
}

_8_u static
rcvop(_int_u __pipe, _f_err_t *__err){
	_8_u op;
	op = ffly_pipe_rd8l(__pipe, __err);
	return op;
}

void _disconnect();
void _halt();
void _resume();
void _meminfo();
void *op_tbl[] = {
	_disconnect,
	_halt,
	_resume,
	_meminfo
};

# define jmpagain \
	__asm__("jmp _again")
# define jmpend \
	__asm__("jmp _end")
void ffly_panel_start(void) {
	_f_err_t err;
	pipe = ffly_pipe(8, FF_PIPE_CREAT, 0, &err);
	printf("shmid: %u\n", ffly_pipe_get_shmid(pipe));
	ffly_fdrain(_ffly_out);
	ffly_pipe_listen(pipe);

	_8_u op;
	__asm__("_again:\n\t");
	op = rcvop(pipe, &err);
	__asm__("jmp *%0": : "r"(op_tbl[op]));
	
	__asm__("_halt:\n\t");
		
	jmpagain;
	__asm__("_resume:\n\t");

	jmpagain;
	__asm__("_meminfo:\n\t"); {
		struct ffly_meminfo info;
		ffly_meminfo(&info);
		ffly_pipe_write(&info, sizeof(struct ffly_meminfo), pipe);
		printf("operator requested memory infomation.\n");
	}
	jmpagain;
	__asm__("_disconnect:\n\t"
			"_end:\n\t");
	ffly_pipe_close(pipe);
	printf("goodbye.\n");
}

//_f_err_t ffmain(int __argc, char **__argv) {
//	ffly_panel_start();
//}
