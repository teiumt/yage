#!/bin/sh
ffly_cc=/usr/bin/gcc
# memory scarcity = -D__ffly_mscarcity
cc_flags="$cc_flags -O0 -g -D__ffly_crucial -mincoming-stack-boundary=4 -D__f_debug -D__ffly_mscarcity -D__ffly_debug -fno-builtin -D__ffly_no_task_pool -D__ffly_use_allocr -D__fflib -D__ylib -D__ffly_source -D__slurry_client"
# takes to much time if it works it works
# fixing warning can be done later
# even if there very bad
#cc_flags="$cc_flags -Wall"
dus=$PWD/dus/ffdus
ffly_objs=$(
	dst_dir=$PWD/amd64
	root_dir=amd64;
	. ./amd64/compile.sh;
	echo "$ffly_objs"
)
ffly_objs="$ffly_objs $(
	dst_dir=$PWD/dep
	root_dir=dep;
	. ./dep/compile.sh;
	echo "$ffly_objs"
)"
ffly_objs="$ffly_objs $(
	dst_dir=$PWD/m
	root_dir=m
	. ./m/compile.sh
	echo "$ffly_objs"
)"
ffly_objs="$ffly_objs $(
	dst_dir=$PWD/system
	root_dir=system
	. ./system/compile.sh
	echo "$ffly_objs"
)"
ffly_objs="$ffly_objs $(
	dst_dir=$PWD/flue
	root_dir=flue
	. ./flue/compile.sh
	echo "$ffly_objs"
)"

ffly_objs="$ffly_objs $(
	dst_dir=$PWD/havoc
	root_dir=havoc
	. ./havoc/compile.sh
	echo "$ffly_objs"
)"

dst_dir=$PWD
root_dir=.

compile () {
	$ffly_cc $cc_flags -c -o $dst_dir/$1 $root_dir/$2
}

as $root_dir/start.s -o $dst_dir/start.o
compile y.o y.c
compile msg.o msg.c
compile m.o m.c
compile lld.o lld.c
compile lhs.o lhs.c
compile common.o common.c
compile proc.o proc.c
compile bole.o bole.c
compile oddity.o oddity.c
compile nail.o nail.c
compile ad.o ad.c
compile config.o config.c
compile log.o log.c
compile env.o env.c
compile strf.o strf.c
compile abort.o abort.c
compile bbs.o bbs.c
compile chrdump.o chrdump.c
compile hexdump.o hexdump.c
compile iis.o iis.c
#compile storage/cistern.o storage/cistern.c
#compile wh.o wh.c

#compile havoc/font.o havoc/font.c
compile bitfont.o bitfont.c
compile bog.o bog.c
compile clock.o clock.c
compile doze.o doze.c
compile piston.o piston.c
compile sched.o sched.c
compile sched_core.o sched/core.c
compile in.o in.c
compile send.o send.c
compile recv.o recv.c
compile inet_addr.o inet_addr.c
compile evdev/evdev.o evdev/evdev.c
compile vekas/maj_connect.o vekas/maj_connect.c
compile vekas/io.o vekas/io.c
compile vekas/fd.o vekas/fd.c
compile clay.o clay.c
compile clay/hash.o clay/hash.c
compile clay/input.o clay/input.c
compile clay/lexer.o clay/lexer.c
compile clay/memalloc.o clay/memalloc.c
compile clay/parser.o clay/parser.c
compile clay/solidify.o clay/solidify.c
compile lib/hash.o lib/hash.c
compile amdgpu.o amdgpu.c
compile drm_mode.o drm_mode.c
compile drm.o drm.c
compile amdgpu/amdgpu_dev.o amdgpu/amdgpu_dev.c
compile amdgpu/amdgpu_vam.o amdgpu/amdgpu_vam.c
compile amdgpu/amdgpu_bo.o amdgpu/amdgpu_bo.c
compile amdgpu/amdgpu_ctx.o amdgpu/amdgpu_ctx.c
compile amdgpu/amdgpu_cs.o amdgpu/amdgpu_cs.c
compile ys/as/as.o ys/as/as.c
compile ys/as/exp.o ys/as/exp.c
compile ys/as/input.o ys/as/input.c
compile ys/as/parser.o ys/as/parser.c
compile ys/as/radeon.o ys/as/radeon.c
compile ys/as/software.o ys/as/software.c
compile y4.o y4.c
compile sys/t_general.o sys/t_general.c
compile thread.o thread.c
compile printf.o printf.c
compile io.o io.c
compile billet.o billet.c
compile hard.o hard.c
compile crypt.o crypt.c
compile crypt/rest.o crypt/rest.c
compile freight.o freight.c
compile vat.o vat.c
compile opt.o opt.c
compile pic_dis.o pic_dis.c
compile depart.o depart.c

#IHC
compile ima.o flue/compiler/ima/ima.c
compile hg.o flue/compiler/hg/hg.c
compile hg_to_ima.o flue/compiler/ima/hg_to_ima.c
compile ima_to_r.o flue/compiler/ima/ima_to_r.c
compile r_to_ys.o flue/compiler/ima/r_to_ys.c
compile evy.o ihc/evy/evy.c
compile fsl.o ihc/fsl/fsl.c
compile fsl_parser.o ihc/fsl/parser.c
compile yl.o ihc/ylang/yl.c
compile yl_parser.o ihc/ylang/parser.c
compile yl_lexer.o ihc/ylang/lexer.c
compile ihc.o ihc/ihc.c
compile evy_resin.o ihc/evy/output/resin.c
compile ihc_util.o ihc/util.c

compile im.o im.c
compile ppm_im.o ppm_im.c
compile mutate.o mutate.c
compile file_posix.o file_posix.c
compile rdm.o rdm.c
compile resin.o resin.c
as resin.s -o resin0.o
compile em_pic/pic18.o em_pic/pic18.c
compile em_pic/em.o em_pic/em.c
compile lib.o lib.c
compile base64.o base64.c
compile bin_tree.o bin_tree.c
compile rand.o rand.c
compile signal.o signal.c
compile sigemptyset.o signal/sigemptyset.c

#TUG
compile tug/tug.o tug/tug.c
compile tug/tug_client.o tug/tug_client.c
compile clipman/cm_connect.o clipman/cm_connect.c
#YARN
compile yarn/flue.o yarn/flue.c

compile tools.o tools.c
compile hungbuf.o hungbuf.c
compile afac/afac_wavefront.o afac/afac_wavefront.c
compile string.o string.c

compile errand.o errand.c
compile lib/cellar.o lib/cellar.c
compile graph.o graph.c
compile rws_file.o rws_file.c
compile slab_dealer.o slab_dealer.c
compile wh.o wh.c
compile real_tree.o real_tree.c
compile ima_r.o flue/compiler/ima/r.c
compile r_optm.o flue/compiler/ima/r_optm.c

compile r_to_amde.o flue/compiler/ima/r_to_amde.c
export ffly_objs="$ffly_objs $dst_dir/hg.o $dst_dir/r_optm.o $dst_dir/r_to_amde.o $dst_dir/ima_r.o $dst_dir/r_to_ys.o $dst_dir/ihc_util.o $dst_dir/real_tree.o $dst_dir/wh.o $dst_dir/slab_dealer.o $dst_dir/rws_file.o $dst_dir/graph.o $dst_dir/lib/cellar.o $dst_dir/errand.o $dst_dir/string.o $dst_dir/afac/afac_wavefront.o $dst_dir/hungbuf.o  $dst_dir/tools.o $dst_dir/yarn/flue.o $dst_dir/clipman/cm_connect.o $dst_dir/tug/tug_client.o $dst_dir/tug/tug.o $dst_dir/sys/t_general.o $dst_dir/sigemptyset.o $dst_dir/signal.o $dst_dir/rand.o $dst_dir/bin_tree.o $dst_dir/base64.o $dst_dir/lib.o $dst_dir/ima_to_r.o $dst_dir/em_pic/em.o $dst_dir/em_pic/pic18.o $dst_dir/resin.o $dst_dir/evy_resin.o $dst_dir/resin0.o \
$dst_dir/rdm.o $dst_dir/file_posix.o $dst_dir/pic_dis.o $dst_dir/depart.o $dst_dir/opt.o $dst_dir/vat.o $dst_dir/freight.o $dst_dir/crypt.o \
$dst_dir/crypt/rest.o $dst_dir/hard.o $dst_dir/billet.o $dst_dir/io.o $dst_dir/printf.o $dst_dir/thread.o $dst_dir/y4.o \
$dst_dir/drm_mode.o $dst_dir/amdgpu/amdgpu_cs.o $dst_dir/amdgpu/amdgpu_ctx.o $dst_dir/amdgpu/amdgpu_bo.o $dst_dir/amdgpu/amdgpu_vam.o \
$dst_dir/amdgpu/amdgpu_dev.o $dst_dir/drm.o $dst_dir/amdgpu.o $dst_dir/lib/hash.o $dst_dir/bbs.o $dst_dir/start.o $dst_dir/y.o $dst_dir/m.o \
$dst_dir/lld.o $dst_dir/lhs.o $dst_dir/msg.o $dst_dir/common.o $dst_dir/proc.o \
$dst_dir/bole.o $dst_dir/oddity.o $dst_dir/nail.o $dst_dir/ad.o $dst_dir/config.o \
$dst_dir/log.o $dst_dir/env.o $dst_dir/strf.o $dst_dir/abort.o $dst_dir/chrdump.o \
$dst_dir/hexdump.o $dst_dir/iis.o $dst_dir/bitfont.o \
$dst_dir/bog.o $dst_dir/clock.o $dst_dir/doze.o $dst_dir/piston.o $dst_dir/sched.o \
$dst_dir/sched_core.o $dst_dir/in.o $dst_dir/send.o $dst_dir/recv.o $dst_dir/inet_addr.o \
$dst_dir/evdev/evdev.o $dst_dir/vekas/maj_connect.o $dst_dir/vekas/io.o $dst_dir/vekas/fd.o \
$dst_dir/clay.o $dst_dir/clay/hash.o $dst_dir/clay/input.o $dst_dir/clay/lexer.o \
$dst_dir/clay/memalloc.o $dst_dir/clay/parser.o $dst_dir/clay/solidify.o \
$dst_dir/ys/as/as.o $dst_dir/ys/as/exp.o $dst_dir/ys/as/input.o $dst_dir/ys/as/parser.o \
$dst_dir/ys/as/radeon.o $dst_dir/ys/as/software.o $dst_dir/ima.o $dst_dir/hg_to_ima.o $dst_dir/evy.o $dst_dir/fsl.o $dst_dir/fsl_parser.o $dst_dir/yl.o $dst_dir/yl_parser.o $dst_dir/yl_lexer.o $dst_dir/ihc.o $dst_dir/im.o $dst_dir/ppm_im.o $dst_dir/mutate.o"
# $dst_dir/storage/cistern.o"
