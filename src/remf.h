# ifndef __ffly__remf__h
# define __ffly__remf__h
# include "y_int.h"
# include "h_int.h"
# include "header.h"
#define FF_REMF_IL		16
#define FF_REMF_MAG0	'r'
#define FF_REMF_MAG1	'e'
#define FF_REMF_MAG2	'm'
#define FF_REMF_MAG3	'f'

#define FF_REMF_NULL (~(QWORD)0)

#define remf_hdrsz		sizeof(struct remf_hdr)

#define remf_sysz		sizeof(struct remf_sy)
#define remf_reghdrsz	sizeof(struct remf_reg_hdr)
#define remf_seghdrsz	sizeof(struct remf_seg_hdr)
#define remf_relsz		sizeof(struct remf_rel)
#define remf_hoksz		sizeof(struct remf_hok)
#define remf_ttsz		sizeof(struct remf_tt)

#define F_REMF_EPDEG 0x01
#define F_REMF_RG_N 3
#define F_RG_STT	0x00
#define F_RG_SYT	0x01
#define F_RG_RLT	0x02
#define F_SY_TG 0x00
#define F_SY_WA 0x01
#define F_SY_RG 0x02
#define F_SY_RS 0x01
#define F_REMF_SY_GBL 0x02

#define F_REMF_SH 3
#define F_REMF_SZ (1<<F_REMF_SH)
#define F_REMF_SM (F_REMF_SZ-1)
#define F_REMF_STE 1

#define F_REMF_R_16 0
#define F_REMF_R_32 1
#define F_REMF_R_UDD 0x01
#define	F_REMF_AGG_SY_GBL 0
#define F_REMF_AGG_SY_N 2
/*
	PLAN:

		regions - a place
		tangs - a hole
		symbol - to resolve a hole

*/
/*
	TODO:
		turn into its own and not just strings??????
	
	NOTE:
		stent struct must be < 8-bytes(F_REMF_SZ)
		so in the case of aligned address
		we can do
		entry-ptr|F_REMF_STE = string address

		also stent+string must be aligned to F_REMF_SZ

		i was just going to add the length value into 
		whever it was being pointed to so

		struct sym{
			name
			length
		}
		but i dont know????
		as this added issues as we would need to de this for everything
		that has a string associated with it
*/
struct remf_stent {
	HWORD l;
} __attribute__((aligned(F_REMF_STE)));

typedef struct remf_sy {
	DWORD name;	
	HWORD type;
	DWORD val;
	HWORD flags;
} *remf_syp;

typedef struct remf_agg {
	HWORD flags;
	DWORD start, n;
} *remf_aggp;

typedef struct remf_reg_hdr {
	QWORD offset;
	DWORD size, n;
	QWORD adr;
	DWORD _0;
	DWORD _1;
} *remf_reg_hdrp;

typedef struct remf_seg_hdr {
	HWORD type;
	QWORD offset;
	QWORD adr;
	DWORD sz;
} *remf_seg_hdrp;

/*
	internal no symbols only frags
*/
typedef struct remf_rel {
	HWORD type, flags;
	DWORD src, dst;
} *remf_relp;

// where abouts
typedef struct remf_wa {
	QWORD adr;
};

typedef struct remf_tang {
	QWORD src, adr;
	DWORD size;
};

typedef struct remf_tt {
	DWORD t;
	QWORD t_n;
	DWORD t_m;

	QWORD w;
	DWORD w_n;
	DWORD w_m;
} *remf_ttp;

#define REMF_ID_RES 0x00

typedef struct remf_hdr {
	struct f_fh h;
	char ident[FF_REMF_IL];
	HWORD format; // <- remove
	WORD id;
	/*
		start routine could just rename to entry .... COULD
	*/
	QWORD routine;
	// region structure totaling size
	DWORD rsts;
	DWORD rn;
	QWORD rg;

	DWORD ssts;
	DWORD sn;
	QWORD sg;

	/*
		bottom of program data usage: bond -> where next file program data will be placed
	*/
	DWORD adr;

	// tang table location
	QWORD tt;
	HWORD flags;
} __attribute__((aligned(FH_BS))) *remf_hdrp;

# endif /*__ffly__remf__h*/
