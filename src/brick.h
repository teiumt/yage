# ifndef __ffly__brick__h
# define __ffly__brick__h
# include "y_int.h"
#define bricksz(__sz) (1<<__sz)
enum {
	_ff_brick_8 = 3,
	_ff_brick_16,
	_ff_brick_32,
	_ff_brick_64,
	_ff_brick_128,
	_ff_brick_256
};

#define BRICK_OPEN 0x01

struct f_brick_func {
	void(*read)(_ulonglong, void*, _8_u);
	void(*write)(_ulonglong, void*, _8_u);
	void(*del)(_ulonglong);
};

typedef struct ffly_brick {
	struct ffly_brick *next, **pn, *fd, **bk;
	void(*read)(long long, void*, _8_u);
	void(*write)(long long, void*, _8_u);
	void(*del)(long long);
	void *p;
	long long arg;
	_8_u sz;
	_8_i inuse;
	_32_u id;
	_8_u flags;
} *ffly_brickp;

_8_i ffly_brick_exist(_32_u);
void ffly_bricks_show(void);
_32_u ffly_brick_new(_8_u, void(*)(long long, void*, _8_u), void(*)(long long, void*, _8_u), void(*)(long long), long long);
void ffly_brick_rid(_32_u);
void ffly_brick_cleanup(void);
void ffly_brick_open(_32_u);
void ffly_brick_close(_32_u);
void* ffly_brick_get(_32_u);
# endif /*__ffly__brick__h*/
