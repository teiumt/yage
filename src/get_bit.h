# ifndef __ffly__get__bit__h
# define __ffly__get__bit__h
# include "y_int.h"
# include "types.h"
typedef struct ffly_getbit {
    _8_u buf;
    _8_u left;
    _8_u(*in)(void*);
	void *arg_p;
} *ffly_getbitp;
# define ffly_init_get_bit(__getbit, __in, __arg_p) \
    (__getbit)->buf = 0x0; \
    (__getbit)->left = 0; \
    (__getbit)->in = __in; \
	(__getbit)->arg_p = __arg_p

_8_u ffly_get_bit(ffly_getbitp, _8_u);
# endif /*__ffly__get__bit__h*/
