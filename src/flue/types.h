# ifndef __flue__types
# define __flue__types
#include "../y_int.h"
typedef float _flue_float;
typedef _32_u _flu_int_u;
typedef _32_u _flu_size;
typedef float _flu_float;


struct flue_vec3 {
	_flue_float x, y, z;
};

struct flu_vertex {
	_flu_float x, y, z;
};


struct flue_uv3 {
	_flue_float u0, v0;
	_flue_float u1, v1;
	_flue_float u2, v2;
};

struct flue_tgl3 {
	struct {
		struct flue_vec3 v0;
		struct flue_vec3 v1;
		struct flue_vec3 v2;
		struct flue_uv3 uv;
	} data;
	struct flue_tex *tx;	
};

struct flue_colour {
	_flue_float r, g, b, a;
};

struct flue_tglv3 {
	_int_u n;
	_flue_float *vtx;
	_flue_float *uv;
	struct flue_tex *tx;
};
# endif /*__flue__types*/
