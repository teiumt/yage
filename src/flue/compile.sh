#!/bin/sh
compile () {
	$ffly_cc $cc_flags -c -o $dst_dir/$1 $root_dir/$2
}
compile format.o format.c
compile context.o context.c
compile common.o common.c
compile render_buff.o render_buff.c
compile tex.o tex.c
compile buffer_object.o buffer_object.c
compile teximage.o teximage.c
compile view.o view.c
compile matrix.o matrix.c
compile nt_common.o nought/common.c
compile nt_context.o nought/context.c
compile nt_buffer_object.o nought/buffer_object.c
compile nt_renderbuff.o nought/renderbuff.c
compile nt_tri.o nought/tri.c
compile nt_surface.o nought/surface.c
compile nt_tile.o nought/tile.c
compile nt_tex.o nought/tex.c
compile nt_draw.o nought/draw.c
compile nt_scaf.o nought/scaf.c
compile nt_shader.o nought/shader.c
compile shader.o shader.c
compile rd_common.o radeon/common.c
compile rd_context.o radeon/context.c
compile rd_state.o radeon/state.c
compile rd_shader.o radeon/shader.c
compile rd_cmd_buffer.o radeon/cmd_buffer.c
compile rd_debug.o radeon/debug.c
compile rd_tex.o radeon/tex.c
compile fbdev.o fbdev.c
compile mjx.o mjx.c
compile rd_compute.o radeon/compute.c
compile rd_constant.o radeon/constant.c
export ffly_objs="$dst_dir/format.o $dst_dir/mjx.o $dst_dir/rd_constant.o $dst_dir/rd_compute.o $dst_dir/fbdev.o $dst_dir/rd_common.o $dst_dir/rd_context.o  $dst_dir/rd_state.o $dst_dir/rd_shader.o $dst_dir/rd_cmd_buffer.o \
$dst_dir/rd_debug.o $dst_dir/rd_tex.o $dst_dir/shader.o $dst_dir/context.o $dst_dir/common.o $dst_dir/render_buff.o \
$dst_dir/tex.o $dst_dir/teximage.o $dst_dir/view.o $dst_dir/matrix.o $dst_dir/nt_common.o $dst_dir/nt_context.o \
$dst_dir/nt_buffer_object.o $dst_dir/nt_renderbuff.o $dst_dir/nt_tri.o $dst_dir/nt_surface.o \
$dst_dir/nt_tile.o $dst_dir/nt_tex.o $dst_dir/nt_shader.o $dst_dir/nt_draw.o $dst_dir/nt_scaf.o $dst_dir/buffer_object.o"
