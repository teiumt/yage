# include "common.h"
# include "nought/common.h"
# include "radeon/common.h"
# include "../ffly_def.h"
# include "../m_alloc.h"
# include "../vekas/common.h"
#include "../string.h"
#include "../msg.h"
#define MSG_BITS MSG_DOMAIN(_DOM_FLUE)
struct flue_fmtdesc flu_fmts[10] = {
	{
		.depth = 4,
		.id = 0
	}
};

//for radeon
#define COLOUR_UNORM    0
#define COLOUR_SNORM    1
#define COLOUR_USCALED  2
#define COLOUR_SSCALED  3
#define COLOUR_UINT     4
#define COLOUR_SINT     5
#define COLOUR_SRGB     6
#define COLOUR_FLOAT    7

#define NF_UNORM    0
#define NF_SNORM    1
#define NF_USCALED  2
#define NF_SSCALED  3
#define NF_UINT     4
#define NF_SINT     5
#define NF_FLOAT    7
#define NF_SRGB     9
/*
    can be
    NF_8
    NF_8_8
    NF_8_8_8_8
*/
#define NF_SRGB     9

#define DF_32_32_32_32  14
#define DF_8_8_8_8      10
#define DF_32           4

struct flue_common flue_comm = {
	.drv_tab = {
		{&nt_drv_struc, nt_drv, nt_drv_deinit, NULL, "NOUGHT"},
		{&rd_drv_struc, rd_drv, rd_drv_deinit, NULL, "RADEON"}	
	},
	.auth = NULL,
	.nfmt = {
		{
			FLUE_NF_FLOAT,
			{
				NF_FLOAT,COLOUR_FLOAT,0,0,0,0,0,0,
			}
		},
		{
			FLUE_NF_UINT,
			{
				NF_UINT,COLOUR_UINT,0,0,0,0,0,0,

			}
		},
		{
			FLUE_NF_SRGB,
			{
				NF_SRGB,COLOUR_SRGB,0,0,0,0,0,0
			}
		},
		{
			FLUE_NF_UNORM,
			{
				NF_UNORM,COLOUR_UNORM,0,0,0,0,0,0
			}
		}


	},
	.dfmt = {
		{
			FLUE_DF_32_32_32_32,
			{
				DF_32_32_32_32,0,0,0,0,0,0,0,
			}
		},
		{
			FLUE_DF_8_8_8_8,
			{
				DF_8_8_8_8,0,0,0,0,0,0,0,
			}
		},
		{
			FLUE_DF_32,
			{
				DF_32,0,0,0,0,0,0,0
			}
		}
	}
};

void flue_framebuffer(_int_u __width, _int_u __height) {
	FLUE_CTX->d->framebuffer(FLUE_CTX->priv,__width,__height);
}

void flue_readpixels(void *__rb, void *__buf, _flu_int_u __x, _flu_int_u __y, _flu_size __width, _flu_size eight) {
	FLUE_CTX->d->readpixels(__rb, __buf, __x, __y, __width, eight);
}

#include "../ys/as/as.h"
void flue_prime(void) {
	y_log_file(LOG_FLUE,"flulog");
	FLU_PRIO1.ndv = 1;
	FLU_PRIO0.ndv = 0;
	FLU_PRIO1.dv[0].d = nt_dev_add(0);
	FLU_PRIO1.dv[0].n_use = 0;
	FLU_PRIO1.dv[0].m = ys_software; 
	FLU_PRIO1.dv[0].dv = FLUE_NOUGHT;
	FLU_PRIO1.dv[0].dv->prep();

	
	FLUE_RADEON->prep();
}

/*do device sweep*/
void flue_sweep(void) {
	rd_drv_sweep();
}

void flue_xchgbufs(struct flue_framebuffer *fb) {
	_16_u front,back;
	back = fb->xback;
	front = fb->xfront;
	fb->xfront = back;
	fb->xback = front;
	fb->ptr = fb->attachments[fb->xback];
}

void flue_sampler(struct flue_sampler *__info, _int_u __n, _64_u *__placement) {
	FLUE_CTX->d->sampler(FLUE_CTX->priv,__info,__n,__placement);
}

void flue_textures(void **__list, struct flue_shinfo *__info, _int_u __n, _64_u *__placement) {
	FLUE_CTX->d->textures(FLUE_CTX->priv,__list,__info,__n,__placement);
}

/*
	__list:				a pointer list of buffers.
	__info:				a description of the buffer(format,shader back linkage)
	__n:					number of these
	__placement:	the binding point of this buffer within the descriptor table
*/
void flue_resources(void **__list, struct flue_resspec *__info, _int_u __n,_64_u *__placement) {
	FLUE_CTX->d->resources(FLUE_CTX->priv,__list,__info,__n,__placement);
}

void flue_rendertgs(void **__list, _int_u __n) {
	FLUE_CTX->d->rendertgs(FLUE_CTX->priv,__list,__n);
}

void flue_clear(void **__list, _int_u __n,float r, float g, float b, float a) {
	FLUE_CTX->d->clear(FLUE_CTX->priv,__list,__n,r,g,b,a);
}

void flue_zbuffer(struct flue_tex *__zb) {
	FLUE_CTX->priv->zb = __zb;
}

void flue_bo_sync(struct flue_bo *__bo) {
	FLUE_CTX->d->bo_sync(FLUE_CTX->priv,__bo);
}

void flue_initz(void *__zb) {
	FLUE_CTX->d->initz(__zb);
}

void flue_draw_array(struct flue_bo *__buffer,_64_u __offset, _int_u __n,_64_u __type) {
	FLUE_CTX->d->draw_array(FLUE_CTX->priv,!__buffer?NULL:__buffer,__offset,__n,__type);
}

void flue_pushstate(void) {
	FLUE_CTX->d->pushstate(FLUE_CTX->priv);
}
/*

	shader program is linked to underneath shader

	program -> private-shader(driver/device holdings) -> assigned id of X. X is reused and is not keept?
*/
void flue_shader(struct flue_shader *__shader, _32_u __id) {
	FLUE_CTX->priv->shaders[__id] = __shader;
}

struct flue_state* flue_state(void) {
	struct flue_state *s;
	s = FLUE_CTX->priv->states+FLUE_CTX->priv->curst;

	FLUE_CTX->priv->curst+=STATE_SS;
	return s;
}

void flue_deinit(void) {
	FLU_PRIO1.dv[0].dv->deinit();
	FLUE_RADEON->deinit();
	y_log_fileend(LOG_FLUE);
}

void flue_dev(void) {
		
}

void flue_start(void) {
//	FLUE_CTX->d->configure(FLUE_CTX->priv);
}

#include "../clock.h"
#include "../tmu.h"
void flue_done(void) {
	struct y_timespec ts, ts_0;
	y_clock_gettime(&ts);
	FLUE_CTX->d->submit();
	y_clock_gettime(&ts_0);
	//might cause math errors ie. 1/0
	long double x = (1.0/(long double)CLICK_BASE)*(long double)((_64_s)ts_0.lower-(_64_s)ts.lower);
	MSG(INFO,"flue took: %u%f clicks.\n", ts_0.higher-ts.higher, (float)x)
}

/*
	swap out front and back buffers or ...
	just draw is fbdev etc.
*/
void flue_swapbufs(void *__target) {

//	FLUE_CTX->scaf->update(FLUE_CTX->dev,FLUE_CTX->priv,FLUE_CTX->draw,__target);
}
