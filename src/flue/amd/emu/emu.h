#ifndef __flue__amd__emu___h
#define __flue__amd__emu___h
#include "../../../y_int.h"
#include "../../compiler/ima/r_ops.h"
#define AMDE_VGPR 0
#define AMDE_SGPR 128
#define AMDE_REG	0
#define AMDE_IMM	1
struct amde_envir{
	_32_u gpr[256];
	_32_u mem[64];
};
#endif/*__flue__amd__emu___h*/
