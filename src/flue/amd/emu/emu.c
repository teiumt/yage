#include "emu.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include<math.h>
# include <stdarg.h>
static struct amde_envir env;
#define REG(__x) ((float*)env.gpr)[__x]
#define OPERAND_TYP(__op,__x) (((__op)>>(8+((__x)*4)))&0xf)
#define OTSTR(__op,__x) ot_str[OPERAND_TYP(__op,__x)]
static char const *ot_str[] = {"REG","IMM"};
static _8_s bypass = -1;
void static debug(char const *__format,...){
	if(bypass == -1){
	va_list args;
	va_start(args, __format);
	vprintf(__format,args);
	va_end(args);
	}
}

float static getvalue(_32_u *__code,_int_u idx){
	_32_u u;
	u = __code[0]>>((idx*4)+8);
	u = u&0xf;
	float r;
	_32_u imm;
	imm = __code[1+idx];
	if(u == AMDE_IMM){
		r = *(float*)&imm;
	}else
	if(u == AMDE_REG){
		r = REG(imm);
	}
	
	return r;
}
void static _init(void){
	_int_u i;
	i = 0;
	for(;i != 256;i++){
		env.gpr[i] = -1;
	}
	float *m;
	m = env.mem;
	//test matrix random numbers.
	m[0] = 0.45;m[1] = 0.0;m[2] = 0.0;m[3] = 0.0;
	m[4] = 0.0;m[5] = 0.31;m[6] = 0.0;m[7] = 0.0;
	m[8] = 0.0;m[9] = 0.0;m[10] = 0.95;m[11] = 0.0;
	m[12] = 0.0;m[13] = 0.0;m[14] = 0.0;m[15] = 1.0;

	m[16] = 1.32;
	m[17] = 0.24;
	m[18] = 4.01;
	m[19] = 3.05;
	
	m[20] = 3.03;
	m[21] = 9.23;
	m[22] = 5.15;
	m[23] = 1.31;

	
	//desc table
	env.mem[32] = 0;
	env.mem[32+16] = 16;

	//desc table pointer
	env.gpr[128] = 32;
}

void static _regdump(void){
	_int_u i;
	i = 0;
	for(;i != 256;i++){
		if(env.gpr[i] != -1){
			debug("GPR-%u: %f.\n",i,*(float*)&env.gpr[i]);
		}
	}
}
void static _op_rcp32f(_32_u *__code){
	REG(__code[1]) = 1./getvalue(__code,1);
}
void static _op_mulu16(_32_u *__code){}
void static _op_add32u(_32_u *__code){}
void static _op_mov(_32_u *__code){
	REG(__code[1]) = getvalue(__code,1);
}
void static _op_fadd(_32_u *__code){
	REG(__code[3])=getvalue(__code,0)+REG(__code[2]);
}
void static _op_fsub(_32_u *__code){
	REG(__code[3])=getvalue(__code,0)-REG(__code[2]);
}
void static _op_fmul(_32_u *__code){
	REG(__code[3])=getvalue(__code,0)*REG(__code[2]);
}
void static _op_fmac(_32_u *__code){
	REG(__code[3])=getvalue(__code,0)*REG(__code[2])+REG(__code[3]);	
}
void static _op_rsq_f32(_32_u *__code){
	float v;
	REG(__code[1]) = 1./sqrt(v = getvalue(__code,1));
	printf("RSQ: %f, %f\n",REG(__code[1]),v);
}
void static _op_max32f(_32_u *__code){
	float s0;
	s0 = getvalue(__code,0);
	REG(__code[3]) = s0>REG(__code[2])?s0:REG(__code[2]);
}
void static _op_min32f(_32_u *__code){
	float s0;
	s0 = getvalue(__code,0);
	REG(__code[3]) = s0<REG(__code[2])?s0:REG(__code[2]);
}

void static _op_sldw(_32_u *__code){
	printf("OP_SLDW: %u, %u, %u\n",__code[1],__code[2],__code[3]);
	_32_u offset;
	offset = __code[1];
	
	_32_u ptr;
	_32_u *src,*dst;
	ptr = env.gpr[__code[3]];
	src = env.mem+ptr+offset;
	dst = env.gpr+__code[2];
	dst[0] = src[0];
	dst[1] = src[1];
	dst[2] = src[2];
	dst[3] = src[3];
	printf("%u<%x,%x,%x,%x>\n",ptr+offset,dst[0],dst[1],dst[2],dst[3]);
}

void static _op_load(_32_u *__code){
	printf("OP_LOAD: %u, %u, %u\n",__code[1],__code[2],__code[3]);
	_32_u offset;
	offset = __code[1];

	_32_u *dst,ptr,*src;
	dst = env.gpr+__code[2];  
	ptr = env.gpr[__code[3]];
	src = env.mem+ptr+offset;
	dst[0] = src[0];
	dst[1] = src[1];
	dst[2] = src[2];
	dst[3] = src[3];
}

void static _op_store(_32_u *__code){
	printf("OP_STORE: %u, %u, %u\n",__code[1],__code[2],__code[3]);
	_32_u offset;
	offset = __code[1];
	_32_u *dst,ptr,*src;
	src = env.gpr+__code[2];  
	ptr = env.gpr[__code[3]];
	dst = env.mem+ptr+offset;
	dst[0] = src[0];
	dst[1] = src[1];
	dst[2] = src[2];
	dst[3] = src[3];
}


static void(*_ft[])(_32_u*) = {
	_op_rcp32f,//_r_op_rcp32f
	_op_mulu16,//_r_op_mulu16
	_op_add32u,//_r_op_add32u
	_op_mov,//_r_op_mov
	_op_fadd,//_r_op_fadd
	_op_fsub,//_r_op_fsub
	_op_fmul,//_r_op_fmul
	_op_fmac,//_r_op_fmac
	NULL,//_r_op_cvt_i32f32
	NULL,//_r_op_cvt_pkrtz_f16_f32
	_op_rsq_f32,//_r_op_rsq_f32
	_op_max32f,//_r_op_max32f
	_op_min32f,//_r_op_min32f
	NULL,//_r_op_imgstore
	NULL,//_r_op_imgsample
	NULL,//_r_op_imgload
	NULL,//_r_op_sldw
	NULL,//_r_op_endprog
	NULL,//_r_op_waitcnt
	NULL,//_r_op_load16
};

//here for debug
void static _r_instr_vop1(_32_u *__code){
	debug("VOP1: DST: (%u)%s, SRC: {%u}%s\n",__code[1],OTSTR(__code[0],0),__code[2],OTSTR(__code[0],1));
	_ft[__code[0]&0xff](__code);
}
void static _r_instr_vop2(_32_u *__code){
	debug("VOP1: SRC: {%u}%s, SRC1: {%u}%s, DST: {%u}%s\n",__code[1],OTSTR(__code[0],0),__code[2],OTSTR(__code[0],1),__code[3],OTSTR(__code[0],2));
	_ft[__code[0]&0xff](__code);
}
static void(*ft[])(_32_u*) = {
	_r_instr_vop1,//_r_op_rcp32f
	_r_instr_vop2,//_r_op_mulu16
	_r_instr_vop2,//_r_op_add32u
	_r_instr_vop1,//_r_op_mov
	_r_instr_vop2,//_r_op_fadd
	_r_instr_vop2,//_r_op_fsub
	_r_instr_vop2,//_r_op_fmul
	_r_instr_vop2,//_r_op_fmac
	NULL,//_r_op_cvt_i32f32
	NULL,//_r_op_cvt_pkrtz_f16_f32
	_r_instr_vop1,//_r_op_rsq_f32
	_r_instr_vop2,//_r_op_max32f
	_r_instr_vop2,//_r_op_min32f
	NULL,//_r_op_imgstore
	NULL,//_r_op_imgsample
	NULL,//_r_op_imgload
	_op_sldw,//_r_op_sldw
	NULL,//_r_op_endprog
	NULL,//_r_op_waitcnt
	NULL,//_r_op_load16
	_op_load,
	_op_store
};


_32_u static oplen[] = {
	3,//_r_op_rcp32f
	4,//_r_op_mulu16
	4,//_r_op_add32u
	3,//_r_op_mov
	4,//_r_op_fadd
	4,//_r_op_fsub
	4,//_r_op_fmul
	4,//_r_op_fmac
	1,//_r_op_cvt_i32f32
	1,//_r_op_cvt_pkrtz_f16_f32
	3,//_r_op_rsq_f32
	4,//_r_op_max32f
	4,//_r_op_min32f
	1,//_r_op_imgstore
	1,//_r_op_imgsample
	1,//_r_op_imgload
	4,//_r_op_sldw
	1,//_r_op_endprog
	1,//_r_op_waitcnt
	1,//_r_op_load16
	4,
	4
};

static char const *op_names[] = {
	"_r_op_rcp32f",
	"_r_op_mulu16",
	"_r_op_add32u",
	"_r_op_mov",
	"_r_op_fadd",
	"_r_op_fsub",
	"_r_op_fmul",
	"_r_op_fmac",
	"_r_op_cvt_i32f32",
	"_r_op_cvt_pkrtz_f16_f32",
	"_r_op_rsq_f32",
	"_r_op_max32f",
	"_r_op_min32f",
	"_r_op_imgstore",
	"_r_op_imgsample",
	"_r_op_imgload",
	"_r_op_sldw",
	"_r_op_endprog",
	"_r_op_waitcnt",
	"_r_op_load16",
	"_r_op_load",
	"_r_op_store"
};

void amde_run(_32_u *__code,_int_u __len){
	_32_u ip;
	ip = 0;
	for(;ip < __len;){
		_32_u op;
		op = __code[ip]&0xff;
		debug("op-%u: %s.\n",op,op_names[op]);	
		ft[op](__code+ip);
		ip+=oplen[op];
	}
}

#include "../../test/shader/test.h"
#include <stdlib.h>
int main(int argc,char const *__argv[]){
	_init();

	char *mode;
	mode = getenv("EMU_MODE");
	if(mode != NULL){
	if(!strcmp(mode,"debug")){
		bypass = -1;
	}else
	if(!strcmp(mode,"real")){
		bypass = 0;
	}else{
		printf("unknown operating mode.\n");
		return -1;
	}
	}

	debug("FILE: %s.\n",__argv[1]);
	_int_u i;
	i = 0;
	for(;i != 4;i++){
		REG(AMDE_TEST+i) = atof(__argv[2+i]);
	}

	int fd;
	if((fd = open(__argv[1], O_RDONLY)) == -1) {
		return -1;
	}

	struct stat st;
	fstat(fd,&st);

	_32_u *buffer;
	buffer = malloc(st.st_size);
	read(fd,buffer,st.st_size);

	debug("program words: %u.\n",st.st_size>>2);
	amde_run(buffer,st.st_size>>2);
	_regdump();
	close(fd);

	printf("'%g %g %g %g'",
		REG(AMDE_TEST+0),
		REG(AMDE_TEST+1),
		REG(AMDE_TEST+2),
		REG(AMDE_TEST+3)
	);

	return 0;
}
