# ifndef __flue__dd
# define __flue__dd

struct flue_dd {
	void*(*ctx_new)(void*);
	void(*ctx_destroy)(void*);
	void(*submit)(void);

	void(*rb_new)(void*, struct flue_rb*);
	void(*rb_init)(void*, struct flue_rb*, _32_u,_32_u,struct flue_fmtdesc*,void*);
	void(*rb_destroy)(void*, void*);

	void(*configure)(void*);
	void*(*tex_new)(void*,_int_u,_int_u,void*,_64_u);
	void(*tex_destroy)(void*);

	void(*readpixels)(void*, void*, _flu_int_u, _flu_int_u, _flu_size, _flu_size);
	struct flue_scaf scaf[1];

	void*(*bo_data)(void*,_32_u);
	void(*bo_map)(void*);
	void(*bo_unmap)(void*);
	void(*bo_destroy)(void*);

	void(*initz)(void*);
	void(*pushstate)(void*);
	/*
		shared render buffer - init
	*/
	int(*srb_init)(void*,void*, int*,int*,struct flue_rb*, _32_u,_32_u,struct flue_fmtdesc*,void*);
	void(*rbcopy)(void*,struct flue_rb*,_32_u,_32_u);
	void(*draw_array)(void*,void*,_64_u,_int_u,_32_u);
	void(*viewport)(void*,struct flue_viewport*,_int_u,_int_u);

	void(*resources)(void*,void**,void*,_int_u,_64_u*);
	void(*rendertgs)(void*,void**,_int_u);
	void(*textures)(void*,void**,void*,_int_u,_64_u*);
	void(*sampler)(void*,void*,_int_u,_64_u*);

	void(*framebuffer)(void*,_int_u,_int_u);
	struct flue_shinfo sh[8];

	void(*bo_sync)(void*,void*);

	void(*clear)(void*,void**,_int_u,float,float,float,float);
	void(*tex_relinquish)(void*);
	_64_u id;
};

# endif /*__flue__dd*/
