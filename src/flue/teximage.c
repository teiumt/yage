# include "common.h"
# include "../m_alloc.h"
# include "../linux/unistd.h"
# include "../linux/stat.h"
# include "../linux/fcntl.h"
# include "../io.h"
flue_teximagep flue_teximage_new(struct flue_sips *__s, _32_u __width, _32_u __height, struct flue_fmtdesc *__fmt) {
	flue_teximagep img;
	img = (flue_teximagep)m_alloc(sizeof(struct flue_teximage));
	img->s = __s;
	
	_8_u error;
	error = ((__width|__height)&FLUE_TILE_MASK);
	if (error>0) {
		printf("error misalignment of width and height of teximage.\n");
		return;
	}

	_32_u tx, ty;

	img->w = __width;
	img->h = __height;
	img->pxz = __fmt->depth;
	tx = img->w>>FLUE_TILE_SHFT;
	ty = img->h>>FLUE_TILE_SHFT;
	img->tw = tx;
	img->th = ty;
	_32_u tn;
	tn = tx*ty;
	img->fmt = *__fmt;
	img->totsz = img->w*img->h*__fmt->depth;
	img->tn = tn;
	img->tiles = (struct flue_teximgtile*)m_alloc(tn*sizeof(struct flue_teximgtile));

	struct flue_teximgtile *t;
	_int_u i;
	i = 0;
	for(;i != tn;i++) {
		t = img->tiles+i;
		t->map = NULL;
	}

	return img;
}
#include "../assert.h"
void flue_teximg_load(flue_teximagep __tx) {
	//function does not work
	_8_u *data;
	data = (_8_u*)m_alloc(__tx->totsz);
	_32_u offset = 0;
	struct flue_teximgtile *t;
	_32_u x, y;
	_32_u tx, ty;
	y = 0;
	while(y != __tx->h) {
		ty = y>>FLUE_TILE_SHFT;
		x = 0;
		while(x != __tx->w) {
			tx = x>>FLUE_TILE_SHFT;
			t = __tx->tiles+tx+(ty*__tx->tw);
			t->map = data+offset;
			assert(((x+(y*__tx->w))*__tx->pxz)<__tx->totsz);
			__tx->s->copy_rect(__tx, t->map, x, y, FLUE_TILE_SIZE, FLUE_TILE_SIZE);
			offset+=FLUE_TILE_SIZE2*__tx->pxz;
			x+=FLUE_TILE_SIZE;
		}
		y+=FLUE_TILE_SIZE;
	}
}

void flue_tximg_read(flue_teximagep __img, _8_u *__dst, _32_u __x, _32_u __y, _32_u __width, _32_u __height) {
	_32_u x, y;
	_32_u xx, yy;
	_32_u tx, ty;
	_32_u txo, tyo;
	_8_u *d, *s;
	__x&=~FLUE_TILE_MASK;
	__y&=~FLUE_TILE_MASK;
	__width&=~FLUE_TILE_MASK;
	__height&=~FLUE_TILE_MASK;
	_int_u ndt = 0;
	struct flue_teximgtile *t;
	y = 0;
	while(y != __height) {
		yy = __y+y;
		ty = yy>>FLUE_TILE_SHFT;
		tyo = yy&FLUE_TILE_MASK;
		x = 0;
		while(x != __width) {
			xx = __x+x;
			tx = xx>>FLUE_TILE_SHFT;
			txo = xx&FLUE_TILE_MASK;
			t = __img->tiles+tx+(ty*__img->tw);
			if (!t->map) {
				ndt++;
				t->map = m_alloc(FLUE_TILE_SIZE2*__img->pxz);
	
				__img->s->copy_rect(__img, t->map, x&~FLUE_TILE_MASK, y&~FLUE_TILE_MASK, FLUE_TILE_SIZE, FLUE_TILE_SIZE);
			}
			d = __dst+((x+(y*__width))*__img->pxz);
			s = ((_8_u*)t->map)+((txo|(tyo<<FLUE_TILE_SHFT))*__img->pxz);
			if (__img->fmt.depth == 16) {
				((float*)d)[0] = ((float*)s)[0];
				((float*)d)[1] = ((float*)s)[1];
				((float*)d)[2] = ((float*)s)[2];
				((float*)d)[3] = ((float*)s)[3];
			}else
				*(_32_u*)d = *(_32_u*)s;

			x++;
		}
		y++;
	}
	printf("TXIMG-READ: %u-tiles-created, %u, %u\n", ndt, __height, __width);
}

void flue_teximage_destroy(flue_teximagep __img) {
	m_free(__img);
}
