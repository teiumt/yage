#include "mjx.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../amdgpu.h"
#include "../time.h"
#include "../string.h"
static void _share(_32_u __fd) {

}

void mjx_make(flue_contextp __ctx, struct flue_drawable *__d) {
}

void static _auth(struct maj_conn *__conn, int fd,_32_u __drv) {
	printf("MAJ AUTH- %u.\n",fd);

	maj_auth(__conn, fd);
	doze(0,1*8);
}

_8_s mjx_connect(struct maj_conn *conn) {
	flue_comm.auth = _auth;
	flue_comm.conn = conn;
	maj_ctxmake(conn,0);
	maj_ctxsetdev(conn,VK_DRV_AMDGPU,0);
	return 0;
}

void mjx_disconnect(struct maj_conn *__conn) {
	maj_exit(__conn);
	maj_close(__conn);
}

void
mjx_creatwindow(struct mjx_window*d) {
}
#include "../vekas/struc.h"
void mjx_wdmap(struct maj_conn *__conn,struct mjx_window *__w, _int_u __x, _int_u __y, _int_u __width, _int_u __height) {
	struct flue_fmtdesc fmt = {.depth=4,.nfmt=FLUE_NFMT(FLUE_NF_UNORM),.dfmt=FLUE_DFMT(FLUE_DF_8_8_8_8)};
	__w->back = mjx_buffer(__width,__height,&fmt);
	__w->front = mjx_buffer(__width,__height,&fmt);
	__w->fb.attachments[FLUE_BACK]	= __w->back->render;
	__w->fb.attachments[FLUE_FRONT]	= __w->front->render;
	__w->fb.xback = FLUE_BACK;
	__w->fb.xfront = FLUE_FRONT;
	__w->fb.ptr = __w->back->render;

	maj_patch(__conn,&__w->pt,__x,__y,__width,__height,MAJ_SHARED);
	sendfd(__conn->u_fd,__w->back->handle);	
	sendfd(__conn->u_fd,__w->front->handle);
	ffly_fprintf(ffly_err, "sent : back: %d, front: %d.\n",__w->back->handle,__w->front->handle);
	maj_ctxsetpt(__conn,&__w->pt);
//	maj_ptconfig(__conn,_PT_HOLDPTR);
	__w->back->render->config |= FLUE_TEX_BLEND_EN;
	__w->front->render->config |= FLUE_TEX_BLEND_EN;
}

void static
mjx_buffer_destroy(struct mjx_buffer *__buffer) {
	flue_tex_destroy(__buffer->render);
	m_free(__buffer);
}

void mjx_wdunmap(struct maj_conn *__conn,struct mjx_window *__w) {
	maj_ptdestroy(__conn,&__w->pt);
	/*
		maj_ptdes non-wait i think?
	*/
	doze(0,1);
	mjx_buffer_destroy(__w->back);
	mjx_buffer_destroy(__w->front);
}

struct mjx_buffer*
mjx_buffer(_int_u __width, _int_u __height, struct flue_fmtdesc *__fmt) {
	struct mjx_buffer *buf;
	buf = m_alloc(sizeof(struct mjx_buffer));
	struct flue_tex *r;
	r = flue_tex_new(__width,__height,__fmt,0);
	mem_set(r->tx.ptr,0,__width*__height*4);

	FLUE_CTX->d->srb_init(FLUE_CTX->dev, FLUE_CTX->priv, &buf->handle,NULL,r, __width, __height, __fmt, FLUE_CTX->dev->fct+__fmt->id);
	buf->render = r;	
	return buf;
}

void mjx_swapbufs(struct maj_conn *__conn, struct mjx_window *__w) {
	//flue_swapbufs(__w->render);

	/*
		send scanout commad
	*/
	maj_swapbufs(__conn);
	flue_xchgbufs(&__w->fb);
}
