/*
	okay so im at a deadend here.

	on one hand you could do it like this

	struct framebuffer{
		struct texture *front,*back;
	}

	or 

	just do this

	struct texture{
		struct buffer *front,*back;
	};

	how does MESA do this?

	they use this.

	#define GL_FRONT
	#define GL_BACK
	struct framebuffer{
		struct gl_renderbuffer buffers[MAX];
	};
	buffers[GL_FRONT] = front buffer
	buffers[GL_BACK] = back buffer

	this is where i dont realy understand.
	
	the buffers we swap are mirror images of themselfs.
	same format,same width and height, etc(twins).

	so why not define it within the driver?

	NO!!!!!!!!!!!

	a texture is a piece of text that describes its buffer.

	struct texture{
		struct buffer *buf;
		struct buffer *second;
		struct format fmt;(RGBA/RGAB)
	};

	best solution to this would be leave it as a single buffer.
	and at the front ends allocate another buffer.

	struct texture *tx;

	tx = DRIVER->tex_new(512,512,format);
	
	struct buffer *second;

	; allocate buffer using texture as refrence
	second = DRIVER->alloc_bo_for(tx,COPY_TEXTURE);
	tx->second = second;
	
	swapbuffers(){
		struct buffer *tmp;
		tmp = tx->buf;
		tx->buf = tx->second;
		tx->second = tmp;
	}

	but that is however delusional why? my concern is the fact that the formatting and size is the same.
	so you could just do this.

	struct texture{
		struct tex_info *info;
		struct buffer *buf;
	};

	struct texture *back,*front;
	back = DRIVER->tex_new(512,512,format);

	front = DRIVER->tex_new_copy(back);
		
	tex_new_copy(struct texture *tx){
		struct texture *t;
		t = malloc(sizeof(struct texture));
		t->buf = buffer_alloc(tx->info);
		t->info = tx->info;
	}

	so to conclude both ways are complete stupid.
	and it is better just to allocate two textures for front and back with no input from driver.
*/
