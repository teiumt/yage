# ifndef __flue__common
# define __flue__common
#include "../y_int.h"

typedef struct flue_tex *flue_texp;
struct flue_tex;
#include "../log.h"
#include "types.h"
#include "struc.h"
#include "teximage.h"
#include "tex.h"
#define flue_printf(...)\
	log_printf(LOG_FLUE,__VA_ARGS__);

#define FLUE_ALLOT_CONST 2
#define FLUE_ALLOT_SAMPLE 4
#define FLUE_ALLOT_VERTEX 1
#define FLUE_ALLOT_PRIM 3
/*
	MULTIGPU.

	for flue a device is linked and only one device can beused per context.
*/
/*
	FLUE_STATE should be used with immanate values

	why are we using structures and not functions,
	because i dont like branching routines

	example

	draw_tri
	draw_tri
	draw_tri
	... *16


	v.s.

	a single 
	draw(tris to draw, rects to draw, etc)

	NOTE: 
		i know this might not be faster as we are not making 
		use of registers but!...
	
		it not just branching 
		its a ptr-form branching

		ie.

		func_ptr();
		
		and anyway memorys only getting faster
		so i think its okay as we can make up for this
		internaly


	SHADER resource placement value?
	how should it be done?
	shaders can read N consecutive registers so grouping resource placement values where needed will be a preformance benefit

	for example if read
	LOAD from A and right after we load from B the placement value for B should follow A
	as we can do a LOAD-8DWORDS to load the resource descriptor and not two LOAD-4DWORDS
	NO!
	what we can do it allow the user to use this?
	so we generate a table guiding the user insted of forcing them

	for example 
		hey it might be beneficial to use the placement values for A and B that ive generated for YOU? 
*/
#define FLUE_CAN(__b, __c, __op, __n_ops)\
	{(__c)->ops=(__b)->ops+__op;(__c)->n_ops=__n_ops;}
#define FLUE_STATE(__bb, __s, __a,  __c)\
	{(__s)->t=(__bb)->text+__a;(__s)->op=(__bb)->ops+__c;}
#define FLUE_INITSTATE(__b, __s)\
	{(__s)->t=(__b)->text;(__s)->op=(__b)->ops;}
#define FLUE_setctx(__ct) FLUE_CTX=__ct
#define FLUE_CTX (flue_comm.ctx)
#define FLUE_NOUID 0
#define FLUE_RADID 1
#define FLUE_NOUGHT (flue_comm.drv_tab+0)
#define FLUE_RADEON (flue_comm.drv_tab+1)

#define FLU_M(__ct) (__ct->_m+16)
#define FLUE_TEXTURE0 0
#define FLUE_TEXTURE1 1
#define FLUE_TEXTURE2 2
#define FLUE_TEXTURE3 3

#define FSH_PIXEL_I 0
#define FSH_VETX_I 1
#define FLUE_SH_PIXEL	FSH_PIXEL,FSH_PIXEL_I
#define FLUE_SH_VETX	FSH_VETX,FSH_VETX_I
#define FLUE_ID (FLUE_CTX->d->id)
/*
	we are going to tri somthing diffrent
*/
//in quad words
void flue_placement(void *__ptr, _int_u __comp, _64_u __placement);
void* flue_allot(struct flue_prog *__pgm, char const *__ident, _int_u __len,_64_u,_64_u,_64_u);
#define FLUE_DEVSTC (0x100*8)
void flue_sampler(struct flue_sampler*, _int_u __n, _64_u *__placement);
void flue_file_shader(struct flue_prog *__pg, char const *__path, _int_u __pathlen);
void flue_prog_init(struct flue_prog *pg);
void flue_draw_array(struct flue_bo *__buffer,_64_u,_int_u __n,_64_u __type);
void flue_sweep(void);
void flue_initz(void*);
void flue_prime(void);
void flue_start(void);
void flue_deinit(void);
void flue_pushstate(void);
void flue_dev(void);
void flue_done(void);
void flue_textures(void **__list, struct flue_shinfo *__info, _int_u __n, _64_u*);
void flue_translate(_flue_float, _flue_float, _flue_float);
void flue_shader(struct flue_shader*,_32_u);
void flue_clear(void **__list, _int_u __n,float,float,float,float);
void flue_zbuffer(struct flue_tex *__zb);
void flue_rendertgs(void **__list, _int_u __n);
void fsl_load(struct flue_shader*,char const*);
//render_buff.c
struct flue_rb* flue_rb_new(void);
void flue_rb_init(struct flue_rb*,_int_u,_int_u,struct flue_fmtdesc*);
void flue_rb_destroy(struct flue_rb*);
void flue_resources(void**,struct flue_resspec*,_int_u,_64_u*);
void flue_readpixels(void*, void*, _flu_int_u, _flu_int_u, _flu_size, _flu_size);
void flue_viewport(struct flue_viewport*, _int_u,_int_u);
//matrix.c
void flue_mbase(void);
void flue_matrix(void);
void flue_matrix_mul4(_flue_float*, _flue_float*, _flue_float*);
void flue_matrix_inv16(_flu_float*);
void flue_perspective(_flu_float, _flu_float);
void flue_pushm(_flu_float*);
//view
void flue_translate(_flu_float, _flu_float, _flu_float);
void flue_lookat(_flu_float, _flu_float, _flu_float,
    _flu_float, _flu_float, _flu_float,
    _flu_float, _flu_float, _flu_float);
void flue_frustum(_flu_float, _flu_float, _flu_float, _flu_float);
void flue_prog_ready(struct flue_prog*);
struct flue_prog* flue_prog(void);
void ysa_loader(struct ys_mech*, struct flu_plarg*, struct ysa_data*);
void flue_prog_delete(struct flue_prog*);
void flue_xchgbufs(struct flue_framebuffer *t);
//vluf
flue_contextp flue_ctx_new(void);
void flue_ctx_destroy(flue_contextp);
extern struct flue_common flue_comm;

struct flue_state* flue_state(void);
void flue_bo_sync(struct flue_bo *__bo);


//buffer_object.c
struct flue_bo* flue_bo_data(_int_u);
void flue_bo_map(struct flue_bo*);
void flue_bo_unmap(struct flue_bo*);
void flue_bo_destroy(struct flue_bo*);


void flue_swapbufs(void*);
void flue_framebuffer(_int_u __width, _int_u __height);
flue_contextp _flue_ctx(struct flue_dev*);
void flue_prog_data(void *__shader, _int_u __size);
#define MAPPING_ALLOWED 0x01
struct flue_sips {
	void(*copy_rect)(_ulonglong, void*, _32_u, _32_u, _32_u, _32_u);
	void*(*map)(_ulonglong);
	void(*unmap)(_ulonglong);
	_8_u bits;
};
struct flue_fmtdesc flu_fmts[10];
char const* flue_dfmtstr(void *__fmt, _64_u __what);
char const* flue_nfmtstr(void *__fmt, _64_u __what);
# endif /*__flue__common*/
