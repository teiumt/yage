# ifndef __flue__teximage
# define __flue__teximage
#define FLUE_TILE_SHFT 4
#define FLUE_TILE_SIZE (1<<FLUE_TILE_SHFT)
#define FLUE_TILE_MASK (FLUE_TILE_SIZE-1)
#define FLUE_TILE_SIZE2 (1<<(FLUE_TILE_SHFT*2))
/*
	NOTE TO SELF

	xy, width,height must be multables of tile size
*/
struct flue_teximgtile {
	_8_u *map;
};
typedef struct flue_teximage {
	_8_u userdata[0x100];
	_32_u w, h;
	_32_u tw, th;
	_8_u pxz;
	_32_u totsz;
	struct flue_fmtdesc fmt;
	struct flue_teximgtile *tiles;
	_int_u tn;
	_ulonglong arg;
	struct flue_sips *s;
} *flue_teximagep;
flue_teximagep flue_teximage_new(struct flue_sips* , _32_u, _32_u,struct flue_fmtdesc*);
void flue_teximage_destroy(flue_teximagep);
void flue_teximg_load(flue_teximagep);
void flue_tximg_read(flue_teximagep, _8_u*, _32_u, _32_u, _32_u, _32_u);
# endif /*__flue__teximage*/
