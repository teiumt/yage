rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd flue
gcc $cc_flags -c context.c
gcc $cc_flags -c common.c
gcc $cc_flags -c op.c
gcc $cc_flags -c render_buff.c
gcc $cc_flags -c tex.c
gcc $cc_flags -c teximage.c
gcc $cc_flags -c view.c
gcc $cc_flags -c nought/common.c -o nt_common.o
gcc $cc_flags -c nought/context.c -o nt_context.o
gcc $cc_flags -c nought/buffer_object.c -o nt_buffer_object.o
gcc $cc_flags -c nought/renderbuff.c -o nt_renderbuff.o
gcc $cc_flags -c nought/tri.c -o nt_tri.o
gcc $cc_flags -c nought/surface.c -o nt_surface.o
gcc $cc_flags -c nought/tile.c -o nt_tile.o
gcc $cc_flags -c nought/tex.c -o nt_tex.o

ffly_objs="$ffly_objs view.o common.o context.o op.o render_buff.o tex.o teximage.o nt_common.o nt_context.o nt_buffer_object.o nt_renderbuff.o nt_tri.o nt_surface.o nt_tile.o nt_tex.o"
gcc $cc_flags -o a.out main.c $ffly_objs -nostdlib
