# include "common.h"
# include "../m_alloc.h"
/*
	issue textures cant be bound to specific GPUS like this

	what happens if texture is used of two GPU?

	we dont want to call flue_tex_new twice?
	we need a way for the texure to exist on all GPU's
	this is so we dont have to 

	this-gpu
	texture_new(TEXTURE0)

	switch-gpu
	texture_new(TEXTURE0)
*/
/*
	create new texture structure resided here and driver side
*/
flue_texp flue_tex_new(_32_u __width, _32_u __height, struct flue_fmtdesc *__fmt,_64_u __config) {
	return FLUE_CTX->d->tex_new(FLUE_CTX->priv,__width,__height,__fmt,__config);
}

/*
	FIXME: remove one???????
*/
//free memory
void flue_tex_relinquish(flue_texp t) {
	FLUE_CTX->d->tex_relinquish(t);
}

void flue_tex_destroy(flue_texp t) {
	FLUE_CTX->d->tex_destroy(t);
}


void flue_texload(struct flue_tex *__t) {
	if (__t->img != NULL)
		flue_tximg_read(__t->img, __t->mmap, __t->x, __t->y, __t->width, __t->height);
}
