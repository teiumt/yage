/*
  how shaders are delt with in mesa/gallium drivers.

  shaders could have variants of that shader.
  shaders are fetched using a hash table with
  the key being the state.
*/
#include "common.h"
#include "reg.h"
#include "../../m_alloc.h"
#include "../../io.h"
#include "../../y_int.h"
#include "../../string.h"
#include "../../assert.h"
#include "../../lib.h"
#include "../shader.h"
void static
construed(struct flue_prog *__pgm, struct rd_shader *__sh) {}
#include "../../tools.h"
void rd_shader_alloc_memory(struct rd_dev *d, struct rd_shader *sh, _int_u __size) {
	sh->bo = rd_bo_create(d->dev,
		__size,
		0,//aligment
		AMDGPU_GEM_DOMAIN_VRAM,//shader must be placed in video memory i think??
		AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED,
		AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE
	);
	rd_bo_map(sh->bo);
	sh->secondary = ((struct amdgpu_bo*)sh->bo)->cpu_ptr;
	sh->ss = __size;
	
	sh->adrlow = (sh->bo->v->addr>>8)&0xffffffff;
	sh->adrhigh = sh->bo->v->addr>>40;
}
//void rd_shader_imprint();
/*
  this does what it says it does.
  it translates some input into usable output for this specific driver;
*/
struct rd_shader*
rd_get_shader(struct rd_context *__ct,struct rd_shader_key *__key){
	struct rd_shader *sh;
	sh = f_lhash_get(&__ct->ht,__key,sizeof(struct rd_shader_key));
	if(!sh){
		sh = m_alloc(sizeof(struct rd_shader));
		f_lhash_put(&__ct->ht,__key,sizeof(struct rd_shader_key),sh);
		sh->bits = RD_UNCOMPILED;
		sh->sh = __key->shader;
		sh->bo = NULL;
		sh->dtyp = __key->dtyp;
	}

	return sh;
}

#include "../compiler/ima/ima.h"
#include "../../ys/as/as.h"
void static dump_dwords(_32_u *__words,_int_u __n){
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		printf("DW%u: %x.\n",i,__words[i]);
	}
}

struct vik{
	_16_u param_cnt:4;
	_16_u mrt_cnt:4;
	_16_u pos_cnt:4;
	_16_u pad:4;
	_32_u dtyp;
};


#define vikset(__elem,v,s)\
	(v)->__elem = (s)->__elem
void static variant_strident(char *__buf,struct rd_shader *__sh){
	struct vik v;
	struct vik *p = &v;
	struct r_shader *info = &__sh->info;
	vikset(param_cnt,p,info);
	vikset(mrt_cnt,p,info);
	vikset(pos_cnt,p,info);
	v.pad = 0;
	v.dtyp = __sh->dtyp;
	__buf[y4_enc(&v,__buf,sizeof(struct vik))] = '\0';
}
extern struct r_desc _rdesc_table[64];
void _translate_shader(struct rd_context *__ct,struct rd_shader *__sh){
	r_desc_table = _rdesc_table;

	rd_sh = &__sh->info;
	r_init_shader(rd_sh);

	ima_pd = __sh->sh->priv;
	ihc_util_file(&ima_pd->file);
	//produces R-instrs
  r_propagate();

	char ident[128];
	variant_strident(ident,__sh);

	char save[128],name[128];
	str_cpy(name,__sh->sh->name);
	str_rplc(name,'/','_');
	str_cpy(save,"/opt/yage/radeon_");
	str_cat(save,ident);
	str_cat(save,"_");
	str_cat(save,name);
	str_cat(save,".s");
	str_cpy(ima_pd->file.file_params.buf,save);
	ima_pd->file.open(&ima_pd->file);
 // produces final data
 	r_produce(&r_ys);
	ima_pd->file.close(&ima_pd->file);
	printf("##############SAVE: %s.\n",save);

	struct ysa_data data;
	struct flu_plarg arg;
  arg.p = save;
  ysa_loader(ys_radeon,&arg,&data);
	__sh->primary = data.ptr;
	__sh->ps = data.size;
	dump_dwords(__sh->primary,__sh->ps/4);
}

void static shader_upload(struct rd_context *__ctx,struct rd_shader *sh){
	printf("RADEON SHADER- %p\n", sh);
	void *src;
	src = sh->primary;
	_int_u size;
	size = sh->ps;
	if (sh->bo != NULL) {
		rd_bo_destroy(sh->bo);
	}
	rd_shader_alloc_memory(__ctx->d,sh,size);

	//copy data
	mem_cpy(sh->secondary,src,size);

	_int_u i;
	i = 0;
	for(;i != size/4;i++) {
		printf("DW%u: %x.\n",i,((_32_u*)src)[i]);
	}
	printf("copyed %u-dwords.\n",size/4);
	
	// make sure is aligned on 256
	assert(!(sh->bo->v->addr&0xff));
	sh->adrlow = (sh->bo->v->addr>>8)&0xffffffff;
	sh->adrhigh = sh->bo->v->addr>>40;
}

_32_u static get_dtyp(void){
	_32_u dtyp = 0;
	_int_u i;
	i = 0;
	struct r_desc *d;
	for(;(d = &_rdesc_table[FLUE_USERSLOT0+i])->type != -1;i++){
		dtyp |= d->type;
		dtyp<<=4;
	}
	return dtyp;
}

void rd_shader_fs_update(rd_contextp __ctx){
	struct rd_shader_key key;
	key.shader = __ctx->sh.shaders[FSH_PIXEL_I];
	key.dtyp = get_dtyp();
	struct rd_shader *sh;
	sh = rd_get_shader(__ctx,&key);
	__ctx->ps = sh;
	
	struct r_desc *d;
	_int_u flat = 4;//flats start at location 4
	_int_u smooth = 0;
	_int_u i;
	i = FLUE_EXPORT_PARAM0;
	for(;i != FLUE_EXPORT_PARAM5;i++){
		d = &_rdesc_table[i];
		_16_u intrp;
		intrp = key.shader->interp_slots[i];
		if(intrp == FLUE_INTERP_MODE_FLAT){
			d->val = flat++;
		}else
		if(intrp == FLUE_INTERP_MODE_SMOOTH){
			d->val = smooth++;
		}
	}


	if(sh->bits&RD_UNCOMPILED){
		_translate_shader(__ctx,sh);
		shader_upload(__ctx,sh);
		sh->bits &= ~RD_UNCOMPILED;
	}
}

void rd_shader_vs_update(rd_contextp __ctx){
	struct rd_shader_key key;
	key.shader = __ctx->sh.shaders[FSH_VETX_I];
	key.dtyp = get_dtyp();
	struct rd_shader *sh;
	sh = rd_get_shader(__ctx,&key);
	__ctx->vs = sh;
	if(sh->bits&RD_UNCOMPILED){
		_translate_shader(__ctx,sh);
		shader_upload(__ctx,sh);
		sh->bits &= ~RD_UNCOMPILED;
	}
}

void rd_shader_update(rd_contextp __ctx){
	//FS should be first
	rd_shader_fs_update(__ctx);
	rd_shader_vs_update(__ctx);
}

void rd_shader_dump(void) {
	return;
	/*
	_int_u i;
	i = 0;
	for(;i != 8;i++) {
		struct rd_shader *s;
		s = FSH(i);
		flue_printf("SHADER-%u : %p: param_cnt: %u.\n",i,s,s->info.param_cnt);
	}
	*/
}

char const *ps_name = "PIXEL-SHADER";
char const *vs_name = "VERTEX-SHADER";
char const *cs_name = "COMPUTE-SHADER";
/*
	NOTE:
		RSRC3 does not follow RSRC2
*/
void
rd_shader_ps_state_emit(rd_contextp __ctx,struct rd_shader *ps){
	struct rd_cmd_buffer *__cmdbuf = __ctx->cmdbuf;
	printf("PIXEL_SHADER: %p.\n",ps);
	assert(ps->bo != NULL);
 
 	struct rd_dbblock *b;
    b = rd_dbblock(__ctx);
	b->ident = ps_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	printf("PIXEL-SHADER: %x.\n",ps->bo->v->addr);	
	*dw = PKT3(SET_SH_REG, 4);
	dw[1] = SH_REG(SPI_SHADER_PGM_LO_PS);
	dw[2] = ps->adrlow;/*SPI_SHADER_PGM_LO_PS*/
	dw[3] = ps->adrhigh;/*SPI_SHADER_PGM_HI_PS*/
	dw[4] = 63|(4<<6);/*RSRC1*/
	/*
		1-5 = user sgpr terms to load
		from SPI_SHADER_USER_DATA_XXX
	*/
	dw[5] = ((FLUE_RSPS->ud_offset+8)<<1);/*RSRC2*/
	printf("PIXEL SHADER USER-DATA: %u.\n",FLUE_RSPS->ud_offset);
/*
	NOTE:
		POS_W_FLOAT_ENA requires that one PERSP_ is enabled
*/
	dw[6] = PKT3(SET_SH_REG, 1);
	dw[7] = SH_REG(SPI_SHADER_PGM_RSRC3_PS);
	dw[8] = 0xffff|(0x8<<16);/*RSRC3*/
	printf("PS, low: %x, high: %x, size: %u\n", ps->adrlow, ps->adrhigh,ps->ss);

/*
	if FLAT_SHADE and OFFSET(5) are both enabled then
	P0, P10, and P20 are loaded,
	if this is not the case 
	P0 works but P10 and P20 dont?
*/

	/*
		Opengl uses !(FLAT_SHADE OFFSET(5)) for smooth
	*/
	dw[9] = PKT3(SET_CONTEXT_REG, 11);
	dw[10] = CONTEXT_REG(SPI_PS_INPUT_CNTL_0);
	dw[11] = 0;
	dw[12] = 1;
	dw[13] = 2;
	dw[14] = 3;
	dw[15] = 4|(1<<10)|(1<<5);
	dw[16] = 5|(1<<10)|(1<<5);
	dw[17] = 6|(1<<10)|(1<<5);
	dw[18] = 7|(1<<10)|(1<<5);
	dw[19] = 8|(1<<10)|(1<<5);
	dw[20] = 9|(1<<10)|(1<<5);
	dw[21] = 10|(1<<10)|(1<<5);


	dw[22] = PKT3(SET_CONTEXT_REG, 1);
	dw[23] = CONTEXT_REG(SPI_PS_IN_CONTROL);
#ifdef __vekas
	dw[24] = (1<<1)|(1<<0xe);
#else
	dw[24] = (8<<1)|(1<<0xe/*BC_OPTIMIZE_DISABLE*/)/*N-params*/;
#endif
	dw[25] = PKT3(SET_CONTEXT_REG, 1);
	dw[26] = CONTEXT_REG(SPI_PS_INPUT_ENA);
	dw[27] = (1<<0)|(0x7<<8)/*perspective*/;

	dw[28] = PKT3(SET_CONTEXT_REG, 1);
	dw[29] = CONTEXT_REG(SPI_BARYC_CNTL);
	dw[30] = (1<<20);//|(1<<4)|(1<<8)|(1<<16)|(1<<20)|(1<<24);

	dw[31] = PKT3(SET_CONTEXT_REG, 1);
	dw[32] = CONTEXT_REG(DB_SHADER_CONTROL);
	dw[33] = (1<<0xb);//|_A203_Z_EXPORT_EN;
	
	dw[34] = PKT3(SET_CONTEXT_REG, 1);
	dw[35] = CONTEXT_REG(SPI_PS_INPUT_ADDR);
	dw[36] = (1<<0)|(0x7<<8);/*perspective*/

	/*no such export*/
	dw[37] = PKT3(SET_CONTEXT_REG,1);
	dw[38] = CONTEXT_REG(SPI_SHADER_Z_FORMAT);
	dw[39] = 0;
	__cmdbuf->ndw+=40;
	b->ndw = 40;
	rd_addtothelist(__ctx,ps->bo);
}

void
rd_shader_vs_state_emit(rd_contextp __ctx,struct rd_shader *vs){
	struct rd_cmd_buffer *__cmdbuf = __ctx->cmdbuf;
	assert(vs->bo != NULL);
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = vs_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	printf("VERTEX-SHADER: %x.\n",vs->bo->v->addr);	
	/*
		rsrc3 should be fixed to this
		cu_en = ~0
		wave_limit = ~0
	*/

	*dw = PKT3(SET_SH_REG, 4);
	dw[1] = SH_REG(SPI_SHADER_PGM_LO_VS);
	dw[2] = vs->adrlow;/*SPI_SHADER_PGM_LO_VS*/
	dw[3] = vs->adrhigh;/*SPI_SHADER_PGM_HI_VS*/
	dw[4] = 63|(4<<6)|(3<<0x18);/*RSRC1*/
	dw[5] = (FLUE_RSVS->ud_offset+8)<<1;/*RSRC2*/
	printf("VERTEX SHADER USER-DATA: %u.\n",FLUE_RSVS->ud_offset);

	dw[6] = PKT3(SET_SH_REG, 2);
	dw[7] = SH_REG(SPI_SHADER_PGM_RSRC3_VS);
	dw[8] = 0xffff|(0x8<<16);/*RSRC3*/
	dw[9] = 0;

	dw[10] = PKT3(SET_CONTEXT_REG, 1);
	dw[11] = CONTEXT_REG(SPI_VS_OUT_CONFIG);
#ifdef __vekas
	dw[12] = ((1 -1)<<1);
#else
	_32_u param_cnt = !vs->info.param_cnt?1:vs->info.param_cnt;
	dw[12] = ((param_cnt-1)<<1);
	flue_printf("AMDGPU: PARAM_COUNT: %u.\n",param_cnt);
#endif
	dw[13] = PKT3(SET_CONTEXT_REG, 1);
	dw[14] = CONTEXT_REG(PA_CL_VS_OUT_CNTL);
	dw[15] = 0;

	dw[16] = PKT3(SET_CONTEXT_REG, 1);
	dw[17] = CONTEXT_REG(VGT_REUSE_OFF);
	dw[18] = 0;

	dw[19] = PKT3(SET_CONTEXT_REG, 1);
	dw[20] = CONTEXT_REG(VGT_VERTEX_REUSE_BLOCK_CNTL);
	dw[21] = 0;
	printf("VS, low: %x, high: %x, size: %u\n", vs->adrlow, vs->adrhigh,vs->ss);
	__cmdbuf->ndw+=22;
	b->ndw = 22;
	rd_addtothelist(__ctx,vs->bo);

}

void
rd_shader_compute(rd_contextp __ctx,struct rd_shader *cs){
	struct rd_cmd_buffer *__cmdbuf = __ctx->cmdbuf;
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = cs_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;

	*dw = PKT3(SET_SH_REG, 2);
	dw[1] = SH_REG(COMPUTE_PGM_LO);
	dw[2] = cs->adrlow;
	assert(!(cs->adrhigh&~0xff));
	dw[3] = cs->adrhigh;
	dw[4] = PKT3(SET_SH_REG, 2);
	dw[5] = SH_REG(COMPUTE_PGM_RSRC1);
	dw[6] = 63|(15<<6);
	dw[7] = (8<<1/*USER DATA TERMS*/)|(7<<7)|(1<<11);
	dw[8] = PKT3(SET_SH_REG, 1);
	dw[9] = SH_REG(COMPUTE_RESOURCE_LIMITS);
	dw[10] = 0;

	dw[11] = PKT3(SET_SH_REG, 3);
	dw[12] = SH_REG(COMPUTE_NUM_THREAD_X);
	dw[13] = cs->cs.block_size[0];/*THREAD_X*/
	dw[14] = cs->cs.block_size[1];/*THREAD_Y*/
	dw[15] = cs->cs.block_size[2];/*THREAD_Z*/
	
	dw[16] = PKT3(SET_SH_REG, 3);
	dw[17] = SH_REG(COMPUTE_START_X);
	dw[18] = 0;/*START_X*/
	dw[19] = 0;/*START_Y*/
	dw[20] = 0;/*START_Z*/
	
	__cmdbuf->ndw+=21;
	b->ndw = 21;
	rd_addtothelist(__ctx,cs->bo);
}
