/*
	runtime constants for shader
*/
#include "common.h"
#include "../../assert.h"
#include "../../io.h"
#include "../../string.h"
#include "reg.h"
#include "../compiler/ima/radeon.h"
#include "../shader.h"
char const static *vd_name = "USER DATA TERMS";
extern struct r_desc _rdesc_table[64];
/*
	for AMD, we can define constants like this. we can access the data at the address using a instruction.
	we can then load buffer resources constants that are stored in memory and write them to non-accessible SGPRs(USER_DATA 0-16)
*/
void rd_emit_userdata(struct rd_context *__ct,struct flue_shinfo *sh) {
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	struct rd_dbblock *b;
	b = rd_dbblock(__ct);
	b->ident = vd_name;

	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_SH_REG, sh->ud_offset+8);
	dw[1] = SH_REG(sh->reg);
	printf("EMITTING USER DATA TERMS: %x, terms to write: %u-dws\n",sh->reg,sh->ud_offset+8);
	/*
		this needs cleaning up.

		the first 2 QWORDS are for the desciptor table.
		the second lot is for the constants.

		so
		SGPR(0) to SGPR(3) = desciptor table
		SGPR(4) to SGPR(7) = constants
	*/
	struct rd_bo *bo = __ct->desc[__ct->ndesc-1];
	_64_u addr = bo->v->addr;
	assert(!(addr&0x3));
	dw[2] = addr&0xffffffff;
	dw[3] = addr>>32;
	printf("DESCRIPTORS - %x : %x.\n", addr>>32, addr&0xffffffff);

	bo = __ct->consts[__ct->nconsts-1];
	addr = bo->v->addr;
	dw[4] = 0xdead;
	dw[5] = 0xbeef;
	dw[6] = addr&0xffffffff;
	dw[7] = addr>>32;
	dw[8] = 0xdead;
	dw[9] = 0xbeef;
	dw+=10;

	_int_u i;
	i = 0;
	for(;i != __ct->d_offset;i++) {
		printf("terms - %x.\n",__ct->d_dw[i]);
	}

	if (sh->ud_offset>0)
		mem_cpy(dw,sh->user_data,sh->ud_offset*4);

	__cmdbuf->ndw += sh->ud_offset+10;
	b->ndw = sh->ud_offset+10;
}

/*
	create some buffer resource constants descriptors
	__info		  ; a desciption of the data
	__list		  ; the data
	__placement	 ; where its going to be placed within the table of descriptors

	NOTE:
		the resion for __placement is because we can have diffrent placements for example

		we can have the arrays predefined.
		meaning we wont have to say __info->placement = XXX

		total wast of time ^

		arrayA = 0,1,2 = shaderA usage
		arrayB = 2,1,0 = shaderB usage

	also __info stores are register number for where the user data terms are to be placed.
*/
#include "../compiler/ima/ima.h"
_64_u static r_rs_bits[] = {
	[FLUE_RS_BUFFER_VERTEX]			= IMA_INTR_K,
	[FLUE_RS_BUFFER]						= 0,
	[FLUE_RS_BUFFER_PRIM]							= IMA_PRIM
};

void
rd_resources(struct rd_context *__ct,struct rd_bo0 **__list, struct flue_resspec *__info, _int_u __n, _64_u *__placement) {
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	struct flue_shinfo *sh = __info->sh;

	/*
		here we are working in quad words
	*/
	_64_u *k;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		struct r_desc *desc;
		struct flue_resspec *spec = __info+i;
		assert(spec->desc>=FLUE_USERSLOT0 && spec->desc<FLUE_USERSLOT4);
		desc = &_rdesc_table[spec->desc];
		k = __ct->d_dw+(desc->val*4);
		struct rd_bo0 *b;
		b = __list[i];
		/*
			we have a max total of 16 dword slots avalable for use
			so about 4 bufer resources

			TODO:
				allocate user data section in memory
				to store all the desciptors then load then into SGPRS for use
				where user data is 1 of the max of 4 descriptors
		*/
		struct amdgpu_bo *a_= b->bo;
		struct rd_bo *bo = a_;
		rd_addtothelist(__ct, a_);
		/*
			BUFFER ADDRESSING

			offset = stride*(thred ID)
			stride = size of one vertex
		*/
		assert(!(bo->v->addr&0xff));
		assert(b->_0.size != 0);
		_64_u nfmt = RD_FMTGET(spec->fmt.nfmt,RD_NF_BUF);
		_64_u dfmt = RD_FMTGET(spec->fmt.dfmt,RD_DF_BUF);
		_int_u stride = __info[i].stride;

		flue_printf("VDESC: %x, PLACEMENT: %u\n", bo->v->addr,desc->val*16);
		/*
			if swizzle is disabled then num_records is in units of bytes else units of stride
		*/
		/*
			TODO:
				pull the descriptor from array of posable descriptors
				--- a template

				this will remove the pesty ~ spec->id == FLUE_RS_VERTEX?1:0
				FLUE_RS_VERTEX = vertex shader template descriptor
				! diffrent one?


		*/
		k[0] = BUF_RD_QW0(bo->v->addr,stride,0,0);
		k[1] = BUF_RD_QW1(
				(b->_0.size),
				SEL_R,
				SEL_G,
				SEL_B,
				SEL_A,
				nfmt,
				dfmt,
				0,/*dont care*/
				0,
				0/*spec->id == FLUE_RS_VERTEX?1:0*/,/*add thread ID*/
				0,
				0,
				0,
				0,
				0
		);

		_32_u *dw = k;
		printf("# %x - %x.\n",dw[0], dw[1]);
		printf("# %x - %x.\n",dw[2], dw[3]);
		flue_printf("RESOURCE BUFFER: format: %s_%s, size: %u, stride: %u\n",flue_nfmtstr(spec->fmt.nfmt,0),flue_dfmtstr(spec->fmt.dfmt,0),b->_0.size,stride);
		/*
			each desciptor is 4-dwords wide
			or just two quads
		*/
	}
	__ct->d_offset+=__n*4;
}

void rd_sampler(struct rd_context *__ct,struct flue_sampler *__list,  _int_u __n, _64_u *__placement) {
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	_64_u *k;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		struct r_desc *desc;
		desc = &_rdesc_table[__placement[i]];
		k = __ct->d_dw+(desc->sampler*4);
		struct flue_sampler *sm = __list+i; 
		k[0] = IMR_SAMP_QW0(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		k[1] = IMR_SAMP_QW1(0,0,0,0,0,0,0,0,0,0,0,0);
		flue_printf("SAMPLER-%u: %lx, %lx.\n",desc->sampler*16,k[0],k[1]);
	}
	__ct->d_offset+=__n*4;
}

void
rd_textures(struct rd_context *__ct,struct flue_tex **__list, struct flue_shinfo *__sh, _int_u __n, _64_u *__placement) {
	struct rd_cmd_buffer *__cmdbuf = __ct->cmdbuf;
	_64_u *k;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		struct r_desc *desc;
		desc = &_rdesc_table[__placement[i]];

		k = __ct->d_dw+(desc->val*4);
		struct flue_tex *tx = __list[i];
		struct rd_tex *tx0 = tx;
		struct rd_bo *bo;
		struct amdgpu_bo *_bo;
		if (!tx0->bo) {
			assert(1 == 0);
/*
			tx0->surf = rd_surf_new(__ct,tx->width,tx->height,&tx->fmt);
			bo = tx0->surf->bo;
			_bo = bo;
			rd_bo_map(bo);
			tx->mmap = _bo->cpu_ptr;

//bickering lots of it
			flue_texload(tx);*/
		}else {
			bo = tx0->bo;
			_bo = bo;
		}
		assert(tx->fmt.dfmt != NULL && tx->fmt.nfmt != NULL);
		rd_addtothelist(__ct,bo);
		/*
			NOTE:
				tile_index is GB_TILE_MODE
				GB_TILE_MODE is set by the kernel/amdgpu gfx ring
		*/
		printf("####################: %p, %p, %p\n",tx, tx->fmt.dfmt,tx->fmt.nfmt);
		_64_u dfmt = RD_FMTGET(tx->fmt.dfmt,RD_DF_IMG);
		_64_u nfmt = RD_FMTGET(tx->fmt.nfmt,RD_NF_IMG);
		assert(!(bo->v->addr&0xff));
		k[0] = IMR_QW0((bo->v->addr>>8)&0xffffffffff,0,dfmt,nfmt,0);
		k[1] = IMR_QW1(tx0->width-1,tx0->height-1,0,0,4,5,6,7,0,0,8,0,0,0,9);
		flue_printf("IMR_FORMAT: data: %u, number: %u, placement: %u\n",dfmt,nfmt,desc->val*16);
		flue_printf("IMR: addr: %x, width: %u, height: %u, format: %s_%s\n",bo->v->addr, tx0->width,tx0->height,flue_nfmtstr(tx->fmt.nfmt,0),flue_dfmtstr(tx->fmt.dfmt,0));
	}
	__ct->d_offset+=__n*4;
}
