void static context_reg_seq_array(rd_contextp __ct,_32_u __reg,_int_u __num,_32_u*__values){
	struct rd_cmd_buffer *cmdbuf = __ct->cmdbuf;
	_32_u *dw = cmdbuf->bo->cpu_ptr;
	dw+=cmdbuf->ndw;
	*dw = PKT3(SET_CONTEXT_REG, __num);
	dw[1] = CONTEXT_REG(__reg);

	_int_u i;
	i = 0;
	for(;i != __num;i++){
		dw[i+1] = __values[i];
	}

	cmdbuf->ndw+=__num+2;
}

void rd_clear_registers(rd_contextp __ct){
#define SET(array) (sizeof(array)/4), array
}

