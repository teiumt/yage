# include "common.h"
# include "reg.h"
#include "../../assert.h"
# include "../../io.h"
# include "../../string.h"
char const static *cb_name = "COLOUR-BUFFERS";
/*
	you can se here that we use __ctx and __cmdbuf
	the meaning behind this is that


	test_A(__ctx)
	test_B(__ctx)

	where each access __ctx->cmdbuf

	is worser then just passing the cmdbuf seperatly
*/

void rd_cb_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {	
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = cb_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	
	_int_u i;
	i = 0;
	struct rd_tex *cb;
	for(;i != __ctx->ncb;i++) {
		cb = __ctx->cb[i];
	
		*dw = PKT3(SET_CONTEXT_REG, 13);
		dw[1] = CONTEXT_REG(CB_COLOR0_BASE+(i*15));
		mem_cpy(dw+2, cb->buf, 13*4);

		printf("COLOUR-BUFFER##########: %x\n",CB_COLOR0_BASE+(i*15));
		_int_u j = 0;
		for(;j != 13;j++) {
			printf("DW%u: %x = %x\n", j, cb->buf[j], dw[2+j]);
		}
		rd_addtothelist(__ctx,cb->bo);
		dw+=15;
	}
	__cmdbuf->ndw+=__ctx->ncb*15;
	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(CB_TARGET_MASK);
	dw[2] = 0x0f;

	dw[3] = PKT3(SET_CONTEXT_REG, 1);
	dw[4] = CONTEXT_REG(CB_SHADER_MASK);
	dw[5] = 0x0f;

	dw[6] = PKT3(SET_CONTEXT_REG, 1);
	dw[7] = CONTEXT_REG(CB_COLOR_CONTROL);
	dw[8] = (1<<4)|(204<<16);

	dw[9] = PKT3(SET_CONTEXT_REG, 1);
	dw[10] = CONTEXT_REG(CB_COLOR0_DCC_BASE);
	dw[11] = 0;

	__cmdbuf->ndw+=12;
	b->ndw = (__ctx->ncb*15)+12;
}

char const static *vp_name = "VIEWPORT";
void rd_viewport_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
    b = rd_dbblock(__ctx);
    b->ident = vp_name;
    _32_u *dw = __cmdbuf->bo->cpu_ptr;
   	dw+=__cmdbuf->ndw;
	b->start = dw;
	_int_u dwc = __ctx->viewport_dwc;
	assert(dwc>0);
	assert(dwc<64);
	mem_cpy(dw,__ctx->viewport,dwc*4);
	__cmdbuf->ndw+=dwc;
	b->ndw = dwc;
}

char const static *db_name = "DEPTH-BOUNDS";
void rd_depth_bounds_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = db_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	float mn,mx;
	mn = -1;
	mx = 1;
	*dw = PKT3(SET_CONTEXT_REG, 2);
	dw[1] = CONTEXT_REG(DB_DEPTH_BOUNDS_MIN);
	dw[2] = *(_32_u*)&mn;//MIN
	dw[3] = *(_32_u*)&mx;//MAX
	__cmdbuf->ndw+=4;
	b->ndw = 4;
}

char const static *bl_name = "BLEND";
void rd_blend_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = bl_name;

	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 8);
	dw[1] = CONTEXT_REG(CB_BLEND0_CONTROL);

	dw[2] = (1<<31)|__ctx->blend[0];
	dw[3] = 1<<31;
	dw[4] = 1<<31;
	dw[5] = 1<<31;
	dw[6] = 1<<31;
	dw[7] = 1<<31;
	dw[8] = 1<<31;
	dw[9] = 1<<31;

	dw[10] = PKT3(SET_CONTEXT_REG, 1);
	dw[11] = CONTEXT_REG(DB_ALPHA_TO_MASK);
	dw[12] = (2<<8)|(2<<10)|(2<<12)|(2<<14);

	__cmdbuf->ndw+=13;
	b->ndw = 13;
}
char const static *st_name = "STENCIL";
void rd_stencil_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = st_name;
	
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(DB_STENCILREFMASK);
	dw[2] = 0;

	dw[3] = PKT3(SET_CONTEXT_REG, 1);
	dw[4] = CONTEXT_REG(DB_STENCILREFMASK_BF);
	dw[5] = 0;
	__cmdbuf->ndw+=6;
	b->ndw = 6;
}
char const static *dpb_name = "DEPTHBUFFER";
void rd_dpb_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_tex *__zb = __ctx->sh.zb;
	if (!__zb) return;
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = dpb_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 9);
   	dw[1] = CONTEXT_REG(DB_DEPTH_INFO);

	mem_cpy(dw+2, __zb->buf,9*4);

	dw[11] = PKT3(SET_CONTEXT_REG, 1);
	dw[12] = CONTEXT_REG(DB_DEPTH_VIEW);
	dw[13] = 0xffe000;
	__cmdbuf->ndw+=14;
	b->ndw = 14;
//CHANGED!
	rd_addtothelist(__ctx, __zb->bo);
}
char const static *fb_name = "FRAMEBUFFER";
void rd_framebuffer_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = fb_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(DB_RENDER_CONTROL);
	dw[2] = (1<<5)|(1<<6)/*disable compression for depth and stencil*/;

	dw[3] = PKT3(SET_CONTEXT_REG, 1);
	dw[4] = CONTEXT_REG(DB_RENDER_OVERRIDE);
	dw[5] = 2|(2<<2)|(2<<4)|(1<<0x10)|(1<<6);

	dw[6] = PKT3(SET_CONTEXT_REG, 1);
	dw[7] = CONTEXT_REG(DB_RENDER_OVERRIDE2);
	dw[8] = 1<<0xa;

	dw[9] = PKT3(SET_CONTEXT_REG, 1);
	dw[10] = CONTEXT_REG(PA_SC_WINDOW_SCISSOR_BR);
	dw[11] = __ctx->fb;

	dw[12] = PKT3(SET_CONTEXT_REG, 1);
	dw[13] = CONTEXT_REG(CB_DCC_CONTROL);
	dw[14] = 3;

	__cmdbuf->ndw+=15;
	b->ndw = 15;

}

char const static *rst_name = "RASTER";
void 
rd_raster_state_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = rst_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(PA_CL_CLIP_CNTL);
	dw[2] = (3<<0x1a);
	/*
		quick fix - if we are not using the zbuffer then 3d is not enabled, FIXME:
	*/
	if(!__ctx->sh.zb)
		dw[2] |= (1<<0x10)/*disable clipping*/;

	dw[3] = PKT3(SET_CONTEXT_REG, 1);
	dw[4] = CONTEXT_REG(SPI_INTERP_CONTROL_0);
	dw[5] = 1;

	dw[6] = PKT3(SET_CONTEXT_REG, 1);
	dw[7] = CONTEXT_REG(PA_SU_VTX_CNTL);
	dw[8] = (0x1)|(5<<3);

	dw[9] = PKT3(SET_CONTEXT_REG, 1);
	dw[10] = CONTEXT_REG(PA_SU_SC_MODE_CNTL);
	dw[11] = 0;

	dw[12] = PKT3(SET_CONTEXT_REG, 1);
	dw[13] = CONTEXT_REG(PA_SC_RASTER_CONFIG);
	dw[14] = RB_MAP_PKR0(2) | RB_XSEL2(1) | SE_MAP(2) |
			  SE_XSEL(1) | SE_YSEL(1);

	dw[15] = PKT3(SET_CONTEXT_REG, 1);
	dw[16] = CONTEXT_REG(PA_SC_RASTER_CONFIG_1);
	dw[17] = 0;

	__cmdbuf->ndw+=18;
	b->ndw = 18;

}

char const *sc_name = "SCISSORS";
void rd_scissors_emit(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = sc_name;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 4);
	dw[1] = CONTEXT_REG(PA_CL_GB_VERT_CLIP_ADJ);
	float def = 1;

	dw[2] = *(_32_u*)&def;/*PA_CL_GB_VERT_CLIP_ADJ*/
	dw[3] = *(_32_u*)&def;/*PA_CL_GB_VERT_DISC_ADJ*/
	dw[4] = *(_32_u*)&def;/*PA_CL_GB_HORZ_CLIP_ADJ*/
	dw[5] = *(_32_u*)&def;/*PA_CL_GB_HORZ_DISC_ADJ*/

	__cmdbuf->ndw+=6;
	b->ndw = 6;
}


void rd_multisample_state(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;

	*dw = PKT3(SET_CONTEXT_REG, 2);
	dw[1] = CONTEXT_REG(PA_SC_AA_MASK_X0Y0_X1Y0);
	dw[2] = ~0;
	dw[3] = ~0;	

	dw[4] = PKT3(SET_CONTEXT_REG, 1);
	dw[5] = CONTEXT_REG(PA_SC_LINE_CNTL);
	dw[6] = 0;

	dw[7] = PKT3(SET_CONTEXT_REG,2);
	dw[8] = CONTEXT_REG(PA_SC_MODE_CNTL_0);
	dw[9] = 0;
	dw[10] = 0;

	dw[11] = PKT3(SET_CONTEXT_REG,1);
	dw[12] = CONTEXT_REG(DB_EQAA);
	dw[13] = (1<<0x15);

	dw[14] = PKT3(SET_CONTEXT_REG, 1);
	dw[15] = CONTEXT_REG(PA_SC_LINE_STIPPLE);
	dw[16] = 0;

	dw[17] = PKT3(SET_CONTEXT_REG,2);
	dw[18] = CONTEXT_REG(PA_SC_CENTROID_PRIORITY_0);
	dw[19] = 0;
	dw[20] = 0;

	dw[21] = PKT3(SET_CONTEXT_REG, 1);
	dw[22] = CONTEXT_REG(PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y0_0);
	dw[23] = 0;

	dw[24] = PKT3(SET_CONTEXT_REG, 1);
	dw[25] = CONTEXT_REG(PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y0_0);
	dw[26] = 0;

	dw[27] = PKT3(SET_CONTEXT_REG, 1);
	dw[28] = CONTEXT_REG(PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y1_0);
	dw[29] = 0;

	dw[30] = PKT3(SET_CONTEXT_REG, 1);
	dw[31] = CONTEXT_REG(PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y1_0);
	dw[32] = 0;
	__cmdbuf->ndw+=33;

}
void rd_common_state(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
/*
	docs show multible viewports are available for use but only a signle enable register?
*/
	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(PA_CL_VTE_CNTL);
	dw[2] = 63|(1<<10);

	dw[3] = PKT3(SET_CONTEXT_REG, 1);
	dw[4] = CONTEXT_REG(SPI_SHADER_POS_FORMAT); 
	dw[5] = 4;

	//ac_choose_spi_color_formats
	dw[6] = PKT3(SET_CONTEXT_REG, 1);
	dw[7] = CONTEXT_REG(SPI_SHADER_COL_FORMAT);
	dw[8] = 4;

	/*
		as we dont need this GS_MODE=GS_OFF
		else GS_MODE=A
	*/
	dw[9] = PKT3(SET_CONTEXT_REG, 1);
	dw[10] = CONTEXT_REG(VGT_PRIMITIVEID_EN);
	dw[11] = 1;
	
	dw[12] = PKT3(SET_CONTEXT_REG, 1);
	dw[13] = CONTEXT_REG(VGT_PRIMITIVEID_RESET);
	dw[14] = 0;

	dw[15] = PKT3(SET_CONTEXT_REG, 1);
	dw[16] = CONTEXT_REG(VGT_VTX_CNT_EN);
	dw[17] = 0;

	dw[18] = PKT3(SET_CONTEXT_REG, 1);
	dw[19] = CONTEXT_REG(PA_SC_AA_CONFIG);
	dw[20] = 0;

	dw[21] = PKT3(SET_CONTEXT_REG, 1);
	dw[22] = CONTEXT_REG(VGT_MULTI_PRIM_IB_RESET_INDX);
	dw[23] = 0;

	dw[24] = PKT3(SET_CONTEXT_REG, 1);
	dw[25] = CONTEXT_REG(VGT_MULTI_PRIM_IB_RESET_EN);
	dw[26] = 0;

	dw[27] = PKT3(SET_CONTEXT_REG, 1);
	dw[28] = CONTEXT_REG(DB_COUNT_CONTROL);
	dw[29] = 0;

	dw[30] = PKT3(SET_CONTEXT_REG, 1);
	dw[31] = CONTEXT_REG(VGT_GS_MODE);
	dw[32] = 1;/*GS_OFF*/
	
	dw[33] = PKT3(SET_CONTEXT_REG, 1);
	dw[34] = CONTEXT_REG(DB_HTILE_DATA_BASE);
	dw[35] = 0x41;
	
	dw[36] = PKT3(SET_CONTEXT_REG, 2);
	dw[37] = CONTEXT_REG(VGT_INSTANCE_STEP_RATE_0);
	dw[38] = 1;
	dw[39] = 0;

	dw[40] = PKT3(SET_CONTEXT_REG, 4);
	dw[41] = CONTEXT_REG(CB_BLEND_RED);
	float blend = 0;
	dw[42] = *(_32_u*)&blend;
	dw[43] = *(_32_u*)&blend;
	dw[44] = *(_32_u*)&blend;
	dw[45] = *(_32_u*)&blend;
	
	dw[46] = PKT3(SET_CONTEXT_REG, 1);
	dw[47] = CONTEXT_REG(VGT_STRMOUT_CONFIG);
	dw[48] = 0;
	
	dw[49] = PKT3(SET_CONTEXT_REG, 1);
	dw[50] = CONTEXT_REG(PA_CL_NANINF_CNTL);
	dw[51] = 0;

	float x = 1.0, y = 0;
	dw[52] = PKT3(SET_CONTEXT_REG, 6);
	dw[53] = CONTEXT_REG(PA_SU_POLY_OFFSET_DB_FMT_CNTL);
	dw[54] = ((-23)&0xff)|1<<8;
	dw[55] = *(_32_u*)&x;
	dw[56] = *(_32_u*)&x;
	dw[57] = *(_32_u*)&y;
	dw[58] = *(_32_u*)&x;
	dw[59] = *(_32_u*)&y;
	
	dw[60] = PKT3(SET_CONTEXT_REG, 1);
	dw[61] = CONTEXT_REG(DB_HTILE_SURFACE);
	dw[62] = 0;

	dw[63] = PKT3(SET_CONTEXT_REG, 2);
	dw[64] = CONTEXT_REG(DB_SRESULTS_COMPARE_STATE0);
	dw[65] = 0;
	dw[66] = 0;

	__cmdbuf->ndw+=67;
}

void static
rd_depth_state(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;

	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(DB_DEPTH_CONTROL);
/*
	TODO:

		disable this for vekas as will cause issue
		method to disable like blending
*/
#ifdef __vekas
	dw[2] = 0;
#else
	dw[2] = _A200_Z_EN|_A200_ZFUNC_GREATER|_A200_Z_WRITE_EN|(7<<0x8)|(7<<0x14)|(1<<0x7);//|_A200_DEPTH_BOUNDS_EN;
	if (!__ctx->sh.zb)
		dw[2] = 0;
#endif
	dw[3] = PKT3(SET_CONTEXT_REG, 1);
	dw[4] = CONTEXT_REG(DB_STENCIL_CONTROL);
	dw[5] = 0;
	__cmdbuf->ndw+=6;
}

void static
rd_depth_clear(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;

	*dw = PKT3(SET_CONTEXT_REG, 2);
	dw[1] = CONTEXT_REG(DB_STENCIL_CLEAR);
	float clear = 0;
	dw[2] = *(float*)&clear;/*DB_STENCIL_CLEAR*/
	dw[3] = *(float*)&clear;/*DB_DEPTH_CLEAR*/
	__cmdbuf->ndw+=4;
}
#define emit_state(__ctx,__cmdbuf,__bit,__func)\
	if(((__ctx)->bits&(__bit))){\
		__func(__ctx,__cmdbuf);\
		(__ctx)->bits &= ~(__bit);\
	}
char const static* state_names[] = {
	"RD_FRAMEBUFFER_DIRTY",
	"RD_DPB_DIRTY",
	"RD_COLOURBUFFER_DIRTY",
	"RD_VIEWPORT_DIRTY",
	"RD_STENCIL_DIRTY",
	"RD_DEPTHC_DIRTY",
	"RD_DEPTH_DIRTY",
	"RD_MULTISAMPLE_DIRTY",
	"RD_RASTER_DIRTY",
	"RD_SCISSORS_DIRTY",
	"RD_BLEND_DIRTY",
	"RD_DEPTH_BOUNDS_DIRTY",
	NULL
};
char const *init_name = "INIT";
void static
print_states(_64_u __bits){
	flue_printf("STATE{");
	char const *name;
	_int_u i;
	for(i = 0;(name = state_names[i]) != NULL;i++){
		_64_u r;
		r = __bits>>(i+1);
		char c;
		if(!r){
			c = '}';
		}else{
			c = ',';
		}

		if(__bits&(1<<i)){
			flue_printf("%s%c",name,c);
		}
	}
	flue_printf("\n");
}

void rd_init_state(rd_contextp __ctx, struct rd_cmd_buffer *__cmdbuf) {
	print_states(__ctx->bits);
	struct rd_dbblock *b;
	b = rd_dbblock(__ctx);
	b->ident = init_name;

	/*
		set up context state
	*/
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	b->start = dw;
	*dw = PKT3(SET_CONTEXT_REG, 1);
	dw[1] = CONTEXT_REG(VGT_SHADER_STAGES_EN);
	dw[2] = 0;

	dw[3] = PKT3(SET_CONTEXT_REG, 2);
	dw[4] = CONTEXT_REG(PA_SC_SCREEN_SCISSOR_TL);
	dw[5] = 1<<31;
	dw[6] = 16384|(16384<<16);


	dw[7] = PKT3(SET_CONTEXT_REG, 2);
	dw[8] = CONTEXT_REG(PA_SC_GENERIC_SCISSOR_TL);
	dw[9] = 1<<31;
	dw[10] = 16384|(16384<<16);
	
	dw[11] = PKT3(SET_CONTEXT_REG, 1);
	dw[12] = CONTEXT_REG(PA_SC_WINDOW_SCISSOR_TL);
	dw[13] = 1<<31;

	unsigned tmp = (unsigned)(1.0*8.0);
	dw[14] = PKT3(SET_CONTEXT_REG, 2);
	dw[15] = CONTEXT_REG(PA_SU_POINT_SIZE);
	dw[16] = tmp|(tmp<<16);/*PA_SU_POINT_SIZE*/
	dw[17] = 0|0xffff<<16;/*PA_SU_POINT_MINMAX*/

	dw[18] = PKT3(SET_CONTEXT_REG, 1);
	dw[19] = CONTEXT_REG(PA_SC_EDGERULE);
	dw[20] = 0xaaaaaaaa;

	dw[21] = PKT3(SET_CONTEXT_REG, 3);
	dw[22] = CONTEXT_REG(VGT_MAX_VTX_INDX);
	dw[23] = ~0;/*VGT_MAX_VTX_INDX*/
	dw[24] = 0;/*VGT_MIN_VTX_INDX*/
	dw[25] = 0;/*VGT_INDX_OFFSET*/

	dw[26] = PKT3(SET_CONTEXT_REG, 1);
	dw[27] = CONTEXT_REG(PA_SU_PRIM_FILTER_CNTL);
	dw[28] = 0;

	dw[29] = PKT3(SET_CONTEXT_REG, 1);
	dw[30] = CONTEXT_REG(PA_SC_CLIPRECT_RULE);
	dw[31] = 0xffff;
	
	dw[32] = PKT3(SET_CONTEXT_REG, 1);
	dw[33] = CONTEXT_REG(IA_MULTI_VGT_PARAM);
	dw[34] = 0;
	__cmdbuf->ndw+=35;
	b->ndw = 35;

	emit_state(__ctx,__cmdbuf,RD_COLOURBUFFER_DIRTY,rd_cb_state_emit);
	rd_common_state(__ctx,__cmdbuf);
	emit_state(__ctx,__cmdbuf,RD_DEPTH_BOUNDS_DIRTY,rd_depth_bounds_emit);
	emit_state(__ctx,__cmdbuf,RD_BLEND_DIRTY,rd_blend_state_emit);
	emit_state(__ctx,__cmdbuf,RD_SCISSORS_DIRTY,rd_scissors_emit);

	rd_raster_state_emit(__ctx,__cmdbuf);
	rd_multisample_state(__ctx,__cmdbuf);
	emit_state(__ctx,__cmdbuf,RD_DEPTH_DIRTY,rd_depth_state);
	emit_state(__ctx,__cmdbuf,RD_DEPTHC_DIRTY,rd_depth_clear);
	emit_state(__ctx,__cmdbuf,RD_VIEWPORT_DIRTY,rd_viewport_emit);
	emit_state(__ctx,__cmdbuf,RD_STENCIL_DIRTY,rd_stencil_state_emit);	
	emit_state(__ctx,__cmdbuf,RD_DPB_DIRTY,rd_dpb_state_emit);
	emit_state(__ctx,__cmdbuf,RD_FRAMEBUFFER_DIRTY,rd_framebuffer_state_emit);
}
