#include "common.h"
#include "../../m_alloc.h"
/*name to relieve?

	consept behind this is to free the contents of the texture without completly removing it.
	a light weight destroy.

	lets say we are creating and destorying texture repeatedly, this strips out the most time consuming stuff.
	ie dirty destroy. saying along the line if its yours now, i dont want it.
*/
void rd_tex_relinquish(struct rd_tex *__tx) {
	_64_u config = ((flue_texp)__tx)->config;
	flue_printf("byebye texture{width: %u, height: %u, format: %s_%s}.\n",__tx->width,__tx->height,flue_nfmtstr(__tx->fmt.nfmt,0),flue_dfmtstr(__tx->fmt.dfmt,0));
	rd_bo_destroy(__tx->bo);
}

void rd_tex_destroy(struct rd_tex *__tx) {
	rd_tex_relinquish(__tx);
}

void rd_maptex(struct rd_context *__ctx) {
	_int_u i;

	i = 0;
	struct rd_tex *tx;
	for(;i != __ctx->sh.m.ntex;i++) {
		tx = __ctx->sh.m.tp[i];

	}
	__ctx->sh.m.ntex = 0;
}
