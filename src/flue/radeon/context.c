# include "common.h"
# include "../../m_alloc.h"
# include "../../io.h"
#include "../../assert.h"
#include "reg.h"
void rd_nowhere_func(rd_contextp __c,struct rd_cmd_buffer*__b) {}
static struct rd_reg_range context_range[] = {
	//FRAMEBUFFER
	{//0
		CONTEXT_REG(DB_RENDER_CONTROL),
		4
	},
	{//1
		CONTEXT_REG(DB_RENDER_OVERRIDE),
		4
	},
	{//2
		CONTEXT_REG(DB_RENDER_OVERRIDE2),
		4
	},
	{//3
		CONTEXT_REG(PA_SC_WINDOW_SCISSOR_BR),
		4
	},
	{//4
		CONTEXT_REG(CB_DCC_CONTROL),
		4
	},
	//VIEWPORT
	{//5
		CONTEXT_REG(PA_CL_VPORT_XSCALE),
		4*6
	},
	{//6
		CONTEXT_REG(PA_SC_VPORT_SCISSOR_0_TL),
		4*2
	},
	{//7
		CONTEXT_REG(PA_SC_VPORT_ZMIN_0),
		4*2
	},
	//DEPTH BUFFER
	{//8
		CONTEXT_REG(DB_DEPTH_INFO),
		4*9
	},
	{//9
		CONTEXT_REG(DB_DEPTH_VIEW),
		4
	},
	//STENCIL
	{//10
		CONTEXT_REG(DB_STENCILREFMASK),
		4
	},
	{//11
		CONTEXT_REG(DB_STENCILREFMASK_BF),
		4
	},
	//DEPTH CLEAR
	{//12
		CONTEXT_REG(DB_STENCIL_CLEAR),
		4*2
	},
	//DEPTH
	{//13
		CONTEXT_REG(DB_DEPTH_CONTROL),
		4
	},
	{//14
		CONTEXT_REG(DB_STENCIL_CONTROL),
		4
	},
	//SCISSORS
	{//15
		CONTEXT_REG(PA_CL_GB_VERT_CLIP_ADJ),
		4*4
	},
	//BLEND
	{//16
		CONTEXT_REG(CB_BLEND0_CONTROL),
		4*8
	},
	{//17
		CONTEXT_REG(DB_ALPHA_TO_MASK),
		4
	},
	//DEPTH BOUNDS
	{//18
		CONTEXT_REG(DB_DEPTH_BOUNDS_MIN),
		4*2
	},
	//COLOUR BUFFER
	{//19
		CONTEXT_REG(CB_TARGET_MASK),
		4
	},
	{//20
		CONTEXT_REG(CB_SHADER_MASK),
		4
	},
	{//21
		CONTEXT_REG(CB_COLOR_CONTROL),
		4
	},
	{//22
		CONTEXT_REG(CB_COLOR0_DCC_BASE),
		4
	},
	{//23
		CONTEXT_REG(CB_COLOR0_BASE),
		4*15*8
	}
};
void rd_load_reg(rd_contextp __ct){
	struct rd_cmd_buffer *cmdbuf = __ct->cmdbuf;
	_64_u addr;
	_int_u nr = 24;
	addr = __ct->shadow->v->addr;
	_32_u *dw = cmdbuf->bo->cpu_ptr;
	dw+=cmdbuf->ndw;
	dw[0] = PKT3(LOAD_CONTEXT_REG,(2+nr*2)-1);
	dw[1] = addr&0xffffffff;
	dw[2] = addr>>32;
	flue_printf("LOAD_REG: LOW: %x, HIGH: %x.\n",dw[1],dw[2]);
	_int_u i;
	i = 0;
	for(;i != nr;i++){
		struct rd_reg_range *r;
		r = context_range+i;
		_int_u off;
		off = i*2+3;
//		flue_printf("RANGE: %u, %u.\n",r->offset,r->words);
		dw[off]		= r->offset;
		dw[off+1]	= r->words/4;
	}

	cmdbuf->ndw+=3+(nr*2);
}

void rd_printf_load_reg(rd_contextp ct){
	return;
	rd_bo_map(ct->shadow);
	_32_u *d;
	d = ct->shadow->bo.cpu_ptr;
	_int_u nr = 24;
	_int_u i;
	i = 0;
	for(;i != nr;i++){
		struct rd_reg_range *r;
		r = context_range+i;
		_int_u w;
		w = 0;
		for(;w != r->words/4;w++){
			flue_printf("WORD[%u]: %u.\n",r->offset+w,d[r->offset+w]);
		}
	}


	rd_bo_unmap(ct->shadow);
}


rd_contextp rd_ctx_new(struct rd_dev *__d) {
	assert(__d->dev != NULL);
	rd_contextp ctx;
	ctx = (rd_contextp)m_alloc(sizeof(struct rd_context));
	ctx->d = __d;
	
	printf("RD-ctx_new.\n");
	ctx->ctx = amdgpu_ctx_create(__d->dev, 0);
	if (!ctx->ctx) {
		printf("failed to create AMDGPU context.\n");
		return NULL;
	}
	ctx->nconsts = 0;
	ctx->ndesc = 0;
	f_lhash_init(&ctx->ht);
	ctx->d_dw = m_alloc(RD_DESCMAX*8);
	ctx->viewport_dwc = 0;
	ctx->blend[0] = 0;
	ctx->d_offset = 0;
	ctx->cmdbuf = rd_cmd_buffer_new(__d);
	ctx->shadow = rd_bo_create(__d->dev,
		SHADOWED_REG_BUFFER_SIZE,
		0,
		AMDGPU_GEM_DOMAIN_VRAM,
		AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED,
		AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE

	);
	rd_bo_map(ctx->shadow);
	mem_set(ctx->shadow->bo.cpu_ptr,0x00,SHADOWED_REG_BUFFER_SIZE);
	rd_bo_unmap(ctx->shadow);
	/*
		will be reset back to zero after first state push
	*/
	ctx->cmdbuf->ndw = 0;
	ctx->list_top = NULL;	
	struct rd_state *s = ctx->sh.states;
	_int_u i;
	for(i = 0;i != STATE_MAX;i++) {

		s++;
	}
	ctx->zb = NULL;
	ctx->bits = RD_DIRTY;
	ctx->list_size = 0;
	ctx->top = NULL;
	ctx->bk = NULL;
	ctx->f0 = rd_nowhere_func;
	ctx->cache_bits = 0;
	return ctx;
}

void rd_ctx_destroy(rd_contextp __ctx) {
	printf("RD-ctx_destroy.\n");
	rd_cmd_buffer_destroy(__ctx->cmdbuf);
	amdgpu_ctx_free(__ctx->ctx);
	m_free(__ctx);
}
