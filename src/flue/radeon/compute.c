#include "common.h"
#include "reg.h"
#include "../../ffly_def.h"
#include "../compiler/ima/ima.h"
#include "../../io.h"
#include "../../ys/as/as.h"
#include "../../bbs.h"
#include "../../string.h"
#include "../../ihc/ihc.h"
#include "../../tools.h"
#include "../../assert.h"
#include "../compiler/ima/radeon.h"
static struct rd_shader clear_shader;
void rd_shader_alloc_memory(struct rd_dev *d, struct rd_shader *sh, _int_u __size);

#define VGPR(__x) __x
#define SGPR(__x) (19+(__x))

void static setup_gprs(void){
	_int_u i;
	i = 0;
	for(;i != 19;i++){
		ima_regmap[i] = i|R_REG_STAMP(_R_VGPR);
	}
	i = 0;
	for(;i != 19;i++){
		ima_regmap[19+i] = i|R_REG_STAMP(_R_SGPR);
	}
}

static struct r_desc image = {
	.val = 0,
	.bits = _r_image_ndf
};
void rd_cs_init(struct rd_dev *d){
	r_desc_table = &image;
	struct ima_produce prod;
	ima_pd = &prod;
	ima_init();
	ima_ys();
	struct bbs_node *text;
	text = bbsn_new();
	ihc_bbs(&prod.file,text);

	struct ima_instr i;
	i.op = _ima_op_mov;
	i.info = _ima_op_mov_4;
	/*
		NOTE:
			MIMG-VADDR is a vector
	*/

	setup_gprs();	
	
	i.r[0].type = _ima_instr_em_const_int;
	i.r[0].val = VGPR(0); 
	i.l[0].val = VGPR(4);
	i.l[1].val = VGPR(5);
	i.l[2].val = VGPR(6);
	i.l[3].val = VGPR(7);
	rd_instr_emit(&i);

	/**/

	struct ima_instr i0;
	i0.op = _ima_op_mov;
	i0.info = 0;
	i0.r[0].type = _ima_instr_em_reg;
	i0.r[0].val = SGPR(8);
	i0.l[0].val = VGPR(4);

	rd_instr_emit(&i0);

	i0.r[0].val = SGPR(9);
	i0.l[0].val = VGPR(5);

	rd_instr_emit(&i0);

	/**/
	i0.op = _ima_op_mulu16;
	i0.dest = VGPR(4);
	i0.l[0].type = _ima_instr_em_const_int;
	i0.l[0].val = 16;
	i0.r[0].val = VGPR(4);
	
	rd_instr_emit(&i0);
	i0.dest = VGPR(5);
	i0.r[0].val = VGPR(5);
	rd_instr_emit(&i0);

	/**/

	i0.op = _ima_op_add32u;
	i0.dest = VGPR(4);
	i0.l[0].type = _ima_instr_em_reg;
	i0.l[0].val = VGPR(0);
	i0.r[0].val = VGPR(4);

	rd_instr_emit(&i0);
	
	i0.dest = VGPR(5);
	i0.l[0].val = VGPR(1);
	i0.r[0].val = VGPR(5);
	rd_instr_emit(&i0);

	/**/
	i.info = 0;
	i.r[0].type = _ima_instr_em_reg;
	i.r[0].val = SGPR(4);
	i.l[0].val = VGPR(0);
	rd_instr_emit(&i);
	i.r[0].val = SGPR(5);
	i.l[0].val = VGPR(1);
	rd_instr_emit(&i);
	i.r[0].val = SGPR(6);
	i.l[0].val = VGPR(2);
	rd_instr_emit(&i);
	i.r[0].val = SGPR(7);
	i.l[0].val = VGPR(3);
	rd_instr_emit(&i);
	
	struct ima_instr ii;
	ii.desc = 0;
	ii.r->val = VGPR(4);
	ii.l->val = VGPR(0);
	ii.bits = 0;
	ii.op = _ima_op_image_store;
	rd_instr_emit(&ii);
	rd_instr_exit(NULL);
	r_write_regs();
	r_emit_instrs(&r_ys);
	*(text->src+text->ptr) = '\0';
	printf("%s.\n",text->src);

	struct bbs_node *bin;
	bin = bbsn_new();
	ys_msel = ys_radeon;
	ys_init();

	as.limit = text->ptr;
	as.rw_ops = ith0;
	as.in_ctx = text;
	as.out_ctx = bin;
	struct ys_state state;
	ysa_assemble(&state);

	rd_shader_alloc_memory(d,&clear_shader,512*8);
	mem_cpy(clear_shader.secondary,bin->src,bin->ptr);
	assert(bin->ptr>0);
	ffly_hexdump(bin->src,bin->ptr);
	bbsn_destroy(bin);
	bbsn_destroy(text);
	ihc_cleanup();
	ys_de_init();
	clear_shader.cs.block_size[0] = 16;
	clear_shader.cs.block_size[1] = 16;
	clear_shader.cs.block_size[2] = 1;
}

void rd_cs_despatch(rd_contextp __ctx,struct rd_shader *__sh, _int_u __x, _int_u __y,_int_u __z){
	struct rd_cmd_buffer *__cmdbuf = __ctx->cmdbuf;
	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;

  *dw = PKT3(CONTEXT_CONTROL, 1);
  dw[1] = (1<<31);
  dw[2] = 0;

  //  ensure all defaults are set

  dw[3] = PKT3(CLEAR_STATE, 0);
  dw[4] = 0;
	dw+=5;
	
	*dw = PKT3(DISPATCH_DIRECT,3)|SHADER_TYPE_COMPUTE;
	dw[1] = __x/16;/*DIM_X*/
	dw[2] = __y/16;/*DIM_Y*/
	dw[3] = __z;/*DIM_Z*/
	dw[4] = B800_COMPUTE_ENA|B800_FORCE_START_000;/*DISPATCH_INITIATOR*/
	
	dw[5] = PKT3(EVENT_WRITE,0);
	dw[6] = EVENT_TYPE(7)|EVENT_INDEX(4);
	__cmdbuf->ndw+=7+5;
}
void
rd_submit(void);
void rd_shader_compute(rd_contextp __ctx,struct rd_shader *cs);
void rd_cs_buffer_clear(rd_contextp __ctx,struct rd_tex *__tex, struct rd_bo *__bo, float r, float g, float b, float a){
	struct rd_cmd_buffer *__cmdbuf = __ctx->cmdbuf;

	_64_u width,height;
	width	= __tex->width;
	height	= __tex->height;
    _64_u nfmt = RD_FMTGET(__tex->fmt.nfmt,RD_NF_COLOUR);
    _64_u dfmt = RD_FMTGET(__tex->fmt.dfmt,RD_DF_COLOUR);

	if (nfmt == 9) {
		nfmt = 4;
	}
	if (dfmt == 4) {
		width>>=2;
		dfmt = 14;
	}

	_32_u *dw = __cmdbuf->bo->cpu_ptr;
	dw+=__cmdbuf->ndw;
	*dw = PKT3(SET_SH_REG, 8);
	dw[1] = SH_REG(COMPUTE_USER_DATA_0);
	_64_u *q = dw+2;
	*q		= IMR_QW0((__bo->v->addr>>8)&0xffffffffff,0,dfmt,nfmt,0);
	q[1]	= IMR_QW1(width-1,height-1,0,0,4,5,6,7,0,0,8,0,0,0,9); 
	
	*(float*)(dw+6) = r;
	*(float*)(dw+7) = g;
	*(float*)(dw+8) = b;
	*(float*)(dw+9) = a;

	__cmdbuf->ndw+=10;

	rd_shader_compute(__ctx,&clear_shader);	
	rd_cs_despatch(__ctx,&clear_shader,width,height,1);
/*
	fences need to be put in place for this, im too lazy to do this now sooooo dont care;
	i mean multiable cmdbufs need to be put in place and this.
*/
	rd_submit();
}
