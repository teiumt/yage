# ifndef __radeon__reg__h
# define __radeon__reg__h
#define QW _64_u
#define BUF_RD_QW0(__badr, __stride, __cs, __se)\
    ((((QW)(__badr))&0xffffffffffff)|((QW)(__stride)<<48)|((QW)(__cs)<<62)|((QW)(__se)<<63))
#define BUF_RD_QW1(__nr, __dsx, __dsy, __dsz, __dsw, __nf, __df, __es, __is, __ate, __atc, __he, __h, __mt, __ty)\
    ((QW)(__nr)|((QW)(__dsx)<<32)|((QW)(__dsy)<<35)|((QW)(__dsz)<<38)|((QW)(__dsw)<<41)|((QW)(__nf)<<44)\
    |((QW)(__df)<<47)|((QW)(__es)<<51)|((QW)(__is)<<53)|((QW)(__ate)<<55)|((QW)(__atc)<<56)|((QW)(__he)<<57)\
    |((QW)(__h)<<58)|((QW)(__mt)<<59)|((QW)(__ty)<<62))

#define IMR_QW0(__addr,__minlod,__dfmt,__nfmt,__mtyp)\
    ((QW)(__addr)|((QW)(__minlod)<<40)|((QW)(__dfmt)<<52)|((QW)(__nfmt)<<58)|((QW)(__mtyp)<<62))
#define IMR_QW1(__width,__height,__perfmod,__interlace,__selx,__sely,__selz,__selw,__baselv,__lastlv,__tileidx,__pow2,__mtyp,__atc,__type)\
    ((QW)(__width)|((QW)(__height)<<14)|((QW)(__perfmod)<<28)|((QW)(__interlace)<<31)|((QW)(__selx)<<32)|((QW)(__sely)<<35)|((QW)(__selz)<<38)|((QW)(__selw)<<41)|((QW)(__baselv)<<44)|((QW)(__lastlv)<<48)|((QW)(__tileidx)<<52)|((QW)(__pow2)<<57)|((QW)(__mtyp)<<58)|((QW)(__atc)<<59)|((QW)(__type)<<60))
#define IMR_QW2(__depth,__pitch,__base_array,__last_array)\
    ((QW)(__depth)|((QW)(__pitch)<<13)|((QW)(__base_array)<<32)|((QW)(__last_array)<<45))
#define IMR_QW3(__minlodwarn)\
    ((QW)(__minlodwarn))


#define IMR_SAMP_QW0(__clmx,__clmy,__clmz,__mx_aniso_ratio,__dp_compfunc,__force_unorm,__aniso_thres,__mc_coord_trunc,__force_degam,__aniso_bias,__trunc_coord,__no_cubewrap,__filter_mode,__compat_mode,__minlod,__maxlod,__perfmip,__perfz)\
	( (QW)(__clmx)|( ((QW)(__clmy))<<3 )|( ((QW)(__clmz))<<6 )|( ((QW)(__mx_aniso_ratio))<<8 )|( ((QW)(__dp_compfunc))<<12 )|( ((QW)(__force_unorm))<<15 )|( ((QW)(__aniso_thres))<<16 )|( ((QW)(__mc_coord_trunc))<<19 )\
	|( ((QW)(__force_degam))<<20 )|( ((QW)(__aniso_bias))<<21 )|( ((QW)(__trunc_coord))<<27 )|( ((QW)(__no_cubewrap))<<28 )|( ((QW)(__filter_mode))<<29 )|( ((QW)(__compat_mode))<<31 )|( ((QW)(__minlod))<<32 )\
	|( ((QW)(__maxlod))<<44 )|( ((QW)(__perfmip))<<56 )|( ((QW)(__perfz))<<60 ) )


#define IMR_SAMP_QW1(__lod_bias,__lod_bias_sec,__xy_mag_filter,__xy_min_filter,__z_filter,__mip_filter,__mip_point_preclm,__no_lsb_ceil,__filter_prec_fix,__aniso_ovr,__bdr_colptr,__bdr_coltyp)\
	( (QW)(__lod_bias)|( ((QW)(__lod_bias_sec))<<14 )|( ((QW)(__xy_mag_filter))<<20 )|( ((QW)(__xy_min_filter))<<22 )|( ((QW)(__z_filter))<<24 )|( ((QW)(__mip_filter))<<26 )|( ((QW)(__mip_point_preclm))<<28 )\
	|( ((QW)(__no_lsb_ceil))<<29 )|( ((QW)(__filter_prec_fix))<<30 )|( ((QW)(__aniso_ovr))<<31 )|( ((QW)(__bdr_colptr))<<32 )|( ((QW)(__bdr_coltyp))<<62 ) )

#define _A203_Z_EXPORT_EN	1
#define _A200_Z_EN				(1<<1)
#define _A200_ZFUNC_NEVER		(0<<4)
#define _A200_ZFUNC_LESS		(1<<4)
#define _A200_ZFUNC_EQUAL		(2<<4)
#define _A200_ZFUNC_LESSEQUAL	(3<<4)
#define _A200_ZFUNC_GREATER		(4<<4)
#define _A200_ZFUNC_ALWAYS		(7<<4)
#define DB_HTILE_DATA_BASE                                                    0xa005
#define DB_HTILE_SURFACE                                                      0xa2af
#define DB_PRELOAD_CONTROL                                                    0xa2b2
#define DB_SRESULTS_COMPARE_STATE0                                            0xa2b0
#define DB_SRESULTS_COMPARE_STATE1                                            0xa2b1

#define _A200_Z_WRITE_EN		(1<<2)
#define _A200_DEPTH_BOUNDS_EN	(1<<3)
#define _A010_Z_INVALID	0
#define _A010_Z_16		1
#define _A010_Z_24		2
#define _A010_Z_32_FLOAT		3

#define _A206_VTX_XY_FMT	(1<<8)
#define _A206_VTX_Z_FMT		(1<<9)
#define _A206_VTX_W0_FMT	(1<<10)
#define _MRT_BLEND_CLAMP (1<<15)
#define _MRT_BLEND_BYPASS (1<<16)
#define VGT_VERTEX_REUSE_BLOCK_CNTL                                           0xa316
#define IA_MULTI_VGT_PARAM                                                    0xa2aa
#define SEL_R 4
#define SEL_G 5
#define SEL_B 6
#define SEL_A 7

#define CB_DCC_CONTROL                                                        0xa109
#define VGT_STRMOUT_CONFIG                                                    0xa2e5
#define PA_CL_NANINF_CNTL                                                     0xa208

#define PA_SC_ENHANCE                                                         0x22fc

#define PA_SU_POLY_OFFSET_DB_FMT_CNTL                                         0xa2de
#define PA_SU_POLY_OFFSET_CLAMP                                               0xa2df
#define PA_SU_POLY_OFFSET_FRONT_SCALE                                         0xa2e0
#define PA_SU_POLY_OFFSET_FRONT_OFFSET                                        0xa2e1
#define PA_SU_POLY_OFFSET_BACK_SCALE                                          0xa2e2
#define PA_SU_POLY_OFFSET_BACK_OFFSET                                         0xa2e3
// pixel shader
#define SPI_SHADER_PGM_LO_PS			0x2c08
#define SPI_SHADER_PGM_HI_PS			0x2c09
#define SPI_SHADER_PGM_RSRC1_PS			0x2c0a
#define SPI_SHADER_PGM_RSRC2_PS			0x2c0b
#define SPI_SHADER_PGM_RSRC3_PS			0x2c07
#define SPI_SHADER_USER_DATA_PS_0		0x2c0c
#define SPI_SHADER_USER_DATA_PS_1		0x2c0d
#define SPI_SHADER_USER_DATA_PS_2		0x2c0e
#define SPI_SHADER_USER_DATA_PS_3		0x2c0f
#define SPI_SHADER_USER_DATA_PS_4		0x2c10
#define SPI_SHADER_USER_DATA_PS_5		0x2c11
#define SPI_SHADER_USER_DATA_PS_6		0x2c12
#define SPI_SHADER_USER_DATA_PS_7		0x2c13
#define SPI_SHADER_USER_DATA_PS_8		0x2c14
#define SPI_SHADER_USER_DATA_PS_9		0x2c15
#define SPI_SHADER_USER_DATA_PS_10		0x2c16
#define SPI_SHADER_USER_DATA_PS_11		0x2c17
#define SPI_SHADER_USER_DATA_PS_12		0x2c18
#define SPI_SHADER_USER_DATA_PS_13		0x2c19
#define SPI_SHADER_USER_DATA_PS_14		0x2c1a
#define SPI_SHADER_USER_DATA_PS_15		0x2c1b

#define VGT_SHADER_STAGES_EN                                                  0xa2d5


#define SPI_PS_INPUT_CNTL_0		  0xa191
#define SPI_PS_INPUT_CNTL_1		  0xa192
#define SPI_PS_INPUT_CNTL_2		  0xa193
#define SPI_PS_INPUT_CNTL_3		  0xa194
#define SPI_PS_INPUT_CNTL_4		  0xa195
#define SPI_PS_INPUT_CNTL_5		  0xa196
#define SPI_PS_INPUT_CNTL_6		  0xa197
#define SPI_PS_INPUT_CNTL_7		  0xa198
#define SPI_PS_INPUT_CNTL_8		  0xa199
#define SPI_PS_INPUT_CNTL_9		  0xa19a
#define SPI_PS_INPUT_CNTL_10		 0xa19b
#define SPI_PS_INPUT_CNTL_11		 0xa19c
#define SPI_PS_INPUT_CNTL_12		 0xa19d
#define SPI_PS_INPUT_CNTL_13		 0xa19e
#define SPI_PS_INPUT_CNTL_14		 0xa19f
#define SPI_PS_INPUT_CNTL_15		 0xa1a0
#define SPI_PS_INPUT_CNTL_16		 0xa1a1
#define SPI_PS_INPUT_CNTL_17		 0xa1a2
#define SPI_PS_INPUT_CNTL_18		 0xa1a3
#define SPI_PS_INPUT_CNTL_19		 0xa1a4
#define SPI_PS_INPUT_CNTL_20		 0xa1a5
#define SPI_PS_INPUT_CNTL_21		 0xa1a6
#define SPI_PS_INPUT_CNTL_22		 0xa1a7
#define SPI_PS_INPUT_CNTL_23		 0xa1a8
#define SPI_PS_INPUT_CNTL_24		 0xa1a9
#define SPI_PS_INPUT_CNTL_25		 0xa1aa
#define SPI_PS_INPUT_CNTL_26		 0xa1ab
#define SPI_PS_INPUT_CNTL_27		 0xa1ac
#define SPI_PS_INPUT_CNTL_28		 0xa1ad
#define SPI_PS_INPUT_CNTL_29		 0xa1ae
#define SPI_PS_INPUT_CNTL_30		 0xa1af
#define SPI_PS_INPUT_CNTL_31		 0xa1b0

#define VGT_MULTI_PRIM_IB_RESET_INDX                                          0xa103
#define VGT_MULTI_PRIM_IB_RESET_EN                                            0xa2a5


#define SPI_VS_OUT_CONFIG                                                     0xa1b1
#define SPI_PS_INPUT_ENA                                                      0xa1b3
#define SPI_PS_INPUT_ADDR                                                     0xa1b4
#define SPI_INTERP_CONTROL_0                                                  0xa1b5
#define SPI_PS_IN_CONTROL                                                     0xa1b6
#define SPI_BARYC_CNTL                                                        0xa1b8
#define SPI_TMPRING_SIZE                                                      0xa1ba
#define SPI_SHADER_POS_FORMAT                                                 0xa1c3
#define SPI_SHADER_Z_FORMAT                                                   0xa1c4
#define SPI_SHADER_COL_FORMAT                                                 0xa1c5


// vertex shader
#define SPI_SHADER_PGM_LO_VS			0x2c48
#define SPI_SHADER_PGM_HI_VS			0x2c49
#define SPI_SHADER_PGM_RSRC1_VS			0x2c4a
#define SPI_SHADER_PGM_RSRC2_VS			0x2c4b
#define SPI_SHADER_PGM_RSRC3_VS			0x2c46
#define SPI_SHADER_LATE_ALLOC_VS		0x2c47
#define SPI_SHADER_USER_DATA_VS_0		0x2c4c
#define SPI_SHADER_USER_DATA_VS_1		0x2c4d
#define SPI_SHADER_USER_DATA_VS_2		0x2c4e
#define SPI_SHADER_USER_DATA_VS_3		0x2c4f
#define SPI_SHADER_USER_DATA_VS_4		0x2c50
#define SPI_SHADER_USER_DATA_VS_5		0x2c51
#define SPI_SHADER_USER_DATA_VS_6		0x2c52
#define SPI_SHADER_USER_DATA_VS_7		0x2c53
#define SPI_SHADER_USER_DATA_VS_8		0x2c54
#define SPI_SHADER_USER_DATA_VS_9		0x2c55
#define SPI_SHADER_USER_DATA_VS_10		0x2c56
#define SPI_SHADER_USER_DATA_VS_11		0x2c57
#define SPI_SHADER_USER_DATA_VS_12		0x2c58
#define SPI_SHADER_USER_DATA_VS_13		0x2c59
#define SPI_SHADER_USER_DATA_VS_14		0x2c5a
#define SPI_SHADER_USER_DATA_VS_15		0x2c5b

#define PA_CL_VS_OUT_CNTL				0xa207

//color buffer
#define CB_COLOR0_BASE					0xa318
#define CB_COLOR1_BASE					0xa327
#define CB_COLOR2_BASE					0xa336
#define CB_COLOR3_BASE					0xa345
#define CB_COLOR4_BASE					0xa354
#define CB_COLOR5_BASE					0xa363
#define CB_COLOR6_BASE					0xa372
#define CB_COLOR7_BASE					0xa381
#define CB_COLOR0_PITCH					0xa319
#define CB_COLOR1_PITCH					0xa328
#define CB_COLOR2_PITCH					0xa337
#define CB_COLOR3_PITCH					0xa346
#define CB_COLOR4_PITCH					0xa355
#define CB_COLOR5_PITCH					0xa364
#define CB_COLOR6_PITCH					0xa373
#define CB_COLOR7_PITCH					0xa382
#define CB_COLOR0_SLICE					0xa31a
#define CB_COLOR1_SLICE					0xa329
#define CB_COLOR2_SLICE					0xa338
#define CB_COLOR3_SLICE					0xa347
#define CB_COLOR4_SLICE					0xa356
#define CB_COLOR5_SLICE					0xa365
#define CB_COLOR6_SLICE					0xa374
#define CB_COLOR7_SLICE					0xa383
#define CB_COLOR0_VIEW					0xa31b
#define CB_COLOR1_VIEW					0xa32a
#define CB_COLOR2_VIEW					0xa339
#define CB_COLOR3_VIEW					0xa348
#define CB_COLOR4_VIEW					0xa357
#define CB_COLOR5_VIEW					0xa366
#define CB_COLOR6_VIEW					0xa375
#define CB_COLOR7_VIEW					0xa384
#define CB_COLOR0_INFO					0xa31c
#define CB_COLOR1_INFO					0xa32b
#define CB_COLOR2_INFO					0xa33a
#define CB_COLOR3_INFO					0xa349
#define CB_COLOR4_INFO					0xa358
#define CB_COLOR5_INFO					0xa367
#define CB_COLOR6_INFO					0xa376
#define CB_COLOR7_INFO					0xa385
#define CB_COLOR0_ATTRIB				0xa31d
#define CB_COLOR1_ATTRIB				0xa32c
#define CB_COLOR2_ATTRIB				0xa33b
#define CB_COLOR3_ATTRIB				0xa34a
#define CB_COLOR4_ATTRIB				0xa359
#define CB_COLOR5_ATTRIB				0xa368
#define CB_COLOR6_ATTRIB				0xa377
#define CB_COLOR7_ATTRIB				0xa386
/*
#define mmCB_COLOR0_DCC_CONTROL		0xa31e
#define mmCB_COLOR1_DCC_CONTROL		0xa32d
#define mmCB_COLOR2_DCC_CONTROL		0xa33c
#define mmCB_COLOR3_DCC_CONTROL		0xa34b
#define mmCB_COLOR4_DCC_CONTROL		0xa35a
#define mmCB_COLOR5_DCC_CONTROL		0xa369
#define mmCB_COLOR6_DCC_CONTROL		0xa378
#define mmCB_COLOR7_DCC_CONTROL		0xa387
*/

#define CB_COLOR0_CMASK					0xa31f
#define CB_COLOR1_CMASK					0xa32e
#define CB_COLOR2_CMASK					0xa33d
#define CB_COLOR3_CMASK					0xa34c
#define CB_COLOR4_CMASK					0xa35b
#define CB_COLOR5_CMASK					0xa36a
#define CB_COLOR6_CMASK					0xa379
#define CB_COLOR7_CMASK					0xa388
#define CB_COLOR0_CMASK_SLICE			0xa320
#define CB_COLOR1_CMASK_SLICE			0xa32f
#define CB_COLOR2_CMASK_SLICE			0xa33e
#define CB_COLOR3_CMASK_SLICE			0xa34d
#define CB_COLOR4_CMASK_SLICE			0xa35c
#define CB_COLOR5_CMASK_SLICE			0xa36b
#define CB_COLOR6_CMASK_SLICE			0xa37a
#define CB_COLOR7_CMASK_SLICE			0xa389
#define CB_COLOR0_FMASK					0xa321
#define CB_COLOR1_FMASK					0xa330
#define CB_COLOR2_FMASK					0xa33f
#define CB_COLOR3_FMASK					0xa34e
#define CB_COLOR4_FMASK					0xa35d
#define CB_COLOR5_FMASK					0xa36c
#define CB_COLOR6_FMASK					0xa37b
#define CB_COLOR7_FMASK					0xa38a
#define CB_COLOR0_FMASK_SLICE			0xa322
#define CB_COLOR1_FMASK_SLICE			0xa331
#define CB_COLOR2_FMASK_SLICE			0xa340
#define CB_COLOR3_FMASK_SLICE			0xa34f
#define CB_COLOR4_FMASK_SLICE			0xa35e
#define CB_COLOR5_FMASK_SLICE			0xa36d
#define CB_COLOR6_FMASK_SLICE			0xa37c
#define CB_COLOR7_FMASK_SLICE			0xa38b
#define CB_COLOR0_CLEAR_WORD0			0xa323
#define CB_COLOR1_CLEAR_WORD0			0xa332
#define CB_COLOR2_CLEAR_WORD0			0xa341
#define CB_COLOR3_CLEAR_WORD0			0xa350
#define CB_COLOR4_CLEAR_WORD0			0xa35f
#define CB_COLOR5_CLEAR_WORD0			0xa36e
#define CB_COLOR6_CLEAR_WORD0			0xa37d
#define CB_COLOR7_CLEAR_WORD0			0xa38c
#define CB_COLOR0_CLEAR_WORD1			0xa324
#define CB_COLOR1_CLEAR_WORD1			0xa333
#define CB_COLOR2_CLEAR_WORD1			0xa342
#define CB_COLOR3_CLEAR_WORD1			0xa351
#define CB_COLOR4_CLEAR_WORD1			0xa360
#define CB_COLOR5_CLEAR_WORD1			0xa36f
#define CB_COLOR6_CLEAR_WORD1			0xa37e
#define CB_COLOR7_CLEAR_WORD1			0xa38d
#define CB_COLOR0_DCC_BASE                                                    0xa325
#define CB_COLOR1_DCC_BASE                                                    0xa334
#define CB_COLOR2_DCC_BASE                                                    0xa343
#define CB_COLOR3_DCC_BASE                                                    0xa352
#define CB_COLOR4_DCC_BASE                                                    0xa361
#define CB_COLOR5_DCC_BASE                                                    0xa370
#define CB_COLOR6_DCC_BASE                                                    0xa37f
#define CB_COLOR7_DCC_BASE                                                    0xa38e
#define CB_BLEND_RED					0xa105
#define CB_BLEND_GREEN					0xa106
#define CB_BLEND_BLUE					0xa107
#define CB_BLEND_ALPHA					0xa108
#define CB_TARGET_MASK				0xa08e
#define CB_SHADER_MASK				0xa08f

#define VGT_GS_MODE                                                           0xa290

#define CB_COLOR_CONTROL				0xa202
#define CB_BLEND0_CONTROL				0xa1e0
#define CB_BLEND1_CONTROL				0xa1e1
#define CB_BLEND2_CONTROL				0xa1e2
#define CB_BLEND3_CONTROL				0xa1e3
#define CB_BLEND4_CONTROL				0xa1e4
#define CB_BLEND5_CONTROL				0xa1e5
#define CB_BLEND6_CONTROL				0xa1e6
#define CB_BLEND7_CONTROL				0xa1e7
#define DB_RENDER_CONTROL				0xa000
#define DB_COUNT_CONTROL				0xa001
#define DB_RENDER_OVERRIDE				0xa003
#define DB_RENDER_OVERRIDE2				0xa004

#define DB_DEPTH_BOUNDS_MIN                                                   0xa008
#define DB_DEPTH_BOUNDS_MAX                                                   0xa009
#define DB_STENCIL_CLEAR                                                      0xa00a
#define DB_DEPTH_CLEAR                                                        0xa00b


#define DB_DEPTH_INFO                                                         0xa00f
#define DB_Z_INFO                                                             0xa010
#define DB_STENCIL_INFO                                                       0xa011
#define DB_Z_READ_BASE                                                        0xa012
#define DB_STENCIL_READ_BASE                                                  0xa013
#define DB_Z_WRITE_BASE                                                       0xa014
#define DB_STENCIL_WRITE_BASE                                                 0xa015
#define DB_DEPTH_SIZE                                                         0xa016

#define DB_DEPTH_SLICE                                                        0xa017
#define DB_DEPTH_VIEW                                                         0xa002

#define DB_EQAA                                                               0xa201
#define DB_SHADER_CONTROL                                                     0xa203
#define DB_STENCILREFMASK                                                     0xa10c
#define DB_STENCILREFMASK_BF                                                  0xa10d

#define PA_CL_VPORT_XSCALE                                                    0xa10f
#define PA_CL_VPORT_XOFFSET                                                   0xa110
#define PA_CL_VPORT_YSCALE                                                    0xa111
#define PA_CL_VPORT_YOFFSET                                                   0xa112
#define PA_CL_VPORT_ZSCALE                                                    0xa113
#define PA_CL_VPORT_ZOFFSET                                                   0xa114
#define PA_CL_VPORT_XSCALE_1                                                  0xa115
#define PA_CL_VPORT_XSCALE_2                                                  0xa11b
#define PA_CL_VPORT_XSCALE_3                                                  0xa121
#define PA_CL_VPORT_XSCALE_4                                                  0xa127
#define PA_CL_VPORT_XSCALE_5                                                  0xa12d
#define PA_CL_VPORT_XSCALE_6                                                  0xa133
#define PA_CL_VPORT_XSCALE_7                                                  0xa139
#define PA_CL_VPORT_XSCALE_8                                                  0xa13f
#define PA_CL_VPORT_XSCALE_9                                                  0xa145
#define PA_CL_VPORT_XSCALE_10                                                 0xa14b
#define PA_CL_VPORT_XSCALE_11                                                 0xa151
#define PA_CL_VPORT_XSCALE_12                                                 0xa157
#define PA_CL_VPORT_XSCALE_13                                                 0xa15d
#define PA_CL_VPORT_XSCALE_14                                                 0xa163
#define PA_CL_VPORT_XSCALE_15                                                 0xa169
#define PA_CL_VPORT_XOFFSET_1                                                 0xa116
#define PA_CL_VPORT_XOFFSET_2                                                 0xa11c
#define PA_CL_VPORT_XOFFSET_3                                                 0xa122
#define PA_CL_VPORT_XOFFSET_4                                                 0xa128
#define PA_CL_VPORT_XOFFSET_5                                                 0xa12e
#define PA_CL_VPORT_XOFFSET_6                                                 0xa134
#define PA_CL_VPORT_XOFFSET_7                                                 0xa13a
#define PA_CL_VPORT_XOFFSET_8                                                 0xa140
#define PA_CL_VPORT_XOFFSET_9                                                 0xa146
#define PA_CL_VPORT_XOFFSET_10                                                0xa14c
#define PA_CL_VPORT_XOFFSET_11                                                0xa152
#define PA_CL_VPORT_XOFFSET_12                                                0xa158
#define PA_CL_VPORT_XOFFSET_13                                                0xa15e
#define PA_CL_VPORT_XOFFSET_14                                                0xa164
#define PA_CL_VPORT_XOFFSET_15                                                0xa16a

#define PA_CL_VPORT_YSCALE_1                                                  0xa117
#define PA_CL_VPORT_YSCALE_2                                                  0xa11d
#define PA_CL_VPORT_YSCALE_3                                                  0xa123
#define PA_CL_VPORT_YSCALE_4                                                  0xa129
#define PA_CL_VPORT_YSCALE_5                                                  0xa12f
#define PA_CL_VPORT_YSCALE_6                                                  0xa135
#define PA_CL_VPORT_YSCALE_7                                                  0xa13b
#define PA_CL_VPORT_YSCALE_8                                                  0xa141
#define PA_CL_VPORT_YSCALE_9                                                  0xa147
#define PA_CL_VPORT_YSCALE_10                                                 0xa14d
#define PA_CL_VPORT_YSCALE_11                                                 0xa153
#define PA_CL_VPORT_YSCALE_12                                                 0xa159
#define PA_CL_VPORT_YSCALE_13                                                 0xa15f
#define PA_CL_VPORT_YSCALE_14                                                 0xa165
#define PA_CL_VPORT_YSCALE_15                                                 0xa16b
#define PA_CL_VPORT_YOFFSET_1                                                 0xa118
#define PA_CL_VPORT_YOFFSET_2                                                 0xa11e
#define PA_CL_VPORT_YOFFSET_3                                                 0xa124
#define PA_CL_VPORT_YOFFSET_4                                                 0xa12a
#define PA_CL_VPORT_YOFFSET_5                                                 0xa130
#define PA_CL_VPORT_YOFFSET_6                                                 0xa136
#define PA_CL_VPORT_YOFFSET_7                                                 0xa13c
#define PA_CL_VPORT_YOFFSET_8                                                 0xa142
#define PA_CL_VPORT_YOFFSET_9                                                 0xa148
#define PA_CL_VPORT_YOFFSET_10                                                0xa14e
#define PA_CL_VPORT_YOFFSET_11                                                0xa154
#define PA_CL_VPORT_YOFFSET_12                                                0xa15a
#define PA_CL_VPORT_YOFFSET_13                                                0xa160
#define PA_CL_VPORT_YOFFSET_14                                                0xa166
#define PA_CL_VPORT_YOFFSET_15                                                0xa16c
#define PA_CL_VPORT_ZSCALE_1                                                  0xa119
#define PA_CL_VPORT_ZSCALE_2                                                  0xa11f
#define PA_CL_VPORT_ZSCALE_3                                                  0xa125
#define PA_CL_VPORT_ZSCALE_4                                                  0xa12b
#define PA_CL_VPORT_ZSCALE_5                                                  0xa131
#define PA_CL_VPORT_ZSCALE_6                                                  0xa137
#define PA_CL_VPORT_ZSCALE_7                                                  0xa13d
#define PA_CL_VPORT_ZSCALE_8                                                  0xa143
#define PA_CL_VPORT_ZSCALE_9                                                  0xa149
#define PA_CL_VPORT_ZSCALE_10                                                 0xa14f
#define PA_CL_VPORT_ZSCALE_11                                                 0xa155
#define PA_CL_VPORT_ZSCALE_12                                                 0xa15b
#define PA_CL_VPORT_ZSCALE_13                                                 0xa161
#define PA_CL_VPORT_ZSCALE_14                                                 0xa167
#define PA_CL_VPORT_ZSCALE_15                                                 0xa16d
#define PA_CL_VPORT_ZOFFSET_1                                                 0xa11a
#define PA_CL_VPORT_ZOFFSET_2                                                 0xa120
#define PA_CL_VPORT_ZOFFSET_3                                                 0xa126
#define PA_CL_VPORT_ZOFFSET_4                                                 0xa12c
#define PA_CL_VPORT_ZOFFSET_5                                                 0xa132
#define PA_CL_VPORT_ZOFFSET_6                                                 0xa138
#define PA_CL_VPORT_ZOFFSET_7                                                 0xa13e
#define PA_CL_VPORT_ZOFFSET_8                                                 0xa144
#define PA_CL_VPORT_ZOFFSET_9                                                 0xa14a
#define PA_CL_VPORT_ZOFFSET_10                                                0xa150
#define PA_CL_VPORT_ZOFFSET_11                                                0xa156
#define PA_CL_VPORT_ZOFFSET_12                                                0xa15c
#define PA_CL_VPORT_ZOFFSET_13                                                0xa162
#define PA_CL_VPORT_ZOFFSET_14                                                0xa168
#define PA_CL_VPORT_ZOFFSET_15                                                0xa16e

#define PA_SC_VPORT_SCISSOR_0_TL                                              0xa094
#define PA_SC_VPORT_SCISSOR_1_TL                                              0xa096
#define PA_SC_VPORT_SCISSOR_2_TL                                              0xa098
#define PA_SC_VPORT_SCISSOR_3_TL                                              0xa09a
#define PA_SC_VPORT_SCISSOR_4_TL                                              0xa09c
#define PA_SC_VPORT_SCISSOR_5_TL                                              0xa09e
#define PA_SC_VPORT_SCISSOR_6_TL                                              0xa0a0
#define PA_SC_VPORT_SCISSOR_7_TL                                              0xa0a2
#define PA_SC_VPORT_SCISSOR_8_TL                                              0xa0a4
#define PA_SC_VPORT_SCISSOR_9_TL                                              0xa0a6
#define PA_SC_VPORT_SCISSOR_10_TL                                             0xa0a8
#define PA_SC_VPORT_SCISSOR_11_TL                                             0xa0aa
#define PA_SC_VPORT_SCISSOR_12_TL                                             0xa0ac
#define PA_SC_VPORT_SCISSOR_13_TL                                             0xa0ae
#define PA_SC_VPORT_SCISSOR_14_TL                                             0xa0b0
#define PA_SC_VPORT_SCISSOR_15_TL                                             0xa0b2
#define PA_SC_VPORT_SCISSOR_0_BR                                              0xa095
#define PA_SC_VPORT_SCISSOR_1_BR                                              0xa097
#define PA_SC_VPORT_SCISSOR_2_BR                                              0xa099
#define PA_SC_VPORT_SCISSOR_3_BR                                              0xa09b
#define PA_SC_VPORT_SCISSOR_4_BR                                              0xa09d
#define PA_SC_VPORT_SCISSOR_5_BR                                              0xa09f
#define PA_SC_VPORT_SCISSOR_6_BR                                              0xa0a1
#define PA_SC_VPORT_SCISSOR_7_BR                                              0xa0a3
#define PA_SC_VPORT_SCISSOR_8_BR                                              0xa0a5
#define PA_SC_VPORT_SCISSOR_9_BR                                              0xa0a7
#define PA_SC_VPORT_SCISSOR_10_BR                                             0xa0a9
#define PA_SC_VPORT_SCISSOR_11_BR                                             0xa0ab
#define PA_SC_VPORT_SCISSOR_12_BR                                             0xa0ad
#define PA_SC_VPORT_SCISSOR_13_BR                                             0xa0af
#define PA_SC_VPORT_SCISSOR_14_BR                                             0xa0b1
#define PA_SC_VPORT_SCISSOR_15_BR                                             0xa0b3

#define PA_SC_VPORT_ZMIN_0                                                    0xa0b4
#define PA_SC_VPORT_ZMIN_1                                                    0xa0b6
#define PA_SC_VPORT_ZMIN_2                                                    0xa0b8
#define PA_SC_VPORT_ZMIN_3                                                    0xa0ba
#define PA_SC_VPORT_ZMIN_4                                                    0xa0bc
#define PA_SC_VPORT_ZMIN_5                                                    0xa0be
#define PA_SC_VPORT_ZMIN_6                                                    0xa0c0
#define PA_SC_VPORT_ZMIN_7                                                    0xa0c2
#define PA_SC_VPORT_ZMIN_8                                                    0xa0c4
#define PA_SC_VPORT_ZMIN_9                                                    0xa0c6
#define PA_SC_VPORT_ZMIN_10                                                   0xa0c8
#define PA_SC_VPORT_ZMIN_11                                                   0xa0ca
#define PA_SC_VPORT_ZMIN_12                                                   0xa0cc
#define PA_SC_VPORT_ZMIN_13                                                   0xa0ce
#define PA_SC_VPORT_ZMIN_14                                                   0xa0d0
#define PA_SC_VPORT_ZMIN_15                                                   0xa0d2
#define PA_SC_VPORT_ZMAX_0                                                    0xa0b5
#define PA_SC_VPORT_ZMAX_1                                                    0xa0b7
#define PA_SC_VPORT_ZMAX_2                                                    0xa0b9
#define PA_SC_VPORT_ZMAX_3                                                    0xa0bb
#define PA_SC_VPORT_ZMAX_4                                                    0xa0bd
#define PA_SC_VPORT_ZMAX_5                                                    0xa0bf
#define PA_SC_VPORT_ZMAX_6                                                    0xa0c1
#define PA_SC_VPORT_ZMAX_7                                                    0xa0c3
#define PA_SC_VPORT_ZMAX_8                                                    0xa0c5
#define PA_SC_VPORT_ZMAX_9                                                    0xa0c7
#define PA_SC_VPORT_ZMAX_10                                                   0xa0c9
#define PA_SC_VPORT_ZMAX_11                                                   0xa0cb
#define PA_SC_VPORT_ZMAX_12                                                   0xa0cd
#define PA_SC_VPORT_ZMAX_13                                                   0xa0cf
#define PA_SC_VPORT_ZMAX_14                                                   0xa0d1
#define PA_SC_VPORT_ZMAX_15                                                   0xa0d3

#define PA_SC_AA_CONFIG                                                       0xa2f8
#define PA_SC_AA_MASK_X0Y0_X1Y0                                               0xa30e
#define PA_SC_AA_MASK_X0Y1_X1Y1                                               0xa30f
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y0_0                                     0xa2fe
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y0_1                                     0xa2ff
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y0_2                                     0xa300
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y0_3                                     0xa301
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y0_0                                     0xa302
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y0_1                                     0xa303
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y0_2                                     0xa304
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y0_3                                     0xa305
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y1_0                                     0xa306
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y1_1                                     0xa307
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y1_2                                     0xa308
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X0Y1_3                                     0xa309
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y1_0                                     0xa30a
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y1_1                                     0xa30b
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y1_2                                     0xa30c
#define PA_SC_AA_SAMPLE_LOCS_PIXEL_X1Y1_3                                     0xa30d
#define PA_SC_CENTROID_PRIORITY_0                                             0xa2f5
#define PA_SC_CENTROID_PRIORITY_1                                             0xa2f6
#define PA_SC_CLIPRECT_0_TL                                                   0xa084
#define PA_SC_CLIPRECT_0_BR                                                   0xa085
#define PA_SC_CLIPRECT_1_TL                                                   0xa086
#define PA_SC_CLIPRECT_1_BR                                                   0xa087
#define PA_SC_CLIPRECT_2_TL                                                   0xa088
#define PA_SC_CLIPRECT_2_BR                                                   0xa089
#define PA_SC_CLIPRECT_3_TL                                                   0xa08a
#define PA_SC_CLIPRECT_3_BR                                                   0xa08b
#define PA_SC_CLIPRECT_RULE                                                   0xa083

#define PA_CL_VTE_CNTL                                                        0xa206

#define PA_SU_PRIM_FILTER_CNTL                                                0xa20b
#define VGT_INSTANCE_STEP_RATE_0                                              0xa2a8
#define VGT_INSTANCE_STEP_RATE_1                                              0xa2a9


#define VGT_MAX_VTX_INDX                                                      0xa100
#define VGT_MIN_VTX_INDX                                                      0xa101
#define VGT_INDX_OFFSET                                                       0xa102



#define PA_CL_CLIP_CNTL                                                       0xa204
#define PA_CL_GB_VERT_CLIP_ADJ                                                0xa2fa
#define PA_CL_GB_VERT_DISC_ADJ                                                0xa2fb
#define PA_CL_GB_HORZ_CLIP_ADJ                                                0xa2fc
#define PA_CL_GB_HORZ_DISC_ADJ                                                0xa2fd

#define PA_SU_VTX_CNTL                                                        0xa2f9
#define PA_SU_POINT_SIZE                                                      0xa280
#define PA_SU_POINT_MINMAX                                                    0xa281

#define PA_SU_SC_MODE_CNTL                                                    0xa205

#define PA_SC_GENERIC_SCISSOR_TL                                              0xa090
#define PA_SC_GENERIC_SCISSOR_BR                                              0xa091
#define PA_SC_SCREEN_SCISSOR_TL                                               0xa00c
#define PA_SC_SCREEN_SCISSOR_BR                                               0xa00d
#define PA_SC_WINDOW_OFFSET                                                   0xa080
#define PA_SC_WINDOW_SCISSOR_TL                                               0xa081
#define PA_SC_WINDOW_SCISSOR_BR                                               0xa082
#define CONTEXT_REG(__reg) ((__reg)-0xa000)
#define SH_REG(__reg) ((__reg)-0x2c00)
#define LOAD_CONTEXT_REG      0x61
#define SET_CONTEXT_REG 0x69
#define CONTEXT_CONTROL 0x28
#define SET_SH_REG          0x76
#define CLEAR_STATE 0x12
#define INDEX_TYPE              0x2a
#define SET_UCONFIG_REG 0x79
#define VGT_IDX_32  1
#define DI_PT_TRILIST		0x4
#define DI_PT_TRIFAN		0x5
#define DI_PT_TRISTRIP		0x6
#define DI_PT_RECTLIST		0x11
#define DI_PT_QUADLIST		0x13
#define DI_PT_POLYGON		0x15

/*
	PULLED from radeonsi,

	i have no idea about this,
	from looking at the amdgpu linux driver
	SI_CONTEXT_REG_END should be 0xa400 but is 0xc000 ?
*/

/* si values */
#define SI_CONFIG_REG_OFFSET       0x00008000
#define SI_CONFIG_REG_END          0x0000B000
#define SI_SH_REG_OFFSET           0x0000B000
#define SI_SH_REG_END              0x0000C000
#define SI_CONTEXT_REG_OFFSET      0x00028000
#define SI_CONTEXT_REG_END         0x00030000
#define CIK_UCONFIG_REG_OFFSET     0x00030000
#define CIK_UCONFIG_REG_END        0x00040000
#define SI_UCONFIG_PERF_REG_OFFSET 0x00034000
#define SI_UCONFIG_PERF_REG_END    0x00038000

/* For register shadowing: */
#define SH_REG_SPACE_SIZE           (SI_SH_REG_END - SI_SH_REG_OFFSET)
#define CONTEXT_REG_SPACE_SIZE      (SI_CONTEXT_REG_END - SI_CONTEXT_REG_OFFSET)
#define UCONFIG_REG_SPACE_SIZE      (CIK_UCONFIG_REG_END - CIK_UCONFIG_REG_OFFSET)

#define SHADOWED_REG_BUFFER_SIZE (SH_REG_SPACE_SIZE+CONTEXT_REG_SPACE_SIZE+UCONFIG_REG_SPACE_SIZE)


/*pkt3 types*/
#define DISPATCH_DIRECT             0x15
#define DRAW_INDEX_AUTO             0x2d
#define DRAW_INDEX_2                0x27
#define NUM_INST               0x2f
#define VGT_PRIM_TYPE                                                    0xc242
#define VGT_PRIMITIVEID_EN                                                    0xa2a1
#define VGT_PRIMITIVEID_RESET                                                 0xa2a3
#define VGT_VTX_CNT_EN                                                        0xa2ae
#define VGT_REUSE_OFF                                                         0xa2ad

/* PA_SC_RASTER_CONFIG mask */
#define RB_MAP_PKR0(x)              ((x) << 0)
#define RB_MAP_PKR0_MASK            (0x3 << 0)
#define RB_MAP_PKR1(x)              ((x) << 2)
#define RB_MAP_PKR1_MASK            (0x3 << 2)
#define RB_XSEL2(x)             ((x) << 4)
#define RB_XSEL2_MASK               (0x3 << 4)
#define RB_XSEL                 (1 << 6)
#define RB_YSEL                 (1 << 7)
#define PKR_MAP(x)              ((x) << 8)
#define PKR_MAP_MASK                (0x3 << 8)
#define PKR_XSEL(x)             ((x) << 10)
#define PKR_XSEL_MASK               (0x3 << 10)
#define PKR_YSEL(x)             ((x) << 12)
#define PKR_YSEL_MASK               (0x3 << 12)
#define SC_MAP(x)               ((x) << 16)
#define SC_MAP_MASK             (0x3 << 16)
#define SC_XSEL(x)              ((x) << 18)
#define SC_XSEL_MASK                (0x3 << 18)
#define SC_YSEL(x)              ((x) << 20)
#define SC_YSEL_MASK                (0x3 << 20)
#define SE_MAP(x)               ((x) << 24)
#define SE_MAP_MASK             (0x3 << 24)
#define SE_XSEL(x)              ((x) << 26)
#define SE_XSEL_MASK                (0x3 << 26)
#define SE_YSEL(x)              ((x) << 28)
#define SE_YSEL_MASK                (0x3 << 28)

/* PA_SC_RASTER_CONFIG_1 mask */
#define SE_PAIR_MAP(x)              ((x) << 0)
#define SE_PAIR_MAP_MASK            (0x3 << 0)
#define SE_PAIR_XSEL(x)             ((x) << 2)
#define SE_PAIR_XSEL_MASK           (0x3 << 2)
#define SE_PAIR_YSEL(x)             ((x) << 4)
#define SE_PAIR_YSEL_MASK           (0x3 << 4)

#define PA_SC_EDGERULE                                                        0xa08c
#define PA_SC_LINE_CNTL                                                       0xa2f7
#define PA_SC_LINE_STIPPLE                                                    0xa283
#define PA_SC_MODE_CNTL_0                                                     0xa292
#define PA_SC_MODE_CNTL_1                                                     0xa293

#define PA_SC_RASTER_CONFIG                                                   0xa0d4
#define PA_SC_RASTER_CONFIG_1                                                 0xa0d5


#define DB_DEPTH_CONTROL                                                      0xa200
#define DB_STENCIL_CONTROL                                                    0xa10b
#define DB_ALPHA_TO_MASK                                                      0xa2dc

#define GB_TILE_MODE0                                                         0x2644

#define COMPUTE_PGM_LO                                                        0x2e0c
#define COMPUTE_PGM_HI                                                        0x2e0d
#define COMPUTE_PGM_RSRC1                                                     0x2e12
#define COMPUTE_PGM_RSRC2                                                     0x2e13
#define COMPUTE_VMID                                                          0x2e14
#define COMPUTE_RESOURCE_LIMITS                                               0x2e15
#define COMPUTE_DIM_X                                                         0x2e01
#define COMPUTE_DIM_Y                                                         0x2e02
#define COMPUTE_DIM_Z                                                         0x2e03
#define COMPUTE_START_X                                                       0x2e04
#define COMPUTE_START_Y                                                       0x2e05
#define COMPUTE_START_Z                                                       0x2e06
#define COMPUTE_NUM_THREAD_X                                                  0x2e07
#define COMPUTE_NUM_THREAD_Y                                                  0x2e08
#define COMPUTE_NUM_THREAD_Z                                                  0x2e09

#define COMPUTE_USER_DATA_0                                                   0x2e40
#define COMPUTE_USER_DATA_1                                                   0x2e41
#define COMPUTE_USER_DATA_2                                                   0x2e42
#define COMPUTE_USER_DATA_3                                                   0x2e43
#define COMPUTE_USER_DATA_4                                                   0x2e44
#define COMPUTE_USER_DATA_5                                                   0x2e45
#define COMPUTE_USER_DATA_6                                                   0x2e46
#define COMPUTE_USER_DATA_7                                                   0x2e47
#define COMPUTE_USER_DATA_8                                                   0x2e48
#define COMPUTE_USER_DATA_9                                                   0x2e49
#define COMPUTE_USER_DATA_10                                                  0x2e4a
#define COMPUTE_USER_DATA_11                                                  0x2e4b
#define COMPUTE_USER_DATA_12                                                  0x2e4c
#define COMPUTE_USER_DATA_13                                                  0x2e4d
#define COMPUTE_USER_DATA_14                                                  0x2e4e
#define COMPUTE_USER_DATA_15                                                  0x2e4f


#define SHADER_TYPE_GRAPHICS	0
#define SHADER_TYPE_COMPUTE		(1<<1)	
#define B800_COMPUTE_ENA	1
#define B800_FORCE_START_000 (1<<2)

#define UCONFIG_REG(__reg)((__reg)-0xc000)
#define PKT3(__op, __n) ((3<<30)|(__op<<8)|(__n<<16))
#define PKT0(__reg, __cnt)\
	((__cnt<<16)|(__reg))/*dw only*/
#define SURFACE_SYNC 0x43
#define PFP_SYNC_ME 0x42
#define EVENT_WRITE             0x46
#define     EVENT_TYPE(x)                           ((x) << 0)
#define     EVENT_INDEX(x)                          ((x) << 8)
enum {
    DI_SRC_SEL_DMA                                   = 0x0,
    DI_SRC_SEL_IMMEDIATE                             = 0x1,
    DI_SRC_SEL_AUTO_INDEX                            = 0x2,
    DI_SRC_SEL_RESERVED                              = 0x3,
};

#define COHER_DEST_BASE_HI_0                                                  0xa07a
#define VGT_HOS_MAX_TESS_LEVEL                                                0xa286
#define VGT_HOS_MIN_TESS_LEVEL                                                0xa287


# endif /*__radeon__reg__h*/
