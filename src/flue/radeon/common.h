# ifndef __radeon__common__h
# define __radeon__common__h
# include "../common.h"
# include "../dd.h"
# include "../../amdgpu.h"
#include "../compiler/ima/r_struc.h"
#include "../../lib/hash.h"
void rd_drv(void);
void rd_drv_deinit(void);
void rd_drv_sweep(void);

/*
	TEXTURE ONLY WORKS ON Y-AXIS:
	- textures have size restraints, meaning that have a min width and height(dependent on tile size).


	SYSMPOMS OF LACK OF MEMORY:
	- colour buffer will get drawn to but may not, may be on off issue. one time it works next time is doesent.

	SYMPTOMS OF LACK OF Z PRESISSION:
	- screen tearing, texture will tear up with white slashes

	issue from vekas thats still left unknown.
	-	flickering of colourbuffer even when drawn successfully
*/

/*
	NOTE:
		this bo is ties into amdgpu_bo structure

	ISSUE:
		colour formating 
		for things like CB_COLOR0_INFO
		require an int representation of the format
		involved, this structure will provide this

	NOTE:
	things here are in horrid state.
	the posability of garbage colourbuffer data is high.
*/

#define RD_NF_IMG 0 
#define RD_NF_COLOUR 1
#define RD_NF_BUF 0
#define RD_DF_IMG 0 
#define RD_DF_COLOUR 0
#define RD_DF_BUF 0
#define RD_FMTGET(__base,__for)\
	(((_64_u*)__base)[__for])

struct rd_bo {
	struct amdgpu_bo bo;
	struct amdgpu_va *v;
	struct rd_bo *next;
	struct rd_bo **bk;
};

struct rd_bo0 {
	struct flue_bo _0;
	struct rd_bo *bo;
};
/*
	THINGS TO KEEP IN MIND.

	shaders could be compiled specific for diffrent contexts with diffrent semantics

*/
struct rd_shader_key{
	struct flue_shader *shader;
	_32_u dtyp;
};

struct rd_shader_cs_variant {
	_64_u block_size[4];
};

struct rd_shader {
	struct rd_bo *bo;
	struct rd_shader_cs_variant cs;
	struct r_shader info;
	_32_u adrlow;
	_32_u adrhigh;
	_int_u size;
	_64_u bits;
	_32_u dtyp;
	/*
		in are memory
	*/
	void *primary;
	_int_u ps;
	_8_u *secondary;
	_int_u ss;
		_32_u user_data[32];
		_32_u ud_offset;
	_64_u name;
	struct flue_shader *sh;
};

struct rd_cmd_buffer {
	_int_u ndw;
	struct amdgpu_bo *bo;
	struct amdgpu_va *v;
};

/*
	shader table
X is the same shader but gpu specific
	
	r300 - XAB
	r600 - XAB


*/
struct rd_dev {
	
	struct flue_devsh sh;
	int fd;
	struct amdgpu_dev *dev;
	struct rd_dev *next;
};
#include "../../struc.h"
struct rd_tex{
	struct flue_tex tx;

	_32_u width, height;
	_32_u buf[32];
	struct rd_bo *bo;
	
	struct rd_bo *clear_bo;
	_64_u clear;

	//here
	struct rd_bo *cbuf, *fbuf;
	struct flue_fmtdesc fmt;
};
//_Static_assert(sizeof(struct rd_tex)<(0x200*8),"");

struct rd_framebuff {
	struct rd_tex *cb[8];
	_int_u ncb;
	struct rd_bo *dpb;
};

struct rd_state {
	union {
		struct {
			struct flue_state st;
		};
		_8_u tmp[STATE_SS];
	};
};

struct rd_dbblock {
	char const *ident;
	_32_u *start;
	_int_u ndw;
	struct rd_dbblock *next;
};
#define RD_DESCMAX 512
#define RD_UNCOMPILED 1
/*
  rd_cb_state_emit(__ctx,__cmdbuf);
  rd_common_state(__ctx,__cmdbuf);
  rd_depth_bounds_emit(__ctx,__cmdbuf);
  rd_blend_state_emit(__ctx,__cmdbuf);

  rd_scissors_emit(__ctx,__cmdbuf);
  rd_raster_state_emit(__ctx,__cmdbuf);
  rd_multisample_state(__ctx,__cmdbuf);
  rd_depth_state(__ctx,__cmdbuf);
  rd_depth_clear(__ctx,__cmdbuf);
  rd_viewport_emit(__ctx,__cmdbuf);
  rd_stencil_state_emit(__ctx,__cmdbuf);
  rd_dpb_state_emit(__ctx,__cmdbuf);
  rd_framebuffer_state_emit(__ctx,__cmdbuf);
*/
#define RD_FRAMEBUFFER_DIRTY	1ull
#define RD_DPB_DIRTY					2ull
#define RD_COLOURBUFFER_DIRTY	4ull
#define RD_VIEWPORT_DIRTY			8ull
#define RD_STENCIL_DIRTY			16ull
#define RD_DEPTHC_DIRTY				32ull
#define RD_DEPTH_DIRTY				64ull
#define RD_MULTISAMPLE_DIRTY	128ull
#define RD_RASTER_DIRTY				256ull
#define RD_SCISSORS_DIRTY			512ull
#define RD_BLEND_DIRTY				1024ull
#define RD_DEPTH_BOUNDS_DIRTY	2048ull
#define RD_DIRTY (~0ull>>(64-12))
struct rd_bo_list_entry;
typedef struct rd_context {
	struct flue_ctx sh;//never fucking move
	
	void(*f0)(struct rd_context*,struct rd_cmd_buffer*);
	void(*f1)(struct rd_context*,struct rd_cmd_buffer*);
	void(*f2)(struct rd_context*,struct rd_cmd_buffer*);
	void(*f3)(struct rd_context*,struct rd_cmd_buffer*);
	struct rd_bo *consts[16];
	_int_u nconsts;

	struct rd_bo *desc[16];
	_int_u ndesc;
	struct rd_bo *shadow;
	struct rd_state *state;
	struct rd_cmd_buffer *cmdbuf;
	struct rd_dev *d;
	struct amdgpu_ctx *ctx;

	void *zb;
	_32_u *d_dw;
	_32_u d_offset;
	struct rd_shader *ps;
	struct rd_shader *vs;
	struct rd_tex *cb[8];

	_64_u bits;
	_int_u ncb;
	struct rd_dbblock *top, *bk;
	struct rd_bo_list_entry *list_top;
	_int_u list_size;
	_64_u cache_bits;
	_64_u blend[8];
	_32_u viewport[64];
	_int_u viewport_dwc;
	struct f_lhash ht;
	_32_u fb;
} *rd_contextp;


struct rd_reg_range{
	_64_u offset;
	_64_u words;
};

struct rd_common {
	struct rd_dev *dvs;
};

struct rd_bo_list_entry {
	struct rd_bo *bo;
	_64_u bo_priority;
	struct rd_bo_list_entry *nextover;
};
void rd_addtothelist(struct rd_context*, struct rd_bo*);

void rd_cache_(struct rd_context *__ct, struct rd_cmd_buffer *__buf);
//surface.c
void rd_tex__bo(struct rd_tex *__t, struct rd_bo*, struct rd_bo*,_32_u __width, _32_u __height, struct flue_fmtdesc *__fmt);
void rd_tex_alloc(struct rd_context *__ctx, struct rd_tex *__tx, _32_u __width, _32_u __height, struct flue_fmtdesc *__fmt);
void rd_tex_alloc_with_own_bo(struct rd_tex *__tx, struct rd_bo *__bo, _32_u __width, _32_u __height, struct flue_fmtdesc *__fmt);

//shader.c
void rd_shader_ps_state_emit(rd_contextp,struct rd_shader*);
void rd_shader_vs_state_emit(rd_contextp,struct rd_shader*);
//tex.c
void rd_maptex(struct rd_context*);
void rd_tex_destroy(struct rd_tex*);
void rd_nowhere_func(rd_contextp,struct rd_cmd_buffer*);

//debug.c
void rd_cmdbuf_dump(rd_contextp);
struct rd_dbblock* rd_dbblock(rd_contextp);

void rd_shader_init(rd_contextp, struct flue_shader*);
struct rd_cmd_buffer* rd_cmd_buffer_new(struct rd_dev*);
void rd_cmd_buffer_destroy(struct rd_cmd_buffer*);

void rd_init_state(rd_contextp, struct rd_cmd_buffer*);
struct rd_bo* rd_bo_create(struct amdgpu_dev*, _64_u, _64_u, _64_u, _64_u, _64_u);
void rd_bo_destroy(struct rd_bo*);
void rd_bo_map(struct rd_bo*);
void rd_bo_unmap(struct rd_bo*);
rd_contextp rd_ctx_new(struct rd_dev*);
void rd_ctx_destroy(rd_contextp);
struct flue_dev* rd_drv_dev(struct rd_dev*,struct amdgpu_dev*);
extern struct flue_dd rd_drv_struc;
void rd_shader_update(rd_contextp __ctx);
# endif /*__radeon__common__h*/
