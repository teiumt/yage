# include "common.h"
# include "../../m_alloc.h"
/*
	smodge
	smidge
*/
struct rd_cmd_buffer*
rd_cmd_buffer_new(struct rd_dev *__dv) {
	struct rd_cmd_buffer *buf;
	buf = m_alloc(sizeof(struct rd_cmd_buffer));
	/*
		4096 bytes
		so 1024 dwords;
	*/
#define SIZE (AMDGPU_GPU_PAGE_SIZE*8)
	
	struct amdgpu_bo *bo;
	bo = m_alloc(sizeof(struct amdgpu_bo));
	amdgpu_bo_alloc(__dv->dev, bo,SIZE, AMDGPU_GPU_PAGE_SIZE, AMDGPU_GEM_DOMAIN_GTT, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);

	struct amdgpu_va *v;
	v = amdgpu_va_alloc(amdgpu_va_range_general(__dv->dev), SIZE, 0);
	buf->v = v;
	amdgpu_bo_va_op(BO_DEVICE(bo), bo, AMDGPU_VA_OP_MAP, AMDGPU_VM_PAGE_READABLE|AMDGPU_VM_PAGE_WRITEABLE|AMDGPU_VM_PAGE_EXECUTABLE,
		0, v->addr, AMDGPU_GPU_PAGE_ALIGN(bo->size));
	
	amdgpu_bo_cpu_map(BO_DEVICE(bo), bo);
	buf->bo = bo;
	return buf;
}

void rd_cmd_buffer_destroy(struct rd_cmd_buffer *__buf) {
	struct amdgpu_bo *bo = __buf->bo;
	amdgpu_bo_cpu_unmap(bo);
	amdgpu_bo_va_op(BO_DEVICE(bo), bo, AMDGPU_VA_OP_UNMAP, 0, 0, __buf->v->addr, bo->size);
	amdgpu_bo_free(BO_DEVICE(bo), bo);
	m_free(__buf);
}
