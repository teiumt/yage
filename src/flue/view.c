# include "common.h"
# include "../io.h"


void flue_viewport(struct flue_viewport *__vp, _int_u __offset, _int_u __n) {
	struct flue_context *ct;
	ct = FLUE_CTX;
	ct->d->viewport(ct->priv,__vp,__offset,__n);
}

void flue_translate(_flu_float __x, _flu_float __y, _flu_float __z) {
	_flu_float rm[16] = {
		1, 0, 0, __x,
		0, 1, 0, __y,
		0, 0, 1, __z,
		0, 0, 0, 1
	};

	flue_pushm(rm);
}

void static cross(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1) {
	*__dst		= __v0[1]*__v1[2]-__v0[2]*__v1[1]; 
	__dst[1]	= __v0[2]*__v1[0]-__v0[0]*__v1[2];
	__dst[2]	= __v0[0]*__v1[1]-__v0[1]*__v1[0];
}
# include "../maths.h"

void static norm(_flu_float *__v) {
	_flu_float r;
	r = sqr(__v[0]*__v[0]+__v[1]*__v[1]+__v[2]*__v[2]);
	if (r == .0) return;
	__v[0]/=r;
	__v[1]/=r;
	__v[2]/=r;
}

void flue_lookat(_flu_float __ex, _flu_float __ey, _flu_float __ez,
	_flu_float __atx, _flu_float __aty, _flu_float __atz,
	_flu_float __upx, _flu_float __upy, _flu_float __upz)
{
	_flu_float fw[3], side[3], up[3];
	*fw		= __atx-__ex;
	fw[1]	= __aty-__ey;
	fw[2]	= __atz-__ez;

	*up		= __upx;
	up[1]	= __upy;
	up[2]	= __upz;

	norm(fw);
	cross(side, fw, up);
	norm(side);
	cross(up, side, fw);

	_flu_float m[16] = {
		side[0], side[1], side[2], 0,
		up[0], up[1], up[2], 0,
		fw[0], fw[1], fw[2], 0,
		0, 0, 0, 1
	};

	flue_pushm(m);
	flue_translate(-__ex, -__ey, -__ez);
}

void flue_frustum(_flu_float __near, _flu_float __far, _flu_float __left, _flu_float __right) {
	_flu_float f = 1./__far;
	_flu_float rm[16] = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, f, __near,
		0, 0, -1.+f, 1
	};
	
	flue_pushm(rm);
}
