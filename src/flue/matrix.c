# include "common.h"
# include "../io.h"
# include "../maths.h"
# include "../string.h"
/*
	copy 16-bytes
*/
#define copy128(__dst, __src)\
	__asm__("movdqu (%1), %%xmm0\n\t"\
			"movdqu %%xmm0, (%0)\n\t"\
		: : "r"(__dst), "r"(__src) : "xmm0");

_flue_float static ident[16] = {
    1, 0, 0, 0,
    0, 1, 0, 0,
    0, 0, 1, 0,
    0, 0, -1, 1
};


void static
matcopY(_flu_float *__dst, _flu_float *__src) {
	copy128(__dst, __src);
	copy128(__dst+4, __src+4);
	copy128(__dst+8, __src+8);
	copy128(__dst+12, __src+12);
}

void flue_matrix_inv16(_flu_float *__m) {
	_flu_float *m = __m;
	_flu_float iv[4*4];
	iv[0] = (m[5]*(m[10]*m[15]-m[14]*m[11]))-(m[6]*(m[9]*m[15]-m[13]*m[11]))+(m[7]*(m[9]*m[14]-m[13]*m[10]));
	iv[1] = (m[4]*(m[10]*m[15]-m[14]*m[11]))-(m[6]*(m[8]*m[15]-m[12]*m[11]))+(m[7]*(m[8]*m[14]-m[12]*m[10]));
	iv[2] = (m[4]*(m[9]*m[15]-m[13]*m[11]))-(m[5]*(m[8]*m[15]-m[12]*m[11]))+(m[7]*(m[8]*m[13]-m[12]*m[9]));
	iv[3] = (m[4]*(m[9]*m[14]-m[13]*m[10]))-(m[5]*(m[8]*m[14]-m[12]*m[10]))+(m[6]*(m[8]*m[13]-m[12]*m[9]));
	iv[4] = (m[1]*(m[10]*m[15]-m[14]*m[11]))-(m[2]*(m[9]*m[15]-m[13]*m[11]))+(m[3]*(m[9]*m[14]-m[13]*m[10]));
	iv[5] = (m[0]*(m[10]*m[15]-m[14]*m[11]))-(m[2]*(m[8]*m[15]-m[12]*m[11]))+(m[3]*(m[8]*m[14]-m[12]*m[10]));
	iv[6] = (m[0]*(m[9]*m[15]-m[13]*m[11]))-(m[1]*(m[8]*m[15]-m[12]*m[11]))+(m[3]*(m[8]*m[13]-m[12]*m[9]));
	iv[7] = (m[0]*(m[9]*m[14]-m[13]*m[10]))-(m[1]*(m[8]*m[14]-m[12]*m[10]))+(m[2]*(m[8]*m[13]-m[12]*m[9]));
	iv[8] = (m[1]*(m[6]*m[15]-m[14]*m[7]))-(m[2]*(m[5]*m[15]-m[13]*m[7]))+(m[3]*(m[5]*m[14]-m[13]*m[6]));
	iv[9] = (m[0]*(m[6]*m[15]-m[14]*m[7]))-(m[2]*(m[4]*m[15]-m[12]*m[7]))+(m[3]*(m[4]*m[14]-m[12]*m[6]));
	iv[10] = (m[0]*(m[5]*m[15]-m[13]*m[7]))-(m[1]*(m[4]*m[15]-m[12]*m[7]))+(m[3]*(m[4]*m[13]-m[12]*m[5]));
	iv[11] = (m[0]*(m[5]*m[14]-m[13]*m[6]))-(m[1]*(m[4]*m[14]-m[12]*m[6]))+(m[2]*(m[4]*m[13]-m[12]*m[5]));
	iv[12] = (m[1]*(m[6]*m[11]-m[10]*m[7]))-(m[2]*(m[5]*m[11]-m[9]*m[7]))+(m[3]*(m[5]*m[10]-m[9]*m[6]));
	iv[13] = (m[0]*(m[6]*m[11]-m[10]*m[7]))-(m[2]*(m[4]*m[11]-m[8]*m[7]))+(m[3]*(m[4]*m[10]-m[8]*m[6]));
	iv[14] = (m[0]*(m[5]*m[11]-m[9]*m[7]))-(m[1]*(m[4]*m[11]-m[8]*m[7]))+(m[3]*(m[4]*m[9]-m[8]*m[5]));
	iv[15] = (m[0]*(m[5]*m[10]-m[9]*m[6]))-(m[1]*(m[4]*m[10]-m[8]*m[2]))+(m[2]*(m[4]*m[9]-m[8]*m[5]));

	_flu_float det;
	det = m[0]*iv[0]-m[1]*iv[4]+m[2]*iv[8]-m[3]*iv[12];

	if (!det)
		return;
	det = 1./det;

	m[0]*=det;
	m[1]*=det;
	m[2]*=det;
	m[3]*=det;
	m[4]*=det;
	m[5]*=det;
	m[5]*=det;
	m[6]*=det;
	m[7]*=det;
	m[8]*=det;
	m[9]*=det;
	m[10]*=det;
	m[11]*=det;
	m[12]*=det;
	m[13]*=det;
	m[14]*=det;
	m[15]*=det;
}

#define M0(__row, __col) m0[__row+__col]
#define M1(__row, __col) m1[__row+__col]
#define M2(__row, __col) m2[__row+__col]
void flue_matrix_mul4(_flue_float *m0, _flue_float *m1, _flue_float *m2) {
	_int_u i;
	i = 0;
	for(;i != 16;i+=4) {
		_flu_float a, b, c, d;
		a = M1(i,0);
		b = M1(i,1);
		c = M1(i,2);
		d = M1(i,3);
		M0(i,0) = a*M2(0,0)+b*M2(4,0)+c*M2(8,0)+d*M2(12,0);
		M0(i,1) = a*M2(0,1)+b*M2(4,1)+c*M2(8,1)+d*M2(12,1);
		M0(i,2) = a*M2(0,2)+b*M2(4,2)+c*M2(8,2)+d*M2(12,2);
		M0(i,3) = a*M2(0,3)+b*M2(4,3)+c*M2(8,3)+d*M2(12,3);
	}
}

void flue_matrix(void) {
	_int_u i;
	i = 0;
	struct flue_context *ct;
	ct = FLUE_CTX;
	_flu_float *_m = FLU_M(ct)-16, *m;
	matcopY(_m, ident);	
	for(;i != ct->m;i+=16) {
		m = FLU_M(ct)+i;
		flue_matrix_mul4(_m, _m, m);
	}
	ct->m = ct->mb;
    printf("[%f,\t%f,\t%f,\t%f]\n", _m[0], _m[1], _m[2], _m[3]);
    printf("[%f,\t%f,\t%f,\t%f]\n", _m[4], _m[5], _m[6], _m[7]);
    printf("[%f,\t%f,\t%f,\t%f]\n", _m[8], _m[9], _m[10], _m[11]);
    printf("[%f,\t%f,\t%f,\t%f]\n", _m[12], _m[13], _m[14], _m[15]);
	ffly_fdrain(_ffly_out);
}

void flue_mbase(void) {
	FLUE_CTX->mb = FLUE_CTX->m;
}

void flue_pushm(_flu_float *__m) {
	struct flue_context *ct;
	ct = FLUE_CTX;
	matcopY(FLU_M(ct)+ct->m, __m);
	ct->m+=16;
}

void flue_perspective(_flu_float __fv, _flu_float __m) {
	_flu_float fv;
	fv = 1./tan(__fv);
	_flu_float rm[16] = {
		fv, 0, 0, 0,
		0, fv, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};

	flue_pushm(rm);
}
