#ifndef __mjx
#define __mjx
#include "common.h"
#include "../vekas/maj.h"
struct mjx_buffer;
struct mjx_window {
	struct maj_patch pt;//stays at top
	struct mjx_buffer *back,*front;
	struct flue_framebuffer fb;
};

struct mjx_buffer {
	int handle;
	struct flue_tex *render;
};
_8_s mjx_connect(struct maj_conn*);
void mjx_disconnect(struct maj_conn*);
void mjx_creatwindow(struct mjx_window*);
void mjx_wdmap(struct maj_conn*,struct mjx_window*,_int_u,_int_u,_int_u,_int_u);
struct mjx_buffer* mjx_buffer(_int_u,_int_u,struct flue_fmtdesc*);
void mjx_wdunmap(struct maj_conn*,struct mjx_window*);
void mjx_swapbufs(struct maj_conn*,struct mjx_window*);
#endif
