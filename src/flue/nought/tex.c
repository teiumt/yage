# include "common.h"
# include "../../m_alloc.h"
# include "../../string.h"
# include "../../io.h"

nt_texp nt_tex_new(void) {
	nt_texp t;
	t = (nt_texp)m_alloc(sizeof(struct nt_tex));

	return t;
}

void nt_tex_destroy(nt_texp __tx) {

}

void static
tex_unmap(struct nt_tex *__tx) {
	if ((__tx->sh.bits&FLUE_TEX_MMAP)>0) {
		m_free(__tx->sh.mmap);
		__tx->sh.bits ^= FLUE_TEX_MMAP;
	}
}

void static
tex_map(struct nt_tex *__tx) {
	if (!(__tx->sh.bits&FLUE_TEX_MMAP)) {
		__tx->sh.mmap = m_alloc(__tx->sh.totsz);
		flue_texload(&__tx->sh);
		__tx->sh.bits |= FLUE_TEX_MMAP;
	}
}

void nt_maptex(struct nt_context *__ctx) {
	_int_u i;
	i = 0;
	struct nt_tex *tx;
	for(;i != __ctx->sh.m.ntex;i++) {
		printf("LOADING TEXTURE(%u).\n", i);
		tx = __ctx->sh.m.tp[i];
		tex_map(tx);
	}
	__ctx->sh.m.ntex = 0;

	i = 0;
	for(;i != __ctx->sh.m.u_ntex;i++) {
		printf("UNLOADING TEXTURE(%u).\n", i);
		tx = __ctx->sh.m.u_tp[i];
		tex_unmap(tx);
	}
	__ctx->sh.m.u_ntex = 0;
}
