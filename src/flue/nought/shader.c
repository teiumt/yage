# include "common.h"
# include "../../io.h"
# include "../../m_alloc.h"
# include "../../ys/ysm.h"
# include "../../maths.h"
# include "../../string.h"
void static
construed(struct flue_prog *__pgm, struct nt_shader *__sh) {
	__sh->ss = __pgm->ps;
	__sh->secondary = __pgm->primary;
}

void nt_shader_init(struct nt_context *__ctx, struct flue_prog *__pgm) {
	struct nt_shader *sh = NT_SHFROMPGM(__pgm);
	sh->out = NULL;
	construed(__pgm, sh);
}

void nt_shader_ready(struct flue_prog *__pgm, struct nt_shader *__sh) {
	/* FIXME:
	_64_u *d = __sh->reg+4;
	_int_u i;
	_int_u off = 0;
	i = 0;
	for(;i != __pgm->nb;i++) {
		struct flue_yard *yrd;
		yrd = __pgm->binds[i];
		mem_cpy(__sh->ram+off, ((_8_u*)yrd->bo->cpu_map)+yrd->abouts, yrd->dwc<<2);
		printf("loading shader yard, %u, %u, %u\n", off, yrd->abouts, yrd->dwc);
		off+=yrd->bo->size;
	}
	*/
}
struct _opstc { 
	char const *name;
};

struct _opstc optab[] = {
	{"ADDFQ"},
	{"XPRT"},
	{"AGNL"},
	{"AGNH"},
	{"FLDW"},
	{"FSDW"},
	{"MOVLL"},
	{"MOVLH"},
	{"MOVHL"},
	{"MOVHH"},
	{"DIST"},
	{"CLMP"},
	{"LD"},
	{"ST"}
};

struct _opstc optab0[] = {
	{"MUL"},
	{"ADD"},
	{"COS"},
	{"SIN"}
};

void nt_shader_run(struct nt_shader *__sh) {
	struct yss_op *op;
	_int_u i;
	i = 0;
	while(i != __sh->ss) {
		op = ((struct yss_op*)__sh->secondary)+i;
//		printf("OP: %x, -%s\n", op->op, !op->form?optab[op->op].name:optab0[op->op].name);	
		if (op->form>0) {

			float *d = __sh->reg+op->opand[0].reg;
			float *s = __sh->reg+op->opand[1].reg;
			switch(op->op) {
				case _ys_mul:
					d[0]*=s[0];
					d[1]*=s[1];
					d[2]*=s[2];
					d[3]*=s[3];
				break;
				case _ys_add:
					d[0]+=s[0];
					d[1]+=s[1];
					d[2]+=s[2];
					d[3]+=s[3];
				break;
				case _ys_cos:
					d[0] = cos(s[0]);
					d[1] = cos(s[1]);
					d[2] = cos(s[2]);
					d[3] = cos(s[3]);
				break;
				case _ys_sin:
					d[0] = sin(s[0]);
					d[1] = sin(s[1]);
					d[2] = sin(s[2]);
					d[3] = sin(s[3]);
				break;
				default:
					printf("unknown.\n");
			}
		} else {
		switch(op->op){
			case _ys_agnl: {
				_32_u *reg;
				reg = __sh->reg+op->opand[0].reg;
				*reg = op->opand[1].i;
			}
			break;
			case _ys_agnh: {
				_32_u *reg;
				reg = __sh->reg+op->opand[0].reg;
				*(reg+1) = op->opand[1].i;
			}
			break;
			case _ys_xprt: {
				__sh->out = __sh->reg+op->opand[0].reg;
			}
			break;
			case _ys_fldw: {
				_64_u *r, adr;
				adr = __sh->reg[op->opand[0].reg];
				r = __sh->reg+op->opand[1].reg;
				if (adr>512) {
					break;
				}
				*r = *(_64_u*)(__sh->ram+adr);
			}
			break;
			case _ys_fsdw: {
				_64_u *r, adr;
				adr = __sh->reg[op->opand[0].reg];
				r = __sh->reg+op->opand[1].reg;
				if (adr>512) {
					break;
				}
				*(_64_u*)(__sh->ram+adr) = *r;
			}
			break;
			case _ys_movll: {
				_32_u *r0, *r1;
				r0 = __sh->reg+op->opand[0].reg;
				r1 = __sh->reg+op->opand[1].reg;
				*r0 = *r1;
			}
			break;
			case _ys_movlh: {
				_32_u *r0, *r1;
				r0 = __sh->reg+op->opand[0].reg;
				r1 = __sh->reg+op->opand[1].reg;
				*r0 = *(r1+1);
			}
		   break;
			case _ys_movhl: {
				_32_u *r0, *r1;
				r0 = __sh->reg+op->opand[0].reg;
				r1 = __sh->reg+op->opand[1].reg;
				*(r0+1) = *r1;
			}
			break;
			case _ys_movhh: {
				_32_u *r0, *r1;
				r0 = __sh->reg+op->opand[0].reg;
				r1 = __sh->reg+op->opand[1].reg;
				*(r0+1) = *(r1+1);
			}
			break;
			case _ys_dist: {
				float *r0 = __sh->reg+op->opand[1].reg;
				float *r1 = __sh->reg+op->opand[2].reg;
				float *d = __sh->reg+op->opand[0].reg;
   

				float dx, dy, dz;
				dx = r0[0]-r1[0];
				dy = r0[1]-r1[1];
				dz = r0[2]-r1[2];
				*d = sqr(dx*dx)+sqr(dy*dy)+sqr(dz*dz);
				*d = 1./(*d);
			}
			break;
			case _ys_clmp: {
				float *r = __sh->reg+op->opand[0].reg;
				float mn, mx;
				mn = op->opand[1].f;
				mx = op->opand[2].f;

				//printf("MIN: %f, MAX: %f.\n", mn, mx);
				if (r[0]<=mn)
					r[0] = mn;
				else if (r[0]>=mx)
					r[0] = mx;
				if (r[1]<=mn)
					r[1] = mn;
				else if (r[1]>=mx)
					r[1] = mx;
				if (r[2]<=mn)
					r[2] = mn;
				else if (r[2]>=mx)
					r[2] = mx;
				if (r[3]<=mn)
					r[3] = mn;
				else if (r[3]>=mx)
					r[3] = mx;
			}
			break;
			default:
				printf("the unknown is scary!!!.\n");
		}

		}
		
		i++;
	}
}
