# include "common.h"
# include "../../io.h"
# include "../../m_alloc.h"
# include "../../string.h"
# include "../../time.h"
# include "../../assert.h"
void nt_maptex(struct nt_context*);
void static copyframe(struct flue_scaf *__scf, struct flue_drawable *__fb, struct nt_rb *__rt, struct flue_context *__fc) {
	__scf->update(__fb, __rt->sf, __fc);
	sfreset(__rt->sf);
}

void static clearrb(struct nt_rb *__rt) {
	mem_set(__rt->sf->map, 0, __rt->sf->w*__rt->sf->h*4);
	_int_u n, i = 0;
	n = __rt->sf->w*__rt->sf->h;
	for(;i != n;i++) {
		*(__rt->dpb+i) = -101987;
	}
}
struct nt_shader nt_shtab[8];

void nt_rect(nt_contextp, _flue_float*);
void nt_crect(nt_contextp, _flue_float*, struct flue_colour*);
void static
submit(void) {	
	struct flue_context *fc;
	fc = flue_comm.ctx;

	DEFINE_CONTEXT(nc)
	nt_maptex(nc);


	flue_blobp b;
	b = fc->b;
	
	struct flue_optext *tx;
	struct nt_tri3 tbuf[0x1000];
	struct flue_op *op;

	nc->render = b->rtgs;
	nc->pgm = b->pgm;
	nc->ps = NT_SHFROMPGM(b->pgm[0]);
	nc->vs = NT_SHFROMPGM(b->pgm[1]);
	nt_shader_ready(b->pgm[0],nc->ps);
	nt_shader_ready(b->pgm[1],nc->vs);
	
	if (nc->sh.bits&FLUE_CLEAR) {
		clearrb(b->rtgs[0]);
	}
	f_printf("SUBMIT-%u.\n", b->n);
	flue_canisterp cst;
	_8_u pgfix = 0;
	_int_u i, ii;
	i = 0;
	for(;i != b->n;i++) {
		cst = b->can+i;
		nc->tx = cst->tex;
		nc->m = cst->m;
		if (cst->bits&SWAPOUTSHADER) {
			nc->pgm = cst->pgm;
			pgfix = 1;
		} else {//quick lazy fix
			nc->pgm = b->pgm;
			pgfix = 0;
		}
		if (cst->m != NULL) {
			printf("[%f,\t%f,\t%f,\t%f]\n", cst->m[0], cst->m[1], cst->m[2], cst->m[3]);
			printf("[%f,\t%f,\t%f,\t%f]\n", cst->m[4], cst->m[5], cst->m[6], cst->m[7]);
			printf("[%f,\t%f,\t%f,\t%f]\n", cst->m[8], cst->m[9], cst->m[10], cst->m[11]);
			printf("[%f,\t%f,\t%f,\t%f]\n", cst->m[12], cst->m[13], cst->m[14], cst->m[15]);
		}
		f_printf("CANISTER-%u.\n", i);
		ii = 0;
		for(;ii != cst->n_ops;ii++) {
			op = cst->ops+ii;
			f_printf("OPERATION-%u : %u*%u.\n", ii, op->op, op->n);
			switch(op->op) {
				case FLUE_TRI3: {
					if (!(op->bits&FLUE_TOI)) {
						assert(cst->ntx != 0);
						_int_u ii;
						ii = 0;
						for(;ii != op->n;ii++) {
							tx = op->t+ii;
							struct nt_tri3 *t;
							t = tbuf+ii;
							t->uv = tx->buf+12;
							t->buf = tx->buf;
						}
						nt_raster_tgl(nc, tbuf, op->n);
					}
				}
				break;
				case FLUE_QUAD: {
					struct flue_optext *tx;
					tx = op->t;
					_int_u ii;
					ii = 0;
					_flu_float *uv, *vtx;
					for(;ii != tx->n;ii++) {
						vtx = tx->buf+(ii*20);
						uv = vtx+12;
						struct nt_tri3 *t0, *t1;
						t0 = tbuf+(ii*2);
						t1 = t0+1;
						t0->uv = uv;
						t0->buf = vtx;
						t1->uv = uv+2;
						t1->buf = vtx+3;
					}
					nt_raster_tgl(nc, tbuf, tx->n*2);	
				}
				break;
				case FLUE_RECT: {
					nt_rect(nc, op->t->buf);
				}
				break;
				case FLUE_CRECT: {
					nt_crect(nc, op->t->buf, &op->t->c);
				}
				break;
				case FLUE_LPCM: {
					nt_lpcm(nc, op->t->buf);
				}
				break;
				case FLUE_ILPCM: {
					nt_ilpcm(nc, op->t->buf);
				}
				break;
				case FLUE_LINE: {
					nt_line(nc, op->t->buf, op->t->n);
				}
				break;
			}
		}
	}

	copyframe(fc->scaf, fc->draw, b->rtgs[0], fc);	
}

void static 
_readpixels(struct nt_rb *__rb, void *__buf, _flu_int_u __x, _flu_int_u __y, _flu_size __width, _flu_size eight) {
	nt_surf_read(__rb->sf, __buf, __x, __y, __width, eight, __width);
}
# include "../../vekas/struc.h"
void nt_dw_init(struct flue_drawable*);
void nt_dw_deinit(struct flue_drawable*);
void nt_maj_update(struct flue_drawable*, struct nt_surf*, struct flue_context*);
#define DEFINE(__name)\
	(void*)nt_ ## __name
struct flue_dd nt_drv_struc = {
	DEFINE(ctx_new),
	DEFINE(ctx_destroy),
	submit,
	DEFINE(rb_new),
	DEFINE(rb_destroy),
	NULL,
	DEFINE(tex_new),
	DEFINE(tex_destroy),
	_readpixels,
	.scaf = {
		{
			(void*)nt_maj_update,
			(void*)nt_dw_init,
			(void*)nt_dw_deinit
		}
	},
	DEFINE(bo_new),
	DEFINE(bo_map),
	DEFINE(bo_unmap),
	DEFINE(bo_destroy),
	DEFINE(shader_init)
};

struct nt_dev* nt_dev_add(int __fd) {
	struct nt_dev *d;
	d = (struct nt_dev*)m_alloc(sizeof(struct nt_dev));
	d->sh.fd = __fd;
	return d;
}
# include "../../linux/fb.h"
# include "../../linux/unistd.h"
# include "../../linux/fcntl.h"
# include "../../linux/ioctl.h"
# include "../../linux/mman.h"
void nt_drv(void) {
}

void nt_drv_deinit(void) {
}
