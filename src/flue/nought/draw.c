# include "common.h"
# include "../../io.h"
# include "../../string.h"
# include "../../maths.h"
# include "../../m_alloc.h"
/*
	draw a grid a cells in a linear/lonigated format

	X, X, X, X,
	X, X, X, X
	
	= X, X, X, X, X, X, X, X

	ie 2d to 1d you could say
	so we can make use of the 2d texture space
*/

static _flu_float dummy_pos[4] = {0, 0, 0, 0};
#define nt_setpix(...) _nt_setpix(__VA_ARGS__, dummy_pos)

void nt_vertex(nt_contextp __ct, _flu_float *__vtx, _int_u __n) {
	_int_u i;
	i = 0;
	struct nt_shader *pd = __ct->vs;
	float *in = pd->reg;
	float *out;
	for(;i != __n;i++) {
		_flu_float *v = __vtx+(i*8);
		in[0] = v[0];
		in[1] = v[1];
		in[2] = v[2];
		in[3] = v[3];
		nt_shader_run(__ct->vs);
		if (!pd->out) {
			printf("error.\n");
			return;
		}
		out = pd->out;
		v[0] = out[0];
		v[1] = out[1];
		v[2] = out[2];
		v[3] = out[3];

		v[4] = out[4];
		v[5] = out[5];
		v[6] = out[6];
		v[7] = out[7];
	}
}

void _nt_setpix(nt_contextp __ct, _8_u *__d, _8_u *__s, _flu_float *__pos) {
	struct nt_shader *pd = __ct->ps;
	float *in = pd->reg;
	float *out;
	in[0] = ((float)__s[0])/255.0;
	in[1] = ((float)__s[1])/255.0;
	in[2] = ((float)__s[2])/255.0;
	in[3] = ((float)__s[3])/255.0;
	in[4] = __pos[0];
	in[5] = __pos[1];
	in[6] = __pos[2];
	in[7] = __pos[3];
	nt_shader_run(__ct->ps);
	out = pd->out;
	_8_u s[4];
	s[0] = out[0]*255.0;
	s[1] = out[1]*255.0;
	s[2] = out[2]*255.0;
	s[3] = out[3]*255.0;

	_32_u alpha = __s[3];
	_32_u ia = 0xff^alpha;
	__d[0] = ((_32_u)s[0]*alpha+(_32_u)__d[0]*ia)>>8;
	__d[1] = ((_32_u)s[1]*alpha+(_32_u)__d[1]*ia)>>8;
	__d[2] = ((_32_u)s[2]*alpha+(_32_u)__d[2]*ia)>>8;
	__d[3] = 0xff;
}

void nt_lpcm(nt_contextp __ctx, _flue_float *__p) {
	_int_u x, y;
	_flue_float x0, y0, x1, y1;
	_flue_float u0, v0, u1, v1;
	_flue_float sz;
	x0 = __p[0]*__ctx->sh.vp.width;
	y0 = __p[1]*__ctx->sh.vp.height;
	x1 = __p[2]*__ctx->sh.vp.width;
	y1 = __p[3]*__ctx->sh.vp.height;
	
	x = x0;
	y = y0;


	struct nt_tex *tx = __ctx->tx[0];
	u0 = __p[4]*tx->sh.width;
	v0 = __p[5]*tx->sh.height;
	u1 = __p[6]*tx->sh.width;
	v1 = __p[7]*tx->sh.height;
	
	sz = __p[8];
	_int_u cnt;
	cnt = __p[9];
	_flue_float rat = cnt/(x1-x0);
	_int_u w, h;
	w = (x1-x0)/sz;
	h = y1-y0;
	
	_int_u xx, x_;
	_flue_float ch;
	ch = h/sz;
	_int_u _x, _b, _y;
	_int_u xoff = 0;
	_int_u to, yo;
	struct nt_surf *sf;
	sf = __ctx->render[0]->sf;
	_8_u *d, *s;
	_int_u _tx;
	_int_u ty;
	_int_u yoff;
	struct nt_tile *t;
	printf("lpcm: %u, %u.\n", w, h);
	_b = 0;
	for(;_b != sz;_b++) {
		_y = 0;
		for(;_y != h;_y++) {
			_x = 0;
			for(;_x != w;_x++) {	
				xx = _x+xoff;
				x_ = xx+x;
				_tx = x_>>NT_TILE_SHFT;
				ty = ((y+_y)>>NT_TILE_SHFT)*sf->tw;
				yo = ((y+_y)&NT_TILE_MASK)<<NT_TILE_SHFT;
				to = (x_&NT_TILE_MASK)|yo;
				t = sf->tiles+_tx+ty;
				talter(sf, t);
				d = t->map0+(to*4);
				_flue_float u, v;
				u = _x*((u1-u0)/((x1-x0)/sz));	
				v = _y*(((v1-v0)/sz)/(y1-y0));
				_int_u txx, txy;
				txx = u0+u;
				txy = v0+v+(_b*((v1-v0)/sz));
				_int_u xt = (txx+(txy*tx->sh.width))*4;
				s = tx->sh.mmap+xt;
				nt_setpix(__ctx, d, s);		
			}
		}
		xoff+=w;
	}
}

/*
	imperfect
*/
void nt_ilpcm(nt_contextp __ctx, _flue_float *__p) {
	_flue_float x0, y0, x1, y1;
	_flue_float u0, v0, u1, v1;
	_flue_float sz;
	x0 = __p[0]*__ctx->sh.vp.width;
	y0 = __p[1]*__ctx->sh.vp.height;
	x1 = __p[2]*__ctx->sh.vp.width;
	y1 = __p[3]*__ctx->sh.vp.height;

	_flu_float x, y;
	x = x0;
	y = y0;
	struct nt_tex *tx = __ctx->tx[0];
	u0 = __p[4]*tx->sh.width;
	v0 = __p[5]*tx->sh.height;
	u1 = __p[6]*tx->sh.width;
	v1 = __p[7]*tx->sh.height;

	struct nt_surf *sf;
	sf = __ctx->render[0]->sf;

	struct nt_tile *t;
	_int_u _tx, ty;
	sz = __p[8];
	_int_u yy, xx, yo, to;
	_int_u i;
	_flu_float _x, _y;
	_8_u *d, *s;
	i = 0;
	_flu_float w, h;
	w = (x1-x0)/sz;
	h = y1-y0;
	_flu_float xoff;

	for(;i != sz;i++) {
		_y = 0;
		xoff = i*w;
		printf("XOFF: %f.\n", xoff);
		for(;_y<h;_y++) {
			_x = 0;
			for(;_x<w;_x++) {
				xx = (_int_u)(x+_x+xoff);
				yy = (_int_u)(y+_y);
				_tx = xx>>NT_TILE_SHFT;
				ty = (yy>>NT_TILE_SHFT)*sf->tw;
				yo = (yy&NT_TILE_MASK)<<NT_TILE_SHFT;
				t = sf->tiles+_tx+ty;
				to = ((xx&NT_TILE_MASK)|yo)*4;
				d = t->map0+to;
				_flue_float u, v;
				u = u0+_x*(((u1-u0)/__p[10])/w);
				_int_u row;
				row = i/__p[10];
				u+=(i-(row*__p[10]))*((u1-u0)/__p[10]);
				v = v0+_y*(((v1-v0)/__p[10])/h);
				v+=row*((v1-v0)/__p[10]);
				_int_u xt = (((_int_u)u)+(((_int_u)v)*tx->sh.width))*4;
				s = tx->sh.mmap+xt;
				nt_setpix(__ctx, d, s);
			}
		}
	}
}

void nt_crect(nt_contextp __ctx, _flue_float *__p, struct flue_colour *__c) {
	_flue_float x0, y0;
	_flue_float x1, y1;
	
	_flu_float vtx[24];
	vtx[0] = __p[0];
	vtx[1] = __p[1];
	vtx[8] = __p[2];
	vtx[9] = __p[3];

	nt_vertex(__ctx, vtx, 2);
	x0 = vtx[0]*__ctx->sh.vp.width;
	y0 = vtx[1]*__ctx->sh.vp.height;
	x1 = vtx[8]*__ctx->sh.vp.width;
	y1 = vtx[9]*__ctx->sh.vp.height;
	clamp(x1, __ctx->sh.vp.width);
	clamp(y1, __ctx->sh.vp.height);

	struct nt_surf *sf;
	sf = __ctx->render[0]->sf;
	_int_u w, h;
	w = x1-x0;
	h = y1-y0;

	struct nt_tile *t;
	_int_u xs, ys;
	xs = x0;
	ys = y0;
	_int_u tx, ty;
	_int_u to, yo;
	_int_u xx, yy;
	_int_u x, y;
	_8_u *d;
	printf("CRECT: %u, %u, %u, %u.\n", xs, ys, w, h);
	y = 0;
	for(;y != h;y++) {
		x = 0;
		for(;x != w;x++) {
			xx = x+xs;
			yy = y+ys;
			ty = yy>>NT_TILE_SHFT;
			yo = (yy&NT_TILE_MASK)<<NT_TILE_SHFT;

			tx = xx>>NT_TILE_SHFT;
			t = sf->tiles+tx+(ty*sf->tw);
			to = ((xx&NT_TILE_MASK)|yo)*4;

			talter(sf, t);

			d = t->map0+to;
			_8_u s[4];
			s[0] = 255.*__c->r;
			s[1] = 255.*__c->g;
			s[2] = 255.*__c->b;
			s[3] = 255.*__c->a;
			_flu_float pos[4];
			pos[0] = ((_flu_float)x)/w;
			pos[1] = ((_flu_float)y)/h;
			pos[2] = 0;
			pos[3] = 0;
			_nt_setpix(__ctx, d, s, pos);
		}
	}
}

void nt_rect(nt_contextp __ctx, _flue_float *__p) {
	_flue_float x0, y0;
	_flue_float x1, y1;
	_flue_float x2, y2;
	_flue_float x3, y3;
	_flue_float u0, v0;
	_flue_float u1, v1;
	_flue_float u2, v2;
	_flue_float u3, v3;

	_flu_float vtx[24];
	vtx[0] = __p[0];
	vtx[1] = __p[1];
	vtx[8] = __p[2];
	vtx[9] = __p[3];
	nt_vertex(__ctx, vtx, 2);
	x0 = vtx[0]*__ctx->sh.vp.width;
	y0 = vtx[1]*__ctx->sh.vp.height;
	x1 = vtx[8]*__ctx->sh.vp.width;
	y1 = vtx[9]*__ctx->sh.vp.height;

	clamp(x1, __ctx->sh.vp.width);
	clamp(y1, __ctx->sh.vp.height);

	u0 = __p[4];
	v0 = __p[5];
	u1 = __p[6];
	v1 = __p[7];
	u2 = __p[8];
	v2 = __p[9];
	u3 = __p[10];
	v3 = __p[11];

	struct nt_tex *xt = __ctx->tx[0];
	u0*=xt->sh.width;
	v0*=xt->sh.height;
	u1*=xt->sh.width;
	v1*=xt->sh.height;
	u2*=xt->sh.width;
	v2*=xt->sh.height;
	u3*=xt->sh.width;
	v3*=xt->sh.height;

	struct nt_surf *sf;
	sf = __ctx->render[0]->sf;
	_int_u w, h;
	w = x1-x0;
	h = y1-y0;

	_int_u xs, ys;
	xs = x0;
	ys = y0;
	_int_u tx, ty;
	_int_u to, yo;
	_int_u xx, yy;
	_int_u x, y;
	y = 0;
	_8_u *d, *s;
	struct nt_tile *t;
	_flue_float u, v;
	printf("RECT: %u, %u, %u, %u.\n", xs, ys, w, h);
	for(;y != h;y++) {
		x = 0;
		for(;x != w;x++) {
			xx = x+xs;
		 	yy = y+ys;
			ty = yy>>NT_TILE_SHFT;
			yo = (yy&NT_TILE_MASK)<<NT_TILE_SHFT;

			tx = xx>>NT_TILE_SHFT;
			t = sf->tiles+tx+(ty*sf->tw);
			to = ((xx&NT_TILE_MASK)|yo)*4;
			talter(sf, t);

			_flue_float a0, a1, a2, a3;
			_flue_float b0, b1, b2;
		
			float _x, _y;
			_x = x*(1./w);
			_y = y*(1./h);

			a0 = (u1-u0);
			a1 = (u3-u2);
			a2 = (v2-v0);
			a3 = (v3-v1);

			b0 = v0+(a2*_y)+((a2-a3)*_x);
			b1 = u0+(a0*_x)+((a0-a1)*_y);
			u = b1;
			v = b0;
	
			d = t->map0+to;
			s = ((_8_u*)xt->sh.mmap)+((((_64_u)(u))+(((_64_u)(v))*xt->sh.width))*4);
			nt_setpix(__ctx, d, s);
		}
	}
}

void static
line_dsc(nt_contextp __ctx, _flue_float __x0, _flue_float __y0, _flue_float __x1, _flue_float __y1, _flue_float *__buf, _flu_float __z, _flu_float __z0) {
	_int_u width;
	width = abs(__x1-__x0);
	_int_u i;
	i = 0;
	_flue_float a;
	a = (__y1-__y0)/(__x1-__x0);
	a = abs(a);
	struct nt_surf *sf;
	struct nt_rb *rb;
	sf = (rb = __ctx->render[0])->sf;
	struct nt_tile *t;
	_int_u tx, ty;
	_int_u txo, tyo, to;
	_8_u *d, s[4];
		

	float dir;
	if (__x1>__x0)
		dir = 1;
	else
		dir = -1;

	_flu_float zf = 1./((__x1-__x0)*(__y1-__y0));
	for(;i != width;i++) {
		_int_u y, x;
		y = __y0+(((float)i)*a);
		x = __x0+(i*dir);
		tx = x>>NT_TILE_SHFT;
		ty = y>>NT_TILE_SHFT;
		txo = x&NT_TILE_MASK;
		tyo = (y&NT_TILE_MASK)<<NT_TILE_SHFT;
		t = sf->tiles+tx+(ty*sf->tw);
		talter(sf, t);
		to = (txo|tyo)*4;
		d = t->map0+to;
		_flu_float dx, dy;
		dx = x-__x0;
		dy = y-__y0;
		_flu_float dd;
		dd = dx*(__y1-__y0);
		_flu_float z = __z0+(dd*zf*__z);
		z = abs(z);
		_flu_float *dv = rb->dpb+x+(y*sf->width);
		if (z<=*dv) {
			s[0] = 255.**__buf;
			s[1] = 255.*__buf[1];
			s[2] = 255.*__buf[2];
			s[3] = 255.*__buf[3];
			nt_setpix(__ctx, d, s);
			*dv = z;
		}
	}
}

#define M0(__col) m[__col]
#define M1(__col) m[4+__col]
#define M2(__col) m[8+__col]
#define M3(__col) m[12+__col]
void static
_line(nt_contextp __ctx, _flue_float *__buf) {
	_flue_float *m;
	m = __ctx->m;

	_flue_float t0, t1, w;
	_flue_float x0, y0, z0;
	_flue_float x1, y1, z1;
	x0 = __buf[0];
	y0 = __buf[1];
	z0 = __buf[2];
	x1 = __buf[3];
	y1 = __buf[4];
	z1 = __buf[5];
	printf("####### %f:%f.\n", y0, y1);
	w = x0*M3(0)+y0*M3(1)+z0*M3(2)+M3(3);
	t0 = x0*M0(0)+y0*M0(1)+z0*M0(2)+M0(3);
	t1 = x0*M1(0)+y0*M1(1)+z0*M1(2)+M1(3);
	z0 = x0*M2(0)+y0*M2(1)+z0*M2(2)+M2(3);
	if (w != 0) {
		t0/=w;
		t1/=w;
	}
	
	x0 = __ctx->sh.vp.x+t0;
	y0 = __ctx->sh.vp.y+t1;

	w = x1*M3(0)+y1*M3(1)+z1*M3(2)+M3(3);
	t0 = x1*M0(0)+y1*M0(1)+z1*M0(2)+M0(3);
	t1 = x1*M1(0)+y1*M1(1)+z1*M1(2)+M1(3);
	z1 = x1*M2(0)+y1*M2(1)+z1*M2(2)+M2(3);
	if (w != 0) {
		t0/=w;
		t1/=w;
	}
	
	x1 = __ctx->sh.vp.x+t0;
	y1 = __ctx->sh.vp.y+t1;

	printf("######## %f,%f,%f,%f,%f,%f.\n", x0, y0, z0, x1, y1, z1);
	if (x0<0 || x0>1 || y0<0 || y0>1) {
		printf("error.\n");
		return;
	}
	if (x1<0 || x1>1 || y1<0 || y1>1) {
		printf("error.\n");
		return;
	}

	_flu_float _z = z1-z0;
	x0*=__ctx->sh.vp.width;
	y0*=__ctx->sh.vp.height;
	x1*=__ctx->sh.vp.width;
	y1*=__ctx->sh.vp.height;
	printf("<%f,%f>:<%f,%f>.\n", x0, y0, x1, y1);
	_flue_float *buf = __buf+6;
	_int_u x, y;
	struct nt_surf *sf;
	struct nt_rb *rb;
	sf = (rb = __ctx->render[0])->sf;
	struct nt_tile *t;
	_int_u tx, ty;
	_int_u txo, tyo, to;
	_8_u *d, s[4];
	_int_u wide;
	if (x0 == x1) {
		if (y0>y1) {
			y = y0;
			y0 = y1;
			y1 = y;
		}
	
		_flu_float len = 1./(y1-y0);
		_int_u _x, xs;
		wide = __buf[10];
		xs = x0-wide*0.5;
		y = y0;
		for(;y<y1;y++) {
			_x = 0;
			for(;_x != wide;_x++) {
				x = _x+xs;
				tx = x>>NT_TILE_SHFT;
				txo = x&NT_TILE_MASK;
				ty = y>>NT_TILE_SHFT;
				to = (txo|((y&NT_TILE_MASK)<<NT_TILE_SHFT))*4;
				t = sf->tiles+tx+(ty*sf->tw);
				talter(sf, t);
				d = t->map0+to;
				_flu_float z;
				z = z0+(_z*len*(y-y0));	
				_flu_float *dv = rb->dpb+x+(y*sf->width);
				if (z<=*dv) {

					s[0] = 255.**buf;
					s[1] = 255.*buf[1];
					s[2] = 255.*buf[2];
					s[3] = 255.*buf[3];
					nt_setpix(__ctx, d, s);
					*dv = z;
				}
			}
		}
	} else if (y0 == y1) {
		if (x0>x1) {
			x = x0;
			x0 = x1;
			x1 = x;
		}

		_flu_float len = 1./(x1-x0);
		_int_u _y, ys;
		wide = __buf[10];
		ys = y0-wide*0.5;
		x = x0;
		for(;x<x1;x++) {
			_y = 0;
			for(;_y != wide;_y++) {
				y = _y+ys;
				ty = y>>NT_TILE_SHFT;
				tyo = (y&NT_TILE_MASK)<<NT_TILE_SHFT;
				tx = x>>NT_TILE_SHFT;
				to = ((x&NT_TILE_MASK)|tyo)*4;
				t = sf->tiles+tx+(ty*sf->tw);
				talter(sf, t);
				_flu_float z;
				z = z0+(_z*len*(x-x0));
				d = t->map0+to;
				_flu_float *dv = rb->dpb+x+(y*sf->width);
				if (z<=*dv) {
					s[0] = 255.**buf;
					s[1] = 255.*buf[1];
					s[2] = 255.*buf[2];
					s[3] = 255.*buf[3];
					nt_setpix(__ctx, d, s);
					*dv = z;
				}
			}
		}
	} else{
		if (y1>y0) {
			line_dsc(__ctx, x0, y0, x1, y1, buf, z1-z0, z0);
		} else if (y1<y0) {
			line_dsc(__ctx, x1, y1, x0, y0, buf, z1-z0, z0);		
		}
	}
}
void nt_line(nt_contextp __ctx, _flue_float *__buf, _int_u __n) {
	_flue_float *p;
	p = __buf;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		_line(__ctx, p);
		p+=11;
	}
}

