# include "common.h"
# include "../../m_alloc.h"
struct nt_bo* nt_bo_new(void) {
	struct nt_bo *buf;
	buf = (struct nt_bo*)m_alloc(sizeof(struct nt_bo));
	return buf;
}


void nt_bo_map(struct nt_bo *__buf) {
	__buf->sh.cpu_map = m_alloc(__buf->sh.size);
}

void nt_bo_unmap(struct nt_bo *__buf) {
	m_free(__buf->sh.cpu_map);
}

void nt_bo_destroy(struct nt_bo *__buf) {
	m_free(__buf);
}

