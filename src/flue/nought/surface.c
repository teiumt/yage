# include "common.h"
# include "macros.h"
# include "../../io.h"
# include "../../m_alloc.h"
# include "../../assert.h"
# include "../../string.h"
struct nt_surf *nt_surf_new(_32_u __width, _32_u __height) {
	struct nt_surf *sf;
	sf = (struct nt_surf*)m_alloc(sizeof(struct nt_surf));

	_int_u w, h;
	w = (__width+NT_TILE_MASK)&~NT_TILE_MASK;
	h = (__height+NT_TILE_MASK)&~NT_TILE_MASK;

	_int_u tw, th;
	tw = w>>NT_TILE_SHFT;
	th = h>>NT_TILE_SHFT;
	sf->tw = tw;
	sf->th = th;
	sf->width = __width;
	sf->height = __height;
	sf->tn = tw*th;
	F_assert(sf->tn != 0);
	sf->tiles = (struct nt_tile*)m_alloc(sf->tn*sizeof(struct nt_tile));
	sf->w = w;
	sf->h = h;
	sf->map = (_8_u*)m_alloc((sf->tn<<(NT_TILE_SHFT+NT_TILE_SHFT))*4);

	sf->tb = (struct nt_tb*)m_alloc(sf->tn*sizeof(struct nt_tb));
	struct nt_tile *t;
	_int_u i;
	i = 0;
	_32_u x, y;
	_32_u yy = tw;

	y = 0;
	for(;y != h;y+=NT_TILE_SIZE) {
		x = 0;
		for(;x != w;x+=NT_TILE_SIZE) {
			t = sf->tiles+i;
			t->map0 = sf->map+((i<<(NT_TILE_SHFT+NT_TILE_SHFT))*4);
			t->map1 = NULL;
			t->av = -1;
			t->bits = 0x00;
			t->x = x;
			t->y = y;
			i++;
		}
	}

	sf->_tb = 0;
	return sf;
}

void nt_surf_clear(struct nt_surf *__sf) {
	struct nt_tile *t;
	_int_u x;
	x = 0;
	for(;x != __sf->tn;x++) {
		t = __sf->tiles+x;
		_int_u i;
		i = 0;
		for(;i != NT_TILE_SIZE*4;i+=4) {
			t->map0[i] = 0x00;
			t->map0[i+1] = 0x00;
			t->map0[i+2] = 0xff;
			t->map0[i+3] = 0x00;
		}

		_int_u d;
		i = 0;
		for(;i != NT_TILE_SIZE;i++) {
			d = (((i+1)<<NT_TILE_SHFT)*4)-4;
			t->map0[d] = 0x00;
			t->map0[d+1] = 0x00;
			t->map0[d+2] = 0xff;
			t->map0[d+3] = 0x00;
		}

		i = 1;
		for(;i != NT_TILE_SIZE;i++) {
			d = (i<<NT_TILE_SHFT)*4;
			mem_set(t->map0+d, 0x00, (NT_TILE_SIZE*4)-4);
		}
	}
}

void nt_surf_destroy(struct nt_surf *__sf) {
	m_free(__sf->map);
	m_free(__sf->tiles);
	m_free(__sf->tb);
	m_free(__sf);
}


void nt_surf_write(struct nt_surf *__sf, void *__buf, _32_u __x, _32_u __y, _32_u __width, _32_u __height) {
	_32_u tx, ty;
	_32_u xs, ys;
	_32_u xe, ye;
	
	_32_u txo, tyo;

	txo = __x&NT_TILE_MASK;
	tyo = __y&NT_TILE_MASK;

	/*
		get all the middle whole tiles
	*/
	ys = (__y+NT_TILE_MASK)&~NT_TILE_MASK;
	xs = (__x+NT_TILE_MASK)&~NT_TILE_MASK;
	xe = (__x+__width)&~NT_TILE_MASK;
	ye = (__y+__height)&~NT_TILE_MASK;
	_32_u x, y;
	_32_u ttx, tty;

	struct nt_tile *dt;
	_8_u *s, *d;
	ty = ys;
	while(y != ye) {
		tx = xs;
		while(tx != xe) {
			ttx = tx>>NT_TILE_SHFT;
			tty = ty>>NT_TILE_SHFT;
			dt = __sf->tiles+ttx+(tty*__sf->tw);
			nt_tile_ww(dt, __buf, tx-xs, ty-ys, __width, __height);
			tx+=NT_TILE_SIZE;
		}
		ty+=NT_TILE_SIZE;
	}
}

void nt_surf_copy(struct nt_surf *__dst, struct nt_surf *__src, _32_u __x, _32_u __y) {
	struct nt_tile *dt, *st;
	_32_u txo, tyo;
	txo = __x&NT_TILE_MASK;
	tyo = __y&NT_TILE_MASK;

	if (!txo && !tyo) {

	_32_u txd, tyd;
	txd = __x>>NT_TILE_SHFT;
	tyd = __y>>NT_TILE_SHFT;

	_32_u tx, ty, _tx, _ty;
	ty = 0;
	while(ty != __src->th) {
		_ty = ty;
		tx = 0;
		while(tx != __src->tw) {
			_tx = tx;
			dt = __dst->tiles+_tx+(_ty*__dst->tw);
			st = __src->tiles+tx+(ty*__src->tw);

			mem_cpy(dt->map0, st->map0, 1<<(NT_TILE_SHFT+NT_TILE_SHFT));
			tx++;
		}
		ty++;
	}
	} else {
		_32_u tx, ty;
		_32_u xt, yt;
		_32_u _x, _y;
		_32_u x, y;
		_8_u *d, *s;
		xt = __y>>NT_TILE_SHFT;
		yt = __x>>NT_TILE_SHFT;

		_32_u txo_inv, tyo_inv;
		txo_inv = NT_TILE_SIZE-txo;
		tyo_inv = NT_TILE_SIZE-tyo;
		ty = 0;
		while(ty != __src->th) {
			tx = 0;
			_y = ty+ty;
			while(tx != __src->tw) {
				st = __src->tiles+tx+(ty*__src->tw);
	
				_x = xt+tx;

				// block-A
				dt = __dst->tiles+_x+(_y*__dst->tw);
				y = tyo;
				for(;y != tyo_inv;y++) {
					x = txo;
					for(;x != txo_inv;x++) {
						d = dt->map0+((x+(y<<NT_TILE_SHFT))*4);
						s = st->map0+(((x-txo)+((y-tyo)<<NT_TILE_SHFT))*4);
						*(_32_u*)d = *(_32_u*)s;
					}
				}
		
				// block-B
				dt = __dst->tiles+(_x+1)+(_y*__dst->tw);
				y = tyo;
				for(;y != (NT_TILE_SIZE-tyo);y++) {
					x = 0;
					for(;x != txo;x++) {
						d = dt->map0+((x+(y<<NT_TILE_SHFT))*4);
						s = st->map0+(((txo_inv+x)+((y-tyo)<<NT_TILE_SHFT))*4);
						*(_32_u*)d = *(_32_u*)s;
					}
				}

				// block-C
				dt = __dst->tiles+_x+((_y+1)*__dst->tw);
				y = 0;
				for(;y != tyo;y++) {
					x = txo;
					for(;x != txo_inv;x++) {
						d = dt->map0+((x+(y<<NT_TILE_SHFT))*4);
						s = st->map0+(((x-txo)+((tyo_inv+y)<<NT_TILE_SHFT))*4);
						*(_32_u*)d = *(_32_u*)s;
					}
				}

				// block-D
				dt = __dst->tiles+(_x+1)+((_y+1)*__dst->tw);
				y = 0;
				for(;y != tyo;y++) {
					x = 0;
					for(;x != txo;x++) {
						d = dt->map0+((x+(y<<NT_TILE_SHFT))*4);
						s = st->map0+(((txo_inv+x)+((tyo_inv+y)<<NT_TILE_SHFT))*4);
						*(_32_u*)d = *(_32_u*)s;
					}
				}
				tx++;
			}
			ty++;
		}

	}
}

/*
	memory must be aligned to tile size
	adds to more complexity when attempting to copy
*/
void nt_surf_read(struct nt_surf *__sf, void *__buf, _32_u __x, _32_u __y, _32_u __width, _32_u __height, _32_u __w) {
	_32_u tx, ty;
	_32_u xs, ys;
	_32_u xe, ye;
	
	_32_u txo, tyo;
	_32_u itxo, ityo;

	if (__width&NT_TILE_MASK || __height&NT_TILE_MASK) {
		f_printf("width and h__height must be aligned to %u.\n", NT_TILE_SIZE);
		return;
	}

	itxo = ((~__x)+1)&NT_TILE_MASK;
	ityo = ((~__y)+1)&NT_TILE_MASK;
	/*
		get all the middle whole tiles
	*/
	ys = (__y+NT_TILE_MASK)&~NT_TILE_MASK;
	xs = (__x+NT_TILE_MASK)&~NT_TILE_MASK;
	xe = (__x+__width)&~NT_TILE_MASK;
	ye = (__y+__height)&~NT_TILE_MASK;
	f_printf("SURFREAD: %u:%u, %u:%u.\n", xs, xe, ys, ye);
	_32_u x, y;
	if (xs == xe || ys == ye)
		return;
	txo = __x&NT_TILE_MASK;
	tyo = __y&NT_TILE_MASK;

	if (itxo>0 && ityo>0) {
		_8_u *s, *d;
		tx = __x>>NT_TILE_SHFT;
		ty = __y>>NT_TILE_SHFT;
		struct nt_tile *t;
		t = tileat(__sf, tx, ty);
		y = tyo;
		for(;y != NT_TILE_SIZE;y++) {
			x = txo;
			for(;x != NT_TILE_SIZE;x++) {
				s = tilepx(t, x, y);
				d = ((_8_u*)__buf)+(((x-txo)+((y-tyo)*__w))*4);
				dwmov(d, s);
			 }
		}

		tx = (__x+__width)>>NT_TILE_SHFT;
		t = tileat(__sf, tx, ty);
		_32_u xoff;
		xoff = xe-__x;
		y = tyo;
		for(;y != NT_TILE_SIZE;y++) {
			x = 0;
			for(;x != txo;x++) {
				s = tilepx(t, x, y);
				d = ((_8_u*)__buf)+(((xoff+x)+((y-tyo)*__w))*4);
				dwmov(d, s);
			}
		}

		_32_u yoff;
		yoff = ye-__y;
		tx = __x>>NT_TILE_SHFT;
		ty = (__y+__height)>>NT_TILE_SHFT;
		t = tileat(__sf, tx, ty);
		y = 0;
		for(;y != NT_TILE_SIZE;y++) {
			x = txo;
			for(;x != NT_TILE_SIZE;x++) {
				s = tilepx(t, x, y);
				d = ((_8_u*)__buf)+(((x-txo)+((y+yoff)*__w))*4);
				dwmov(d, s);
			}
		}

		tx = (__x+__width)>>NT_TILE_SHFT;
		t = t = tileat(__sf, tx, ty);
		y = 0;
		for(;y != tyo;y++) {
			x = 0;
			for(;x != txo;x++) {
				s = tilepx(t, x, y);
				d = ((_8_u*)__buf)+(((x+xoff)+((y+yoff)*__w))*4);
				dwmov(d, s);
			}
		}
	}

	if (itxo>0) {
		_8_u *s, *d;
		struct nt_tile *t;
		_32_u xoff = xe-__x;
		tx = __x>>NT_TILE_SHFT;
		_32_u txx;
		txx = (__x+__width)>>NT_TILE_SHFT;
		ty = ys;
		_32_u _yof;
		while(ty != ye) {
			t = tileat(__sf, tx, ty>>NT_TILE_SHFT);
			y = 0;
			_yof = ty-__y;
			for(;y != NT_TILE_SIZE;y++) {
				x = txo;
				for(;x != NT_TILE_SIZE;x++) {
					s = tilepx(t, x, y);
					d = ((_8_u*)__buf)+(((x-txo)+((_yof+y)*__w))*4);
					dwmov(d, s);
				}
			}

			t = tileat(__sf, txx, ty>>NT_TILE_SHFT);
			y = 0;
			for(;y != NT_TILE_SIZE;y++) {
				x = 0;
				for(;x != txo;x++) {
					s = tilepx(t, x, y);
					d = ((_8_u*)__buf)+(((xoff+x)+((_yof+y)*__w))*4);
					dwmov(d, s);
				}
			}

			ty+=NT_TILE_SIZE;
		}
	}

	if (ityo>0) {
		_8_u *s, *d;
		struct nt_tile *t;
		_32_u yoff = ye-__y;
		_32_u tyy;
		ty = __y>>NT_TILE_SHFT;
		tyy = (__y+__height)>>NT_TILE_SHFT;
		tx = xs;
		_32_u _xof;
		while(tx != xe) {
			t = tileat(__sf, tx>>NT_TILE_SHFT, ty);
			y = tyo;
			_xof = tx-__x;
			for(;y != NT_TILE_SIZE;y++) {
				x = 0;
				for(;x != NT_TILE_SIZE;x++) {
					s = tilepx(t, x, y);
					d = ((_8_u*)__buf)+(((_xof+x)+((y-tyo)*__w))*4);
					dwmov(d, s);
				}
			}

			t = tileat(__sf, tx>>NT_TILE_SHFT, tyy);
			y = 0;
			for(;y != tyo;y++) {
				x = 0;
				for(;x != NT_TILE_SIZE;x++) {
					s = tilepx(t, x, y);
					d = ((_8_u*)__buf)+(((_xof+x)+((y+yoff)*__w))*4);
					dwmov(d, s);
				}
			}

			tx+=NT_TILE_SIZE;
		}
	}
	
	struct nt_tile *st;
	_8_u *s, *d;
	y = ys;
	while(y != ye) {
		x = xs;
		while(x != xe) {
			tx = x>>NT_TILE_SHFT;
			ty = y>>NT_TILE_SHFT;
			st = __sf->tiles+tx+(ty*__sf->tw);
			nt_tile_wr(st, __buf, (x-xs)+itxo, (y-ys)+ityo, __w, __height);
			x+=NT_TILE_SIZE;
		}
		y+=NT_TILE_SIZE;
	}
}
