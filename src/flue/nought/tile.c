# include "common.h"
// tile whole write
void nt_tile_ww(struct nt_tile *__d, _8_u *__s, _32_u __x, _32_u __y, _32_u __width, _32_u eight) {
	_32_u x, y;

	_8_u *s, *d;
	y = 0;
	for(;y != NT_TILE_SIZE;y++) {
		x = 0;
		for(;x != NT_TILE_SIZE;x++) {
			s = __s+(((__x+x)+((__y+y)*__width))*4);
			d = __d->map0+((x|(y<<NT_TILE_SHFT))*4);
			*(_32_u*)d = *(_32_u*)s;
		}
	}
}

void nt_tile_wr(struct nt_tile *__s, _8_u *__d, _32_u __x, _32_u __y, _32_u __width, _32_u eight) {
	_32_u x, y;

	_8_u *s, *d;
	y = 0;
	for(;y != NT_TILE_SIZE;y++) {
		x = 0;
		for(;x != NT_TILE_SIZE;x++) {
			d = __d+(((__x+x)+((__y+y)*__width))*4);
			s = __s->map0+((x|(y<<NT_TILE_SHFT))*4);
			*(_32_u*)d = *(_32_u*)s;
		}
	}
}
