# include "common.h"
# include "../../m_alloc.h"
nt_contextp nt_ctx_new(struct nt_dev *__d) {
	nt_contextp ctx;
	ctx = (nt_contextp)m_alloc(sizeof(struct nt_context));
	ctx->d = __d;
	return ctx;
}

void nt_ctx_destroy(nt_contextp __ctx) {
	m_free(__ctx);
}
