# include "common.h"
# include "../../io.h"
# include "../../assert.h"
struct vertex {
	_flu_float x, y, z, w;
};

_8_s static
dim(_flue_float __x, _flue_float __y, struct vertex *__v) {
	_flue_float x, y;
	x = __x-__v[0].x;
	y = __y-__v[0].y;

	_flue_float f0, f1, f2, f3, f4, f5;
	f0 = x*(__v[2].y-__v[0].y);
	f1 = y*(__v[2].x-__v[0].x);

	f2 = x*(__v[0].y-__v[1].y);
	f3 = y*(__v[0].x-__v[1].x);
 
	f4 = ((__v[2].x-__v[0].x)-x)*(__v[2].y-__v[1].y);
	f5 = (__v[2].x-__v[1].x)*((__v[2].y-__v[0].y)-y);
	if ((f0-f1)*(f2-f3)>=0 && (f2-f3)*(f4-f5)>=0 && (f0-f1)*(f4-f5)>=0)
		return 0;
	return -1;
}
# include "../../maths.h"
#define max3(__a, __b, __c) (__a<__b?(__b<__c?__c:__b):(__a<__c?__c:__a))
#define min3(__a, __b, __c) (__a<__b?(__a<__c?__a:__c):(__b<__c?__b:__c))
#define M0(__col) m[__col]
#define M1(__col) m[4+__col]
#define M2(__col) m[8+__col]
#define M3(__col) m[12+__col]
#define _VBX(__n) vb[NT_TR3_X ## __n]
#define _VBY(__n) vb[NT_TR3_Y ## __n]
#define _VBZ(__n) vb[NT_TR3_Z ## __n]
#define VBX(__n) buf[__n*8]
#define VBY(__n) buf[(__n*8)+1]
#define VBZ(__n) buf[(__n*8)+2]


#define dot(__x0, __y0, __x1, __y1)\
	(((__x0)*(__x1))+((__y0)*(__y1)))
#define dot3(__x0, __y0, __z0, __x1, __y1, __z1)\
	((__x0*__x1)+(__y0*__y1)+(__z0*__z1))



void static cross(_flu_float *__dst, _flu_float *__v0, _flu_float *__v1) {
	*__dst	  = __v0[1]*__v1[2]-__v0[2]*__v1[1];
	__dst[1]	= __v0[2]*__v1[0]-__v0[0]*__v1[2];
	__dst[2]	= __v0[0]*__v1[1]-__v0[1]*__v1[0];
}
void static norm(_flu_float *__v) {
	_flu_float r;
	r = sqr(__v[0]*__v[0]+__v[1]*__v[1]+__v[2]*__v[2]);
	if (r == .0) return;
	__v[0]/=r;
	__v[1]/=r;
	__v[2]/=r;
}

struct vec {
	_flu_float x, y, z;
};

#define V0 __v[0]
#define V1 __v[1]
#define V2 __v[2]
/*
	we are better doing it this way as
	single plane clipping is bad for preformance
*/

/*
	this code is duping i dont like it but its much better then the dynamic approch
	as we are dealing with constants ie. 0,1
	and moving a constant into a memory address is better then memory to memory or reg to memory i hope
*/

_int_u static 
tgl_wxclip(struct vertex *__v, _flu_float *__uv, struct vertex *__out) {
	_int n_in, n_out;
	n_in = 0;
	n_out = 0;
	struct vertex *in[3], *out[3];
	if (V0.x>=0) {
		in[n_in] = &V0;
		n_in++;
	} else {
		out[n_out] = &V0;
		n_out++;
	}
	if (V1.x>=0) {
		in[n_in] = &V1;
		n_in++;
	} else {
		out[n_out] = &V1;
		n_out++;
	}
	if (V2.x>=0) {
		in[n_in] = &V2;
		n_in++;
	} else {
		out[n_out] = &V2;
		n_out++;
	}

	if (n_in == 3) {
		n_in = 0;
		n_out = 0;
		if (V0.x<=1) {
			in[n_in] = &V0;
			n_in++;
		} else {
			out[n_out] = &V0;
			n_out++;
		}
		if (V1.x<=1) {
			in[n_in] = &V1;
			n_in++;
		} else {
			out[n_out] = &V1;
			n_out++;
		}
		if (V2.x<=1) {
			in[n_in] = &V2;
			n_in++;
		} else {
			out[n_out] = &V2;
			n_out++;
		}


		_flu_float dom;
		_flu_float xd, yd;
		_flu_float x;
		if (n_out == 2 && n_in == 1) {	
			_flu_float _dom;
			_flu_float _xd, _yd;
			xd = out[0]->x-in[0]->x;
			yd = out[0]->y-in[0]->y;
			_xd = out[1]->x-in[0]->x;
			_yd = out[1]->y-in[0]->y;
			assert(xd != 0 && _xd != 0);

			__out[0] = *in[0];
			__out[1].x = 1;
			__out[1].y = in[0]->y-(yd/xd)*(in[0]->x-1);
			__out[1].z = out[0]->z;
			__out[2].x = 1;
			__out[2].y = in[0]->y-(_yd/_xd)*(in[0]->x-1);
			__out[2].z = out[1]->z;
			return 1;
		}

		if (n_out != 1) return 0;
		xd = in[0]->x-out[0]->x;
		yd = in[0]->y-out[0]->y;
		dom = 1./xd;
		x = out[0]->x-1;
		__out[0] = *in[0];
		__out[1] = *in[1];
		__out[2].x = 1;
		__out[2].y = out[0]->y-(yd*dom)*x;
		__out[2].z = out[0]->z;

		xd = in[1]->x-out[0]->x;
		yd = in[1]->y-out[0]->y;
		dom = 1./xd;
		__out[3] = *in[1];
		__out[4] = __out[2];
		__out[5].x = 1;
		__out[5].y = out[0]->y-(yd*dom)*x;
		__out[5].z = out[0]->z;

		return 2;
	}

	if (n_out == 3)
		return 0;
	_flu_float dom;
	_flu_float xd, yd;
	_flu_float x;
	if (n_out == 2 && n_in == 1) {	
		_flu_float _dom;
		_flu_float _xd, _yd;
		xd = out[0]->x-in[0]->x;
		yd = out[0]->y-in[0]->y;
		_xd = out[1]->x-in[0]->x;
		_yd = out[1]->y-in[0]->y;
		assert(xd != 0 && _xd != 0);

		__out[0] = *in[0];
		__out[1].x = 0;
		__out[1].y = in[0]->y-(yd/xd)*in[0]->x;
		__out[1].z = out[0]->z;
		__out[2].x = 0;
		__out[2].y = in[0]->y-(_yd/_xd)*in[0]->x;
		__out[2].z = out[1]->z;
		return 1;
	}

	if (n_out != 1) return 0;
	xd = in[0]->x-out[0]->x;
	yd = in[0]->y-out[0]->y;
	dom = 1./xd;
	x = out[0]->x;
	__out[0] = *in[0];
	__out[1] = *in[1];
	__out[2].x = 0;
	__out[2].y = out[0]->y-(yd*dom)*x;
	__out[2].z = out[0]->z;

	xd = in[1]->x-out[0]->x;
	yd = in[1]->y-out[0]->y;
	dom = 1./xd;
	__out[3] = *in[1];
	__out[4] = __out[2];
	__out[5].x = 0;
	__out[5].y = out[0]->y-(yd*dom)*x;
	__out[5].z = out[0]->z;
	return 2;
}

void static
_rast_tri3(nt_contextp __ctx, struct vertex *v, _flu_float *__norm, _flu_float light, _flu_float *uv, _flu_float *buf) {
	if (v[0].x<0 || v[1].x<0 || v[2].x<0 || v[0].x>1 || v[1].x>1 || v[2].x>1) {
//		ffly_fprintf(ffly_log, "Xissue: %f, %f, %f\n", v[0].x, v[1].x, v[2].x);

		return;
	}
	
	if (v[0].y<0 || v[1].y<0 || v[2].y<0 || v[0].y>1 || v[1].y>1 || v[2].y>1) {
//		ffly_fprintf(ffly_log, "Yissue: %f, %f, %f\n", v[0].y, v[1].y, v[2].y);
		return;
	}

	_32_u x, y;
	_32_u nx, ny;
	_32_u fx, fy;

	struct nt_tex *tx = __ctx->tx[0];
	assert((tx->sh.bits&FLUE_TEX_MMAP)>0);
//	printf("triangle, texture: %u, %u\n", tx->sh.width, tx->sh.height);
	v[0].x*=__ctx->sh.vp.width;
	v[0].y*=__ctx->sh.vp.height;
	v[1].x*=__ctx->sh.vp.width;
	v[1].y*=__ctx->sh.vp.height;
	v[2].x*=__ctx->sh.vp.width;
	v[2].y*=__ctx->sh.vp.height;

	/*
		NOTE:
			near must round down
			far must round up
	*/
	nx = min3(v[0].x, v[1].x, v[2].x);
	fx = ceil(max3(v[0].x, v[1].x, v[2].x));
	ny = min3(v[0].y, v[1].y, v[2].y);
	fy = ceil(max3(v[0].y, v[1].y, v[2].y));
	struct nt_rb *rb;
	struct nt_surf *sf;
	struct nt_tile *t;

	rb = __ctx->render[0];
	
	sf = rb->sf;
//	printf("%f, %f.\n", __ctx->sh.vp.width, __ctx->sh.vp.height);
//	printf("%u>%u, %u>%u.\n", nx, fx, ny, fy);
//	printf("%f, %f, %f, %f, %f, %f.\n", v[0].x, v[0].y, v[1].x, v[1].y, v[2].x, v[2].y);
	// im lazy this will do for now for testing
	
	_32_u _tx, ty;
	_32_u to, yo;
	y = ny;
	_8_u *d;
	_8_u *s;
	while(y < fy) {
		ty = y>>NT_TILE_SHFT;
		yo = (y&NT_TILE_MASK)<<NT_TILE_SHFT;
		x = nx;
		while(x < fx) {
			_tx = x>>NT_TILE_SHFT;
			t = sf->tiles+_tx+(ty*sf->tw);
			to = ((x&NT_TILE_MASK)|yo)*4;
			if (!dim(x, y, v)) {
				talter(sf, t);
				_flu_float den;
				_flu_float a, b, g;
				_flu_float _x, _y;
				_x = ((_flu_float)x);
				_y = ((_flu_float)y);
				den = (v[1].y-v[2].y)*(v[0].x-v[2].x)+(v[2].x-v[1].x)*(v[0].y-v[2].y);
				den = 1./den;
				a = ((v[1].y-v[2].y)*(_x-v[2].x)+(v[2].x-v[1].x)*(_y-v[2].y))*den;
				b = ((v[2].y-v[0].y)*(_x-v[2].x)+(v[0].x-v[2].x)*(_y-v[2].y))*den;
				g = (1.-a)-b;
				_flu_float u, v_;

				u = a*uv[NT_TR3_U0]+b*uv[NT_TR3_U1]+g*uv[NT_TR3_U2];
				v_ = a*uv[NT_TR3_V0]+b*uv[NT_TR3_V1]+g*uv[NT_TR3_V2];

				_32_u _u, _v;
				_u = floor(u*(_flu_float)tx->sh.width);
				_v = floor(v_*(_flu_float)tx->sh.height);

//				printf("%f, %f\n", u, v);
				_v = _v*tx->sh.width;
				_32_u dummy = 0xffffffff;
				s = ((_8_u*)tx->sh.mmap)+((_u+_v)*4);
				if ((_u+_v)>=(tx->sh.width*tx->sh.height)) {
					s = &dummy;
					printf("error.\n");
				}
		
				//printf("%f.\n", b3);
				_flu_float col[16];
				col[0] = buf[4]*a;
				col[1] = buf[5]*a;
				col[2] = buf[6]*a;
				col[3] = buf[7]*a;

				col[4] = buf[12]*b;
				col[5] = buf[13]*b;
				col[6] = buf[14]*b;
				col[7] = buf[15]*b;

				col[8] = buf[20]*g;
				col[9] = buf[21]*g;
				col[10] = buf[22]*g;
				col[11] = buf[23]*g;

				_flu_float z, z0, z1, z2;
				z = v[0].z+b*(v[1].z-v[0].z)+g*(v[2].z-v[0].z);
				_flu_float *dv = rb->dpb+x+(y*sf->width);
				if (z>=*dv) {
					d = t->map0+to;
					_flu_float pos[4];
					_nt_setpix(__ctx, d, s, pos);
					
					d[0] = ((d[0]*(col[0]+col[4]+col[8]))/255.)*light;
					d[1] = ((d[1]*(col[1]+col[5]+col[9]))/255.)*light;
					d[2] = ((d[2]*(col[2]+col[6]+col[10]))/255.)*light;					
					*dv = z;
				}
			}
			x++;
		}
		y++;
	}


}


void static
rast_tri3(nt_contextp __ctx, struct nt_tri3 *__t) {
	_flue_float *vb = __t->buf;
	_flue_float *uv = __t->uv;

	_flu_float buf[24];

	buf[0] = _VBX(0);
	buf[1] = _VBY(0);
	buf[2] = _VBZ(0);

	buf[8] = _VBX(1);
	buf[9] = _VBY(1);
	buf[10] = _VBZ(1);
	
	buf[16] = _VBX(2);
	buf[17] = _VBY(2);
	buf[18] = _VBZ(2);
	nt_vertex(__ctx, buf, 3);
	struct vertex v[3];
	_flue_float *m;
	m = __ctx->m;
	_flue_float t0, t1, t2, w;
	w = VBX(0)*M3(0)+VBY(0)*M3(1)+VBZ(0)*M3(2)+M3(3);
	t0 = VBX(0)*M0(0)+VBY(0)*M0(1)+VBZ(0)*M0(2)+M0(3);
	t1 = VBX(0)*M1(0)+VBY(0)*M1(1)+VBZ(0)*M1(2)+M1(3);
	t2 = VBX(0)*M2(0)+VBY(0)*M2(1)+VBZ(0)*M2(2)+M2(3);
	if (w != 0) {
		t0/=w;
		t1/=w;
//		t2/=w;
	}

	v[0].x = __ctx->sh.vp.x+t0;
	v[0].y = __ctx->sh.vp.y+t1;
	v[0].z = t2;
	v[0].w = w;

	w = VBX(1)*M3(0)+VBY(1)*M3(1)+VBZ(1)*M3(2)+M3(3);
	t0 = VBX(1)*M0(0)+VBY(1)*M0(1)+VBZ(1)*M0(2)+M0(3);
	t1 = VBX(1)*M1(0)+VBY(1)*M1(1)+VBZ(1)*M1(2)+M1(3);
	t2 = VBX(1)*M2(0)+VBY(1)*M2(1)+VBZ(1)*M2(2)+M2(3);
	if (w != 0) {
		t0/=w;
		t1/=w;
//		t2/=w;
	}

	v[1].x = __ctx->sh.vp.x+t0;
	v[1].y = __ctx->sh.vp.y+t1;
	v[1].z = t2;
	v[1].w = w;

	w = VBX(2)*M3(0)+VBY(2)*M3(1)+VBZ(2)*M3(2)+M3(3);
	t0 = VBX(2)*M0(0)+VBY(2)*M0(1)+VBZ(2)*M0(2)+M0(3);
	t1 = VBX(2)*M1(0)+VBY(2)*M1(1)+VBZ(2)*M1(2)+M1(3);
	t2 = VBX(2)*M2(0)+VBY(2)*M2(1)+VBZ(2)*M2(2)+M2(3);
	if (w != 0) {
		t0/=w;
		t1/=w;
//		t2/=w;
	}

	v[2].x = __ctx->sh.vp.x+t0;
	v[2].y = __ctx->sh.vp.y+t1;
	v[2].z = t2;
	v[2].w = w;
/*	if (v[0].z<-1 ||v[1].z<-1 || v[2].z<-1) {
		printf("Z-ERROR: %f- %f- %f.\n", v[0].z,v[1].z,v[2].z);
		return;
	}


	if (v[0].z>0 ||v[1].z>0 || v[2].z>0) {
		printf("Z-ERROR: %f- %f- %f.\n", v[0].z,v[1].z,v[2].z);
		return;
	}
*/
	_flu_float p0[3], p1[3];
	_flu_float normal[3];

	p0[0] = VBX(1)-VBX(0);
	p0[1] = VBY(1)-VBY(0);
	p0[2] = VBZ(1)-VBZ(0);
	p1[0] = VBX(2)-VBX(0);
	p1[1] = VBY(2)-VBY(0);
	p1[2] = VBZ(2)-VBZ(0);

	normal[0] = vb[9];
	normal[1] = vb[10];
	normal[2] = vb[11];
	norm(normal);

	_flu_float light = dot3(0, 0, 1, normal[0], normal[1], normal[2]);
	if (light<0.21299)
		light = 0.21299;
	if (light>1)
		light = 1;
	light = 1;

	if (v[0].x<0 || v[1].x<0 || v[2].x<0 || v[0].x>1 || v[1].x>1 || v[2].x>1) {
		goto _clipping;
	}
	
	if (v[0].y<0 || v[1].y<0 || v[2].y<0 || v[0].y>1 || v[1].y>1 || v[2].y>1) {
		return;
		goto _clipping;
	}

	_rast_tri3(__ctx, v, normal, light, uv, buf);
	return;
_clipping:
{
	struct vertex tmpbuf[64];
	_int_u n;
	n = tgl_wxclip(v, uv, tmpbuf);
	if (n != 0) {		
		_rast_tri3(__ctx, tmpbuf, normal, light, uv, buf);

		if (n>1) {
			_rast_tri3(__ctx, tmpbuf+3, normal, light, uv, buf);
		}
	}
}
}

void nt_raster_tgl(nt_contextp __ctx, struct nt_tri3 *__tri, _32_u __n) {
	struct nt_tri3 *t;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		t = __tri+i;
		rast_tri3(__ctx, t);
	}
}
