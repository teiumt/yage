#ifndef __flue__shader__h
#define __flue__shader__h
#include "common.h"
/*
	this structure contains a shader in its premature state.

	EXAMPLE-CODE:

	struct flue_shader *sh;

	
	- load shader program, NOTE: at this point knowledge about device is a mystery
	- this part should have no connection to FLUE.
	struct ima_shader *ima;
	ima = fsl_load("fragment.fsl");

	

	- returns shader, this struct should also include info about its origin(who generated it(IMA)).

	- now we pass this to flue and it gives us a usable item for use in rendering process.
	sh = flue_shader_new(ima);

	HOW OPENGL DOES THINGS:

	shader glCreateShader(TYPE);
	glShaderSource(shader);
	glCompileShader(shader) <-- there the first bit of magic happens (GLSL->IR)

	prog = glCreateProgram();
	glAttachShader(prog,shader);
	glAttachShader(prog); <-- where second bit of magic come in (GLSL->IR->NIR) and in some cases to GPU language

CRITIQUE:
	glCreateShader() and glShaderSource() functions serve no purpose and do nothing but set data.

	glCreateProgram() whats the point in programs? this restricts and user and makes things difficult to use the same vertex shader while using diffrent fragment shaders.
	- a pointless process i digress.

	and MESA dosent make this apparent.
	for example in code somtimes programs are treated as a single vertex shader, wtf?

*/


/*
	Buffer binding.


	when defining a symbol for FSL it must be passed a descriptor for the buffer.
	this descriptor tells the backend how in its language the buffer shall be delt with.


	What makes sence and what dosent?

	not all GPUS are the same and have their own limitations.

	- when compiling the shader we tell the shader to bind this symbol to this SLOT
	shader_allot("testA",FLUE_USERSLOT0);
	shader_allot("testB",FLUE_USERSLOT1);

	flue_resource(buffer,FLUE_USERSLOT0,PLACEMENT0);
	flue_resource(buffer,FLUE_USERSLOT1,PLACEMENT0);

	- NOTE: above is somthing thats allowed, a SLOT could point to the same location
*/

/*
	shader texture binding OpenGL
	GLint loc;
	loc = glGetUniformLocation(program,"texture_ident");

	- this is very simple, in opengl it binds a shader location to a texture unit.
	- the first parameter takes the location of the shader uniform, the second one defines the textures unit (0-31)
	glUniform1i(loc,0);
	
	- the texture unit corresponds to GL_TEXTURE# with formula GL_TEXTURE#texture unit 0-31

	glActiveTexture(GL_TEXTURE0){
		ctx->Texture.CurrentUnit = GL_TEXTURE0;
	}

	- this takes in ctx->Texture.CurrentUnit from above api call
	glBindTexture(GL_TEXTURE_2D,texture_buffer);

	- this OpenGL code works like this. first glActiveTexture will select the texture we are going to work on.
	- glBindTexture uses what glActiveTexture selected and bind the buffer to that location.

*/

/*
	binding points/slots

	NOTE:
	slots can be setup to point to the same buffer.
*/
#define FLUE_USERSLOT(__x) (FLUE_USERSLOT0+__x)
#define FLUE_EXPORT_COLOUR			0
#define FLUE_EXPORT_VERTEX			1
#define FLUE_EXPORT_PARAM0			2
#define FLUE_EXPORT_PARAM1			3
#define FLUE_EXPORT_PARAM2			4
#define FLUE_EXPORT_PARAM3			5
#define FLUE_EXPORT_PARAM4			6
#define FLUE_EXPORT_PARAM5			7

//the binding point for the consts buffer
#define FLUE_BUFFER_CONSTANTS		8
//user defined slots
/*
	could be buffer,texture or what ever
	NOTE: non-user slots ares still user slots 
	they are just predefined slots.
*/
#define FLUE_USERSLOT0					9
#define FLUE_USERSLOT1					10
#define FLUE_USERSLOT2					11
#define FLUE_USERSLOT3					12
#define FLUE_USERSLOT4					13
#define FLUE_USERSLOT5					14
#define FLUE_USERSLOT6					15
#define FLUE_USERSLOT7					16

#define FLUE_INTERP_MODE_FLAT			0
#define FLUE_INTERP_MODE_SMOOTH		1
void flue_clearslots(void);

void flue_rsdesc(_64_u __desc,_64_u __placement,_64_u __bits);
void flue_txdesc(_64_u __desc,_64_u __placement,_64_u __sampler);
void flue_shader_new(struct flue_shader*);
void flue_shader_destroy(struct flue_shader*);

/*
	NOTE: 
	when flue_shader_allot() is called shader language is left unknown;
	nothing is known until it comes time to compile the real source code.
*/
void flue_shader_allot(struct flue_shader*,char const*,_int_u,_64_u,_64_u,_64_u);


void flue_shader_compile(struct flue_shader*,char const*,_int_u);
void flue_shader_dump(struct flue_shader*);
/*
	struct flue_shader shader;
	flue_shader_new(&shader);

	flue_shader_allot(&shader,...);
	...
	.

	flue_shader_compile(&shader,"test.fsl",...);
	- at this stage we have turned the fsl into IMA code.
*/
#endif /*__flue__shader__h*/
