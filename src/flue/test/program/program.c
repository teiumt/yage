#include "../../mjx.h"
#include "../../../time.h"
#include "../../../string.h"
#include "../../shader.h"
#include "../../../im.h"
void static
shader_concoct(struct flue_shader *sh, char const *__str){
	flue_shader_compile(sh,__str,str_len(__str));
}

static struct flue_shader shader_ps;
static struct flue_shader shader_vs;

void static
shader_setup(void){
	flue_shader_new(&shader_ps);
	flue_shader_new(&shader_vs);

	flue_shader_allot(&shader_vs,"position",8,1,0,FLUE_USERSLOT0);
	flue_shader_allot(&shader_vs,"uv",2,1,0,FLUE_USERSLOT2);
	flue_shader_allot(&shader_ps,"texture",7,FLUE_ALLOT_SAMPLE,0,FLUE_USERSLOT1);
	shader_concoct(&shader_ps,"flue/test/program/test_ps.fsl");
	shader_concoct(&shader_vs,"flue/test/program/test_vs.fsl");
}

void static
framebuffer(_int_u __wdt, _int_u __hgt) {
  flue_framebuffer(__wdt,__hgt);
}

void static
viewport(void) {
  struct flue_viewport vp;
  vp.scale[0] = 1024;
  vp.scale[1] = 1024;
  vp.scale[2] = 1;

  vp.translate[0] = 0;
  vp.translate[1] = 0;
  vp.translate[2] = 0;
  flue_viewport(&vp,0,1);
}
#include "../../radeon/common.h"
static struct maj_conn *conn;
static struct mjx_window w;
void static
_draw(void){
	struct y_img *img;
	img = im_creat();

	im_load(img,"test.ppm");
	flue_shader(&shader_ps,0);
	flue_shader(&shader_vs,1);

	struct flue_bo *_uv;
	struct flue_bo *bo;
	bo = flue_bo_data(4*sizeof(float)*3);
	_uv = flue_bo_data(4*sizeof(float)*3);
	flue_bo_map(bo);
	flue_bo_map(_uv);
	
	float *vtx = bo->cpu_map;
	vtx[0] = 1;vtx[1] = 0;vtx[2] = 0;vtx[3] = 1.0;
	vtx[4] = 0;vtx[5] = 1;vtx[6] = 0;vtx[7] = 1.0;
	vtx[8] = 0;vtx[9] = 0;vtx[10] = 0;vtx[11] = 1.0;
	
	float *uv;
	uv = _uv->cpu_map;
	uv[0] = 1;uv[1] = 0;uv[2] = 0;uv[3] = 0;
	uv[4] = 0;uv[5] = 1;uv[6] = 0;uv[7] = 0;
	uv[8] = 0;uv[9] = 0;uv[10] = 0;uv[11] = 0;

	void *list[] = {bo};

	struct flue_resspec spec = {
    .stride = 4*sizeof(float),
    //what shader is this for?
    .sh = FLUE_RSVS,
    .fmt = {.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)},
    .id = FLUE_RS_VERTEX,
		/*
			the base plate for shader and flue,
			shader symbols get bound to these slots.
		*/
		.desc = FLUE_USERSLOT0
	};
	flue_rsdesc(FLUE_USERSLOT0,0,FLUE_RS_BUFFER_VERTEX);
	flue_rsdesc(FLUE_USERSLOT2,3,FLUE_RS_BUFFER_VERTEX);

	flue_txdesc(FLUE_USERSLOT1,1,2);
	/*
		where is this resource going to be placed?

	*/
	flue_resources(list,&spec,1,NULL);
	list[0] = _uv;
	spec.desc = FLUE_USERSLOT2;
	flue_resources(list,&spec,1,NULL);

	flue_texp t;
	struct flue_fmtdesc fmt = {.depth=4*sizeof(float),.nfmt=FLUE_NFMT(FLUE_NF_FLOAT),.dfmt=FLUE_DFMT(FLUE_DF_32_32_32_32)};
	t = flue_tex_new(img->width,img->height,&fmt,0);
	float *pixs = t->tx.ptr;
	img->rgb_rgba_float(img,pixs);

	void *textures[] = {t};
	void *tgs[] = {&w.fb};
	_64_u linking[] = {FLUE_USERSLOT1};
	struct flue_sampler s;
	flue_textures(textures,FLUE_RSPS,1,linking);
	flue_sampler(&s,1,linking);

	float r[4] = {0.0, 1.0, 1.0, 1.0};
	float g[4] = {0.0, 0.0, 1.0, 0.0};
	float b[4] = {0.0, 1.0, 0.0, 1.0};
	_int_u i;
	i = 0;
	for(;i != 1;i++){
	_int_u idx =0;// i&3;
	flue_rendertgs(tgs,1);
	flue_clear(tgs,1,r[idx],g[idx],b[idx],1.0);

	flue_draw_array(NULL,0,1,FLUE_TRI3);
/*
	_int_u x,y;
	y = 0;
	for(;y != 4;y++){
		float _y = y;
		vtx[1] = _y*0.25;
		vtx[5] = _y*0.25;
		vtx[9] = 0.25+_y*0.25;
		
		x = 0;
		for(;x != 4;x++){
			float _x = x;
			vtx[0] = _x*0.25;
			vtx[4] = _x*0.25+0.25;
			vtx[8] = _x*0.25+0.25;

			flue_draw_array(NULL,0,1,FLUE_TRI3);
		}
	}
*/
	mjx_swapbufs(conn,&w);
	doze(0,3);
	}

//	rd_shader_update(FLUE_CTX->priv);
	doze(0,16);
	flue_bo_unmap(bo);
	flue_bo_destroy(bo);
}

void test_program(void){
	conn = maj_connect();
	mjx_connect(conn);
	
	flue_prime();
	flue_sweep();

	FLUE_setctx(flue_ctx_new());

	flue_zbuffer(NULL);
	shader_setup();

	mjx_wdmap(conn,&w,0,0,1024,1024);


	viewport();
	framebuffer(1024,1024);

	_draw();
	mjx_swapbufs(conn,&w);
	/*
	_int_u c;
	c = 0;
	while(c != 0){
		doze(0,4);
		printf("C: %u.\n",c);
		void *tgs[] = {&w.fb};
		flue_clear(tgs,1,1.0,0.0,0.0,0.0);

		_draw();
		mjx_swapbufs(conn,&w);
		c++;
	}
*/
	mjx_wdunmap(conn,&w);
}
