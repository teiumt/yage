#include "../../../ihc/ihc.h"
#include "../../../ihc/fsl/fsl.h"
#include "../../compiler/ima/ima.h"
#include "../../../m_alloc.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../compiler/ima/radeon.h"
#include "../../../y_int.h"
#include "../../../types.h"
#include "../../../lib.h"
static struct rd_reg __r0 = {.rval=16};
static struct r_producer *producer;
void static builtin_vars(void){
	struct fsl_symb *s;
	s = fsl_symb("test",4);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.id = 0;
	s->s.em.val = 0;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = &__r0;
	s->s.em.ident = _ima_vec;
	s->s.em.placement = 0;
	
	s = fsl_symb("buffer0",7);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 0;
	s->s.em.id = 0;
	s->s.em.offset = 0;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->s.em.ident = _ima_buf;
	
	s = fsl_symb("buffer1",7);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 1;
	s->s.em.id = 0;
	s->s.em.offset = 0;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->s.em.ident = _ima_buf;
	
	s = fsl_symb("buffer14",8);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 1;
	s->s.em.id = 0;
	s->s.em.offset = 4;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->s.em.ident = _ima_buf;

	s = fsl_symb("constant",8);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 4;
	s->s.em.id = 0;
	s->s.em.offset = 0;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->s.em.ident = _ima_buf;
	
	s = fsl_symb("position",8);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 1;
	s->s.em.id = 0;
	s->s.em.offset = 0;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = IMA_PRIM;
	s->s.em.ident = _ima_buf;

	s = fsl_symb("pos",3);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.id = _ima_pos;
	s->s.em.val = 0;
	s->s.em.exp = 3;
	s->s.em.ident = _ima_exp;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;

	s = fsl_symb("col",3);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.exp = 2;
	s->s.em.id = _ima_col;
	s->s.em.val = 0;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.ident = _ima_exp;

	s = fsl_symb("texture",7);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 5;
	s->s.em.id = 0;
	s->s.em.offset = 0;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->s.em.ident = _ima_buf;

	s = fsl_symb("sampler",7);
	s->s.em.c.ys = &s->s.em;
	s->s.ign = -1;
	s->s.em.placement = 6;
	s->s.em.id = 0;
	s->s.em.offset = 0;
	s->s.em.val = 4;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->s.em.ident = _ima_buf;
}
static struct r_desc _desc_table[] = {
	{
		.val = 0,
		.type = _r_buf_desc
	},
	{
		.val = 16,
		.type = _r_buf_desc
	},
	{
		.val = R_REG_STAMP(_R_MRT),
		.type = _r_exp_col
	},
	{
		.val = R_REG_STAMP(_R_POS),
		.type = _r_exp_vert
	},
	{
		.val = 0,
		.type = _r_buf_spec
	},
	{
		.val = 0,
		.sampler = 24,
		.type = _r_img_nosample
	},
	{
		.val = 0,
		.sampler = 24,
		.type = _r_img_sample
	},
	{
		.atb = 0
	},
	{
		.atb = 1
	},
	{
		.atb = 2
	}
};
void static _compile(char const *__in, char const *__out){

	r_desc_table = _desc_table;
	struct hg_produce h;
	struct ima_produce m;
	ima_pd = &m;
	hg_pd = &h;
	fsl_init();
	/*
		we init FSL backend
	*/
	hg_init();

	ima_init();
	str_cpy(m.file.file_params.buf,__out);
	builtin_vars();
	
	ihc_util_file(&ima_pd->file);
	/*
		now we can get FSL to start working
	*/
	fsl_compile(__in);

	ima_ys();
	ima_takein_hg();

	//produces R-instrs
	r_propagate();
	
	ima_pd->file.open(&ima_pd->file);
	//produces final data
	r_produce(producer);
	ima_pd->file.close(&ima_pd->file);
	
	r_clean();
}
_y_err main(int __argc, char const *__argv[]){
	if(__argc<4){
		printf("./compile [input] [output] [ys/amde]");
		return -1;
	}

	char const *pr = __argv[3];
	if(!str_cmp(pr,"ys")){
		producer = &r_ys;
	}else
	if(!str_cmp(pr,"amde")){
		producer = &r_amde;
	}else{
		printf("unknown producer.\n");
		return;
	}
	_compile(__argv[1],__argv[2]);
}
