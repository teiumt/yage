#ifndef __flue__struc
#define __flue__struc
#include "../y_int.h"
#include "types.h"
#include "compiler/ima/r_struc.h"
/*
struct texture{
	struct texture *ptr;
};

struct framebuffer{
	struct texture *ptr;

	struct texture *attachments[2];
};

void main{
	void *list[] = {
		framebuffer->attachments[FL_BACK],
		texture
	}
	render_targets(list);

	void *list[] = {
		framebuffer->ptr,
		texture->ptr
	}
	render_targets(list);
}
*/
/*
	IMPORTANT:
	
	flue_viewport has constants or can have so no memaccess

	flue_viewport(memory,const,const)
	reg to reg move is faster then accessing memory directly
*/

#define FLUE_PLACEMAT0 0
#define FLUE_PLACEMAT1 1
#define FLUE_PLACEMAT2 2
#define FLUE_PLACEMAT3 3
enum {
	FLU_COLOR_SRGB
};

/*
	shader control structure size
*/
#define FLU_CNKSIZE 8
#define STATE_MAX 4
#define FMTD_SS	(64/8)
#define STATE_SS	(1024/8)
/*
	128-quadwords
*/
#define SHADER_CSS (1024/8)
#define FLUE_N_DRV 4
#define FLUE_CLEAR 0x01
struct flue_texx {
	union {
		_flue_float u0, v0;
		_flue_float u1, v1; 
		_flue_float u2, v2; 
	} data;
};

struct flue_texinfo {
	_flue_float *uv;
	flue_texp tx;
};

#define FLUE_TRI3 0x00
#define FLUE_RECT 0x01
#define FLUE_CRECT 0x02
#define FLUE_QUAD 0x06
struct flue_bo;

struct flue_shader_cs_variant {
	_64_u block_size[4];
};

struct flue_sampler {

};

struct ysa_data{
	void *ptr;
	_int_u size;
};

/*
  organization matters, we want the user to be able to do this flexibly
*/
struct flue_shader_allot{
  char ident[64];
  _int_u len;
  _64_u type;
  _64_u offset;
  _64_u placement;
  struct flue_shader_allot *next_allotment;
};

/*
	this structure is not bound to driver.
*/
struct flue_shader{
	_8_u *secondary;
  _int_u size;
  _8_u *primary;
  _int_u ps;
	char name[128];
	union {
		struct flue_shader_cs_variant cs;
	};
	void *priv;
	void *hg;
	struct flue_shader_allot *allotment;
	_16_u *interp_slots;
};
/*
	REMOVE:
*/
struct flue_yard {
	_int_u abouts;
	struct flue_bo *bo;
	_int_u dwc;//dwordcount
	_64_u pm;
};

/*
	NOTE:
		the size of the shader is unknown until compiled/asembled
		so we store it in memory then copy over to gpu memory as it could be reqired memory to be linear in nature
	idk.
*/
struct flue_prog {
	_8_u userdata[0x1000];
	/*
		primary memory storage
	*/
	_8_u *primary;
	/*
		gpu memory
	*/
	_8_u *secondary;
	_int_u ps, ss;

	struct flue_yard *binds;
	struct flue_yard *y;
	_64_u shader[SHADER_CSS];
};
/*
	longingated Pixelcell mash
*/
#define FLUE_LPCM 0x03
#define FLUE_ILPCM 0x04
#define FLUE_TOI 0x01
#define FLUE_LINE 0x05
struct flue_optext {
	_flue_float *buf;
	struct flue_colour c;
	_int_u n;
};

struct flu_plarg {
	void *p;
	_int_u len;
};

/*
	window, screen, etc
*/
#define FLUE_MAJ 0
# include "../vekas/maj.h"
struct flue_drawable {
	_32_u width, height;
	/*
		scan line length - width*4
	*/
	_32_u line_length;

	void *sf;
	void *ptr;
};

struct flue_maj {
	struct flue_drawable d;
	struct maj_patch *pt;
};

struct flue_scaf {
	void(*update)(void*, void*, void*);
	void(*dw_init)(void*,void*);
	void(*dw_deinit)(void*);
};
//plak/plankton
struct flue_op {
	_8_u op;

	/*
		how many of said operations
	*/
	_32_u n;
	_8_u bits;
	struct flue_optext *t;
};


#define SWAPOUTSHADER 0x01
typedef struct flue_canister {
	struct flue_texinfo tx_info;
	void *tex[16];
	_int_u ntx;
	_flue_float *m;
	struct flue_prog **pgm;


	struct flue_op *ops;
	_int_u n_ops;
	
	_8_u bits;
} *flue_canisterp;

struct flue_bisc {
	_int_u width, height;
	void *format;
};

struct flue_state {
	void *rtgs[8];
	_int_u ntgs;
	struct flue_prog *pgm[8];
	//current facilitated zbuffer
	void *zb;
	void *vbo[20];
	_int_u nvbo;


};

typedef struct flue_blob {
	// render targets
	struct flue_state *state;
	void *rtgs[8];
	struct flue_prog **pgm;
	struct flue_canister *can;
	_int_u n;
} *flue_blobp;

struct flue_fb {
	_16_u width, height;
};


struct flue_tm {
	void *tp[20];
	_int_u ntex;
	void *u_tp[20];
	_int_u u_ntex;
};

struct flue_viewport {
	//offsets
	_flu_float x, y;
	/*
		size

		W = x*width
		H = y*height

		where x and y are normalized floating point numbers
	*/
	_flu_float width, height;

	float scale[4];
	float translate[4];
};

struct flue_ctx {
	_8_u const_data[4096];
	_64_u states[STATE_SS*STATE_MAX];
	_int_u curst;
	struct flue_tm m;
	struct flue_viewport vp, *_vp;
	_int_u vp_offset;
	_int_u vp_n;

	struct flue_shader *shaders[4];

	_8_u bits;
	struct flue_fb fb;
	void *zb;
};
struct flue_devsh {
	_64_u *fct;
	int fd;
};

struct flue_dd;
struct flue_drv;
typedef struct flue_context {
	struct flue_ctx *priv;
	struct flue_devsh *dev;
	void *prv;

	struct flue_ctx *c[12];
	_int_u nc;

	struct flue_blob *b;
	struct flue_dd *d;
	struct ad_funcs *ad;

	struct flue_scaf *scaf;
	struct flue_dev *dv;
	_flu_float _m[16*8];
	_int_u m, mb;
	
	/*
		drawable for final render,window,screen,etc - fbdev!
	*/
	struct flue_drawable *draw;

	/*
		write final frame to its dest, if using indirect method,
		else will be handled diffrently
	*/
	void(*writeout)(void*,void*,void*);
	void(*share)(void*,_32_u);
	void(*update)(void*);
} *flue_contextp;

struct flue_bo {
	_32_u size;
	void *cpu_map;
};

struct flue_rb0 {
	/*
		offsets
	*/
	_32_u x, y;
};

struct flue_rb {
	_8_u userdata[0x200];
	struct flue_rb0 _0;
};

struct flue_fmtdesc {
	_int_u depth;
	_int_u id;

	void *dfmt;
	void *nfmt;
};

#define FLUE_NF_FLOAT		0
#define FLUE_NF_UINT		1
#define FLUE_NF_SRGB		2
#define FLUE_NF_UNORM		3
#define FLUE_DF_32_32_32_32 0
#define FLUE_DF_8_8_8_8 1
#define FLUE_DF_32 2

/*
	this is context controled/driven
*/
#define FLUE_DFMT(__wht) (flue_comm.dfmt[__wht].fmt+(FLUE_CTX->d->id*8))
#define FLUE_NFMT(__wht) (flue_comm.nfmt[__wht].fmt+(FLUE_CTX->d->id*8))

#define FLUE_RSVS (&FLUE_CTX->d->sh[0])
#define FLUE_RSPS (&FLUE_CTX->d->sh[1])
#define FLUE_RS_VERTEX 0
#define FLUE_RS_OTHER 1

/*
	this says this resource is used to fetch vertex data,
	this enabled indexing
*/
#define FLUE_RS_BUFFER_VERTEX 0
/*
	treatment as a
	standalone buffer nothing special 
*/
#define FLUE_RS_BUFFER 1
/*
	buffer data is accessed on a per primative basis 
*/
#define FLUE_RS_BUFFER_PRIM 2
struct flue_shinfo;
struct flue_resspec {
	/*
		in bytes
	*/
	_int_u stride;
	struct flue_fmtdesc fmt;
	struct flue_shinfo *sh;
	_64_u id;
	_64_u desc;
	_64_u bits;
};


struct flue_shinfo {
	_32_u user_data[32];
	_int_u ud_offset;
	_64_u reg;
};


struct flue_dv {
	int fd;
};

struct flue_drv {
	struct flue_dd *d;
	void(*prep)(void);
	void(*deinit)(void);
//	struct ad_funcs *ad;
	void *pad;
	char const *desc;
};


struct flue_dev {
	void *d;
	/*
		for yage shader
		mech = mechine
	*/
	struct ys_mech *m;
	/*
		how many people are using
	*/
	_int_u n_use;
	// driver ie loading driver dd is the real driver
	struct flue_drv *dv;
	_64_u dvid;
};

struct flue_dvblk {
	struct flue_dev dv[20];
	_int_u ndv;
};

enum {
	_FLU_SRGB
};
/*
	hardware gpu
*/
#define FLU_PRIO0 flue_comm.pdv[0]
/*
	software gpu
*/
#define FLU_PRIO1 flue_comm.pdv[1]
#include "../const.h"
struct flue_fmtblock{
	_64_u info;
	_64_u fmt[8*4];
};

struct flue_common {
	struct flue_drv drv_tab[FLUE_N_DRV];
	union {
		struct flue_context *ctx;
		_8_u data[GSC_SIZE];
	};
	// block of physical devices
	struct flue_dvblk pdv[2];
	void *fb;
	void(*auth)(void*,int,_32_u);
	void *conn;

	struct flue_fmtblock nfmt[8];
	struct flue_fmtblock dfmt[8];

	/*
		moved here to removed device pointer access
		ctx->d->shtab		= bad
		flue_comm.shtab		= good no pointers only shtab
	
		how should we do this? 
		option
		A) we have the user allocate the shader structure for each driver
			- in order to iterate over all existing shader structures we would need a pointer chain
			

		B) we have the user allocate a bank of shader structures and go from there.
			- to iterate over all existing just save the banks and go from there. 


	*/
	struct {
		_64_u *shtab;
	};
};
# include "dd.h"
# endif /*__flue__struc*/
