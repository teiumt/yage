#include "common.h"
#include "../m_alloc.h"
#include "../io.h"

flue_contextp _flue_ctx(struct flue_dev *dev) {
	flue_contextp ctx;
	ctx = (flue_contextp)m_alloc(sizeof(struct flue_context));

	printf("DRIVER: %p, %p.\n", dev, dev->dv);
	struct flue_drv *dv = dev->dv;

	dev->n_use++;
	ctx->priv = dv->d->ctx_new(dev->d);
	if (!ctx->priv) {
		printf("private context came back null.\n");
		return NULL;
	}
	ctx->d = dv->d;
	ctx->dv = dev;
	ctx->priv->m.ntex = 0;
	ctx->priv->m.u_ntex = 0;
	ctx->b = NULL;
	ctx->m = 0;
	ctx->mb = 0;
	ctx->priv->curst = 0;
	ctx->dev = dev->d;
	return ctx;
}
flue_contextp flue_ctx_new(void) {
	//perfured
	struct flue_dev *perf, *d = NULL;

// if no gpu then use software one
	if (!flue_comm.pdv[0].ndv)
		perf = flue_comm.pdv[1].dv;//software
	else
		perf = flue_comm.pdv[0].dv;//hardware
	_int_u i, b = 0;
	b = 0;
	for(;b != 2;b++) {
		i = 0;
		f_printf("scaning device list for block-%u, devcnt-%u.\n", b, flue_comm.pdv[b].ndv);
		for(;i != flue_comm.pdv[b].ndv;i++) {
			d = flue_comm.pdv[b].dv+i;
			if (d->n_use<perf->n_use)
				perf = d;
		}
	}

	if (!d) {
		printf("no device has been selected, non-detected wtf?.\n");
		return NULL;
	}

	
	return _flue_ctx(perf);
}

void flue_ctx_destroy(flue_contextp __ctx) {
	__ctx->d->ctx_destroy(__ctx->priv);
	m_free(__ctx);
}
