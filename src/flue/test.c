#include "common.h"
#include "shader.h"
#include "radeon/common.h"
void flue_test(void){
	struct flue_shader ps;
	struct flue_shader vs;

 flue_prime();
	flue_sweep();

	void *__ctx = flue_ctx_new();
	FLUE_setctx(__ctx);
//return;
	flue_shader_new(&ps);
	flue_shader_new(&vs);

	flue_shader_allot(&vs,"position",8,1,0,FLUE_USERSLOT0);
	flue_shader_allot(&ps,"texture",7,
		FLUE_ALLOT_SAMPLE,0,FLUE_USERSLOT1
	);
	flue_shader_compile(&ps,"ihc/test_ps.fsl",sizeof("ihc/test_ps.fsl"));
	flue_shader_compile(&vs,"ihc/test_vs.fsl",sizeof("ihc/test_vs.fsl"));
	flue_shader(&ps,0);
	flue_shader(&vs,1);
/*
void flue_sampler(struct flue_sampler *__info, _int_u __n, _64_u *__placement) {
  FLUE_CTX->d->sampler(FLUE_CTX->priv,__info,__n,__placement);
}

void flue_textures(void **__list, struct flue_shinfo *__info, _int_u __n, _64_u *__placement) {
*/
	flue_txdesc(FLUE_USERSLOT1,8,16);
	flue_rsdesc(FLUE_USERSLOT0,2,FLUE_RS_BUFFER_VERTEX);
	rd_shader_update(FLUE_CTX->priv);
//	flue_draw_array(NULL,0,0,0);
//	FLUE_CTX->d->translate_shader(FLUE_CTX->priv,&shader);

 // flue_shader_dump(&shader);
}
