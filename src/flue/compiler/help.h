/*
	how do you deal with vendor specific ops?

	as an example lets say we end up with a non-compliant GPU, now what?

	for another example: AMD uses buffer descriptors, so how do we deal with GPU that dont have that?.

	'
		ima_load_desc
		ima_image_load
	'

	this wouldent work for non-AMD in the case that non-AMD dosent have such things as descriptors.
	

	ANOTHER THING: AMD supports loading descriptors from memory. meaning you can BIND as many textures you want(could be no limit).
	and other GPUs may support only a limited set of them(16 or above that some what).

	MORE-EXAMPLES; cubemaps

	if you build a shader that makes use of cubemaps and someone with a GPU that has no support for that trys to run that shader it will fail.

	is there away to support both GPUS?

	# PIXEL
	'
		in vec4 look;  <= this is a pixel shader input that has to undergo interpolation
		in vec4 col;
		out vec4 colour;
		user_const damp_frac;
		cubemap mycube;

		vec4 altcol;
		altcol = texture(mycube,look);
		altcol = (altcol+col)*damp_frac;	
		colour = altcol;
	'

	if cubemaps are unavailable then the user has to look for a substitute.

	SO we conclude this by saying we are looking at features not vendors or GPUS.

	so how do we deal with features not being present?

	do we force the user to change the shader so something more appropriate that conforms to this GPU?.
	

	# COMPLEX SHADERS ISSUE

	we have only have issues with features but these features dont impact algorithm or the more complient section of code.

	'
		# - compliant
		in vec4 look;
		in vec4 col;
		out vec4 colour;
		user_const damp_frac;
	

		# - non-compliant
		cubemap mycube;

		vec4 altcol;
		altcol = texture(mycube,look);
		

		# - compliant
		altcol = (altcol+col)*damp_frac;	
		colour = altcol;	
	'

	see here the non-compliant piece is a INPUT. so in sense the non-compliant would be swaped out for somthing more compliant.


	EXAMPLE:

#	cubemap.fsl
	'
		void func(){
			cubemap mycube;
			in vec4 look;
			out vec4 altcol;
			altcol = texture(mycube,look);
		}
	'
# default.fsl
	'
		void func(){
			in vec4 look;
			out vec4 altcol;
			altcol = 1.0;
		}
	'
# program.fsl
	'
		# - compliant
		in vec4 look;
		in vec4 col;
		out vec4 colour;
		user_const damp_frac;
	
		func();
	
		# - compliant
		altcol = (altcol+col)*damp_frac;	
		colour = altcol;
	'

	to conclude this, its non-compliant is going to exist sometimes.
	to back this up you can look at CPUS. the linux kernal has a 'arch' directory
	that contains architecture specific code.

	another example; if you try to use a instruction that only exists for some CPUs.
	sometimes specific architectures have there own special things.

	the most simplest way of doing things would be checking if the shader is fulling compliant and
	if its not make one that is.

	the best way would be to have some external program find compliants.


	like the above example{cubemap.fsl,default.fsl,program.fsl}

	meaning each shader can have its of flavour/arrangement.
	{
		{program.fsl+default.fsl} = compliant with AMD
		{program.fsl+cubemap.fsl} = compliant with nvidia
	} my_program;


	CONCLUSION: compliants only means in some regards and there will be divergence between vendors and GPUs.
	this means feature-specific shaders are a thing.


	for example; compliants would be thrown out the window if your shader has to run on a device that had no vertex shader.
	this would affect the whole graphics pipeline and means that the program rendering would have to take detours.


	for example; AMD supports many textures, so if we have a shader that inputs 32+ textures then we would for devices that supports such a thing do that.
	if not supported then we would have to use diffrent shader and do multiple passes.
	- A) changes the program code
	- B) changes the shader code


	compliant = rudimentary things(add,sub,mul,div, etc)
	non-compliant = device enhancements


	NOTE: there is no such thing as compliants its just coincidence.

	EXAMPLE; image processing.

	for this lets say your constructing a games console. the CPU is permanent however GPU can be upgraded.
	you start with an image. you then tell the GPU by sending a command to execute this code on every pixel of the image.

	'
		in vec2 position;
		image haziness_map;
		image input_image;
		image output_image;
		vec4 hazval;
		vec4 pixel
		pixel = image_load(input_image,position);
		hazval = image_load(haziness_map,position);
		image_store(position) = pixel*hazval;
	'

	so now lets say we upgrade the GPU.
	so now the GPU supports a microcode version of this same shader.


	now are shader looks like this.
	'
		in vec2 position;
		image haziness_map;
		image input_image;
		image output_image;

		image2_mul(output_image,input_image,haziness_map);
	'

	you can go about this two ways. either you implement image2_mul in the compiler to generate the code needed to do this or force the user to use a compliant shader.


	okay so how about this;

	'
		# 0.0-1.0
		in vec2 position;
		image input_image;
		out vec4 output;

		vec4 pixel;
		pixel = image_load(input_image,position);
		pixel = pixel*position; => pixel*(float)(position.x ? position.x)


		output = pixel;
	'

	okay cool now lets fuck every thing up and say the GPU dosent support 'position'
	and can only tweek pixels.

	'
		image fixup;
		image input_image;
		out vec4 output;
		vec4 pixel;
		vec4 frac;
		frac = image_load(fixup);
		pixel = image_load(input_image);
		pixel = pixel*frac;

		output = pixel;
	'

	do with this GPU getting the position is prohibited.
	so are fixup is to use a image_load() to get the position.

	okay so now how would you go about dealing with this?
	are first gen GPU dosent support 'position' now 
	we upgrade are GPU.


	so not only does this force the HOST to change its tactics it also forces 
	us to change the shader in its entirety.


	for are second gen GPU the HOST no longer needs to generate this 'fixup' map.
	so.
	'
		if('position' not supported){
			generate_fixup();
		}
	'

	also this.

	'
		if('position' not supported){
			load_shader('shader_gen1.fsl');
		}else{
			load_shader('shader_gen2.fsl');
		}
	'

	and even more. we would take this as an algorithm that simply provides a gradiant to the pixel using 'position'.
	so we could just implement it in the compiler.

	'
		image input_image;
		out vec4 output;
		output = algorithm(input_image);
	'

	then in the compiler we could adjust the code to meet compliants.
	my question is how far do you go with this? how far should it be taken?
	what you will end up with is the compiler having every algorithm implemented within it.


	or another situation where the GPU dosent support the 'image_(load/store)' instruction.
	those instruction can be replaced with buffer_(load/store) instructions.

	so in the case shader supports 'IJ' coords

	'
		in vec2 position;
		const_vec2 hw;

		int offset;
		offset = position.x+(position.y*hw.x);
		vec4 pixel;

		pixel = buffer_loadvec4(offset);
		...
		; alter pixel
		...
		buffer_storevec4(offset,pixel);
	
	'

	so in the upgrade from no 'image' instruction to image instructions.
	would you just implement a fake 'image' instruction at compiler level.
	then generate equivalent code using the buffer instructions?

	# THIS is the IMPASS!!

	you can either force the user to use a diffrent and more compliant shader or 
	implement a image function at compiler level.


	so with all that the 'image' function is more of a library then anything else.

	[buffer + position]							= buffer instructions exists and position vec supported
	[buffer - position + offset]		= buffer instructions exists and position vec is not supported but offset is a pixel shader input
	[image + position]							= image instructions exists and position is passable.
	[image - position]							= image instructions exists but position cant be specified(position is what pixel the shader was executed on).

	so the question is how to we converge both shaders?. should they even be converged to begin with?

	in the end to maxamize performance we would build the shaders around the GPU, meaning that shader is specific for this GPU.
	for example;

	for this device algorithmA includes non-compliant instructions.
	while algorithmB does as no performance optimization can be done.

	'DEVICE(0)
		vec4 j;
		vec4 pixel;
		j = algorithmA(pixel);
		pixel = algorithmB(j,pixel);
	'

	for this device algorithmB includes non-compliant instructions.
	while algorithmA does as no performance optimization can be done.

	'DEVICE(1)
		vec4 j;
		vec4 pixel;
		j = algorithmA(pixel);
		pixel = algorithmB(j,pixel);
	'

	the non-compliant algorithms make use of faster instructions.
	however these are user algorithms. they are not built into the compiler.
	so are only choices are to build compliant code and not to care about 
	the performance.

	are best choice is to think about these constraints as enhancements.
	so in this case theres no such thing as a vendor and we only look at instructions.

	what im getting at is AMD,INTEL,NIVDIA can come under the umbrella of a single vendor.
	where the only diffrence is either instructions exist or do not.

	so in this case we would be looking to do somthing like this.

	'algorithm
		vec4 pixel;
		vec4 result;
#ifdef __faster
		;non-compliant
		result = square_root_faster_less_precise(pixel);
#else
		result = square_root(pixel);
#endif
	'

	and we dont look at it as

	'algorithm
		vec4 pixel;
		vec4 result;
#ifdef __amd_gpu
		;non-compliant
		result = square_root_faster_less_precise(pixel);
#else
		result = square_root(pixel);
#endif
	'

	only issue about this is __faster might be defined but also 'square_root'
	meaning square_root = square_root_faster_less_precise;
	as only one square_root instruction exists.
	or both could exists.

	either way its user directed as they decide if they want low resolution results or not.

	so we can assume a variety of square_root functions could exists. but diffrent GPU may or may not have specific ones.
	functions
	square_root_low_resolution
	square_root_moderate_resolution
	square_root_high_resolution

EXAMPLE
	GPU(r0) = {square_root_low_resolution+square_root_moderate_resolution}
	GPU(r1) = {square_root_high_resolution+square_root_moderate_resolution}
	GPU(r2) = {square_root_low_resolution+square_root_moderate_resolution+square_root_high_resolution}

## this is easy to deal with realy.

	however in the end one GPU may have a better tool set for the algorithm.


	however how far will it be taken?

	maths librarys take it down to assembly and arch specific.
*/
