#ifndef __bitword__h
#define __bitword__h
#include "../../../y_int.h"


#define BITWORD _32_u
#define BITWORD_WORDS(x) (((x)+31)>>5)
#define BITWORD_SET(x,b) x[(b)>>5] |= 1<<((b)&31)
#define BITWORD_CLEAR(x,b) x[(b)>>5] &= ~(1<<((b)&31))
#define BITWORD_TEST(x,b) ((x[(b)>>5]&(1<<((b)&31))) != 0)
#endif /*__bitword__h*/
