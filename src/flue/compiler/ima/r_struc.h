#ifndef __r__struc__h
#define __r__struc__h
#include "../../../y_int.h"
/*
	this structure is a part of the radeon driver.
*/

struct r_shader{
  //param exports
  _16_u param_cnt;
	_16_u mrt_cnt;
	_16_u pos_cnt; 
};

#define _r_exp_col 0
#define _r_exp_vert 1
#define _r_exp_par 2
#define _r_buf_desc 0
#define _r_buf_spec 1
#define _r_img_nosample 0
#define _r_img_sample 1
extern struct r_desc *r_desc_table;
/*
  no desc fetch and val replaces desc
*/
#define _r_image_ndf 1
struct r_desc{
  //place within desciprtor table multable of 16-dwords
  _16_u val;
  _16_u sampler;
  _int_u type;
  _16_u atb;
  _64_u bits;
  _64_u buf_bits;
  _64_u offset;
};


void r_init_shader(struct r_shader*);
extern struct r_shader *rd_sh;

#endif/*__r__struc__h*/
