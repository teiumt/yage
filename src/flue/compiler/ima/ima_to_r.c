#include "ima.h"
#include "radeon.h"
#include "../../../assert.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../strf.h"
#include "../../../ihc/ihc.h"
#include "../../../m_alloc.h"

#define TMP0 0
#define TMP1 1
#define TMP2 2
#define TMP3 3

/*
	NOTE: this step is the last intermediate step;

	any output following runs by device format.
*/
void rd_instr_exit(struct ima_instr*);
struct _r_opdetails{
	void(*f)(struct ima_instr*);
};
void rd_instr_st(struct ima_instr*);
void rd_cvt_pkrtz_f16_f32(struct ima_instr*);
void rd_instr_export(struct ima_instr*);
static struct _r_opdetails _opd[] = {
	{//_ima_op_rcp32f
		rd_instr_vop1	
	},
	{//_ima_op_mulu16
		rd_instr_vop2
	},
	{//_ima_op_add32u
		rd_instr_vop2
	},
	{//_ima_op_mov
		rd_instr_vop1
	},
	{//_ima_op_fadd
		rd_instr_vop2
	},
	{//_ima_op_fsub
		rd_instr_vop2
	},
	{//_ima_op_fmul
		rd_instr_vop2
	},
	{//_ima_op_fmac
		rd_instr_vop2
	},
	{//_ima_op_cvt_i32f32
		NULL
	},
	{//_ima_op_cvt_pkrtz_f16_f32
		rd_cvt_pkrtz_f16_f32
	},
	{//_ima_op_rsq_f32
		rd_instr_vop1
	},
	{//_ima_op_max32f
		rd_instr_vop2
	},
	{//_ima_op_min32f
		rd_instr_vop2
	},
	{//_ima_op_vecmov
		rd_instr_vecmov
	},
	{//_ima_op_export
		rd_instr_export
	},
	{//_ima_op_load
		rd_instr_load
	},
	{//_ima_op_store
		rd_instr_store
	},
	{//_ima_op_load16
		rd_instr_load16
	},
	{//_ima_op_image_load
		rd_instr_image_load
	},
	{//_ima_op_image_store
		rd_instr_image_store
	},
	{//_ima_op_image_sample
		NULL
	},
	{//_ima_op_tex
		rd_instr_tex
	},
	{//_ima_op_param
		rd_instr_param
	},
	{//_ima_op_endprog
		rd_instr_exit
	},
	{//_ima_op_st
		rd_instr_st
	}
};

_16_u static direct_opmap[] = {
	[_ima_op_rcp32f] = _r_op_rcp32f, 
	[_ima_op_mulu16] = _r_op_mulu16,
	[_ima_op_add32u] = _r_op_add32u,
	[_ima_op_mov] = _r_op_mov,
	[_ima_op_fadd] = _r_op_fadd,
	[_ima_op_fsub] = _r_op_fsub,
	[_ima_op_fmul] = _r_op_fmul,
	[_ima_op_fmac] = _r_op_fmac,
	[_ima_op_cvt_i32f32] = _r_op_cvt_i32f32, 
	[_ima_op_cvt_pkrtz_f16_f32] = _r_op_cvt_pkrtz_f16_f32,
	[_ima_op_rsq_f32] = _r_op_rsq_f32,
	[_ima_op_max32f] = _r_op_max32f,
	[_ima_op_min32f] = _r_op_min32f,
	[_ima_op_vecmov] = -1,
	[_ima_op_export] = -1,
	[_ima_op_load] = -1,
	[_ima_op_store] = -1,
	[_ima_op_load16] = -1,
	[_ima_op_image_load] = -1,
	[_ima_op_image_store] = -1,
	[_ima_op_image_sample] = -1,
	[_ima_op_tex] = -1,
	[_ima_op_param] = -1,
	[_ima_op_endprog] =  -1
};
static struct r_desc dummy_desc = {-1};
struct r_desc *r_desc_table = NULL;
static struct r_desc *getdesc(_int_u __id){
	if(!r_desc_table){
		return &dummy_desc;
	}
	return r_desc_table+__id;
}
static _64_u __reg[4];
static _64_u exg_reg[4] = {0,0,0,0};
static _64_u table[] ={
	[_ima_instr_em_reg]						_R_TYP_REG,
	[_ima_instr_em_const_float]		_R_TYP_FLOAT,
	[_ima_instr_em_const_int]			_R_TYP_IMM,
	[_ima_instr_em_const_str]			_R_TYP_STR
};

//bridge
struct _reginfo{
	_16_u info;
};
static struct _reginfo reginfo[512];

void static _init(void){
	_int_u i;
	i = 0;
	for(;i != 101;i++){
		reginfo[i].info = _R_SGPR;
	}

	for(;i != 357;i++){
		reginfo[i].info = _R_VGPR;
	}
}

void rd_instr_st(struct ima_instr *__i){
	exg_reg[1] = 1;
	__reg[__i->v0] = 0;
	__reg[__i->v1] = 1;
}

/*
	PLEASE NOTE:

	ys can be seen as the final produce but just in a text format to make debug and testing more lenient.
*/
#define REG(__x) ima_regmap[__x]
#include "r_struc.h"
void static _vop3a(_16_u __op,_16_u __r0,_16_u __r1,_16_u __r2){
	struct r_instr *i;
	i = r_instr_new();
	i->op = __op;
	i->r_info[0] = _R_TYP_REG;
	i->r_info[1] = _R_TYP_REG;
	i->r_info[2] = _R_TYP_REG;

	i->r_reg[0] = __r0;
	i->r_reg[1] = __r1;
	i->r_reg[2] = __r2;
}

void rd_cvt_pkrtz_f16_f32(struct ima_instr *__i){
	_vop3a(_r_op_cvt_pkrtz_f16_f32,__i->dst,__i->l->val,__i->r->val);
}

void rd_instr_export(struct ima_instr *__i){
	struct r_instr *i;
	struct r_desc *d;
	d = r_desc_table+__i->exp;
	_16_u val = d->val;
	_16_u v0 = __i->v0;
	_64_u info = 0;
	if(d->type == _r_exp_col){
		_vop3a(_r_op_cvt_pkrtz_f16_f32,TMP0,v0,v0+1);
		_vop3a(_r_op_cvt_pkrtz_f16_f32,TMP1,v0+2,v0+3);
		info |= 1|(3<<1);
		val |= R_REG_STAMP(_R_MRT);
	}else{
		info |= 0xf<<1;
		if(d->type == _r_exp_vert){
			val |= R_REG_STAMP(_R_POS);
		}else{
			val |= R_REG_STAMP(_R_PAR);
		}
	}
	
	i = r_instr_new();
	_rstrc.last_exports[R_REG_GSTMP(val)] = i;
	i->info = info;
	i->op = _r_op_export;
	i->r_info[0] = _R_TYP_PHYREG;
	i->r_reg[0] = val;
	if(d->type == _r_exp_col){
		i->r_info[1] = _R_TYP_REG;
		i->r_info[2] = _R_TYP_REG;
		i->r_reg[1] = TMP0;
		i->r_reg[2] = TMP1;
	}else{
		i->r_info[1] = _R_TYP_REG;
		i->r_info[2] = _R_TYP_REG;
		i->r_info[3] = _R_TYP_REG;
		i->r_info[4] = _R_TYP_REG;
		i->r_reg[1] = v0;
		i->r_reg[2] = v0+1;
		i->r_reg[3] = v0+2;
		i->r_reg[4] = v0+3;
	}
	r_sign_export(i);
}
void static _movrinstr(_16_u __l,_16_u __r){
	struct r_instr *i;
	i = r_instr_new();
	i->op = _r_op_mov;
	i->r_info[0] = _R_TYP_REG;
	i->r_info[1] = _R_TYP_REG;
	i->r_reg[0] = __l;
	i->r_reg[1] = __r;
}

void _rd_instr_vop1(struct ima_instr *__i, struct ima_instr_em *__l, struct ima_instr_em *__r) {
	struct r_instr *i;
	i = r_instr_new();

	i->op = direct_opmap[__i->op];
	i->r_info[0] = _R_TYP_REG;
	i->r_info[1] = table[__r->type];
	i->r_reg[0] = __l->val;
	i->r_reg[1] = __r->val;
}

void _instr_mubuf(_16_u __op,_32_u __offset,_32_u __lval,_32_u __desc){
	struct r_instr *i;
	i = r_instr_new();
	i->op = __op;
	i->r_info[0] = _R_TYP_REG;
	i->r_reg[0] = __lval;
	i->desc = __desc;
	i->offset = __offset;
}

void rd_instr_load16(struct ima_instr *__i){
	_instr_mubuf(_r_op_load16,__i->offset,__i->l->val,__i->desc);
	_instr_mubuf(_r_op_load16,__i->offset+16,__i->l->val+4,__i->desc);
	_instr_mubuf(_r_op_load16,__i->offset+32,__i->l->val+8,__i->desc);
	_instr_mubuf(_r_op_load16,__i->offset+48,__i->l->val+12,__i->desc);
}

void static __setreg0(struct r_instr *__i,_64_u __val,_64_u __idx){
	__i->r_info[__idx] = _R_TYP_REG;
	__i->r_reg[__idx] = __val;
}
void static __setreg1(struct r_instr *__i,_64_u __val,_64_u __idx){
	__i->r_info[__idx] = _R_TYP_PHYREG;
	__i->r_reg[__idx] = __reg[__val];
}


static void(*__setreg[])(struct r_instr*,_64_u,_64_u) = {
	__setreg0,__setreg1
};

void static _setreg(struct r_instr *__i,_64_u __val,_64_u __idx){
	_int_u i;
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~, %u\n",__val>>31);
	assert((__val>>31)<4);
	i = exg_reg[__val>>31];
	assert(i<2);
	__setreg[i](__i,__val&~__ima_HW,__idx);
}
/*
	VOP2 SRC/SRC/DST
*/
void rd_instr_vop2(struct ima_instr *__i) {
	printf("R_VOP2: %u, %u.\n",__i->l->val,__i->r->val);
	struct r_instr *i;
	i = r_instr_new();
	i->op = direct_opmap[__i->op];
	i->r_info[0] = table[__i->l->type];
	i->r_reg[0] = __i->l->val;
	_setreg(i,__i->r->val,1);
	i->r_info[2] = _R_TYP_REG;
	i->r_reg[2] = __i->dest;
}

void rd_instr_vop1(struct ima_instr *__in) {
	if(__in->info == _ima_op_mov_4){
		rd_instr_vop1d(__in);
	}else{
		_rd_instr_vop1(__in,__in->l,__in->r);
	}
}

void rd_instr_vop3a(struct ima_instr *__i) {
	struct r_instr *i;
	i = r_instr_new();
	i->op = direct_opmap[__i->op];
	i->r_info[0] = _R_TYP_REG;
	i->r_info[1] = _R_TYP_REG;
	i->r_info[2] = _R_TYP_REG;

	i->r_reg[0] = __i->dst;
	i->r_reg[1] = __i->l->val;
	i->r_reg[2] = __i->r->val;
}

void rd_instr_vop1d(struct ima_instr *__in) {
	_rd_instr_vop1(__in,__in->l,__in->r);
	_rd_instr_vop1(__in,__in->l+1,__in->r);
	_rd_instr_vop1(__in,__in->l+2,__in->r);
	_rd_instr_vop1(__in,__in->l+3,__in->r);
}

void static fetch_decriptor_from_table(_16_u __d,_16_u __offset,_16_u __dst){
	struct r_instr *i;
	i = r_instr_new();
	i->op = _r_op_sldw;
	i->r_info[0] = _R_TYP_IMM;
	i->r_info[1] = _R_TYP_PHYREG;
	i->r_info[2] = _R_TYP_PHYREG;
	i->r_reg[0] = __offset;
	i->r_reg[1] = __dst|R_REG_STAMP(_R_SGPR);
	i->r_reg[2] = __d|R_REG_STAMP(_R_SGPR);
}


void static _img(_16_u __op,struct ima_instr *__i,_32_u __desc,_32_u __sampler){
	struct r_instr *i;
	i = r_instr_new();
	i->op = __op;
	i->r_info[0] = _R_TYP_REG;
	i->r_info[1] = _R_TYP_REG;
	i->r_info[2] = _R_TYP_IMM;

	i->r_reg[0] = __i->l->val;
	i->r_reg[1] = __i->r->val;
	i->r_reg[2] = __desc;
	i->r_reg[3] = __sampler;
}

void rd_instr_image_store(struct ima_instr *__i) {
	struct r_desc *d = getdesc(__i->desc);
	_int_u desc;
	if(d->bits&_r_image_ndf){
		desc = d->val;
	}else{
		desc = 12;
		fetch_decriptor_from_table(0,d->val*16,12);
	}
	_img(_r_op_imgstore,__i,desc,16);
}

void static _cast2_int(_16_u __v0,_16_u __v1){
	struct r_instr *i;
	i = r_instr_new();
	i->op = _r_op_cvt_i32f32;
	i->r_info[0] = _R_TYP_REG;
	i->r_info[1] = _R_TYP_REG;
	i->r_reg[0] = __v0;
	i->r_reg[1] = __v1;
}

void rd_instr_image_load(struct ima_instr *__i) {
	struct r_desc *d = getdesc(__i->desc);
	if(d->type == _r_img_nosample){
		fetch_decriptor_from_table(0,d->val*16,12);
		_cast2_int(__i->r->val,__i->r->val);
		_cast2_int(__i->r->val+1,__i->r->val+1);
		_img(_r_op_imgload,__i,12,16);
	}else{
		fetch_decriptor_from_table(0,d->val*16,12);
		fetch_decriptor_from_table(0,d->sampler*16,16);
		_img(_r_op_imgsample,__i,12,16);
	}
}

void rd_instr_vecmov(struct ima_instr *__i){
	_movrinstr(__i->l->val,__i->r->val);
	_movrinstr(__i->l->val+1,__i->r->val+1);
	_movrinstr(__i->l->val+2,__i->r->val+2);
	if(__i->comp&8){
		_movrinstr(__i->l->val+3,__i->r->val+3);
	}else{
		struct r_instr *i;
		i = r_instr_new();
		i->op = _r_op_mov;
		i->r_info[0] = _R_TYP_REG;
		i->r_info[1] = _R_TYP_FLOAT;
		i->r_reg[0] = __i->l->val+3;
		*(double*)(i->r_reg+1) = 0;
	}
}

void rd_instr_emit(struct ima_instr *__i){
	struct _r_opdetails *d;
	d = _opd+__i->op;
	if(!d->f){
		printf("unreachable.\n");
		return;
	}
	printf(">>> {%u}.\n",__i->op);
	d->f(__i);
}

void rd_instr_exit(struct ima_instr *__i){
	struct r_instr *i;
	i = r_instr_new();
	i->op = _r_op_endprog;
}
void rdump(void);
void r_consume(void){
	r_init();
	printf("consuming.\n");
	struct ima_instr *i;
	i = ima_instr_head;
	while(i != NULL){
		rd_instr_emit(i);
		i = i->next;
	}
	rdump();
}

void static interp(_16_u __op,_16_u __comp,_16_u __atb,_16_u __v0,_16_u __v1){
	struct r_instr *i;
	i = r_instr_new();
	i->op = __op;
	i->comp = __comp;
	i->atb = __atb;
	i->r_info[0] = _R_TYP_REG;
	i->r_reg[0] = __v0;
	i->r_info[1] = _R_TYP_PHYREG;
	i->r_reg[1] = __v1;
}

void rd_instr_tex(struct ima_instr *__i){
}

static struct r_instr* bufinstr(_16_u __op,_16_u __r,_32_u __offset,_32_u __desc,_16_u __fmt){
	struct r_instr *i;
	i = r_instr_new();
	i->op = __op;
	i->r_info[0] = _R_TYP_REG;
	i->r_reg[0] = __r;
	i->offset = __offset;
	i->desc = __desc;
	i->fmt = __fmt;
	i->sub = 1;
	return i;
}

static _16_u buf_locs[] = {
	[_r_buf_desc] = 0,
	[_r_buf_spec] = 4
};

void rd_instr_load(struct ima_instr *__i){
	struct r_desc *d = getdesc(__i->desc);
	_16_u offset;
	if(d->type == _r_buf_spec){
		offset = 0;
	}else{
		offset = d->val*16;
	}

	fetch_decriptor_from_table(buf_locs[d->type],offset,12);	
	struct r_instr *i;
	i = bufinstr(_r_op_load,__i->l->val,__i->offset,12,__i->fmt);
	if(d->type == _r_buf_spec){
		i->sub = 0;
	}
	i->bits = d->buf_bits;
}
void rd_instr_store(struct ima_instr *__i){
	struct r_desc *d = getdesc(__i->desc);
	fetch_decriptor_from_table(buf_locs[d->type],d->val*16,12);
	struct r_instr *i;
	i = bufinstr(_r_op_store,__i->l->val,__i->offset,12,__i->fmt);
	i->bits = d->buf_bits;
}

void rd_instr_param(struct ima_instr *__i){
	struct r_instr *i;
	struct r_desc *d = getdesc(__i->desc);
	i = r_instr_new();
	i->op = _r_op_mov_s;
	i->r_info[0] = _R_TYP_PHYREG;
	i->r_info[1] = _R_TYP_PHYREG;
	i->r_reg[0] = 0|R_REG_STAMP(_R_M);
	i->r_reg[1] = 8|R_REG_STAMP(_R_SGPR);
	_16_u atb;
	atb = d->val;
	if(__i->comp&1){
	interp(_r_op_intrp_p1f32,_r_comp_selx,atb,__i->v0,0);
	interp(_r_op_intrp_p2f32,_r_comp_selx,atb,__i->v0,1);
	}
	if(__i->comp&2){
	interp(_r_op_intrp_p1f32,_r_comp_sely,atb,__i->v0+1,0);
	interp(_r_op_intrp_p2f32,_r_comp_sely,atb,__i->v0+1,1);
	}
	if(__i->comp&4){
	interp(_r_op_intrp_p1f32,_r_comp_selz,atb,__i->v0+2,0);
	interp(_r_op_intrp_p2f32,_r_comp_selz,atb,__i->v0+2,1);
	}
	if(__i->comp&8){
	interp(_r_op_intrp_p1f32,_r_comp_selw,atb,__i->v0+3,0);
	interp(_r_op_intrp_p2f32,_r_comp_selw,atb,__i->v0+3,1);
	}
}
