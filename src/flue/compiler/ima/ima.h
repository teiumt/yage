#ifndef __ima__h
#define __ima__h
#include "../../../y_int.h"
#include "../../../ihc/fsb.h"
#include "../../common.h"
#include "../../radeon/common.h"
#include "../../../ihc/struc.h"
#include "../hg/hg.h"
/* component selection

	GLSL - in glsl component selection like this.
	'
		vec4 test;
		test.xy = test.zw;
	'
	works by using swizzle.

	#ir_swizzle
	ASSIGNMENT{
		LHS: SWIZZLE
		RHS: SWIZZLE
	}

	- this then produces a 
	#nir_swizzle
	nir_alu_src alu_src;
	alu_src->swizzle[x] =
	alu_src->swizzle[y] =
	alu_src->swizzle[z] =
	alu_src->swizzle[w] =

	then in the drivers;

	- so swizzle is not apart of the real source but apart of instruction

	ntq_get_alu_src(){
		unsigned chan = #
		nir_alu_src *src;
		struct qreg r;
		r = ntq_get_src(#,(nir_src)src->src,src->swizzle[chan]);
	}
*/


/*
	IMA_REGISTERS:

	registers are arranged into blocks of 16*32

	single access to these registers are possibe.


	REGISTER-TYPES:
		radeon driver has two flavours of registers
		SGPRS, and VGPRS.

	what happens if the instruction sets the register flavour not the user.
	EXAMPLE

	for example, if i want to read somthing like the vertexID

	lets say its stored in SGPRS.

	so 
	ima_load_vertexid
		-> r_load_vertexid{
			reginfo[dstIDX].flavour = SGPR;
		}
	this means any instructions that use the vertex ID will be affected by this.
*/

/*
	what should and should not exist.
	
# this function should not exist.
	rd_intr_load_sampler_desc();

	just like registers we dont define registers, example.

	rd_intr_register_alloc();

	each one of these are the same.
	at lower levels we allocate and dealloc registers.
	the same goes for descriptors, there first use is there allocation.

	all this can be taken are of in the backend.
	for example somthing like this would be disgraceful.
	'
		image my_image = {
			.format = RGBA_32F
			.width = 512
			.height = 512
			.address_start = const_address0
		};
	'

	however in some cases the user may or may not want to define some sort of 'interpretation'.
	for example post processing of pixel or read type or offset(xy)(relocating axis).

	NOTE:
	 ima could have non-complicit instructions.

	ISSUE:
		rd_intr_load_sampler_desc(); is like loading internal shader things. for example VERTEXID.
		so you going have a instruction called 'load_vertex_id' and not 'load_descriptor'?

		i realy dont understand these things they make zero sense to me(im not jokeing).

		i would go about doing this like this.

		table[]={
			[0] = VERTEX_ID
			[1] = DESCRIPTOR 
		}

		load_somthing(INDEX);
	
		or i would just state in the instruction that we want to pull the vertexID

		instr mov;
		mov.dst[0] = REG;
		mov.src[0] = VERTEX_ID;

		but you can make the case that the user may want to use the VertexID and thats 
		what makes it diffrent to a descriptor where it cant be influenced by shader code.

	okay so we have a basis now.

	having its own instruction means that the value given can be subjected to change or altering <= may not be shared.

	things like descriptors are constant and are shared.


*/

/*
	deciding register assignment.

	many compilers just run though every instruction collecting registers as they go and building a liveness chart.

	sound great however going though every instruction sounds stupid.
	i mean what happens if we have 100+ instructions that dont touch a register?

	and another thing is control over this.

	for an example

	void emit_somthing(){
		struct reg = alloc_reg();

		store_int(&reg,21299);
		
		emit_ins();
		emit_ins();
		emit_ins();
		emit_ins();
		...

	}

	here you expect things.

	for example the register we allocated exists till the end of the emit_somthing.
	why should register allocation be controled on a per instruction basis?

another given example:
	void emit_somethingelse(){
		...
	}

	void emit_somthing(){
		struct reg = alloc_reg();
		store_int(&reg,21299);

		emit_ins();
		emit_ins();
		...

		emit_somethingelse();

		emit_ins();
		emit_ins();
		...
	}

	here we face this issue.
*/
/*
	NOTE:
	when manufacturing output it should go though preprocessing.

	like this.

	- in terms on memory no stated type/ typesless move/ flavourless move
	void preprocess_instruction_move64bit(){
		if("device only supports 32bit"){
			- do two 32bit move operations
		}else{
			- do single 64-bit move
		}
	}


	also NOTE: this process should take place on the final stage.
	meaning from IMA-> DEVICE LANGUAGE

	what im going for is impersonation.
	if an instruction that higher level tryes to access dosent exist then a substitute will be put in place.
	higher levels abide by this rule as well, so if the device is missing key things then diffrent instructions will be used
	so the same functionality is achieved.
*/

/*
  {
    {'3','2','f','4','\0'}
  },
  {
    {'3','2','u','1','\0'}
  }
};
struct ima_expl ima_ys_yfmt[64] = {
  {
    {'x','y','z','w','\0'}
  },
  {
    {'x','\0'}
  }


*/

#define _ima_32f4				0
#define _ima_32u1				1
#define _ima_xyzw				0
#define _ima_x					1
#define _ima_imm 0
#define _ima_vec 1

#define _ima_const 2
#define _ima_exp 2
#define _ima_buf 3
#define _ima_mat 4
#define _ima_float 5
#define _ima_other 6
#define _ima_intrp 9
#define _ima_vconst 10
/*
	and image/texture routine but has a param of vec ie not an shader input
*/
#define _ima_imgv	11
#define _ima_pos 0
#define _ima_mrt 1
#define _ima_z 2
#define _ima_col 3
#define _ima_img 12
#define _ima_reg 5

#define _ima_rel 7
#define _ima_param 7
//left
#define __ima_fsv0   0
#define __ima_gv0    1

//right hand side
#define __ima_imm	0
//func spec var
#define __ima_fsv	1
//global var
#define __ima_gv	2 
#define _ima_matindex (1<<8)
/*
	what is a register?
	a register is a 32-bit word

	how bunches of registers work:

	here we world with registers by 32-bit words.
	if more then a 32-bit word is used. then the
	allocation method changes.

EXAMPLE:
	void mov(int reg,int mask,float val){
		regcode[reg].code++ = _op_store|mask;
		
		reginfo[reg] |= mask;
		...
	}


	void emit_somthing(){
		struct rd_reg *src;
	
		mov(src->rval,1<<0,1.0);
		mov(src->rval,1<<1,1.0);

	}

	void reg_load_real_values(){
		struct code *c;
		while(c ! = NULL){
			if(c->op == _op_store){
				// if register is being used then its evicted/reallocated
				c->val = reg_alloc(c->reg,c->index);
			}
			c = c->next;
		}
	}

*/
struct rd_reg {
	void *a;
	_8_s alloc;
	_int_u rval;
	_int_u last;
	void *priv;
	struct rd_reg *next;
};

struct ima_instr;
/*
  tells us that the register group is made of 16 32-bit registers
*/
#define IMA_REG_GROUP_OF_16_32  (1<<15)
//extern _16_u ima_regg[512];
/*
	maps registers to hardware ones
*/
//extern _64_u ima_regmap[512];
struct ima_il {
/*
	dynamic storage allocation
*/
	void *alloc[16];
	_int_u nalloc;
};
//Q
struct ima_vs {
	_int_u size;
};

struct ima_w {
	_64_u offset;
	_int_u size;
};

struct ima_dwel {
	struct ima_w *vars;
	_int_u nvar;
};

#define _ima_vec_comp_default (1<<4)|(2<<8)|(3<<12)|(4<<16)

//explicit
/*
	TODO:
		assign and arth 

		_assign_vec 
		_arth_vec

		like _ima_vec but indpendent from eatch other
*/

/*
	okay i want to make somthing clear.

LHS		  RHS
	X = Y;
		.
		. => operator

for arithmetic
	mat O vec = op_matvec
	mat O float = op_matfloat
	vec O float = op vec_float

	as you see these functions have there own function liked to them.
	the issue comes for assignments.

	here we are just dealing with registers. so this means all we need is a copy function.

	mat O mat = op_copy
	vec O vec = op_copy
	float O float = op_copy


	and the issue in FSL you dont assume what operator is present.
	so we dont know if its a ASSIGNMENT or ARITHMETIC.

	space ISSUE:

	the way we do this is we do a lookup.
	lhs_type = 1
	rhs_type = 2

	type_idx = lhs_type|(rhs_type<<4)

	this gets is a lookup index. its downside is that the more types we have the bigger the array.
	to conserving space we can just do this.

	lhs_type = 1
	rhs_type = 2

	type_idx = lhs_table_assigment[lhs_type]|rhs_table_assignment[rhs_type]

	this means we can do this for assignment

	lhs_table_assigment[1] = type_copy;
	rhs_table_assigment[2] = type_copy<<4;

	lookup_table[type_copy|(type_copy<<4)] = op_copy;

	the ISSUE in FSL is the type passed to us.

	either we go though the trobble of putting into the lookup table
	or just use this.
*/
#define _ima_nf_float 0
#define _ima_add 0
#define _ima_sub 1
#define _ima_mul 2
#define _ima_div 3

struct ima_opinfo {
	_int_u op;
	char const *opident;
};
#define _ima_instr_em_reg 0
#define _ima_instr_em_const_float 1
#define _ima_instr_em_const_int 2
#define _ima_instr_em_const_str 3
struct ima_instr_em {
	_8_u type;
	_8_u fmt[2];
	struct ima_expl *xe, *ye;
	union {
		_64_u val;
		double valf;
		char const *str;
	};
};
#define _ima_op_mov_4 1
struct ima_instr {
	_32_u op;
	_32_u info;
	void *priv;
	struct ima_instr_em l[4], r[4];
	_64_u dest,dst,comp;
	_16_u fmt;
	_64_u atb;
	_64_u word;
	_64_u desc;
	_64_u desc0;
	_64_u sampler;
	_64_u offset;
	_int_u type;
	_64_u v0,v1,v2,v3;
	_8_s end;
	_64_u bits;
	_int_u exp;
	struct ima_instr *next;
};
#define _ima_format_32f4xyzw	0
#define _ima_format_32f4x			1
#define _ima_format_32u1xyzw	2
#define _ima_format_32u1x			3


#define IMA_INTR_K	1
#define IMA_PRIM	2
#define IMA_SAMPLE	4

#include "radeon.h"

void ima_init(void);
void ima_ys(void);
void ima_resin(void);
//ima_instr.c
//allocate instr instructions
struct ima_instr* ima_instr_alloc(_int_u);

struct ima_instr*
ima_instr_new(void);
struct ima_instr* ima_instr_new(void);
//a block of them
void ima_ib_new(struct ima_instr*,_int_u);
void rd_instr_vop1d(struct ima_instr*);
void rd_instr_vecmov(struct ima_instr*);
void rd_instr_param(struct ima_instr*);
void rd_instr_exit(struct ima_instr*);
void rd_intr_export(struct ima_instr *__i);
/*
	diffrence between image_* and just load/store?
	think of it as texture store/load bad naming? should change to tex_store/ld?

	image_* uses coords of pixel shader to determine the placement of the pixel
	store/load uses linear offsets.
*/
void rd_instr_load16(struct ima_instr*);
void rd_instr_image_store(struct ima_instr*);
void rd_instr_image_load(struct ima_instr*);
void rd_instr_image_sample(struct ima_instr*);

void rd_instr_store(struct ima_instr*);
void rd_instr_load(struct ima_instr*);
void rd_instr_tex(struct ima_instr*);
void ima_interreg_cvt(_64_u,_64_u,_64_u,_64_u);
#define __ima_HW	((_64_u)1<<31)
#define _ima_op_rcp32f										0
#define _ima_op_mulu16										1
#define _ima_op_add32u										2
#define _ima_op_mov												3
#define _ima_op_fadd											4
#define _ima_op_fsub											5
#define _ima_op_fmul											6
#define _ima_op_fmac											7
#define _ima_op_cvt_i32f32								8
#define _ima_op_cvt_pkrtz_f16_f32					9
#define _ima_op_rsq_f32										10
#define _ima_op_max32f										11
#define _ima_op_min32f										12
#define _ima_op_vecmov										13
#define _ima_op_export										14
#define _ima_op_load											15
#define _ima_op_store											16
#define _ima_op_load16										17
#define _ima_op_image_load								18
#define _ima_op_image_store								19
#define _ima_op_image_sample							20		
#define _ima_op_tex												21
#define _ima_op_param											22
#define _ima_op_endprog										23
#define _ima_op_st												24
void rd_instr_vop1(struct ima_instr*);
void rd_instr_vop2(struct ima_instr*);
void rd_instr_vop3a(struct ima_instr*);
void rd_instr_sload(struct ima_instr*);
void rd_instr_sstore(struct ima_instr*);
void rd_instr_load(struct ima_instr*);
void rd_instr_store(struct ima_instr*);


void rd_instr_const_load(struct ima_instr*);
void rd_instr_const_store(struct ima_instr*);
void rd_instr_load16(struct ima_instr*);
/*
	what ima is going to produce(its output)
*/
#define ima_regg ima_pd->_ima_regg
#define ima_regmap ima_pd->_ima_regmap

struct ima_produce{
	_16_u _ima_regg[512];
	_64_u _ima_regmap[512];
	struct ima_instr *ima_ihead;
	struct ima_instr *ima_itail;

	struct ihc_file file;
};
#define ima_instr_head ima_pd->ima_ihead
#define ima_instr_tail ima_pd->ima_itail
_64_u ima_getregmap(_64_u);
extern struct ima_produce *ima_pd;
void rd_instr_emit(struct ima_instr*);
void ima_instr_dump(void);
/*
	this stage is only at shader parse
*/
void ima_takein_hg(void);
#endif/*__ima__h*/
