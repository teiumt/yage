#include "ima.h"
#include "../../../assert.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../strf.h"
#include "../../../ihc/ihc.h"
#include "../../../m_alloc.h"
#include "../../../assert.h"
#define REG_SAUCE 0
#define REG_DEST 1
struct r_opdetails{
	char const *name;
	_16_u r_info[8];
};
#define __default__ {REG_SAUCE,REG_SAUCE,REG_SAUCE,REG_SAUCE}
static struct r_shader *r_dummy;

struct r_shader *rd_sh = &r_dummy;
void r_init_shader(struct r_shader *__sh){
	__sh->param_cnt = 0;
	__sh->pos_cnt = 0;
	__sh->mrt_cnt = 0;
}
void r_sign_export(struct r_instr *__i){
	_int_u rtyp;
	rtyp = R_REG_GSTMP(__i->r_reg[0]);
	switch(rtyp){
		case _R_POS:
			rd_sh->pos_cnt++;
		break;
		case _R_MRT:
			rd_sh->mrt_cnt++;
		break;
		case _R_PAR:
			rd_sh->param_cnt++;
		break;
	}
}

static struct r_opdetails _opd[] = {
	{//_r_op_rcp32f
		.name = "rcp32f",
		{REG_DEST,REG_SAUCE}
	},
	{//_r_op_mulu16
		.name = "mulu16",
		__default__
	},
	{//_r_op_add32u
		.name = "add32u",
		__default__
	},
	{//_r_op_mov
		.name = "mov",
		{REG_DEST,REG_SAUCE}
	},
	{//_r_op_fadd
		.name = "fadd",
		{REG_SAUCE,REG_SAUCE,REG_DEST}
	},
	{//_r_op_fsub
		.name = "fsub",
		{REG_SAUCE,REG_SAUCE,REG_DEST}
	},
	{//_r_op_fmul
		.name = "fmul",
		{REG_SAUCE,REG_SAUCE,REG_DEST}
	},
	{//_r_op_fmac
		.name = "fmac",
		{REG_SAUCE,REG_SAUCE,REG_DEST}
	},
	{//_r_op_cvt_i32f32
		.name = "cvt_i32f32",
		__default__
	},
	{//_r_op_cvt_pkrtz_f16_f32
		.name = "cvt_pkrtz_f16_f32",
		{REG_DEST,REG_SAUCE,REG_SAUCE}
	},
	{//_r_op_rsq_f32
		.name = "rsq_f32",
		{REG_DEST,REG_SAUCE}
	},
	{//_r_op_max32f
		.name = "max32f",
		{REG_SAUCE,REG_SAUCE,REG_DEST}
	},
	{//_r_op_min32f
		.name = "min32f",
		{REG_SAUCE,REG_SAUCE,REG_DEST}
	},
	{//_r_op_imgstore
		.name = "imgstore",
		__default__
	},
	{//_r_op_imgsample
		.name = "imgsample",
		__default__
	},
	{//_r_op_imgload
		.name = "imgload",
		__default__
	},
	{//_r_op_sldw
		.name = "sldw",
		__default__
	},
	{//_r_op_endprog
		.name = "endprog",
		__default__
	},
	{//_r_op_waitcnt
		.name = "waitcnt",
		__default__
	},
	{//_r_op_load16
		.name = "load16",
		__default__
	},
	{//_r_op_load
		.name = "load",
		{REG_DEST}
	},
	{//_r_op_store
		.name = "store",
		__default__
	},
	{//_r_op_intrp
		.name = "intrp_p1f32",
		{REG_DEST}
	},
	{//_r_op_intrp
		.name = "intrp_p2f32",
		__default__
	},
	{//_r_op_mov_s
		.name = "mov_s",
		__default__
	}
};
void rdump(void){
	struct r_instr *i;
	i = _rstrc.r_ihead;
	_int_u ip = 0;
	while(i != NULL){
		printf("r_instr: %u, %s.\n",ip++,_opd[i->op].name);
		i = i->next;
	}
}

struct r_instr* r_instr_new(void){
	printf("new instrc.\n");
	struct r_instr *i;
	i = m_alloc(sizeof(struct r_instr));
	i->next = NULL;
	_int_u j;
	j = 0;
	for(;j != 8;j++){i->r_info[j] = -1;}
	if(!_rstrc.r_ihead){
		_rstrc.r_ihead = i;
	}

	if(_rstrc.r_itail != NULL){
		_rstrc.r_itail->next = i;
	}

	_rstrc.r_itail = i;

	return i;
}
struct _r_struc _rstrc;
#define REG_ALLOCED 2
_64_u static bridgeinfo[512];
void r_init(void){
	_rstrc.r_ihead = NULL;
	_rstrc.r_itail = NULL;
	_int_u i;
	i = 0;
	for(;i != 8;i++){
		_rstrc.last_exports[i] = NULL;
	}
}


#define REG(__x) ima_regmap[__x]
#include "r_struc.h"
_16_u consolidated_info(struct r_instr *i){
	_16_u r = 0;
	_int_u j;
	j = 0;
	for(;i->r_info[j] != (_16_u)-1;j++){	
		assert(j<8);
		r |= _R_TY_BIT(i->r_info[j]);
	}
	return r;
}
/*
	NOTE:

*/
struct regdata{
	_16_u word;
	_16_u start;					//instruction that a write took place.
	_16_u last;					 //last place it has either read or tweeked
};


struct instrdata{
	_16_u alloc[8];
	_16_u dealloc[8];
	_int_u d_n;
	_int_u a_n;
};

struct reg_dish {
	_64_u reg[32];
	_int_u nfree;
	_int_u widness;
};

const static struct reg_dish vec_disht = {
	{56,52,48,44,40,32,28,24,20,16,12,8},
	12,
	4
};
const static struct reg_dish mat_disht = {
	{60,76,92},
	3,
	16
};

static struct reg_dish vec_dish;
static struct reg_dish mat_dish;

_64_u static regalloc(struct reg_dish *__dish) {
	assert(__dish->nfree != 0);
	__dish->nfree--;

	return __dish->reg[__dish->nfree];
}

void static regfree(struct reg_dish *__dish, _64_u __n) {
	__dish->reg[__dish->nfree] = __n;
	__dish->nfree++;
}
static _32_u regg_bits[512];
#define REG_GROUP_ALLOC(__x) (1<<(__x))
void static _reg_alloc(_16_u __reg){
	_16_u gidx;
	_16_u group;
	gidx = __reg>>4;
	group = ima_regg[gidx];
	_int_u start;
	start = __reg&~(_16_u)0xf;
	_32_u bits, bit;
	bits = regg_bits[gidx];
	bit = REG_GROUP_ALLOC(__reg&0xf);
	if((bits&bit)>0){
		printf("REGISTER_DEFINED, %u.\n",__reg);
		return;
	}
	printf("REGISTER_ALLOC(%u, %x, %u).\n",__reg,bits,gidx);

	_int_u ncomps;
	ncomps = !(group&IMA_REG_GROUP_OF_16_32)?4:16;
	/*
		this tells us the first register.
		R0<<==
		R1
		R2
		R3

		so in the case an instruction touches lets say R2.
		we need a way back to R0 the beginning of the grouped registers.

		WHY?

		EXAMPLE FSL CODE:

		"
			vec4 test;
			test.x = 21.0;
		"
	*/
	struct reg_dish *dish;
	if(ncomps == 4){
		dish = &vec_dish;
		regg_bits[gidx] = 0xf;
	}else{
		dish = &mat_dish;
		regg_bits[gidx] = 0xffff;
	}

	_16_u rv;
	rv = regalloc(dish)|R_REG_STAMP(_R_VGPR);
	_int_u i;
	i = 0;
	for(;i != ncomps;i++){
		printf("REMAP{%u} to %u, for %u\n",start+i,rv+i,__reg);
		ima_regmap[start+i] = rv+i;
	}
}
void _reg_free(_16_u __reg){
	_16_u gidx;
	_16_u group;
	gidx = __reg>>4;
	group = ima_regg[gidx];
	_int_u start;
	start = __reg&~(_16_u)0xf;
	_32_u bits, bit;
	bits = regg_bits[gidx];
	bit = REG_GROUP_ALLOC(__reg&0xf);

	if((bits&bit)>0){
		bits &= ~bit;
		regg_bits[gidx] = bits;
		if(bits>0){
			goto _out;
		}
	}else{
	_out:
		printf("##> %u, %x\n",__reg,bits);
		return;
	}
	printf("REGISTER_DEALLOC(%u).\n",__reg);
	
	struct reg_dish *dish;
	if(!(group&IMA_REG_GROUP_OF_16_32)){
		dish = &vec_dish;
	}else{
		dish = &mat_dish;
	}
	regfree(dish,ima_regmap[start]);
}
#define REG_DEF 1
#include "../../../lib.h"
#include "../../test/shader/test.h"

void r_testing(struct regdata *data){
	_int_u t;
	t = 0;
	for(;t != 4;t++){
		_16_u reg;
		reg = 16+t;
		ima_regmap[reg] = (AMDE_TEST+t)|R_REG_STAMP(_R_VGPR);
		data[reg].word |= REG_DEF;
	}
}

void r_write_regs(void){
  struct r_instr *i;
	i = _rstrc.r_ihead;
  while(i != NULL){
    _int_u j;
    j = 0;
    for(;i->r_info[j] != (_16_u)-1;j++){
      if(i->r_info[j] == _R_TYP_REG){
        i->r_reg[j] = REG(i->r_reg[j]);
        i->r_info[j] = _R_TYP_PHYREG;
      }
    }
    i = i->next;
  }
}

void r_optm(void);
void _r_instr_emits(void){
	mem_set(ima_regmap,0x00,512*sizeof(_64_u));
	vec_dish = vec_disht;
	mat_dish = mat_disht;

	struct instrdata *idata;
	idata = m_alloc(sizeof(struct instrdata)*512);
	struct regdata *data;
	data = m_alloc(sizeof(struct regdata)*512);
	_int_u j;
	j = 0;
	for(;j != 512;j++){
		data[j].word = 0;
		_int_u k;
		k = 0;
		for(;k != 8;k++){
			idata[j].d_n = 0;
			idata[j].a_n = 0;
		}
		regg_bits[j] = 0;
	}
	
	_int_u n_alloc = 0;
	_int_u k = 0;
	struct r_instr *i;
	i = _rstrc.r_ihead;
	while(i != NULL){
		_16_u op;
		op = i->op;
		struct r_opdetails *inf;
		inf = _opd+op;
		_16_u info;
		info = consolidated_info(i);
		if(info&_R_TY_BIT(_R_TYP_REG)){
			/*
				this means that this instruction make use of registers,
				and may alter its contents or read it.
			*/
			_16_u rinf;
			_int_u j;
			j = 0;
			for(;(rinf = i->r_info[j]) != (_16_u)-1;j++){
				assert(j<8);
				_16_u reg;
				reg = i->r_reg[j];
				struct regdata *r;
				if(rinf == _R_TYP_REG){
					r = data+reg;
					if(BIT_TEST(inf->r_info[j],REG_DEST)){
						printf("%u: REG_DEST; %u.\n",i->op,reg);
						if(!BIT_TEST(r->word,REG_DEF)){
							r->start = k;
							r->word |= REG_DEF;
							r->last = -1;
							idata[k].alloc[idata[k].a_n++] = reg;
						}
					}else{
						printf("%u: SRC_DEST; %u.\n",i->op,reg);
						r->last = k;
					}
				}
			}
		}
		k++;
		i = i->next;
	}

	/*
		next we go over each and every register and fix them up
	*/	
	j = 0;
	for(;j != 512;j++){
		struct regdata *r;
		r = data+j;
		if(BIT_TEST(r->word,REG_DEF)){
			struct instrdata *id;
			id = idata+r->last;

			id->dealloc[id->d_n++] = j;
			
		}
	}

	j = 0;
	for(;j != 512;j++){
		struct instrdata *id;
		id = idata+j;
		k = 0;
		_16_u reg;
		for(;k != id->a_n;k++){
			reg = id->alloc[k];
			_reg_alloc(reg);
		}

		k = 0;
		for(;k != id->d_n;k++){
			reg = id->dealloc[k];
			_reg_free(reg);
		}
	}

	r_write_regs();
	r_optm();

	j = 0;
	for(;j != 8;j++){
		i = _rstrc.last_exports[j];
		if(i != NULL){
			i->info |= 32;
		}
	}
	m_free(idata);
	m_free(data);
}

_8_s static can_expel(struct r_instr *i){
	if(i->op == _r_op_mov){
		if((i->r_info[0]|(i->r_info[1]<<8)) == (_R_TYP_REG|(_R_TYP_REG<<8))){
			if(i->r_reg[0] == i->r_reg[1]){
				return 0;
			}
		}
	}
	return -1;
}
#define REG(__x) ima_getregmap(__x)
void r_emit_instrs(struct r_producer *__p){
	struct r_instr *i;
	i = _rstrc.r_ihead;
	while(i != NULL){
		void(*f)(struct r_instr*);
		_16_u sel;
		sel = __p->selt[i->op];
		if(sel == (_16_u)-1){
			printf("(YS)unreachable.\n");
		}else{
			f = __p->ft[sel];
			printf("got instruction- %u.\n",i->op);

			if(can_expel(i) == -1){
				f(i);
			}
		
		}

		i = i->next;
	}

}

/*
	go from ima to (R)
*/
void r_propagate(void){
	r_consume();
	_r_instr_emits();
}

void r_produce(struct r_producer *__pd){
	r_emit_instrs(__pd);
}

void r_clean(void){
	struct r_instr *i;
	i = _rstrc.r_ihead;
	while(i != NULL){	
		struct r_instr *j;
		j = i;
		i = i->next;
		m_free(j);
	}
}

