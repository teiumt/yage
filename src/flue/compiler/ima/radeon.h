#ifndef __ima__radeon__h
#define __ima__radeon__h
#include "ima.h"
#include "r_struc.h"
struct rd_operand {
		char const *name;
		_int_u n;
};
/*
		this is needed for optimizing
*/
#define _rreg_gpr 0
#define _rreg_const 1
struct rd_rreg {
		_8_u type;
		char const *name;
		_int_u nlen;
		char const *id;
		_int_u idlen;
};
/*
	ambiguity:

	either reg could be SCALER or VECTOR. however is unknown until a instruction desides if it is or not.
	mov %reg,0
	xor %reg,1
*/
struct r_reg{
};
/*
  tells us that the register group is made of 16 32-bit registers
*/
#define R_REG_GROUP_OF_16_32  (1<<15)
#define R_REG_GROUP_ALLOC(__x)  (1<<(__x))
#define R_REG_STAMP(__x) ((__x)<<12)
#define R_REG_GSTMP(__x) (((__x)>>12)&0xf)
#define R_REG_GET(__x) ((__x)&~(0xf<<12))
struct r_reggroup{
	_16_u comps[16];
	_int_u n;
};

//flavour
#define _R_VGPR 0
#define _R_SGPR 1
#define _R_M		2
#define _R_POS	3
#define _R_PAR	4
#define _R_MRT	5
enum{
	_R_TYP_IMM,
	_R_TYP_FLOAT,
	_R_TYP_STR,
/*
	NOTE: registers are connections between instructions.
	NOT! storage locations.

	if you have an two instructions connected then a register is used.
	thats how they should be thought about in that way.
*/
	_R_TYP_REG,
	_R_TYP_PHYREG,
/*
	in this case this data may be a referance to someplace else.
	example vectors with more then 1-component will be stored else where accessable by index.
*/
	_R_TYP_REFER,
	_R_TYP_DESC
};

#define _R_TY_BIT(__typ) (1<<__typ)
#define R_REG(__typ) __typ

/*
	this structure exists so in the case we allocate 
	a 4-component vector for exmaple

	""
	vec4 test;
	test.x = 21299;
	test.y = 0.1
	float y;
	y = 33;
	y *= 2;

	test.y *= y;
	test.z = -1;
	""

	for example in this case vec4 leaves 2 vectors unused while doing calulcations for y.
*/

/*
	for instructions that dont touch any registers, we can just 
	create some sort of skip list.
*/
#include "r_ops.h"
struct r_instr{
	_16_u op;
	_16_u sub;
	/*
		ignore word 'reg'
	*/
	_64_u r_reg[8];
	_16_u r_info[8];

	_64_u atb;
	_64_u desc;
	_64_u offset;
	_16_u fmt;
	_64_u bits;
	_16_u comp;

	_64_u info;

	struct r_instr *next;
};
#define _r_comp_selx 0
#define _r_comp_sely 1
#define _r_comp_selz 2
#define _r_comp_selw 3
struct _r_struc{
	struct r_instr *last_exports[8];
	struct r_instr *r_ihead,*r_itail;
};
extern struct _r_struc _rstrc;
struct r_producer{
	//array of routines
	void(*ft[64])(struct r_instr*);
	//routine selector
	_16_u selt[64];
};

void r_sign_export(struct r_instr*);
void r_produce(struct r_producer*);
extern struct r_producer r_ys;
extern struct r_producer r_amde;
void r_init(void);
void r_emit_instrs(struct r_producer*);
void r_consume(void);
void r_propagate(void);
void r_clean(void);
void r_write_regs(void);

struct r_instr* r_instr_new(void);
#endif /*__ima__radeon__h*/
