#include "ima.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../strf.h"
#include "../../../ihc/ihc.h"
#include "../../../m_alloc.h"
#include "../../../assert.h"
/*
	HOW WE DO THIS SHIT!!!!!


	first we have the real routine.
	this routine generates the corrsponding IMA instructions.

	void vecvec_assign(_16_u *dst,_16_u *src,_int_u n){
		_int_u i;
		i = 0;
		for(;i != __n;i++){
			vop1(_ima_op_mov,dst[i],src[i]);
		}
	}


	second we have the initiation routine.

	void _vecvec_assign(struct ys_em *__em){
		_16_u dst[4],src[4];
			

		- this is were we pull the data we need from __em
		- in a way that respects its layout

		vecvec_assign(dst,src);
	}
*/
#define _l_r		0
#define _r_l		1
static void(*functab[])(struct ys_em*);
#define ring(__x,...) functab[__x](__VA_ARGS__)
/*
	why¿?

	we have to iterate over the ima_node structure twice. TWIX

	so we could do that or but we might iterate over a node that does not use a register thus no point in iterating over it.

	what we do?

	node that dont interact with registers, we iterate over once,
	the onces that do a create a new node structure for and later iterate over it.
	avoiding such wast. !would be more inefficient if number of non register nodes is low

	why emit text??
	because its easer to debug its clear whats happening and what instructions are being run
	addition to ease of altering instructions simply.

	later it will be more direct. ie emitting code to shader code store
*/
static _64_u personal[64];
struct ima_instr* 
emitmov(void) {
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_mov;
	return i;
}
#define emitclear(__idx,__rval) \
	{\
		struct ima_instr *i;\
		i = emitmov();\
		i->l->type = _ima_instr_em_reg;\
		i->l->val = __rval+__idx;\
		i->r->type = _ima_instr_em_const_float;\
		i->r->valf = 0.0;\
	}

#define EXTRACT_COMP(__t,__q) (((__t)>>((__q)<<2))&0xf)
#define EXTRACT_COMP_CNT(__t) (((__t)>>16)&0xf)
#define EXTRACT_COMP_INDEX(__t) (((__t)>>20)&0xf)
void static _vec_assign(_16_u *__dst,_16_u *__src,_32_u __c,struct hg_val *xyzw) {
	struct ima_instr *i;
	_int_u j;
	j = 0;
	for(;j != __c;j++){
		i = emitmov();
		i->l->type = _ima_instr_em_reg;
		i->l->val = __dst[j];
		i->r->type = _ima_instr_em_const_float;
		i->r->valf = hg_unpack_float(&xyzw[__src[j]].pack);		
	}
}
void static vec_assign(struct ys_em *__n) {
	struct rd_reg *r = __n->l->reg;

//	_vec_assign(r->rval,__n->r->vec.xyzw,__n->r->vec.comp);
}

void static _assign(struct ys_em *__n) {
	printf("IMA- ASSIGN.\n");
}

void static mat_assign(struct ys_em *__n){
	printf("(FSL_TO_IMA)-MAT_ASSIGN.\n");
	struct rd_reg *r = __n->l->reg;
	/*
	_vec_assign(r->rval,__n->r->vec.xyzw,__n->r->vec.comp);
	_vec_assign(r->rval+4,__n->r->vec.xyzw+4,__n->r->vec.comp>>4);
	_vec_assign(r->rval+8,__n->r->vec.xyzw+8,__n->r->vec.comp>>8);
	_vec_assign(r->rval+12,__n->r->vec.xyzw+12,__n->r->vec.comp>>12);
*/
}

void static capon(struct ys_em *__em){
	if(__em->ys != NULL) {
		struct ys_em *n;
		n = __em->ys;
		if(n->f != -1){
			ring(n->f,n);
		}
	}
}

void static vop1(_32_u __op, _64_u l, _64_u r) {
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = __op;
	i->l->val = l;
	i->r->type = _ima_instr_em_reg;
	i->r->val = r;
}

/*
	l = SRC0
	r = SRC1
	il = DST
*/
void static vop2(_32_u __op, _64_u l, _64_u r, _64_u il) {
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = __op;
	i->l->type = _ima_instr_em_reg;
	i->l->val = l;
	i->r->val = r;
	i->dest = il;
}

_16_u static _regvalue0(struct ys_em *__em,_int_u __idx){
	struct rd_reg *r;
	r = __em->c.ys->reg;
	return r->rval+EXTRACT_COMP(__em->c.comp.comp,__idx);
}


void static _reg_transcribe(_16_u __r,_16_u *lv,_32_u __cd,_int_u __c){
	_int_u j;
	j = 0;
	for(;j != __c;j++){
		lv[j] = __r+EXTRACT_COMP(__cd,j);
	}
}


void static _vec_move(_16_u *__lv,_16_u *__rv,_int_u __c){
	_int_u j;
	j = 0;
	for(;j != __c;j++){
		vop1(_ima_op_mov,__lv[j],__rv[j]);
	}
}

void static _loadreg0(_16_u *dst, struct ys_em *__em){
	struct rd_reg *r;

	r = __em->c.ys->reg;
	
	_16_u rv;
	rv = r->rval;

	if(__em->ident&_ima_matindex){
		rv+=EXTRACT_COMP_INDEX(__em->c.comp.comp)*4;
	}

	_reg_transcribe(rv,dst,__em->c.comp.comp,4);
}


_int_u static bitcnt(_64_u __bits) {
	_int_u i;
	i = 0;
	for(;!(__bits&1);i++,__bits>>=1);
	return i;
}
#define TMP0 0
#define TMP1 1
#define TMP2 2
#define TMP3 3

static struct rd_reg __r0 = {.rval=TMP0};



void static vecother(struct ys_em *__n) {
	return;

//	ring(((struct ima_node*)__n->ys_r)->f,__n->ys_r);
	struct rd_reg *l, *r;
	l = __n->l->reg;
	r = __n->r->reg;

//	_vec_move(l->rval,r->rval,0xf);
}

/*
	this only works in some cases where order dose not matter;

	test = test*test; PASS
	test = test+test; PASS
	test = test/test; FAIL
	test = test-test; FAIL
*/
void static _vecmat_arth(struct ys_em *__n,_16_u __op){
	struct rd_reg *v,*m;
	m = __n->ems[1-__op]->reg;
	v = __n->ems[__op]->reg;


	struct rd_reg *il;
	il = &__r0;
	__n->reg = il;
	
	vop2(_ima_op_fmul,v->rval,m->rval,il->rval);
	vop2(_ima_op_fmac,v->rval+1,m->rval+1,il->rval);
	vop2(_ima_op_fmac,v->rval+2,m->rval+2,il->rval);
	vop2(_ima_op_fmac,v->rval+3,m->rval+3,il->rval);

	vop2(_ima_op_fmul,v->rval,m->rval+4,il->rval+1);
	vop2(_ima_op_fmac,v->rval+1,m->rval+5,il->rval+1);
	vop2(_ima_op_fmac,v->rval+2,m->rval+6,il->rval+1);
	vop2(_ima_op_fmac,v->rval+3,m->rval+7,il->rval+1);

	vop2(_ima_op_fmul,v->rval,m->rval+8,il->rval+2);
	vop2(_ima_op_fmac,v->rval+1,m->rval+9,il->rval+2);
	vop2(_ima_op_fmac,v->rval+2,m->rval+10,il->rval+2);
	vop2(_ima_op_fmac,v->rval+3,m->rval+11,il->rval+2);

	vop2(_ima_op_fmul,v->rval,m->rval+12,il->rval+3);
	vop2(_ima_op_fmac,v->rval+1,m->rval+13,il->rval+3);
	vop2(_ima_op_fmac,v->rval+2,m->rval+14,il->rval+3);
	vop2(_ima_op_fmac,v->rval+3,m->rval+15,il->rval+3);
}

void static vecmat_arth(struct ys_em *__n) {
	printf("VECMAT_ARTH.\n");
	_vecmat_arth(__n,personal[__n->f]);
}

void static vecvec_arth(struct ys_em *__n) {
	_16_u lh[4],rh[4];

	_loadreg0(lh,__n->l);
	_loadreg0(rh,__n->r);

	struct rd_reg *il;
	il = &__r0;
	__n->reg = il;

	_32_u op;
	op = -1;
	switch(__n->op) {
		case _ima_add:
			op = _ima_op_fadd;
		break;
		case _ima_sub:
			op = _ima_op_fsub;
		break;
		case _ima_mul:
			op = _ima_op_fmul;
		break;
	}

	vop2(op,lh[0],rh[0],il->rval);
	vop2(op,lh[1],rh[1],il->rval+1);
	vop2(op,lh[2],rh[2],il->rval+2);
	vop2(op,lh[3],rh[3],il->rval+3);
}
void static _vec_int_arth(struct ys_em *__n) {
	while(1);
}

void static
emitend(void) {
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_endprog;
}
void rd_export(struct ima_instr*);
void static exp_vec(struct ys_em *__n) {
	struct ys_em *em = __n->l;
	struct rd_reg *r = __n->r->reg;
	_64_u rval;
	rval = r->rval;
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_export;
	i->exp = em->exp;
	i->v0 = rval;
}
void _r_instr_emits(void);
void decl(struct ys_em*);
void ima_takein_hg(void) {
	struct ys_em *k;
	k = hg_pd->hg_decl;
	while(k != NULL){
		decl(k);	
		k = k->decl;
	}

	k = hg_pd->hg_top_node;
	while(k != NULL) {
		assert(k->f != -1);
		ring(k->f,k);
		k = k->next;
	}
	emitend();
	ima_instr_dump();
}

void static _buf(_16_u __op,_int_u __placement,_int_u __offset,_16_u __r,_64_u __bits){
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = __op;
	i->desc = __placement;
	i->offset = __offset;//__n->offset;
	i->l->val = __r;

	i->fmt = _ima_format_32f4xyzw;
	i->bits = __bits;
}

void static _bufn(_16_u __op,_int_u __placement,_int_u __offset,_16_u __r,_64_u __bits,_int_u __n){
	_int_u i;
	i = 0;
	__n*=4;
	for(;i != __n;i+=4){
		_buf(__op,__placement,__offset+(i*4),__r+i,__bits);
	}
}

void static __buf(struct ys_em *__n,_16_u __op){
	struct ys_em *buf;
	buf = __n->ems[1-__op];
	struct ys_em *em;
	em = __n->ems[__op];
	struct rd_reg *r = em->reg;
	_int_u c = 1;
	if(em->ident == _ima_mat){
		c = 4;
	}
	_bufn(_ima_op_load+__op,buf->placement,buf->offset,r->rval,buf->bits,c);
}

void vecbuf_load(struct ys_em *__n){ 
	__buf(__n,personal[__n->f]);
}

void vecimg_load(struct ys_em *__n) {
}

void vecimg_store(struct ys_em *__n) {
}

void static vecrel_assign(struct ys_em *__n) {
	struct rd_reg *r = __n->l->reg;
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_load;
	i->desc0 = 12;
	i->desc = __n->r->placement;
	i->offset = __n->r->offset;
	i->l->val = r->rval;

	i->bits = 0;
}

void static matrel_assign(struct ys_em *__n) {
	struct rd_reg *r = __n->l->reg;
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_load16;
	i->desc0 = 12;
	i->desc = __n->r->placement;
	i->offset = __n->r->offset;
	i->l->val = r->rval;
}

/*
	texv is used in the case are passing coords are not normalized
*/
void static texv(struct ys_em *__n, struct ys_em *__l) {
	struct rd_reg *o = __n->ems[0]->reg;
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_st;
	i->v0 = 0;
	i->v1 = 1;
	vop2(_ima_op_fadd,o->rval,0|__ima_HW,TMP0);
	vop2(_ima_op_fadd,o->rval+1,1|__ima_HW,TMP0+1);

	struct rd_reg *l = __l->l->reg;

	vop2(_ima_op_fmul,o->rval+2,TMP0,TMP0);
	vop2(_ima_op_fmul,o->rval+3,TMP0+1,TMP0+1);

	struct ima_instr *img;
	img = ima_instr_new();
	img->op = _ima_op_image_load;	
	img->l->val = l->rval;
	img->r->val = TMP0;
	img->desc = __n->ems[1]->placement;
	img->op = _ima_op_image_load;	
}

void static tex(struct ys_em *__n, struct ys_em *__l) {
	/*
		NOTE: could or could not exist
	*/
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_param;
	
	struct rd_reg *l = __n->ems[0]->reg;
	i->desc = __n->ems[0]->exp;
	/*
		il stores are pixel positon
	*/
	i->v0 = TMP0;
	i->comp = 0x3;
	struct rd_reg *o = __l->l->reg;

	struct ima_instr *img;
	img = ima_instr_new();
	img->op = _ima_op_image_load;
	img->l->val = o->rval;
	img->r->val = TMP0;
	img->desc = __n->ems[1]->placement;	
}

void static
vecintrp_assign(struct ys_em *__n) {
	printf("VECINTRP.\n");
	struct rd_reg *l = __n->l->reg;
	struct ima_instr *i;

	i = ima_instr_new();
	i->op = _ima_op_param;
	i->desc = __n->r->exp;
	i->v0 = l->rval; 
	i->comp = 0xf;
}
/*
	NOTE:
		this is an assign operation when talking in FSL
*/
void static tex_load(struct ys_em *__n) {
	tex(__n->r->ys,__n);
}

void static texv_load(struct ys_em *__n) {
	texv(__n->r->ys,__n);
}


void static tex_store(struct ys_em *__n) {

}



/*
	this function serves no purpose but to allow us to connect instructions to other instruction though a number
*/
static _int_u ima_regn;
static _int_u ima_totrc;

void static *__reg_alloc(_int_u __n) {
	struct rd_reg *ret;
	ret = m_alloc(sizeof(struct rd_reg));

	_int_u i;
	i = 0;
	if(__n == 1) {
		ima_regg[ima_regn] = 0;
	}else{
		ima_regg[ima_regn] = IMA_REG_GROUP_OF_16_32;
		printf("LARGE REG!\n");
	}
	ret->rval = ima_totrc;
	ima_totrc+=16;
	ima_regn++;

	return ret;
}
void decl(struct ys_em *__em) {
  __em->val = 0;
  __em->reg = __reg_alloc(__em->cnt);
  printf("REG_ALLOC, CNT: %u, %u\n",__em->cnt,((struct rd_reg*)__em->reg)->rval);
}

void static normalize(struct ys_em *__n) {
	printf("NORMALIZE.\n");
	struct rd_reg *l = __n->ems[0]->reg;
	struct rd_reg *il = &__r0;

	vop2(_ima_op_fmul,l->rval,l->rval,TMP0);
	vop2(_ima_op_fmac,l->rval+1,l->rval+1,TMP0);
	vop2(_ima_op_fmac,l->rval+2,l->rval+2,TMP0);
	vop2(_ima_op_fmac,l->rval+3,l->rval+3,TMP0);
	
	vop1(_ima_op_rsq_f32,TMP0,TMP0);
	vop2(_ima_op_fmul,l->rval,TMP0,il->rval);
	vop2(_ima_op_fmul,l->rval+1,TMP0,il->rval+1);
	vop2(_ima_op_fmul,l->rval+2,TMP0,il->rval+2);
	vop2(_ima_op_fmul,l->rval+3,TMP0,il->rval+3);
	__n->reg = il;
}

void static dot_product(struct ys_em *__n) {
	printf("DOT_PRODUCT.\n");

	_16_u lh[4],rh[4];
	_loadreg0(lh,__n->ems[0]);
	_loadreg0(rh,__n->ems[1]);
	struct rd_reg *il = &__r0;
	vop2(_ima_op_fmul,lh[0],rh[0],il->rval);
	vop2(_ima_op_fmac,lh[1],rh[1],il->rval);
	vop2(_ima_op_fmac,lh[2],rh[2],il->rval);
	vop2(_ima_op_fmac,lh[3],rh[3],il->rval);
	__n->reg = il;
}

_32_u static op_for_arth(_int_u __op) {
	switch(__op) {
		case _ima_add:
			return _ima_op_fadd;
		case _ima_sub:
			return _ima_op_fsub;
		case _ima_mul:
			return _ima_op_fmul;
	}
	assert(1 == 0);
	return -1;
}
/*
	we have three choices for this,
	A) we add the index to the register name.
		A+1
		A+2
		A+...
	B) we store the register name in memory
		A[0]
		A[1]
		A[...]
	C) we add an adjacent array
		A+array[0]
		A+array[1]
		A+array[...]
*/
static _16_u inc_arr[] = {0,1,2,3};
static _16_u zero_arr[] = {0,0,0,0};
void static
_vfloat_arth(struct ys_em *__n,struct rd_reg *l,struct rd_reg *r,_16_u *__l, _16_u *__r,_32_u __op) {	
	struct rd_reg *il = &__r0;
	vop2(__op,l->rval+__l[0],r->rval+__r[0],il->rval);
	vop2(__op,l->rval+__l[1],r->rval+__r[1],il->rval+1);
	vop2(__op,l->rval+__l[2],r->rval+__r[2],il->rval+2);
	vop2(__op,l->rval+__l[3],r->rval+__r[3],il->rval+3);
	__n->reg = il;
}

#define vfloat_arth(__x,__y,__z,__op)			_vfloat_arth(__x,__y,__z,inc_arr,zero_arr,__op)
#define vfloat_arth_r(__x,__y,__z,__op)		_vfloat_arth(__x,__y,__z,zero_arr,inc_arr,__op)

void static vop2_strreg(double val, _64_u __reg, _64_u __dst) {
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_fsub;
	i->l->type = _ima_instr_em_const_float;
	i->l->valf = val;
	i->r->val = __reg;
	i->dest = __dst;
}

void static constvec_arth(struct ys_em *__n){
	struct rd_reg *r = __n->r->reg;
	struct rd_reg *il = __n->il->reg;
	double val = hg_unpack_float(&__n->l->pack);
	vop2_strreg(val,r->rval,il->rval);
	vop2_strreg(val,r->rval+1,il->rval+1);
	vop2_strreg(val,r->rval+2,il->rval+2);
	vop2_strreg(val,r->rval+3,il->rval+3);
}

void static vecfloat_arth(struct ys_em *__n) {
	/*
		find good way to remove this
	*/
//	capon(__n->r);
	struct rd_reg *l = __n->l->c.ys->reg;
	struct rd_reg *r = __n->r->c.ys->reg;
	_16_u *_l,*_r;	
	
	if(__n->l->ident == _ima_float){
		_l = zero_arr;
		_r = inc_arr;
	}else{
		_l = inc_arr;
		_r = zero_arr;
	}

	_vfloat_arth(__n,l,r,_l,_r,op_for_arth(__n->op));
}

void static inverse(struct ys_em *__n) {
	printf("INVERSE.\n");
	struct rd_reg *r = __n->ems[0]->c.ys->reg;
	struct rd_reg *il = &__r0;
	vop1(_ima_op_rcp32f,il->rval,r->rval);
	vop1(_ima_op_rcp32f,il->rval+1,r->rval+1);
	vop1(_ima_op_rcp32f,il->rval+2,r->rval+2);
	vop1(_ima_op_rcp32f,il->rval+3,r->rval+3);
	__n->reg = il;
}

void static constfloat_arth(struct ys_em *__n) {
/*
	major clean up needed to remove this

	TODO:
		add way to store ints/consts/floats into a register without the thing above
*/
	struct ys_em *con;
	struct ys_em *em;
	if(__n->r->ident == _ima_const){
		con = __n->r;
		em = __n->l;
	}else{
		con = __n->l;
		em = __n->r;
	}
	capon(em->c.ys);
	struct rd_reg *il = &__r0;
	
	_16_u r;
	r = _regvalue0(em,0);
	struct ima_instr *i;
	i = emitmov();
	i->l->type = _ima_instr_em_reg;
	i->l->val = il->rval;
	i->r->type = _ima_instr_em_const_float;
	i->r->valf = hg_unpack_float(&con->vec.xyzw[0].pack);

	vop2(op_for_arth(__n->op),il->rval,r,il->rval);
	__n->reg = il;
}

void static vecother_arth(struct ys_em *__n) { 
	vecfloat_arth(__n);
}

void static
ys_em_to_instr(struct ima_instr_em *__em,struct ys_em *__em0){
	if(__em0->ident == _ima_float || __em0->ident == _ima_vec){
		__em->type = _ima_instr_em_reg;
		__em->val = _regvalue0(__em0,0);
	}else
	if(__em0->ident == _ima_const){
		__em->type = _ima_instr_em_const_float;
		__em->valf = hg_unpack_float(&__em0->vec.xyzw[0].pack);
	}else{
		printf("unreachable, %u\n",__em0->ident);
		assert(1 == 0);
	}
}

void static _min_or_max(_16_u __op,struct ys_em *__n){
	struct rd_reg *l;
	l = __n->ems[1]->c.ys->reg;
	struct rd_reg *il = &__r0;
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = __op;
	ys_em_to_instr(i->l,__n->ems[1]);	
	i->r->val = _regvalue0(__n->ems[0],0);
	i->dest = il->rval;
	__n->reg = il;
}

void static _max(struct ys_em *__n) {
	_min_or_max(_ima_op_max32f,__n);
}
void static _min(struct ys_em *__n) {
	_min_or_max(_ima_op_min32f,__n);
}

/*
	clamp(value,min,max)
*/
void static clamp(struct ys_em *__n) {
	printf("CLAMP.\n");
	struct rd_reg *il = &__r0;
	struct ima_instr *i;
	i = ima_instr_new();
	i->op = _ima_op_max32f;
	ys_em_to_instr(i->l,__n->ems[1]);
	i->r->val = _regvalue0(__n->ems[0],0);
	i->dest = il->rval;
 
	i = ima_instr_new();
	i->op = _ima_op_min32f;
	ys_em_to_instr(i->l,__n->ems[2]);
	i->r->val = il->rval;
	i->dest = il->rval;
	__n->reg = il;
}

void static float_assign(struct ys_em *__n) {
	_16_u dst[4];
	_16_u src[4];
	/*
		TODO:

		replace _r with function pointer
	*/
	capon(__n->r->c.ys);
	_loadreg0(dst,__n->l);
	_loadreg0(src,__n->r);
	
	_vec_move(dst,src,EXTRACT_COMP_CNT(__n->l->c.comp.comp));
}

void static floatconst_assign(struct ys_em *__n) {
	_16_u dst[4];
	_16_u src[4];
	_loadreg0(dst,__n->l);

	struct ys_em *_r;
	_r = __n->r->c.ys;
	_int_u c;
	c = EXTRACT_COMP_CNT(__n->l->c.comp.comp);
	_int_u j;
	j = EXTRACT_COMP_CNT(__n->r->c.comp.comp);
	if(c>j)c = j;
	_int_u i;
	i = 0;
	for(;i != c;i++){
		src[i] = EXTRACT_COMP(__n->r->c.comp.comp,i);
	}

	_vec_assign(dst,src,c,_r->vec.xyzw);
}

/*
	simply the dot product on itsself
*/
void static amalgamate(struct ys_em *__n) {
	struct rd_reg *l = __n->ems[0]->reg;
	struct rd_reg *il = __n->il->reg;
	vop2(_ima_op_fmul,l->rval,l->rval,il->rval);
	vop2(_ima_op_fmac,l->rval+1,l->rval+1,il->rval);
	vop2(_ima_op_fmac,l->rval+2,l->rval+2,il->rval);
	vop2(_ima_op_fmac,l->rval+3,l->rval+3,il->rval);
	vop1(_ima_op_rsq_f32,il->rval,il->rval);
}
void static
vecconst_assign(struct ys_em *__n) {
/*
	struct rd_reg *l = __n->l->reg;
	_int_u comp = bitcnt(__n->comp0);
	printf("############ %u -- >> %u\n",comp, __n->comp0);

	struct ima_instr *i;
	i = emitmov();
	i->l->type = _ima_instr_em_reg;
	i->l->val = l->rval+comp;
	i->r->type = _ima_instr_em_const_float;
	i->r->valf = hg_unpack_float(&__n->r->pack);
*/
}
void ima_ys(void) {
	_int_u i;
	i = 0;
	for(;i != 512;i++){
		ima_regmap[i] = i;
	}
	/*
		this is assigned the way it is to make room for temp registers
	*/
	ima_regg[0] = 0;
	ima_regn = 2;
	ima_totrc = 32;
	personal[_hg_ASSIGN_VECBUF] = _l_r;
	personal[_hg_ASSIGN_BUFVEC] = _r_l;
	personal[_hg_ARTH_VECMAT] = _l_r;
	personal[_hg_ARTH_MATVEC] = _r_l;
}

static void(*functab[])(struct ys_em*) = {
	[_hg_ARTH_VECMAT]							= vecmat_arth,
	[_hg_ARTH_VECOTHER]						= vecother_arth,
	[_hg_ARTH_CONSTFLOAT]					= constfloat_arth,
	[_hg_ASSIGN_FLOATCONST]				= floatconst_assign,
	[_hg_ASSIGN_FLOATFLOAT]				= float_assign,
	[_hg_ASSIGN_FLOATOTHER]				= float_assign,
	[_hg_ASSIGN_VECCONST]					= vecconst_assign,
	[_hg_ASSIGN_MATVCONST]					= mat_assign,
	[_hg_ASSIGN_MATREL]						= matrel_assign,
	[_hg_ASSIGN_VECINTRP]					= vecintrp_assign,
	[_hg_ASSIGN_VECVCONST]					= vec_assign,
	[_hg_ASSIGN_VECVEC]						= NULL,
//buffer vector ops
	[_hg_ASSIGN_VECBUF]						= vecbuf_load,
	[_hg_ASSIGN_BUFVEC]						= vecbuf_load,
//image vector ops
	[_hg_ASSIGN_VECIMG]						= tex_load,
//load image where (st) is from vector
	[_hg_ASSIGN_VECIMGV]						= texv_load,
	[_hg_ASSIGN_IMGVEC]						= tex_store,
	[_hg_ASSIGN_VECOTHER]					= vecother,
	[_hg_ARTH_CONSTVEC]						= constvec_arth,
	[_hg_ARTH_VECIMM]							= vecvec_arth,
	[_hg_ARTH_VECVEC]							= vecvec_arth,
	[_hg_ARTH_VECFLOAT]						= vecfloat_arth,
	[_hg_ASSIGN_VECREL]						= vecrel_assign,
	[_hg_ASSIGN_EXPVEC]						= exp_vec,
//intergrated routines
	[_hg_NORMALIZE]								= normalize,
	[_hg_DOT_PRODUCT]							= dot_product,
	[_hg_INVERSE]									= inverse,
	[_hg_CLAMP]										= clamp,
	[_hg_MAX]											= _max,
	[_hg_MIN]											= _min,
	[_hg_AMALGAMATE]								= amalgamate,
	[_hg_ARTH_FLOATVEC]						= vecfloat_arth,
	[_hg_ARTH_VECMAT]							= vecmat_arth
};
void rd_emit(struct ys_em *n) {
	_int_u i;
	i = 0;
	//alert of register usage - alertusage
//	for(;i != n->il.nalloc;i++) {
//		struct rd_reg *r = n->il.alloc[i];

//	}
	
}
