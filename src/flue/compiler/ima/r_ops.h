#ifndef __ima__r__ops__h
#define __ima__r__ops__h
#define _r_op_rcp32f                  0
#define _r_op_mulu16                  1
#define _r_op_add32u                  2
#define _r_op_mov                     3
#define _r_op_fadd                    4
#define _r_op_fsub                    5
#define _r_op_fmul                    6
#define _r_op_fmac                    7
#define _r_op_cvt_i32f32              8
#define _r_op_cvt_pkrtz_f16_f32       9
#define _r_op_rsq_f32                 10
#define _r_op_max32f                  11
#define _r_op_min32f                  12
#define _r_op_imgstore                13
#define _r_op_imgsample               14
#define _r_op_imgload                 15
#define _r_op_sldw                    16
#define _r_op_endprog                 17
#define _r_op_waitcnt                 18
#define _r_op_load16                  19
#define _r_op_load										20
#define _r_op_store										21
#define _r_op_intrp_p1f32							22
#define _r_op_intrp_p2f32							23
#define _r_op_mov_s										24 	
#define _r_op_export									25
#endif/*__ima__r__ops__h*/
