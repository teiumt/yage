#include "ima.h"
#include "radeon.h"
#include "../../../assert.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../strf.h"
#include "../../../ihc/ihc.h"
#include "../../../m_alloc.h"
#include "../../../assert.h"
/*
	NOTE: dose not take into account size of affected registers.
*/
void r_optm(void){
	struct r_instr **reg_to_instr;
	reg_to_instr = m_alloc(256*sizeof(struct r_instr*));
	mem_set(reg_to_instr,0,256*sizeof(struct r_instr*));
  struct r_instr **rr;
	struct r_instr *i;
  i = _rstrc.r_ihead;
	rr = &_rstrc.r_ihead;
	while(i != NULL){
		/*
			check each register in instruction and match
		*/
		if(i->op == _r_op_sldw){
			_int_u score = 0;
			_16_u inf;

			struct r_instr *past;
			past = reg_to_instr[R_REG_GET(i->r_reg[1])];
			if(past != NULL){
				//we expect are source to be produce of the same instruction
				if(past->op == _r_op_sldw){
					_int_u j;
					j = 0;
					for(;(inf = i->r_info[j]) != (_16_u)-1;j++){
						if(i->r_reg[j] == past->r_reg[j]){
							score |= 1<<j;
						}
					}
				}
			}

			reg_to_instr[R_REG_GET(i->r_reg[1])] = i;
			printf("got scored %u must be %u.\n",score,0x7);
			if(score == 0x7){
				*rr = i->next;
			}
		}
		rr = &i->next;
		i = i->next;
	}
	m_free(reg_to_instr);
}
