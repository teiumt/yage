#include "hg.h"
#include "../../../m_alloc.h"
#include "../../../string.h"
#include "../../../io.h"
#include "../../../lib.h"
#include "../../../strf.h"
static struct ys_em *tail_node = NULL;
struct hg_produce *hg_pd;
static _int_u nn;
struct ys_em* hg_new_node(void) {
  struct ys_em *n = m_alloc(sizeof(struct ys_em));
  if(!hg_pd->hg_top_node) {
    hg_pd->hg_top_node = n;
  }

  if(tail_node != NULL) {
    tail_node->next = n;
  }

  n->i = nn++;

  //this is only needed at end so to be MOVED later
  n->next = NULL;
  tail_node = n;
  return n;
}

static double helper_tab[] = {
	1,
	0.1,
	0.01,
	0.001,
	0.0001,
	0.00001,
	0.000001,
	0.0000001,
	0.00000001
};

void hg_pack_float_str(hg_pack *__pk,char const*__str){
	_int_u dp = 0;
	_8_u neg;
	if(*__str == '.'){
		__pk->a = 0;
	}else{
		dp = strchrl(__str,'.');
		neg = *__str == '-'?1:0;
		__pk->a = _ffly_dsn(__str+neg,dp-neg);
		if(neg){
			__pk->a = -__pk->a;
		}
	}

	char *s;
	s = __str+dp+1;
	_int_u len;
	len = str_len(s);
	__pk->b = _ffly_dsn(s,len);
	__pk->frac = helper_tab[len];
	if(neg){
		__pk->frac = -__pk->frac;
	}
}

double hg_unpack_float(hg_pack *__pk){
	double v;
	v = ((double)__pk->a)+(((double)__pk->b)*__pk->frac);
	return v;
}

_16_u hg_asglhs[32];
_16_u hg_asgrhs[32];
_16_u hg_asgtab[256];
_16_u hg_arthtab[256];
void hg_decl(struct ys_em *__em){
	__em->decl = hg_pd->hg_decl;
	hg_pd->hg_decl = __em;
}

void hg_init(void){
	nn = 0;
  _int_u i;
  i = 0;
  for(;i != 32;i++){
    hg_asglhs[i] = i;
    hg_asgrhs[i] = i;
  	hg_pd->interp_mode[i] = -1;
	}

  hg_asglhs[_hg_vec] = _hg_float;
  hg_asgrhs[_hg_vec] = _hg_float;
  hg_asglhs[_hg_mat] = _hg_float;
  hg_asgrhs[_hg_mat] = _hg_float;
  hg_asgrhs[_hg_vconst] = _hg_const;

  mem_set(hg_asgtab,0xff,256*sizeof(_16_u));
  mem_set(hg_arthtab,0xff,256*sizeof(_16_u));
  hg_plopfunc(hg_arthtab,_hg_vec,_hg_mat,_hg_ARTH_VECMAT);
  hg_plopfunc(hg_arthtab,_hg_mat,_hg_vec,_hg_ARTH_MATVEC);
  hg_plopfunc(hg_arthtab,_hg_vec,_hg_other,_hg_ARTH_VECOTHER);

  hg_plopfunc(hg_arthtab,_hg_const,_hg_float,_hg_ARTH_CONSTFLOAT);
  hg_plopfunc(hg_arthtab,_hg_float,_hg_const,_hg_ARTH_CONSTFLOAT);

  hg_plopfunc(hg_asgtab,_hg_float,_hg_const,_hg_ASSIGN_FLOATCONST);

  hg_plopfunc(hg_asgtab,_hg_float,_hg_float,_hg_ASSIGN_FLOATFLOAT);
  hg_plopfunc(hg_asgtab,_hg_float,_hg_other,_hg_ASSIGN_FLOATOTHER);

  hg_plopfunc(hg_asgtab,_hg_vec,_hg_const,_hg_ASSIGN_VECCONST);
  hg_plopfunc(hg_asgtab,_hg_mat,_hg_vconst,_hg_ASSIGN_MATVCONST);
  hg_plopfunc(hg_asgtab,_hg_mat,_hg_rel,_hg_ASSIGN_MATREL);
  hg_plopfunc(hg_asgtab,_hg_float,_hg_intrp,_hg_ASSIGN_VECINTRP);
  hg_plopfunc(hg_asgtab,_hg_vec,_hg_vconst,_hg_ASSIGN_VECVCONST);

  hg_plopfunc(hg_asgtab,_hg_vec,_hg_vec,_hg_ASSIGN_VECVEC);

  //buffer vector ops
  hg_plopfunc(hg_asgtab,_hg_float,_hg_buf,_hg_ASSIGN_VECBUF);
  hg_plopfunc(hg_asgtab,_hg_buf,_hg_float,_hg_ASSIGN_BUFVEC);
 
  //hgge vector ops
  hg_plopfunc(hg_asgtab,_hg_float,_hg_img,_hg_ASSIGN_VECIMG);
  //load hgge where (st) is from vector
  hg_plopfunc(hg_asgtab,_hg_float,_hg_imgv,_hg_ASSIGN_VECIMGV);
  hg_plopfunc(hg_asgtab,_hg_img,_hg_float,_hg_ASSIGN_IMGVEC);

  hg_plopfunc(hg_asgtab,_hg_vec,_hg_other,_hg_ASSIGN_VECOTHER);

  hg_plopfunc(hg_arthtab,_hg_const,_hg_vec,_hg_ARTH_CONSTVEC);
  hg_plopfunc(hg_arthtab,_hg_vec,_hg_imm,_hg_ARTH_VECIMM);
  hg_plopfunc(hg_arthtab,_hg_vec,_hg_vec,_hg_ARTH_VECVEC);
  hg_plopfunc(hg_arthtab,_hg_vec,_hg_float,_hg_ARTH_VECFLOAT);
  hg_plopfunc(hg_arthtab,_hg_float,_hg_vec,_hg_ARTH_FLOATVEC);
  hg_plopfunc(hg_asgtab,_hg_vec,_hg_rel,_hg_ASSIGN_VECREL);

  hg_plopfunc(hg_asgtab,_hg_exp,_hg_float,_hg_ASSIGN_EXPVEC);
  hg_pd->hg_top_node = NULL;
	hg_pd->hg_decl = NULL;
	tail_node = NULL;
}



