# include "slag.h"
# include "../m_alloc.h"

struct flue_slag* flue_slag(void *__prog) {
	struct flue_slag *s;
	s = (struct flue_slag*)m_alloc(sizeof(struct flue_slag));
	s->prog = __prog;
	return s;
}

void flue_slagrun(struct flue_slag *__slg) {
	((void(*)(void*))__slg->prog)(__slg);
}
