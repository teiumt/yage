# include "common.h"
#include "../m_alloc.h"

struct flue_bo* flue_bo_data(_int_u __size) {
	return FLUE_CTX->d->bo_data(FLUE_CTX->dev,__size);
}

void flue_bo_map(struct flue_bo *__buf) {
	FLUE_CTX->d->bo_map(__buf);
}

void flue_bo_unmap(struct flue_bo *__buf) {
	FLUE_CTX->d->bo_unmap(__buf);
}
void flue_bo_destroy(struct flue_bo *__buf) {
	FLUE_CTX->d->bo_destroy(__buf);
}
/*
	intricate buffer object???
	for things like VBO? non striped buffer? ie linear???
*/
