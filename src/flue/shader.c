#include "common.h"
#include "../assert.h"
#include "../ihc/ihc.h"
#include "../m_alloc.h"
#include "../ihc/fsl/fsl.h"
#include "compiler/ima/ima.h"
#include "compiler/ima/radeon.h"
#include "shader.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../assert.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../io.h"
#include "../string.h"
#include "../tools.h"
#include "../ys/as/as.h"
/*
	i have a habbit of giving strings along with there length,
	this is mainly to avoid calling str_len() as its only needed in cases where we need to find the length

	__placement: binding point of symbol
*/
void flue_shader_allot(struct flue_shader *__sh,char const *__ident,_int_u __len,_64_u __type,_64_u __offset,_64_u __placement){
	struct flue_shader_allot *atm;
	atm = m_alloc(sizeof(struct flue_shader_allot));
	atm->next_allotment = __sh->allotment;
	__sh->allotment = atm;
	str_cpy(atm->ident,__ident);
	atm->len				= __len;
	atm->type				= __type;
	atm->offset			= __offset;
	atm->placement	= __placement;
}

void static* _allot(struct flue_shader_allot *__atm) {
	struct fsl_symb *s;
	s = fsl_symb(__atm->ident,__atm->len);
  s->s.em.c.ys = &s->s.em;
	s->s.em.c.comp.comp = _ima_vec_comp_default;
	s->s.ign = -1;
	s->s.em.placement = __atm->placement;
	s->s.em.id = 0;
	s->s.em.offset = __atm->offset;
	s->s.em.val = 4;
	s->s.em.reg = NULL;
	s->s.em.bits = 0;
	s->bits = _fsl_symb_buf;
	if (__atm->type == 3) {
		s->s.em.bits |= IMA_PRIM;
		__atm->type = 1;
	}

	if (__atm->type == 1) {
		s->s.em.bits |= IMA_INTR_K;
	}
	if(__atm->type == 4) {
		s->s.em.ident = _ima_buf;
		s->s.em.bits |= IMA_SAMPLE;
	}else{
	if (__atm->type == 0 || __atm->type == 1) {
		s->s.em.ident = _ima_buf;
	}else {
		s->s.em.placement = FLUE_BUFFER_CONSTANTS;
		//constants
		s->s.em.ident = _ima_buf;
	}
	}
	return s;
}

#include "compiler/ima/r_struc.h"
#include "compiler/ima/ima.h"
_64_u static r_rs_bits[] = {
  [FLUE_RS_BUFFER_VERTEX]     = IMA_INTR_K,
  [FLUE_RS_BUFFER]            = 0,
  [FLUE_RS_BUFFER_PRIM]             = IMA_PRIM
};

struct r_desc _rdesc_table[64] = {
  {
    .val = 0,
    .type = _r_exp_col
  },
  {
		.val = 0,
    .type = _r_exp_vert
  },
  {
		.val = 0,
    .type = _r_exp_par
  },
  {
		.val = 1,
    .type = _r_exp_par
  },
  {
		.val = 2,
    .type = _r_exp_par
  },
  {
		.val = 3,
    .type = _r_exp_par
  },
  {
		.val = 4,
    .type = _r_exp_par
  },
  {
		.val = 5,
    .type = _r_exp_par
  },
  {
    .offset = 0,
    .type = _r_buf_spec
  }
};


void flue_clearslots(void){
	_int_u i;
	i = 0;
	for(;i != 5;i++){
		struct r_desc *r;
		r = _rdesc_table+FLUE_USERSLOT0+i;
		r->type = -1;
	}
}

void flue_rsdesc(_64_u __desc,_64_u __placement,_64_u __bits){
	assert(__desc>=FLUE_USERSLOT0 && __desc<FLUE_USERSLOT4);
	struct r_desc *r;
	r = _rdesc_table+__desc;
	r->val = __placement;
	r->type = 0;
	r->buf_bits = r_rs_bits[__bits];
}

void flue_txdesc(_64_u __desc,_64_u __placement,_64_u __sampler){
	assert(__desc>=FLUE_USERSLOT0 && __desc<FLUE_USERSLOT4);
	struct r_desc *r;
	r = _rdesc_table+__desc;
	r->val = __placement;
	r->type = _r_img_nosample;
	if(__sampler != -1){
		r->type = _r_img_sample;
		r->sampler = __sampler;
	}
}

void flue_shader_new(struct flue_shader *__sh){
	__sh->allotment = NULL;
}
void flue_shader_destroy(struct flue_shader *__sh){

}

void fsl_load(struct flue_shader *__shader, char const *__in) {
	struct fsl_symb *s;
	s = fsl_symb("pos",3);
	s->s.ign = -1;
	s->s.em.exp = 1;
	s->s.em.id = _ima_pos;
	s->s.em.val = 0;
	s->s.em.ident = _ima_exp;
	s->s.em.comp = 0xf;
	s->s.em.reg = NULL;

	s = fsl_symb("col",3);
	s->s.ign = -1;
	s->s.em.id = _ima_col;
	s->s.em.exp = 0;
	s->s.em.val = 0;
	s->s.em.comp = 0xf;
	s->s.em.reg = NULL;
	s->s.em.ident = _ima_exp;

	struct rd_reg r0 = {.rval=2};
		s = fsl_symb("fragpos",7);
		s->s.ign = -1;
		s->s.em.id = 0;
		s->s.em.val = 0;
		s->s.em.comp = 0x7;
		s->s.em.reg = &r0;
		s->s.em.ident = _ima_vec;
		s->s.em.placement = 0;

		s = fsl_symb("innorm",6);
		s->s.ign = -1;
		s->s.em.offset = 0;
		s->s.em.id = 0;
	s->s.em.val = 0;
		s->s.em.comp = 0xf;
		s->s.em.reg = NULL;
		s->s.em.ident = _ima_intrp;
		s->s.em.placement = 0;

	flue_printf("IMA_SH = %p.\n",__shader);
//	rd_sh = __shader->shader;
	/*
		first we need to setup IMA
	*/
	hg_pd = __shader->hg;
	ima_pd = __shader->priv;
	/*
		now we can get FSL to start working
	*/
	fsl_compile(__in);
	__shader->interp_slots = hg_pd->interp_mode;

	ima_ys();
	//takein the nodes -> ima_instr
	ima_takein_hg();
}


int static fd;
_32_u static _read(_ulonglong __ctx, void *__buf, _int_u __size, _8_s *__error) {
  read(fd, __buf, __size);
  as.off+=__size;
}
#include "../assert.h"
static _8_u *buf;
static _int_u off = 0,limit;
_32_u static _write(_ulonglong __ctx, void *__buf, _int_u __size, _8_s *__error) {
  assert(off<limit);
  mem_cpy(buf+off, __buf, __size);
  off+=__size;
}
/*
  NOTE: this function requires the context to be passed.

  purpose: final step into getting device specific code
*/
void ysa_loader(struct ys_mech *__m, struct flu_plarg *__arg, struct ysa_data *__data) {
	as.rw_ops.read = _read;
  as.rw_ops.write = _write;
  fd = open(__arg->p, O_RDONLY, 0);
  if (fd<0) {
    printf("failed to open source file, %s\n", __arg->p);
    return;
  }
  off = 0;

  __data->ptr = buf = m_alloc(512*8);
	limit = 512*8;
  struct stat st;
  fstat(fd, &st);
  as.off = 0;
  as.limit = st.st_size;
  ys_MACHINE(__m);
  ys_init();
  struct ys_state state;
  ysa_assemble(&state);
  ys_de_init();
  close(fd);
  __data->size = off;//state.in_c;
  _int_u i;
  i = 0;
  for(;i != off/4;i++) {
    printf("DW%u: %x\n",i,((_32_u*)buf)[i]);
  }
  /*
    to root out errors

    ysa is error prone
  */
  assert(off<4096);
}

/*
	NOTE: this routine has no part in translating to driver language.
	only from and to intermediat language.
*/
void flue_shader_compile(struct flue_shader *__sh, char const *__path, _int_u __pathlen){
	__sh->hg = m_alloc(sizeof(struct hg_produce));
	__sh->priv = m_alloc(sizeof(struct ima_produce));
	struct ima_produce *m;
	m = __sh->priv;
	hg_pd = __sh->hg;
	ima_pd = __sh->priv;
	fsl_init();
	/*
		we init FSL backend
	*/
	hg_init();
	ima_init();

	struct flue_shader_allot *atm;
	atm = __sh->allotment;
	while(atm != NULL){
		_allot(atm);
		atm = atm->next_allotment;
	}
	char ext[64];
	char c;
	_int_u i = __pathlen,j = 0;
	while((c = __path[i-1]) != '.') {
			ext[j++] = c;
			i--;
	}
	i--;

	if (!j)
			return;//some error

	ffly_fprintf(ffly_err, "compiling shader: %s.\n",__path);
	char *buf = __sh->name;
	mem_cpy(buf,__path,i);
	buf[i] = '\0';

	/*
		we need to tell IMA where to put the output.
	*/
//	str_cpy(m->file.file_params.buf,buf);
//	printf("SHADER_OUTPUT: %s\n",m->file.file_params.buf);
	
	fsl_load(__sh,__path);
}

void flue_shader_dump(struct flue_shader *__sh){
	struct flue_shader_allot *atm;
	atm = __sh->allotment;
	while(atm != NULL){
		printf("FLUE_ALLOT: %s{%u}, %u, %u, %u.\n",atm->ident,atm->len,atm->type,atm->offset,atm->placement);
		atm = atm->next_allotment;
	}

}
