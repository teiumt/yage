#include "fbdev.h"
#include "../assert.h"
#include "../linux/unistd.h"
#include "../linux/ioctl.h"
#include "../linux/fcntl.h"
#include "../linux/mman.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../string.h"
#include "../linux/fb.h"
struct flue_fbdev* fbdev_init(char const *__path) {
	int fb;
	fb = open(__path, O_RDWR, 0);
	if (fb<0) {
		printf("failed to open framebuffer.\n");
		return;
	}
	struct flue_fbdev *d;
	d = m_alloc(sizeof(struct flue_fbdev));


	_int_u fb_len;
	void *fb_pixels;
	ioctl(fb, FBIOGET_VSCREENINFO, &d->fb_info);
	ioctl(fb, FBIOGET_FSCREENINFO, &d->fix);
	fb_len = d->fb_info.yres*d->fix.line_length;
	printf("FRAMEBUFFER: %u, %u. %u, %u",d->fb_info.xres,d->fb_info.yres,d->fix.line_length/4,d->fb_info.yres_virtual);
	fb_pixels = mmap(NULL, fb_len, PROT_READ|PROT_WRITE, MAP_SHARED, fb, 0);
	if (!fb_pixels) {
		return NULL;
	}
	//&fb_len = randomish number
	mem_set(fb_pixels, (_8_u)(&fb_len), fb_len);
	d->d.ptr = fb_pixels;
	d->d.line_length = d->fix.line_length;
	d->fd = fb;
	d->fb_len = fb_len;
	struct flue_fmtdesc fmt;
	fmt.nfmt = FLUE_NFMT(FLUE_NF_SRGB);
	fmt.dfmt = FLUE_DFMT(FLUE_DF_8_8_8_8);
	fmt.depth = 4;
	d->render = flue_tex_new(d->fb_info.xres,d->fb_info.yres,&fmt,0);
	mem_set(d->render->tx.ptr,0,d->fb_info.xres*d->fb_info.yres*4);
	return d;
}

void fbdev_deinit(struct flue_fbdev *__d) {
	munmap(__d->d.ptr, __d->fb_len);
	close(__d->fd);
	m_free(__d);
}

struct flue_fbdev* flue_fbdev(void) {
	struct flue_fbdev *d;
	d = fbdev_init("/dev/fb0");
	FLUE_CTX->draw = d;
	return d;
}

void flue_fbdev_cleanup(void) {
	fbdev_deinit(FLUE_CTX->draw);
}
void fbdev_swapbufs(struct flue_fbdev *__dev) {
	FLUE_CTX->d->rbcopy(__dev,__dev->render,__dev->fb_info.xres,__dev->fb_info.yres);
}
