# include "../y_int.h"
# include "../types.h"
# include "../br.h"
# include "../hexdump.h"
# include "../chrdump.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	_int_u const sz = (1<<_ff_brick_32)*4;
	char buf[sz];
	ffly_br_retrieve(_ff_brick_32, 4, buf);
	ffly_hexdump(buf, sz);
	ffly_chrdump(buf, sz);
}
