#ifndef __bin__tree__h
#define __bin__tree__h
#include "y_int.h"
struct bt_node {
	_64_u userdata[64];

	_8_s alive, null;
	struct bt_node *l,*r, *b, **bb, *fw, **bw;
	_64_u key;
};

struct bin_tree {
	 struct bt_node *root, *top;
};
void bt_show(struct bt_node *__n);
void bt_cleanup(struct bin_tree*);
void bt_init(struct bin_tree *__tree);

void bt_erase(struct bin_tree *__tree, struct bt_node*);
void* bt_find(struct bin_tree *__tree, _64_u __key);
void* bt_embed(struct bin_tree *__tree, _64_u __key);
#endif /*__bin__tree__h*/
