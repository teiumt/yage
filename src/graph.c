#include "graph.h"
#include "strf.h"
void graph_ploty(struct graph *__g,_64_u __y){
	graph_plot(__g,__g->x,__y);
	__g->x++;
}

void graph_plot(struct graph *__g,_64_u __x, _64_u __y) {
	char buf[4096];

	_int_u len;
	len = ffly_strf(buf,0,"%u %u\n",__x,__y);
	bwrite(&__g->f,buf,len);
}

void graph_open(struct graph *__g,char const *__path){
	__g->x = 0;
	bopen(&__g->f,__path,_O_TRUNC|_O_CREAT|_O_WRONLY,_S_IRWXU|_S_IRWXO);
}
void graph_close(struct graph *__g){
	bclose(&__g->f);
}
