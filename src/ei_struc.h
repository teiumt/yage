# ifndef __ffly__ei__struc__h
# define __ffly__ei__struc__h
# include "y_int.h"
# include "ad.h"
struct ei_struc {
	void(*grab)(_ulonglong, _8_u);
	struct ffly_adentry *ad;
};

# endif /*__ffly__ei__struc__h*/
