# ifndef __ffly__drm__mode__h
# define __ffly__drm__mode__h
# include "y_int.h"
# include "drm.h"
#define DRM_DISPLAY_INFO_LEN	32
#define DRM_CONNECTOR_NAME_LEN  32
#define DRM_DISPLAY_MODE_LEN	32
#define DRM_PROP_NAME_LEN   32

struct drm_mode_fb_cmd {
	_32_u fb_id;
	_32_u width;
	_32_u height;
	_32_u pitch;
	_32_u bpp;
	_32_u depth;
	/* driver specific handle */
	_32_u handle;
};

struct drm_mode_fb_cmd2 {
	_32_u fb_id;
	_32_u width;
	_32_u height;
	_32_u pixel_format; /* fourcc code from drm_fourcc.h */
	_32_u flags; /* see above flags */

	/*
	 * In case of planar formats, this ioctl allows up to 4
	 * buffer objects with offsets and pitches per plane.
	 * The pitch and offset order is dictated by the fourcc,
	 * e.g. NV12 (http://fourcc.org/yuv.php#NV12) is described as:
	 *
	 *   YUV 4:2:0 image with a plane of 8 bit Y samples
	 *   followed by an interleaved U/V plane containing
	 *   8 bit 2x2 subsampled colour difference samples.
	 *
	 * So it would consist of Y as offsets[0] and UV as
	 * offsets[1].  Note that offsets[0] will generally
	 * be 0 (but this is not required).
	 *
	 * To accommodate tiled, compressed, etc formats, a
	 * modifier can be specified.  The default value of zero
	 * indicates "native" format as specified by the fourcc.
	 * Vendor specific modifier token.  Note that even though
	 * it looks like we have a modifier per-plane, we in fact
	 * do not. The modifier for each plane must be identical.
	 * Thus all combinations of different data layouts for
	 * multi plane formats must be enumerated as separate
	 * modifiers.
	 */
	_32_u handles[4];
	_32_u pitches[4]; /* pitch for each plane */
	_32_u offsets[4]; /* offset of each plane */
	_64_u modifier[4]; /* ie, tiling, compress */
};

struct drm_mode_modeinfo {
	_32_u clock;
	_16_u hdisplay;
	_16_u hsync_start;
	_16_u hsync_end;
	_16_u htotal;
	_16_u hskew;
	_16_u vdisplay;
	_16_u vsync_start;
	_16_u vsync_end;
	_16_u vtotal;
	_16_u vscan;

	_32_u vrefresh;

	_32_u flags;
	_32_u type;
	char name[DRM_DISPLAY_MODE_LEN];
};

struct drm_mode_crtc {
	_64_u set_connectors_ptr;
	_32_u count_connectors;

	_32_u crtc_id; /**< Id */
	_32_u fb_id; /**< Id of framebuffer */

	_32_u x; /**< x Position on the framebuffer */
	_32_u y; /**< y Position on the framebuffer */

	_32_u gamma_size;
	_32_u mode_valid;
	struct drm_mode_modeinfo mode;
};

struct drm_mode_card_res {
	_64_u fb_id_ptr;
	_64_u crtc_id_ptr;
	_64_u connector_id_ptr;
	_64_u encoder_id_ptr;
	_32_u count_fbs;
	_32_u count_crtcs;
	_32_u count_connectors;
	_32_u count_encoders;
	_32_u min_width;
	_32_u max_width;
	_32_u min_height;
	_32_u max_height;
};
#define DRM_MODE_CONNECTOR_Unknown  0
#define DRM_MODE_CONNECTOR_VGA      1
#define DRM_MODE_CONNECTOR_DVII     2
#define DRM_MODE_CONNECTOR_DVID     3
#define DRM_MODE_CONNECTOR_DVIA     4
#define DRM_MODE_CONNECTOR_Composite    5
#define DRM_MODE_CONNECTOR_SVIDEO   6
#define DRM_MODE_CONNECTOR_LVDS     7
#define DRM_MODE_CONNECTOR_Component    8
#define DRM_MODE_CONNECTOR_9PinDIN  9
#define DRM_MODE_CONNECTOR_DisplayPort  10
#define DRM_MODE_CONNECTOR_HDMIA    11
#define DRM_MODE_CONNECTOR_HDMIB    12
#define DRM_MODE_CONNECTOR_TV       13
#define DRM_MODE_CONNECTOR_eDP      14
#define DRM_MODE_CONNECTOR_VIRTUAL      15
#define DRM_MODE_CONNECTOR_DSI      16
#define DRM_MODE_CONNECTOR_DPI      17
#define DRM_MODE_CONNECTOR_WRITEBACK    18
#define DRM_MODE_CONNECTER_MAX 19

struct drm_mode_get_connector {
	_64_u encoders_ptr;
	_64_u modes_ptr;
	_64_u props_ptr;
	_64_u prop_values_ptr;

	_32_u count_modes;
	_32_u count_props;
	_32_u count_encoders;

	_32_u encoder_id; /**< Current Encoder */
	_32_u connector_id; /**< Id */
	_32_u connector_type;
	_32_u connector_type_id;

	_32_u connection;
	_32_u mm_width;  /**< width in millimeters */
	_32_u mm_height; /**< height in millimeters */
	_32_u subpixel;

	_32_u pad;
};

#define DRM_MODE_PAGE_FLIP_EVENT 0x01
#define DRM_MODE_PAGE_FLIP_ASYNC 0x02
#define DRM_MODE_PAGE_FLIP_TARGET_ABSOLUTE 0x4
#define DRM_MODE_PAGE_FLIP_TARGET_RELATIVE 0x8
#define DRM_MODE_PAGE_FLIP_TARGET (DRM_MODE_PAGE_FLIP_TARGET_ABSOLUTE | \
                   DRM_MODE_PAGE_FLIP_TARGET_RELATIVE)
#define DRM_MODE_PAGE_FLIP_FLAGS (DRM_MODE_PAGE_FLIP_EVENT | \
                  DRM_MODE_PAGE_FLIP_ASYNC | \
                  DRM_MODE_PAGE_FLIP_TARGET)
#define DRM_MODE_CURSOR_BO  0x01
#define DRM_MODE_CURSOR_MOVE    0x02
#define DRM_MODE_CURSOR_FLAGS   0x03


struct drm_mode_cursor {
    _32_u flags;
   	_32_u crtc_id;
   	_32_s x;
    _32_s y;
    _32_u width;
    _32_u height;
    /* driver specific handle */
    _32_u handle;
};

struct drm_mode_cursor2 {
    _32_u flags;
    _32_u crtc_id;
    _32_s x;
    _32_s y;
    _32_u width;
    _32_u height;
    /* driver specific handle */
    _32_u handle;
    _32_s hot_x;
    _32_s hot_y;
};


struct drm_mode_crtc_page_flip {
    _32_u crtc_id;
    _32_u fb_id;
    _32_u flags;
    _32_u reserved;
    _64_u user_data;
};

struct drm_mode_res {
	struct drm_mode_card_res rs;
};

struct drm_mode_ctr {
	struct drm_mode_modeinfo *modes;
	_32_u count_modes;
};
int drmmode_cursor_move(int,_32_u,_32_s,_32_s);
int drmmode_cursor_set(int __fd,_32_u __crtc,_32_u __bo_handle,_32_u __width,_32_u __height);
extern char const *drmmode_cntrtab[DRM_MODE_CONNECTER_MAX];
void drmmode_discrt(struct drm_mode_ctr *__crt);
void drmmode_disrs(struct drm_mode_card_res*);
int drmmode_cursor_set(int,_32_u,_32_u,_32_u,_32_u);
int drmmode_ctr_get(struct drm_mode_ctr *__ctr, int, _32_u);
int drmmode_rs_get(struct drm_mode_res*, int);
int drmmode_crtc_set(int, _32_u, _32_u, _32_u, _32_u, _32_u*, int, struct drm_mode_modeinfo*);
int drmmode_fb_add(int, _32_u, _32_u, _8_u, _8_u, _32_u, _32_u, _32_u*);
int drmmode_fb_rm(int, _32_u);
int drmmode_connector_get(struct drm_mode_get_connector *__cntr, int fd, _32_u __id);
void drmmode_disctr(struct drm_mode_get_connector *);
int drmmode_page_flip(int __fd, int __crtc_id,int __fb_id,_32_u __flags, void *__user_data);
int drm_event_handle(int fd, char *buffer);
# endif /*__ffly__drm__mode__h*/
