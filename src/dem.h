# ifndef __f__dem__h
# define __f__dem__h
# include "iis.h"
# include "avs.h"
# include "rws.h"
/*
	rename

	data emit 

	to deh????
	data emit helper
*/
struct f_dem_gwn;
# include "gwn.h"
struct f_dem_struc {
	void(*dem)(struct f_dem_gwn*);
};

struct f_dem_core {
	void(*in)(_ulonglong, void* , _int_u, _64_u);
	void(*out)(_ulonglong, void* , _int_u, _64_u);
	_ulonglong in_arg, out_arg;
	void(*stat)(_ulonglong, struct f_gwn_stat*);
};

struct f_dem_gwn {
	struct f_dem_core core;
};

struct f_dem {
	struct f_iss *iss;
	struct f_avs *avs;
};
#define F_DEM_CRDMP	0x00
#define F_DEM_HXDMP	0x01
void f_dem(_8_u, struct f_gwn*);
extern struct f_gwn _f_dem_fout;
# endif /*__f__dem__h*/
