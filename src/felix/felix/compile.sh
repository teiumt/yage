rm -f *.o boot0.elf boot.bin
cpp boot0.s -o boot0.S
cpp boot1.s -o boot1.S
cpp boot2.s -o boot2.S
as boot0.S -o boot0.o
as boot1.S -o boot1.o
as boot2.S -o boot2.o

#gcc -O0 -ffreestanding -fno-asynchronous-unwind-tables -c start.c -o start.o -nostdlib -fno-builtin
cd ../felix
sh compile.sh
cd ../fboot
cp ../felix/felix.o .
ld -O0 -s -o boot.elf -T boot0.ld
objcopy -j .text0 -j .bootsig -j .text1 -j .text2 -j .rodata -O binary boot.elf boot.bin
#rm -f boot.img
#cat boot0.bin start.bin >> boot.img
