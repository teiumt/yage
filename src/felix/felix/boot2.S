# 1 "boot2.s"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "boot2.s"
# 1 "boot0.h" 1
# 2 "boot2.s" 2
.code32
# 11 "boot2.s"
.text
_boot:
 movw $16, %ax
 movw %ax, %ds
 movw %ax, %es
 movw %ax, %fs
 movw %ax, %gs
 movw %ax, %ss

 movl $(1024+(1024)), %esp
 movl %esp, %ebp
 subl $4, %esp
  movl $0x000afc00, -4(%ebp)

 movl -4(%ebp), %ebx
 movl $msg, %esi
 movl $msglen, %edx
 call _printf


 movl %cr0, %eax
 btsl $21, %eax
 mov %eax, %cr4


 movl %cr4, %eax
 btsl $5, %eax
 mov %eax, %cr4



 movl $_p_4, %eax
 movl %eax, %cr3

 movl $_p_3, %eax
 shl $11, %eax
 orl $0x03, %eax
 movl %eax, _p_4
 movl $0, _p_4+4

 movl $_p_2, %eax
 shl $11, %eax
 orl $0x03, %eax
 movl %eax, _p_3
 movl $0, _p_3+4

 movl $_p_1, %eax
 shl $11, %eax
 orl $0x03, %eax
 movl %eax, _p_2

 movl $0, _p_2+4
 movl $8, _p_1
 movl $0, _p_1+4

 movl 0xc0000080, %ecx
 rdmsr

 btsl $8, %eax

 wrmsr


 movl %cr0, %eax
 btsl $21, %eax
 movl %eax, %cr0

 jmp _64test
.code64
_64test:


5:
 jmp 5b
.code32
_printf:
0:
 xorl %eax, %eax
 lodsb
 orl $0x300, %eax
 movw %ax, (%ebx)
 addl $2, %ebx
 decl %edx
 cmpl $0, %edx
 jne 0b
 ret
.section .rodata
msg:.ascii "WELCOME TO FBOOT V-0.00             "
.set msglen, . - msg
.bss
.align 4096
.lcomm _p_4,4096
.lcomm _p_3,4096
.lcomm _p_2,4096
.lcomm _p_1,4096
.fill 1024, 1, 0
