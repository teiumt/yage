# ifndef __felix__printf__h
# define __felix__printf__h
# include <stdarg.h>
void printf(char const*, ...);
# endif /*__felix__printf__h*/
