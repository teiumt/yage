#define VIDMEMORY 0xb8000
#define WIDTH 0x44a
#define HSH 4
.data
loc:
.quad 0
line:
.quad 0
.globl outf
.globl newline
.globl clear
.text

/*
    movq $VIDMEMORY, %rbx
    movq %rsi, %rdx
    movq %rdi, %rsi
0:
    xorq %rax, %rax
    lodsb
    orq $0x200, %rax
    movw %ax, (%rbx)
    addq $2, %rbx
    decq %rdx
    cmpq $0, %rdx
    jne 0b
    ret

*/
/*
	rdi - data
	rsi - length
*/
outf: 
	movq loc, %rax
	addq $VIDMEMORY, %rax

	xorq %rbx, %rbx
	movw $0x0200, %cx
0:
	movb (%rdi, %rbx, 1), %cl
	movw %cx, (%rax, %rbx, 2)
	incq %rbx
	cmpq %rbx, %rsi
	jne 0b
	shlq $1, %rsi
	addq %rsi, loc
	ret
newline:
	movq line, %rax
	movq (WIDTH), %rbx
	andq $0xffff, %rbx
	shl $1, %rbx
	
	movq %rbx, %rcx
	shl $HSH, %rcx
	subq %rbx, %rcx
	cmpq %rax, %rcx
	je 0f

	addq %rbx, %rax 
	movq %rax, loc
	addq %rbx, line
	ret
0:
	call clear
	movq $0, loc
	movq $0, line
	ret
clear:
	xorq %rdi, %rdi
	movq (WIDTH), %rbx
	andq $0xffff, %rbx
	shl $HSH, %rbx
	movq $VIDMEMORY, %rcx
0:
	movw $0x0000, (%rcx, %rdi, 2)
	incq %rdi
	cmpq %rbx, %rdi
	jne 0b
	ret
