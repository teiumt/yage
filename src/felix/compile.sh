rm -f *.o *.S
cpp boot/boot0.s -o boot0.S
cpp boot/boot2.s -o boot2.S
as boot0.S -o boot0.o
as boot2.S -o boot2.o

cpp boot.s -o boot.S
as boot.S -o boot.o
as lib.s -o lib.o
cpp video.s -o video.S
as video.S -o video.o

gcc -O0 -mincoming-stack-boundary=4 -ffreestanding -fno-asynchronous-unwind-tables -c start.c -o start.o -nostdlib -fno-builtin
gcc -O0 -mincoming-stack-boundary=4 -ffreestanding -fno-asynchronous-unwind-tables -c printf.c -o printf.o -nostdlib -fno-builtin
ld -r -o felix.o boot.o start.o lib.o video.o printf.o
ld -O0 -s -o felix.elf -T felix.ld 
objcopy -j .text0 -j .bootsig -j .text2 -j .rodata -O binary felix.elf felix.bin
