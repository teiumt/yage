.globl str_len
.globl mem_cpy
.text
/*
	rdi - dest
	rsi - src
	rdx - num
*/
mem_cpy:
	xorq %rax, %rax
0:
	movb (%rsi, %rax, 1), %bl
	movb %bl, (%rdi, %rax, 1)
	incq %rax
	cmpq %rdx, %rax
	jne 0b
	ret
str_len:
	xorq %rax, %rax

	jmp 1f
0:
	incq %rax
1:
	cmpb $0, (%rdi, %rax, 1)
	jne 0b
	ret
