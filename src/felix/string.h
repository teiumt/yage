# ifndef __felix__string__h
# define __felix__string__h
# include "../y_int.h"
void mem_cpy(void*, void*, _64_u);
_64_u str_len(char const*);
# endif /*__felix__string__H*/
