# include "printf.h"
# include "string.h"
# include "video.h"
void printf(char const *__str, ...) {
	va_list args;
	va_start(args, __str);

	char buf[128];
	char *p = __str;
	char *bp;
	_64_u i, ii;
	i = 0;
	ii = 0;
	char c;
	while((c = *(p+i)) != '\0') {
	_back:
		if (c == '%') {
			c = *(p+i+1);
			i+=2;
			switch(c) {
				case 'c':
					buf[ii] = va_arg(args, int);
					ii++;
				break;
				case 's': {
					_int len;
					char const *str = va_arg(args, char const*);
					len = str_len(str);
					mem_cpy(buf+ii, str, len);
					ii+=len;
				break;
				}
			}
		} else {
			if (c == '\n') {
				outf(buf, ii);
				newline();
				i++;
				ii = 0;
				c = *(p+i);
				if (c == '\0')
					goto _end;
				goto _back;	
			}
			bp = buf+ii;
			*bp = c;
	
			ii++;
			i++;
		}
	}
	if (ii>0)
		outf(buf, ii);
_end:
	va_end(args);
}
