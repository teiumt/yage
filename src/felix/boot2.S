# 1 "boot/boot2.s"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "boot/boot2.s"
# 1 "boot/boot0.h" 1
# 2 "boot/boot2.s" 2
# 11 "boot/boot2.s"
.globl _boot0


.code16
.section .data
GDT32:
.quad 0x0000000000000000
GDT32_cs:
.byte 0xff, 0xff, 0x00, 0x00, 0x00, 0b10011011, 0b01001111, 0x00
GDT32_ds:
.byte 0xff, 0xff, 0x00, 0x00, 0x00, 0b10010011, 0b01001111, 0x00

.set GDT32_size, (. - GDT32)
.set GDT32_cs_offset, (GDT32_cs - GDT32)
.set GDT32_ds_offset, (GDT32_ds - GDT32)
GDT32_ptr:
.word GDT32_size
.long GDT32
IDT32_ptr:
.word 0
.long 0
GDT64:
.quad 0x0000000000000000
GDT64_cs:
.byte 0xff, 0xff, 0x00, 0x00, 0x00, 0b10011011, 0b00101111, 0x00
GDT64_ds:
.byte 0xff, 0xff, 0x00, 0x00, 0x00, 0b10010011, 0b00101111, 0x00
.set GDT64_size, (. - GDT64)
.set GDT64_cs_offset, (GDT64_cs - GDT64)
.set GDT64_ds_offset, (GDT64_ds - GDT64)
GDT64_ptr:
.word GDT64_size
.quad GDT64
IDT64_ptr:
.word 0
.quad 0


.text
_boot:
 cli
 lgdt GDT32_ptr
 lidt IDT32_ptr


 movl $0x00000001, %eax
 movl %eax, %cr0

 ljmp $GDT32_cs_offset, $0f
.code32
0:
 movw $GDT32_ds_offset, %ax
 movw %ax, %ds
 movw %ax, %es
 movw %ax, %fs
 movw %ax, %gs
 movw %ax, %ss

 movl $0x1ffff, %esp
 movl %esp, %ebp

 movl $msg, %esi
 movl $msglen, %edx
 call _new_printf



 movl $((0x10000 +0x1000)|(0x00000003)), %eax
 movl %eax, ((0x10000))
 movl $0, ((0x10000)+4)




 movl $((0x10000 +0x2000)|(0x00000003)), %eax
 movl %eax, ((0x10000 +0x1000))
 movl $0, ((0x10000 +0x1000)+4)




 movl $((0x10000 +0x3000)|(0x00000003)), %eax
 movl %eax, ((0x10000 +0x2000))
 movl $0, ((0x10000 +0x2000)+4)

 movl $(0x00000003), %eax
 movl $(0x10000 +0x3000), %edi
0:
 movl %eax, (%edi)
 movl $0, 4(%edi)
 addl $0x1000, %eax
 addl $8, %edi
 cmpl $(0x200000|(0x00000003)), %eax
 jb 0b


 movl %cr4, %eax
 btsl $5, %eax
 movl %eax, %cr4



 movl $(0x10000), %eax
 movl %eax, %cr3

 movl $0xc0000080, %ecx
 rdmsr


 btsl $8, %eax

 wrmsr


 movl %cr0, %eax
 orl $(1<<31), %eax
 movl %eax, %cr0
 lgdt GDT64_ptr
 ljmp $GDT32_cs_offset, $0f
.code64
0:
 movw $GDT64_ds_offset, %ax
    movw %ax, %ds
    movw %ax, %es
    movw %ax, %fs
    movw %ax, %gs
    movw %ax, %ss

 jmp _boot0
.code64
_felix_printf:
 movq $0xb8000, %rbx
 movq %rsi, %rdx
 movq %rdi, %rsi
0:
 xorq %rax, %rax
 lodsb
 orq $0x200, %rax
 movw %ax, (%rbx)
 addq $2, %rbx
 decq %rdx
 cmpq $0, %rdx
 jne 0b
 ret

.code32
_new_printf:
 movl $0xb8000, %ebx
0:
 xorl %eax, %eax
 lodsb
 orl $0x300, %eax
 movw %ax, (%ebx)
 addl $2, %ebx
 decl %edx
 cmpl $0, %edx
 jne 0b
 ret
.section .rodata
msg:.ascii "WELCOME TO FELIX V-0.00"
.set msglen, . - msg
