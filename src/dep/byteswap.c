# include "byteswap.h"
_16_u bswp_16(_16_u __v) {
	register _16_u x = __v;
	register _16_u ret = 0;
	__asm__("rorw $8, %w0" : "=r"(ret) : "0"(x));
	return ret;
}
