# include "system/file.h"
# include "printf.h"
# include "io.h"
# include "string.h"
# include "string.h"
# include "m_alloc.h"
# include "linux/unistd.h"
# include "mutex.h"
# include "lld.h"
// multithread support
// TODO: cleanup
# include "lhs.h"
#define CHUNK_SHIFT 8
#define CHUNK_SIZE (1<<CHUNK_SHIFT)
#define CHUNK_MASK (CHUNK_SIZE-1)
#define CHUNK_UMASK ~(CHUNK_SIZE-1)
/*
	chunk update adds said chunk to a linked list
	thus window size ! how big the list is

	and end is poped off and checked if loaded
	if yes then cache
*/
#define WINDOW_SIZE 7
mlock static lock = FFLY_MUTEX_INIT;

/*
	TODO:
		use direct file ???? 
		create own hard storage allocator cant use 'hard'->wh
		because uses MSG() -> here

		will cause loop and then segfault
		could just disable output but would be more issuematic
*/
void f_io_outlet(struct f_rw_struc*, void*, _int_u);
/*
	TODO:

		lets say user uses printf to print a file then what ????? 
		or we could be limited on memory

		allow for caching of chunk??? or page of chunks???
		# include "../hard.h"
*/
/*

	? why not use a single int???
	cut | cut offset i dont know i think
	its because if needed to write to a specific location
	be would need to do bit operations on it everytime??????
*/
#define CNK_LOADED 0x01
#define BEVY_SIZE 17
struct chunk {
	void *p;
	_8_u flags;
	struct chunk *next;
	struct bevy *b;
	_32_u off;
};
struct platter {
	struct chunk **chunks;
	_16_u cut;
	_8_u off;
	_int_u chunk_c;
	struct chunk *window;
	struct chunk *cutoff;
	_int_u wsize;
	struct bevy *bt;
};
static struct platter *plat;
struct bevy {
	struct f_lhs *h;
	_32_u end, off;
	struct bevy *next;
};

void static _new_bevy(void) {
	struct bevy *b;
	b = (struct bevy*)m_alloc(sizeof(struct bevy));

	b->next = plat->bt;
	plat->bt = b;
	b->h = f_lhs_alloc(b->end = (BEVY_SIZE<<CHUNK_SHIFT));
	b->off = 0;
}

void static _chunk_alloc(struct chunk *__chunk) {
	if (!plat->bt) {
		_new_bevy();
	}
	
	struct bevy *b;
_back:
	b = plat->bt;
	if (b->off == b->end) {
		_new_bevy();
		goto _back;
	}

	__chunk->b = b;
	__chunk->off = b->off;
	b->off+=CHUNK_SIZE;
}

void static
_update_chunk(struct platter *__plat, struct chunk *__c) {
	if (!__plat->cutoff) 
		__plat->cutoff = __c;

	if (__plat->window != NULL) {
		__plat->window->next = __c;
	}

	__c->next = NULL;
	__plat->window = __c;

	_8_u loaded;
	loaded = __c->flags&CNK_LOADED;

	if (__plat->wsize == WINDOW_SIZE && loaded>0) {
		struct chunk *c;
		c = __plat->cutoff;
		__plat->cutoff = c->next;
		c->next = NULL;
		c->flags ^= CNK_LOADED;
		f_lhs_write(c->b->h, c->p, CHUNK_SIZE, c->off);
		m_free(c->p);
		c->p = NULL;
		f_lld_out("unloaded chunk, off: %u\n", c->off);
		plat->wsize--;
	}

	if (!loaded) {
		__c->p = m_alloc(CHUNK_SIZE);
		__c->flags |= CNK_LOADED;
		f_lhs_read(__c->b->h, __c->p, CHUNK_SIZE, __c->off);
		f_lld_out("loaded chunk, off: %u\n", __c->off);
	}
	__plat->wsize++;
}


void static
_read_chunk(struct platter *__plat, struct chunk *__c, void *__buf, _int_u __size, _32_u __offset) {
	_update_chunk(__plat, __c);
	mem_cpy(__buf, ((_8_u*)__c->p)+__offset, __size);
}

void static
_read_chunkf(struct platter *__plat, struct chunk *__c, _int_u __size, _32_u __offset, void(*__out)(_ulonglong, void*, _int_u), _ulonglong __arg) {
	_8_u cbuf[CHUNK_SIZE];
	_read_chunk(__plat, __c, cbuf, __size, __offset);
	__out(__arg, cbuf, __size);
}

void static
drain(_8_u *__p, _16_u __size, _16_u __cut, _8_u __off) {
	_8_u *p = __p;
	_8_u *end = p+__size;

	_int_u end_off = (__cut<<CUT_SHIFT)+__off+__size;
	_int_u cc = (end_off>>CHUNK_SHIFT)+((end_off&((~(_64_u)0)>>(64-CHUNK_SHIFT)))>0);

	if (cc>plat->chunk_c) {
		struct chunk **chunks;
		chunks = plat->chunks;
		if (!chunks)
			chunks = (struct chunk**)m_alloc(cc*sizeof(struct chunk*));
		else
			chunks = (struct chunk**)m_realloc(chunks, cc*sizeof(struct chunk*));
		if (!chunks) {
			// error
		}


		// could just use memset
		struct chunk **c = chunks+plat->chunk_c;
		struct chunk **end = chunks+cc;
		while(c != end)
			*(c++) = NULL;
		plat->chunk_c = cc;
		plat->chunks = chunks;
	}

	while(p != end) {
		_int_u off = ((__cut<<CUT_SHIFT)+__off)+(p-__p);
		_int_u chunk = off>>CHUNK_SHIFT;

		_int_u chunk_off = off-(chunk<<CHUNK_SHIFT);
		_int_u left = CHUNK_SIZE-chunk_off;
		struct chunk **c = plat->chunks+chunk;
		struct chunk *cp;
		if (!(cp = *c)) {
			cp = *c = m_alloc(sizeof(struct chunk));
			cp->p = m_alloc(CHUNK_SIZE);
			cp->flags = CNK_LOADED;
			cp->next = NULL;
			cp->b = NULL;
			_chunk_alloc(cp);

			// only for debug
			mem_set(cp->p, '@', CHUNK_SIZE);
		}

		_int_u size = __size-(p-__p);

		if (left<size)
			size = left;

		mem_cpy(((_8_u*)cp->p)+chunk_off, p, size);
		p+=size;
	}
}
#include "y.h"
_f_err_t printf(char const *__format, ...) {
	if (y_bits&Y_PRINTF_DISABLE)return 0;
	va_list args;

	va_start(args, __format);
	ffly_vfprintf(ffly_out, __format, args); 
	va_end(args);

	return 0;

}

_f_err_t vprintf(char const *__format,va_list __args){
	if(y_bits&Y_PRINTF_DISABLE)return 0;
	ffly_vfprintf(ffly_out, __format,__args); 
	return 0;
}

_f_err_t ffly_fprintf(struct f_rw_struc *__rws, char const *__format, ...) {
	va_list args;
	va_start(args, __format);
	ffly_vfprintf(__rws, __format, args);
	va_end(args);
}

_f_err_t ffly_vfprintf(struct f_rw_struc *__rws, char const *__format, va_list __args) {
	_int_u l = str_len(__format);
	ffly_vsfprintf(__rws, l, __format, __args);
}

char const *NULL_STRING = "(NULL)";
_int_u static
gen(_f_size_t __n, char const *__format, va_list __args) {
	char const *p = __format;
	char const *start;
	_int_u nc;
	nc = 0;
	while(p != __format+__n) {
		if (*(p++) == '%') {
			if (nc>0) {
				drain(start-nc, nc, plat->cut, plat->off);
				plat->cut+=nc>>CUT_SHIFT;
				plat->off+=nc&CUT_MASK;
				nc = 0;
			}
			_8_u is_long;
			if (is_long = (*p == 'l')) p++; 
			if (*p == 'd') {
				_64_i v = is_long? va_arg(__args, _64_i):va_arg(__args, _32_i);
				_8_u neg;
				if ((neg = (v < 0))) {
					char c = '-';
					drain(&c, 1, plat->cut, plat->off++);
					v = -v;
				} 
				_ffly_nots(v, &plat->cut, &plat->off, drain);
			} else if (*p == 'u') {
				_64_u v = is_long?va_arg(__args, _64_u):va_arg(__args, _32_u);
				_ffly_nots(v, &plat->cut, &plat->off, drain);
			} else if (*p == 's') {
				char *s = va_arg(__args, char*);
				if(!s){
					s = NULL_STRING;
				}
				_int_u l = str_len(s);
				drain(s, l, plat->cut, plat->off);
				plat->cut+=l>>CUT_SHIFT;
				plat->off+=l&CUT_MASK;
			} else if (*p == 'c') {
				// TODO if we get large amount of %c then buffer them and then drain
				char c = va_arg(__args, int);
				drain(&c, 1, plat->cut, plat->off++);
			} else if (*p == 'f') {
				double v = va_arg(__args, double);
				_ffly_floatts(v, &plat->cut, &plat->off, drain);
			} else if (*p == 'p') {
				void *v = va_arg(__args, void*);
				if (!v) {
					drain("(NULL)", 6, plat->cut, plat->off);
					plat->cut+=6>>CUT_SHIFT;
					plat->off+=6&CUT_MASK;
				} else
					_ffly_noths((_64_u)v, &plat->cut, &plat->off, drain);
			} else if (*p == 'x') {
				_64_u v = is_long? va_arg(__args, _64_u):va_arg(__args, _32_u);
				_ffly_noths(v, &plat->cut, &plat->off, drain);
			}else
			if (*p == 'w') {
				char *s = va_arg(__args, char*);
				_int_u l = va_arg(__args, char*);
				drain(s, l, plat->cut, plat->off);
				plat->cut+=l>>CUT_SHIFT;
				plat->off+=l&CUT_MASK;
			}
			p++;
		} else {
			nc++;
			start = p;
		}

		if (plat->off>=CUT_SIZE) {
			plat->cut++;
			plat->off-=CUT_SIZE;
		}
	}
	if (nc>0) {
		drain(start-nc, nc, plat->cut, plat->off);
		plat->cut+=nc>>CUT_SHIFT;
		plat->off+=nc&CUT_MASK;
	}

	char c = '\0';
	drain(&c, 1, plat->cut, plat->off);
	return (plat->cut<<CUT_SHIFT)+plat->off;
}

struct platter static*
plat_new(void) {
	struct platter *plt;
	plt = (struct platter*)m_alloc(sizeof(struct platter));
	plt->chunks = NULL;
	plt->chunk_c = 0;
	plt->cut = 0;
	plt->off = 0;
	plt->window = NULL;
	plt->cutoff = NULL;
	plt->wsize = 0;
	plt->bt = NULL;
	return plt;
}

void static
plat_destroy(struct platter *__plat) {
	if (__plat->chunks != NULL) {
		struct chunk **c = __plat->chunks;
		struct chunk **end = c+__plat->chunk_c;
		struct chunk *cp;
		while(c != end) {
			cp = *c;
			if (cp->p != NULL)
				m_free(cp->p);
			m_free(cp);
			c++;
		}
	
		m_free(__plat->chunks);
	}
	
	struct bevy *b, *bb;
	b = __plat->bt;
	while(b != NULL) {
		bb = b;
		b = b->next;
		f_lhs_free(bb->h);
		m_free(bb);
	}
	
	m_free(__plat);
}

_int_u ffly_vsprintf(char *__buf, char const *__format, va_list __args) {
	struct platter *_plat;
	mt_lock(&lock);
	_int_u l;
	plat = plat_new();
	l = gen(str_len(__format), __format, __args)+1;
	_plat = plat;
	mt_unlock(&lock);
	struct chunk **c = _plat->chunks;
	struct chunk **end = c+(l>>CHUNK_SHIFT);
	char *p = __buf;
	while(c != end) {
		_read_chunk(_plat, *c, p, CHUNK_SIZE, 0);
		p+=CHUNK_SIZE;
		c++;
	}

	_int_u left = l&CHUNK_MASK;
	if (left>0)
		_read_chunk(_plat, *c, p, left, 0);
	plat_destroy(_plat);
	return l;
}

_int_u ffly_sprintf(char *__buf, char const *__format, ...) {
	va_list args;
	_int_u l;
	va_start(args, __format);
	l = ffly_vsprintf(__buf, __format, args);
	va_end(args);
	return l;
}

_int_u static out(_ulonglong, _f_size_t, char const*, va_list, void(*)(_ulonglong, void*, _int_u));
_int_u ffly_fprintfs(struct f_rw_struc *__rws, char const *__format, ...) {
	va_list args;
	_int_u l;
	va_start(args, __format);
	l = out((_ulonglong)__rws, str_len(__format), __format, args, (void*)f_io_outlet);
	va_end(args);
	return l;
}

_f_err_t ffly_vsfprintf(struct f_rw_struc *__rws, _f_size_t __n, char const *__format, va_list __args) {
	out((_ulonglong)__rws, __n, __format, __args, (void*)f_io_outlet);
}

_int_u
out(_ulonglong __arg, _f_size_t __n, char const *__format, va_list __args, void(*__write)(_ulonglong, void*, _int_u)) {
	struct platter *_plat;
	mt_lock(&lock);
	plat = plat_new();
	_int_u l;
	l = gen(__n, __format, __args);
	_plat = plat;
	mt_unlock(&lock);

	struct chunk **c = _plat->chunks;
	struct chunk **end = c+(l>>CHUNK_SHIFT);

	while(c != end) {
		_read_chunkf(_plat, *c, CHUNK_SIZE, 0, __write, __arg);
		c++;
	}

	_int_u left = l&CHUNK_MASK;
	if (left>0)
		_read_chunkf(_plat, *c, left, 0, __write, __arg);

	plat_destroy(_plat);
}


// until fixed
void static
_dummy(_ulonglong __arg, void *__buf, _int_u __size) {
	((void(*)(void*, _int_u))__arg)(__buf, __size);
}

void ffly_printin(char const *__format, va_list __args, void(*__write)(void*, _int_u), void(*__prep)(_int_u)) {
	struct platter *_plat;	
	mt_lock(&lock);
	plat = plat_new();
	_int_u l;
	l = gen(str_len(__format), __format, __args);
	_plat = plat;
	mt_unlock(&lock);
	__prep(l);

	
	struct chunk **c = _plat->chunks;
	struct chunk **end = c+(l>>CHUNK_SHIFT);
	while(c != end) {
		_read_chunkf(_plat, *c, CHUNK_SIZE, 0, _dummy, (_ulonglong)__write);
		c++;
	}

	_int_u left = l&CHUNK_MASK;
	if (left>0)
		_read_chunkf(_plat, *c, left, 0, _dummy, (_ulonglong)__write);

	plat_destroy(_plat);
}

/*
int main() {
	ffly_io_init();
	_8_u x;
	ffly_fprintf(ffly_out, "test: %p\n", &x);
	ffly_io_closeup();
}*/

