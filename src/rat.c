# include "rat.h"
# include <stdarg.h>
# include "system/io.h"
# include "system/file.h"

/*
	TODO:
		extreme buffering
*/
/*
	like log but outputs everything - only used for allocr at the time 
*/
_8_u static level = _ff_rat_2;

void ff_rat_put(_8_u __level) {
	level = __level;
}

void ff_rat(_8_u __level, void(*__out)(char const*, va_list), char const *__format, ...) {
	if (__level != level) return;
	va_list args;
	va_start(args, __format);
	__out(__format, args);
	va_end(args);
}
