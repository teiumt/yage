#include "common.h"
#include "../io.h"
#include "../assert.h"
/*
	software cursor
*/
void static drawrectbm(struct vk_scrn *__s, _8_u *__pixels, _int_u __width, _int_u __height, _int_u __soff, _int_u __doff, _int_u __w) {
	_8_u s, *d;
	_int_u x,y;
	y = 0;
	for(;y != __height;y++) {
		x = 0;
		for(;x != __width;x++) {
			s = __pixels[x+(y*__w)+__soff];
			d = __s->d->pixels+((x*4)+(y*__s->d->line_length)+__doff);
			if (s) {
			d[0] = s;
			d[1] = s;
			d[2] = s;
			d[3] = s;
			}
		}
	}
}

void static drawrect(struct vk_scrn *__s, _8_u *__pixels, _int_u __width, _int_u __height, _int_u __soff, _int_u __doff, _int_u __w) {
	_8_u *s, *d;
	_int_u x,y;
	y = 0;
	for(;y != __height;y++) {
		x = 0;
		for(;x != __width;x++) {
			s = __pixels+((x+(y*__w))*4)+__soff;
			d = __s->d->pixels+((x*4)+(y*__s->d->line_length))+__doff;
			*(_32_u*)d = *(_32_u*)s;
		}
	}

}


void static readpixs(struct vk_scrn *__s, _8_u *__pixels, _int_u __width, _int_u __height, _int_u __soff, _int_u __doff, _int_u __w) {
	_8_u *s, *d;
	_int_u x,y;
	y = 0;
	for(;y != __height;y++) {
		x = 0;
		for(;x != __width;x++) {
			d = __pixels+((x+(y*__w))*4)+__doff;
			s = __s->d->pixels+((x*4)+(y*__s->d->line_length))+__soff;
			*(_32_u*)d = *(_32_u*)s;
		}
	}
}

_8_s static inreach(struct vk_scrn *__s,_int_u __x, _int_u __y, _int_u __width, _int_u __height) {
	if (__x+__width<=__s->d->width) {
		if (__y+__height<=__s->d->height) {
			return 0;
		}
	}

	return -1;
}

void static
_drawrect(struct vk_scrn *__s, _8_u *__pixels, _int_u __width, _int_u __height, _int_u __x, _int_u __y) {
	if (!inreach(__s,__x,__y,__width,__height)) {
		drawrect(__s,__pixels,__width,__height,0,(__x+(__y*__s->d->width))*4,__width);
	}else if (__x+__width>__s->d->width && __y+__height<=__s->d->height){
		//adjacent screen
		struct vk_scrn *adj;
		adj = __s->bod[MR_SCRN_BOD];
		if (!adj)
			return;

		_int_u w;
		w = __s->d->width-__x;
		drawrect(__s,__pixels,w,__height,0,(__x+(__y*__s->d->width))*4,__width);
		drawrect(adj,__pixels,__width-w,__height,w*4,(__y*adj->d->width)*4,__width);
	}
}


void static
_drawrectbm(struct vk_scrn *__s, _8_u *__pixels, _int_u __width, _int_u __height, _int_u __x, _int_u __y) {
	if (!inreach(__s,__x,__y,__width,__height)) {
		drawrectbm(__s,__pixels,__width,__height,0,(__x+(__y*__s->d->width))*4,__width);
	} else if (__x+__width>__s->d->width && __y+__height<=__s->d->height){
		struct vk_scrn *adj;
		adj = __s->bod[MR_SCRN_BOD];
		if (!adj)
			return;
		
		_int_u w;
		w = __s->d->width-__x;
		drawrectbm(__s,__pixels,w,__height,0,(__x+(__y*__s->d->width))*4,__width);
		drawrectbm(adj,__pixels,__width-w,__height,w,(__y*adj->d->width)*4,__width);
	}
}


void static
_readpixs(struct vk_scrn *__s, _8_u *__pixels, _int_u __width, _int_u __height, _int_u __x, _int_u __y) {
	if (!inreach(__s,__x,__y,__width,__height)){
		readpixs(__s,__pixels,__width,__height,(__x+(__y*__s->d->width))*4,0,__width);
	}else if (__x+__width>__s->d->width && __y+__height<=__s->d->height){
		struct vk_scrn *adj;
		adj = __s->bod[MR_SCRN_BOD];
		if (!adj)
			return;
	
		_int_u w;
		w = __s->d->width-__x;
		readpixs(__s,__pixels,w,__height,(__x+(__y*__s->d->width))*4,0,__width);
		readpixs(adj,__pixels,__width-w,__height,(__y*adj->d->width)*4,w*4,__width);
	}

}

/*
	drawpixels using software
*/
void vk_drawpixs(void *__junk, struct vk_drawable *__dst, _8_u *__src, _int_u __width, _int_u __height, _int_u __soff, _int_u __doff, _int_u __w) {
	_8_u *s, *d;
	_int_u x,y;
	y = 0;
	for(;y != __height;y++) {
		x = 0;
		for(;x != __width;x++) {
			s = __src+((x+(y*__w))*4)+__soff;
			d = __dst->pixels+((x*4)+(y*__dst->line_length))+__doff;
			d[0] = s[0];
			d[1] = s[1];
			d[2] = s[2];
			d[3] = s[3];
		}
	}
}
#define W 8
#define H 8
/*
	left hand rule.

	the owner of the cursor is where to coords are so the left side.

	if we are in a transissnal stage moving from one screen to the next
	the left screen is the owner of the cursor so "cursor->s" is equal this.

_____________________________________________________

	lets say the cursor coords are within screen(0)
	
	and we move by an increment of 8-px along the x-axis
	one moment we are in screen(0) now we are in screen(1)
	with no transission.

	transission = is only invoked if we move 1-px per time step

	to to complencate for this we need to fill in the last ocupied space from the previous screen
	and init the cursor with new cursor buf.

	if this is not done the cursor may lingure in the previous screen it was in.


	issue:
		is we used normal be would end up drawing the cursor buffer to the wrong screen and such.
*/
/*
	we leave a screen and fillin the missing space that cursor took up,

	writing the cursor buffer accomplishes one thing... ensuring the cursor is erased and dosent lingure on the screen
*/
void vk_leavescreen(struct vk_cursor *__c) {
	_drawrect(__c->s,__c->cursorbuf,W,H,__c->cs_x,__c->cs_y);
}
/*
	first pass
*/
void vk_cursor_sw0(struct vk_cursor *__c, _int_s __x, _int_s __y) {
	/*
		cursorbuf remnants are unneeded
	*/
	_readpixs(__c->s,__c->cursorbuf,W,H,__x,__y);

	_drawrectbm(__c->s,__c->img->bitmap,W,H,__x,__y);
	__c->cs_x = __x;
	__c->cs_y = __y;
}

void vk_cursor_sw(struct vk_cursor *__c, _int_s __x, _int_s __y) {
	if (__c->cs_x == __x && __c->cs_y == __y) {
		return;
	}
	/*
		if the cursor has moved then its last position needs to be refilled n
	*/
	_drawrect(__c->s,__c->cursorbuf,W,H,__c->cs_x,__c->cs_y);


	/*
		we are going to place the cursor so read pixels we are writing over
	*/
	_readpixs(__c->s,__c->cursorbuf,W,H,__x,__y);
	/*
		draw cursor at new position
	*/
	
	_drawrectbm(__c->s,__c->img->bitmap,W,H,__x,__y);
	__c->cs_x = __x;
	__c->cs_y = __y;
}
void pt_readback(struct vk_patch *__pt) {
	_readpixs(__pt->s,__pt->backbuf->pixels,__pt->backbuf->width,__pt->backbuf->height,__pt->x,__pt->y);
}

void pt_clear(struct vk_patch *__pt) {
	_drawrect(__pt->s,__pt->backbuf->pixels,__pt->d->width,__pt->d->height,__pt->ex,__pt->ey);
}

#include "../const.h"
void vk_gstart(struct vk_scrn *__s) {
}

void vk_gend(void) {

}

void pt_draw(struct vk_patch *__pt) {

//	__pt->s->g->copyarea(&__pt->s->d,&__pt->d,__pt->d->width,__pt->d->height,__pt->x,__pt->y);
}

