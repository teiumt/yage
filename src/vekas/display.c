#include "common.h"
#define PERF_W 1920
#define PERF_H 1080
struct vk_display_mode* vk_findbestmode(struct vk_display_mode *__modes, _int_u __n) {
	struct vk_display_mode *perf = __modes, *m;
	_int_u i;
	i = 1;
	for(;i != __n;i++) {
		m = __modes+i;
		printf("mode: {%u, %ux%u} vs {%u, %ux%u}.\n", m->vrefresh,m->hdisplay,m->vdisplay,perf->vrefresh,perf->hdisplay,perf->vdisplay);
		if (m->vrefresh>=perf->vrefresh) {
			if ((PERF_W-m->hdisplay)<=(PERF_W-perf->hdisplay)) {
				if ((PERF_H-m->vdisplay)<(PERF_H-perf->vdisplay)) {
					perf = m;
				}
			}
		}
	}
	return perf;
}
