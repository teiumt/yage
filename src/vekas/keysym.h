#ifndef __keysym__h
#define __keysym__h
#define VK_KEY_a		0
#define VK_KEY_b		1
#define VK_KEY_c		2
#define VK_KEY_d		3
#define VK_KEY_e		4
#define VK_KEY_f		5
#define VK_KEY_g		6
#define VK_KEY_h		7
#define VK_KEY_i		8
#define VK_KEY_j		9
#define VK_KEY_k		10
#define VK_KEY_l		11
#define VK_KEY_m		12
#define VK_KEY_n		13
#define VK_KEY_o		14
#define VK_KEY_p		15
#define VK_KEY_q		16
#define VK_KEY_r		17
#define VK_KEY_s		18
#define VK_KEY_t		19
#define VK_KEY_u		20
#define VK_KEY_v		21
#define VK_KEY_w		22
#define VK_KEY_x		23
#define VK_KEY_y		24
#define VK_KEY_z		25

#define VK_KEY_A		26
#define VK_KEY_B		27
#define VK_KEY_C		28
#define VK_KEY_D		29
#define VK_KEY_E		30
#define VK_KEY_F		31
#define VK_KEY_G		32
#define VK_KEY_H		33
#define VK_KEY_I		34
#define VK_KEY_J		35
#define VK_KEY_K		36
#define VK_KEY_L		37
#define VK_KEY_M		38
#define VK_KEY_N		39
#define VK_KEY_O		40
#define VK_KEY_P		41
#define VK_KEY_Q		42
#define VK_KEY_R		43
#define VK_KEY_S		44
#define VK_KEY_T		45
#define VK_KEY_U		46
#define VK_KEY_V		47
#define VK_KEY_W		48
#define VK_KEY_X		49
#define VK_KEY_Y		50
#define VK_KEY_Z		51


#define VK_BTN_LEFT     52
#define VK_BTN_RIGHT    53


#define VK_ESC			54
#define VK_KEY_SPACE	55
#define VK_KEY_LEFTSHIFT	56
#define VK_KEY_UP		57
#define VK_KEY_DOWN		58
#define VK_KEY_LEFT		59
#define VK_KEY_RIGHT	60

#define VY_F1			61
#define VY_F2			62
#define VY_F3			63
#define VY_F4			64
#define VY_F5			65
#define VY_F6			66
#define VY_F7			67
#define VY_F8			68
#define VY_F9			69
#define VY_F10			70
#define VY_F11			71
#define VY_F12			72
#define VY_KEY_BACKSPACE			73

#define VY_DOT		74
#define VY_SLASH			75
#endif /*__keysym__h*/
