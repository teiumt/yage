#include "common.h"
#include "../ffly_def.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../linux/time.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../string.h"
#include "../linux/ioctl.h"
#include "../linux/vt.h"
#include "../linux/kd.h"
#include "../linux/stat.h"
#include "../linux/types.h"
#include "../linux/socket.h"
#include "../inet.h"
#include "../linux/errno-base.h"
#include "../linux/un.h"
#include "../linux/unistd.h"
#include "../assert.h"
#include "../linux/net.h"
#include "../linux/in.h"
#include "../in.h"
#include "../drm.h"
#include "../lib/hash.h"
#include "hw/amdgpu/amdgpu.h"
#include "../linux/ipc/shm.h"
struct vk_common vk_com;
struct vk_context vk_ctx = {
	NULL, 0, 0, NULL
};
void *cursor_buffer[8];
static struct f_lhash devtab;
void fbdev_cursor(void);
void fbdev_delete(void);
void vk_amdgpu_cursor_move(struct amdgpu_ent *ent, struct vk_crtc *__crtc, _32_s __x, _32_s __y);
void vk_amdgpu_cursor_show(struct amdgpu_ent*,struct vk_crtc *__crtc);
void vk_amdgpu_pageflip(struct vk_scrn *__s);
void *vk_amdgpu_bo_new(void*,void*,_int_u,_int_u);
void *vk_amdgpu_fb_new(void*,void*,void*,_int_u,_int_u);
void vk_amdgpu_fb_destroy(void*);
void vk_amdgpu_cursor_load(struct amdgpu_ent*, struct vk_cursor*);
struct vk_drv vk_drvtab[2] = {
	{
		NULL,
		NULL,
		NULL,
		fbdev_cursor,
		fbdev_delete,
		NULL
	},
	{
		vk_amdgpu_bo_new,
		vk_amdgpu_fb_new,
		vk_amdgpu_fb_destroy,
		NULL,
		NULL,
		NULL,
		vk_amdgpu_crtc_mode_set,
		vk_amdgpu_get_modes,
		vk_amdgpu_pageflip,
		vk_amdgpu_cursor_show,
		vk_amdgpu_cursor_load,
		vk_amdgpu_cursor_move
	}
};
void vk_devadd(void *__id,_int_u __len,struct vk_dev *__dev) {
	f_lhash_put(&devtab,__id,__len, __dev);
}

struct vk_dev *vk_devloopup(_32_u __id) {
	char card[8];
	str_cpy(card,"card");
	card[4] = '0'+__id;
	/*get rid of this eyesore*/
	return f_lhash_get(&devtab,card,5);
}

struct vk_scrn vk_scrn_clear = {
	{0},
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	0,
	0,
	0,
	MUTEX_INIT,
	.bod = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL}
};
#define _ 0xff,0xff,0xff,0xff
#define X 0,0,0,0
#define Y 0,0,0,0xff


_8_u static cursors[2][8*8*4] = {
	{
		_, _, _, _, _, X, X, X,
		_, _, _, _, X, X, X, X,
		_, _, _, _, X, X, X, X,
		_, _, _, _, _, X, X, X,
		_, X, X, _, _, _, X, X,
		X, X, X, X, _, _, _, X,
		X, X, X, X, X, _, _, _,
		X, X, X, X, X, X, _, _
	},
	{
		Y,Y,Y,Y,X,X,X,X,
		Y,_,_,_,Y,Y,X,X,
		Y,_,_,_,_,_,Y,X,
		Y,_,_,_,Y,Y,X,X,
		Y,_,_,_,_,Y,X,X,
		X,Y,Y,_,_,_,Y,X,
		X,X,X,Y,_,_,_,Y,
		X,X,X,X,Y,Y,Y,Y
	}
};

struct bitimage cursor_images[] = {
	{8,8,cursors[1]}
};

struct vk_cursor static cursor = {
	0, 0, 0,0,cursor_images,.id = 0
};

/*
	temp
*/
static _int_u xcor = 0;
void vk_scrn_newcords(struct vk_scrn *__s,_int_u __width, _int_u __height) {
	__s->scrn.x = xcor;
	__s->scrn.y = 0;
	xcor+=__width;
	if (__s->scrn.x>0) {
		__s->bod[ML_SCRN_BOD] = getscrn(__s->scrn.x-1,0);
		__s->bod[ML_SCRN_BOD]->bod[MR_SCRN_BOD] = __s;

/*
		for testing/debugging as dont have any more monitors
*/
	//	__s->bod[BT_SCRN_BOD] = __s->bod[ML_SCRN_BOD];
	//	__s->bod[ML_SCRN_BOD]->bod[TP_SCRN_BOD] = __s;
	}
}

#include "../time.h"
#include "../im.h"
#include "flue/flue.h"
void vk_init(void) {
		vk_log_init();
		f_lhash_init(&devtab);
}

void vk_probe(void) {
	vk_init();
	vk_amdgpu_sweep(&vk_ctx);
	vf_init();

	struct vk_drv *drv;
	drv = vk_drvtab+1;
	drv->cursor_load(drv->devs->priv, &cursor);

	_int_u i;
	/*
		setup all screens 
	*/
	i = 0;
	for(;i != 1;i++) {
		drv = vk_drvtab+i+1;
		_int_u j;
		j = 0;
		for(;j != drv->ndevs;j++) {
			struct vk_dev *dv;
			dv = drv->devs+j;
			_int_u crtc_n = 0;
			_int_u ot;
			ot = 0;
			/*
				check each output/connector
			*/
			for(;ot != dv->nouts;ot++) {
				struct vk_output *out = dv->out+ot;
				_int_u nmods;
				struct vk_display_mode *modes, *m;
				modes = drv->get_modes(dv->priv0,out,&nmods);
				/*
					most likly is that the output is not connected to anything :)
				*/
				if (!modes) {
					vk_printf("error had occured, dont worry is not critical.\n");
					continue;
				}
				/*
					select best mode because thats the best we can do for now
				*/
				m = vk_findbestmode(modes,nmods);
				vk_printf("selected mode: %ux%u.\n",m->hdisplay,m->vdisplay);
				struct vk_output **outlist[1];
				*outlist = out;
				/*	
					chose top mode as for now
				*/
				_int_u w,h;
				w = m->hdisplay;
				h = m->vdisplay;

				/*
					create buffer object and fb for crtc
				*/
				void *bo,*fb;
				//stays here
				bo = drv->bo_new(dv->priv,dv->priv0,w,h);

				fb = drv->fb_new(dv->priv,dv->priv0,bo,w,h);
				struct vk_crtc *crtc = dv->crts+crtc_n;			
				
				drv->crtc_mode_set(
					dv->priv,
					dv->priv0,
					crtc,
					m,outlist,
					1,
					fb
				);
				crtc_n++;
				vk_printf("device: %p, %p, modes: %p\n", dv->priv, dv->priv0,m);	
			}
		}
	}

//	vk_fbdev_sweep(&vk_ctx);
	evdev_init();
	vk_ctx.cursor = &cursor;
	int tty;
	tty = open("/dev/tty0", O_RDWR, 0);
	if (tty<0) {

		return;
	}
	int r;
	int vt;
	/*
	r = ioctl(tty, VT_OPENQRY, &vt);
	if (r<0) {
		printf("failed vt-openqry.\n");
	}
	
	r = ioctl(tty, VT_ACTIVATE, vt);
	if (r<0) {
		printf("failed vt-activate.\n");
	}
	r = ioctl(tty, VT_WAITACTIVE, vt);
	if (r<0) {
		printf("failed vt-wait.\n");
	}
*/	
	r = ioctl(tty, KDSETMODE, KD_GRAPHICS);
	if (r<0) {
		printf("kdsetmode error.\n");
	}
	close(tty);
}

void vk_deinit(void) {
	struct vk_scrn *sc;
	sc = vk_ctx.screens;
	while(sc != NULL) {
		sc->drv->delete(sc);
		sc = sc->scrn.next;
	}

	evdev_deinit();
}
#include "../sched.h"
#include "subs.h"
#include "../time.h"

#include "../linux/futex.h"
#include "../mutex.h"
#include "../cradle.h"
/*
	any side jobs we cant complete???
*/
#include "../thread.h"
#include "../assert.h"
void vk_cursor_sw(struct vk_cursor *__c, _int_u __x, _int_u __y);
void vk_cursor_sw0(struct vk_cursor *__c, _int_u __x, _int_u __y);
#include "../clock.h"
static struct vk_patch *g_pt;
static _8_s griped = -1;
static _8_s running = 0;	
_32_u static maj_sig;
_8_s static run = -1;
#include "../ffly_def.h"
sched_entityp static __FORCE_INLINE__ submit_sched(_8_s(*__func)(_ulonglong)){
	sched_entityp ent;
	ent = sched_new();
	ent->type = _sched_just_run;
	ent->arg = 0;
	ent->func = __func;
	sched_submit(ent);
	return ent;
}
static tstrucp maj_th;

void static screen_swap(struct vk_scrn *s) {
	_32_u tmp;
	tmp = s->fb->front_id;
	s->fb->front_id = s->fb->back_id;
	s->fb->back_id = tmp;
	s->drv->page_flip(s);
}

void static dorender(void){
	mt_lock(&vk_ctx.drawlock);
	_int_u i;
	i = 0;
	struct vk_scrn *s;
	struct vk_patch *pt;
	pt = vk_ctx.draw_top;
	while(pt != NULL){
		s = pt->s;

		if(!(s->crtc->bits&CRTC_PAGEIDLE)){
			mt_unlock(&vk_ctx.drawlock);
			return;
		}
		if(pt == vk_ctx.draw_top){
			vf_clear(s->d,1,1,1,1);
		}
		vk_printf("proccessing pending draw, %u, %u, %u %u.\n",pt->x,pt->y,pt->d->width,pt->d->height);
		struct timespec start,stop;
		clock_gettime(CLOCK_MONOTONIC,&start);

		mt_lock(&pt->lock);
		vf_copyarea(pt->s->d,pt->d,pt->x,pt->y,pt->d->width,pt->d->height);
		mt_unlock(&pt->lock);
		clock_gettime(CLOCK_MONOTONIC,&stop);

		double timetaken = (stop.tv_sec-start.tv_sec)*1000000000.;
		timetaken+=stop.tv_nsec-start.tv_nsec;
		vk_printf("completed with cycle rate of %f.\n",1000000000./timetaken);

		pt = pt->next_draw;
	}

	/*
		swap the screen buffers
	*/
	if(vk_ctx.draw_top != NULL){
	
	vf_swapbufs(s->d);
	screen_swap(s);

	}
	mt_unlock(&vk_ctx.drawlock);
}


void vk_update(void) {
	mem_set(&vk_com.pt_map,0,sizeof(struct pt_node));
	vk_com.bits = 0;
	vk_ctx.draw_top = NULL;
	vk_com.grab_pat = NULL;
	tcreat(&maj_th,maj_start, NULL);
	vk_com.pt_map.i = 0;
	maj_sig = maj_th->sig;
//	while(!(vk_com.bits&1));	
//	if (!run) {
//		vk_printf("what the fuck is happening?.\n");
//		maj_shutdown();
//		return;
//	}
	run = 0;
	vk_ctx.php = NULL;
	vk_ctx.cursor_has_moved = -1;
	vk_ctx.drawlock = MUTEX_INIT;
	vk_ctx.lock = MUTEX_INIT;
	vk_ctx.movmask = 0xf;
	struct y_timespec start,stop;
	y_clock_gettime(&start);
	y_clock_gettime(&stop);
	vk_ctx.cursor->s = getscrn(0,0);

	assert(vk_ctx.cursor->s != NULL);
	vk_ctx.cursor->s->cursor = vk_ctx.cursor;
	vk_ctx.cursor->s->drv->cursor_show(vk_ctx.cursor->s->priv,vk_ctx.cursor->s->crtc);

	// 100 times a second
	struct timespec delay = {0, 10000000};

	_64_s cx = 0,cy = 0;
	vk_ctx.m_head = NULL;
	vk_ctx.m = NULL;
	while(!running) { 

	cx = vk_ctx.x;
	cy = vk_ctx.y;

	evdev_drv.update(&vk_ctx);

	y_clock_gettime(&stop);
	_64_u delt = (stop.lower-start.lower)|(((stop.higher-start.higher))<<27);
	y_clock_gettime(&start);

	/*
		pointer is locked to patch so its not moving from one screen to another.
	*/
	if (vk_ctx.php != NULL) {
	}else
	if (!vk_ctx.cursor_has_moved){
		/*	if (vk_ctx.cursor->s != NULL) {
			mt_lock(&vk_ctx.cursor->s->scrn.lock);
			struct vk_patch *pt = pt_here(vk_ctx.cursor->s,cx,cy);
			mt_unlock(&vk_ctx.cursor->s->scrn.lock);
			if (pt != NULL) {
				
				//	if the patch is configured to hold the pointer if it enteres its premises then...
				
				if (pt->config&_PT_HOLDPTR) {
					vk_printf("pointer is being held by a patch.\n");
					pt->held.save_x = vk_ctx.x;
					pt->held.save_y = vk_ctx.y;
					pt->config |= _PT_HASPTR;
					vk_ctx.php = pt;
					goto _nodelt;
				}
			}else{
				vk_printf("could not locate patch at %d, %d.\n",cx,cy);
			}

			}else {
				vk_printf("cursor screen is(NULL).\n");
			}
		*/
			_int_s x,y;
			x = vk_ctx.x;
			y = vk_ctx.y;
			if(vk_com.grab_pat != NULL){
				pt_move(vk_com.grab_pat,x-vk_com.grab_xd,y-vk_com.grab_yd);
			}
		//	printf("CURSOR: %u, %u.\n",x,y);
			/*
				we we enter a new screen then reset cursor coords,
				why?? because its more effect as screen relative coords are more used then whole screen ones
				removes 
				x = X-screenX

			*/
			
			_int_u rs_x = x,rs_y = y;
			struct vk_scrn *s = NULL;
			struct vk_cursor *__c = vk_ctx.cursor;
			vk_ctx.movmask = 0xf;
			if (x<0 && y>=(_int_s)__c->s->d->height) {
				s = __c->s->bod[TR_SCRN_BOD];
				vk_ctx.movmask &= ~(VK_MM_LEFT|VK_MM_BOTTOM);
			}else if (y<0 && x>=(_int_s)__c->s->d->width) {
				s = __c->s->bod[BL_SCRN_BOD];
				vk_ctx.movmask &= ~(VK_MM_TOP|VK_MM_RIGHT);
				y = 0;
				x = __c->s->d->width;
			}else
			if (x<0 && y<0) {
				s = __c->s->bod[TL_SCRN_BOD];
				vk_ctx.movmask &= ~(VK_MM_LEFT|VK_MM_TOP);
				x = 0;
				y = 0;
			}else
			if (x>=__c->s->d->width && y>=__c->s->d->height) {
				s = __c->s->bod[BR_SCRN_BOD];
				vk_ctx.movmask &= ~(VK_MM_RIGHT|VK_MM_BOTTOM);					
				x = __c->s->d->width;
				y = __c->s->d->height;
			} else if (x<0) {
				/*
					left
				*/
				s = __c->s->bod[ML_SCRN_BOD];
				if (!s) {
					vk_ctx.movmask &= ~VK_MM_LEFT;
					x = 0;
				}else
					x = s->d->width;
			} else if (y<0) {
				/*
					top
				*/
				s = __c->s->bod[TP_SCRN_BOD];
				if (!s) {
					vk_ctx.movmask &= ~VK_MM_TOP;
					y = 0;
				}else
					y = s->d->height;
			} else
			if (x>=(_int_s)__c->s->d->width) {
				/*
					right
				*/
				s = __c->s->bod[MR_SCRN_BOD];
				if (!s) {
					vk_ctx.movmask &= ~VK_MM_RIGHT;
					x = __c->s->d->width;
				} else
					x = 0;
			} else if (y>=(_int_s)__c->s->d->height) {
				/*
					bottom
				*/
				s = __c->s->bod[BT_SCRN_BOD];
				if (!s) {
					vk_ctx.movmask &= ~VK_MM_BOTTOM;
					y = __c->s->d->height;
				} else {
					y = 0;
				}
			} else {

				goto _sk;
			}

			//mouse coords are screen selective
			vk_ctx.x = x;
			vk_ctx.y = y;
		_sk:
			/*
				a screen change has taken place.
				ie. owner of the cursor has changed.
			*/
			if (s != NULL) {
				printf("screen change.\n");
				/*
					as we are changing screen. make sure cursor is erased
					NOTE:
						__c->s = s; is to take place after vk_leavescreen routine
						never before hand.
				*/
				vk_leavescreen(__c);
				__c->s = s;
				s->cursor = __c;
				/*
					this routine functions diffrently from the normal because on this screen
					the cursor doesent exist and hasent been drawn yet. so cursorbuf will contain junk data.
					and we dont want that.
				*/
				if (vk_ctx.x>=0&&vk_ctx.y>=0) {
				//	vk_cursor_sw0(__c,vk_ctx.x,vk_ctx.y);
				}
			} else {	
				/*
					woops lol '||' not '&&', god damit 
				*/
				if(vk_ctx.x != cx || vk_ctx.y != cy) {
					printf("MOVING CURSOR, %d, %d\n",vk_ctx.x, vk_ctx.y);		
					vk_amdgpu_cursor_move(__c->s->priv,__c->s->crtc,vk_ctx.x,vk_ctx.y);
					//negative number/s are not acceptable
				//	vk_cursor_sw(__c,vk_ctx.x,vk_ctx.y);
				}
			}
	}
_nodelt:
	/* never going to be used
	could call pinched	
	if (!(griped|vk_ctx.cursor_has_moved)) {
		vk_printf("PATCH GRIPED.\n");
		pt_move(g_pt,g_pt->x+(vk_ctx.x-g_pt->x),g_pt->y+(vk_ctx.y-g_pt->y));
	
	}
	*/
	dorender();
{
  struct vk_msg *m;
  m = vk_ctx.m_head;
  while(m != NULL){
		m->type = _VK_MSG_INPUT;
		vk_msg_in(m);
    m = m->next;
  }
	vk_ctx.m_head = NULL;
	vk_ctx.m = NULL;

}

	nanosleep(&delay, NULL);
	}

	maj_shutdown();
}

