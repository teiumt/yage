#include "maj.h"
#include "../io.h"
#include "../string.h"
#include "../types.h"
#include "../linux/time.h"
#include "../amdgpu.h"
#include "../m_alloc.h"
void static
draw_rect(_8_u *__pixels,_int_u __x, _int_u __y) {
	_int_u x,y;
	y = __y;
	for(;y != __y+64;y++) {
		x = __x;
		for(;x != __x+64;x++) {
			_8_u  *d = __pixels+((x+(y*512))*4);
			d[0] = 255;
			d[1] = 0;
			d[2] = 0;
			d[3] = 255;
		}
	}
}
#include "../signal.h"
static _8_s running = 0;
void static sig_handler(int s){
	running = -1;
}

_err_t main(int __argc, char const *__argv[]) {
	struct sigaction sig;
	sig.sa_handler = sig_handler;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags = 0;
	sigaction(SIGINT,&sig,NULL);

 	struct amdgpu_dev *dv;
	int fd;
	fd = ffly_drm_open("/dev/dri/renderD128");
	dv = amdgpu_dev_init(fd);
	struct amdgpu_bo *back = m_alloc(sizeof(struct amdgpu_bo));
	struct amdgpu_bo *front = m_alloc(sizeof(struct amdgpu_bo));
#define BO_SIZE 512*512*4
	amdgpu_bo_alloc(dv, back,512*512*4,0, AMDGPU_GEM_DOMAIN_GTT, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);
	amdgpu_bo_alloc(dv, front,512*512*4,0, AMDGPU_GEM_DOMAIN_GTT, AMDGPU_GEM_CREATE_CPU_ACCESS_REQUIRED);


	_32_u back_handle, front_handle;
	amdgpu_bo_export(dv,back,amdgpu_bo_handle_dma_buf_fd,&back_handle);
	amdgpu_bo_export(dv,front,amdgpu_bo_handle_dma_buf_fd,&front_handle);
	amdgpu_bo_cpu_map(dv,back);
	amdgpu_bo_cpu_map(dv,front);
	void *back_ptr = back->cpu_ptr;
	void *front_ptr = front->cpu_ptr;

	printf("BO_HANDLE: %u,%u\n", back_handle,front_handle);
	struct maj_conn *c;
	struct timespec ts;
	ts.tv_nsec = 5000000/*200-cps*/;
	ts.tv_sec = 0;
	c = maj_connect();
 	_32_u wx,wy;
	wx = 100;
	wy = 100;

 
	if (!c) return -1;
	mem_set(back_ptr,100,512*512*4);	
	struct maj_patch *pt, _pt;
	maj_patch(c, &_pt,wx, wy, 512, 512,MAJ_SHARED);
	pt = &_pt;
	sendfd(c->u_fd,back_handle);
	sendfd(c->u_fd,front_handle);
	_8_u msgbuf[4+sizeof(struct vk_msg)*16];

	_32_u cx,cy;
	cx = 0;
	cy = 0;
	maj_ctxmake(c,0);
	maj_ctxsetpt(c,&_pt);
 	while(!running) { 
		
		//	clear back buffer to use in next frame
		
		mem_set(back_ptr,100,512*512*4);
		maj_msg(c, pt, msgbuf, sizeof(msgbuf));
		struct vk_msg *m = msgbuf+4;
		_int_u i;
		i = 0;
		for(;i != *(_32_u*)msgbuf;i++){
			if((m->type&0xff) == _VK_MSG_CONTROL){
				if(m->type == (_VK_MSG_CONTROL|_VK_MSG_PATCH_MOVE)){
					wx = m->newpatch.x;
					wy = m->newpatch.y;
				}
			}else{
				cx = m->x-wx;
				cy = m->y-wy;
				printf("MSG_COUNT: %u.\n",*(_32_u*)msgbuf);
				printf("MSG: ID: %u, CODE: %u, X: %d, Y: %d.\n", m->id, m->code, m->x, m->y);
			}
			m++;
		}

		if(cx<512-64 && cy < 512-64 && cx>0 && cy>0) {
			draw_rect(back_ptr,cx,cy);
		}

	//	mem_cpy(front_ptr,back_ptr,512*512*4);
		
		
		maj_swapbufs(c);

		void *tmp;
		tmp = back_ptr;
		back_ptr = front_ptr;
		front_ptr = tmp;
		
		printf("TICK.\n");
		
		nanosleep(&ts, NULL);
}

	printf("done goodbye.\n");
	ffly_fdrain(_ffly_out);
	maj_ptdestroy(c, pt);
   
	maj_exit(c);
	maj_close(c);
	amdgpu_dev_de_init(dv);
}
