#include "common.h"
#include "maj.h"
#include "../linux/stat.h"
#include "../linux/types.h"
#include "../linux/socket.h"
#include "../inet.h"
#include "../linux/errno-base.h"
#include "../linux/un.h"
#include "../linux/unistd.h"
#include "../assert.h"
#include "../io.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../linux/net.h"
#include "../linux/in.h"
#include "../in.h"
#include "../system/errno.h"
#include "../system/tls.h"
#include "../thread.h"
#include "../tools.h"
#include "../log.h"
#include "../strf.h"
#include "../sched.h"
#include "../linux/time.h"
#define TLS_ (TLS_SIZE-sizeof(struct tls_storage))
#define TLS ((struct tls_storage*)ffly_tls_getp(TLS_))
#include "../log.h"
struct opstrc {
	_8_u(*func)(struct local_context*,_8_u*);
};

#define REALID(__cl,__id)\
	(__id|((__cl)->id<<6)
struct opstrc static ops[64];
static struct vk_client_priv *wm = NULL;
static _64_u idslist[64*16];

struct local_context {
	int csk;
	struct sockaddr_un cli;
	sched_entityp ent;
	_64_u mask;
	_32_u id;

	struct maj_context ctxs[8];
	struct maj_context *curctx;
	struct vk_client_priv priv;
	struct local_context *next, **bk;
	struct timespec swp_start;
	/*
		debug
	*/
	_int_u state;
	_32_u last_op;
	_int_u op_state;
	_int_u exec_len;
	struct log_struc log;
};

#define maj_printf(__ctx,...)\
	log_printf(&(__ctx)->log,__VA_ARGS__)

struct tls_storage {};

static int sock;
static int sock0;
static struct sockaddr_un adr;
static struct sockaddr_un u_adr;
#define _LEAVE_newpatch		(24+1)
#define _LEAVE_ptdestroy	(4+1)
#define _LEAVE_msg			(4+1)
#define _LEAVE_auth			(4+1)
#define _LEAVE_ctxnew		(0)
#define _LEAVE_ctxsetdev	(8+1)
#define _LEAVE_ctxsetpt		(5)
#define _LEAVE_ctxmake		(4+1)
#define _LEAVE_config		(4+1)
#define _LEAVE_swapbufs		(1)
#define _LEAVE_grab			(1+12)
#define _LEAVE_ungrab			(1)
#define _LEAVE_conjoin			(1+8)
// the leave value
#define LEAVE(__what) _LEAVE_ ## __what
/*
	DESIGN_NOTES

	window header = patch
	window inner = patch

		WINDOW
	________________
	|______________|-> PAT0 -| LINKED
	|			         |-> PAT1 -|
	|			         |
	|              |
	|______________|

	vekas should be notified of any changes or frame pushes in advanced
	EXAMPLE:
		say we have a changing scene we should tell vekas we are in an active state,
		else tell vekas to do X frames at Yinterval????

	NOTE:
		buffer object client holded may be on diffrent card then what we are using for the screen
*/

/*
	BIGNOTE:
		nothing time consuming should be placed in thies routines
		its "in then out"
*/
static struct pt_node dummy_ptn;
_8_u static new_patch(struct local_context *__ctx, _8_u *__p) {
	_32_u x = *(_32_u*)__p;
	_32_u y = *(_32_u*)(__p+4);
	_32_u width = *(_32_u*)(__p+8);
	_32_u height = *(_32_u*)(__p+12);
	_32_u adr = *(_32_u*)(__p+16);
	_32_u flags = *(_32_u*)(__p+20);	

	maj_printf(__ctx,"new patch{%u, %u, %u, %u, %u}\n", x, y, width, height, adr);
	struct vk_patch *pt;

	pt = m_alloc(sizeof(struct vk_patch));
	idslist[adr] = pt;
	maj_printf(__ctx,"patch has pointer of %p.\n",pt);
	struct vk_scrn *s;
	s = getscrn(x,y);
	if (!s) {
		maj_printf(__ctx,"#WARNING patch location is prohibited.\n");
		goto _sk;
	}
	maj_printf(__ctx,"found screen that patch resides within, %p\n",s);

	pt->d = s->g->dw_alloc(); 
	if (flags&MAJ_SHARED){
		maj_printf(__ctx,"shared drawable.\n");
		ffly_fdrain(_ffly_out);
		int front;
		int back;
		/*
			these are switched
		*/
		front = recvfd(sock0);//clients back buffer
		back = recvfd(sock0);//clients front buffer

		s->g->shared_backing(s->priv,pt->d,back,front,width,height);
	}
	pt->client = &__ctx->priv;
	pt->config = 0;
	pt->g = s->g;
	pt->x = x;
	pt->y = y;
	pt->adr = adr;
	pt->width = width;
	pt->height = height;
	pt->xfar = x+width;
	pt->yfar = y+height;
	pt->ex = x;
	pt->ey = y;
	pt->update = -1;
	pt->s = s;
	pt->ptn = &dummy_ptn;
	pt->slain = -1;
	pt->lock = MUTEX_INIT;
	pt_place(pt);

	mt_lock(&vk_ctx.drawlock);
	pt->next_draw = vk_ctx.draw_top;
	vk_ctx.draw_top = pt;
	mt_unlock(&vk_ctx.drawlock);
	if(wm != pt->client && wm != NULL){
		struct vk_msg *m;
		m = vk_msgalloc();
		m->type = _VK_MSG_CONTROL|_VK_MSG_NEW_PATCH;
		m->newpatch.patch = adr; 	
		m->newpatch.x = x;
		m->newpatch.y = y;
		m->newpatch.width = width;
		m->newpatch.height = height;
		vk_msgclient(wm,m,1);
	
	}
	/*
	mt_lock(&s->scrn.lock);
	pt->bk = &s->scrn.pt;
	pt->next = s->scrn.pt;
	if (s->scrn.pt != NULL)
		s->scrn.pt->bk = &pt->next;
	s->scrn.pt = pt;
//	pt_place(pt);
	mt_unlock(&s->scrn.scrn.lock);
*/
_sk:
	return LEAVE(newpatch);
}

_8_u static ptdestroy(struct local_context *__ctx, _8_u *__p) {
	maj_printf(__ctx,"ptdestroy.\n");
	_32_u adr = *(_32_u*)__p;
	struct vk_patch *pt;
	pt = (struct vk_patch*)idslist[adr];
	assert(adr<1024);//extra measures
	
	maj_printf(__ctx,"PATCH: %p, SCREEN: %p, adr: %u\n",pt,pt->s,adr);
	mt_lock(&pt->s->scrn.lock);
	pt_rm(pt);

	*pt->bk = pt->next;
	if (pt->next != NULL)
		pt->next->bk = pt->bk;
	mt_unlock(&pt->s->scrn.lock);
	
	pt->d->free(pt->s->priv,pt->d);
	if (pt->config&_PT_HASPTR)
		vk_pt_unhold();
	return LEAVE(ptdestroy);
}

_8_u static msg(struct local_context *__ctx, _8_u *__p) {	
	struct vk_inbox *in = &__ctx->priv.inbox;
	if (!in->nmsg) {
		_32_u none = 0;
		_maj_send(__ctx->csk, &none, sizeof(_32_u));
		goto _done;
	}

	_8_u buf[4+sizeof(struct vk_msg)*16];
	struct vk_msg *_m = buf+4;
	_int_u i = 0;

	struct vk_msgpers *m;
	_int_u msgs;
	mt_lock(&in->lock);
	/*
		we have a max of 16 messages, sould be more dynamic
	*/
	msgs = in->nmsg>16?16:in->nmsg;
	for(;i != msgs;i++) {
		m = in->top;
		in->top = m->next[in->id];
		_m[i] = m->m;
		maj_printf(__ctx,"dispatching message: %u, %u, %d, %d.\n", m->m.id, m->m.code, m->m.x, m->m.y);
		vk_msgfree(m);
	}
	in->nmsg-=msgs;
	if(!in->nmsg) {
		in->m = NULL;
		in->top = NULL;
	}
	mt_unlock(&in->lock);
	*(_32_u*)buf = msgs;
	assert(msgs<=16);
	maj_printf(__ctx,"sending message hullage.\n");
	_maj_send(__ctx->csk, buf, 4+(msgs*sizeof(struct vk_msg)));
_done:
	return LEAVE(msg);
}

_8_u static frame(struct local_context *__ctx, _8_u *__p) {
	printf("frame.\n");
	return 4+1;
}

_8_u static _auth(struct local_context *__ctx, _8_u *__p) {
	_32_u magic;
	magic = *(_32_u*)__p;
	if (drm_auth_magic(__ctx->curctx->dev->fd, magic)<0) {
		maj_printf(__ctx,"failed to auth.\n");
	}
	maj_printf(__ctx,"AUTH: %u.\n", magic);
	return LEAVE(auth);
}

_8_u static _ctxnew(struct local_context *__ctx, _8_u *__p) {
	_32_u id = *(_32_u*)__p;
	struct maj_context *ct = __ctx->ctxs+id;
}
/*
	set driver the client is using

	NOTE:
		screen driver 'might not equal' the client driver
	TODO:
		client might be using more then 1 driver at a time????
		not likly but for future refrence.
*/
_8_u static _ctxsetdev(struct local_context *__ctx, _8_u *__p) {
	_32_u drv = *(_32_u*)__p;
	_32_u dev = *(_32_u*)(__p+4);
	maj_printf(__ctx,"driver: %u, device: %u.\n", drv,dev);
	struct maj_context *ct = __ctx->curctx;
	ct->drv = vk_drvtab+drv;
	struct vk_dev *d = vk_devloopup(dev);
	if (!d) {
		maj_printf(__ctx,"failed to find device.\n");
	}
	ct->dev = d;
	return LEAVE(ctxsetdev);
}

_8_u static _ctxsetpt(struct local_context *__ctx, _8_u *__p) {
	_32_u adr = *(_32_u*)__p;
	struct vk_patch *pt;
	pt = (struct vk_patch*)idslist[adr];
	__ctx->curctx->pt = pt;
	return LEAVE(ctxsetpt);
}

_8_u static _ctxdestroy(struct local_context *__ctx, _8_u *__p) {
	_32_u id = *(_32_u*)__p;
	struct maj_context *ct = __ctx->ctxs+id;
}

_8_u static _ctxmake(struct local_context *__ctx, _8_u *__p) {
	_32_u id = *(_32_u*)__p;
	struct maj_context *ct = __ctx->ctxs+id;
	__ctx->curctx = ct;
	return LEAVE(ctxmake);
}

_8_u static _config(struct local_context *__ctx, _8_u *__p) {
	_32_u config;
	config = *(_32_u*)__p;
	if(config&2){
		wm = &__ctx->priv;
	}else{
	__ctx->curctx->pt->config = *(_32_u*)__p;
	}

	return LEAVE(config);
}

#include "flue/flue.h"
/*

	NOTE: no double buffer action taking place.
	TODO: double buffer
*/

_8_u static _swapbufs(struct local_context *__ctx, _8_u *__p) {
	__ctx->op_state = 0;
	maj_printf(__ctx,"SWAPBUFFERS.\n");
	struct vk_patch *pt = __ctx->curctx->pt;
	mt_lock(&pt->lock);
	vf_swapbufs(pt->d);
	mt_unlock(&pt->lock);

	_8_u dummy;
	_maj_send(__ctx->csk, &dummy, 1);
	__ctx->op_state = 1;
	return LEAVE(swapbufs);
}

_8_u static _grab(struct local_context *__ctx, _8_u *__p) {
	_32_u patch = *(_32_u*)(__p);
	_32_u xd = *(_32_u*)(__p+4);
	_32_u yd = *(_32_u*)(__p+8);
	struct vk_patch *pt;
	pt = (struct vk_patch*)idslist[patch];
	pt_rm(pt);
	vk_com.grab_xd = xd;
	vk_com.grab_yd = yd;
	vk_com.grab_pat = pt;
	
	return LEAVE(grab);
}

void static move_patch(struct vk_patch *pt){
	struct vk_msg *m;
	m = vk_msgalloc();
	m->type = _VK_MSG_CONTROL|_VK_MSG_PATCH_MOVE;
	m->newpatch.patch = pt->adr;
	m->newpatch.x = pt->x;
	m->newpatch.y = pt->y;
	vk_msgclient(pt->client,m,1);
	if((pt = pt->child) != NULL){
		m = vk_msgalloc();
		m->type = _VK_MSG_CONTROL|_VK_MSG_PATCH_MOVE;
		m->newpatch.patch = pt->adr;
		m->newpatch.x = pt->x;
		m->newpatch.y = pt->y;
		vk_msgclient(pt->client,m,1);
	}
	
}

_8_u static _ungrab(struct local_context *__ctx, _8_u *__p) {
	struct vk_patch *tmp = vk_com.grab_pat;
	vk_com.grab_pat = NULL;//FIXME: timing
	pt_place(tmp);
	move_patch(tmp);
	return LEAVE(ungrab);
}
//adopt
_8_u static _conjoin(struct local_context *__ctx, _8_u *__p){
	_32_u x = *(_32_u*)(__p);
	_32_u y = *(_32_u*)(__p+4);
	struct vk_patch *a,*b;
	a = (struct vk_patch*)idslist[x];
	b = (struct vk_patch*)idslist[y];

	a->child = b;
	printf("conjoining, %u, %u\n",x,y);
	return LEAVE(conjoin);
}

/*
	TODO:
		vekas and PTC run there own maj server,
		to do somthing about the none needed stuff.
*/
struct opstrc static ops[64] = {
	{NULL},
	{new_patch},
	{frame},
	{ptdestroy},
	{msg},
	{_auth},
	{_ctxnew},
	{_ctxdestroy},
	{_ctxmake},
	{_ctxsetdev},
	{_swapbufs},
	{_ctxsetpt},
	{_config},
	{_grab},
	{_conjoin},
	{_ungrab}
};

_8_s static exec(struct local_context *__ctx, _8_u *__buf, _int_u __len) {
	_8_u *p, *end;
	p = __buf;
	end = p+__len;
	_8_u on;
	struct opstrc *op;
	while(p<end) {
		on = *p;
		op = ops+on;
		if (on>15) return 0;

		if (on == 0x00) {
			return 0;
		}
		_int_u sz;
		__ctx->state = 3;
		__ctx->last_op = on;
		__ctx->exec_len = __len;
		p+=(sz = op->func(__ctx,p+1));
	}

	__ctx->state = 4;
	/*
		if this happens then we got are selfs an io issue.
	*/
	if (p != end) {
		maj_printf(__ctx,"it seems that we overshoot are intended target.\n");
	}

	assert(p == end);

	return -1;
}
static struct local_context *top_clctx = NULL;
#include "../mutex.h"
static mlock clctx_lock = MUTEX_INIT;

static _64_u client_ids[] = {
	1,2,4,8,16,32,64
};
static _64_u client_idx = 7;

static _64_u clients = 0;
_8_s static clienthandler(sched_entityp __ent) {
	struct local_context *cl;
	cl = __ent->arg;
	if(_maj_poll(cl->csk) == -1)
		return -1;
	_32_u sz;
	cl->state = 0;
	_8_u buf[128];

	sz = 0;
	if (_maj_recv(cl->csk, &sz, sizeof(_32_u)) == -1)
		goto _end;
	if (sz>128) {
		maj_printf(cl,"size is to big, got: %u\n", sz);
		goto _end;
	}
	cl->state = 1;
	if (_maj_recv(cl->csk, buf, sz) == -1) {
		goto _end;
	}

	cl->state = 2;
	if (!exec(cl,buf, sz)) {
		goto _end;
	}
	return -1;

_end:
	cl->state = 5;

	mt_lock(&clctx_lock);
	*cl->bk = cl->next;
	if(cl->next != NULL) {
		cl->next->bk = cl->bk;
	}
	/*
		not safe, needs working on
	*/
	client_ids[client_idx++] = cl->mask;
	mt_unlock(&clctx_lock);
	maj_printf(cl,"finishing-up client handle.\n");	
	cl->state = 6;
	close(cl->csk);
	clients &= ~cl->mask;
	y_log_fileend(&cl->log);
	return 0;
}

void static preform_cleansing(void) {
	while(clients != 0) {
		mt_lock(&clctx_lock);
		struct local_context *cl;
		cl = top_clctx;
		while(cl != NULL) {
			ffly_fprintf(ffly_err,"client at a state of %x, last_operation: %x, exec_length: %u, op_state: %x\n",cl->state,cl->last_op,cl->exec_len,cl->op_state);
			cl = cl->next;
		}
		mt_unlock(&clctx_lock);
		printf("waiting for clients to kill themselfs off, %x.\n",clients);
		doze(0,4);
	}
}

void static loop(void) {
	while(1) {
		if (listen(sock, 1)<0) {
			break;
		}
		struct local_context *cl = m_alloc(sizeof(struct local_context));
		socklen_t len;
		len = sizeof(struct sockaddr_un);
		mem_set(&cl->cli, 0, sizeof(struct sockaddr_un));
		if ((cl->csk = accept(sock, (struct sockaddr*)&cl->cli, &len))<0) {
			printf("failed to accept.\n");
			break;
		}

		mt_lock(&clctx_lock);
		cl->next = top_clctx;
		if(top_clctx != NULL)
			top_clctx->bk = &cl->next;
		top_clctx = cl;
		cl->bk = &top_clctx;

		cl->mask = client_ids[--client_idx];
		cl->id = client_idx;
		clients |= cl->mask;
	
		mt_unlock(&clctx_lock);
		char logpath[200];
		ffly_strf(logpath,0,"maj.log%u",cl->id);
		y_log_file(&cl->log,logpath);
  	cl->priv.inbox.lock = MUTEX_INIT;
		cl->priv.inbox.m = NULL;
		cl->priv.inbox.top = NULL;
		cl->priv.inbox.nmsg = 0;
		cl->priv.inbox.id = cl->id;
		struct maj_initial initial;
		initial.adrnow = cl->id<<6;
		if(_maj_send(cl->csk,&initial, sizeof(struct maj_initial)) == -1){
		}

		sched_entityp ent;
		ent = sched_new();
		cl->ent = ent;
		ent->type = _sched_just_run;
		ent->arg = cl;
		ent->func = clienthandler;
		sched_submit(ent);
	}
	return;
}
static _8_s done = -1;
void maj_shutdown(void) {
	mt_lock(&clctx_lock);
	struct local_context *cl;
	cl = top_clctx;
	while(cl != NULL) {
		ffly_fprintf(ffly_err,"shutting down client socket, for %x\n",cl->mask);
	
		shutdown(cl->csk,SHUT_RDWR);
		cl = cl->next;
	}
	mt_unlock(&clctx_lock);
	shutdown(sock0,SHUT_RDWR);
	shutdown(sock, SHUT_RDWR);
	preform_cleansing();
	while(done == -1);
}
#define SOCKNAME "/opt/yage/vekas.skt"
#define SOCKNAME_COMS "/opt/yage/pcom.skt"
#define SOCKNAME_COMS2 "/opt/yage/vcom.skt"
void maj_start(void *__arg) {
	printf("starting MAJ server.\n");
	int r;
	/*
		unix socket to recve rights to fds
	*/
	sock0 = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (sock0<0) {
		printf("failed to open UNIX socket.\n");
		goto _err;
	}
	mem_set(&u_adr,0,sizeof(struct sockaddr_un));
	
	u_adr.sun_family = AF_UNIX;
	mem_cpy(u_adr.sun_path,SOCKNAME,sizeof(SOCKNAME));
	unlink(SOCKNAME);
_bsk0:
	r = bind(sock0,(struct sockaddr*)&u_adr,sizeof(struct sockaddr_un));
	if (r<0) {
		if (errno == -EAGAIN)
			goto _bsk0;
		printf("failed to bind UNIX socket, %s\n", strerror(errno));
		goto _err;
	}

	chmod(SOCKNAME,S_IRWXU|S_IRWXG|S_IRWXO);
	sock = socket(AF_UNIX, SOCK_STREAM, 0);
	mem_set(&adr, 0, sizeof(struct sockaddr_un));
	adr.sun_family = AF_UNIX;
	mem_cpy(adr.sun_path,SOCKNAME_COMS,sizeof(SOCKNAME_COMS));
	unlink(SOCKNAME_COMS);

	r = bind(sock,(struct sockaddr*)&adr,sizeof(struct sockaddr_un));
	if (r<0) {
		printf("failed to bind socket to" SOCKNAME_COMS ".\n");
	}
	chmod(SOCKNAME_COMS,S_IRWXU|S_IRWXG|S_IRWXO);
	loop();
	printf("GOODBYE.\n");
_err:
	printf("MAJ CLOSURE.\n");
	ffly_fdrain(_ffly_out);
	close(sock0);
	close(sock);
	done = 0;
}
