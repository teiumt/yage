#include "common.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../mutex.h"
void vk_pt_unhold(void) {
/*	mt_lock(&vk_ctx.lock);
	struct vk_patch *pt = vk_ctx.php;
	vk_ctx.x = pt->held.save_x;
	vk_ctx.y = pt->held.save_y;
	vk_printf("restoring cursor position of %d, %d.\n",vk_ctx.x,vk_ctx.y);
	vk_ctx.php = NULL;
	mt_unlock(&vk_ctx.lock);
*/
}
#define BX(__x) (((__x)+PT_MAP_MSK)>>PT_MAP_SH)

static struct pt_node* getptnode(struct vk_patch *__pt) {
 	_16_u _tx = __pt->x;
	_16_u _ty = __pt->y;
	struct pt_node *n = &vk_com.pt_map;
	return n;
	_16_u i,j;
	_16_u k;
	i = 0;
	for(;i != 16;i++) {
		j = (_tx>>(15-i))&1;
		k = (_ty>>(15-i))&1;
		_16_u y = 1<<(15-i);
		if(__pt->x<y && __pt->xfar>=y)
			break;
		if(__pt->y<y && __pt->yfar>=y)
			break;
		struct pt_node **np;
		np = n->downwards+(j+(k<<1));
		if (!*np) {
			n = *np = m_alloc(sizeof(struct pt_node));
			mem_set(n,0,sizeof(struct pt_node));
			n->lock = MUTEX_INIT;
		}else{
			n = *np;
		}
	}

	return n;
}

void pt_place(struct vk_patch *__pt) {
	struct pt_node *n;
	n = getptnode(__pt);
	mt_lock(&n->lock);
	__pt->ptn_idx = n->i;
	n->pts[n->i++] = __pt;
	__pt->ptn = n;
	mt_unlock(&n->lock);
}
/*
	i dont think im going to keep this.
	tilizing can only get you soo far.
*/
void pt_rm(struct vk_patch *__pt) {
	mt_lock(&__pt->ptn->lock);
	__pt->ptn->pts[__pt->ptn_idx] = __pt->ptn->pts[--__pt->ptn->i];
	if(__pt->ptn_idx<__pt->ptn->i){
		__pt->ptn->pts[__pt->ptn_idx]->ptn_idx = __pt->ptn_idx;
	}
	mt_unlock(&__pt->ptn->lock);
}

/*
	this should be better then brute force look up
*/
void pt_move(struct vk_patch *__pt, _int_u __x, _int_u __y) {
	__pt->x = __x;
	__pt->y = __y;
	if(__pt->child != NULL){
		__pt->child->x = __x;
		__pt->child->y = __y+__pt->height;
	}
}

struct vk_patch*
pt_here(_int_u __x, _int_u __y) {
	_16_u _tx = __x;
	_16_u _ty = __y;
	struct pt_node *n = &vk_com.pt_map;
	_16_u i,j;
	_16_u k;
	i = 0;
	for(;i != 16;i++) {
		j = (_tx>>(15-i))&1;
		k = (_ty>>(15-i))&1;
		struct vk_patch *pt;
		mt_lock(&n->lock);
		_int_u pat;
		pat = 0;
		for(;pat != n->i;pat++){
			pt = n->pts[pat];
			vk_printf("checking patch: %u,%u>=%u,%u.\n",pt->x,pt->y,__x,__y);
			if(__x>=pt->x && __y>=pt->y) {
				if(__x<(pt->x+pt->width) && __y<(pt->y+pt->height)){
					mt_unlock(&n->lock);
					return pt;
				}
			}
		}
		
		mt_unlock(&n->lock);
		n = n->downwards[j+(k<<1)];
		if (!n)break;
	}

	return NULL;
}

