# ifndef __vk__maj__h
# define __vk__maj__h
# include "common.h"
# include "../linux/in.h"
#include "../linux/un.h"
#define MAJ_SHARED 0x01
enum{
	_MAJ_NULL,
	_MAJ_PATCH_NEW,
	_MAJ_FRAME,
	_MAJ_DESTROY,
	_MAJ_MSG,
	_MAJ_AUTH
};

struct maj_initial{
	_32_u adrnow;
};
/*
	short for majestic
*/
struct maj_patch;
struct maj_conn {
	struct maj_initial i;
	int fd;
	struct sockaddr_un adr;

	int u_fd;
	struct sockaddr_un u_adr;
	struct maj_patch *patches[64];
};

struct maj_patch {
	_32_u adr,id;
	_int_u width, height;
	_int_s x,y;
};

struct maj_context {
	struct vk_drv *drv;
	struct vk_dev *dev;
	struct vk_patch *pt;
};
void maj_ptconjoin(struct maj_conn *__c, _32_u __a,_32_u __b);
void maj_ptgrab(struct maj_conn *__c, struct maj_patch *__pt,_32_u,_32_u);
void maj_ptungrab(struct maj_conn *__c,struct maj_patch *__pt);

_8_s _maj_poll(int fd);
void maj_ptconfig(struct maj_conn*,_32_u);
void maj_ctxsetpt(struct maj_conn*,struct maj_patch*);
void maj_swapbufs(struct maj_conn*);
void maj_ctxmake(struct maj_conn*,_32_u);
void maj_ctxsetdev(struct maj_conn*,_32_u,_32_u);
void maj_patch(struct maj_conn*,struct maj_patch*,_int_u,_int_u,_int_u,_int_u,_32_u);
void maj_ptdestroy(struct maj_conn*,struct maj_patch*);
void maj_msg(struct maj_conn*,struct maj_patch*,void*,_int_u);
void maj_frame(struct maj_conn*,struct maj_patch*,void*);
void maj_auth(struct maj_conn*, int);
extern struct maj_conn *maj_con;

void maj_send(int, void*, _int_u);
void maj_recv(int, void*, _int_u);
_int_s _maj_send(int, void*, _int_u);
_int_s _maj_recv(int, void*, _int_u);

struct maj_conn *maj_connect(void);
void maj_exit(struct maj_conn*);
void maj_close(struct maj_conn*);
void maj_start(void*);
void maj_shutdown(void);
void mjk_start(void);
# endif /*__vk__maj__h*/
