rm -f *.o hw/*.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine -D__vekas"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd vekas
ffly_objs="$ffly_objs patch.o message.o screen.o display.o log.o amdgpu_flue.o flue.o flue_copy.o flue_program.o render.o common.o fbdev.o maj.o evdev.o amdgpu.o amdgpu_drv.o amdgpu_display.o"
gcc -c $cc_flags message.c
gcc -c $cc_flags screen.c
gcc -c $cc_flags patch.c
gcc -c $cc_flags display.c
gcc -c $cc_flags log.c
gcc -c $cc_flags hw/fbdev.c
gcc -c $cc_flags common.c
gcc -c $cc_flags maj.c
gcc -c $cc_flags render.c
gcc -c $cc_flags hw/evdev.c
gcc -c $cc_flags hw/amdgpu/amdgpu_flue.c
gcc -c $cc_flags hw/amdgpu/amdgpu.c
gcc -c $cc_flags hw/amdgpu/amdgpu_drv.c
gcc -c $cc_flags hw/amdgpu/amdgpu_display.c
gcc -c $cc_flags flue/flue.c
gcc -c $cc_flags flue/copy.c -o flue_copy.o
gcc -c $cc_flags flue/flue_program.c
gcc $cc_flags -o connect connect.c $ffly_objs -nostdlib
#gcc $cc_flags -o smwm ptc_flue.o canc.o canc_screen.o ../smwm/smwm_main.c $ffly_objs -nostdlib
gcc $cc_flags -o vk vk_main.c $ffly_objs -nostdlib
