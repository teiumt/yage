# include "common.h"
# include "../m_alloc.h"
# include "../string.h"
# include "../io.h"
# include "../linux/socket.h"
#define HDR_SPACE 8
#define SEG_HDRSPACE 8
#define SEG_SHFT 12
#define SEG_SIZE (1<<SEG_SHFT)
#define SEG_MASK (SEG_SIZE-1)
#define SD_SIZE (SEG_SIZE-SEG_HDRSPACE)
struct hdr {
	_int_u segs;
};

struct segment {
	_8_u *data;
};

struct seghdr {
	_16_u n;
};
#include "../linux/poll.h"

_8_s _maj_poll(int fd){
	struct pollfd pfd = {
		.fd = fd,
		.events = POLLIN,
		.revents = 0
	};

	if (poll(&pfd,1,1)<=0) {
		return -1;

	}
	return 0;
}
void maj_send(int fd, void *__buf, _int_u __size) {
	_int_u ns;
	ns = (__size+HDR_SPACE+(SD_SIZE-1))/SD_SIZE;

	_8_u *p;
	p = (_8_u*)__buf;

	_8_u *data;	
	data = (_8_u*)m_alloc(ns<<SEG_SHFT);

	struct segment *segs = (struct segment*)m_alloc(ns*sizeof(struct segment));
	struct segment *sp = segs+1;

	struct hdr *h = data+SEG_HDRSPACE;
	segs->data = data;
	h->segs = ns;
	printf("SND-segs: %u.\n", ns);
	if (__size>=(SD_SIZE-HDR_SPACE)) {
	mem_cpy(data+(SEG_HDRSPACE+HDR_SPACE), p, SD_SIZE-HDR_SPACE);
	p+=(SD_SIZE-HDR_SPACE);

	_int_u i;
	i = 0;
	_int_u seg;
	_int_u nds = (__size-(SD_SIZE-HDR_SPACE))/SD_SIZE;
	while(i != nds) {
		seg = ((i+1)<<SEG_SHFT)|SEG_HDRSPACE;
		mem_cpy(data+seg, p, SD_SIZE);

		sp->data = data+(seg-SEG_HDRSPACE);
		sp++;
		p+=SD_SIZE;
		i++;
	}

	_int_u left;
	left = __size-(p-((_8_u*)__buf));
	if (left>0) {
		seg = ((nds+1)<<SEG_SHFT)|SEG_HDRSPACE;
		sp->data = data+(seg-SEG_HDRSPACE);	
		mem_cpy(data+seg, p, left);
	}

	} else {
		mem_cpy(data+(SEG_HDRSPACE+HDR_SPACE), p, __size);
	}

	int r;
	_int_u i = 0;
	for(;i != ns;i++) {
		sp = segs+i;

		((struct seghdr*)sp->data)->n = i;
		if ((sp->data+SEG_SIZE) > (data+(ns<<SEG_SHFT))) {
			while(1);
		}
		if ((r = send(fd, sp->data, SEG_SIZE, 0))<SEG_SIZE) {
			printf("failed to send.\n");
			if (r>0) {
				_int_u needed = SEG_SIZE-r;
				_8_u *p = sp->data;
				p+=r;
			_more:
				if ((r = send(fd, p, needed, 0))<needed) {
					p+=r;
					needed-=r;
					goto _more;
				}
			} else
				break;
		}
	}	

	m_free(data);
	m_free(segs);
}

#include "../assert.h"
# include "../tools.h"
void maj_recv(int fd, void *__buf, _int_u __size) {
	_8_u buf[SEG_SIZE];
	
	_int_u end = ~0;
	_int_u i;
	i = 0;
	_8_u *d;
	_8_u *dst;

	_int_u max;
	dst = (_8_u*)m_alloc(max = (((__size+(SD_SIZE-1))/SD_SIZE)*SD_SIZE));

/*
	must remain true
*/
	printf("MAJ_RECV: size: %u, MAX: %u.\n", __size, max);
	assert(max>__size);

	int r;
	d = dst;
	struct seghdr *sh = buf;
	while(i != end) {
		if ((r = recv(fd, buf, SEG_SIZE, 0))<SEG_SIZE) {
			printf("failed to recv, R: %d\n", r);
			if (r>0) {
				_int_u needed = SEG_SIZE-r;
				_8_u *p = buf+r;
			_more:
				r = recv(fd, p, needed, 0);
				if (r<needed) {
					p+=r;
					needed-=r;
					goto _more;
				}
			} else
				goto _err;
		}
//		printf("seg-%u.\n", sh->n);
		if (!sh->n) {
			struct hdr *h = buf+SEG_HDRSPACE;
			end = h->segs;
			printf("N-segs: %u, %u.\n", h->segs, SD_SIZE-HDR_SPACE);
			mem_cpy(d, buf+SEG_HDRSPACE+HDR_SPACE, SD_SIZE-HDR_SPACE);
			d+=SD_SIZE-HDR_SPACE;
		} else {
			mem_cpy(d, buf+SEG_HDRSPACE, SD_SIZE);
			d+=SD_SIZE;
		}
		i++;
	}
	mem_cpy(__buf, dst, __size);
//	ffly_chrdump(dst, __size);
_err:
	m_free(dst);
}

#include "../linux/errno-base.h"
_int_s _maj_send(int fd, void *__buf, _int_u __size) {
	
	int r;
_again:
	r = send(fd, __buf, __size, 0);
	if (r == -EAGAIN || r == -EINTR) {
		goto _again;
	}
	if (r<__size) {
		ffly_fprintf(ffly_err,"send error.\n");
		return -1;
	}
	return r;

}
_int_s _maj_recv(int fd, void *__buf, _int_u __size) {
	int r;
_again:
	r = recv(fd, __buf, __size, 0);
	if (r == -EAGAIN || r == -EINTR) {
		goto _again;
	}
	if (r<__size) {
		ffly_fprintf(ffly_err,"recv error.\n");
		return -1;
	}
	return r;

}
