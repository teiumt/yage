#ifndef __vf__struc__h
#define __vf__struc__h
#include "../../flue/common.h"
struct vf_dwstrc {
	flue_texp tex;
};
struct vf_drawable {
	struct vk_drawable d;
    struct vf_dwstrc s;
};
#endif /*__vf__struc__h*/
