#include "flue.h"
static struct flue_prog *rect_vs,*rect_ps;
void vf_swapbufs(struct vf_drawable *__d) {
	flue_xchgbufs(__d->s.tex);
}
void static
rect_shaders(void) {
	struct flue_context *ct = FLUE_CTX;
	rect_vs = flue_prog();
	rect_ps = flue_prog();

	struct flu_plarg arg;

	arg.p = "/home/blort/firefly/src/vekas/test_vs.s";
	ysa_loader(ct,&arg, rect_vs);

	arg.p = "/home/blort/firefly/src/vekas/test_ps.s";
	ysa_loader(ct,&arg, rect_ps);

	flue_prog_ready(rect_ps,FSH(0));
	flue_prog_ready(rect_vs,FSH(1));
}

void vf_rect_shaders(void) {
	flue_shader(rect_ps,FSH(0),FSH_PIXEL_I);
	flue_shader(rect_vs,FSH(1),FSH_VETX_I);
}

void vf_init(void) {
	struct flue_context *ct;
	ct = flue_ctx_new();
	FLUE_setctx(ct);

	rect_shaders();
}

void vf_clear(struct vf_drawable *__d, float r, float g, float b, float a){
  void *priv[1] = {__d->s.tex};
  flue_clear(priv,1,r,g,b,a);
}

//ignore naming for now
void vf_ready(void) {
	rect_shaders();
}


void vf_finish(void) {
	flue_done();
}

void vf_drawable_init(struct vf_drawable *__d,_int_u __width,_int_u __height) {
	flue_texp tex;
	tex = flue_tex_new();
    flue_texconfig(tex,NULL,0,0,__width,__height,NULL);
	__d->s.tex = tex;
}

