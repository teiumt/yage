#ifndef __vk__flue__h
#define __vk__flue__h
#include "../../ffly_def.h"
#include "../common.h"
#include "../../flue/common.h"
#include "struc.h"
void vf_swapbufs(struct vf_drawable*);
void vf_copyarea(struct vf_drawable *__d,struct vf_drawable *__s,_int_u __x,_int_u __y,_int_u __width,_int_u __height);
void vf_clear(struct vf_drawable *__d, float r, float g, float b, float a);
void vf_init(void);
void vf_finish(void);
#endif /*__vk__flue__h*/
