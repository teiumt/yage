# ifndef __vk__struc__h
# define __vk__struc__h
#include "../mutex.h"
#include "subs.h"
struct vk_fb {
	_32_u back_id;
	_32_u front_id;
};
struct vk_scrn;
struct vk_patch;
struct vk_gc {
	struct vk_drawable*(*dw_alloc)(void);
	void(*dw_free)(void*,struct vk_drawable*);
	
	void(*write)(struct vk_drawable*, void*);
	
	void(*drawpixs)(void*,struct vk_drawable*,_8_u*,_int_u,_int_u,_int_u,_int_u,_int_u);
	void(*backing)(void*,struct vk_drawable*,_32_u, _32_u);
	void(*shared_backing)(void*,struct vk_drawable*,int,int,_32_u, _32_u);
	void *context;
	void(*drawrect)(struct vk_drawable*,_int_u,_int_u,_int_u,_int_u);
	void(*pt_draw)(struct vk_patch*);

	void(*copyarea)(struct vk_drawable*,struct vk_drawable*,_int_u,_int_u,_int_u,_int_u);
	void(*enter)(struct vk_scrn*);
	void(*exit)(struct vk_scrn*);
};
struct vk_crtc;
struct vk_cursor;
struct vk_display_mode;
struct vk_output;
struct vk_scrn;
struct vk_dev;
struct bitimage;
struct vk_drv {
	void*(*bo_new)(void*,void*,_int_u,_int_u);
	void*(*fb_new)(void*,void*,void*,_int_u,_int_u);
	void(*fb_destroy)(void*);
	void(*cursor)(void*, struct bitimage*, _int_u, _int_u);
	/*
		delete screen
	*/
	void(*delete)(void*);
	void *screens;
	/*
		attach outputs to crtc

		EXAMPLE:
		output-0 -|-> crtc
		output-1 -|
	*/
	void(*crtc_mode_set)(void*,void*,struct vk_crtc*, struct vk_display_mode*, struct vk_output**,_int_u,void*);
	struct vk_display_mode*(*get_modes)(void*,struct vk_output*, _int_u*);
	void(*page_flip)(struct vk_scrn*);
	void(*cursor_show)(void*,struct vk_crtc*);
	void(*cursor_load)(void*,struct vk_cursor*);
	void(*cursor_move)(void*,struct vk_crtc*,_32_s,_32_s);
	struct vk_scrn*(*screen)(void);
	struct vk_dev *devs;
	_int_u ndevs;
};
struct vk_context;
struct vk_iodrv {
	void(*update)(struct vk_context*);
};

struct vk_display_mode {
    _32_u clock;
    _16_u hdisplay;
    _16_u hsync_start;
    _16_u hsync_end;
    _16_u htotal;
    _16_u hskew;
    _16_u vdisplay;
    _16_u vsync_start;
    _16_u vsync_end;
    _16_u vtotal;
    _16_u vscan;

    _32_u vrefresh;

    _32_u flags;
};
#define CRTC_PAGEIDLE 1
struct vk_crtc {
	_64_u id;
	void *fb;
	struct vk_scrn *scrn;
	_64_u bits;
};
struct vk_output {
	_64_u id;
};
/*
	structure is only to convay details about the drawable nothing else?
*/
struct vk_drawable {
	void(*free)(void*,void*);
	_int_u width, height;
	_int_u size;
	_int_u line_length;
	_8_u *pixels;
	int fd_front,fd_back;
};

#define VK_PTR_MEV 0x00
#define VK_KEY_TEV 0x01
#include "keysym.h"

#define _VK_MSG_INPUT 0
#define _VK_MSG_CONTROL 1
#define _VK_MSG_NEW_PATCH (0<<8)
#define _VK_MSG_PATCH_MOVE (1<<8)
#define VK_RELEASE		0
#define VK_PRESS		1
#define VK_PTR_M		2
/*
	we are going to deal with messages a bit diffrently,
	each message has its own destination address{for what patch}

	its the clients job to deal with this mess.
*/
struct vk_msg {
	_int_u type;
	_int_u id, code, value;
	_int_s x, y;
	struct{
		_32_u patch;
		_int_u x,y;
		_int_u width, height;
	} newpatch;
	struct vk_msg *next, **bk;
};

//personal
struct vk_msgpers{
	struct vk_msg m;
/*
	look these structures are recycled so we can use as much memory as we want
	so next[8] isent wastful
*/
	struct vk_msg *next[8];
};


extern struct vk_scrn vk_scrn_clear;
struct vk_patch;
struct vk_cursor;

//short for bond
#define TL_SCRN_BOD 0
#define TP_SCRN_BOD 1
#define TR_SCRN_BOD 2
#define ML_SCRN_BOD 3
#define MR_SCRN_BOD 4
#define BL_SCRN_BOD 5
#define BT_SCRN_BOD 6
#define BR_SCRN_BOD 7

#define PT_MAP_SH 1
#define PT_MAP_SZ (1<<PT_MAP_SH)
#define PT_MAP_MSK (PT_MAP_SZ-1)
struct vk_patch;
struct pt_node {
	struct pt_node *downwards[4];
	struct vk_patch *pts[8];
	_int_u i;
	mlock lock;
};
struct vk_fbinfo {
	
	union {
		struct {
			struct vk_drawable *d;
			/*fb object*/
			void *_d;
		};
	};
};
/*
	this screen info will be the same accross vekas,ptc, and other contractors.
*/
struct vk_scrninfo {
	_int_u x, y;
	_int_u width,height;
	mlock lock;
	struct vk_scrninfo *next;
};

struct vk_stateinfo {
	struct {
		_int_s x,y;
	}cursor;
};
struct vk_scrn {
	/*
		screen stuff that is not vekas specific
	*/
	struct vk_scrninfo scrn;
	struct vk_fb *fb;

	struct vk_fbinfo front,back;
		
	/*
		drawable associated with this screen
		drawable may consist of more then one screen
	*/
	struct vk_drawable *d;
	/*
		private device ptr 
	*/
	void *priv;
	/*
		driver related functions
	*/
	struct vk_drv *drv;
	/*
		graphics functions
	*/
	struct vk_gc *g;
	
	_int_u off;
	_int_u xfar,yfar;
	
	struct vk_scrn *bod[8];

	struct vk_crtc *crtc;
	struct vk_cursor *cursor;
};

struct bitimage {
	_int_u width, height;
	_8_u *bitmap;
};

/*
	cursor movment restriction
*/
#define VK_MM_LEFT 1
#define VK_MM_RIGHT 2
#define VK_MM_TOP 4
#define VK_MM_BOTTOM 8
struct vk_cursor {
	_int_s x, y;
	_int_s cs_x,cs_y;
	struct bitimage *img;
	_8_u cursorbuf[64*4];
	struct vk_scrn *s;
	_32_u id;
};

/*
	who do we forward all the messages to? whoever PTC tells us to.
*/
struct vk_inbox {
	mlock lock;
	_int_u id;
	struct vk_msg *m, *top;
	_int_u nmsg;
};

struct vk_context {
	struct vk_scrn *screens;
	_int_s x, y;
	_64_u movmask;	
	struct vk_msg *m, *m_head;
	mlock lock;
	struct vk_cursor *cursor;
	_8_s cursor_has_moved;
	struct vk_patch *php;

	mlock drawlock;
	struct vk_patch *draw_top;
	_32_u wait;
};
#define VK_SHARE 450
#define VK_SHARE_SIZE (64)
struct vk_common {
  struct pt_node pt_map;
  /*
    blockes along xandy
  */
  _int_u bx,by;


	_64_u bits;
	struct vk_patch *grab_pat;
	_32_u grab_xd,grab_yd;
};
extern struct vk_common vk_com;
struct vk_fbdev {
	struct vk_scrn s;	
    _8_u *pixels;
	int fd;
	_int_u line_length;
	_int_u len;
	_8_u cursorbuf[64*4];
	/*
		cursor position
	*/
	_int_u x, y;
	struct vk_fbdev *next;
};
struct vk_client_priv {
	struct vk_inbox inbox;
	_32_u adr;
};
//comsume ptr
#define _PT_HOLDPTR 1
/* locked on to pointer
*/
#define _PT_HASPTR 2

struct vk_patch {
	_8_s update;
	struct vk_drawable *d;
	_int_u x, y;
	_int_u ex,ey;
	_int_u xfar,yfar;
	struct vk_gc *g;

	struct vk_patch *next, **bk;
	
	struct vk_scrn *s;
	
	mlock lock;
	struct vk_drawable *backbuf;
	_32_u config;
	_8_s slain;
	struct pt_node *ptn;
	_32_u ptn_idx;
	struct vk_client_priv *client;
	struct vk_patch *child;
	_int_u width;
	_int_u height;
	_32_u adr;
	struct vk_patch *next_draw;
};

struct vk_dev {
	int fd;
	struct vk_output *out;
	_int_u nouts;

	struct vk_crtc *crts;
	_int_u ncrts;
	void *priv;//layer1
	void *priv0;//layer0
};

void vk_fbdev_update(void);
void vk_update(void);
void vk_probe(void);
struct vk_msg* vk_msgnew(struct vk_context*);
# endif /*__vk__struc__h*/
