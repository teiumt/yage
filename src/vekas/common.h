# ifndef __vk__common__h
# define __vk__common__h
# include "../y_int.h"
# include "struc.h"
#include "../io.h"
#include "../rws.h"
#include "../ioc_struc.h"
#include "../system/file.h"
#define vk_printf(...) ffly_fprintf(vkout,__VA_ARGS__)
extern void *vkout;
/*
	GOALS -
		(1)client must be able to interact with the main reception without holdup.
		(2)client must not be heldup, by things such as pageswaps or other.
*/

/*
	DESIGN RULE:
		cursor must be drawn by cpu, unless 
		fast hardware alternative exists,
		RULE: cursor image data is not to be issued to gpu unless size is over X*X amount otherwise overhead will cause more time to be taken

	AMDGPU:
		reading from gpu memory takes more time then writing  - cpu mapping.

*/
#define VK_DRV_FBDEV	0
#define VK_DRV_AMDGPU	1
extern struct vk_patch *vk_pt;
extern void *cursor_buffer[8];
//screen.c
void vk_scrn_init(struct vk_scrn*);
struct vk_scrn *getscrn(_int_s,_int_s);
void vk_scrninit(struct vk_scrninfo*);
struct vk_msg* vk_msgalloc(void);
void vk_msgfree(struct vk_msg *m);
//log.c
void vk_log_init(void);

//display.c
struct vk_display_mode* vk_findbestmode(struct vk_display_mode*, _int_u);

//message.c
struct vk_msg* vk_msgnew(struct vk_context*);
void vk_msg_in(struct vk_msg*);
void pt_move(struct vk_patch *__pt, _int_u __x, _int_u __y) ;
//patch.c
struct vk_patch* pt_here(_int_u,_int_u);
void pt_place(struct vk_patch*);
void pt_rm(struct vk_patch*);

void vk_leavescreen(struct vk_cursor*);
void vk_fbdev_sweep(struct vk_context*);
void vk_amdgpu_sweep(struct vk_context*);
extern struct vk_drv vk_drvtab[2];
struct vk_dev *vk_devloopup(_32_u);
void vk_devadd(void*,_int_u,struct vk_dev*);
void vk_deinit(void);
void vk_probe(void);
void vk_scrn_newcords(struct vk_scrn*,_int_u,_int_u);
void evdev_init(void);
void evdev_deinit(void);
struct vk_iodrv extern evdev_drv;
int sendfd(int,int);
int recvfd(int);
void vk_drawpixs(void *__junk, struct vk_drawable *__dst, _8_u *__src, _int_u __width, _int_u __height, _int_u __soff, _int_u __doff, _int_u __w);
//render.c
extern struct vk_context vk_ctx;
void vk_gstart(struct vk_scrn*);
void vk_gend(void);
void pt_draw(struct vk_patch *__pt);
void pt_readback(struct vk_patch *__pt);
void pt_clear(struct vk_patch *__pt);
void vk_pt_unhold(void);
void vk_msgclient(struct vk_client_priv *cl, struct vk_msg *b,_16_u);
# endif /*__vk__common__h*/
