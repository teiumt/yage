#ifndef __subs__h
#define __subs__h
#include "common.h"
//substitutes
struct sst_scrninfo {
	_16_u width, height;
};
/*
	this screen structure is common to all contractors
*/
struct sst_scrn {
	struct sst_scrninfo info;
};

#endif/*__subs__h*/
