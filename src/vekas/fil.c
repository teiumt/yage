#include "fil.h"
#include "../io.h"
#include "../types.h"
#include "../thread.h"
#include "../ffly_def.h"
#include "../time.h"
#include "../y.h"
#include "../linux/stat.h"
#include "../linux/types.h"
#include "../linux/socket.h"
#include "../inet.h"
#include "../linux/errno-base.h"
#include "../linux/un.h"
#include "../linux/unistd.h"
#include "../assert.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../linux/net.h"
#include "../linux/in.h"

void static initbunk(struct fil_bunk *__bnk) {
	y_list_init(&__bnk->inbound);
	y_list_init(&__bnk->outbound);
	__bnk->ib_lock = MUTEX_INIT;
	__bnk->ob_lock = MUTEX_INIT;
}
void fil_strcinit(struct fil_struc *__fil) {
	initbunk(__fil->bunks);
	initbunk(__fil->bunks+1);
	initbunk(__fil->bunks+2);
	initbunk(__fil->bunks+3);
	__fil->in_lock = MUTEX_INIT;
	__fil->out_lock = MUTEX_INIT;
	__fil->flags = 0;
}
#include "../mutex.h"
void static _sndpkt(struct fil_struc *__stc,struct fil_pkt *__pkt) {
	printf("writing: %u\n",__pkt->size+sizeof(struct fil_pkt));
	ffly_fdrain(_ffly_out);
	int r;
	r = write(__stc->fds[1],__pkt,sizeof(struct fil_pkt));
	if(r != sizeof(struct fil_pkt)){
		printf("dident write everything out.\n");
		ffly_fdrain(_ffly_out);
	}

	if(__pkt->size>0) {
		r = write(__stc->fds[1],((_8_u*)__pkt)+sizeof(struct fil_pkt),__pkt->size);
	}
	printf("PACKET-WRITE: %u, %u.\n",__pkt->size, __pkt->bunk);
	ffly_fdrain(_ffly_out);
	/*
		pipes dont allow for read of less then BUFSIZE(what written),

		so 
		write(10-bytes);
		read(5-bytes);

		the other 5-bytes will be discarded / thrown out
	*/
}
/*
	NON BLOCKING? function.
*/
#include "../tmu.h"
#include "../time.h"
void fil_sndoff(struct fil_struc *__stc) {
	if(mt_trylock(&__stc->out_lock) == -1)
		return;
	_int_u bunk;
	bunk = 0;
	struct fil_bunk *bnk;
	struct fil_pkt *head = NULL,*tail = NULL;
	for(;bunk != 4;bunk++) {
		bnk = __stc->bunks+bunk;
		//if the head of the list is null just skip it.
		if(!bnk->outbound.head)
			continue;

		if(!mt_trylock(&bnk->ob_lock)){
			if(!head) {
				head = bnk->outbound.head;
			}
			if(tail != NULL) {
				tail->ent.next = bnk->outbound.head;
			}
			tail = bnk->outbound.tail;
			bnk->outbound.head = NULL;
			bnk->outbound.tail = NULL;
			mt_unlock(&bnk->ob_lock);
		}
	}

	/*
		now we have accumulated all packets we are to send them on mary way
	*/
	struct fil_pkt *pkt;
	while(head != NULL) {
		printf("HEAD-size: %u, packet destend for bunk(%u)\n",head->size,head->bunk);
		ffly_fdrain(_ffly_out);

		_sndpkt(__stc,head);
		head = (pkt = head)->ent.next;
		printf("--- FREE_PTR: %p, NEXT: %p\n",pkt,head);

		m_free(pkt);
	}
	mt_unlock(&__stc->out_lock);
}

_8_s static _fil_poll(int fd);

void static filcoms(struct fil_struc *__stc){
	fil_sndoff(__stc);
	if(mt_trylock(&__stc->in_lock) == -1) {
//		printf("failed to lock.\n");
		return;
	}

	if(_fil_poll(__stc->fds[0]) == -1){
//		printf("poll failed.\n");
		mt_unlock(&__stc->in_lock);
		return;	
	}
	printf("recving data.\n");
	fil_rcvfromall(__stc);	
}

void fil_sndpkt(struct fil_struc *__stc,struct fil_pkt *__pkt){
	/*
		FIXME:
	*/
	_int_u size;
	size = sizeof(struct fil_pkt)+__pkt->size;

	struct fil_pkt *pkt;
	pkt = m_alloc(size);
	printf("PTR_OF_PACKET: %p, dest-bunk(%u).\n",pkt,__pkt->bunk);
	mem_cpy(pkt,__pkt,size);

	struct fil_bunk *bnk = __stc->bunks+__pkt->bunk;
	mt_lock(&bnk->ob_lock);
	y_list_insert(&bnk->outbound,&pkt->ent);	
	mt_unlock(&bnk->ob_lock);
}
_8_s fil_rcvpkt(struct fil_struc *__stc,_64_u __bunk,struct fil_pkt **__pkt, _int_u __size) {
	printf("waiting for packet for bunk(%u) of size: %u.\n",__bunk,__size);
	*__pkt = NULL;
	struct fil_pkt *pkt;
	struct fil_bunk *bnk = __stc->bunks+__bunk;
	/*
		check if any packet for this bunk had been received prior
	*/
	while(!bnk->inbound.head){
		if(__stc->flags&FIL_INTERRUPT){
			printf("got interrupted.\n");
			return -1;
		}
		filcoms(__stc);
		printf("waiting for...\n");	
	}
	printf("caught somthing.\n");
	ffly_fdrain(_ffly_out);
	mt_lock(&bnk->ib_lock);
	pkt = bnk->inbound.head;
	y_list_pickoff(&bnk->inbound);
	*__pkt = pkt;
	if(!bnk->inbound.head){
		__stc->flags &= ~(2<<__bunk);
	}
	mt_unlock(&bnk->ib_lock);
	assert(pkt->size == __size);
	printf("recved packet for bunk(%u) of size %u.\n",__bunk,__size);
	ffly_fdrain(_ffly_out);
	return 0;
}
#include "../linux/poll.h"
/*
	the packet might have got caught up in someone elses mess
*/
_8_s fil_existentdata(struct fil_struc *__stc, _64_u __bunk) {
		return !__stc->bunks[__bunk].inbound.head?-1:0;
}
_8_s _fil_poll(int fd){
 	struct pollfd pfd = {
    .fd = fd,
    .events = POLLIN,
    .revents = 0
  };
  if (poll(&pfd,1,1)<=0) {
		return -1;
  }
	return 0;
}

_8_s fil_pollin(struct fil_struc *__stc) {
	return _fil_poll(__stc->fds[0]);
}

_8_s fil_rcvp(struct fil_struc *__stc,_64_u __bunk,void *__buf,_int_u __size) {
	/*
		TODO:
			if we dont find packet ie no once else got there before us,
			then pass and write data directly to __buf
	*/
	struct fil_pkt *fpkt;
	if(fil_rcvpkt(__stc,__bunk,&fpkt,__size) == -1) return -1;
	if(!fpkt->data && __size != 0) {
		printf("masive fucking issue.\n");
	}else if (!__size){
	}else{
		mem_cpy(__buf,fpkt->data,__size);
	}
	fil_pktfree(fpkt);
}

#include "../sched.h"
/*
	NOTE: only here for cases where no recv requests are put forward.
*/
_8_s static _fil(sched_entityp __ent) {
	filcoms((struct fil_struc*)__ent->arg);
	return -1;
}

void fil_start(struct fil_struc *__stc) {
	sched_entityp ent;
	ent = sched_new();
	ent->type = _sched_just_run;
	ent->arg = __stc;
	ent->func = _fil;
	sched_submit(ent);
}
/*
	free the packet and data if any
*/
void fil_pktfree(struct fil_pkt *__pkt) {
	if(__pkt->data != NULL){
		m_free(__pkt->data);
	}
	m_free(__pkt);
}
void fil_rcvfromall(struct fil_struc *__stc){
	struct fil_pkt *pkt;
	pkt = m_alloc(sizeof(struct fil_pkt));
	pkt->data = NULL;
	int r;
	struct fil_bunk *bnk;
	r = read(__stc->fds[0],pkt,sizeof(struct fil_pkt));
	if(r != sizeof(struct fil_pkt)) {
		printf("incompleted packet, expected %u but got: %u.\n",sizeof(struct fil_pkt),r);
		assert(1 == 0);
	}
	printf("read: %u\n",sizeof(struct fil_pkt));	
	ffly_fdrain(_ffly_out);

	printf("read: %u for bunk(%u)\n",pkt->size,pkt->bunk);	
	ffly_fdrain(_ffly_out);

	if(pkt->size>0) {
		pkt->data = m_alloc(pkt->size);
		printf("recved data of %u-bytes in size\n",pkt->size);
		r = read(__stc->fds[0],pkt->data,pkt->size);
		if(r != pkt->size) {
			printf("FIL packet read got cut short.\n");
			assert(1 == 0);
		}
	}
	mt_unlock(&__stc->in_lock);
	bnk = __stc->bunks+pkt->bunk;
	mt_lock(&bnk->ib_lock);
	y_list_insert(&bnk->inbound,&pkt->ent);
	mt_unlock(&bnk->ib_lock);
	printf("#### another round.\n");
	ffly_fdrain(_ffly_out);
	__stc->flags |= (2<<pkt->bunk);
}

