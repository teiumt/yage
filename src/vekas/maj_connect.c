#include "maj.h"
#include "../linux/types.h"
#include "../linux/socket.h"
#include "../inet.h"
#include "../linux/unistd.h"
#include "../io.h"
#include "../m_alloc.h"
#include "../string.h"
#include "../linux/net.h"
#include "../linux/in.h"
#include "../in.h"
#include "../assert.h"
#include "../system/errno.h"
#define CLNT_SOCKNAME "/opt/yage/vekas_clnt.skt"
#define SOCKNAME "/opt/yage/vekas.skt"
struct maj_conn *maj_con;
#include "../graph.h"
static struct graph g;

#define SOCKNAME_COMS "/opt/yage/pcom.skt"
struct maj_conn *maj_connect(void) {
	graph_open(&g,"swapbuf.plt");
	struct maj_conn *con;

	con = m_alloc(sizeof(struct maj_conn));
	con->u_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if(con->u_fd<0) {
		printf("failed to open UNIX socket.\n");
		return NULL;
	}

	struct sockaddr_un u_adr, s_adr;
	u_adr.sun_family = AF_UNIX;
	mem_cpy(u_adr.sun_path,CLNT_SOCKNAME,sizeof(CLNT_SOCKNAME));
	unlink(CLNT_SOCKNAME);

	if(bind(con->u_fd, (struct sockaddr*)&u_adr, sizeof(struct sockaddr_un))<0) {
		printf("failed to bind socket, %s\n",strerror(errno));
		return NULL;
	}
	s_adr.sun_family = AF_UNIX;
	mem_cpy(s_adr.sun_path,SOCKNAME,sizeof(SOCKNAME));

	if(connect(con->u_fd,(struct sockaddr*)&s_adr,sizeof(struct sockaddr_un))<0) {
		printf("failed to connect, %s\n",strerror(errno));
		return NULL;
	}

	/*
		connection with vekas patch contractor
	*/
	con->fd = socket(AF_UNIX, SOCK_STREAM, 0);

	struct sockaddr_un *adr = &con->adr;
	adr->sun_family = AF_UNIX;
	mem_cpy(adr->sun_path,SOCKNAME_COMS,sizeof(SOCKNAME_COMS));
	
	if(connect(con->fd, &con->adr, sizeof(struct sockaddr_un))<0) {
		printf("failed to connect to" SOCKNAME_COMS ".\n");
		return NULL;
	}

	_maj_recv(con->fd,&con->i,sizeof(struct maj_initial));
	printf("INITIAL: %u.\n",con->i.adrnow);
	maj_con = con;
	return con;
}

void maj_ptgrab(struct maj_conn *__c, struct maj_patch *__pt,_32_u __xdis,_32_u __ydis){
	_8_u code[32];
	*(_32_u*)code = 13;//length of tape
	*(_8_u*)(code+4) = 13;
	*(_32_u*)(code+5) = __pt->adr;
	*(_32_u*)(code+9) = __xdis;
	*(_32_u*)(code+13) = __ydis;
	_maj_send(__c->fd, code, 17);
}

void maj_ptungrab(struct maj_conn *__c, struct maj_patch *__pt){
  _8_u code[32];
  *(_32_u*)code = 1;//length of tape
  *(_8_u*)(code+4) = 15;
  _maj_send(__c->fd, code, 5);
}
void maj_ptconjoin(struct maj_conn *__c, _32_u __a,_32_u __b){
	_8_u code[32];
	*(_32_u*)code = 9;//length of tape
	*(_8_u*)(code+4) = 14;
	*(_32_u*)(code+5) = __a;
	*(_32_u*)(code+9) = __b;
	_maj_send(__c->fd, code, 13);
}

void maj_patch(struct maj_conn *__c, struct maj_patch *__pt, _int_u __x, _int_u __y, _int_u __width, _int_u __height, _32_u __flags) {
	_8_u code[29];

	*(_32_u*)code = 25;//length of tape
	*(_8_u*)(code+4) = 0x01;
	*(_32_u*)(code+5) = __x;
	*(_32_u*)(code+9) = __y;
	*(_32_u*)(code+13) = __width;
	*(_32_u*)(code+17) = __height;
	*(_32_u*)(code+21) = __c->i.adrnow;
	*(_32_u*)(code+25) = __flags;
	
	__pt->x = __x;
	__pt->y = __y;
	__pt->width = __width;
	__pt->height = __height;
	_maj_send(__c->fd, code, 29);
	__pt->adr = __c->i.adrnow;
	__c->patches[__pt->adr&63] = __pt;	
	__c->i.adrnow+=1;
}

void maj_exit(struct maj_conn *__c) {
	_8_u code[5];
	*(_32_u*)code = 1;
	*(_8_u*)(code+1) = 0x00;
	_maj_send(__c->fd, code, 5);
}

void maj_ptdestroy(struct maj_conn *__c, struct maj_patch *__pt) {
	_8_u code[9];
	*(_32_u*)code = 5; 
	*(_8_u*)(code+4) = 0x03;
	*(_32_u*)(code+5) = __pt->adr;
	_maj_send(__c->fd, code, 9);
}

void maj_frame(struct maj_conn *__c, struct maj_patch *__pt, void *__buf) {
	_8_u code[9];
	*(_32_u*)code = 5;
	*(_8_u*)(code+4) = 0x02;
	*(_32_u*)(code+5) = __pt->adr;
	printf("patch: %u, %u.\n", __pt->width, __pt->height);
	_maj_send(__c->fd, code, 9);
	maj_send(__c->fd, __buf, __pt->width*__pt->height*4);
}

void maj_msg(struct maj_conn *__c, struct maj_patch *__pt, void *__buf, _int_u __size) {
	_8_u code[9];
	*(_32_u*)code = 5;
	*(_8_u*)(code+4) = 0x04;
	*(_32_u*)(code+5) = 0;
	_maj_send(__c->fd, code, 9);
	_maj_recv(__c->fd, __buf, 4);
	if(*(_32_u*)__buf > 0) {
	_maj_recv(__c->fd, ((_8_u*)__buf)+4, (*(_32_u*)__buf)*sizeof(struct vk_msg));
	} else {
//		printf("no messages.\n");
	}
}
#include "../drm.h"
void maj_auth(struct maj_conn *__c, int fd) {
	int unsigned magic;
	if(drm_get_magic(fd, &magic)<0) {
		printf("failed to get magic.\n");
	}
	printf("AUTH MAGIC: --> %u.\n",magic);
	_8_u code[20];
	*(_32_u*)code = 5;
	*(_8_u*)(code+4) = 0x05;
	*(_32_u*)(code+5) = (_32_u)magic;
	_maj_send(__c->fd, code, 9);
}

void maj_ctxnew(struct maj_conn *__c) {

}
void maj_ctxdestroy(struct maj_conn *__c) {

}

void maj_ctxmake(struct maj_conn *__c, _32_u __ctx) {
	_8_u code[9];
	*(_32_u*)code = 5;
	*(_8_u*)(code+4) = 0x08;
	*(_32_u*)(code+5) = __ctx;
	_maj_send(__c->fd, code, 9);
}
void maj_ctxsetdev(struct maj_conn *__c, _32_u __drv, _32_u __dev) {
	_8_u code[20];
	*(_32_u*)code = 9;
	*(_8_u*)(code+4) = 0x09;
	*(_32_u*)(code+5) = __drv;
	*(_32_u*)(code+9) = __dev;
	_maj_send(__c->fd, code, 13);
}

void maj_ctxsetpt(struct maj_conn *__c,struct maj_patch *__pt) {
	_8_u code[20];
	*(_32_u*)code = 5;
	*(_8_u*)(code+4) = 11;
	*(_32_u*)(code+5) = __pt->adr;
	_maj_send(__c->fd, code, 9);
}
void maj_ptconfig(struct maj_conn *__c, _32_u __config) {
	_8_u code[20];
	*(_32_u*)code = 5;
	*(_8_u*)(code+4) = 12;
	*(_32_u*)(code+5) = __config;
	_maj_send(__c->fd, code, 9);
}
#include "../linux/time.h"
void maj_swapbufs(struct maj_conn *__c) {
	
	struct timespec start,stop;
	clock_gettime(CLOCK_MONOTONIC,&start);
	
	_8_u code[5];
	*(_32_u*)code = 1;
	*(_8_u*)(code+4) = 10;
	_maj_send(__c->fd, code, 5);

	_8_u dummy;
	_maj_recv(__c->fd,&dummy,1);
}


void maj_close(struct maj_conn *__con) {
	graph_close(&g);
	shutdown(__con->fd, SHUT_RDWR);
	close(__con->fd);
	m_free(__con);
}
