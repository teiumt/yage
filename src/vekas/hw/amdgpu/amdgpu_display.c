#include "amdgpu.h"
#include "../../../io.h"
#include "../../common.h"
#include "../../../m_alloc.h"
#include "../../../drm.h"
#include "../../../drm_mode.h"

#define copy(__d, __s, __what)\
	__d->__what = __s->__what;
void vk_amdgpu_fromkmode(struct vk_display_mode *__dst, struct drm_mode_modeinfo *__src) {
	copy(__dst, __src, clock)
	copy(__dst, __src, hdisplay)
	copy(__dst, __src, hsync_start)
	copy(__dst, __src, hsync_end)
	copy(__dst, __src, htotal)
	copy(__dst, __src, hskew)
	copy(__dst, __src, vdisplay)
	copy(__dst, __src, vsync_start)
	copy(__dst, __src, vsync_end)
	copy(__dst, __src, vtotal)
	copy(__dst, __src, vscan)
	copy(__dst, __src, vrefresh)
	copy(__dst, __src, flags)
}

void vk_amdgpu_tokmode(struct vk_display_mode *__src, struct drm_mode_modeinfo *__dst) {
	copy(__dst, __src, clock)
	copy(__dst, __src, hdisplay)
	copy(__dst, __src, hsync_start)
	copy(__dst, __src, hsync_end)
	copy(__dst, __src, htotal)
	copy(__dst, __src, hskew)
	copy(__dst, __src, vdisplay)
	copy(__dst, __src, vsync_start)
	copy(__dst, __src, vsync_end)
	copy(__dst, __src, vtotal)
	copy(__dst, __src, vscan)
	copy(__dst, __src, vrefresh)
	copy(__dst, __src, flags)
}


void vk_amdgpu_detect(struct vk_output *__out) {

}

struct vk_display_mode*
vk_amdgpu_get_modes(struct amdgpu_dev *dv,struct vk_output *__out, _int_u *__n) {
	
	struct drm_mode_ctr ctr;
	//get crt
	drmmode_ctr_get(&ctr, dv->fd, __out->id);
	if (!ctr.count_modes)
		return NULL;

	struct vk_display_mode *modes = m_alloc(ctr.count_modes*sizeof(struct vk_display_mode));
	*__n = ctr.count_modes;
	_int_u i;
	i = 0;
	for(;i != ctr.count_modes;i++) {
		struct vk_display_mode *mode = modes+i;
		struct drm_mode_modeinfo *inf;
		inf = ctr.modes+i;
		vk_amdgpu_fromkmode(mode,inf);
	}
	return modes;
}

void vk_amdgpu_pageflip(struct vk_scrn *__s) {
	while(!(__s->crtc->bits&CRTC_PAGEIDLE)){
	printf("PAGEFLIP.\n");
	}
	
	struct amdgpu_ent *ent = __s->priv;
	struct amdgpu_dev *dev = ent->pdv;
	struct amdgpu_fb *fb = __s->fb;
	/*
		replace current fb of a crtc with another
	*/
	drmmode_page_flip(dev->fd,__s->crtc->id,fb->fb.front_id,DRM_MODE_PAGE_FLIP_EVENT,NULL);
	__s->crtc->bits &= ~CRTC_PAGEIDLE;
}
#include "../../../sched.h"
#include "../../../linux/poll.h"
_8_s vk_amdgpu_events(sched_entityp ent){
	struct vk_dev *dev = ent->arg;
	struct pollfd pfd = {
		.fd = dev->fd,
		.events = POLLIN,
		.revents = 0
  };
	if(poll(&pfd,1,1)<=0) {
		return -1;
	}

	char buffer[1024];
	int len;
	len = drm_event_handle(dev->fd,buffer);
	if(len>0){
		_int_u i = 0;
		for(;i<len;) {
			struct drm_event *e = buffer+i;
			switch(e->type) {
				case DRM_EVENT_FLIP_COMPLETE:
					dev->crts->bits |= CRTC_PAGEIDLE; 	
				break;
			}
			i+=e->length;
		}
	}
	return -1;
}
void vk_amdgpu_cursor_show(struct amdgpu_ent *ent, struct vk_crtc *__crtc) {
	struct vk_cursor *c = __crtc->scrn->cursor;
	if (!c){
		vk_printf("so cursor present for CRTC.\n");
		return;
	}
	struct amdgpu_pixmap *pm = cursor_buffer[c->id];
	vk_printf("crtc%u set with cursor%u and bo %u.\n",__crtc->id,c->id,pm->bo->bo.handle);
	if(drmmode_cursor_set(ent->pdv->fd,__crtc->id,pm->bo->bo.handle,pm->width,pm->height)<0) {
		vk_printf("failed to set cursor.\n");
	}
}
#include "../../../string.h"
void vk_amdgpu_cursor_load(struct amdgpu_ent *ent, struct vk_cursor *__c) {
	struct amdgpu_pixmap *pm;
	struct bitimage *img = __c->img;
	pm = vk_amdgpu_pixmap_new(ent->pdv, CIK_CURSOR_WIDTH,CIK_CURSOR_HEIGHT);
	cursor_buffer[__c->id] = pm;

	_int_u x,y;
	vk_printf("CURSOR_LOAD: %ux%u.\n",img->width,img->height);
	_8_u *cpu_ptr = pm->bo->bo.cpu_ptr;
	mem_set(cpu_ptr,0,CIK_CURSOR_WIDTH*CIK_CURSOR_HEIGHT*4);
	y = 0;
	for(;y != img->height;y++) {
		x = 0;
		for(;x != img->width;x++) {
			_8_u *bit = img->bitmap+((x+(y*img->width))*4);
			_8_u *c = cpu_ptr+((x+(y*CIK_CURSOR_WIDTH))*4);
			c[0] = bit[0];
			c[1] = bit[1];
			c[2] = bit[2];
			c[3] = bit[3];
		}
	}
}

void vk_amdgpu_cursor_move(struct amdgpu_ent *ent, struct vk_crtc *__crtc, _32_s __x, _32_s __y) {
	vk_printf("moving cursor for crtc: %u.\n",__crtc->id);
	if (drmmode_cursor_move(ent->pdv->fd,__crtc->id,__x,__y)<0) {
		vk_printf("AMDGPU: failed to move cursor.\n");
	}
}

