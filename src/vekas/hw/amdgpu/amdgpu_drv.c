#include "amdgpu.h"
#include "../../../io.h"
#include "../../common.h"
#include "../../../m_alloc.h"
#include "../../../drm.h"
#include "../../../drm_mode.h"
#define DRV vk_drvtab[VK_DRV_AMDGPU]
struct vk_fbdev* vk_fbdev_open(struct vk_context *__ctx);
extern struct vk_gc amdgpu_gc;
static struct amdgpu_ent *ent;
static struct vk_dev *vd;
#include "../../../sched.h"
_8_s vk_amdgpu_events(sched_entityp);
void vk_amdgpu_device(struct amdgpu_dev *__bo) {
	ent = m_alloc(sizeof(struct amdgpu_ent));

	struct vk_drv *vdr = &DRV;

	vdr->ndevs = 1;
	vdr->devs = m_alloc(sizeof(struct vk_dev));
	printf("AMDGPU sweep.\n");
	vd = vdr->devs;
	vd->fd = __bo->fd;

	ent->pdv = __bo;
	vd->priv = ent;
	vd->priv0 = __bo;

//	vk_devadd("card0",5, vd);
}
void vk_amdgpu_sweep(struct vk_context *__ctx) {
/*
	sweep all devices that use this driver
*/
	int fd;
    fd = ffly_drm_open("/dev/dri/card0");
    drm_master_set(fd);
	struct amdgpu_dev *dv;
	dv = amdgpu_dev_init(fd);
	vk_amdgpu_device(dv);

	struct rd_dev *rd;
	rd = m_alloc(sizeof(struct rd_dev));

	vk_devadd("card0",5, vd);
	ent->g_dev = rd_drv_dev(rd,dv);	
/*
	struct vk_fbdev *fb;
	fb = vk_fbdev_open(__ctx);
	fb->s.g = &amdgpu_gc; 
	fb->s.priv = drv;
*/
	struct drm_mode_res rs;
    drmmode_rs_get(&rs, fd);
	drmmode_disrs(&rs);
	_int_u i;
	i = 0;
	_32_u id;
	vd->crts = m_alloc(rs.rs.count_crtcs*sizeof(struct vk_crtc));
	vd->ncrts = rs.rs.count_crtcs;
	for(;i != rs.rs.count_crtcs;i++) {
		struct vk_crtc *c;
		c = vd->crts+i;
		printf("amdgpu-crtc: %u.\n",id = ((_32_u*)rs.rs.crtc_id_ptr)[i]);
		c->id = id;
		c->bits = CRTC_PAGEIDLE;
	}

	vd->out = m_alloc(sizeof(struct vk_output)*rs.rs.count_connectors);
	vd->nouts = rs.rs.count_connectors;
	i = 0;
	for(;i != rs.rs.count_connectors;i++) {
		printf("amdgpu-connector: %u.\n",id = ((_32_u*)rs.rs.connector_id_ptr)[i]);
		struct drm_mode_ctr crt;
		drmmode_ctr_get(&crt,fd,id);
	//	drmmode_discrt(&crt);

		struct drm_mode_get_connector cntr;
		drmmode_connector_get(&cntr,fd,id);
		drmmode_disctr(&cntr);
		struct vk_output *out = vd->out+i;
		out->id = id;
	}
	sched_entityp en;
	en = sched_new();
	en->type = _sched_just_run;
	en->arg = vd;
	en->func = vk_amdgpu_events;
	sched_submit(en);


}
