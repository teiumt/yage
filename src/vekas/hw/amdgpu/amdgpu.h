#ifndef __vk__amdgpu__h
#define __vk__amdgpu__h 
#include "../../../ffly_def.h"
#include "../../common.h"
#include "../../../amdgpu.h"
#include "../../../drm_mode.h"
#include "../../../flue/common.h"
#include "../../flue/struc.h"
#include "../../../flue/radeon/common.h"

/*
	defined - 'linux/drivers/drm/amd/amdgpu/amdgpu.h'
	
	the is the prefixed size for all cursors that are in memory,
	say we have a 16x16 cursor the memory is inhabits is CIK_CURSOR_WIDTH by CIK_CURSOR_HEIGHT
	(in pixels)
*/
#define CIK_CURSOR_WIDTH 128
#define CIK_CURSOR_HEIGHT 128

/*
	if we use flue as the graphics drivers we need to be able to turn a shared BO into a texture
*/
struct amdgpu_ent {
	/*
		phyisical device
	*/
	struct amdgpu_dev *pdv;
	struct flue_dev *g_dev;
};

struct amdgpu_dw {
	//keep at top of structure
	struct vk_drawable d;
	struct vf_dwstrc vf;

	struct amdgpu_buffer *buffer;
};


/*
    NOTE:
        a buffer object can be used for multible framebuffers i think?
        but also same goes for crtc
*/

struct amdgpu_fb{
	struct vk_fb fb;
	_int_u width, height;

	struct amdgpu_dw *dw;
};

struct amdgpu_buffer {
	union {
		struct amdgpu_bo bo;
		struct rd_bo rd;
	};
	struct amdgpu_va *v;
};

struct amdgpu_pixmap {
	struct amdgpu_buffer *bo;
	_int_u width,height;
};
void
amdgpu_shared_backing(struct amdgpu_ent *ent, struct amdgpu_dw *__dw, int, int, _32_u __width, _32_u __height);

//AMDGPU_FLUE
void amdgpu_drawable_flue_bo(struct amdgpu_ent *ent, struct amdgpu_dw *__dw, _32_u __width, _32_u __height);
void amdgpu_shared_backing_flue(struct amdgpu_ent *ent, struct amdgpu_dw *__dw, int,int, _32_u __width, _32_u __height);
void vk_amdgpu_update(void);
void vk_amdgpu_fromkmode(struct vk_display_mode *__dst, struct drm_mode_modeinfo *__src);
void vk_amdgpu_tokmode(struct vk_display_mode *__src, struct drm_mode_modeinfo *__dst);

void vk_amdgpu_crtc_mode_set(struct amdgpu_ent *,struct amdgpu_dev *,struct vk_crtc *,struct vk_display_mode *,struct vk_output **,_int_u,struct amdgpu_fb*);
struct vk_display_mode*
vk_amdgpu_get_modes(struct amdgpu_dev*,struct vk_output*,_int_u*);
struct amdgpu_buffer* vk_amdgpu_buffer_new(struct amdgpu_dev *__dv, _int_u __size);
struct amdgpu_pixmap* vk_amdgpu_pixmap_new(struct amdgpu_dev *__dv, _int_u __width,_int_u __height);
#endif/*__vk__amdgpu__h*/
