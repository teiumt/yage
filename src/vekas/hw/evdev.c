# include "../common.h"
# include "../../evdev/evdev.h"
# include "../../types.h"
# include "../../io.h"
# include "../../linux/unistd.h"
# include "../../linux/fcntl.h"
# include "../../linux/input-event-codes.h"
# include "../../string.h"
static struct evdev *m;
static struct evdev *k;
static _8_u keymap[512] = {
	[KEY_A] = VK_KEY_A,
	[KEY_B] = VK_KEY_B,
	[KEY_C] = VK_KEY_C,
	[KEY_D] = VK_KEY_D,
	[KEY_E] = VK_KEY_E,
	[KEY_F] = VK_KEY_F,
	[KEY_G] = VK_KEY_G,
	[KEY_H] = VK_KEY_H,
	[KEY_I] = VK_KEY_I,
	[KEY_J] = VK_KEY_J,
	[KEY_K] = VK_KEY_K,
	[KEY_L] = VK_KEY_L,
	[KEY_M] = VK_KEY_M,
	[KEY_N] = VK_KEY_N,
	[KEY_O] = VK_KEY_O,
	[KEY_P] = VK_KEY_P,
	[KEY_Q] = VK_KEY_Q,
	[KEY_R] = VK_KEY_R,
	[KEY_S] = VK_KEY_S,
	[KEY_T] = VK_KEY_T,
	[KEY_U] = VK_KEY_U,
	[KEY_V] = VK_KEY_V,
	[KEY_W] = VK_KEY_W,
	[KEY_X] = VK_KEY_X,
	[KEY_Y] = VK_KEY_Y,
	[KEY_Z] = VK_KEY_Z,	
	[BTN_LEFT] = VK_BTN_LEFT,
	[BTN_RIGHT] = VK_BTN_RIGHT,
	[KEY_ESC] = VK_ESC,
	[KEY_SPACE] = VK_KEY_SPACE,
	[KEY_LEFTSHIFT] = VK_KEY_LEFTSHIFT,
	[KEY_UP] = VK_KEY_UP,
	[KEY_DOWN] = VK_KEY_DOWN,
	[KEY_LEFT] = VK_KEY_LEFT,
	[KEY_RIGHT] = VK_KEY_RIGHT,
	[KEY_F1] = VY_F1,
	[KEY_F2] = VY_F2,
	[KEY_F3] = VY_F3,
	[KEY_F4] = VY_F4,
	[KEY_F5] = VY_F5,
	[KEY_F6] = VY_F6,
	[KEY_F7] = VY_F7,
	[KEY_F8] = VY_F8,
	[KEY_F9] = VY_F9,
	[KEY_F10] = VY_F10,
	[KEY_F11] = VY_F11,
	[KEY_F12] = VY_F12,
	[KEY_BACKSPACE] = VY_KEY_BACKSPACE,
	[KEY_DOT]		= VY_DOT,
	[KEY_SLASH]		= VY_SLASH
};

static _8_u valmap[512] = {
	VK_RELEASE,
	VK_PRESS
};
/*
	linux/Doc/input/devices/sentelic.rst
	says a (1) indecated a press and 0 released
	i dont know about other devices know

	NOTE: this was a lazy grep on all the drivers as i dont have time
*/
void static dump(struct vk_context *__ctx, struct evdev *d) {
	_8_s r;
	struct input_event evbuf[EVDEVBUF_SZ];
	mem_set(evbuf, 0, EVDEVBUF_SZ*sizeof(struct input_event));
	_int_u n;
	n = evdev_bigdrop(d,evbuf);
//	printf("READ: %u-events.\n",n);
	mt_lock(&__ctx->lock);
	_int_u i;
	i = 0;
	_8_s rel = -1;
	struct timeval tv;
	for(;i != n;i++) {
		struct input_event *ev;
		ev = evbuf+i;

		if (ev->type == EV_REL) {
			switch(ev->code) {
				case REL_X:
					if ((ev->value<0 && (vk_ctx.movmask&VK_MM_LEFT)) || (ev->value>0 && (vk_ctx.movmask&VK_MM_RIGHT)))
						__ctx->x+=ev->value;
				break;
				case REL_Y:
					if ((ev->value<0 && (vk_ctx.movmask&VK_MM_TOP)) || (ev->value>0 && (vk_ctx.movmask&VK_MM_BOTTOM)))
						__ctx->y+=ev->value;
				break;
				default:
					goto _end;
			}

			struct vk_msg *m;
			m = vk_msgnew(__ctx);
			if(!m)break;
			m->value = VK_PTR_M;
			m->code = ~0;
			m->x = __ctx->x;
			m->y = __ctx->y;
//			printf(">++++%d.\n",ev->value);		
		} else if (ev->type == EV_KEY) {
			struct vk_msg *m;
			m = vk_msgnew(__ctx);
			if(!m)break;
	
			if (ev->code<512) {
			m->code = keymap[ev->code];
			} else
				m->code = ~0;
			if (ev->value<2) {
			m->value = valmap[ev->value];
			} else
				m->value = ~0;
			m->x = __ctx->x;
			m->y = __ctx->y;
		}else if (ev->type == EV_SYN) {
//			printf("SYNC.\n");
		} else {
//			printf("UNKNOWN.\n");
		}
	_end:
		tv = ev->time;
	}

	mt_unlock(&__ctx->lock);
}

void static update(struct vk_context *__ctx) {
	dump(__ctx, m);
	dump(__ctx, k);
}

struct vk_iodrv evdev_drv = {
	update
};

void evdev_init(void) {
	int fd;
	//mouse
	fd = open("/dev/input/event4", O_RDONLY|O_NONBLOCK, 0);
	if (fd<0) {
		printf("failed to open.\n");
		return;
	}

	m = evdev_from_fd(fd);
	//keyborad
	fd = open("/dev/input/event2", O_RDONLY|O_NONBLOCK, 0);
	if (fd<0) {
		printf("failed to open.\n");
		return;
	}
	k = evdev_from_fd(fd);
}
void evdev_deinit(void) {
	close(m->fd);
	close(k->fd);
	evdev_free(m);
	evdev_free(k);
}
