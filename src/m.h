# ifndef __m__h
# define __m__h
# include "y_int.h"

struct mstruc {
#ifdef __f_debug
	_64_u used;
	//active allocations
	_64_u aa;
#endif
	void*(*memalloc)(_64_u);
	void(*memfree)(void*);
	void*(*memrealloc)(void*, _64_u);
};

struct meminfo {
	_int_u used;
	_int_u aa
};


// display memory info
void dmi(void);
void meminfo(struct meminfo*);
extern struct mstruc _f_m;
# endif /*__m__h*/
