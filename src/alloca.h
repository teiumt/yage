# ifndef __f__alloca__h
# define __f__alloca__h

#ifdef __fflib
# include "memory/alloca.h"
#define alloca __builtin_alloca
#else
# include <alloca.h>
#endif

# endif /*__f__alloca__h*/
