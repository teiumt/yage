# include "../pci.h"
# include <malloc.h>
struct pci_dev*
pci_device_add(_32_u __domain, _8_u __bus, _8_u __dev, _8_u __func) {
	_int_u r;
	r = _pci_sys->ndev++;
	struct pci_dev *dev;
	dev = _pci_sys->devices+r;
	_pci_sys->funcs->device_add(dev, __domain, __bus, __dev, __func);
	return dev;
}

void pci_device_statechange(struct pci_dev *__dev, _8_u __towhat) {
	_pci_sys->funcs->statechange(__dev, __towhat);
}

struct pci_dev_iter* pci_device_iter_create(struct pci_slot_match *__match) {
	struct pci_dev_iter *itr;
	itr = (struct pci_dev_iter*)malloc(sizeof(struct pci_dev_iter));

	struct pci_dev **plot;
	plot = itr->devices;
	_int_u i;
	struct pci_dev *dev;
	_8_u flags;
	flags = __match->flags;
	i = 0;
	for(;i != _pci_sys->ndev;i++) {
		dev = _pci_sys->devices+i;

		if (((__match->domain == dev->domain)|(flags&0x01)) && 
			((__match->bus == dev->bus)|(flags>>1&0x01)) &&
			((__match->dev == dev->dev)|(flags>>2&0x01))&&
			((__match->func == dev->func)|(flags>>4&0x01)))
		{
			*(plot++) = dev;
		}
	}

	itr->cur = itr->devices;
	*plot = NULL;
	return itr;
}

struct pci_dev*
pci_device_next(struct pci_dev_iter *__iter) {
	struct pci_dev *d;
	d = *__iter->cur;
	if (d != NULL) {
		__iter->cur++;
		return d;
	}
	return NULL;
}
