# include "tc.h"
# include "stdio.h"
# include "maths.h"
void ff_time_init(void) {
	struct tc_spec dummy;
	ff_tc_gettime(&dummy);

	float v;
	v = dummy.sec;
	float d, h, m, s;
	d = v/86400.;
	v-=floor(d)*86400.;
	h = v/3600.;
	v-=floor(h)*3600.;
	m = v/60.;
	v-=floor(m)*60;
	s = v;

	printf("current time: %u-%u-%u-%u or %u\n", (_64_u)d, (_64_u)h, (_64_u)m, (_64_u)s, dummy.sec);
}
