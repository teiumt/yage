# ifndef __ffly__no__t__h
# define __ffly__no__t__h
# include "../y_int.h"
typedef _int_u ffly_no_t;
# ifdef __cplusplus
namespace mdl {
namespace firefly {
namespace types {
typedef ffly_no_t no_t;
}
}
}
# endif
# endif /*__ffly__no__t__h*/
