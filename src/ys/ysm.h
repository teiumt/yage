//form-A
#define _ys_addfq 0x00
#define _ys_xprt 0x01
#define _ys_agnl 0x02
#define _ys_agnh 0x03
// fetch from memory etc.
/*
	read write from address space no buffer no descriptors etc
*/
#define _ys_fldw 0x04
#define _ys_fsdw 0x05
#define _ys_movll 0x06
#define _ys_movlh 0x07
#define _ys_movhl 0x08
#define _ys_movhh 0x09
#define _ys_dist 0x0a
#define _ys_clmp 0x0b
#define _ys_ld	0x0c
#define _ys_st 0x0d
#define _ys_mul 0x00
#define _ys_add 0x01
#define _ys_cos 0x02
#define _ys_sin 0x03

struct yss_operand {
	union {
		_64_u reg;
		_64_u i;
		float f;
	};
	_8_u type;
};

struct yss_op {
	_8_u op, form;
	struct yss_operand opand[10];
	_int_u n_opands;
};

