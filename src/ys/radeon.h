#ifndef __radeon__h
#define __radeon__h
#define EXP_DW0(__en, __tgt, __compr, __done, __vm)\
    ((__en)|((__tgt)<<4)|((__compr)<<10)|((__done)<<11)|((__vm)<<12)|(0x31<<26))
#define EXP_DW1(__vs0, __vs1, __vs2, __vs3)\
    ((__vs0)|((__vs1)<<8)|((__vs2)<<16)|((__vs3)<<24))
#define VOP1_DW(__src0, __op, __vdst)\
    ((__src0)|((__op)<<9)|((__vdst)<<17)|(0x3f<<25))
#define ENDPGM 1
#define WAITCNT 12
#define SOPP(__op)\
    (((__op)<<16)|(0x17f<<23))
#define VOP1_DW(__src0, __op, __vdst)\
    ((__src0)|((__op)<<9)|((__vdst)<<17)|(0x3f<<25))
//encoding is ZERO
#define VOP2_DW(__src0,__vsrc1,__vdst,__op)\
    ((__src0)|(__vsrc1<<9)|(__vdst<<17)|(__op<<25))

#define TBUF_LOAD_XYZW 3
#define TBUF_STORE_XYZW 7
#define MTBUF_DW0(__offset, __offen, __idxen, __glc, __op, __df, __nf)\
    ((__offset)|((__offen)<<12)|((__idxen)<<13)|((__glc)<<14)|((__op)<<15)|((__df)<<19)|((__nf)<<23)|(0x3a<<26))
#define MTBUF_DW1(__vadr, __vdat, __srsrc, __slc, __tfe, __soffset)\
    ((__vadr)|((__vdat)<<8)|((__srsrc)<<16)|((__slc)<<22)|((__tfe)<<23)|((__soffset)<<24))
#define NF_FLOAT 7
#define NF_UINT 4
#define DF_32_32_32_32 14
#define DF_8_8_8_8 10
#define DF_32 4
#define INTRP_DW(__vsrc,__attrchan,__attr,__op,__vdst)\
    ((__vsrc)|((__attrchan)<<8)|((__attr)<<10)|((__op)<<16)|((__vdst)<<18)|(0x35<<26))
#define SOPK_DW(__simm16,__sdst,__op)\
    ((__simm16)|((__sdst)<<16)|((__op)<<23)|(0x0b<<28))
#define SOP1_DW(__ssrc0,__op,__sdst)\
    ((__ssrc0)|((__op)<<8)|((__sdst)<<16)|(0x17d<<23))
#define MIMG_DW0(__dmask,__unorm,__glc,__da,__r128,__tfe,__lwe,__op,__slc)\
	(((__dmask)<<8)|((__unorm)<<12)|((__glc)<<13)|((__da)<<14)|((__r128)<<15)|((__tfe)<<16)|((__lwe)<<17)|((__op)<<18)|((__slc)<<25)|(0x3c<<26))
#define MIMG_DW1(__vaddr,__vdata,__srsrc,__ssamp,__d16)\
	((__vaddr)|((__vdata)<<8)|((__srsrc)<<16)|((__ssamp)<<21)|((__d16)<<31))
#define MIMG_IMAGE_LOAD 0
#define FLAT_DW0(__glc,__slc,__op)\
	(((__glc)<<16)|((__slc)<<17)|((__op)<<18)|(0x37<<26))
#define FLAT_DW1(__addr,__data,__tfe,__vdst)\
	((__addr)|((__data)<<8)|((__tfe)<<23)|((__vdst)<<24))
#define SMEM_DW0(__sbase,__sdata,__glc,__imm,__op)\
	((__sbase)|((__sdata)<<6)|((__glc)<<16)|((__imm)<<17)|((__op)<<18)|(0x30<<26))
#define SMEM_DW1(__offset)\
	(__offset)

#define MUBUF_DW0(__offset,__offen,__idxen,__glc,__lds,__slc,__op)\
	((__offset)|((__offen)<<12)|((__idxen)<<13)|((__glc)<<14)|((__lds)<<16)|((__slc)<<17)|((__op)<<18)|(0x38<<26))
#define MUBUF_DW1(__vaddr,__vdata,__srsrc,__tfe,__soffset)\
	((__vaddr)|((__vdata)<<8)|((__srsrc)<<16)|((__tfe)<<23)|((__soffset)<<24))

#define v_cvt_pkrtz_f16_f32 662
#define VOP3A_DW0(__vdst,__abs,__clamp,__op)\
	((__vdst)|((__abs)<<8)|((__clamp)<<15)|((__op)<<16)|(0x34<<26))
#define VOP3A_DW1(__src0,__src1,__src2,__omod,__neg)\
	((__src0)|((__src1)<<9)|((__src2)<<18)|((__omod)<<27)|((__neg)<<29))

#endif /*__radeon__h*/
