rm -f *.o hw/*.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../../ && . ./compile.sh && cd ys/as
ffly_objs="$ffly_objs"
gcc $cc_flags -o a.out main.c $ffly_objs -nostdlib
