# include "as.h"
# include "../../io.h"
# include "../../m_alloc.h"
#include "../../assert.h"
#define _ 0
_8_u static sfx_tab[0x100] = {
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_
};

struct ys_blob* ys_parser(void) {
	sfx_tab['l'] = 1;
	sfx_tab['h'] = 2;
	sfx_tab['w'] = 4;
	sfx_tab['d'] = 8;
	struct ys_line *ln;
	ln = ys_line_next();
	if (!ln)
		return NULL;
	_8_u *p;
	p = ln->p;
	struct ys_blob *b;
	b = m_alloc(sizeof(struct ys_blob));
	_int_u i = 0;
	if (*p == '.') {
		p++;
		_8_u buf[128];
		char c;
		c = *p;
		while(c>='a'&&c<='z') {
			buf[i] = c;
			c = p[++i];
		}
		
		//ignore ' ' and '.' make sure its accounted for
		i+=2;
		
		b->id = BLB_DIR;

	} else {
		_8_u buf[128];
		char c;
		c = *p;
		while((c>='a'&&c<='z')||(c>='0'&&c<='9')||c == '_') {
			buf[i] = c;
			c = p[++i];
		}

		char sfx = buf[i-1];
		_8_u bits, fix = 0;
		bits = sfx_tab[sfx];
		buf[i] = '\0';
		if (bits != 0) {
			i--;
			printf("suffix found.\n");
			fix++;
			as.sfx = sfx;
			printf("SPECAL: %u.\n", bits);
		}


		b->id = BLB_INS;
		b->specal = bits;
		_8_s u = -1;
_again:
		b->tray = f_lhash_get(&as.tab, buf, i);
		if (!b->tray && u == -1) {
			i++;
			u = 0;
			goto _again;
		}

		if (!b->tray) {
			printf("could not find optray: %s.\n", buf);
			assert(1 == 0);
		}
		as.selhelp = -1;
		if (c == ',' || c == '\x27') {
			if (c == ',') {
				c = p[++i];
				_int_u bias = i;
				while((c>='a'&&c<='z')||(c>='0'&&c<='9')) {
					as.sel[i-bias] = c;
					c = p[++i];
				}
				as.sel[i-bias] = '\0';
				as.sel_len = i-bias;
				as.selhelp = 0;
			}

			if (c == '\x27') {
				c = p[++i];
				_int_u bias = i;
				while((c>='a'&&c<='z')||(c>='0'&&c<='9')) {
					as.prop[i-bias] = c;
					c = p[++i];
				}
				as.prop[i-bias] = '\0';						
				as.prop_len = i-bias;
			}
		}
		if (ln->len == i) {
			ln = ys_line_next();
			if (!ln) {
				b->sy = NULL;
				return b;
			}
		}
		i+=fix;
	}
	if (ln->len == i)
		return b;
	printf("LINE DISPLACEMENT: %u-charic.\n",i);
	b->sy = ys_eval(ln, i);
	if (ln->p[ln->len-1] == '`') {
		b->specal |= 16;
	}
	return b;
}
