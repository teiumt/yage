# include "as.h"
# include "../../io.h"
# include "../../m_alloc.h"
# include "../../strf.h"
# include "../../system/string.h"
# include "../../string.h"
/*
	evaluate the expression ie line 
	line = IDENT SYM+INT, REG -> result ie blob to process
		   [expression]
*/
struct ys_symb* ys_eval(struct ys_line *__ln, _int_u __dis) {
	_8_u *p;
	p = __ln->p+__dis;
	_int_u i = 0;
	_8_s ok;
	char c;
	struct ys_symb *head = NULL;
	struct ys_symb *sy, *cur = NULL, *lst;
	sy = m_alloc(sizeof(struct ys_symb));
	head = sy;
	cur = head;
	as.oa_en = 0;
	goto _sk;
_next:
	sy = m_alloc(sizeof(struct ys_symb));
_sk:
	sy->i = i;
	sy->attr = 0;
	cur->next = sy;
	lst = cur;
	cur = sy;

	as.oa_en |= 1<<(i++);

	while(*p == ' ')
		p++;
	c = *p;
	printf("DIRECTIVE CHARIC: %c.\n",c);
	_8_u buf[128];
	ok = -1;
	if (c == '!'){
		ok = 0;
		as.oa_en ^= 1<<(i-1);
		p++;
	}else
	if (c == '%') {
		c = *++p;
		_int_u i;
		i = 0;
		while((c>='a'&&c<='z') || (c>='0'&&c<='9')) {
			buf[i++] = c;
			c = *++p;
		}
		buf[i] = '\0';
		
		sy->id = SY_REG;
		sy->_0 = f_lhash_get(&as.reg, buf, i);
		if (!sy->_0) {
			printf("inavlid register-%s.\n", buf);
			return NULL;
		}
		_int_u num = 0;
		if (c == '#') {
			c = *++p;
			i = 0;
			while((c>='0'&&c<='9')) {
				buf[i++] = c;
				c = *++p;
			}
			num = _ffly_dsn(buf, i);
		}
		printf("reg-no: %u.\n",num);
		sy->_1 = num;
	} else if (c>='a'&&c<='z') {
		_int_u i;
		i = 0;
		while((c>='a'&&c<='z') || (c>='0'&&c<='9') || c == '_') {
			buf[i++] = c;
			c = *++p;
		}
		buf[i] = '\0';
		sy->id = SY_IDENT;
		mem_dup((void*)&sy->_0, buf, i+1);
		sy->_1 = i;
	} else if ((c>='0'&&c<='9')||(c == '-')) {
		_8_s neg = 0;
		if (c == '-') {
			neg = -1;
			c = *++p;
		}
	
		_int_u i;
		_8_u flt = 0;
		i = 0;
	_more:
		while(c>='0'&&c<='9') {
			buf[i++] = c;
			c = *++p;
		}

		if (c == '.') {
			buf[i++] = c;
			c = *++p;
			flt = 1;
			goto _more;
		}

		buf[i] = '\0';
		printf("BUF: %s.\n", buf);
		sy->id = SY_INT;
		if (!flt) {
			if (neg == -1)
				sy->_0 = -_ffly_dsn(buf, i);
			else
				sy->_0 = _ffly_dsn(buf, i);	
		} else {
			sy->id = SY_FLOAT;
			sy->_0 = 0;
			float fpn = ffly_stfloat(buf);
			if (neg == -1) {
				fpn = -fpn;
			}
			*(float*)&sy->_0 = fpn;
			printf("FLOAT: %f, %u.\n", *(float*)&sy->_0, sizeof(float));
		}
	} else if (c == '`') {
		c = *++p;
		_int_u i;
		i = 0;
		while((c>='a'&&c<='z') || (c>='0'&&c<='9') || c == '_') {
			buf[i++] = c;
			c = *++p;
		}
		buf[i] = '\0';
			
		sy->id = SY_SEL;
		struct ys_sso *sso;
		sso = f_lhash_get(&as.env, buf, i);
		sy->_0 = sso->binding;
		if (!sso) {
			printf("inavlid storage object-'%s'-%u.\n", buf, i);
			return NULL;
		}
	}

	if (c == '\x27') {
		c = *++p;
		_int_u i;
		char buf[128];
	_another:
		i = 0;
		while(c>='a'&&c<='z') {
			buf[i++] = c;
			c = *++p;
		}
		sy->attr |= ys_findattr(buf, i);
		printf("OPERAND_ATTR: %x.\n", sy->attr);
		if (c == '|') {
			c = *++p;
			goto _another;
		}
	}

	if (c == ','){
		printf("NEXT.\n");
		p++;
		goto _next;
	}

	if (!ok) {
		m_free(sy);
		lst->next = NULL;
	} else {
		sy->next = NULL;
	}
	cur = head;
	while(cur != NULL) {
		printf("SYMB.\n");
		cur = cur->next;
	}
	return head;
}
