# include "as.h"
# include "../../m_alloc.h"
# include "../../string.h"
# include "../../io.h"
#include "../../assert.h"
_8_u static buf[LINE_MAX];
_8_u static *backbuf;
_int_u static bblen = 0;
_int_u static at = 0;

void ys_input_reset(void) {
	backbuf = NULL;
	bblen = 0;
	at = 0;
}

struct ys_line* ys_line_next(void) {
	_8_u *p;
	_int_u len, left;
	struct ys_line *ln;
	ln = (struct ys_line*)m_alloc(sizeof(struct ys_line));
_nextline:
	if (at>=as.limit){
		printf("ENDOF, %u, %u\n", bblen, at);
		return NULL;
	}


	if (bblen>0) {
		mem_cpy(buf, backbuf, bblen);
		YSA_read(buf+bblen, LINE_MAX-bblen);
		bblen = 0;
	} else {
		YSA_read(buf, LINE_MAX);
	}

	p = buf;
	while(*p != '\n') {
		if (at>=as.limit-1)
			break;
		at++;
		p++;
	}	
	len = p-buf;
	at++;//new line char
	left = LINE_MAX-(len+1);
	if (left>0) {
		backbuf = buf+(len+1);
		bblen = left;
	}

	if (*buf == ';') {
		goto _nextline;
	}

	assert(buf[len-1] != '\n');
	ln->p = buf;
	ln->len = len;
	printf("%w\n",ln->p,ln->len);
	return ln;
}
