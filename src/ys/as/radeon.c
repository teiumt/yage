# include "as.h"
#include "../../string.h"
# include "../../io.h"
# include "../../assert.h"
# include "../../m_alloc.h"
#include "../radeon.h"
#define _sgpr 0
#define _vgpr 1
#define _attr_idx 1
#define _attr_off 2

struct reg {
	struct reg_sh sh;
	_64_u val;
	_64_u vop1;
	char const *name;
	_int_u nme_len;
};
static _64_u code0;

struct op_body {
	_64_u code[16];
	_int_u len;
	_64_u bits[8];
	char const *magic[16];
};

struct op_tray {
	void(*emit)(struct op_body*);
	char const *name;
	_int_u nme_len;
	struct op_body *bods;
	_int_u n;
	_64_u bits;
};

void static emit_exp(struct op_body *op) {
	_64_u code[4];
	struct reg *tgt;
	tgt = as.oa_p[0];

	_64_u v0,v1,v2,v3;
	v0 = as.oa_p0[1];
	v1 = as.oa_p0[2];
	v2 = as.oa_p0[3];
	v3 = as.oa_p0[4];
	/*
		its expected that we get a vgpr 
	*/
	_64_u dst = tgt->val+as.oa_p0[0];
	_64_u en = as.oa_en>>1;
	_64_u compr;
	if (as.specal&4) {
		compr = 1;
		/*
			im unsure of this in the arch doc, it says that bits 0 and 2 enable VSRC0 and VSRC1 but
			it also says valid values are 0,3,c,f so???? it says nothing else.
		*/
		en = 0xf;
	} else if (as.specal&8) {
		compr = 0;
	} else {
		assert(1 == 0);
	}
	
	_64_u done;
	done = (as.specal&16)>0;

	_64_u vm = 0;
	if (dst>= 0 && dst<8) {
		vm = 1;
	}
	code[0] = ((_32_u)EXP_DW0(en,dst,compr,done,vm))|((_64_u)EXP_DW1(v0,v1,v2,v3)<<32);

	printf("EXP: %x:%x - COMPR: %u, DST: %u, v{%u,%u,%u,%u}:%x, %c\n", ((_32_u*)code)[0], ((_32_u*)code)[1], compr, dst, v0,v1,v2,v3,en, !done?'~':'D');
	YSA_write(code, 8);
}

_8_u static fpn_tt[8] = {
	240,//0: 0.5
	242,//1: 1.0
	244,//2: 2.0
	0,	//3: xxx
	246//4:	4.0
};
float static fpn_tt0[8] = {
	0.5,
	1.0,
	2.0,
	0,
	4.0
};
static _8_s voa_state;
_64_u vop1_operand(_int_u __n) {
	_64_u bits = as.oa_bits[__n];
	_64_u r = 0;
	if (bits&_o_f32) {
		float f = *(float*)&as.oa_p[__n];
		if (f<0) {
			f = -f;
			r++;
		}

		printf("FLOATING POINT NUMBER: %f, %u.\n", f, f == 1.0);
		/*
			mabey better off

			doing 
			f/0.5
		*/
		_int_u idx = f/1.0;
		printf("INDEX: %u, %f.\n", idx, fpn_tt0[idx]);
		if (idx<=4) {
			/*
				if values differ from values we think they are
			*/
			if (f != fpn_tt0[idx])
				goto _j0;
		}
		if (idx>4) {
		_j0:
			voa_state = 0;
			return;
		}
		r+=fpn_tt[idx];
	}else
	if (bits&_o_imm) {
		printf("IMM.\n");
		if (as.oa_p[__n]>=65) {
			voa_state = 0;
			r = 255;
		} else {
			r = 128+as.oa_p[__n];
		}
	}else
	if (bits&_o_reg) {
		printf("REG.\n");
		struct reg *tgt;
		tgt = as.oa_p[__n];
		_64_u v = as.oa_p0[__n];
		v+=tgt->vop1;
		r = v;
	} else {
		assert(1 == 0);
	}
	return r;
}

void static
emit_vop1(struct op_body *op) {
	_32_u code[2];
	voa_state = -1;
	struct reg *tgt;
	tgt = as.oa_p[0];
	_64_u dst = as.oa_p0[0];
	_64_u src = vop1_operand(1);
	if (!voa_state) {
		src = 255;
		printf("LITERAL float: %f.\n",*(float*)&as.oa_p[1]);
		*(float*)(code+1) = *(float*)&as.oa_p[1];
	}


	code[0] = VOP1_DW(src,op->code[0],dst);

	YSA_write(code,4+(!voa_state?4:0));
	printf("VOP1: src: %u, op: %u, dst: %u, #### %d\n", code[0]&0x1ff, (code[0]>>9)&0xff, (code[0]>>17)&0xff,voa_state);
	printf("%x, %x\n", code[0], VOP1_DW(242/*sgpr(SRC)*/, 1, 0/*vgpr(DST)*/));
}

void static
emit_vop2(struct op_body *op) {
	_32_u code[2];
	voa_state = -1;
	_64_u src0 = vop1_operand(0);
	_64_u vsrc1 = as.oa_p0[1];
	_64_u vdst = as.oa_p0[2];

	if (!voa_state) {
		src0 = 255;
		*(float*)(code+1) = *(float*)&as.oa_p[0];
		printf("FLOAT INTERJECTION: %f.\n",*(float*)(code+1));
	}

	code[0] = VOP2_DW(src0,vsrc1,vdst,op->code[0]);

	printf("VOP2-%u: src0: %u, vsrc1: %u, vdst: %u, #### %d\n",op->code[0],src0,vsrc1,vdst,voa_state);
	YSA_write(code,4+(!voa_state?4:0));
}

void static
emit_vop3a(struct op_body *op) {
	_32_u code[2];
	_64_u dst = as.oa_p0[0];
	_64_u src0 = as.oa_p0[1]+256;
	_64_u src1 = as.oa_p0[2]+256;
	_64_u src2 = as.oa_p0[3]+256;
	code[0] = VOP3A_DW0(dst,0,0,op->code[0]);
	code[1] = VOP3A_DW1(src0,src1,src2,0,0);
	YSA_write(code,8);
	printf("%x:%x;VOP3A-%u: dst: %u, src0: %u, src1: %u, src2: %u.\n",code[0],code[1],op->code[0],dst,src0,src1,src2);
}

_64_u mtbuf_operand(_int_u __n) {
	_64_u bits = as.oa_bits[__n];
	_64_u r;
	if (bits&_o_imm) {
		if (as.oa_p[__n]>64)
			printf("immval %u should be < 65.\n", as.oa_p[__n]);
		assert(as.oa_p[__n]<65);
		
		r = 128+as.oa_p[__n];
	}else
	if (bits&_o_reg) {
		struct reg *tgt;
		tgt = as.oa_p[__n];
		_64_u v = as.oa_p0[__n];
		r = v;
	}
	return r;
}
#define _oa_num 0
#define _oa_str 1
char static globbuf[128];
_int_u static gb_n = 0;
#include "../../strf.h"
_int_u static lex(_ulonglong *__val, _ulonglong *__val0, _8_u *__ty) {
	char *p = as.prop;
	char buf[64];
	_int_u i;
	_int_u opand = 0;
	char *e = p+as.prop_len;
	char c;
_again:
	c = *p;
	printf("CHAR: %c.\n",c);
	if (c>='0'&&c<='9') {
		i = 0;
		while(c>='0'&&c<='9') {
			buf[i] = c;
			c = *++p;
			i++;
		}

		__val[opand] = _ffly_dsn(buf,i);
		__ty[opand] = _oa_num;
	}else
	if (c>='a'&&c<='z') {
		i = 0;
		while(c>='a'&&c<='z') {
			globbuf[gb_n+i] = c;
			c = *++p;
			i++;
		}
		__val[opand] = globbuf+gb_n;
		__val0[opand] = i;
		__ty[opand] = _oa_str;
		gb_n+=i;
	}
	if (p<e) {
		opand++;
		goto _again;
	}
	return opand;
}

void static mtbuf_prop(_64_u *__df, _64_u *__nf) {
	_ulonglong val[4];
	_ulonglong val0[4];
	_8_u ty[4];
	_int_u n;
	n = lex(val, val0, ty);

	if (val[0] == 32 && val[2] == 4) {
		*__df = DF_32_32_32_32;
	}

	if (*(char*)val[1] == 'f') {
		*__nf = NF_FLOAT;
	} else if (*(char*)val[1] == 'u') {
		*__nf = NF_UINT;
	}

	if (val[0] == 8 && val[2] == 4) {
		*__df = DF_8_8_8_8;
	}
	if (val[0] == 32 && val[2] == 1) {
		*__df = DF_32;
	}
}
/*
	ldxyzw,32f4,
	0-	offset
	1-	vaddress
	2-	data
	3-	source
	4-	src_offset
*/
void static
emit_mtbuf(struct op_body *op) {
	_32_u code[2];
	_64_u df, nf;
	mtbuf_prop(&df,&nf);
	_64_u offset = as.oa_p[0];
	_64_u offen = (as.oa_at[1]&_attr_off)>0;
	_64_u idxen = (as.oa_at[1]&_attr_idx)>0;
	struct reg *dat;
	dat = as.oa_p[2];
	_64_u _dat = as.oa_p0[2];

	_64_u ssrc = mtbuf_operand(4);
	struct reg *src;
	src = as.oa_p[3];
	_64_u _op = code0;
	_64_u _src = as.oa_p0[3];
	code[0] = MTBUF_DW0(offset,offen,idxen,0,_op,df,nf);
	code[1] = MTBUF_DW1(as.oa_p0[1],_dat,_src,0,0,ssrc);
	printf(
		"MTBUF: offset: %u, off%s, idx%s, op: %u, vaddr: %u, data: %u, src: %u, src_offset: %u, nf: %u, df: %u\n",
		offset, !offen?"¬":"en",!idxen?"¬":"en",_op,as.oa_p0[1],_dat,_src,ssrc,nf,df
	);
	printf("MTBUF: %x:%x.\n", code[0],code[1]);
	YSA_write(code,8);
}

void static
emit_sopp(struct op_body *op) {
	_32_u code;
	code = SOPP(op->code[0])|as.oa_p[0];
	printf("SOPP: op: %u, val: %u.\n", op->code[0], as.oa_p[0]);
	YSA_write(&code,4);
}
void static
emit_intrp(struct op_body *op) {
	_32_u code;
	_8_u chan = 0;
	switch(*as.prop) {
		case 'x': chan = 0;
		break;
		case 'y': chan = 1;
		break;
		case 'z': chan = 2;
		break;
		case 'w': chan = 3;
		break;
	}
	_int_u atb = *(as.prop+1)-'0';
	code = INTRP_DW(as.oa_p0[1],chan,atb,op->code[0],as.oa_p0[0]);
	YSA_write(&code,4);
	printf("INTERP-%x: VDST: %u, CODE: %u, VSRC: %u, chan: %u, attrb: %u\n", code,as.oa_p0[0],op->code[0],as.oa_p0[1], chan,atb);
}

void static
emit_sopk(struct op_body *op) {
	_32_u code;
	code = SOPK_DW(as.oa_p[0],as.oa_p0[1],op->code[0]);
	YSA_write(&code,4);
	printf("SOPK: %u,%u,%u.\n",as.oa_p[0],as.oa_p0[1],op->code[0]);
	assert(1 == 0);

}

void static
emit_sop1(struct op_body *op) {
	_32_u code[2];
	voa_state = -1;
	_64_u ssrc,sdst;
	ssrc = vop1_operand(1);
	sdst = vop1_operand(0);
	code[0] = SOP1_DW(ssrc,op->code[0],sdst);
	if (!voa_state) {
		code[1] = as.oa_p[1];
		printf("DW1: %u or %d\n",code[1],code[1]);
	}

	YSA_write(code,4+(!voa_state?4:0));
	printf("SOP1: %u,%u,%u.\n",ssrc,op->code[0],sdst);
//	assert(1 == 0);



}


void static
emit_mimg(struct op_body *op) {
	_32_u code[2];

	code[0] = MIMG_DW0(0xf,0,0,0,1,0,0,op->code[0],0);
	code[1] = MIMG_DW1(as.oa_p0[0],as.oa_p0[1],as.oa_p0[2],as.oa_p0[3],0);
	printf("MIMG: %u, V%u, V%u, S%u, S%u.\n",op->code[0],as.oa_p0[0],as.oa_p0[1],as.oa_p0[2],as.oa_p0[3]);
	printf("%x : %x\n",code[0],code[1]);
	YSA_write(code,8);
}

void static
emit_flat(struct op_body *op) {

	_32_u code[2];

	code[0] = FLAT_DW0(0,0,op->code[0]);
	code[1] = FLAT_DW1(as.oa_p0[0],as.oa_p0[2],0,as.oa_p0[1]);
	printf("FLAT: %u, %u, %u.\n",as.oa_p0[0],as.oa_p0[2],as.oa_p0[1]);
	YSA_write(code,8);
}
void static
emit_smem(struct op_body *op) {
	_32_u code[2];

	code[0] = SMEM_DW0(as.oa_p0[2],as.oa_p0[1],0,1,code0);
	code[1] = SMEM_DW1(as.oa_p[0]);

	YSA_write(code,8);
	printf("SMEM: sbase: %u, sdata: %u, offset: %u, op: %u\n",as.oa_p0[2],as.oa_p0[1],as.oa_p[0],code0);
}

void static
emit_mubuf(struct op_body *op) {
	_32_u code[2];

	_64_u ssrc = mtbuf_operand(4);
	code[0] = MUBUF_DW0(as.oa_p[0],0,0,0,0,0,code0);
	code[1] = MUBUF_DW1(as.oa_p[1],as.oa_p0[2],as.oa_p0[3],0,ssrc);
	printf("MUBUF: op: %u, %u, %u, %u, %u, %u\n",code0,as.oa_p[0],as.oa_p[1],as.oa_p0[2],as.oa_p0[3],ssrc);
	YSA_write(code,8);
}

/*
	expw mrt0,%r0'1'2'3
	expd mrt9,%r0'1'2'3
	expd mrt#23
*/
#define NOPS 35
struct op_body op_bods[] = {
	{{0},2,{0xff},{}},			//0
	{{ENDPGM},1,{},{}},		//1
	{{0x01/*v_mov_b32*/},0,{},{}},			//2
	{
		{
			3,
			2,
			1,
			0
		},
		0,
		{},
		{
			"xyzw",
			"xyz",
			"xy",
			"x",
			NULL

		}
	},			//3
	{
		{
			7,
			6,
			5,
			4
		},
		0,
		{},
		{
			"xyzw",
			"xyz",
			"xy",
			"x",
			NULL
		}
	},			//4
	{{WAITCNT},0,{},{}},		//5
	{{0x01/*v_add_f32*/},0,{},{}},			//6
	{{0x02/*v_sub_f32*/},0,{},{}},			//7
	{{0x05/*v_mul_f32*/},0,{},{}},			//8
	{{0x07},0,{},{}},			//9
	{{31},0,{},{}},			//10
	{{0},0,{},{}},				//11
	{{1},0,{},{}},				//12
	{{2},0,{},{}},				//13
	{{0},0,{},{}},				//14
	{{0},0,{},{}},				//15
	{{8},0,{},{}},				//16
	{{5},0,{},{}},				//17
	{{6},0,{},{}},				//18
	{{0x14},0,{}},//flat	//19
	{
		{
			2,
			3,
			4,
		},
		0,
		{},
		{
			"4",
			"8",
			"16",
			NULL
		}
	},				//20
	{
		{
			18,
			24,
			26
		},
		0,
		{},
		{
			"4",
			"8",
			"16",
			NULL
		}
	},			//21
	{{0x21},0,{},{}},			//22
	{
		{
			23,
			22,
			21,
			20
		},
		0,
		{},
		{
			"4",
			"3",
			"2",
			"1",
			NULL
		}
	},			//23
	{
		{
			31,
			30,
			29,
			28
		},
		0,
		{},
		{
			"4",
			"3",
			"2",
			"1",
			NULL
		}
	},			//24
	{{8},0,{},{}},				//25
	{{22/*v_mac_f32*/},0,{},{}},				//26
	{{v_cvt_pkrtz_f16_f32},0,{},{}},				//26
	{{36/*v_rsq_f32*/},0,{},{}}	,			//26
	{{11/*v_max_f32*/},0,{},{}},				//26
	{{41/*v_mul_lo_u16*/},0,{},{}},				//26
	{{25/*v_add_u32*/},0,{},{}},				//26
	{{34/*v_rcp_f32*/},0,{},{}},				//26
	{{10/*v_min_f32*/},0,{},{}},				//26
	{{32/*image_sample*/},0,{},{}},				//26
};

static struct op_tray ops[NOPS] = {
	{emit_exp,"exp",3, op_bods,1,0},
	{emit_sopp,"end",3,op_bods+1,1,0},
	{emit_vop1,"mov32v",6,op_bods+2,1,0},
	{emit_mtbuf,"load",4,op_bods+3,1,0},
	{emit_mtbuf,"store",5,op_bods+4,1,0},
	{emit_sopp, "waitcnt",7,op_bods+5,1,0},
	{emit_vop2,"add32f",6,op_bods+6,1,0},
	{emit_vop2,"sub32f",6,op_bods+7,1,0},
	{emit_vop2,"mul32f",6,op_bods+8,1,0},
	/*
		float to unsigned
	*/
	{emit_vop1,"uf32",4,op_bods+9,1,0},
	{emit_vop1,"flr32f",6,op_bods+10,1,0},
	{emit_intrp,"intrp_p1f32",11,op_bods+11,1,0},
	{emit_intrp,"intrp_p2f32",11,op_bods+12,1,0},
	{emit_intrp,"intrp_movf32",12,op_bods+13,1,0},
	{emit_sop1,"mov32s",6,op_bods+14,1,0},
	{emit_mimg,"image_load",10,op_bods+15,1,0},
	{emit_vop1,"cvt_i32f32",10,op_bods+16,1,0},
	{emit_vop1,"cvt_f32i32",10,op_bods+17,1,0},
	{emit_vop1,"cvt_f32u32",10,op_bods+18,1,0},
	{emit_flat,"flat_ldw4",9,op_bods+19,1,0},
	{emit_smem,"s_ldw",5,op_bods+20,1,0},
	{emit_smem,"s_sdw",5,op_bods+21,1,0},
	{emit_smem,"s_dcwbv",7,op_bods+22,1,0},
	{emit_mubuf,"u_ldw",5,op_bods+23,1,0},
	{emit_mubuf,"u_sdw",5,op_bods+24,1,0},
	{emit_mimg,"image_store",11,op_bods+25,1,0},
	{emit_vop2,"mac32f",6,op_bods+26,1,0},
#define v_cvt_pkrtz_f16_f32_str "cvt_pkrtz_f16_f32"
	{emit_vop3a,v_cvt_pkrtz_f16_f32_str,sizeof(v_cvt_pkrtz_f16_f32_str),op_bods+27,1,0},
	{emit_vop1,"rsq32f",6,op_bods+28,1,0},
	{emit_vop2,"max32f",6,op_bods+29,1,0},
	{emit_vop2,"mul16ulo",8,op_bods+30,1,0},
	{emit_vop2,"add32u",6,op_bods+31,1,0},
	{emit_vop1,"rcp32f",6,op_bods+32,1,0},
	{emit_vop2,"min32f",6,op_bods+33,1,0},
	{emit_mimg,"image_sample",12,op_bods+34,1,0}
};

#define NREG 7
static struct reg rtab[NREG] = {
	{{0}, 8, 0,"z", 1},
	{{0}, 0, 0,"mrt", 3},
	{{0},12,0,"pos",3},
	{{0},32,0,"par",3},
	{{_o_reg},_vgpr,256,"v",1},
	{{_o_reg},_sgpr,0,"s",1},
	{{_o_reg},0,124,"m",1},
};

void rd_out(struct ys_state *__st) {
	printf("YSA-RD_OUT.\n");
	struct op_body *b, *op;
	op = NULL;
	_int_u i;
	i = 0;
	struct op_tray *tray = as.tray;
/*
_nope:
	while(i != tray->n) {
		b = tray->bods+(i++);
		if (b->noa == as.noa) {
			_int_u ii;
			ii = 0;
			for(;ii != as.noa;ii++) {
				_64_u a, _b;
				a = as.oa_bits[ii];
				_b = b->bits[ii];
				printf("probing: %u ? %u.\n", a, _b);
				if (!(a&_b)) {
					goto _nope;
				}
			}
			op = b;
			goto _yes;
		} else {
			printf("skipping body-%u operand count does not match, %u != %u\n", i, b->noa, as.noa);
		}
	}

	printf("no operation found that meets requirements.\n");
	return;
_yes:
	printf("looks like we found somthing.\n");
*/
	b = tray->bods;
	code0 = b->code[0];
	if (!as.selhelp) {
		_int_u i;
		i = 0;
		while(1) {
			if (b->magic[i] != NULL) {
			if (!str_cmp(b->magic[i],as.sel)) {
				code0 = b->code[i];
				break;
			}
			}
			i++;
		}
	}
	__st->in_c+=tray->bods->len*4;
	tray->emit(tray->bods);
}
_64_u static ins_attr(char *__ident, _int_u __len) {
	if (!mem_cmpc(__ident, "idx", __len,3)) {
		return _attr_idx;
	}

	if (!mem_cmpc(__ident, "off", __len, 3)) {
		return _attr_off;
	}
}
void rd_init(void) {
	ys_findattr = ins_attr;
	printf("YSA-RD_INIT.\n");
	_int_u i;
	i = 0;
	for(;i != NOPS;i++) {
		struct op_tray *t;
		t = ops+i;
		printf("placing OP-%s in table.\n", t->name);
		f_lhash_put(&as.tab, t->name, t->nme_len, t);
	}
	i = 0;
	for(;i != NREG;i++) {
		struct reg *r;
		r = rtab+i;
		printf("placing REG-%s in table.\n", r->name);
		f_lhash_put(&as.reg, r->name, r->nme_len, r);
	}

}

