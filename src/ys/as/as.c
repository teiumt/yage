# include "as.h"
# include "../../io.h"
# include "../../assert.h"
# include "../../m_alloc.h"
#include "../../string.h"
struct ys_as as;
void yss_out(void);
void rd_out(void);
void yss_init(void);
void rd_init(void);
_64_u(*ys_findattr)(char*,_int_u);
struct ys_mech *ys_msel = NULL;
struct ys_mech ys_machine[2] = {
	{yss_out,yss_init},
	{rd_out,rd_init}
};

void ysa_assemble(struct ys_state *__st) {
	struct ys_blob *b;
	__st->in_c = 0;
	mem_set(as.oa_p0,0,sizeof(_ulonglong)*8);
	while((b = ys_parser()) != NULL) {
		if (b->id == BLB_INS) {
			as.tray = b->tray;
			if (!b->tray) {
				printf("no such operation.\n");
			}
			struct ys_symb *sy;
			sy = b->sy;
			_int_u i;
			while(sy != NULL) {
				i = sy->i;
				assert(i<8);
				as.oa_at[i] = sy->attr;
				if (sy->id == SY_REG) {
					as.oa_bits[i] = ((struct reg_sh*)sy->_0)->bits;
					as.oa_p[i] = sy->_0;
					as.oa_p0[i] = sy->_1;
				} else if (sy->id == SY_INT) {
					_64_u imm;
					_64_u _i;
					_i = sy->_0;
					//printf("INT: %u.\n", _i);
					
					if (!(_i&0xffffffff00000000)) {
						imm = _o_imm32;
					} else {
						imm = _o_imm64;
					}

					as.oa_bits[i] = imm;
					as.oa_p[i] = sy->_0;
				} else if (sy->id == SY_SEL) {
					as.oa_bits[i] = _o_ifds;
					as.oa_p[i] = sy->_0;
				} else if (sy->id == SY_FLOAT) {
					as.oa_bits[i] = _o_f32;
					as.oa_p[i] = sy->_0;
				}
				sy = sy->next;
			}
			as.noa = i;
			as.specal = b->specal;
			printf("operands: %u.\n", i);
			ys_msel->onwards(__st);
		} else if (b->id == BLB_DIR) {
			struct ys_sso *sso = m_alloc(sizeof(struct ys_sso));
			struct ys_symb *name = b->sy;
			struct ys_symb *binding = b->sy->next;
			assert(name != NULL && binding != NULL);
			printf("got directive, name: '%s'-%u, boundto: %u\n", name->_0, name->_1, binding->_0);
			sso->binding = binding->_0;
			f_lhash_put(&as.env, name->_0, name->_1, sso);
			m_free((void*)name->_0);
		}
	}
}
void ys_input_reset(void);
void ys_init(void) {
	f_lhash_init(&as.env);
	f_lhash_init(&as.tab);
	f_lhash_init(&as.reg);
	ys_msel->init();
	as.off = 0;
}

void ys_de_init(void) {
	f_lhash_destroy(&as.env);
	f_lhash_destroy(&as.tab);
	f_lhash_destroy(&as.reg);
	ys_input_reset();
}

