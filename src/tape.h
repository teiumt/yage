# ifndef __ffly__tape__h
# define __ffly__tape__h
# include "y_int.h"

typedef struct ffly_tape {
	void *area;
	_int_u len, page_c;
} *ffly_tapep;

struct ffly_tape *ffly_tape_new(void);
void ffly_tape_raze(ffly_tapep);
void ffly_tape_get(ffly_tapep, _int_u, void*, _int_u);
void ffly_tape_insert(ffly_tapep, void*, _int_u);
# endif /*__ffly__tape__h*/
