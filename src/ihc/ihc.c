#include "ihc.h"
#include "fsl/fsl.h"
struct ihc_struc ihc_comm;
void ihc_read(struct ihc_struc *__s, void *__buf, _int_u __len) {
	__s->rw_ops.read(__s->bbs,__buf,__len,&dummy_error);
}

void ihc_write(struct ihc_struc *__s, void *__buf, _int_u __len) {
	__s->rw_ops.write(__s->bbs,__buf,__len,&dummy_error);
}

#include "../error.h"
#include "../io.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../string.h"
int static fdin;

_32_u static _read(void *__ctx, void *__buf, _int_u __n, _8_s *__error) {
	read(fdin,__buf,__n);
	ihc_comm.in_cur+=__n;
}

void ihc_close(void) {
	close(fdin); 
}
void ihc_cleanup(void)  {
}

/*
	redundant
*/
void ihc_defout(struct ihc_struc *__s) {
	__s->rw_ops.write = (void*)0;//_write;
}

void ihc_bbs(struct ihc_file *__s, struct bbs_node *__n) {
	__s->priv = __n;
	__s->write = ith0.write;
}

_y_err ihc_compile(char const *__in) {
	if (!__in) {
		printf("please provide at most 1 file.\n");
		return Y_FAIL;
	}

	fdin = open(__in,O_RDONLY,0);
	if (fdin == -1) {
		printf("failed to open.\n");
		return Y_FAIL;
	}


	printf("input file-'%s'.\n",__in);
	ihc_comm.rw_ops.read = _read;

	struct stat st;
	fstat(fdin,&st);
	ihc_comm.in_cur = 0;
	/*
		-1 = dont care about file end '\0' or '\n'?
	*/
	ihc_comm.in_limit = st.st_size-1;
   
	return Y_SUCC;
}
