#include "fsl.h"
#include "../../io.h"
#include "../../m_alloc.h"
#include "../../assert.h"
/*
TODO:
	dealing with many compiles of differing programs.


	approch-A) we mark hash entrys for disposal.

	approch-B) we completely destory the hashmap and replace it with a new one brought in.
*/
struct fsl_comm fsl_comm;
void fsl_parser(void);
#define NKWDS 16
static struct yl_keywd keywds[] = {
	{"_8_u",4,_fsl_8_u},
	{"void",4,_fsl_void},
	{"vec4",4,_yl_e(0)},
	{"config",6,_fsl_config},
	{"tex",3,_fsl_tex},
	{"mat4",4,_yl_e(1)},
	{"normalize",9,_fsl_normalize},
	{"dot",3,_fsl_dot_product},
	{"float",5,_yl_e(2)},
	{"inverse",7,_fsl_inverse},
	{"clamp",5,_fsl_clamp},
	{"max",3,_fsl_max},
	{"min",3,_fsl_min},
	{"amalg",5,_fsl_amalg},
	{"smooth",6,_fsl_smooth},
	{"flat",4,_fsl_flat}
};

struct fsl_symb* fsl_symb(char const *__str, _int_u __len) {
    if (__len>=64)
        return NULL;
    assert(__str != NULL && __len>0);
	printf("FSL_SYMBOL: %w, %u.\n",__str,__len,__len);
	struct fsl_symb *s;
    s = f_lhash_get(&fsl_comm.env,__str,__len);
    if (!s) {
        s = m_alloc(sizeof(struct fsl_symb));
       	s->s.ign = 0;
	   	s->bits = 0;
		s->s.em.ident = ~0;

			 f_lhash_put(&fsl_comm.env,__str,__len,s);
	}
    return s;
}

void fsl_init(void) {
	yl_lexinit();
	f_lhash_init(&fsl_comm.env);
	yl_init();
	yl_loadkeywds(keywds,NKWDS);
	yl_comm.valmap['+'] = _ima_add;
	yl_comm.valmap['-'] = _ima_sub;
	yl_comm.valmap['*'] = _ima_mul;
	yl_comm.valmap['/'] = _ima_div;
}
void fsl_compile(char const *__in) {
	if (ihc_compile(__in) == -1) {
	printf("IHC: failed to compile shader.\n");
	}
	fsl_parser();
	ihc_cleanup();
}
