#ifndef __fsl__h
#define __fsl__h
#include "../ihc.h"
#include "../ylang/yl.h"
#include "../fsb.h"
#include "../../flue/compiler/ima/ima.h"
/*
	fixing needed

	NOTE -FSL and the backend IMA

	FSL has no control of IMA.
	IMA is presetup before entering FSL.

	meaning.
	initalizing and setting up of IMA is not within FSLs control.

	FSL is nothing but a producer of nodes for IMA.
	would crops have any say other the operation of a farm???
	anwser: NO they wouldent
*/
#define _fsl_void _yl_void
#define _fsl_8_u _yl_8_u
#define _fsl_vec4 2
#define _fsl_config 9
#define _fsl_tex 10
#define _fsl_normalize 11
#define _fsl_dot_product 12
#define _fsl_inverse 13
#define _fsl_clamp 14
#define _fsl_max 15
#define _fsl_min 16
#define _fsl_smooth 17
#define _fsl_flat	18
/*
	combine x+y+z and w components of vector by some operator(+,*)
*/
#define _fsl_amalg 17
#define _fsl_t_void 0
#define _fsl_t_8_u 1
#define _fsl_t_vec4 2
#define _fsl_t_mat4 3
#define _fsl_t_float 3
#define _fsl_symb_buf 1
struct fsl_type;
struct fsl_symb {
    struct hg_symb s;//must stay at top of struct
    struct fsl_type *ty;
	_64_u bits;
};

struct fsl_wkstrc {
	struct ys_em *n;
	struct fsl_symb *s;	
	_64_u bits;
	char const *ss;
	_int_u len;
	_64_u x;
	struct ys_em *em;
	_64_u comp;
};

struct fsl_type {
	_64_u kind;
	_64_u val;
	_int_u cnt;
};

struct fsl_comm {
	struct f_lhash env;
};

#define _fsl_arth (_yl_arth)
void fsl_process(void);
void fsl_init(void);
void fsl_compile(char const*);
struct fsl_symb* fsl_symb(char const*,_int_u);
extern struct fsl_comm fsl_comm;
#endif/*__fsl__h*/
