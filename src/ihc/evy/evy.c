#include "evy.h"
#include "../../m_alloc.h"
#include "../../string.h"
_64_u evy_reg[64*8];
_64_u evy_dmap[16];
static struct evy_node *top_node = NULL;
static struct evy_node *tail_node = NULL;
struct evy_flock *evy_curf = NULL;
struct evy_node* evy_new_node(void) {
	struct evy_node *n = m_alloc(sizeof(struct evy_node));
	if (!top_node) {
		top_node = n;
	}

	if (tail_node != NULL) {
		tail_node->next = n;
	}
	//this is only needed at end so to be MOVED later
	n->next = NULL;
	tail_node = n;
	return n;
}

struct evy_flock* evy_fknew(void) {
	struct evy_flock *f;
	f = m_alloc(sizeof(struct evy_flock));
	f->top = NULL;
	f->last = NULL;
	return f;
}

void evy_fkplace(struct evy_flock *__f) {
	evy_curf->top = top_node;//noodle
	evy_curf->last = tail_node;
	evy_curf = __f;
	top_node = __f->top;
	tail_node = __f->last;
}

struct evy_symb *evy_sytop = NULL;
void evy_symb(struct evy_symb *s, char const *__str,_int_u __len) {
	if (!__len)	return NULL;
	mem_cpyw(s->str,__str,__len);
	s->len = __len;
	s->next = evy_sytop;
	evy_sytop = s;
}

void evy_init(void) {
	evy_sytop = NULL;
	top_node = NULL;
	tail_node = NULL;
}

void evy_emit(struct evy_node *n) {
	while(n != NULL) {
		n->f(n);
		n = n->next;
	}
}

void evy_process(void) {
	evy_emit(top_node);
}
