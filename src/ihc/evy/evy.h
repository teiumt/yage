#ifndef __evy__h
#define __evy__h
#include "../../y_int.h"
#include "../fsb.h"
#define evy_ploped(__name,__x,__y) evy_functab.__name[__x|(__y<<2)]
#define plopfunc(__name,__x,__y,__func) evy_ploped(__name,__x,__y) = __func;
#define _evy_imm 0
#define _evy_vec 1
#define _evy_const 2
#define _evy_exp 2
#define _evy_buf 3
#define _evy_pos 0
#define _evy_mrt 1
#define _evy_z 2
#define _evy_col 3

//left
#define __evy_fsv0   0
#define __evy_gv0    1

//right hand side
#define __evy_imm	0
//func spec var
#define __evy_fsv	1
//global var
#define __evy_gv	2 

struct evy_vs {
	_int_u size;
};

struct evy_w {
	_64_u offset;
	_int_u size;
};

struct yl_em {
	_64_u val;
	_64_u offset;
	struct evy_w *w;
};

struct evy_em {
	union {
		struct yl_em yl;
	};
};

struct evy_dwel {
	struct evy_w *vars;
	_int_u nvar;
};

struct evy_yl {
	struct evy_dwel *d;
	struct yl_em *l, *r;
};

struct evy_flock;
struct evy_node {
	union {

		struct evy_yl yl;
	};
	struct evy_flock *fk;
	void(*f)(struct evy_node*);

	struct evy_node *next;
};

struct evy_flock {
	struct evy_node *top, *last;
};

struct evy_funcs {
	void(*assign[16])(struct evy_node*);
	void(*init)(void);
	void(*fin)(void);
	void(*arth[16])(struct evy_node*);
	void(*func)(struct evy_node*)
};
#define _evy_add 0
#define _evy_sub 1
#define _evy_mul 2
#define _evy_div 3

extern _64_u evy_dmap[16];
#define EVY_REGSP 64
extern _64_u evy_reg[64*8];
struct evy_symb {
	union {
		struct yl_em yl_em;
	};
	char str[64];
	_int_u len;
	_8_s ign;
	struct evy_symb *next;
};
void evy_emit(struct evy_node*);
extern struct evy_flock *evy_curf;
struct evy_flock* evy_fknew(void);
void evy_fkplace(struct evy_flock*);
void evy_init(void);
extern struct evy_symb *evy_sytop;
void evy_resin(void);
void evy_process(void);
void evy_symb(struct evy_symb *s, char const *__str,_int_u __len);
#define evy_assign(__x) evy_functab.assign[__x]
struct evy_funcs evy_functab;
struct evy_node* evy_new_node(void);
#endif/*__evy__h*/
