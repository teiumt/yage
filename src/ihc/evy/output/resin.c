#include "../evy.h"
#include "../../ihc.h"
#include "../../../io.h"
#include "../../../string.h"
#include "../../../m_alloc.h"
char const static pf[] = {
	'b', 'w', 'd', 'q'
};

char const static *_reg[][4] = {
	{"%ae", "%el", "%ael", "%rel"}
};

void static
out_s(char const *__s) {
//	IHC_write(__s,str_len(__s)); 
}

void static
emitop(char const *__op, char __prefix, char const *__l, char const *__r, char const *__fr) {
	char buf[128];
	char *p = buf;
	*(p++) = '\t';

	p+=str_cpy(p, __op);
	if (!__l)
		goto _sk;
	*(p++) = __prefix;
	*(p++) = ' ';
	p+=str_cpy(p, __l);
	if (!__r)
		goto _sk;
	*(p++) = ',';
	*(p++) = ' ';
	p+=str_cpy(p, __r);
	if (!__fr)
		goto _sk;
	*(p++) = ',';
	*(p++) = ' ';
	p+=str_cpy(p, __fr);
_sk:
	*p = '\n';
//	IHC_write(buf, (p-buf)+1);
}

void static _assign_gv_gv(void *__n) {
	printf("EVY_ASSIGN GVGV.\n");

}
void static _assign_fs_fs(void *__n) {
	printf("EVY_ASSIGN FSFS.\n");

}

void static _assign_gv_fs(void *__n) {
	printf("EVY_ASSIGN GVFS.\n");
}

void static _assign_fs_gv(void *__n) {
	printf("EVY_ASSIGN FSGV.\n");
}



void static init(void) {

}
void static finish(void) {

}

void _func(struct evy_node *__n) {
	_int_u i = 0;
	_int_u off = 0;
	struct evy_w *w;
	for(;i != __n->yl.d->nvar;i++) {
		w = __n->yl.d->vars+i;
		w->offset = off;

		off+=w->size;
		printf("FUNC VAR: %u.\n",i);
	}
	printf("EVY_FUNC.\n");
	evy_emit(__n->fk->top);
	printf("END_OF_FUNC.\n");
}

void evy_resin(void) {
	plopfunc(assign,__evy_gv0,__evy_gv,_assign_gv_gv);
	plopfunc(assign,__evy_gv0,__evy_fsv,_assign_gv_fs);	
	plopfunc(assign,__evy_fsv0,__evy_fsv,_assign_fs_fs);
	plopfunc(assign,__evy_fsv0,__evy_gv,_assign_fs_gv);
	evy_functab.init = init;

	evy_functab.fin = finish;
	evy_functab.func = _func;
}
