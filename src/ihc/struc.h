#ifndef __ihc__struc__h
#define __ihc__struc__h

struct ihc_file{
  struct{
    char buf[128];
  }file_params;

	void *priv;
  int fd;
  void(*open)(struct ihc_file*);
  void(*close)(struct ihc_file*);
  _32_u(*write)(struct ihc_file*, void*, _int_u);
};

#endif/*__ihc__struc__h*/
