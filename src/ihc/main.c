#include "../y_int.h"
#include "../types.h"
#include "ihc.h"
#include "../io.h"
#include "ylang/yl.h"
#include "fsl/fsl.h"
#include "../flue/compiler/ima/ima.h"
void _print(void *ct,void *__buf,_int_u __size) {
	printf("%w",__buf,__size);
}
#include "../bbs.h"
#include "../tools.h"
#include "../ys/as/as.h"
_y_err main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("no file provided.\n");
		return -1;
	}
	struct rd_reg r = {.rval=41};
	fsl_init();
	struct fsl_symb *s;

	struct bbs_node *n;
    n = bbsn_new();
    ihc_bbs(&ihc_comm,n);

	void *lot;

	lot = flue_allot(NULL,"texture",7,4,0,0);
	flue_placement(lot,1,20);
	fsl_compile(__argv[1]);
//	fsl_process();

	(n->src+n->ptr)[0] = 0;
	printf("%s.\n",n->src);
	printf("param_exports: %u.\n",((struct rd_shader*)ima_sh)->param_cnt);
}
