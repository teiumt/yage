#ifndef __ihc__h
#define __ihc__h
#include "../y_int.h"
#include "../types.h"
#include "../ffly_def.h"
#include "../rws.h"
#include "../bbs.h"
#include "struc.h"
#ifndef DUMM_ERROR
static _8_u dummy_error;
#endif
/*
	CHANGELOG:
	
	IHC has no authorty to intrude on how backends output there data.
	may it be file, or some other convoluted format.

*/
#define IHC_write(file,...) (file)->write((file)->priv,__VA_ARGS__)

#define IHC_read(...) ihc_comm.rw_ops.read(ihc_comm.bbs,__VA_ARGS__,&dummy_error)
struct ihc_struc {
	_int_u in_limit;
	_int_u in_cur;
	struct f_rw_funcs rw_ops;
	_ulonglong bbs;
};

void ihc_util_file(struct ihc_file*);
void ihc_defout(struct ihc_struc*);
void ihc_bbs(struct ihc_file*,struct bbs_node*);

void ihc_read(struct ihc_struc*,void*,_int_u);
void ihc_write(struct ihc_struc*,void*,_int_u);
_y_err ihc_compile(char const*);
void ihc_cleanup(void);
extern struct ihc_struc ihc_comm;
#endif/*__ihc__h*/
