#include "yl.h"
#include "../../m_alloc.h"
#include "../../string.h"
#include "../../io.h"
#include "../../assert.h"
static char buf[256];
static char *bufp;
static _int_u bufpos = 0;
void(*GETCHR)(char*,_int_u);
static void _GETBUF(char*,_int_u);


static void _GETCHR(char *__buf,_int_u __n) {
	/*
		anything less then X should result in large read
	*/
//	printf("GETCHR-%u.\n",__n);
	if (__n<64) {
		ihc_read(&ihc_comm,buf,128+__n);
		mem_cpy(__buf,buf,__n);
		bufp = buf+__n;
		bufpos = 0;
		GETCHR = _GETBUF; 
	} else  {
		ihc_read(&ihc_comm,__buf,__n);
	}
}
void(*GETCHR)(char*,_int_u) = _GETCHR;
static _64_u flags = 0x00;
#define FG_CHARBUF 1
#define FG_RIBUF 2
#define FG_BUF 4
static char chrbuf[64];
static char *cbp = chrbuf;
/*
	reintraduction buffer
*/
static char rb_c = '\0';
static char *ribuf;
static _int_u rib_sz = 0;
static _int_u rib_pos = 0;
static _int_u ngoing = 0;
void NXTCHR(char *__buf, _int_u __n) {
	ngoing+=__n;
	/*
		if we have any single chars
	*/
	if (cbp>chrbuf) {
		_int_u n;
		n = cbp-chrbuf;
		if (n>=__n) {
			cbp-=__n;
			mem_cpy(__buf,cbp,__n);
			if (chrbuf == cbp) {
				flags ^= FG_CHARBUF;
			} 
			return;
		}
		/*copy chrbuf over to user buffer*/
		_int_u i;
		i = 0;
		for(;i != n;i++) {
			__buf[i] = chrbuf[(n-1)-i];	
		}
		cbp = chrbuf;
		flags ^= FG_CHARBUF;
		__buf+=n;
		__n-=n;
		//no point staying the course
		if (!__n)
			return;
	}

	_int_u rib = rib_sz-rib_pos;
	if (rib>0) {
		if (rb_c != '\0') {
			__buf[0] = rb_c;
			__buf++;
			__n--;
			rib_sz--;
			rb_c = '\0';
			if (!__n)
				return;
		}
		if (rib>=__n) {
			mem_cpy(__buf,ribuf+rib_pos,__n);
			rib_pos+=__n;
			if (rib_pos>=rib_sz) {
				flags ^= FG_RIBUF;
			}
			return;
		}

		mem_cpy(__buf,ribuf+rib_pos,rib);
		__buf+=rib;
		__n-=rib;
		rib_sz = 0;
		rib_pos = 0;
		flags ^= FG_RIBUF;
	}

	if (!__n)
		return;
	GETCHR(__buf,__n);
//	printf("BUFEND: '%w'\n",__buf,__n);

}

void _GETBUF(char *__buf, _int_u __n) {
	_int_u left = 128-bufpos;

	if (__n<=left) {
		mem_cpy(__buf,bufp+bufpos,__n);
		bufpos+=__n;
		if (bufpos == 128) {
			GETCHR = _GETCHR;
			flags ^= FG_BUF;
		}
		return;
 	}

	/*
		copy what we can
	*/
	if (left>0) {
		mem_cpyw(__buf,bufp+bufpos,left);
		__buf+=left;
		__n-=left;
	}
	GETCHR = _GETCHR;
	flags ^= FG_BUF;
	bufpos = 0;
	//get the rest 
	_GETCHR(__buf,__n);
}
void yl_UNGET(char *__buf, _int_u __n) {
	ngoing-=__n;
	rb_c = *__buf;
	ribuf = __buf+1;
	rib_sz = __n;
	rib_pos = 0;
	flags |= FG_RIBUF;
}

void yl_UNGETC(char c) {
	*(cbp++) = c;
	ngoing--;
	flags |= FG_CHARBUF;
}

char static *readchunk(void) {
	//+1 = '\0'
	char *buf = m_alloc(128+1);
	NXTCHR(buf,128);
//	printf("'%w'\n",buf,128);
	return buf;
}

/*
	NOTE:
		keep seperate ANDING a literal is faster then a register and memory
*/

void
read_ident(struct yl_token *tok) {
	tok->n = NULL;
	char *buf;
	buf = readchunk();
	_64_u k, nlen = 0;
	_8_s n = -1;
	_int_u i;
	i = 0;
	while((k = yl_comm.chrmap[buf[i]])&(_yl_let|_yl_num|_yl_underscore)) {
		if (k&_yl_num) {
			if (n == -1) {
				tok->n = buf+i;
				n = 0;
			}
			nlen++;
		}
		i++;
	}
	yl_UNGET(buf+i,128-i);
	buf[i] = '\0';
	tok->s = buf;
	tok->len = i;
	tok->nlen = nlen;
}

char static*
read_num(_int_u *__len) {
	char *buf;
	buf = readchunk();
	_int_u i;
	i = 0;
	if (*buf == '-')
		i = 1;
	while(yl_comm.chrmap[buf[i]]&(_yl_num|__yl_dot)) {
		i++;
	}
	*__len = i;
	yl_UNGET(buf+i,128-i);
	buf[i] = '\0';
	return buf;
}

struct yl_token *yl_lex(void) {
//	printf("|| %u, %x, %u, %u\n", ihc_comm.in_cur,flags, ngoing,bufpos);
	_64_s overshoot = ihc_comm.in_cur-ihc_comm.in_limit;

//	printf(":: %d, %u, %u - %u\n",overshoot,rib_sz-rib_pos,128-bufpos, cbp>cb);	
	struct yl_token *tok;

	tok = (struct yl_token*)m_alloc(sizeof(struct yl_token));
	tok->s = NULL;
	tok->id = 0;
	tok->kind = 0;
	tok->bits = 0;
	tok->val = 0;
	_64_u k;
	char c,second;
_skip:
	if (overshoot>=0 && overshoot == ((rib_sz-rib_pos)+(128-bufpos))+1/*to correct for the 'second' read*/) {
		return NULL;
	}

	NXTCHR(&c,1);
	if ((k = yl_comm.chrmap[c])&_yl_ignore) {
		goto _skip;
	}

	/*
		TODO:
			bits set in chrmap should control/direct this
	*/
	NXTCHR(&second,1);

	yl_UNGETC(second);
	
	yl_UNGETC(c);
	printf("CHARIC: %c, %d, %d\n",c,overshoot,((rib_sz-rib_pos)+(128-bufpos)));
	if (k&(_yl_let|_yl_underscore)) {
		read_ident(tok);
		tok->kind = _yl_ident;
	}else
	if ((k&_yl_num) || c == '-') {
		if (c == '-') {
			if (!(yl_comm.chrmap[second]&_yl_num)) {
				goto _notthis;
			}
			tok->bits |= _yl_arth;
		}

		tok->s = read_num(&tok->len);
		tok->kind = __yl_num;
	}else
_notthis:
	if (k&(_yl_char|__yl_dot)) {

		NXTCHR(&c,1);
		tok->kind = _yl_keychar;
		tok->id = k>>16;
		tok->bits = yl_comm.bitmap[c];
		tok->val = yl_comm.valmap[c];
	}
	ffly_fdrain(_ffly_out);
	return tok;
}
void yl_lexinit(void) {
	bufpos = 0;
	GETCHR = _GETCHR;
	cbp = chrbuf;
	flags = 0x00;
	ngoing = 0;
	rib_sz = 0;
	rib_pos = 0;
	yl_comm.tokp = yl_comm.tokbuf;
}

