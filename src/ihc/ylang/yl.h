#ifndef __yl__h
#define __yl__h
/*
	clang vs ylang????
	ylang is are own language
	clang is are own compiler but c specs
*/
#include "../ihc.h"
#include "../../lib/hash.h"
#include "../evy/evy.h"
#define _yl_imm 0
#define _yl_fsv 1
#define _yl_gv	2
struct yl_token {
	char *s;
	char *n;
	_int_u len,nlen;
	_64_u kind;
	_64_u id;
	_64_u bits;
	_64_u val;
};

struct yl_keywd {
	char const *str;
	_int_u len;
	_64_u id;
};
struct yl_wkstrc;
struct yl_struc {
	_64_u chrmap[256];
	_64_u bitmap[256];
	_64_u valmap[256];
	struct f_lhash kws, env, *_env;
	struct yl_token *tokbuf[64];
	struct yl_token **tokp;
	char const **tytab;
	struct yl_symb *ur,*gb;
	_int_u ur_cnt;
	_int_u gb_cnt;
	_64_u x;
};
struct yl_type;
struct yl_symb {
	struct evy_symb s;
	struct evy_em em;
	struct yl_type *ty;
	struct yl_symb *next;
};

struct yl_symb* yl_symb(char const*,_int_u);

//short for working structure
struct yl_wkstrc {
	struct yl_symb *s;	
	_64_u flags;
	_64_u x;
	struct evy_em *em;
};

struct yl_type {
	_64_u kind;
};

enum {
	_ylt_void,
	_ylt_8_u
};
#define iftok(__kind,__id)\
	if (yl_tok->kind == __kind && yl_tok->id == __id)
//keyword
#define _yl_void (0|yltype_bit)
#define _yl_8_u (1|yltype_bit)
#define _yl_e(__x) ((2+__x)|yltype_bit)
//keychar
#define _yl_equal		0
#define _yl_semicolon	1
#define _yl_l_brace		2
#define _yl_r_brace		3
#define _yl_comma		4
#define _yl_dot			5
#define _yl_l_paren		6
#define _yl_r_paren		7
#define _yl_arth (1<<0)
enum {
	_yl_ident,
	__yl_num,
	_yl_keywd,
	_yl_keychar
};
#define yl_istype(__kind)((__kind)&(1<<8))
#define yltype_bit (1<<8)
extern char const *yl_kctab[64];
extern char const *yl_tktab[4];
extern char const *yl_titab[8];
#define _yl_let 1
#define _yl_num 2
#define _yl_spec 4
#define _yl_ignore 8
#define _yl_underscore 16
#define _yl_char 32
#define __yl_dot 64
#define _yl_kind(__k)((__k)<<16)
extern struct yl_struc yl_comm;
struct yl_token *yl_lex(void);
void yl_ulex(struct yl_token*);
_8_s yl_expecttok(_64_u,_64_u);
void yl_init(void);
struct yl_token *yl_peektok(void);
void yl_loadkeywds(struct yl_keywd*,_int_u);
struct yl_token *yl_nexttok(void);
_8_s yl_nexttokis(_64_u,_64_u);
extern struct yl_token *yl_tok;
void yl_lexinit(void);
//parser.c
void yl_decl(struct yl_token*);
void yl_expr(struct yl_token*);
void yl_primary_expr(struct yl_wkstrc*);
void yl_assignment_expr(struct yl_wkstrc*);
void yl_process(void);
void yl_init0(void);
#endif/*__yl__h*/
