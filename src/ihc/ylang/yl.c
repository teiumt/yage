#include "yl.h"
#include "../../io.h"
#include "../../m_alloc.h"
#define _ _yl_ignore
struct yl_struc yl_comm = {
	.chrmap = {
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_yl_char|_yl_kind(_yl_l_paren),_yl_char|_yl_kind(_yl_r_paren),_yl_char,_yl_char,_yl_char|_yl_kind(_yl_comma),_yl_char,__yl_dot|_yl_kind(_yl_dot),_yl_char,
	_yl_num,_yl_num,_yl_num,_yl_num,_yl_num,_yl_num,_yl_num,_yl_num,_yl_num,_yl_num,_,_yl_char|_yl_kind(_yl_semicolon),_,_yl_char|_yl_kind(_yl_equal),_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_yl_underscore,
	_,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,
	_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_let,_yl_char|_yl_kind(_yl_l_brace),_,_yl_char|_yl_kind(_yl_r_brace),_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
	_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_
	},
	.tokp = yl_comm.tokbuf
};

char const *yl_kctab[64] = {
	"EQUAL",
	"SEMICOLON"
};

struct yl_token *yl_nexttok(void) {
	if (yl_comm.tokp>yl_comm.tokbuf) {
		return *(--yl_comm.tokp);
	}
	struct yl_token *tok;
	tok = yl_lex();
	if (!tok)
		return NULL;
	if (tok->kind == _yl_ident) {
		if (tok->s != NULL) {	
		struct yl_keywd *kw;
		kw = f_lhash_get(&yl_comm.kws,tok->s,tok->len);
		if (kw != NULL) {
			tok->kind = _yl_keywd;
			tok->id = kw->id;
		}

	//	printf("lookup for '%w' %s.\n",tok->s,tok->len, !kw?"failed":"succceded");
		
		}
	}

	return tok;
}

char const *yl_tktab[4] = {
	"IDENT",
	"NUMBER",
	"KEYWD",
	"KEYCHAR"
};

char const *yl_titab[8] = {
	"EQUAL",
	"SEMICOLON",
	"LEFT BRACE",
	"RIGHT BRACE",
	"COMMA",
	"DOT",
	"LEFT PAREN",
	"RIGHT PAREN"
};

_8_s yl_expecttok(_64_u __kind, _64_u __id) {
	struct yl_token *tok;
	tok = yl_nexttok();
	if (!tok) {
		printf("expect: null token.\n");
		return -1;
	}
	if (tok->kind == __kind && tok->id == __id) {
		printf("expect correct.\n");
		return 0;
	}
	if (__kind>3 || __id > 7) {
		printf("dident expect that.\n");
		return -1;
	}
	printf("expected {%u'%s,%u'%s} got {%u'%s,%u'%s}.\n", __kind,yl_tktab[__kind],__id,yl_titab[__id&~yltype_bit],tok->kind,yl_tktab[tok->kind],tok->id,yl_titab[tok->id&~yltype_bit]);
	return -1;
}

void yl_ulex(struct yl_token *__tok) {
	*(yl_comm.tokp++) = __tok;
}

struct yl_token *yl_peektok(void) {
	struct yl_token *tok = yl_nexttok();
	yl_ulex(tok);
	return tok;
}

_8_s yl_nexttokis(_64_u __kind,_64_u __id) {
	struct yl_token *tok;
	tok = yl_nexttok();
	if (!tok)
		return -1;
	
	if (tok->kind == __kind && tok->id == __id) {
		return 0;
	}
	yl_ulex(tok);
	return -1;
}

void yl_loadkeywds(struct yl_keywd *kw, _int_u __n) {
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		f_lhash_put(&yl_comm.kws,kw->str,kw->len,kw);
		kw++;
	}
}
void yl_init(void) {
	yl_comm.bitmap['*'] = _yl_arth;
	yl_comm.bitmap['+'] = _yl_arth;
	yl_comm.bitmap['-'] = _yl_arth;
	yl_comm.bitmap['/'] = _yl_arth;
	f_lhash_init(&yl_comm.kws);
	f_lhash_init(&yl_comm.env);
}
#define NKWDS 5
static struct yl_keywd keywds[] = {
	{"_8_u",4,yltype_bit|_yl_8_u},
	{"_16_u",5,yltype_bit|_yl_8_u},
	{"_32_u",5,yltype_bit|_yl_8_u},
	{"_64_u",5,yltype_bit|_yl_8_u},
	{"void",4,0}
};

void yl_init0(void) {
	yl_lexinit();
	evy_init();
	yl_init();
	yl_loadkeywds(keywds,NKWDS);
	evy_resin();
	evy_functab.init();
	yl_comm._env = NULL;
	yl_comm.ur = NULL;
	yl_comm.gb = NULL;
	yl_comm.ur_cnt = 0;
	yl_comm.gb_cnt = 0;
}

struct yl_symb* yl_symb(char const *__str, _int_u __len) {
	if (__len>=64)
		return NULL;
	struct yl_symb *s;
	if (yl_comm._env != NULL) {
		s = f_lhash_get(yl_comm._env,__str,__len);
		if (s != NULL) {
			return s;
		}
		s = f_lhash_get(&yl_comm.env,__str,__len);
		if (s != NULL) {
			yl_comm.x = _yl_gv;
			return s;
		}
        s = m_alloc(sizeof(struct yl_symb));
        evy_symb(s,__str,__len);
        f_lhash_put(yl_comm._env,__str,__len,s);
		yl_comm.ur_cnt++;
		s->next = yl_comm.ur;
		yl_comm.ur = s;
		yl_comm.x = _yl_fsv;
		return s;
	} else {
	s = f_lhash_get(&yl_comm.env,__str,__len);
	if (!s) {
		s = m_alloc(sizeof(struct yl_symb));
		evy_symb(s,__str,__len);
		f_lhash_put(&yl_comm.env,__str,__len,s);
		yl_comm.gb_cnt++;
		s->next = yl_comm.gb;
		yl_comm.gb = s;
		yl_comm.x = _yl_gv;
	}
	}
	return s;
}
