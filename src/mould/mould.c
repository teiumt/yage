# include "mould.h"
# include "../string.h"
# include "../ffly_def.h"
_8_u 
is_keywd(bucketp __tok, _8_u __val) {
	return (__tok->sort == _keywd && __tok->val == __val);
}

void static
to_keyword(bucketp __tok, _8_u __val) {
	__tok->sort = _keywd;
	__tok->val = __val;
}
// next token
bucketp mld_nexttok(void) {
	bucketp tok;
	if (!(tok = mld_lex())) return NULL;
	if (tok->sort == _ident)
		mld_maybe_keyword(tok);
	return tok;
}

bucketp mld_peektok(void) {
	bucketp p;
	ulex(p = mld_nexttok());
	return p;
}
# include "../io.h"
void
mld_maybe_keyword(bucketp __tok) {
	if (__tok->p == NULL) {
		printf("error: lexer.\n");
		return;
	}

	if (!str_cmp((char*)__tok->p, "cp"))
		to_keyword(__tok, _keywd_cp);
	else if (!str_cmp((char*)__tok->p, "exit"))
		to_keyword(__tok, _keywd_exit);
	else if (!str_cmp((char*)__tok->p, "end"))
		to_keyword(__tok, _keywd_end);
	else if (!str_cmp((char*)__tok->p, "entry"))
		to_keyword(__tok, _keywd_entry);
	else if (!str_cmp((char*)__tok->p, "echo"))
		to_keyword(__tok, _keywd_echo);
	else if (!str_cmp((char*)__tok->p, "waitall"))
		to_keyword(__tok, _keywd_waitall);
}
