# include "m_alloc.h"
#define F_ALLOCDEF(__name, __type)\
	__type *__name;\
	__name = (__type*)m_alloc(sizeof(__type));

#define __ffly_inna(__dst, __src) \
    if ((__dst) != NULL) *(__dst) = (__src);
#define __ffly_finn(__p) \
	if ((__p) != NULL) __f_mem_free((void*)(__p))
