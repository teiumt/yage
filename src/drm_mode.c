# include "drm_mode.h"
# include "string.h"
# include "io.h"
# include "system/errno.h"
# include "m_alloc.h"
#define alloc_cpy(__d, __s, __c)\
	__d = m_alloc(__c);\
	memcpy(__d, __s, __c);

char const *drmmode_cntrtab[DRM_MODE_CONNECTER_MAX] = {
	"Unknown",
	"VGA",
	"DVII",
	"DVID",
	"DVIA",
	"Composite",
	"SVIDEO",
	"LVDS",
	"Component",
	"9PinDIN",
	"DisplayPort",
	"HDMIA",
	"HDMIB",
	"TV",
	"eDP",
	"VIRTUAL",
	"DSI",
	"DPI",
	"WRITEBACK"
};
int drmmode_cursor_move(int __fd,_32_u __crtc,_32_s __x,_32_s __y) {
	struct drm_mode_cursor arg;
	bzero(&arg, sizeof(arg));
	arg.flags = DRM_MODE_CURSOR_MOVE;
	arg.crtc_id = __crtc;
	arg.x = __x;
	arg.y = __y;

	int r;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_CURSOR, &arg);
	if (r<0) {
		printf("failed to move cursor.\n");
	}

	return r;
}

int drmmode_cursor_set(int __fd,_32_u __crtc,_32_u __bo_handle,_32_u __width,_32_u __height) {
	struct drm_mode_cursor arg;
	bzero(&arg, sizeof(arg));
	arg.flags = DRM_MODE_CURSOR_BO;
	arg.crtc_id = __crtc;
	arg.width = __width;
	arg.height = __height;
	arg.handle = __bo_handle;
	int r;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_CURSOR, &arg);
	if (r<0) {
		printf("failed to set cursor.\n");
	}

	return r;

}

int drmmode_rs_get(struct drm_mode_res *rs, int __fd) {
	struct drm_mode_card_res counts;
	bzero(&counts, sizeof(counts));
	int r;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_GETRESOURCES, &counts);
	if (r == -1) {
		printf("failed to get resources.\n");
	}


	bzero(&rs->rs, sizeof(rs->rs));
	if (counts.count_crtcs>0) {
		/*
			needs to be freed by user
		*/
		rs->rs.crtc_id_ptr = (_64_u)m_alloc(counts.count_crtcs*sizeof(_32_u));
	}
	if (counts.count_connectors>0) {
		rs->rs.connector_id_ptr = (_64_u)m_alloc(counts.count_connectors*sizeof(_32_u));
	}
	rs->rs.count_crtcs = counts.count_crtcs;
	rs->rs.count_connectors = counts.count_connectors;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_GETRESOURCES, &rs->rs);
	if (r == -1) {
		printf("failed to get resources.\n");
	}

	return r;
}
int drmmode_ctr_get(struct drm_mode_ctr *ctr, int __fd, _32_u __id) {
	struct drm_mode_get_connector counts;
	int r;

	bzero(&counts, sizeof(counts));
	counts.connector_id = __id;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_GETCONNECTOR, &counts);
	if (r == -1) {
		printf("failed to get connector.\n");
		return -1;
	}

	struct drm_mode_get_connector c;
	bzero(&c, sizeof(c));

	c.connector_id = __id;
	if (counts.count_modes>0) {
		c.modes_ptr = (_64_u)m_alloc(counts.count_modes*sizeof(struct drm_mode_modeinfo));
	}

	printf("--> %u.\n", counts.count_modes);
	c.count_modes = counts.count_modes;

	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_GETCONNECTOR, &c);
	if (r == -1) {

	}

	ctr->modes = (struct drm_mode_modeinfo*)c.modes_ptr;
	ctr->count_modes = c.count_modes;
	return r;
}

int drmmode_connector_get(struct drm_mode_get_connector *__cntr, int __fd, _32_u __id) {
	int r;	
	bzero(__cntr, sizeof(struct drm_mode_get_connector));
	__cntr->connector_id = __id;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_GETCONNECTOR, __cntr);
	if (r == -1) {
		printf("failed to get connector.\n");
		return -1;
	}
}

void drmmode_disctr(struct drm_mode_get_connector *__cntr) {
	printf(
		"count_modes: %u\n"
		"count_props: %u\n"
		"count_encoders: %u\n"
		"encoder_id: %u\n"
		"connector_id: %u\n"
		"connector_type: %u: %s\n"
		"connector_type_id: %u\n"
		"connection: %u\n"
		"mm_width: %u\n"
		"mm_height: %u\n"
		"subpixel: %u\n",
		__cntr->count_modes,
		__cntr->count_props,
		__cntr->count_encoders,
		__cntr->encoder_id,
		__cntr->connector_id,
		__cntr->connector_type,
		drmmode_cntrtab[__cntr->connector_type],
		__cntr->connector_type_id,
		__cntr->connection,
		__cntr->mm_width,
		__cntr->mm_height,
		__cntr->subpixel
	);
}

int drmmode_crtc_set(int __fd, _32_u __ctrc_id, _32_u __bufid, _32_u __x, _32_u __y, _32_u *__ctrs, int __cnt, struct drm_mode_modeinfo *__mode) {
	struct drm_mode_crtc c;
	bzero(&c, sizeof(c));
	c.x = __x;
	c.y = __y;
	c.crtc_id = __ctrc_id;
	c.fb_id = __bufid;
	c.set_connectors_ptr  = (_64_u)__ctrs;
	c.count_connectors = __cnt;
	if (__mode != NULL) {
		c.mode = *__mode;
		c.mode_valid = 1;
	}
	int r;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_SETCRTC, &c);
	if (r == -1) {
		printf("crtc failed to set.\n");
	}
	return r;
}


int drmmode_fb_add(int __fd, _32_u __width, _32_u __height, _8_u __depth, _8_u __bpp, _32_u __pitch, _32_u __bo, _32_u *__id) {
	struct drm_mode_fb_cmd f;
	int r;

	printf("framebuffer: bpp: %u, depth: %u.\n", __bpp, __depth);
	bzero(&f, sizeof(f));
	f.width = __width;
	f.height = __height;
	f.pitch = __pitch;
	f.bpp = __bpp;
	f.depth = __depth;
	f.handle = __bo;
	
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_ADDFB, &f);
	if (r == -1) {
		printf("failed to add framebuffer, %s\n", strerror(errno));
	}
	*__id = f.fb_id;
	return r;
}

int drmmode_fb_rm(int __fd, _32_u __id) {
	int r;
	r = DRM_IOCTL(__fd, DRM_IOCTL_MODE_RMFB, &__id);

	if (r == -1) {
		printf("failed to remove framebuffer.\n");
	}
	return r;
}


void drmmode_disrs(struct drm_mode_card_res *__rs) {
	printf(
		"count_fbs: %u\n"
		"count_crtcs: %u\n"
		"count_connectors: %u\n"
		"count_encoders: %u\n"
		"min_width: %u\n"
		"max_width: %u\n"
		"min_height: %u\n"
		"max_height: %u\n",
		__rs->count_fbs,
		__rs->count_crtcs,
		__rs->count_connectors,
		__rs->count_encoders,
		__rs->min_width,
		__rs->max_width,
		__rs->min_height,
		__rs->max_height
	);
}

void drmmode_disinfo(struct drm_mode_modeinfo *__info) {
	printf(
		"clock: %u\n"
		"hdisplay: %u\n"
		"hsync_start: %u\n"
		"hsync_end: %u\n"
		"htotal: %u\n"
		"hskew: %u\n"
		"vdisplay: %u\n"
		"vsync_start: %u\n"
		"vsync_end: %u\n"
		"vtotal: %u\n"
		"vscan: %u\n"
		"vrefresh: %u\n",
		__info->clock,
		__info->hdisplay,
		__info->hsync_start,
		__info->hsync_end,
		__info->htotal,
		__info->hskew,
		__info->vdisplay,
		__info->vsync_start,
		__info->vsync_end,
		__info->vtotal,
		__info->vscan,
		__info->vrefresh
	);
	printf("flags: %u, type: %u.\n", __info->flags, __info->type);
	printf("name: %s.\n",__info->name);
}

void drmmode_discrt(struct drm_mode_ctr *__crt) {
	_int_u i;
	i = 0;
	for(;i != __crt->count_modes;i++) {
		struct drm_mode_modeinfo *m;
		m = __crt->modes+i;
		printf("\n########### %u.\n", i);
		drmmode_disinfo(m);
	}
}

int drmmode_page_flip(int __fd, int __crtc_id,int __fb_id,_32_u __flags, void *__user_data) {
	struct drm_mode_crtc_page_flip flip;
	mem_set(&flip,0,sizeof(flip));

	flip.crtc_id = __crtc_id;
	flip.fb_id = __fb_id;
	flip.flags = __flags;
	flip.user_data = __user_data;
	return DRM_IOCTL(__fd, DRM_IOCTL_MODE_PAGE_FLIP, &flip);
}

int drm_event_handle(int fd, char *buffer) {
	int len;
	struct drm_event *e;
	len = read(fd,buffer,1024);
	if (!len)
		return 0;
	else if (len<sizeof(struct drm_event))
		return -1;
	return len;
}

