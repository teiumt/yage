# include "dem.h"
# include "system/io.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "dep/mem_cpy.h"
void static fout(_ulonglong __arg, void *__buf, _int_u __size, _64_u __offset) {
	char *p;
	p = (char*)__f_mem_alloc(__size+1);
	f_mem_cpy(p, __buf, __size);
	p[__size] = '\0';
	printf("%s", p);
}

static struct f_gwn_io out_fout = {
	.io = fout
};

struct f_dem_gwn dem_gwn;
struct f_gwn _f_dem_fout = {
	.dst = &out_fout,
};
extern struct f_dem_struc ds_chardump;
static struct f_dem_struc *ds[] = {
	&ds_chardump
};

void static
_in(_ulonglong __arg, void *__buf, _int_u __size, _64_u __offset) {
	struct f_gwn_io *io = (struct f_gwn_io*)__arg;
	io->io(io->arg, __buf, __size, __offset);
}

void static
_out(_ulonglong __arg, void *__buf, _int_u __size, _64_u __offset) {
	struct f_gwn_io *io = (struct f_gwn_io*)__arg;
	io->io(io->arg, __buf, __size, __offset);
}

void static
_stat(_ulonglong __arg, struct f_gwn_stat *__stat) {
	struct f_gwn_io *io = (struct f_gwn_io*)__arg;
	io->stat(io->arg, __stat);
}

void f_dem(_8_u __what, struct f_gwn *__gwn) {
	__gwn->dem->core.in = _in;
	__gwn->dem->core.out = _out;
	__gwn->dem->core.in_arg = __gwn->src;
	__gwn->dem->core.out_arg = __gwn->dst;
	__gwn->dem->core.stat = _stat;
	ds[__what]->dem(__gwn->dem);
}
