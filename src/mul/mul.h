# ifndef __mul__h
# define __mul__h
# include "../y_int.h"
# define incrp(__mul) __mul->cur++
typedef struct bucket {
	_8_u sort;
	_8_u val;
	_8_u *beg, *end;
	char *p;
	_16_u len;
	struct bucket *next;
} *bucketp;

typedef struct entry {
	struct entry *next, *fd;
	_8_u const* key;
	_int_u len;
	void *p;
} *entryp;

typedef struct hash {
	entryp *table;
	entryp head;
} *hashp;

typedef struct brick {
	struct brick *next;
	_8_u *p, *end;
} *brickp;

enum {
	_quote,
	_comma
};

enum {
	_unknown,
	_ident,
	_keywd,
	_chr,
	_str,
	_brick
};

typedef struct mul {
	_8_u *p, *end;
	_8_u *cur;
	brickp top;
	struct hash bricks;
} *mulp;

_8_u at_eof(mulp);

void to_free(void*);

void hash_init(hashp);
void hash_put(hashp, _8_u const*, _int_u, void*);
void* hash_get(hashp, _8_u const*, _int_u);
void hash_free(hashp);

void lex(mulp, bucketp*);

void mul_prepare(mulp);
void mul_ld(mulp, char const*);
void mul_process(mulp);

brickp get_brick(mulp, char const*);
_int_u mul_bricklen(brickp);
void mul_brickw(void*, _int_u, _int_u, brickp);
void mul_brickr(void*, _int_u, _int_u, brickp);

void mul_oust(mulp, char const*);

void mul_cleanup(mulp);
void lexer_cleanup();
# endif /*__mul__h*/
