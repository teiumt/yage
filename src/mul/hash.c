# include "mul.h"
# include "../string.h"
# include "../malloc.h"
# include "../stdio.h"
_64_u sum(_8_u const *__key, _int_u __len) {
	_8_u const *end = __key+__len;
	_8_u const *p = __key;
	_64_u ret = ~(_64_u)0;
	while(p != end) {
		ret ^= *p;
		ret = ret<<4|ret>>60;
		p++;
	}
	return ret;
}

void hash_init(hashp __table) {
	__table->table = (entryp*)malloc(0x100*sizeof(entryp));
	entryp *cur = __table->table;
	entryp *end = cur+0x100;
	while(cur != end) *(cur++) = NULL;
	__table->head = NULL;
}

void hash_put(hashp __table, _8_u const *__key, _int_u __len, void *__p) {
	_64_u val = sum(__key, __len);
	entryp *table = __table->table+(val&0xff);

	entryp e = (entryp)malloc(sizeof(struct entry));
	e->next = *table;
	*table = e;

	e->fd = __table->head;
	__table->head = e;

	memdup((void**)&e->key, __key, __len);
	e->len = __len;
	e->p = __p;
}

void* hash_get(hashp __table, _8_u const *__key, _int_u __len) {
	_64_u val = sum(__key, __len);
	entryp p;
	if (!(p = *(__table->table+(val&0xff)))) {
		return NULL;
	}

	while(p != NULL) {
		if (p->len == __len)
			if (!memcmp(p->key, __key, __len)) return p->p;
		p = p->next;
	}
	fprintf(stderr, "lookup failed.\n");
	return NULL;
}

void hash_free(hashp __table) {
	free(__table->table);

	entryp cur = __table->head;
	while(cur != NULL) {
		entryp bk = cur;
		cur = cur->fd;
		free((void*)bk->key);
		free(bk);
	}
}
