# include "../remf.h"
# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <malloc.h>
# include <stdio.h>

/*
	TODO:
		move away from glibc.

		make better - dont realy care about efficiency for now
		as files are only small but needs to be done at some point.
*/
int fd;

char const* regtype_s(_8_u __type) {
	switch(__type) {
		case FF_RG_NULL:	return "null";
		case FF_RG_PROG:	return "program";
		case FF_RG_STT:		return "string table";
		case FF_RG_SYT:		return "symbol table";
	}

	return "unknown";
}

char const* segtype_s(_8_u __type) {
	switch(__type) {
		case FF_SG_NULL: return "null";
		case FF_SG_STACK:	return "stack segment";
		case FF_SG_PROG:	return "program segment";
	}
	return "unknown";
}

char const* sytype_s(_8_u __type) {
	switch(__type) {
		case FF_SY_NULL: return "null";
		case FF_SY_IND: return "ind";
		case FF_SY_GBL: return "global";
		case FF_SY_LCA: return "local";
	}
	return "unknown";
}

void prhdr(remf_hdrp __hdr) {
	char *i = __hdr->ident;
	printf("ident: ");
	while(i != __hdr->ident+FF_REMF_IL-1 && *(i+1) != '\0')
		printf("%c, ", *(i++));
	printf("%c\n", *i);	

	printf("adr: %u\n", __hdr->adr);

	if (__hdr->routine == FF_REMF_NULL)
		printf("no entry point.\n");
	else
		printf("entry, at: %u\n", __hdr->routine);
}

void prsy(remf_reg_hdrp __sttr, remf_syp __sy, _int_u __no) {
	char *name = (char*)malloc(__sy->l);
	lseek(fd, (__sttr->offset+__sttr->size)-__sy->name, SEEK_SET);
	read(fd, name, __sy->l);

	printf("%u: symbol,\ttype: %s,\tname: %s\t: %u,\tloc: %u,\treg: %u, len: %u\n", __no, sytype_s(__sy->type),
		name, __sy->name, __sy->loc, __sy->reg, __sy->l);
	free(name);
}

void prseg(remf_seg_hdrp __seg, _int_u __no) {
	printf("%u: segment,\ttype: %s,\tadr: %u,\toffset: %u,\tsize: %u\n", __no, segtype_s(__seg->type),
		__seg->adr, __seg->offset, __seg->sz);
}

void prreg(remf_reg_hdrp __reg, _int_u __no) {
	char *name = (char*)malloc(__reg->l);
	lseek(fd, __reg->name, SEEK_SET);
	read(fd, name, __reg->l);

	printf("%u: region,\tname: %s,\tbeg: %u,\tend: %u,\tsize: %u\ttype: %s,\t%s,\tadr: %u\n", __no, name, __reg->offset,
		__reg->offset+__reg->size, __reg->size, regtype_s(__reg->type), !__reg->size?"empty":"not empty", __reg->adr);
}

void prhok(remf_hokp __hok, _int_u __no) {
	printf("%u: hook,\toffset: %u,\tto: %u,\tadr: %u,\tfrag: %u\n", __no, __hok->offset, __hok->to, __hok->adr, __hok->f);
}

void prrel(remf_relp __rel, _int_u __no) {
	printf("%u: relocate,\toffset: %u,\tsf: %u,\tadr: %u,\tfrag: %u\n", __no, __rel->offset, __rel->to, __rel->adr, __rel->f);
}

int main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("please provide file,\n\tusage: readremf [file].\n");
		return -1;
	}

	if (access(__argv[1], F_OK) == -1) {
		printf("file doesen't exist.\n");
		return -1;
	}

	if ((fd = open(__argv[1], O_RDONLY)) == -1) {
		
		return -1;
	}

	struct remf_reg_hdr sttr;

	struct remf_hdr hdr;
	read(fd, &hdr, remf_hdrsz);

	if (hdr.sttr == FF_REMF_NULL) {
		printf("no string table pressent\n");
	} else {
		lseek(fd, hdr.sttr, SEEK_SET);
		read(fd, &sttr, remf_reghdrsz);
		prreg(&sttr, 0);
	}
	prhdr(&hdr);

	struct remf_ft ft;
	pread(fd, &ft, sizeof(struct remf_ft), hdr.ft);

	struct remf_frag *fr, *f;
	fr = (struct remf_frag*)malloc(ft.n*sizeof(struct remf_frag));

	_int_u fof = ft.array-((ft.n-1)*sizeof(struct remf_frag));
	printf("number of fragments: %u, offset: %u, array: %u\n", ft.n, fof, ft.array);
	pread(fd, fr, ft.n*sizeof(struct remf_frag), fof);
	_int_u i;
	i = 0;
	while(i != ft.n) {
		f = fr+(i++);
		printf("fragment; id: %u, src: %u, sz: %u, offset: %u\n", f->id, f->src, f->size, fof+(i*sizeof(struct remf_frag)));
	}


	free(fr);

	struct remf_hok hok;
	if (hdr.hk != FF_REMF_NULL) {
		printf("hook, entries: %u\n", hdr.nhk);
		_int_u i;
		_64_u offset = hdr.hk;
		for(i = 0;i != hdr.nhk;i++,offset-=remf_hoksz) {
			lseek(fd, offset, SEEK_SET);
			read(fd, &hok, remf_hoksz);
			prhok(&hok, i);
		}
	} else
		printf("no hook/s present.\n");

	struct remf_seg_hdr seg;
	if (hdr.sg != FF_REMF_NULL) {
		printf("segment, entries: %u\n", hdr.nsg);
		_int_u i;
		_64_u offset = hdr.sg;
		for(i = 0;i != hdr.nsg;i++,offset-=remf_seghdrsz) {
			lseek(fd, offset, SEEK_SET);
			read(fd, &seg, remf_seghdrsz);
			prseg(&seg, i);
		}
	} else
		printf("no segment/s present.\n");

	struct remf_reg_hdr reg;
	if (hdr.rg != FF_REMF_NULL) {
		printf("region, entries: %u\n", hdr.nrg);
		_int_u i;
		_64_u offset = hdr.rg;
		for(i = 0;i != hdr.nrg;i++,offset-=remf_reghdrsz+reg.l) {
			lseek(fd, offset, SEEK_SET);
			read(fd, &reg, remf_reghdrsz);
			prreg(&reg, i);
			if (reg.type == FF_RG_SYT) {
				_int_u size;
				remf_syp bed;
				remf_syp sy = (remf_syp)malloc(size = reg.size);
				bed = sy;

				lseek(fd, reg.offset, SEEK_SET);
				read(fd, sy, size);

				remf_syp end = (remf_syp)((_8_u*)sy+size);
				_int_u i = 0;
				while(sy != end) {
					prsy(&sttr, sy, i++);
					sy++;
				}
				free(bed);
			}
		}
	} else
		printf("no region/s present.\n");

	struct remf_rel rel;
	if (hdr.rl != FF_REMF_NULL) {
		_int_u i;
		_64_u offset = hdr.rl;
		for(i = 0;i != hdr.nrl;i++,offset-=remf_relsz) {
			lseek(fd, offset, SEEK_SET);
			read(fd, &rel, remf_relsz);
			prrel(&rel, i);
		}
	} else
		printf("no rel present.\n");

	close(fd);
	return 0;
}
