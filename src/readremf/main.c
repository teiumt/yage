# include "../remf.h"
# include <unistd.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <malloc.h>
# include <stdio.h>
# include <string.h>
char const static *stident[] = {
	"TANG",
	"WA"
};

char const static *rtype[] = {
	"16-BIT-RELOC",
	"32-BIT-RELOC"
};

void static sflags(char *__buf, _8_u __flags) {
	char *p = __buf;
	if (__flags&F_SY_RS) {
		memcpy(p, "RESOLV", 6);
		p+=6;
	} else {
		strcpy(__buf, "NONE");
		return;
	}
	*p = '\0';
}

void static rflags(char *__buf, _8_u __flags) {
	char *p = __buf;
	if (__flags&F_REMF_R_UDD) {
		memcpy(p, "UDD", 3);
		p+=3;
	} else {
		strcpy(__buf, "NONE");
		return;
	}
	*p = '\0';
}

char const* _rtident(_64_u __idx) {
	switch(__idx) {
		case F_RG_STT:
			return "STRING TABLE";
		case F_RG_SYT:
			return "SYMBOL TABLE";
		case F_RG_RLT:
			return "RELOC TABLE";
	}
	return "PROGRAM BITS";
}

void static chrdmp(_8_u *__p, _int_u __size) {
	_int_u i = 0;
	for(;i != __size;i++) {
		_8_u v = *(__p+i);
		char c;
		if ((v>='a'&&v<='z')||(v>='A'&&v<='Z'))
			c = v;
		else
			c = '.';
		printf("%c", c);
	}
	printf("\n");
}

int static fd;
int main(int __argc, char const *__argv[]) {
    if (__argc<2) {
        printf("please provide file,\n\tusage: readremf [file].\n");
        return -1;
    }

    if (access(__argv[1], F_OK) == -1) {
        printf("file doesen't exist.\n");
        return -1;
    }

    if ((fd = open(__argv[1], O_RDONLY)) == -1) {

        return -1;
    }

	struct remf_hdr hdr;
	read(fd, &hdr, remf_hdrsz);
	_int_u mc = 0;

	if (*hdr.ident != FF_REMF_MAG0) {
		printf("mag0 mismatch.\n");
		goto _sk;	
	}

	mc = 1;
	if (hdr.ident[1] != FF_REMF_MAG1) {
		printf("mag1 mismatch.\n");
		goto _sk;
	}

	mc = 2;
	if (hdr.ident[2] != FF_REMF_MAG2) {
		printf("mag2 mismatch.\n");
		goto _sk;
	}

	mc = 3;
	if (hdr.ident[3] != FF_REMF_MAG3) {
		printf("mag3 mismatch.\n");
		goto _sk;
	}
	mc = 4;
_sk:
	printf("HDR MAGIC MATCH COUNT: %u.\n", mc);

	printf("REMF_HEADER' RSTS: %u, RN: %u, RG: %u, ENTRY: %u(%s), ADR: %u.\n", hdr.rsts, hdr.rn, hdr.rg, (hdr.flags&F_REMF_EPDEG)>0?0:hdr.routine, (hdr.flags&F_REMF_EPDEG)>0?"OKAY":"NONE", hdr.adr);
	_int_u i, ii;
	struct remf_tang *t, *ct;
	struct remf_wa *w, *cw;
	if (hdr.tt != FF_REMF_NULL) {

	struct remf_tt tt;
	pread(fd, &tt, sizeof(struct remf_tt), hdr.tt);
	printf("TAKE_TABLE' T_M: %u, T_N: %u, T: %u,\tW_M: %u, W_N: %u, W: %u\n", tt.t_m, tt.t_n, tt.t, tt.w_m, tt.w_n, tt.w);	

	t = (struct remf_tang*)malloc(tt.t_n*sizeof(struct remf_tang));
	pread(fd, t, tt.t_n*sizeof(struct remf_tang), tt.t);

	w = (struct remf_wa*)malloc(tt.w_n*sizeof(struct remf_wa));
	pread(fd, w, tt.w_n*sizeof(struct remf_wa), tt.w);

	i = 0;
	printf("TANG'\n");
	for(;i != tt.t_n;i++) {
		ct = t+i;
		printf("\tix: %u,\tsrc: %u,\tadr: %u\tsize: %u\n", i, ct->src, ct->adr, ct->size);
	}

	i = 0;
	printf("WA'\n");
	for(;i != tt.w_n;i++) {
		cw = w+i;
		printf("\tadr: %u.\n", cw->adr);
	}

	}

	/*
		only present after linking, i think?
	*/
	if (hdr.sg != FF_REMF_NULL) {
		struct remf_seg_hdr *sh, *s;
		sh = (struct remf_seg_hdr*)malloc(hdr.ssts);
		pread(fd, sh, hdr.ssts, hdr.sg);

		printf("SEGMENT' %u, %u\n", hdr.sg, hdr.sn);
		i = 0;
		for(;i != hdr.sn;i++) {
			s = sh+i;
			printf("\t- type: %u, adr: %u, offset: %u, size: %u.\n", s->type, s->adr, s->offset, s->sz);
		}
	}

	_8_u *stt;
	if (hdr.rg != FF_REMF_NULL) {

	struct remf_reg_hdr *rh, *r;
	rh = (struct remf_reg_hdr*)malloc(hdr.rsts);
	pread(fd, rh, hdr.rsts, hdr.rg);
	
	i = 0;
	for(;i != hdr.rn;i++) {
		r = rh+i;
		printf("inspecting region-%u, typ:(%s), size: %u, n: %u, offset: %u, adr: %u\n", i, _rtident(i), r->size, r->n, r->offset, r->adr);
		if (!r->size)
			continue;

		void *p = malloc(r->size);
		pread(fd, p, r->size, r->offset);
		chrdmp(p, r->size);
		if (i == F_RG_SYT) {
			struct remf_sy *sy, *s;
			sy = (struct remf_sy*)p;
			ii = 0;
			printf("SYMBOL'\n");
			for(;ii != r->n;ii++) {
				s = sy+ii;
				struct remf_stent *e = (struct remf_stent*)(stt+s->name);
				char buf[128], bf[128];
				memcpy(buf, (void*)(((_64_u)e)+F_REMF_STE), e->l);
				buf[e->l] = '\0';
				sflags(bf, s->flags);
				printf("\tname: (%u)%s,\tval: %u,\ttype: %u(%s),\tflags: %u(%s) #%u\n", s->name, buf, s->val, s->type, stident[s->type], s->flags, bf, ii);
				if (s->type == F_SY_WA) {
					cw = w+s->val;
					printf("\t- WA' adr: %u.\n", cw->adr);
				}
			}

			struct remf_agg *a = (struct remf_agg*)(((_8_u*)p)+r->_0);
			ii = 0;
			for(;ii != F_REMF_AGG_SY_N;ii++) {
				char const *id = "UNKNOWN";
				switch(ii) {
					case F_REMF_AGG_SY_GBL:
					id = "GLOBAL";
					break;
				}
				printf("SY_AGG' start: %u, n: %u, id: %s\n", a->start, a->n, id);
				a++;
			}
		} else if (i == F_RG_STT) {
			stt = (_8_u*)p;
			_8_u *i;
			i = stt;
			printf("STENT'\n");
			while(i != stt+r->size) {
				struct remf_stent *e = (struct remf_stent*)i;

				char buf[128];
				memcpy(buf, (void*)(((_64_u)e)|F_REMF_STE), e->l);
				buf[e->l] = '\0';
				printf("\t%s,\t\t\tlen: %u.\n", buf, e->l);
				i+=((e->l+F_REMF_SM)&~F_REMF_SM)+F_REMF_SZ;
			}
			continue;
		} else if (i == F_RG_RLT) {
			struct remf_rel *rl, *_r;
			rl = (struct remf_rel*)p;
			ii = 0;
			printf("RELOC'\n");
			for(;ii != r->n;ii++) {
				char bf[128];
				_r = rl+ii;
				rflags(bf, _r->flags);
				printf("\tsrc: %u, dst: %u, flags: %u(%s), type: %u(%s)\n", _r->src, _r->dst, _r->flags, bf, _r->type, rtype[_r->type]);
			}
		} else {
			// program bits
		}

		free(p);
	}
	}
	close(fd);
}
