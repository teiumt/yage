#ifndef __urek__h
#define __urek__h
#include "turn.h"
/*
	memory usage here is use as must to crunch performance
*/
# include "../resin.h"
# include "../y_int.h"
# include "mp.h"
# include "../def.h"
# define fakget(__off) \
	(*((fak_)->p+__off))
//# define inssize struct(ins)
# define is_flag(__flags, __flag) \
	(((__flags)&(__flag))==(__flag))

#define HOOK 0x1
#define RELOCATE 0x2
#define SIGNED 0x1
#define S_ADR 0x2
#define _of_null 0xff

enum {
	_of_remf,
	_of_elf,
	_of_raw
};

struct fix_s;
extern void **op_tray;
extern void*(*ff_as_getreg)(char const*);
extern _8_u(*ff_as_regsz)(void*);
extern _8_i(*ff_as_suffix)(_8_u);
_8_u extern fix_flgs;
_8_u extern of;
_64_u extern offset;

extern void(*post)(void);
#define LA_LOOSE 0x1
#define NK_LOOSE 0x01

typedef struct region* regionp;

#define setinfo(__fak, __bits, __off) \
	(__fak)->info[__off] = __bits
#define getinfo(__fak, __off) \
	((__fak)->info[__off])

#define _o_reg (_o_reg8|_o_reg16|_o_reg32|_o_reg64)
#define _o_imm (_o_imm8|_o_imm16|_o_imm32|_o_imm64)
#define _o_dis (_o_dis8|_o_dis16|_o_dis32|_o_dis64)
#define _o_label 0x1 // resin
#define _o_reg8 0x2
#define _o_reg16 0x4
#define _o_reg32 0x8
#define _o_reg64 0x10
#define _o_imm8 0x20
#define _o_imm16 0x40
#define _o_imm32 0x80
#define _o_imm64 0x100
#define _o_dis8 0x200
#define _o_dis16 0x400
#define _o_dis32 0x800
#define _o_dis64 0x1000

#define F_AS_ISA(__by)\
	f_ass.stackadr+=__by;
/*
	absole max a line can be in length
*/
#define F_AS_RDEND 0x01

//default entry
#define F_AS_DE "_start"
/*
	entry point designated
*/
#define F_AS_EPD 0x01

#define F_AS_LINEMAX 128
#define F_AS_TXCNK_SHFT 4
#define F_AS_TXCNK_SIZE (1<<F_AS_TXCNK_SHFT)
#define F_AS_TXCNK_MASK (F_AS_TXCNK_SIZE-1)
#define F_AS_TCWL (F_AS_LINEMAX>>F_AS_TXCNK_SHFT)

#define F_AS_UKR	0
#define F_AS_TXR	1
#define F_AS_DTR	2
#define F_AS_BSR	3
struct f_as_frag;
struct f_as_symbol;
typedef struct f_as_symbol *f_as_symbolp;
enum {
	_fx_dis
};

enum {
	_f_as_d_byte,
	_f_as_d_word,
	_f_as_d_dword,
	_f_as_d_qword,
	_f_as_d_entry,
	_f_as_d_globl,
	_f_as_d_segment,
	_f_as_d_region,
	_f_as_d_endof,
	_f_as_d_extern,
	_f_as_d_local,
	_f_as_d_fill
};

struct f_as_v {
	_32_u off;
	/*
		used to correct region sizes
		as in file the reloc does not exist
		ie jmp disp -> '0x00 0x00 0x00 0x00 0x00' <- jmp or instruction with fix
		is discluded 
	*/
	_32_u fix;
};

struct f_as_bas {
	_8_u id;
	_int_u len;
};

struct f_as_blk {
	void *p;
	_8_u id;
	_int_u len;
};

struct f_as_blob;
struct f_as_cg;
struct f_as_cgf {
	void(*func)(struct f_as_cg*);
	void *ctx;
};

struct f_as_dirv {
	_8_u id;
	struct f_as_cgf f;
};

struct f_as_dent {
	char const *name;
	_int_u len;
	struct f_as_dirv *d;
};
struct f_as_dirv* f_as_d_lookup(char const*, _int_u);

enum {
	_f_ae_hash_1,
	_f_ae_left_paren_1,
	_f_ae_right_paren_1,
	_f_ae_percent_1,
	_f_ae_dollar_1,
	_f_ae_comma_01,
	_f_ae_plus_01
};

#define BLOB_DIR	1
#define BLOB_MAC	2
#define BLOB_LABEL	4
#define BLOB_CTL	8
#define BLOB_EXP	16
#define BLOB_INS	32
#define is_bdir(__b)\
	(__b)->id == BLOB_DIR
#define is_bmac(__b)\
	(__b)->id == BLOB_MAC
#define is_blabel(__b)\
	(__b)->id == BLOB_LABEL
#define is_bctl(__b)\
	(__b)->id == BLOB_CRL
typedef struct f_as_blob *f_as_blobp;
// common ground
struct f_as_cg {
	f_as_blobp b;
	struct f_as_cgf *f;
};
/*
	RENAME
*/
#define CH_SY	0x00
#define CH_INT	0x01
#define CH_REG	0x02
#define CH_UK	0x03
#define is_chint(__ch)\
	((__ch)->sort == CH_INT)
#define is_chreg(__ch)\
	((__ch)->sort == CH_REG)
#define is_chsy(__ch)\
	((__ch)->sort == CH_SY)
typedef struct f_as_chomp {
	_8_u sort;
	_8_u flags;
	_ulonglong _0;
	_ulonglong _1;
	_ulonglong _2;
	struct f_as_chomp *next;
} *f_as_chompp;

typedef struct f_as_blob {
	_8_u id;
	struct f_as_blob *exp;
	struct f_as_cg cg;
	struct {
		void *p;
		_int_u len;
	};

	union {
		f_as_chompp ch;
		struct {
			struct f_as_dirv *d;
		};//directive
		struct {

		};//macro
		struct {

		};//label
		struct {

		};//control
		struct {
			void *op_tray;
		};
	};
} *f_as_blobp;

struct f_as_reginfo {
	_8_u l;
};
typedef struct f_as_reg {
#define info priv
	char const *name;
	_int_u len;
	void *priv;
} *f_as_regp;

#define LINK_STRUC struct f_as_link link;
struct f_as_link {
	struct f_as_link *next;
	void *ptr;
};

# include "../bbs.h"
struct f_as_tfx;
#define F_AS_AGG_SY 0
struct f_as_fcom {
	void *p[64];
};

struct f_as_tang;
typedef struct f_as_wa {
	_32_u pm;
	_32_u adr;
	struct f_as_tang *t;
} *f_as_wap;

struct f_as_wa_prv {
	struct f_as_wa w;
	struct f_as_wa_prv *next;
};

#define F_AS_R_UDD 0x01
#define F_AS_R_16 0
#define F_AS_R_32 1
typedef struct f_as_reloc {
	_8_u type, flags;
	f_as_wap d;
	struct f_as_tfx *s;
} *f_as_relocp;

#define F_AS_SY_DEF 0
#define F_AS_SY_GBL 1
struct f_as_label;
struct urek_struc {
	void *op_tray;
	_64_u offset;
	_64_u adr;

	// output format
	_8_u of;
	struct hash symbols;
	struct hash defines;
	struct hash env;
	struct hash regs;

	void(*ins_post)(void);
	char const **globl;
	char const **extrn;
	struct {
		char const *ep;
		_int_u eplen;
		f_as_symbolp sy;
	} ety;
	_64_u stackadr;
	_8_u suffix_map[0x100];
	_8_u chisel_map[0x100];
	_8_u suffix;
	f_as_regp(*getreg)(char const*, _int_u);	
	void *plate[0x100];

	struct {
		struct f_as_cgf f_label;
	} funcs;

	_8_u flags;
	struct f_as_label *curlabel;
	struct f_as_frag *curfrag;

	struct {
		void(*remf)(void);
		void(*elf)(void);
		void(*choice)(void);
	} offn;
	struct f_as_frag *topmost;

	struct f_as_tang *tg_top, *tg_end;
	_int_u tg_n, st_n;
	void(*forge)(void);
	_32_u(*stt)(_8_u*, _int_u);
	struct f_as_region*(*reg_new)(void);
	struct f_as_tfx*(*tfx_new)(void*);
	f_as_symbolp(*symb_new)(void*);
	f_as_relocp(*reloc)(void);
	// where abouts
	f_as_wap(*wa_new)(void*);
	void(*fix)(void);
	void(*apply)(void);
	void *_locl, *_globl;

	_8_u stack[4096];
	_8_u *sp;
	struct f_as_fcom com;
	void *w_locl, *w_globl;
	/*
		save info

		- tang
			- displacment from tang
	*/
	struct f_as_v v;
	struct f_as_tfx *tx;
	struct hash *localsymbs;
};

extern struct urek_struc f_ass;

typedef struct flask {
	_16_u info[4];
	void *p[4];
	_8_u n;
} *flaskp;

extern struct flask *fak_;
/*
	NOTE:
		wedge space is the amount we need to fix
		so 8-bytes of fix space
	we are using wedges to avoid data being spred out over two pages i.e split in an issuemattic way
*/
#define F_AS_WSHFT 3
#define F_AS_WSIZE (1<<F_AS_WSHFT)
#define F_AS_WMASK (F_AS_WSIZE-1)
#define F_AS_STG 0x01
#define F_ASTS_MASK 0xf
#define F_ASTS_SHFT 0
#define F_AST_STATE(__bits) ((__bits)&F_ASTS_MASK)
#define F_AST_BSS(__bits, __state)\
	(__bits = ((__bits)&~F_ASTS_MASK)|(__state))
#define F_AST_BGS(__bits)\
	(((__bits)&F_ASTS_MASK)>>F_ASTS_SHFT)
#define F_asts_OKAY	0
//dwindling 
/*
	in a state of both okay and dead
	ie fdone has not been called and tang is in a
	unfazzed state?????
*/
#define F_asts_DW	1
struct f_as_tb {
	void *w;
	_int_u size;
};
/*
	why???
	struct f_as_tb *b;

	^ linear memory 

	hard to resize and if we did its a total wast
*/
// tang block cluster
struct f_as_tbc {
	_int_u n;
	struct f_as_tb *b;
	struct f_as_tbc *next;
};

typedef struct f_as_tang {
	struct f_as_tang *next;
	_8_u flags, bits;
	_64_u size, space;
	_64_u t;
	

	struct f_as_wa_prv *w;
	/*
		virtual address
	*/
	_64_u adr;

	/*
		where tang data was stored, could be a file
	*/
	_64_u dst;
	/*
		if undergone fixing????
		the end of the tang would have changed
	*/
	_16_u sway;


	struct f_as_tbc *b;
	_int_u tbsz;

	struct {
		void *w0, *w1;
		_16_u w0d;
	}placement;

	_8_u buf[F_AS_WSIZE];
	_int bs;

	_int m, k;
} *f_as_tangp;

// tang fixture
typedef struct f_as_tfx {
	_32_u s_off, e_off;
	// h = host
	_32_u fix;
	struct f_as_tang *h, *e;
	_32_u pm;
	_8_u flags;
} *f_as_tfxp;


struct point {
	struct f_as_tang *t;
};

//tclass
extern _8_i _local;
extern void *__label;

typedef struct segment {
	LINK_STRUC

	struct segment *next;
	_64_u offset;
	_64_u adr;

	_int_u size;
	_8_u buf[200];
	_8_u *fresh;
} *segmentp;

typedef struct f_as_region {
	_8_u id;
	_32_u pm;

} *f_as_regionp;

typedef struct region {
	LINK_STRUC
	_8_u id;
	char const *name;
	struct region *next;
	struct point beg, end;
	_16_u no;
	_64_u adr;
} *regionp;

/*
	remove hook or not use rel for resin,
*/

/*
	offset-start = start of instruction in program memory
*/
typedef struct _relocate {
	LINK_STRUC
	/*
		where in fragment the displacement value is located
	*/
	_32_u dis;

	_8_u flags;
	/*
		offset from frag base to opbase
	*/
	_32_u ob;
	struct frag *f;

} *_relocatep;
typedef struct relocate {
	LINK_STRUC
	struct relocate *next;
	/*
		displacement value location within fragment
	*/
	_64_u dis;

	_8_u flags;
	/*
		rename; offset from fragment base to call/jmp opbase
	*/
	_32_u ob;
//	struct frag *f;
	_16_u *sf;
//	f_as_labelp ll;
} *relocatep;

typedef struct hook {
	LINK_STRUC
	struct hook *next;
	/*
		displacement value location within fragment
	*/
	_64_u dis;

	_8_u flags;
	/*
		rename; offset from fragment base to call/jmp opbase
	*/
	_32_u ob;
	struct frag *f;

	// hook it to .... what?
//	symbolp *to;

} *hookp;

#define OPT_CHARS 0
struct tun_opt {
	_8_u present;
	_8_u type;
	_ulonglong val;
	char const *ch;
	_int_u len;
};

segmentp extern curseg;
regionp extern curreg;

typedef struct st {
	struct st *prev, *next;
	char const *p;
	_8_u l;
} *stp;
/*
	tang.c
*/
struct f_as_tang* f_as_tang_new(void);
void f_as_tout(void);
_32_u extern adr;
stp extern stt_head;
stp extern stt_tail;
_64_u(*ff_as_stt_drop)(void);
// alloca.c
// cleanup
void fix(struct frag*, void*, _8_u, _8_u);
// as.c
f_as_wap f_as_wa_new(void);
void ff_as(char*, char*);
f_as_tfxp f_as_tfx_new(void);

// exp.c
f_as_blobp f_as_eval(turn_linep, _8_u*, _int_u);
struct hash extern env;
// stt.c
_int_u ff_as_stt(char const*, _int_u);
void ff_as_de_init(void);
f_as_blobp f_as_parse(void);
_64_u ff_as_read_no(char*, _int_u*, _8_u*);

_int_u ff_as_stackadr();
void ff_as_isa(_int_u);

// local.c
long long* as_local_new(_16_u);
long long* as_local_get(_16_u);

void f_as_prep(void);
# include "frag.h"
# include "symbol.h"

//output
void urek_amd64(void);
void urek_resin(void);

//formats
void urek_remf(void);

void urek_text_asmbl(void);
void urek_final(void);
void urek_init(void);

#endif/*__urek__h*/
