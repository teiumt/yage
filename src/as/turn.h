#ifndef __tern__h
#define __tern__h
/*
	i think the name got lost???
	it was ether tune or turn for turnip?
	or for turn for diverging direction?
*/
# include "../bbs.h"
#define TURN_OBSIZE 64
struct turn_struc{
	struct bbs_ops ops;
	struct{ 
		void *ctx;
		_64_u offset,limit;
	}in;

	struct{ 
		void *ctx;
		_64_u offset;
	}out;
	struct{
		_int_u off, dst;
		_8_u p[TURN_OBSIZE], *end;
	}outbuf;

	_8_u *sp, stack[256];
};

typedef struct turn_txcnk {
	_8_u *p;
} *turn_txcnkp;

#define TURN_LINEMAX 128
#define TURN_TXCNK_SHFT 4
#define TURN_TXCNK_SIZE (1<<TURN_TXCNK_SHFT)
#define TURN_TXCNK_MASK (TURN_TXCNK_SIZE-1)
#define TURN_TCWL (TURN_LINEMAX>>TURN_TXCNK_SHFT)

#define TURN_LLST 0x01
typedef struct turn_line {
	_8_u *p;
	_int_u len;
	_8_u flags;
	_8_u makeup;
	// line could be split up into diffrent sections
	struct turn_line *next;
} *turn_linep;

#define TURN_RDEND 1
#define TURN_write(...)	   turn_strc.ops.out.write(turn_strc.out.ctx,__VA_ARGS__)
#define TURN_pwrite(...)	  turn_strc.ops.out.pwrite(turn_strc.out.ctx,__VA_ARGS__)
#define TURN_read(...)		turn_strc.ops.in.read(turn_strc.in.ctx,__VA_ARGS__)

//input
turn_linep turn_nextline(void);
turn_txcnkp turn_ntxcnk(void);

void tun_al_cu();
//alloca
void *turn_al(_int_u);
void turn_fr(void*);
void *turn_ral(void*, _int_u);

//commmon
void* turn_smemdup(void*, _int_u);
void* turn_memdup(void*, _int_u);
void turn_oust(_8_u*, _16_u);
_8_u turn_read(void*, _int_u);
void turn_drain(void);
void turn_innit(void);
extern struct turn_struc turn_strc;
#endif/*__tern__h*/
