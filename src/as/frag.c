# include "as.h"
# include "frag.h"
# include "../io.h"
struct frag *fr_head = NULL;
struct frag *curfrag = NULL;

#define PAGE_SHFT 5
#define PAGE_SIZE (1<<PAGE_SHFT)
#define PAGE_MASK (PAGE_SIZE-1)
/*
	fragments from top to bottom

	AUTD:
		find fragment inbetween jump and label
*/
struct frag **fr_tbl = NULL;
#define FR_DONE 0x02
#define FRT_PAGE_SHFT 4
#define FRT_PAGE_SIZE (1<<FRT_PAGE_SHFT)
#define FRT_PAGE_MASK (FRT_PAGE_SIZE-1)
/*
	MIGHTDO:
		store pointers of frags in array
*/
#define MORE F_AS_WSIZE

void static
endsf(f_as_fragp __f) {
	struct f_as_subfrag *sf;
	sf = __f->sf_end;
	_32_u sz = ((sf->size = (__f->offset-sf->off))+(F_AS_WMASK|F_AS_WSIZE))&~F_AS_WMASK;
	sf->end = __f->offset;
	sf->t->space = f_ass.v.off;
	sf->t->size = sf->size;
}

static struct f_as_subfrag*
sf_new(f_as_fragp __f) {
	_64_u more = 0;
	struct f_as_subfrag *sf;
	if (__f->sf_end != NULL) {
		endsf(__f);
	}
	sf = (struct f_as_subfrag*)turn_al(sizeof(struct f_as_subfrag));
	if (!__f->sf_top)
		__f->sf_top = sf;

	if (__f->sf_end != NULL)
		__f->sf_end->next = sf;
	__f->sf_end = sf;
	__f->sf_n++;
	sf->off = __f->offset;
	sf->size = 0;
	sf->end = sf->off;
	return sf;
}

struct f_as_subfrag*
f_as_sfnew(f_as_fragp __f) {
	struct f_as_subfrag *sf;
	sf = sf_new(__f);
	sf->t = f_as_tang_new();
	return NULL;
}

// fragment number
_int_u fr_nr = 0;
_int_u static frt_page_c = 0;
struct frag* ff_as_fnew(void) {
	if (curfrag != NULL) {
		if (!(curfrag->flags&FR_DONE)) {
			printf("warning previous fragment not done.\n");
		}
	}
	struct frag *f;
	_int_u fn;
	struct frag **page;
	_int_u pg, pg_off;
	fn = fr_nr++;
	pg = fn>>FRT_PAGE_SHFT;
	pg_off = fn&FRT_PAGE_MASK;

	if (!fr_tbl) {
		fr_tbl = (struct frag**)turn_al(sizeof(struct frag*));
		frt_page_c++;
	} else {
		if (pg>= frt_page_c) {
			fr_tbl = (struct frag**)turn_ral(fr_tbl, (++frt_page_c)*sizeof(struct frag*));
		} else
			goto _sk;
	}

	*(fr_tbl+pg) = (struct frag*)turn_al(FRT_PAGE_SIZE*sizeof(struct frag));
_sk:
	page = fr_tbl+pg;
	f = (*page)+pg_off;

	if (!fr_head)
		fr_head = f;
	if (curfrag != NULL)
		curfrag->next = f;
	curfrag = f;
	f->f = fn;
	f->next = NULL;
	f->p = (void**)turn_al(sizeof(void*));
	f->page_c = 1;
	*f->p = turn_al(PAGE_SIZE);
	f->size = 0;
	f->offset = 0;
	f->flags = 0x00;
	f->sf_n = 0;
	f->sf_top = NULL;
	f->sf_end = NULL;
	f_ass.topmost = f;
	return f;
}

# include "../string.h"
void ff_as_fwrite(struct frag *__f, _32_u __offset, void *__buf, _int_u __size) {
	if ((__offset+__size)>(__f->page_c<<PAGE_SHFT)) {
		ffly_fdrain(_ffly_out);
		while(1) {
//			printf("error.\n");
		}
	}
	_int_u pg, pg_off;
	pg = __offset>>PAGE_SHFT;
	pg_off = __offset&PAGE_MASK;

	_8_u *src = (_8_u*)__buf;
	_8_u **pa = __f->p+pg;
	if (pg_off>0) {
		_int_u rv = (PAGE_MASK^pg_off)+1;
		_8_u *d = (*(pa))+pg_off;
		if (__size>rv) {
			mem_cpy(d, src, rv);
			src+=rv;
			pa++;
		} else {
			mem_cpy(d, src, __size);
			return;
		}
	}

	_8_u **pe = __f->p+((__offset+__size)>>PAGE_SHFT);
	while(pa<pe) {
		mem_cpy(*pa, src, PAGE_SIZE);
		src+=PAGE_SIZE;
		pa++;
	}

	_int_u left = __size-(src-(_8_u*)__buf);
	if (left>0) {
		mem_cpy(*pa, src, left);
	}
}

void ff_as_plant(struct frag *__f, void *__buf, _int_u __size) {
	fgrowb(__f, __size);
	_32_u dst;
	dst = __f->offset;
	ff_as_fwrite(__f, dst, __buf, __size);
	__f->offset+=__size;
}

void tun_fread(struct frag *__f, _32_u __offset, void *__buf, _int_u __size) {
	_int_u pg, pg_off;
	pg = __offset>>PAGE_SHFT;
	pg_off = __offset&PAGE_MASK;

	_8_u *dst = (_8_u*)__buf;
	_8_u **pa = __f->p+pg;
	if (pg_off>0) {
		_int_u rv = (PAGE_MASK^pg_off)+1;
		_8_u *s = (*(pa))+pg_off;
		if (__size>rv) {
			mem_cpy(dst, s, rv);
			dst+=rv;
			pa++;
		} else {
			mem_cpy(dst, s, __size);
			return;
		}
	}

	_8_u **pe = __f->p+((__offset+__size)>>PAGE_SHFT);
	while(pa<pe) {
		mem_cpy(dst, *pa, PAGE_SIZE);
		dst+=PAGE_SIZE;
		pa++;
	}

	_int_u left = __size-(dst-(_8_u*)__buf);
	if (left>0) {
		mem_cpy(dst, *pa, left);
	}
}

void ff_as_fgrow(struct frag *__f, _32_u __size) {
/*
	fragments must be aligned by wedge size
*/
	_32_u pgc = (__size+PAGE_MASK)>>PAGE_SHFT;
	__f->size = __size;
	if (pgc>__f->page_c) {
		__f->p = (void**)turn_ral(__f->p, pgc*sizeof(void*));
		printf("new frag pages.\n");

		// allocate pages
		void **p = __f->p+__f->page_c;
		void **e = __f->p+pgc;
		__f->page_c = pgc;
		while(p != e)
			*(p++) = turn_al(PAGE_SIZE);
	}
}

void static
check_frag(struct frag *__f) {
//	printf("offset: %u, fit: %u.\n ", __f->offset, __f->fitsize);
	_32_u fit = (__f->offset+PAGE_MASK)>>PAGE_SHFT;
	if (fit != __f->page_c) {
		printf("fragment misfitted.\n");
		ff_as_fgrow(__f, __f->offset);
	}
}

static _32_u fr_adr = 0;
#define tbnew (b++)
// we are done with this fragment and are moving onto a new one
void ff_as_fdone(void) {
	struct f_as_frag *__f = f_ass.topmost;
	if (!__f->sf_end) return;
	endsf(__f);
	check_frag(__f);
	__f->flags |= FR_DONE;
	__f->sf_end->next = NULL;
	struct f_as_subfrag *sf;
	sf = __f->sf_top;
	printf("-----> PAGECOUNT: %u.\n", __f->page_c);
	while(sf != NULL) {
		F_AST_BSS(sf->t->bits, F_asts_OKAY);
		_8_u **pg;
		_32_u pg_off;
		_int_u totsz;
		totsz = sf->end-sf->off;
		struct f_as_tb *tb;
		_int_u ntb;

		struct f_as_tb *b;
		_32_u off;
		off = sf->off&PAGE_MASK;
		pg = (_8_u**)(__f->p+(off>>PAGE_SHFT));
		_int_u pg_ei, left;
		_8_u **pg_end = pg+(pg_ei = (sf->end>>PAGE_SHFT));

		ntb = (totsz+PAGE_MASK+(off&PAGE_MASK))>>PAGE_SHFT;
	
		printf("NTB: %u, totsz: %u, off: %u, end: %u, %p = %p\n", ntb, totsz, off, sf->end, pg, pg_end);
		tb = (struct f_as_tb*)turn_al(ntb*sizeof(struct f_as_tb));
		b = tb;	

		if (off>0) {
			b->w = (*pg)+off;
			b->size = (PAGE_MASK^off)+1;
			off = 0;
			pg++;
			tbnew;
			goto _bk;
		}


		if (totsz<=PAGE_SIZE) {
			b->w = *pg;
			b->size = totsz;
			tbnew;
			goto _not;
		}
	_bk:
		if (pg<pg_end) {
			printf("------> %p, %u.\n", *pg, tb-b);
			b->w = *pg;
			b->size = PAGE_SIZE;
			pg++;
			tbnew;
			goto _bk;
		}

		left = sf->end&PAGE_MASK;
		if (left>0) {
			printf("leftover.\n");
			b->w = *pg;
			b->size = left;
			tbnew;
		}
	_not:
		sf->b.b = tb;
		sf->b.n = ntb;
		sf->b.next = sf->t->b;
		sf->t->b = &sf->b;

		sf = sf->next;
	}

	struct frag *f;
	f = ff_as_fnew();
	sf = sf_new(f);
	sf->t = __f->sf_end->t;
}

struct frag *ff_as_fbn(_int_u __n) {
	if (__n>=fr_nr) {
		printf("what the fuck are you trying to do!\n");
		return NULL;
	}
	_int_u pg, pg_off;

	pg = __n>>FRT_PAGE_SHFT;
	pg_off = __n&FRT_PAGE_MASK;
	return (*(fr_tbl+pg))+pg_off;
}

# include "../tools.h"
extern void(*ff_as_fixins)(struct fix_s*);

void ff_as_foutput(void) {
	struct frag *f = fr_head;
//	_32_u page, pg_c;
//	void *p;
//	_16_u left;

	while(f != NULL) {
		printf("outputing frag-%u, size: %u\n", f->f, f->size);
/*	
		f->dst = f_ass.out.offset;
		if (!f->size)
			goto _skf;

		page = 0;
		pg_c = f->size>>PAGE_SHFT;
		while(page != pg_c) {
			printf("page %u out.\n", page);
			p = *(f->p+page);
			ffly_hexdump(p, PAGE_SIZE);
			ffly_chrdump(p, PAGE_SIZE);
			F_as_write(p, PAGE_SIZE);
			page++;
		}
		left = f->size-(pg_c*PAGE_SIZE);
		printf("--> %d\n", left);
		if (left>0) {
			printf("leftover: %u\n", left);
			// remove if statement???????????
			if (!(p = *(f->p+page)) || page>=f->page_c) {

			} else {
				ffly_hexdump(p, left);
				ffly_chrdump(p, left);
				F_as_write(p, left);
			}
		}

		offset+=f->size;
	_skf:
		{
		_int_u x;
		x = f->bs+f->k;
		if (x>0) {
			printf("-============-. %u - %u - %u\n", f->f, x, f->size);
			ffly_hexdump(f->data, x);
			F_as_write(f->data, x);
			offset+=x;
		}
		}
		*/

		f = f->next;
	}
}
