rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -Ddebug -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd as
ffly_objs="$ffly_objs pic.o turn.o m_frag.o blf.o urek.o malk.o opcodes/pic.o output/pic.o output/resin.o remf.o tang.o mp.o input.o proc_text.o local.o symbol.o stt.o output.o opcodes/resin_tbl.o opcodes/amd64_tbl.o output/amd64.o hash.o alloca.o parser.o exp.o frag.o"
gcc $cc_flags -c -o opcodes/pic.o opcodes/pic.c
gcc $cc_flags -c -o opcodes/amd64_tbl.o opcodes/amd64_tbl.c
gcc $cc_flags -c -o opcodes/resin_tbl.o opcodes/resin_tbl.c
gcc $cc_flags -c -o output/pic.o output/pic.c
gcc $cc_flags -c -o output/resin.o output/resin.c
gcc $cc_flags -c -o output/amd64.o output/amd64.c
gcc $cc_flags -c stt.c
gcc $cc_flags -c frag.c
gcc $cc_flags -c symbol.c
gcc $cc_flags -c alloca.c
gcc $cc_flags -c proc_text.c
gcc $cc_flags -c parser.c
gcc $cc_flags -c exp.c
gcc $cc_flags -c turn.c
gcc $cc_flags -c local.c
gcc $cc_flags -c input.c
gcc $cc_flags -c m_frag.c
gcc $cc_flags -c mp.c
gcc $cc_flags -c tang.c
gcc $cc_flags -c remf.c
gcc $cc_flags -c blf.c 
gcc $cc_flags -c urek.c
gcc $cc_flags -c pic.c
#gcc $cc_flags -c elf.c
gcc $cc_flags -c hash.c
gcc $cc_flags -c -o output.o output.c
gcc $cc_flags -c malk.c
gcc $cc_flags -o tas main.c $ffly_objs -nostdlib
