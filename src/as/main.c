# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "as.h"
# include "../elf.h"
# include "../string.h"
# include "../io.h"
# include "../linux/stat.h"
# include "../m_alloc.h"
# include "../remf.h"
# include "../exec.h"
# include "../opt.h"
#include "../strf.h"
# include "../depart.h"
#include "urek.h"
#include "malk.h"
#include "turn.h"
int static out;
int static in;

void static _read(void *__ctx, void *__buf, _int_u __size) {
	pread(in, __buf, __size, turn_strc.in.offset);
}

void static _pwrite(void *__ctx, void *__buf, _int_u __size, _64_u __offset) {
	pwrite(out, __buf, __size, __offset);
}

_int_u static nby = 0;
void static _write(void *__ctx, void *__buf, _int_u __size) {
	write(out, __buf, __size);
	/*_int_u i;
	i = 0;
	for(;i != __size;i++) {
		printf("%x.\n", ((_8_u*)__buf)[i]);
	}
	*/
	nby+=__size;
}

void static _vwrite(void *__ctx, struct f_iov *__v, _int_u __n) {
	struct f_iov *e = __v+__n;
	while(__v != e) {
		_write(__ctx, __v->p, __v->size);
		__v++;
	}
}

struct bbs_ops ops = {
	_read,
	_write,
	_pwrite
};
void malk_blf(void);

struct y_opt opts[] = {
	{
		.str = "i",
		.len = 1,
		.bits = 0
	},
	{
		.str = "o",
		.len = 1,
		.bits = 0
	},
	{
		.str = "g",
		.len = 1,
		.bits = 0
	},
	{
		.str = "p",
		.len = 1,
		.bits = 0
	},
	{
		.str = "f",
		.len = 1,
		.bits = 0
	}
};

#define _ 0xff
_8_u static exd[0x100] = {
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  0,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
   	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
    _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};

static struct m_macval vals[24];
static _int_u macv = 0;

_64_u static exd_func(char *__ln, void *__v) {
	char buf[64];
	_int_u i;
	char c;
	i = 1;
	*buf = *__ln;
	c = *++__ln;
	while(c>='a'&&c<='z') {
		buf[i] = c;
		c = *(__ln+i);
		i++;
	}
	buf[i] = '\0';

	if (!str_cmp(buf, "define")) {
		_int_u idlen;
		__ln+=i;
		i = 1;
		*buf = *__ln;
		c = *++__ln;
		while((c>='A'&&c<='Z')||c=='_'||(c>='0'&&c<='9')) {
			buf[i] = c;
			c = *(__ln+i);
			i++;
		}
		idlen = i;
		printf("MACRO.\n");
		tun_hash_put(&m_strc.mac, buf, idlen, __v);
	}
}

static struct y_opt_comm optc;
static _64_u grounder = -1;
static _64_u processor = -1;
static _64_u format = -1;

#define _64us_(__x,__y) (((_64_u)__x)<<__y)
#define imstr(__a,__b,__c,__d,__e,__f,__g,__h)\
	(_64us_(__a,0)|_64us_(__b,8)|_64us_(__c,16)|_64us_(__d,24)|\
	_64us_(__e,32)|_64us_(__f,40)|_64us_(__g,48)|_64us_(__h,56))
#define _ 0

#define GDR_urek			imstr('u','r','e','k',_,_,_,_)
#define GDR_malk			imstr('m','a','l','k',_,_,_,_)

#define PROC_pic18		imstr('p','i','c','1','8',_,_,_)
#define PROC_pic18em	imstr('p','i','c','1','8','e','m',_)
#define PROC_resin		imstr('r','e','s','i','n',_,_,_)
#define PROC_amd64		imstr('a','m','d','6','4',_,_,_)

#define FMT_BLF				imstr('b','l','f',_,_,_,_,_)

_64_u get_id(char const *__str){
	_64_u id = 0;
	_64_u c;
	_64_u idx = 0;
	while((c = *__str) != '\0'){
		id |= c<<idx;
		__str++;
		idx+=8;
	}
	printf("%s.\n",&id);
	return id;
}
void static parse_options(void){
	grounder = get_id(opts[OPT_GROUNDER].val);
	format = get_id(opts[OPT_FORMAT].val);
	processor = get_id(opts[OPT_PROCESSOR].val);
}

void m_pic_dis(_8_u*, _int_u);
void tun_eval_opts(char *__buf, struct tun_opt *__opt, char const *__argv[], _int_u __c);
_err_t main(int __argc, char const *__argv[]) {
	char buf[4096];
	
	y_opts_prep(&optc,opts,5);
	y_opts(&optc,__argv+1,__argc-1);

	if(!opts[OPT_INPUT].val ||
		!opts[OPT_OUTPUT].val ||
		!opts[OPT_GROUNDER].val ||
		!opts[OPT_PROCESSOR].val ||
		!opts[OPT_FORMAT].val) {
		printf("missing input/output files, %s, %s, %s, %s, %s\n",
			opts[OPT_INPUT].val,opts[OPT_OUTPUT].val,opts[OPT_GROUNDER].val,opts[OPT_PROCESSOR].val,opts[OPT_FORMAT].val
		);
		return -1;
	}
	parse_options();
	printf("input: %s, output: %s\n",opts[OPT_INPUT].val,opts[OPT_OUTPUT].val);
	in = open(opts[OPT_INPUT].val, O_RDONLY, 0);
	out = open(opts[OPT_OUTPUT].val, O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	if (in == -1 || out == -1) {
		printf("file for ether %s or %s does not exist or cant be accessed!\n",opts[OPT_INPUT].val,opts[OPT_OUTPUT].val);
		return;
	}
	void(*asmbl)(void);	
	void(*final)(void) = NULL;

	if(grounder == GDR_urek){
		urek_init();
		if(processor == PROC_resin){
			urek_resin();
		}else if(processor == PROC_amd64){
			urek_amd64();
		}
		
		asmbl = urek_text_asmbl; 
		final = urek_final;
		urek_remf();
	}else if(grounder == GDR_malk) {
		printf("selecting malk.\n");
		m_strc.exd = exd;
		m_strc.exd_func[0] = exd_func;
		if(format == FMT_BLF)
			malk_blf();
		_8_s is_pic = -1;
		if(processor == PROC_pic18){
			m_strc.opn = pic18_real;
			m_strc.mech = m_strc.mechtab[M_MECH_PIC];
			is_pic = 0;
		}
		else if(processor == PROC_pic18em){
			m_strc.opn = pic18_em;
			m_strc.mech = m_strc.mechtab[M_MECH_PICEM];
			is_pic = 0;
		}
		if (!is_pic) {
			asmbl = mpic_asmbl;
			final = mpic_final;
			mpic_init();
		}
	}else{
		printf("unknown, %lx nither %lx or %lx\n",grounder,GDR_urek,GDR_malk);
		return -1;
	}

	if (turn_strc.out.offset>0) {
		printf("offset has been prematurely set, adjusting to %u\n", turn_strc.out.offset);
		lseek(out, turn_strc.out.offset, SEEK_SET);
	}

	struct stat st;
	fstat(in, &st);

	turn_strc.in.limit = st.st_size;
	turn_strc.in.offset = 0;
	turn_strc.ops = ops;
	asmbl();


	final();

/*
	_8_u *tmp = m_alloc(nby);
	pread(out, tmp, nby, 0);
	m_pic_dis(tmp, nby);
	m_free(tmp);
*/
	close(in);
	close(out);
}
