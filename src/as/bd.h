# ifndef __f__as__bd__h
# define __f__as__bd__h
# include "../y_int.h"
// binaric depiction

#define F_BD_S8 0x00
#define F_BD_S16 0x01
#define F_BD_S32 0x02
#define F_BD_S64 0x03
#define F_BD_SM 0x03

#define _f_bd_mov 0x00
#define _f_bd_jmp 0x01
#define _f_bd_add 0x02
#define _f_bd_sub 0x03
#define _f_bd_call 0x04
#define _f_bd_ret 0x05
#define _f_bd_and 0x06
#define _f_bd_lea 0x07


#define F_BD_NBLOB 8
#define F_BD_NULL (~(_64_u)0)
#define F_BD_TRK_STT 0x00
#define F_BD_TRK_SYT 0x01
typedef _64_u _f_bd_addr;

struct f_bd_blob_desc {
	_int_u size;
};
extern struct f_bd_blob_desc f_bd_blob_dsc[F_BD_NBLOB];
struct f_bd_blob {
	_8_u op;
	_8_u flags;
};

struct f_bd_opd {
	_8_u type;
	_64_u val;
};

struct f_bd_hdr {
	_f_bd_addr start, size;
	_32_u n_blobs;
	// symbol table
	_f_bd_addr syt;
	// string table?
	_f_bd_addr stt;
};

struct f_bd_trek {
	_32_u n;
	_64_u start, size;
};

struct f_bd_sym {
	_64_u name, len;
	_8_u flags;
};

# endif /*__f__as__bd__h*/
