#include "as.h"
#include "malk.h"
#include "opcodes/pic.h"
#include "../string.h"
#include "../io.h"
#include "../strf.h"
#include "../assert.h"
#include "../m_alloc.h"
struct reg {
	char const *name;
	_int_u nme_len;
};
void m_flush(void);
#define N_INS 1
struct m_inset inset[N_INS] = {
	{pic16f18426, PIC16F18426_OPS}
};
#define prylk(__id, __py)\
	(__py)->next = m_press[__id];\
	m_press[__id] = __py;
struct m_pry *m_press[8] = {NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL};
#define NRGS 2
struct reg regtab[NRGS] = {
	{"ra",2},
	{"rb",2}
};
void static sect_new(char const *__name, _int_u __len);
#define _mac 1
void m_pic_load(void);
void mpic_init(void) {
	malk_init();
	inmap['#'] = _mac; 
	tun_hash_init(&m_strc.optab);
	tun_hash_init(&m_strc.regtab);
	tun_hash_init(&m_strc.mac);
	struct m_inset *is = inset;
	_int_u i;
	i = 0;
	for(;i != is->nele;i++) {
		struct m_op *o = is->tab+i;
		tun_hash_put(&m_strc.optab, o->name, o->nme_len, o);
		printf("placing %s.\n", o->name);
	}
	i = 0;
	for(;i != NRGS;i++) {
		struct reg *r = regtab+i;
		tun_hash_put(&m_strc.regtab, r->name, r->nme_len, r);
	}
	m_pic_load();
	m_frag_new();
	mem_set(m_strc.sytab, 0, sizeof(struct m_smdata*)*24);
	sect_new("text",4);
}

void malk_deinit(void) {
}
#define _precent 0x00
#define _dollar 0x01
#define _num 0x02
#define _ident 0x04
#define _hcident 0x04
#define _expr (7<<3)
#define _plus (1<<3)
#define _shl (2<<3)
#define _shr (3<<3)
#define _or (4<<3)
#define _and (5<<3)
#define _comp (6<<3)
#define _dot (1<<6)
#define _mod (3<<7)
//jk for just kidding
#define _mod_j (1<<7)
#define _mod_k (2<<7)
#define _comp0 (1<<9)
#define _neg	(1<<10)
#define _ 0xff
_16_u static asmkct[0x100] = {
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _dollar,  _precent,  _and,  _comp,  _,  _,  _,  _plus,  _,  _neg,  _dot,  _,
	 _num,  _num,  _num,  _num,  _num,  _num,  _num,  _num,  _num,  _num,  _,  _,  _shl,  _,  _shr,  _,
	_,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,
	_hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _hcident,  _comp0,  _,  _,  _,  _ident,
	_,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident|_mod_j,  _ident|_mod_k,  _ident,  _ident,  _ident,  _ident,
	_ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _ident,  _,  _or,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};
_16_u static dirkct[0x100] = {
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_num,  _num,  _num,  _num,  _num,  _num,  _num,  _num,  _num,  _num,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};
#include "../lib.h"
#include "../strf.h"
static _int_s n_opands;
static _64_u mod;
static _64_u mods;
struct m_symb static*
eval(_16_u *__tbl, char *__start, char *__end, char **__bp) {
	char *bp = *__bp;
	struct m_symb *yum;
	yum = NULL;
	char c;
	mod = 0x00;
	mods = 0x00;
	if (__start == __end)
		return NULL;
	_64_u mku;
	struct m_symb *sb;
	_16_u kc;//keychar
	char *p = __start;
	while(p != __end) {
		while(*p == ' ')p++;
		kc = __tbl[*p];
		/*
		
			MODS:

			mov j[$0
		
			i dont know, this needs to be FIXME'd

			idents like 'j0' wont work
		*/
		if (kc == _num) {
		if ((__tbl[p[1]]&_comp0)>0) {
			mods |= ((*p-'0')+1)<<8;
			mod |= 0x3<<2;
			p+=2;
			goto _ssk;
		}
		}
		if ((kc&_mod)>0) {
			_16_u k;
			if (__tbl[p[1]] == _num) {
				mods |= ((p[1]-'0')+1)<<8;
				mod |= 0x3<<2;
				p++;
			}
			if (((k = __tbl[p[1]])&_comp0)>0) {
				mods |= kc>>7;
				mod	|= 0x3;
				p+=2;
				if(p == __end) {
					n_opands--;					
					break;
				}

			}
		_ssk:
			if(p == __end) {
				n_opands--;					
				break;
			}
		}

		sb = turn_al(sizeof(struct m_symb));
		sb->next = yum;
		sb->type = 0;
		sb->neg = -1;
		sb->bits = 0;
		sb->_0.p = NULL;
		mku = 0;
		yum = sb;
	_j0:
		sb->exn = 0;
		sb->py.sb = sb;
		kc = __tbl[*p];
		if (kc == _dot) {
			p++;
			p++;
			*bp = *p;
			c = *++p;
			_int_u register len = 1;
			while(c>='0'&&c<='9') {
				*(bp+len) = c;
				c = *(p+len);
				len++;
			}
			p+=len-1;
			_int_u n;
			struct m_smdata *sm;
			n = _ffly_dsn(bp, len);
			if (!(sm = m_strc.sytab[n])) {
				sm = m_strc.sytab[n] = m_dummy_symb();
			}
			mku |= _m_smb_lbl;
			sb->type = _m_smb_lbl;
			sb->_sm0 = sm;
		}else
		if ((kc&(_num|_neg))>0) {
		_nm:
			if (kc == _neg) {
				sb->neg = 0;
				p++;
			}
			mku |= _m_smb_imm;
			if (p[1] == 'x') {
				p++;
				p++;
				prylk(_sb_hex, &sb->py);
			} else {
				prylk(_sb_imm, &sb->py);
			}
			sb->type = _m_smb_imm;
			sb->py.p = bp;
			*bp = *p;
			c = *++p;
			_int_u register len = 1;
			while((c>='0'&&c<='9')||(c>='A'&&c<='F')) {
				*(bp+len) = c;
				c = *(p+len);
				len++;
			}
			sb->py.len = len;
			p+=len-1;
			printf("NUMBER-%s.\n", bp);			
			bp+=len;

		} else if (kc == _ident) {
			printf("IDENT-%c.\n", *p);
			c = *p;
			*bp = c;
			c = *++p;
			sb->py.p = bp;
			_int_u register len = 1;
			while(!(~(_ident|_num|_mod)&__tbl[c])) {
				*(bp+len) = c;
				c = *(p+len);
				len++;
			}
			p+=len-1;
			if (c == '`') {
				c = *(p+1);
				if (c == 'h') {
					sb->bits = _syb_high;	
				}
				p+=2;
				c = *p;
			}
			if (c == '+') {
				sb->dis = 0;
			}else
				sb->dis = 1;
			bp+=len;

			sb->py.len = len;
			mku |= _m_smb_lbl;
			sb->type = _m_smb_lbl;
			prylk(_sb_llu, &sb->py);
			printf(">>>>>>>>>>>>>>>>>.'%c'\n",c);
		} else {
		c = *++p;
		switch(kc) {
			case _precent:{
				sb->type = _m_smb_reg;
				prylk(_sb_rlu, &sb->py);
				sb->py.p = bp;
				*bp = c;
				c = *++p;
				_int_u register len = 1;
				while(c>='a'&&c<='z') {
					*(bp+len) = c;
					c = *(p+len);
					len++;
				}
				sb->py.len = len;
				bp+=len;
				p+=len-1;
			
			}
			break;
			case _dollar: {
				kc = __tbl[c];
				goto _nm;
			}
			break;
			default:
				printf("what the fuck is this???.\n");
		}
		}
		printf("---###0--- '%c'\n", c);
		_8_u sv;
	_j1:
		if (c == '.') {
			c = *++p;
			_int_u register len = 0;
		_j4:
			*(bp+len) = c;
			len++;
			c = *(p+len);
			if (!(~(_ident|_num|_mod)&__tbl[c])) {
				goto _j4;
			}

			sb->_0.p = bp;
			sb->_0.len = len;
			p+=len;
			bp+=len;
		}else
		if ((sv = __tbl[c]) != 0xff) {
		if ((sv&_expr)>0) {
			struct sb_ext *ex;
			ex = yum->ex+yum->exn++;
			ex->sb = turn_al(sizeof(struct m_symb));
			sb = ex->sb;
			printf("ARTH operation.\n");
			_8_u op;	
			switch(sv) {
				case _plus:
					op = m_ADD;
				break;
				case _shl:
					op = m_SHL;
				break;
				case _shr:
					op = m_SHR;
				break;
				case _or:
					op = m_OR;
				break;
				case _and:
					op = m_AND;
				break;
				case _comp:
					op = m_COMP;
				break;
				default:
				while(1);
			}
			ex->op = op;
			p++;
			if (*p == 0x27) {
				ex->op |= m_COMP;
				c = *++p;
				goto _j1;
			} else
				goto _j0;
		}}
		if (c != ',') {
			break;
		} else
			n_opands++;
		yum->makeup = mku;
		p++;
	}
	n_opands++;
	*__bp = bp;
return yum;
}
void m_pic_fix(void);
static _64_u __off;
void static tether(struct m_frag *f) {
	_int_u i;
	i = 0;
	printf("tethers to fragment-%u.\n", f->sm->tn);
	for(;i != f->sm->tn;i++) {
		assert(i<64);
		struct m_tostrc *to = f->sm->_to+i;
		struct m_fat *ft = f->sm->to[i];
		struct m_reloc *rl;
		rl = m_strc.reloc_new();
		rl->from = ft;
		rl->to = f->sm->w;
		rl->value = ft->reloc;
		if (to->sel != NULL)
			rl->sel = to->sel->w;
		else
			rl->sel = NULL;
	}
}

void static emit_frag_real(void) {
	struct m_frag *f;
	f = m_strc.ft;
	_int_u i;
	__off = 0;
	//inwords 
	_64_u adr = 0;
	while(f != NULL) {
		i = 0;
		for(;i != f->sub_n;i++) {
			struct m_frag *_f;
			_f = f->sub[i];
			_int_u sz = (_f->pc-1)*MF_PG_SIZE;
			sz+=_f->over;
			struct m_fat *ft;
			ft = _f->ft;
			ft->size = sz;
			ft->off = __off;
			ft->adr = adr;
			ft->words = _f->words;
			ft->over = _f->underlap;
			ft->nov = _f->btc;
			printf("FRAG'0 over{%x:%u}.\n", _f->underlap, _f->btc);	
			_int_u j;
			j = 0;
			for(;j != _f->pc-1;j++) {
				_8_u *pg = _f->pages[i];
				turn_strc.ops.out.write(NULL, pg, MF_PG_SIZE);
			}
			if (_f->over>0) {
				turn_strc.ops.out.write(NULL, _f->part, _f->over);
			}

			turn_strc.out.offset+=sz;
			__off+=sz;
			adr+=_f->words;
		}
		/*
			is label???
			
			if a interfragment jump occurs
		*/
		if (f->sm != NULL) {
			tether(f);
		}
		f = f->next;
	}

}
#include "../tools.h"
void static emit_frag_em(void) {
	struct m_frag *f;
	f = m_strc.ft;
	_int_u i;
	__off = 0;
	//inwords 
	_64_u adr = 0;
	while(f != NULL) {
		i = 0;
		for(;i != f->sub_n;i++) {
			struct m_frag *_f;
			_f = f->sub[i];
			_int_u sz = (_f->pc-1)*MF_PG_SIZE;
			sz+=_f->over;
			struct m_fat *ft;
			ft = _f->ft;
			ft->size = sz;
			ft->off = __off;
			ft->adr = adr;
			ft->words = _f->words;
			ft->over = 0;;
			ft->nov = 0;

			_int_u j;
			j = 0;
			for(;j != _f->pc-1;j++) {
				_8_u *pg = _f->pages[i];
				turn_strc.ops.out.write(NULL, pg, MF_PG_SIZE);
			}
			if (_f->over>0) {
				turn_strc.ops.out.write(NULL, _f->part, _f->over);
			}


			turn_strc.out.offset+=sz;
			__off+=sz;
			adr+=_f->words;
		}
		/*
			is label???
			
			if a interfragment jump occurs
		*/
		if (f->sm != NULL) {
			tether(f);
		}
		f = f->next;
	}


}

void mpic_final(void) {
	if(m_strc.opn.mode == 0)
		emit_frag_real();
	else
		emit_frag_em();
	struct m_symb0 *sb;
	sb = ((struct m_symb0*)tun_hash_get(&m_strc.labels,"_start",6));
	if (sb != NULL) {
		struct m_smdata *sm;
		sm = sb->data;
		printf("entry point found.\n");
		m_strc.entry = sm;
	} else {
		printf("entry point left undesignated.\n");
	}
	m_strc.output(__off);
}

static struct m_iyield *i_y, *iy_top;
void static yield_init(void) {
	i_y->n = 0;
	i_y->next = NULL;
}
void static yield_new(void) {
	struct m_iyield *y;
	y = m_alloc(sizeof(struct m_iyield));
	i_y->next = y;
	i_y = y;
	yield_init();
}
static void inb_init(void);
void static
split(struct m_smdata *sm) {
	struct m_in *i;	
	i = i_y->in+i_y->n++;
	i->top = m_strc.ib_top;
	i->f = m_strc.fcur;
/*
	NOTE:
		m_frag_new creates empty frag and underlieing does not know
*/
	m_frag_new();
	m_strc.fate_init(m_strc.fcur->ft);
	m_strc.fcur->sm = sm;		
	struct m_wa *w;
	w = m_strc.wa_new();
	w->f = m_strc.fcur;
	sm->w = w;
	m_strc.ib_top = NULL;
	m_strc.ib_last = NULL;
	m_strc.ib_last = m_strc.ib_top = m_alloc(sizeof(struct m_instblock));
	inb_init();
	if (i_y->n>63) {
		yield_new();
	}
}

#define ING_SIZE 64
struct m_inst static *inpage;
static _int_u ing_n;
// alloc a new page of instruction structures
void static
ing_alloc(void) {
	inpage = turn_al(ING_SIZE*sizeof(struct m_inst));
	ing_n = 0;
}
void inb_init(void) {
	m_strc.ib_last->inst = inpage+ing_n;
	m_strc.ib_last->n = 0;
	m_strc.ib_last->next = NULL;
}
static void inb_alloc(void) {
	struct m_instblock *b;
	b = m_alloc(sizeof(struct m_instblock));
	m_strc.ib_last->next = b;
	m_strc.ib_last = b;
	inb_init();
}

static struct m_inst* in_alloc(void) {
	struct m_instblock *b;
	struct m_inst *i;
	if (ing_n>ING_SIZE-1) {
		printf("NEW instruction allocment.\n");
		ing_alloc();
		inb_alloc();

	}
	b = m_strc.ib_last;
	i = b->inst+b->n++;
	ing_n++;
	return i;
}

void sect_new(char const *__name, _int_u __len) {
                struct m_smdata *sm;

                m_sect = m_strc.sect_new();

                __smd = m_strc.smd0;
                sm = m_new_symb_non_exist(__name,__len,0);
                __smd = m_strc.smd;

                if (didsym_exist == -1) {
                    m_strc.section(sm);
                }
                m_sect->tiein = sm;
}

struct m_smdata static *last_smd = NULL;
#include "../tools.h"
#include "../lib.h"
#include "../strf.h"
void mpic_asmbl(void) {
	__smd = m_strc.smd;
	i_y = iy_top = m_alloc(sizeof(struct m_iyield));
	yield_init();
	ing_alloc();
	m_strc.ib_last = m_strc.ib_top = m_alloc(sizeof(struct m_instblock));
	inb_init();
	char buf[256*128];
	char *bp = buf;
	char *be = buf+256*128;
	char c;
	struct m_smdata *sm;
	char *p, *e;
	struct m_inst *in;
	turn_linep ln;
_again:
	ln = turn_nextline();
#ifdef debug
	printf("-----===== %u.\n", !ln);
#endif
	if (ln != NULL) {
		if (!ln->len)
			goto _end;
	}
	if (!ln) {
		struct m_in *i;
	_end:
		m_strc.last_smd = last_smd;
#ifdef debug
		printf("DUMP-----# %p.\n",m_strc.ib_top);
		ffly_chrdump(buf, bp-buf);
#endif
		i = i_y->in+i_y->n++;
		i->top = m_strc.ib_top;
		i->f = m_strc.fcur;
		m_pic(iy_top);
		struct m_frag *f = m_strc.ft;
		while(f != NULL) {
			printf("ROOT_FRAG, sub: %u, idx: %u.\n",f->sub_n,f->ft->idx);
			f = f->next;
		}
		return;
	}

	p = ln->p;
	e = p+ln->len;

	if (*p == ';') {
		goto _again;
	}
	if (*p == '.') {
		if (p[1] == 'L') {
			_int_u n;
			n = _ffly_dsn(p+2, ln->len-2);
			if (!(sm = m_strc.sytab[n])) {
				sm = m_dummy_symb();
				m_strc.sytab[n] = sm;
			}
			split(sm);
			goto _again;
		}
		_int_u register len = 0;
		char buf[256];
		
		len = m_read_ident(buf,&p,256);
		if (len == 5) {
			if (!mem_cmp(buf,"globl",5)) {
				len = m_read_ident(buf,&p,256);
				sm = m_new_symb_non_exist(buf,len,0);	
				sm->dom = m_strc.symdom[M_SYB_GBL];	
			}
		} else if (len == 6) {
			if (!mem_cmp(buf,"extern",6)) {
				len = m_read_ident(buf,&p,256);
				buf[len] = '\0';
				__smd = m_strc.smd0;
				sm = m_new_symb_non_exist(buf,len,0);
				sm->w = &sm->wd;
				sm->flags = m_strc.symflags[M_SYB_UD];
				//bluff this
				printf("##################EXTERN,'%s'\n",buf);
				__smd = m_strc.smd;
				m_strc.w_m++;
			}
		}else if (len == 7) {
			if (!mem_cmp(buf,"section",7)) {
				len = m_read_ident(buf,&p,256);
				buf[len] = '\0';
				sect_new(buf,len);
			}
		}else if (len == 4) {
			if (!mem_cmp(buf,"comm",4)) {
				len = m_read_ident(buf,&p,256);
				buf[len] = '\0';

				__smd = m_strc.smd0;
				sm = m_new_symb_non_exist(buf,len,0);
				__smd = m_strc.smd;

				len = snumext(buf,p+1);
				assert(len>0);
				p+=len;
				buf[len] = '\0';
				_int_u size = _ffly_dsn(buf,len);
				printf("COMM'size{%u},%w.\n",size,buf,len);
				m_strc.comm(sm,size);
			}
		}


		goto _again;
	}
	_8_u exd;
	if (ln->makeup&_mac) {
		_int_u register len = 0;
		char buf[256];
		c = *p;
		while(c != '#') {
			buf[len++] = c;
			c = *++p;
		}

		struct m_symb *s;
		s = eval(asmkct,buf, buf+len,&bp);
		
		m_strc.exd_func[exd](p+1, s);
		goto _again;
	}

	_int_u register len = 1;
	*bp = *p;
	c = *(p+1);

	while((c>='a'&&c<='z')||(c>='0'&&c<='9')||c == '_') {
		*(bp+len) = c;
		len++;
		c = *(p+len);
	}

	bp[len] = '\0';
#ifdef debug
	printf("#####- %s.\n", bp);
#endif
	if (c == ':' || c == '.') {
	/*
			got label
	*/
		_64_u x = 0;
		if (c == '.') {
			char numbuf[64];
			_int_u nlen;
			nlen = snumext(numbuf,p+len+1);
			x = _ffly_dsn(numbuf,nlen);
		}
		printf("new label, %u\n",x);
		struct m_smdata *sm;
		sm = m_new_symb_non_exist(bp,len,x);
		sm->dom = m_strc.symdom[M_SYB_LCL];
		split(sm);
		if (last_smd != NULL)
			last_smd->end = m_strc.fcur->ft;
		sm->start = m_strc.fcur->ft;
		last_smd = sm;
	
		goto _again;
	}

	in = in_alloc();
	in->_op = NULL;
	in->opands[0] = 0;
	in->opands[1] = 0;
	in->opands[2] = 0;
	in->opands[3] = 0;
	in->charac[0] = 0;
	in->charac[1] = 0;
	in->charac[2] = 0;
	in->charac[3] = 0;
	_8_u pass = 0x00;
	_8_u l, dis = len;
	if ((l = m_strc.leadmap[bp[dis-1]]) != 0xff) {
		pass |= 1;
		in->charac[0] = l;
		dis--;
		if ((l = m_strc.leadmap[((_16_u)bp[dis-1])+0x100]) != 0xff) {
			pass |= 1<<1;
			in->charac[1] = l;
			dis--;
			if ((l = m_strc.leadmap[((_16_u)bp[dis-1])+0x200]) != 0xff) {
				pass |= 1<<2;
				in->charac[2] = l;
				dis--;
			}
		}
	}
	printf("PASS: %x.\n", pass);
	in->p = bp;
	in->len = dis;
	in->chc = len-dis;
	bp+=dis;
	p+=len;

	assert(p<=e);
	n_opands = 0;
	in->yum = eval(asmkct, p, e, &bp);
	in->mods = mods;
	in->mod = mod;
	in->n_opands = n_opands;
	goto _again;	
}
