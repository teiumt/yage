#include "malk.h"
#include "../m_alloc.h"
#include "../string.h"
static _int_u cntr = 0;
struct m_frag static* newf(void) {
	printf("NEW FRAGMENT######.\n");
	struct m_frag *f;
	f = (struct m_frag*)m_alloc(sizeof(struct m_frag));
	f->pc = 1;
	f->btc = 64;
	f->underlap = 0;
	f->in = NULL;
	f->sm = NULL;
	f->attend = 0;
	f->over = 0;
	f->words = 0;
	f->id = cntr++;
	f->sub_n = 1;
	f->sub[0] = f;
	f->ft = m_strc.fist;
	m_strc.fist = m_alloc(m_strc.fatsize);
	mem_cpy(m_strc.fist, m_strc.fatinit, m_strc.fatsize);
	f->pages = (_8_u**)m_alloc(sizeof(_8_u*));
	return f;
}

struct m_frag* m_subfrag(struct m_frag *__f) {
	return __f->sub[__f->sub_n++] = newf();
}
struct m_frag* m_frag_new(void) {
	struct m_frag *f;
	f = newf();
	if (!m_strc.ft)
		m_strc.ft = f;
	f->next = NULL;
	if (m_strc.fcur != NULL)
		m_strc.fcur->next = f;
	m_strc.fcur = f;
	return f;
}

void m_frag_pgpush(struct m_frag *__f, _8_u *__ptr) {
	__f->pages[__f->pc-1] = __ptr;
	__f->pages = (_8_u**)m_realloc(__f->pages, (++__f->pc)*sizeof(_8_u*));
}
