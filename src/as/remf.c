# include "as.h"
# include "../string.h"
# include "../m_alloc.h"
# include "../remf.h"
# include "../io.h"
# include "../remf.h"
# include "../exec.h"
# include "../tools.h"
#define NTLR (F_REMF_RG_N)
#define _STT F_RG_STT
#define _SYT F_RG_SYT
#define _RLT F_RG_RLT

#define NPFS 0

#define _BUG_N 3
#define _BUG_SY_S 0
#define _BUG_SY_N 3
#define _AGG_SY_S 1
#define _AGG_SY_N F_REMF_AGG_SY_N
struct tfx;
struct reg {
	_32_u s_off, e_off;
	struct reg *next;
	struct f_as_region r;
	struct f_as_tang *s, *e;
};

struct symb {
	struct f_as_symbol s;
	struct symb *next;
};

struct tfx {
	_8_i locl;
	struct f_as_tfx tx;
	struct tfx *next;
};

struct rel {
	_int_u pm;
	struct f_as_reloc r;
	struct rel *next;
};

struct wa {
	struct f_as_wa w;
	struct wa *next;
};

struct agg {
	_8_u flags;
};

struct bug {
	union {
		struct {
			_int_u sy_n;
			struct symb *sy_top, *sy_end;
		};
	};
	struct agg agg;
};

struct pot {
	union {
		struct {
			_int_u tg_n;
			struct tfx *tg_top, *tg_end;
		};

		struct {
			_int_u w_n;
			struct wa *w_top, *w_end;
		};
	};
};

struct sf;
struct {
	struct wa *w_top;
	struct rel *rl_top;
	struct reg *r_top;
	struct sf *sf_top;
	struct symb *sm_top;
	struct tfx *top;
	_int_u tg_n;
	struct pot locl, globl;
	struct pot _locl, _globl;
	_32_u stt_off;
	_32_u n_n;
	_32_u sy_n;
	_32_u n_r;
	_32_u w_n;
	_32_u rl_n;
	struct remf_reg_hdr rh[8];
	struct f_as_fcom *com;
	struct bug bg[_BUG_N];
} com = {
	NULL,NULL,NULL,NULL,NULL,NULL,
	.tg_n = 0,
	.locl = {0, NULL, NULL},
	.globl = {0, NULL, NULL},
	._locl = {0, NULL, NULL},
	._globl = {0, NULL, NULL},
	.stt_off = 0,
	.n_n = 0,
	.sy_n = 0,
	.w_n = 0,
	.rl_n = 0,
	.n_r = NTLR,
	.bg = {
		{
			0, NULL, NULL, 0x00
		},
		{
			0, NULL, NULL, 0x00
		},
		{
			0, NULL, NULL, 0x00
		}
	}
};


// string fragment
struct sf {
	_8_u buf[64];
	_8_u len;
	_32_u of;
	struct sf *next;
};

_32_u static stt(_8_u *__ident, _int_u __len) {
	struct sf *f;
	f = turn_al(sizeof(struct sf));
	mem_cpy(f->buf, __ident, __len);
	_int_u l;
	l = (__len+F_REMF_SM)&~F_REMF_SM;
	f->len = __len;
	f->next = com.sf_top;
	com.sf_top = f;
	_32_u r;
	f->of = r = com.stt_off;
	com.stt_off+=l+F_REMF_SZ;
	return r;
}

void static
syt_store(void) {
//	reg.name = syt_dst;
//	reg.l = 4;
//	reg.type = FF_RG_SYT;
//	lseek(out, syt_dst, SEEK_SET);
//	write(out,"syt", 4);
//	write(out, &reg, remf_reghdrsz);
}

void static
syt_drop(void) {
	syt_dst = offset;
	offset+=4+remf_reghdrsz;
}

void static 
syt_gut(void) {
/*
	reg.offset = offset;
	symbolp cur = syt_head;
	while(cur != NULL) {
		struct remf_sy sy;
		sy.name = ff_as_stt(cur->p, cur->len);
		sy.type = cur->type;
		sy.reg = 0;
		sy.loc = 0;
		if (is_sylabel(cur)) {
			labelp la = (labelp)ff_as_hash_get(&env, cur->p, cur->len);
			sy.reg = la->reg->no;
			sy.f = la->f->f;
			sy.loc = la->f->adr+la->foffset;
			printf("??? %d, %d\n", la->f->adr, la->foffset);
		}

		printf("symbol out; loc: %d\n", sy.loc);
	
		sy.l = cur->len+1;
		fturn_oust((_8_u*)&sy, remf_sysz);
		symbolp bk = cur;
		cur = cur->next;

		m_free(bk->p);
		m_free(bk);
	}
	reg.size = offset-reg.offset;
*/
}

_64_u static
stt_drop(_64_u __off) {
	struct remf_reg_hdr *reg = com.rh+_STT;
	reg->offset = __off;
	reg->size = com.stt_off;
	reg->n = 0;	
	_8_u *buf = (_8_u*)m_alloc(com.stt_off+F_REMF_SM), *p, *b;
	// align
	/*
		allows us to get pointer to string by a OR operations insted of and ADD op
	*/
	b = (_8_u*)(((((_64_u)buf)+F_REMF_SM))&~F_REMF_SM);
	struct sf *f;
	f = com.sf_top;
	while(f != NULL) {
		p = b+f->of;
		struct remf_stent *e;
		e = (struct remf_stent*)p;
		e->l = f->len;
		p = (_8_u*)(((_64_u)p)|F_REMF_STE);
		mem_cpy(p, f->buf, f->len);
		f = f->next;
	}
	turn_oust(b, com.stt_off);

	ffly_chrdump(b, com.stt_off);
	m_free(buf);
	return __off+com.stt_off;
}

extern char const *globl;
extern char const *extrn;
relocatep extern rel;
hookp extern hok;


// not working not the time at the moment
void outsegs(void) {
	segmentp cur = curseg;
	while(cur != NULL) {
		cur->offset = offset;
		_int_u size = cur->fresh-cur->buf;
		if (size>0)
			turn_oust(cur->buf, size);
		cur = cur->next;
	}
}

f_as_relocp
_remf_reloc(void){
	struct rel *r;
	r = (struct rel*)m_alloc(sizeof(struct rel));
	r->next = com.rl_top;
	com.rl_top = r;
	r->pm = com.rl_n++;
	r->r.flags = 0x00;
	return &r->r;
}

void static
outfrags(void) {


}

void static
endreg(void) {
	com.r_top->e = f_ass.tg_end;
	com.r_top->e_off = f_ass.v.off-f_ass.v.fix;
}

# include "../strf.h"
#define IOF(__by)\
	of+=(__by)
void static
forge(void) {
	f_ass.tx->e = f_ass.tg_end;
	f_ass.tx->e_off = f_ass.v.off;
	endreg();
	struct remf_hdr hdr;
	*hdr.h.ident	= FH_MAG0;
	hdr.h.ident[1]	= FH_MAG1;
	hdr.h.ident[2]	= FH_MAG2;
	hdr.h.ident[3]	= FH_MAG3;
	hdr.h.ident[4]	= FH_MAG4;
	hdr.h.vers = FH_VERS;
	hdr.h.id = _f_remf;

	*hdr.ident		= FF_REMF_MAG0;
	hdr.ident[1]	= FF_REMF_MAG1;
	hdr.ident[2]	= FF_REMF_MAG2;
	hdr.ident[3]	= FF_REMF_MAG3;
	hdr.ident[4]	= '\0';
	hdr.routine = FF_REMF_NULL;

	hdr.format = _ffexec_bc;
	hdr.rn = 0;
	hdr.rg = FF_REMF_NULL;
	hdr.sg = FF_REMF_NULL;
	hdr.adr = f_ass.tg_end->adr+f_ass.tg_end->space;
	struct remf_reg_hdr *_rh; 

	struct remf_tt tt;

	_64_u of = turn_strc.out.offset;
	while(!of);

	_int_u sz;

	_int_u i;
	i = 0;
	tt.w = 0;
	if (com._locl.w_n>0){
		com._locl.w_end->next = NULL;
		struct wa *w;
		w = com._locl.w_top;
		struct remf_wa *_w = (struct remf_wa*)m_alloc(sz = (com._locl.w_n*sizeof(struct remf_wa))), *__wa;
		while(w != NULL) {
			__wa = _w+w->w.pm;
			__wa->adr = w->w.t->adr+w->w.adr;
			printf("======WA: %u #%u, %p\n", __wa->adr, w->w.pm, &w->w);
			w = w->next;
		}

		tt.w = of;

		turn_oust(_w, sz);
		IOF(sz);
	}
	tt.w_n = com._locl.w_n;
	tt.w_m = com._globl.w_n;
	
	i = 0;
	{
		com.locl.tg_end->next = NULL;
		struct tfx *t;
		t = com.locl.tg_top;
		i = 0;
		struct f_as_tang *rt;
		struct remf_tang *pry = NULL;
		struct remf_tang *tg = (struct remf_tang*)m_alloc(sz = (com.locl.tg_n*sizeof(struct remf_tang))), *c;
		while(t != NULL) {
			c = tg+t->tx.pm;	
			rt = t->tx.h;
			c->adr = rt->adr+t->tx.s_off;
			c->src = rt->adr+(t->tx.s_off-t->tx.fix);
			c->size = (t->tx.e->adr+t->tx.e_off)-(rt->adr+t->tx.s_off);
			printf("TANG-%u. type: %s, src: %u, size: %u\n", t->tx.pm, !t->locl?"LOCAL":"GLOBAL", c->src, c->size);
			t->locl = 0;
			pry = c;
			t = t->next;
		}

		tt.t = of;
		tt.t_n = com.locl.tg_n;
		tt.t_m = com.globl.tg_n;
		turn_oust(tg, sz);
		m_free(tg);
	
		IOF(sz);
		hdr.tt = of;
		turn_oust(&tt, sizeof(struct remf_tt));
		IOF(sizeof(struct remf_tt));
	}

	_8_i nn = 0;
	struct bug *b, *b_e;
	_int_u ag_of;
	_8_u *rd  = (_8_u*)m_alloc(sz = ((ag_of = (com.sy_n*sizeof(struct remf_sy)))+(_AGG_SY_N*sizeof(struct remf_agg))));
	struct remf_sy *_sy = (struct remf_sy*)rd, *_y;
	struct remf_agg *_a = (struct remf_agg*)(rd+ag_of);
	com.sy_n+=NPFS;
	/*
		0: symb
		1: symb
		2: symb
		0: agg
		1: agg
	*/

	i = 0;
	struct symb *sm;

	b = com.bg;
	b_e = com.bg;
	_rh = com.rh+_SYT;
	_rh->offset = of;
	_rh->size = sz;
	_rh->n = com.sy_n;
	_rh->_0 = ag_of;
	_rh->_1 = b->sy_n;
	/*
		emit non AGG/ED symbols
	*/
	goto _sk0;
_now:
	while(b<b_e) {
		_a->flags = b->agg.flags;
		_a->start = i;
		_a->n = b->sy_n;
		_a++;
	_sk0:
		printf("BUG: N: %u.\n", b->sy_n);
		sm = b->sy_top;
		while(sm != NULL) {
			_y = _sy+sm->s.pm+i+NPFS;
			char buf[128], bf0[128];
			mem_cpy(bf0, sm->s.p, sm->s.len);
			bf0[sm->s.len] = '\0';
			ffly_strf(buf, 128, "SYMBOL' NAME: %s", bf0);
			_y->flags = 0x00;
			if ((sm->s.flags&F_AS_SY_RS)>0)
				_y->flags |= F_SY_RS;
			_y->type = F_SY_WA;	
			_y->val = sm->s.w->pm;
			_y->type = F_SY_WA;
			_y->name = stt(sm->s.p, sm->s.len);
			printf("%s, VALUE: %u #%u\n", buf, _y->val, sm->s.pm);
			sm = sm->next;
		}
		i+=b->sy_n;
		b++;
	}

	/*
		non agged symbols have been placed now for the agged symbols
	*/
	if (!nn) {
		b = com.bg+_AGG_SY_S;
		b_e = b+_AGG_SY_N;
		nn = -1;
		goto _now;
	}

	printf("SYMBOL PLACMENT: %u, %u.\n", of, com.sy_n);
	turn_oust(rd, sz);
	IOF(sz);

	of = stt_drop(of);

	struct remf_rel *rl = (struct remf_rel*)m_alloc(sz = (com.rl_n*sizeof(struct remf_rel))), *_rl;
	struct rel *_r;
	_r = com.rl_top;
	_rh = com.rh+_RLT;
	_rh->offset = of;
	_rh->size = sz;
	_rh->n = com.rl_n;
	while(_r != NULL) {
		_rl = rl+_r->pm;
		_rl->dst = _r->r.d->pm;
		_rl->src = _r->r.s->pm;
		printf("RLSOURCE: %p\n", _r->r.s);
		_rl->flags = 0x00;
		if ((_r->r.flags&F_AS_R_UDD)>0) {
			_rl->flags = F_REMF_R_UDD;
		}
		switch(_r->r.type) {
			case F_AS_R_16:
				_rl->type = F_REMF_R_16;
			break;
			case F_AS_R_32:
				_rl->type = F_REMF_R_32;
			break;
		}
		printf("RELOC.\n");
		_r = _r->next;
	}

	turn_oust(rl, sz);
	IOF(sz);

	// true number
	_32_u trn;
	struct remf_reg_hdr *rg = (struct remf_reg_hdr*)m_alloc(sz = ((trn = com.n_r)*sizeof(struct remf_reg_hdr))), *_rg;
	struct reg *r;
	r = com.r_top;
	while(r != NULL) {
		_rg = rg+r->r.pm;
		printf("REGION' ID: %u, PM: %u.\n", r->r.id, r->r.pm);
		_rg->adr = r->s->adr+r->s_off;
		_rg->offset = r->s->dst+r->s_off;
		_rg->size = (r->e->dst+r->e_off)-(r->s->dst+r->s_off);
		printf("region: start: %u, end: %u, size: %u\n", r->s->adr, r->e->adr, _rg->size);
		_rg->n = 0;
		r = r->next;
	}

	mem_cpy(rg, com.rh, NTLR*sizeof(struct remf_reg_hdr));
	hdr.rn = trn;
	hdr.rg = of;
	hdr.rsts = sz;
	printf("##########-> %u, %u.\n", (rg+2)->n, NTLR);
	turn_oust(rg, sz);
	printf("REGION PLACMENT: %u, SIZE: %u.\n", of, sz);
	IOF(sz);

	if (f_ass.ety.sy != NULL) {
		hdr.flags = F_REMF_EPDEG;
		hdr.routine = f_ass.ety.sy->w->pm;
	}

	TURN_pwrite(&hdr, remf_hdrsz, 0);
}

f_as_tfxp static tfx_new(struct pot *__p) {
	printf("NEW TANG FIXTURE.\n");
	struct tfx *t = (struct tfx*)turn_al(sizeof(struct tfx));
	if (!__p->tg_top)
		__p->tg_top = t;
	
	if (__p->tg_end != NULL) {
		__p->tg_end->next = t;
	}

	__p->tg_end = t;
	t->tx.pm = __p->tg_n++;
	t->tx.fix = 0;
	return &t->tx;
}

f_as_symbolp static symb_new(struct bug *__b) {
	printf("NEW SYMBOL.\n");
	struct symb *s = (struct symb*)turn_al(sizeof(struct symb));
	if (!__b->sy_top)
		__b->sy_top = s;
	if (__b->sy_end != NULL)
		__b->sy_end->next = s;
	__b->sy_end = s;
	s->next = NULL;
	s->s.pm = __b->sy_n++;
	com.sy_n++;
	return &s->s;
}

f_as_regionp static reg_new(void) {
	printf("NEW REGION.\n");
	if (com.r_top != NULL) {
		endreg();
	}

	struct reg *r = (struct reg*)turn_al(sizeof(struct reg));
	r->next = com.r_top;
	com.r_top = r;
	r->s = f_ass.tg_end;
	r->s_off = f_ass.v.off-f_ass.v.fix;
	r->r.pm = com.n_r++;
	return &r->r;
}

f_as_wap static wa_new(struct pot *__p) {
	printf("NEW WA.\n");
	struct wa *w = (struct wa*)turn_al(sizeof(struct wa));
	if (!__p->w_top)
		__p->w_top = w;
	if (__p->w_end != NULL) {
		__p->w_end->next = w;
	}
	__p->w_end = w;
	w->w.adr = 0;
	w->w.pm = __p->w_n++;
	return &w->w;
}

void urek_remf(void) {
	f_as_tfxp tx;
	tx = tfx_new(&com.locl);
	tx->h = f_ass.tg_end;
	tx->s_off = 0;
	f_ass.tx = tx;
	f_ass.forge = forge;
	f_ass.offn.choice = f_ass.offn.remf;
	f_ass.tfx_new = (void*)tfx_new;
	f_ass.symb_new = symb_new;
	f_ass.reg_new = reg_new;
	f_ass.wa_new = wa_new; 
	f_ass.reloc = _remf_reloc;
	f_ass._locl = &com.locl;
	f_ass._globl = &com.globl;
	f_ass.w_locl = &com._locl;
	f_ass.w_globl = &com._globl;
	turn_strc.out.offset = sizeof(struct remf_hdr);
	com.com = &f_ass.com;
	_int_u i;
	i = 0;
	for(;i != _BUG_N;i++) {
		f_ass.com.p[i] = com.bg+i;
	}
}
