#include "turn.h"
# include "as.h"
# include "../io.h"
# include "../string.h"
# include "../tools.h"
/*
	input file for text
*/
static _8_u flowbkb[TURN_TXCNK_SIZE];
static _8_u *fbb = flowbkb;
static _32_u errorval;
static _8_u res;
/*
	dont reinduct fake data
*/
static _int_u correct = 0;
//next text chunk
turn_txcnkp turn_ntxcnk(void) {
	_64_s er = turn_strc.in.offset-turn_strc.in.limit;
#ifdef debug
	printf("~~~~# %d - %u.\n", er, fbb-flowbkb);
#endif
	if (er>=0 && (fbb-flowbkb) == er) {	
		printf("eather were at the end of the data or an error has occurd in the reading process.\n");
		return NULL;
	}

	turn_txcnkp cnk;
	cnk = (turn_txcnkp)turn_al(sizeof(struct turn_txcnk));
	/*
		NOTE:
		TODO:
		add huge page to store the data of many chunks?? or somthing???
	*/
	if (fbb>flowbkb) {
#ifdef debug
		printf("partial chunk read.\n");
#endif
		_int_u size = fbb-flowbkb;
		_int_u needed = TURN_TXCNK_SIZE-size;
		if (er<0) {
			if (needed>0) {
			/*
				NOTE:
					reading a small amount is bad so read an extra chunk to make up for the loss???? to compromise
			*/
			res = turn_read(fbb, needed);
		}
		} else {
			correct = needed;
			printf("no more to give, dishing out remaining.\n");
		}
		cnk->p = flowbkb;
		fbb = flowbkb;
	} else {	
#ifdef debug
		printf("whole chunk read.\n");
#endif
		cnk->p = turn_al(TURN_TXCNK_SIZE);
		res = turn_read(cnk->p, TURN_TXCNK_SIZE);
	}
#ifdef debug
	ffly_chrdump(cnk->p, TURN_TXCNK_SIZE);
#endif
	return cnk;
}
/*
	NOTE:
		double back slash means theres more to come line has not ended??

		find name more sutable for line??
*/
// if a line contain a certin char then let us know about it i.e make it obvious so thus not obscured
#define _ 0
_8_u inmap[0x100] = {
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};

#define SKU_NL 0x01
#define MORE 0x02
#define SKNNL 0x04 // skip next newline
static _8_u gflags = 0x00;
turn_linep turn_nextline(void) {
	if ((gflags&TURN_LLST)>0) {
		printf("-------------last flags.\n");
		return NULL;
	}
	turn_linep ln, ln_top = NULL, ln_end = NULL;
	turn_txcnkp cnk;
	_int_u c;
	_8_u cc, *p, *e;
	_int_u len;
	_8_u bits;
	_int_u left;
_nextline:
	ln = (turn_linep)turn_al(sizeof(struct turn_line));
	ln->p = (_8_u*)turn_al(TURN_LINEMAX);
	if (!ln_top)
		ln_top = ln;
	if (ln_end != NULL)
		ln_end->next = ln;
	ln_end = ln;
_getnext:
	ln->makeup = 0;
	ln->flags = 0x00;
	bits = 0x00;
	len = 0;
	c = 0;
	for(;c != TURN_TCWL;c++) {
		cnk = turn_ntxcnk();
#ifdef debug
		printf("reading chunk.\n");
		printf("---- %u, %u, %x.\n", turn_strc.in.offset, turn_strc.in.limit, cnk);
#endif
		if (!cnk) {
			goto _done0;
		}
#ifdef debug
		ffly_chrdump(cnk->p, TURN_TXCNK_SIZE);
#endif
		p = cnk->p;
		e = p+TURN_TXCNK_SIZE;
	_fnd:
		while(p != e) {
		_skw:
			cc = *p++;
			ln->makeup |= inmap[cc];
#ifdef debug
			printf("%c.\n", cc);
#endif
			if (cc == '\t')
				goto _skw;

			if (cc == ';'){
				bits |= SKU_NL;
				goto _skw;
			}
			if (cc == 0x5c) {
				bits |= MORE;
				/*
					note \n is apart of each line ie TURN_LINEMAX
				*/
				if (*p == '\n')
					p++;
				goto _done;
			}
			if (cc == '\n') {
				goto _done;
			}
			if (cc == '/') {
			_again:
				while(p != e) {
					if (*(p++) == '/') {
						goto _fnd;
					}
				}

				cnk = turn_ntxcnk();
				p = cnk->p;
				e = p+TURN_TXCNK_SIZE;
				goto _again;
			}

			if (bits&SKU_NL) {
				continue;
			}
			ln->p[len++] = cc;
		}
	}
_done:
	ln->p[len] = '\0';
#ifdef debug
	printf("LINE: '%s'.\n", ln->p);
#endif
	left = (e-p)-correct;
	if (left>0) {
#ifdef debug
		printf("copying leftover-%u chunk to buffer for reintroduction.\n", left);
		ffly_chrdump(p, left);
#endif
		mem_cpy(fbb, p, left);
		fbb+=left;
	}

	if (!len)
		goto _getnext;
_done0:
#ifdef debug
	printf("-----> line length: %u.\n", len);
	ffly_chrdump(ln->p, TURN_LINEMAX);
#endif
	ln->len = len;
	if ((bits&MORE)>0) {
		bits ^= MORE;
		goto _nextline;
	}
	ln_end->next = NULL;
	if (turn_strc.in.offset>=turn_strc.in.limit && fbb == flowbkb) {
		ln->flags |= TURN_LLST;
		gflags = TURN_LLST;
	}


	return ln_top;
}
