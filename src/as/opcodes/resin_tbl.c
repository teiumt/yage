# include "resin.h"
# include "../../types.h"
/*
	op = opcode+(oplen*size)
*/
static struct f_as_opbody body[] = {
	//exit
	{{_resin_op_exit, _resin_exit2r0, _resin_exit2r1}, 1, ous(0, 0, 0, 0), ot(_o_reg, 0, 0, 0), 0, {1, 2}, 1},
	{{_resin_exit1}, 1, ous(0, 0, 0, 0), ot(_o_imm8, 0, 0, 0), 0, {0, 0}, 1},
	//st
	{{_resin_op_stb, _resin_op_stw, _resin_op_std, _resin_op_stq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//ld
	{{_resin_op_ldb, _resin_op_ldw, _resin_op_ldd, _resin_op_ldq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//mov
	{{_resin_op_movb, _resin_op_movw, _resin_op_movd, _resin_op_movq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//rin
	{{_resin_op_rin}, 1, ous(1, sizeof(_f_addr_t), 0, 0), ot(_o_imm8, _o_reg64|_o_imm, 0, 0), 0, {0, 0}, 2},
	//div
	{{_resin_op_divb, _resin_op_divw, _resin_op_divd, _resin_op_divq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//mul
	{{_resin_op_mulb, _resin_op_mulw, _resin_op_muld, _resin_op_mulq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//sub
	{{_resin_op_subb, _resin_op_subw, _resin_op_subd, _resin_op_subq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//add
	{{_resin_op_addb, _resin_op_addw, _resin_op_addd, _resin_op_addq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//inc
	{{_resin_op_incb, _resin_op_incw, _resin_op_incd, _resin_op_incq}, 1, ous(0, 0, 0, 0), ot(_o_reg, 0, 0, 0), asto, {0, 0}, 1},
	//dec
	{{_resin_op_decb, _resin_op_decw, _resin_op_decd, _resin_op_decq}, 1, ous(0, 0, 0, 0), ot(_o_reg, 0, 0, 0), asto, {0, 0}, 1},
	//cmp
	{{_resin_op_cmpb, _resin_op_cmpw, _resin_op_cmpd, _resin_op_cmpq}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_reg, 0, 0), asto, {0, 0}, 2},
	//je
	{{_resin_op_je}, 1, ous(0, 0, 0, 0), ot(_o_label, 0, 0, 0), 0, {0, 0}, 1},
	//jne
	{{_resin_op_jne}, 1, ous(0, 0, 0, 0), ot(_o_label, 0, 0, 0), 0, {0, 0}, 1},
	//jg
	{{_resin_op_jg}, 1, ous(0, 0, 0, 0), ot(_o_label, 0, 0, 0), 0, {0, 0}, 1},
	//jl
	{{_resin_op_jl}, 1, ous(0, 0, 0, 0), ot(_o_label, 0, 0, 0), 0, {0, 0}, 1},
	//jmp
	{{_resin_op_jmp}, 1, ous(0, 0, 0, 0), ot(_o_label, 0, 0, 0), 0, {0, 0}, 1},
	//call
	{{_resin_op_call}, 1, ous(0, 0, 0, 0), ot(_o_label, 0, 0, 0), 0, {0, 0}, 1},
	//ret
	{{_resin_op_ret}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//as
	{{_resin_op_asb, _resin_op_asw, _resin_op_asd, _resin_op_asq,
		_resin_as1br0, _resin_as1wr0, _resin_as1dr0, _resin_as1qr0,
		_resin_as1br1, _resin_as1wr1, _resin_as1dr1, _resin_as1qr1}, 1, ous(0, 0, 0, 0), ot(_o_reg, _o_imm, 0, 0), asto, {4, 8}, 2},
	//out
	{{_resin_op_outb, _resin_op_outw, _resin_op_outd, _resin_op_outq,
		_resin_out1br0, _resin_out1wr0, _resin_out1dr0, _resin_out1qr0,
		_resin_out1br1, _resin_out1wr1, _resin_out1dr1, _resin_out1qr1}, 1, ous(0, 0, 0, 0), ot(_o_reg, 0, 0, 0), asto, {4, 8}, 1},
	//push
	{{_resin_push1br0, _resin_push1wr0, _resin_push1dr0, _resin_push1qr0,
		_resin_push1br1, _resin_push1wr1, _resin_push1dr1, _resin_push1qr1}, 1, ous(0, 0, 0, 0), ot(_o_reg, 0, 0, 0), asto, {0, 4}, 1},
	//pop
	{{_resin_pop1br0, _resin_pop1wr0, _resin_pop1dr0, _resin_pop1qr0,
		_resin_pop1br1, _resin_pop1wr1, _resin_pop1dr1, _resin_pop1qr1}, 1, ous(0, 0, 0, 0), ot(_o_reg, 0, 0, 0), asto, {0, 4}, 1},
	//r0r0
	{{_resin_r0r0}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r1r0
	{{_resin_r1r0}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r2r0
	{{_resin_r2r0}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r3r0
	{{_resin_r3r0}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r0r1
	{{_resin_r0r1}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r1r1
	{{_resin_r1r1}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r2r1
	{{_resin_r2r1}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0},
	//r3r1
	{{_resin_r3r1}, 1, ous(0, 0, 0, 0), ot(0, 0, 0, 0), 0, {0, 0}, 0}
};

struct f_as_optray const resin_optab[NOPS] = {
	{"exit", 4, 2, body},//exit has two bodies
	{"st", 2, 1, body+2},
	{"ld", 2, 1, body+3},
	{"mov", 3, 1, body+4},
	{"rin", 3, 1, body+5},
	{"div", 3, 1, body+6},
	{"mul", 3, 1, body+7},
	{"sub", 3, 1, body+8},
	{"add", 3, 1, body+9},
	{"inc", 3, 1, body+10},
	{"dec", 3, 1, body+11},
	{"cmp", 3, 1, body+12},
	{"je", 2, 1, body+13},
	{"jne", 3, 1, body+14},
	{"jg", 2, 1, body+15},
	{"jl", 2, 1, body+16},
	{"jmp", 3, 1, body+17},
	{"call", 4, 1, body+18},
	{"ret", 3, 1, body+19},
	{"as", 2, 1, body+20},
	{"out", 3, 1, body+21},
	{"push", 4, 1, body+22},
	{"pop", 3, 1, body+23},
	{"r0r0", 4, 1, body+24},
	{"r1r0", 4, 1, body+25},
	{"r2r0", 4, 1, body+26},
	{"r3r0", 4, 1, body+27},
	{"r0r1", 4, 1, body+28},
	{"r1r1", 4, 1, body+29},
	{"r2r1", 4, 1, body+30},
	{"r3r1", 4, 1, body+31},
};
