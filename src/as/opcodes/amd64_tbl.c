# include "amd64.h"
# include "../../types.h"
# include "../as.h"
static struct f_as_opbody body[] = {
	//nop-1
	{{0x90}, 1, 0, 0, 0, 0},
	//mov-4
	{{0xb0}, 1, 0, 0, ot(_o_reg8, _o_imm8, 0, 0), 2},
	{{0xb8}, 1, 0, 0, ot(_o_reg16|_o_reg32|_o_reg64, _o_imm16|_o_imm32|_o_imm64, 0, 0), 2},
	{{0x88}, 1, modrm, 0x0, ot(_o_reg8, _o_reg8, 0, 0), 2},
	{{0x89}, 1, modrm, 0x0, ot(_o_reg16|_o_reg32|_o_reg64, _o_reg16|_o_reg32|_o_reg64, 0, 0), 2},
	//xor-4
	{{0x80}, 1, modrm, 0x6, ot(_o_reg8, _o_imm8, 0, 0), 2},
	{{0x81}, 1, modrm, 0x6, ot(_o_reg16|_o_reg32, _o_imm16|_o_imm32, 0, 0), 2},
	{{0x30}, 1, modrm, 0x0, ot(_o_reg8, _o_reg8, 0, 0), 2},
	{{0x31}, 1, modrm, 0x0, ot(_o_reg16|_o_reg32|_o_reg64, _o_reg16|_o_reg32|_o_reg64, 0, 0), 2},
	//pop-1
	{{0x58}, 1, noprefix, 0, ot(_o_reg16|_o_reg32|_o_reg64, 0, 0, 0), 1},
	//push-2
	{{0x50}, 1, noprefix, 0, ot(_o_reg16|_o_reg32|_o_reg64, 0, 0, 0), 1},
	{{0x68}, 1, noprefix, 0, ot(_o_imm16|_o_imm32|_o_imm64, 0, 0, 0), 1},
	//syscall-1
	{{0x0f, 0x05}, 2, 0, 0, 0},
	//jmp-1
	{{0xeb}, 1, 0, 0, ot(_o_label, 0, 0, 0), 1, _d64},
	//call-1
	{{0xe8}, 1, osof16, 0, ot(_o_label, 0, 0, 0), 1, _d8|_d64},
	//ret-1
	{{0xc3}, 1, 0, 0, 0, 0}
};

static struct f_as_regbody regbody[] = {
	//rax,eax,ax,al
	{{8}, 0, 0, amd64_regtab},
	{{4}, 0, 0, amd64_regtab+1},
	{{2}, 0, 0, amd64_regtab+2},
	{{1}, 0, 0, amd64_regtab+3},
	//rbx,ebx,bx,bl
    {{9}, 3, 3, amd64_regtab+4},
    {{4}, 3, 3, amd64_regtab+5},
    {{2}, 3, 3, amd64_regtab+6},
    {{1}, 3, 3, amd64_regtab+7},
	//rcx,ecx,cx,cl
    {{8}, 1, 1, amd64_regtab+8},
    {{4}, 1, 1, amd64_regtab+9},
    {{2}, 1, 1, amd64_regtab+10},
    {{1}, 1, 1, amd64_regtab+11},
	//rdx,edx,dx,dl
    {{8}, 2, 2, amd64_regtab+12},
    {{4}, 2, 2, amd64_regtab+13},
    {{2}, 2, 2, amd64_regtab+14},
    {{1}, 2, 2, amd64_regtab+15},
	//rdi,edi,di,dil
    {{8}, 7, 8, amd64_regtab+16},
    {{4}, 7, 8, amd64_regtab+17},
    {{2}, 7, 8, amd64_regtab+18},
    {{1}, 7, 8, amd64_regtab+19},
	//rbp,rsp,
	{{8}, 5, 5, amd64_regtab+20},
	{{8}, 4, 4, amd64_regtab+21}
};


struct f_as_optray const amd64_optab[NOPS] = {
	{"nop", 3, 1, body},
	{"mov", 3, 4, body+1},
	{"xor", 3, 4, body+5},
	{"pop", 3, 1, body+9},
	{"push", 4, 2, body+10},
	{"syscall", 7, 1, body+12},
	{"jmp", 3, 1, body+13},
	{"call", 4, 1, body+14},
	{"ret", 3, 1, body+15}
};

struct f_as_reg const amd64_regtab[REGC] = {
	{"rax", 3, regbody}, {"eax", 3, regbody+1}, {"ax", 2, regbody+2}, {"al", 2, regbody+3},
	{"rbx", 3, regbody+4}, {"ebx", 3, regbody+5}, {"bx", 2, regbody+6}, {"bl", 2, regbody+7},
	{"rcx", 3, regbody+8}, {"ecx", 3, regbody+9}, {"cx", 2, regbody+10}, {"cl", 2, regbody+11},
	{"rdx", 3, regbody+12}, {"edx", 3, regbody+13}, {"dx", 2, regbody+14}, {"dl", 2, regbody+15},
	{"rdi", 3, regbody+16}, {"edi", 3, regbody+17}, {"di", 2, regbody+18}, {"dil", 3, regbody+19},
	{"rbp", 3, regbody+20}, {"rsp", 3, regbody+21}
};
