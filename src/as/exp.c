# include "as.h"
#include "../strf.h"
# include "../ffly_def.h"
# include "../mort/stdio.h"
#define isno(__c) ((__c >= '0' && __c <= '9') || __c == '-')
#define is_next(__p, __c) \
	(*(__p+1) == __c)

#define ADD 0x01
#define nextc(__p) (*(__p+1))
/*
	__ld = line displacment
	should rename??
		to line forward bias??
*/
f_as_blobp f_as_eval(turn_linep __ln, _8_u *__tbl, _int_u __ld) {
	turn_linep ln = __ln;
	char *p;
	f_as_chompp head = turn_al(sizeof(struct f_as_chomp));
	f_as_chompp cur = head;
	cur->flags = 0x0;
	char cc;
	char buf[128];
	char *bufp;
	_8_u bits = 0x00;

	p = ln->p+__ld;
	p[ln->len-__ld] = '\0';
	goto _sk;
_again:
	p = ln->p;
	p[ln->len] = '\0';
_sk:
	while(*p != '\0') {
		while(*p == ' ' || *p == '\t') p++;
		if (*p == '\0') break;
		_8_u c;
		c = __tbl[cc = *p];
		if (c != 0xff) {
			p++;
			switch(c) {
			/*
				local label
			*/
				case _f_ae_hash_1: {
					_int_u l;
					bufp = buf;
					cc = *p;
					while((cc >= '0' && cc <= '9') || (cc >= 'a' && cc <= 'z')) {
						*(bufp++) = cc;
						cc = *++p;
					}
					*bufp = '\0';
					cur->sort = CH_SY;
					cur->_0 = f_as_symb(buf, bufp-buf, f_ass.localsymbs);
					break;
				}
				case _f_ae_left_paren_1:
					cur->flags |= S_ADR;
				break;	
				case _f_ae_right_paren_1:
				break;
				case _f_ae_plus_01:
					bits = ADD;
				break;
				case _f_ae_percent_1: {
					bufp = buf;
					cc = *p;
					while((cc >= 'a' && cc <= 'z') || (cc >= '0' && cc <= '9')) {
						*(bufp++) = cc;
						cc = *++p;
					}
					f_as_regp reg;
					if (!(reg = f_ass.getreg((char const*)buf, bufp-buf))) {
						printf("register not recognized, %s\n", cur->_0);
						return;
					}
					cur->_0 = reg;
					cur->sort = CH_REG;
				}
				break;
			/*
				default label
			*/
				case _f_ae_dollar_1:
					cc = *p;
					if ((cc>='a'&&cc<='z')||cc == '_') {
						_int_u l;
						bufp = buf;
						cc = *p;
						while((cc>='a'&&cc<='z')||cc=='_'||(cc>='0'&&cc<='9')) {
							*(bufp++) = cc;
							cc = *++p;
						}
						*bufp = '\0';
						l = bufp-buf;
						cur->_0 = f_as_symb(buf, l, &f_ass.symbols);
						cur->sort = CH_SY;
					} else if (isno(nextc(p))) {
						goto _no;
					}
				break;
				case _f_ae_comma_01: {
					cur->next = turn_al(sizeof(struct f_as_chomp));
					cur = cur->next;
					cur->flags = 0x0;
				}
				break;
			}
		} else {
			if (isno(cc)) _no: {
				_int_u l;
				_8_u sign;
				if (bits&ADD) {
					cur->_0 += ff_as_read_no(p, &l, &sign);
					p+=l;
					bits ^= ADD;
				} else {
					cur->_0 = ff_as_read_no(p, &l, &sign);
					p+=l;
					cur->sort = CH_INT;
					if (sign)
						cur->flags |= SIGNED;
					cur->_1 = sizeof(_64_u);
				}
			} else if ((cc>='a'&&cc<='z')||cc == '_') {
				_int_u l;
				bufp = buf;
				while((cc >= 'a' && cc <= 'z') || cc == '_' || (cc >= '0' && c <= '9')) {
					*(bufp++) = cc;
					cc = *++p;
				}
				*bufp = '\0';
				l = bufp-buf;
				cur->_0 = turn_smemdup(buf, l+1);
				cur->sort = CH_UK;
				cur->_1 = l;
			}
		}
	}

	if (ln->next != NULL) {
		ln = ln->next;
		goto _again;
	}
	cur->next = NULL;

	f_as_blobp b = (f_as_blobp)turn_al(sizeof(struct f_as_blob));
	b->ch = head;
	return b;
}
