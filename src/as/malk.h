#ifndef __malk__h
#define __malk__h
#include "as.h"
#define _m_smb_imm 1
#define _m_smb_reg 2
#define _m_smb_lbl 4
#define _m_smb_uk 8
#define _m_imm7	 (1)
#define _m_imm8		(1<<1)
#define _m_imm16 	(1<<2)
#define _m_imm32	(1<<3)	
#define _m_imm64	(1<<4)
#define _m_imm (0x1f)
#define _m_reg8		(1<<5)
#define _m_reg16 	(1<<6)
#define _m_reg32	(1<<7)	
#define _m_reg64	(1<<8)
#define _m_reg (0xf0)
#define _sb_imm 0
#define _sb_llu 1
#define _sb_hex 2
#define _sb_rlu 3
#define _in_nocrunch 1
#define M_USERDATA _64_u userdata[16];

struct m_opbody {
	_16_u opcode;//real
	_16_u opcode0;//emu
	_16_u opands[4];
	_8_u charac[4];
	_int_u n_opands;
	_8_u opandpos[4];
	_8_u opandpos0[4];
	_16_u opandmask[4];
	_8_u opts;
	//primary bits
	_8_u pmb;
	void(*special)(void*);
	_64_u mask;
	_64_u mods;
	_64_u pos;
	_64_u bits;
};
#define _m_op_odd 1
struct m_op {
	char const *name;
	_int_u nme_len;
	struct m_opbody *bodies;
	_int_u n_bodies;
	_64_u bits;
};

struct m_inset {
	struct m_op *tab;
	_int_u nele;
};

struct m_symb;
struct m_inst;
struct m_pry {
    struct m_pry *next;
    char *p;
    _int_u len;

    union {
        struct m_symb *sb;
        struct m_inst *in;
    };
};
struct m_wa {
	M_USERDATA
	_64_u off, idx;
	struct m_fat *f;
};

struct m_fat;
struct m_symb0;
struct m_section;
#define M_SYB_UD 1
#define M_SYB_LCL 0
#define M_SYB_GBL 1
#define M_SYB_BSPACE 2
#define M_SYB_SECT 3
struct m_smdata;
struct m_tostrc {
	/*
		a symbol that selects a symbolw
	*/
	struct m_smdata *sel;
};

#define M_SMDATA_AT(__p, __i)\
	(struct m_smdata*)(((_64_u*)__p)+(__i*0x100))
struct m_smdata {
	M_USERDATA

	//tether
	struct m_tostrc _to[64];	
	struct m_fat *to[64];
	//number of tethers
	_int_u tn;
	struct m_wa *w;
	_64_u flags;
	_64_u dom;
	_64_u value;
	/*
		dummy w for reloc
		w = &wd
	*/
	struct m_fat *start, *end;
	struct m_wa wd;
	_int_u idx;
	_int_u off;
	struct m_section *sec;
	_int_u sec_idx;
	void *smd;
	_64_u x;
	struct m_smdata *next;
};
struct m_symb0 {
	M_USERDATA

	char name[64];
	_int_u len;
	_int_u idx;
	_int_u off;
	struct m_smdata *top;
	struct m_smdata *data;
	_int_u n;
	_8_s sm_inited[16];
};

#define m_ADD 0
#define m_SHL 1
#define m_SHR 2
#define m_OR 3
#define m_AND 4
// complement
#define m_COMP 8
struct m_symb;
struct sb_ext {
	_8_u op;
	struct m_symb *sb;
};

#define _syb_high 1
struct m_symb {
	struct m_pry py;
    char const *start;
    _8_u type;
	_8_u makeup;
    struct m_symb *next;
    _ulonglong val;
  	struct m_smdata *_sm0; 
	struct sb_ext ex[8];
	_int_u exn;
	_64_s neg;
	_64_u bits;
	_32_s dis;
	struct {
		char const *p;
		_int_u len;
	}_0;
};

struct m_instblock;
struct m_in {
	struct m_frag *f;
	struct m_instblock *top;
};


struct m_inst {
	_64_u mods;
	_64_u mod;
	char const *p;
	_int_u len;
	struct m_opbody *_op;
    struct m_symb *yum;
    struct m_inst *next, **bk;
	_int_u n_opands;
    _8_u charac[4];
    _16_u opands[4];
    _ulonglong opandv[4];
	_int_u chc;
};
//lacing
//fragment attachment
struct m_fat {
	M_USERDATA
	_64_u rof;
	_64_u off;
	_64_u size;
	_64_u over;
	_64_u nov;
	_64_u words;
	_64_u adr;
	_64_u idx;
	_8_u reloc;
	struct m_fat *next;
};

struct m_section {
	M_USERDATA
	struct m_smdata *tiein;
};

#define MF_PG_SIZE 2048
struct m_frag {
	struct m_frag *next;
	_8_u **pages;
	_int_u pc;
	_int_u id;	
	_int_u over;
	_8_u *part;

	_64_u underlap;
	_int_u btc;
	_64_u words;
	struct m_inst *in;
	struct m_smdata *sm;
	_16_u attend;
	struct m_frag *sub[100];
	_int_u sub_n;
	struct m_fat *ft;
};
struct m_macval {
	_ulonglong val;
};
#define RLFLAG(__x)(m_strc.rlflags[__x])
#define RLVALU(__x)(m_strc.rlvalmap[__x])
#define M_R_UD	0
#define M_R_8	1
#define M_R_16	2
#define M_R_32	3	
#define M_R_64	4
#define M_R_BH	5
#define M_R_JMP		0
#define M_R_CALL	1
#define M_R_BSPACE	2
struct m_reloc {
	M_USERDATA
	struct m_fat *from;
	struct m_wa *to, *sel;
	_8_u value;
	_64_u flags;
	struct m_smdata *sm;
	_64_u where;
	_32_s dis;
};

struct m_instblock {
	struct m_inst *inst;
	_int_u n;
	struct m_instblock *next;
};
//operation selection
struct m_opnsel{
	void(*emit)(void);
	_8_u mode;
	//0 = bit mode
	//1 = byte mode
};
extern struct m_section *m_sect;
#define M_MECH_PIC		0
#define M_MECH_PICEM	1
extern struct m_opnsel pic18_real;
extern struct m_opnsel pic18_em;
struct malk_struc {
	_8_u mechtab[8];
	_8_u mech;
	struct m_opnsel opn;
	
	struct m_frag *ft;
	struct m_frag *fcur;
	struct hash optab, regtab, labels, mac;
	_8_u *leadmap;
	_8_u *exd;
	_64_u(*exd_func[4])(char*, void*);
	
	struct m_section*(*sect_new)(void);
	struct m_symb0*(*symb_new)(void);
	struct m_smdata*(*sm_data_alloc)(_int_u);
	struct m_wa*(*wa_new)(void);	
	void(*fate_init)(struct m_fat*);
	void(*fat_new)(struct m_fat*);
	struct m_reloc*(*reloc_new)(void);	
	struct m_smdata *sytab[0x100];
	void(*output)(_64_u);
	void(*section)(struct m_smdata*);
	void(*comm)(struct m_smdata*,_int_u);
	_8_u rlvalmap[24];
	_int_u fatsize;
	_8_u fatinit[0x100];

	void *fist;
	struct m_instblock *ib_top, *ib_last;
	_int_u w_m;
	
	void *last_smd;
	void *smd;
	void *smd0;
	/*
		this is the best we are going to be able to do with flags
		as there no other good way to be unless using a function 
		buts that fucking shit.

		this may be messy but efficient
	*/
	_64_u rlflags[64];
	_64_u symflags[64];
	_64_u symdom[64];
	struct m_smdata *entry;
};

/*
	a yield of instruction to be processed 
*/
struct m_iyield {
	struct m_in in[64];
	_int_u n;
	struct m_iyield *next;
};
void mpic_asmbl(void);
void mpic_init(void);
void mpic_final(void);
struct m_frag* m_subfrag(struct m_frag*);
struct m_frag* m_frag_new(void);
void m_frag_pgpush(struct m_frag*, _8_u*);
extern struct malk_struc m_strc;
extern struct m_pry *m_press[8];
void m_pic(struct m_iyield*);
extern void *__smd;
extern _8_s didsym_exist;
_int_u 
m_read_ident(char *__buf, char **__src, _int_u __len);
struct m_smdata*
m_new_symb_non_exist(char *__name,_int_u __len,_64_u);
struct m_smdata*
m_dummy_symb(void);
#endif/*__malk__h*/
