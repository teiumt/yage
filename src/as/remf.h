# ifndef __ffly__as__remf__h
# define __ffly__as__remf__h
# include "frag.h"
void _remf_reloc(struct frag*, _64_u, _32_u, _8_u);
void _remf_hook(struct frag*, _64_u, _32_u, _8_u);
# endif /*__ffly__as__remf__h*/
