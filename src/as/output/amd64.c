# include "../as.h"
# include "../../ffly_def.h"
# include "../../io.h"
# include "../opcodes/amd64.h"
# include "../../string.h" 

#define _suffix_byte 0x0
#define _suffix_word 0x1
#define _suffix_dword 0x2
#define _suffix_qword 0x3


struct ins {
	char buf[24];
};

struct fix_arg {
//	f_as_labelp l;
	_8_u suffix;
	struct f_as_opbody *op;
};

struct fix_arg arg_lk[20];
struct fix_arg *arg_nxt = arg_lk;

static struct f_as_opbody *op;
char const *prot(_16_u __ot) {
	switch(__ot) {
		case _o_reg8:
			return "reg8";
		case _o_reg16:
			return "reg16";
		case _o_reg32:
			return "reg32";
		case _o_reg64:
			return "reg64";
		case _o_imm8:
			return "imm8";
		case _o_imm16:
			return "imm16";
		case _o_imm32:
			return "imm32";
		case _o_imm64:
			return "imm64";
	}
	return "unknown.";
}

_8_u static imm_sz(_16_u __o) {
	switch(__o) {
		case _o_imm8:
			return 1;
		case _o_imm16:
			return 2;
		case _o_imm32:
			return 4;
		case _o_imm64:
			return 8;
	}
	return 0;
}

void static
locate_op(void) {
	struct f_as_optray *_op;
	_op = f_ass.op_tray;
	printf("attempting to locate body for '%s'-operation.\n", _op->name);
	printf("tray details: n_bodys: %u.\n", _op->n_bodies);

	struct f_as_opbody *b;
	_int_u i;
	i = 0;
	for(;i != _op->n_bodies;i++) {
		b = _op->b+i;
		printf("checking tray entry-%u, body: %u : %u.\n", i, b->oc, fak_->n);
		if (b->oc == fak_->n) {
			_8_u o = 0;
			while(o != fak_->n) {
				
				printf("O: %s\n", prot(getinfo(fak_, o)));
				if (!(getinfo(fak_, o)&get_ot(b, o))) {
					break;	
				}
	
				o++;
			}
			if (o == fak_->n) {
				op = b;
				printf("match was found \x1b[32m[ MATCH ]\x1b[0m.\n");
				return;
			}
		} else {
			printf("incorrect operand count, skipping...\n");
		}
	}
	op = NULL;
}

void static
_post(void) {
	if (f_ass.suffix>0) {
		_8_u i = 0;
		_8_u n = fak_->n;
		_16_u *ip;
		while(i != n) {
			_16_u info = *(ip = fak_->info+i);
			if ((info&_o_imm)>0) {
				*ip = _o_imm8<<f_ass.suffix;
			} else if ((info&_o_reg)>0) {
				*ip = _o_reg8<<f_ass.suffix;
			}
			i++;
		}
	}
	locate_op();
	if (!op) {
		printf("failed to locate operation.\n");
		return;
	}
	_8_u buf[64];
	_8_u *p = buf;

	if (fak_->n>0 && !is_flag(op->flags, noprefix)) {
		if (!(getinfo(fak_, 0)^_o_reg16)) {
			*(p++) = 0x66;
		} else if (!(getinfo(fak_, 0)^_o_reg64)) {
			*(p++) = 0x4<<4|1<<3;
			printf("--> v: %x\n", *(p-1));
		}
	}

	_8_u *mod;
	_8_u *opbase;
	mem_cpy(p, op->opcode, op->l);
	opbase = p;
	p+=op->l;
	*p = 0;
	if (is_flag(op->flags, modrm))
		*(mod = p++) = 0x00;
	void **cur = fak_->p;

	_8_u i = 0;
	_8_u n = fak_->n;

	while(i != n) {
		_8_u off = i++;
		_16_u info = getinfo(fak_, off);
		if ((info&_o_imm)>0) {
			printf("%u, ", *(_64_u*)fakget(off));
			_8_u sz;
			if (is_flag(op->flags, ssad))
				sz = ((f_as_regbodyp)fakget(off-1))->info.l;
			else
				sz = imm_sz(getinfo(fak_, off));
			mem_cpy(p, fakget(off), sz);
			p+=sz;
		} else if ((info&_o_reg)>0) {
			f_as_regbodyp reg = (f_as_regbodyp)fakget(off);
			printf("%s, ", reg->head->name);
			if (is_flag(op->flags, modrm)) {
				*mod = *mod<<3|reg->mrm;
			} else {
				*opbase = (*(_8_u*)opbase)+reg->enc;
			}
		} else if ((info&_o_label)>0) {
		/*
			labelp la = (labelp)__label;
			_int_u sz = p-buf;
			if (sz>0) {
				curfrag->k+=sz;
				mem_cpy(curfrag->data, buf, sz);
				p = buf;
			}
			printf("frag-%u: K: %u\n", curfrag->f, curfrag->k);
			arg_nxt->l = la;
			arg_nxt->op = op;
			arg_nxt->suffix = f_ass.suffix;
			arg_nxt++;
			fix(curfrag, arg_nxt-1, _fx_dis, 0x00);
			pf_flags |= FR_FIX;
			ff_as_fdone(curfrag);
			ff_as_fnew(); // new frag
		*/
		}
		cur++;
	}
	printf("\n");

	if (is_flag(op->flags, modrm))
		*mod |= op->ext<<3|3<<6;
	_int_u sz = p-buf;
	if (sz>0) {
		// grow frag by X amount
		ff_as_plant(curfrag, buf, sz);
	}
}

f_as_regp static
_getreg(char const *__name, _int_u __len) {
	return tun_hash_get(&f_ass.regs, __name, __len);
}

_int_u static __n(long long __val) {
	_int_u i;
	i = 0;
	while((__val&(0xffffffffffffffff<<i))>0)
		i++;
	return i;
}


#define _dn 0x01
_8_u static ff = 0x00;
void static
_fixins(struct fix_s *__fx) {
/*
	printf("^^^^^^^^^^^^^^^fix.\n");
	struct fix_arg *arg;
	arg = (struct fix_arg*)__fx->arg;
	if (__fx->type == _fx_dis) {
		f_as_labelp l = arg->l;
		_int dis;
		_8_u x, j;
		printf("----> fix: %u, %u\n", l->f->m, __fx->f->m);
		dis = ((l->f->adr+l->f->m)+l->f_offset)-(__fx->f->adr+__fx->f->m+__fx->f->size+__fx->f->bs+__fx->f->k);
		printf("dis: %d\n", dis);
		if (arg->suffix != -1) {
			*(_64_i*)(__fx->f->data+__fx->f->k) = dis;
			if (__fx->f->bs != 4) {
				fix_flgs |= 0x01;
			}
			__fx->f->bs = 4;
			return;
		}

		_int_u n;
		n = __n(dis<0?-dis:dis);
		_int_u bs;
		_int *bsp;
		bsp = &__fx->f->bs;
		bs = ((n+1)+((1<<3)-1))>>3;
	
		printf("}}}}}}}}}}}}}M:%u, BS:%u\n", __fx->f->m, bs);

		x = 1<<(bs-1);

		if ((x&0x01) >0) {
			bs = 1+(j = (arg->op->ad>>_d8s&0x01));
			x<<=j;
			
		}
		
		if ((x&0x02) >0) {
			if (is_flag(arg->op->flags, osof16) && !(ff&_dn)) {
				__fx->f->data[1] = *__fx->f->data;
				*__fx->f->data = 0x66;
				__fx->f->k++;
				fix_flgs |= 0x01;
				ff |= _dn;
			}
			bs = 2+((arg->op->ad>>_d16s&0x01)<<1);
		}
		if ((x&0x0c) >0) {
			bs = 4+((arg->op->ad>>_d32s&0x01)<<2);
		}
		if ((x&0xf0) >0) {
			bs = 8;
		}
		printf("B: %u, W: %u, D: %u, Q: %u, DIS: %d\n", x&0x01, x&0x02, x&0x0c, x&0xf0, dis-bs);
	
		*(_64_i*)(__fx->f->data+__fx->f->k) = dis;

		printf("before %u, after %u\n", *bsp, bs);
		if (*bsp != bs)
			fix_flgs |= 0x01;
		*bsp+=(bs-*bsp);
		return;
	}
*/
}

void
urek_amd64(void) {
	_int_u i;
	i = 0;
	struct f_as_optray const *op;
	struct f_as_opbody *b;
	while(i != NOPS) {
		op = amd64_optab+i;
		tun_hash_put(&f_ass.env, op->name, op->nme_len, op);
		i++;
	}
	f_ass.ins_post = _post;
	f_ass.getreg = _getreg;
//	ff_as_fixins = _fixins;
	struct f_as_reg const *reg;
	i = 0;
	while(i != REGC) {
		reg = amd64_regtab+i;
		tun_hash_put(&f_ass.regs, reg->name, reg->len, reg);
		i++;
	}
	f_ass.suffix_map['b'] = _suffix_byte;
	f_ass.suffix_map['w'] = _suffix_word;
	f_ass.suffix_map['d'] = _suffix_dword;
	f_ass.suffix_map['q'] = _suffix_qword;
}
