# include "../as.h"
# include "../../ffly_def.h"
# include "../../io.h"
# include "../opcodes/resin.h"
# include "../../string.h"
# include "../../remf.h"
# include "../../m_alloc.h"
# include "../../assert.h"
#define _suffix_byte 0x0
#define _suffix_word 0x1
#define _suffix_dword 0x2
#define _suffix_qword 0x3

// bed of stack
_int_u static bed = 0;

typedef struct reginfo {
	struct f_as_reginfo info;
	struct f_as_reg r;
	
	_int_u addr;
	_8_u flags;
	_8_u start;

};

static struct fix_s *fx = NULL;
#define _fx_jump 0x00
struct fix_s {
	_8_u type;
	struct fix_s *next;
	f_as_wap d;
	_32_s dis, ds;
	struct f_as_tang *s;
};

static struct fix_s* fix_new(void) {
	struct fix_s *x;
	x = (struct fix_s*)m_alloc(sizeof(struct fix_s));

	x->next = fx;
	fx = x;
	return x;
}

static struct f_as_opbody *op;
void static *p0, *p1, *p2, *p4;

#define sp_rg 0x00
#define bp_rg 0x01

#define rax_rg 0x02
#define eax_rg 0x03
#define ax_rg 0x04
#define al_rg 0x05

#define rlx_rg 0x06
#define elx_rg 0x07
#define lx_rg 0x08
#define ll_rg 0x09

#define rel_rg 0x0a
#define ael_rg 0x0b
#define el_rg 0x0c
#define ae_rg 0x0d

#define xes_rg 0x0e 
#define els_rg 0x0f
#define ls_rg 0x10
#define xs_rg 0x11
#define r0_rg 0x12
#define r1_rg 0x13
void static
oust_addr(_f_addr_t __addr) {
//	ff_as_oust((_8_u*)&__addr, sizeof(_f_addr_t));
}

# define reg_spl 0x1
struct reginfo reg[] = {
	{{8}, {"sp", 2}, 0, 0},				{{8}, {"bp", 2}, 8, 0},
	{{8}, {"rax", 3}, 16, 0},			{{4}, {"eax", 3}, 16, 0},			{{2}, {"ax", 2}, 16, 0},		{{1}, {"al", 2}, 16, 0},
	{{8}, {"rlx", 3}, 24, 0},			{{4}, {"elx", 3}, 24, 0},			{{2}, {"lx", 2}, 24, 0},		{{1}, {"ll", 2}, 24, 0},
	{{8}, {"rel", 3}, 32, 0},			{{4}, {"ael", 3}, 32, 0},			{{2}, {"el", 2}, 32, 0},		{{1}, {"ae", 2}, 32, 0},
	{{8}, {"xes", 3}, 40, 0},			{{4}, {"els", 3}, 40, 0},			{{2}, {"ls", 2}, 40, 0},		{{1}, {"xs", 2}, 40, 0},
	{{8}, {"r0", 2}, 0, reg_spl, 0},	{{8}, {"r1", 2}, 0, reg_spl, 1},
};

/*
	1, 2, 4, 8, 16, 32, 64, 128
	r, a, x, e, l,	s,	b,	p
	   0,				1
*/

/*
	rax	=	00011111	= 248
	eax	=	10001111	= 241
	ax	=	10011111	= 249
	al	=	10110111	= 237
	rlx	=	01010111	= 234	
	elx	=	11000111	= 227
	lx	=	11010111	= 235
	ll	=	11111111	= 255
	rel	=	01100111	= 230
	ael	=	10100111	= 229
	el	=	11100111	= 231
	ae	=	10101111	= 245
	xes	=				= 211
	els	=				= 199
	ls	=				= 207
	xs	=				= 219
	sp	=				= 95
	bp	=				= 63
	r0/ra	=			= 253
	r1/rb	=			= 191
*/

_8_u c[] = {
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//0
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//1
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//2
	1,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//3
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//4
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//5
	0,1,6,0,0,3,0,0,0,0,0,0,4,0,0,0,	//6
	7,0,0,5,0,0,0,0,2,0,0,0,0,0,0,0,	//7
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//8
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//9
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//10
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//11
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//12
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//13
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,	//14
	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 	//15
};

char const* rbl(_8_u __l) {
	switch(__l) {
		case 1: return "al";
		case 2: return "ax";
		case 4: return "eax";
		case 8: return "rax";
	}
}

f_as_regp static
getreg(char const *__name, _int_u __len) {
	char const *p = __name;
	char const *e = p+__len;
	_8_u no;

	no = 0xff;
	while(p != e) {
		no ^= 1<<c[(*p)];
		p++;
	}

	struct reginfo *ri;
/*
	put in array is __ffly_mscars if not defined
	might be a wast of memory but we will give the option
*/
	switch(no) {
		case 0xf8:	ri = reg+rax_rg;break;
		case 0xf1:	ri = reg+eax_rg;break;
		case 0xf9:	ri = reg+ax_rg;break;
		case 0xed:	ri = reg+al_rg;break;
		case 0xea:	ri = reg+rlx_rg;break;
		case 0xe3:	ri = reg+elx_rg;break;
		case 0xeb:	ri = reg+lx_rg;break;
		case 0xff:	ri = reg+ll_rg;break;
		case 0xe6:	ri = reg+rel_rg;break;
		case 0xe5:	ri = reg+ael_rg;break;
		case 0xe7:	ri = reg+el_rg;break;
		case 0xf5:	ri = reg+ae_rg;break;
		case 0xd3:	ri = reg+xes_rg;break;
		case 0xc7:	ri = reg+els_rg;break;
		case 0xcf:	ri = reg+ls_rg;break;
		case 0xdb:	ri = reg+xs_rg;break;
		case 0x5f:	ri = reg+sp_rg;break;
		case 0x3f:	ri = reg+bp_rg;break;
		case 0xfc:	ri = reg+r0_rg;break;
		case 0xbe:	ri = reg+r1_rg;break;
		default:
			printf("failed to get register.\n");
		return NULL;
	}

	ri->r.priv = ri;
	return &ri->r;
}

void static
emit_label(void) {

}
/*
	at the moment some registers might use memory
*/
# define POSTST 48//registers
void ff_as_prepstack(void) {
	bed+=POSTST;		
}

_int_u ff_as_stackadr() {
	return bed;
}

void ff_as_isa(_int_u __by) {
	bed+=__by;
}
/*
void op_asw(_8_u __op, _f_addr_t __dst, _16_u __val) {
	_8_u buf[5];
	*buf = __op;
	*(_16_u*)(buf+1) = __dst;
	*(_16_u*)(buf+3) = __val;

	fgrowb(curfrag, 5);
	ff_as_plant(curfrag, buf, 5);
}

void rgasw(char const *__reg, _16_u __to) {
	op_asw(_resin_op_asw, getreg(__reg)->addr, __to);
}
*/
_8_u imm_sz(_16_u __o) {
	switch(__o) {
		case _o_imm8:
			return 1;
		case _o_imm16:
			return 2;
		case _o_imm32:
			return 4;
		case _o_imm64:
			return 8;
	}
	return 0;
}

void static
locate_op(void) {
	struct f_as_optray *_op;
	_op = f_ass.op_tray;
	printf("attempting to locate body for '%s'-operation.\n", _op->name);
	printf("tray details: n_bodys: %u.\n", _op->n_bodies);
	struct f_as_opbody *b;
	_int_u i;
	i = 0;
	for(;i != _op->n_bodies;i++) {
		b = _op->b+i;
		printf("checking tray entry-%u, body: %u : %u.\n", i, b->oc, fak_->n);
		if (b->oc == fak_->n) {
			_8_u o = 0;
			while(o != fak_->n) {
				printf("matching: %x.\n", getinfo(fak_, o));
				if (!(getinfo(fak_, o)&get_ot(b, o))) {
					break;
				}
   
				o++;
			}
			if (o == fak_->n) {
				op = b;
				printf("match was found \x1b[32m[ MATCH ]\x1b[0m.\n");
				return;
			}
		} else {
			printf("incorrect operand count, skipping...\n");
		}
	}
	op = NULL;
}

_8_i static suffix;
#define _imm 0x01
#define _reg 0x02
#define _lab 0x04
# include "../remf.h"

void static
_special(void) {
	f_as_symbolp s;

	s = (f_as_symbolp)fakget(0);
	F_assert(f_ass.tx != NULL);
	printf("SYMB: %p.\n", s);
	if ((s->flags&F_AS_SY_RS)>0) {
		f_as_relocp rlc;
		rlc = f_ass.reloc();
		rlc->type = F_AS_R_16;
		rlc->s = f_ass.tx;

		rlc->d = s->w;
		rlc->flags = F_AS_R_UDD;
		f_as_tfxp tx;
		tx = f_ass.tfx_new(f_ass._locl);
		tx->h = f_ass.tg_end;
		f_ass.v.off+=3;
		f_ass.v.fix+=2;
		_8_u buf[] = {_resin_op_jmp};
		ff_as_plant(f_ass.topmost, buf, 1);
		f_ass.tx->e = f_ass.tg_end;
		f_ass.tx->e_off = f_ass.v.off-2;
		tx->s_off = f_ass.v.off;
		tx->fix = f_ass.v.fix;
		f_ass.tx = tx;
	} else {
		struct fix_s *x = fix_new();
		x->type = _fx_jump;
		x->s = f_ass.tg_end;
		x->d = NULL;
		if (!(s->flags&F_AS_SY_FND)) {
			*(s->w_bp++) = &x->d;
		} else
			x->d = s->w;
		f_as_sfnew(f_ass.topmost);
		f_ass.v.off = 0;
	}

}

# include "../../tools.h"
void static
_post(void) {
	_16_u si = 0x00;
	_int_u i;
	i = 0;
	for(;i != fak_->n;i++) {
		_16_u info = getinfo(fak_, i);
		if ((info&_o_imm)>0) {
			si |= _imm;
		} else if ((info&_o_reg)>0) {
			si |= _reg;
		} else if ((info&_o_label)>0) {
			si |= _lab;
		}
	}

	if ((si&_lab)>0) {
		_special();
		return;
	} else
		locate_op();

	if (!op) {
		printf("failed to locate operation.\n");
		return;
	}

	printf("OPERATION SUFFIX: %u, %u.\n", f_ass.suffix, is_flag(op->flags, asto));

	_8_u buf[64];
	_8_u *p = buf;
	_8_u *opbase;
	opbase = p;
	_8_u oc;
	oc = 0;
	if (is_flag(op->flags, asto) && f_ass.suffix != 0xff) {
		oc = f_ass.suffix;
	}

	mem_cpy(p, op->opcode+oc, op->l);
	p+=op->l;


	void **cur = fak_->p;
	i = 0;
	_8_u n = fak_->n;
	while(i != n) {
		_8_u off = i++;
		_16_u info = getinfo(fak_, off);
		if ((info&_o_imm)>0) {
			_8_u os;
			if (!oc) {
				os = get_ous(op, off);
				if (!os)
					os = info>>5;
			} else {
				os = 1<<oc;
			}

			mem_cpy(p, &fakget(off), os);
			p+=os;
		} else if ((info&_o_reg)>0) {
			struct reginfo *reg = (struct reginfo*)fakget(off);
			if (is_flag(reg->flags, reg_spl)) {
				oc+=op->splr[reg->start];
			} else {
				mem_cpy(p, &reg->addr, sizeof(_f_addr_t));
				p+=sizeof(_f_addr_t);
			}
		}
		cur++;
	}


	_int_u sz = p-buf;
	ffly_hexdump(buf, sz);
	if (sz>0) {
		f_ass.v.off+=sz;
		// grow frag by X amount
		ff_as_plant(f_ass.topmost, buf, sz);
	}
}

f_as_regp static
_getreg(char const *__name, _int_u __len) {
	return getreg(__name, __len);
}

_int_u static __n(long long __val) {
	if (!(__val&0xffffffffffffff00))
		return 1;
	else if (!(__val&0xffffffffffff0000))
		return 2;
	else if (!(__val&0xffffffff00000000))
		return 4;
	return 8;
}

void static
_fix(void) {
	struct fix_s *x;
	x = fx;
	while(x != NULL) {
		if (x->type == _fx_jump) {
			_8_u neg;
			_32_s dis;
			F_assert(x->d != NULL);
			dis = x->d->adr-(x->s->adr+x->s->size);
			x->dis = dis;
			if ((neg = (dis<0)))
				dis = -dis;
			_int_u n, sz;
			n = __n(dis);
			switch(n) {
				case 1:
				case 2:
				case 4:
				case 8:
				{
					sz = 3;
					x->ds = 2;
				}
				break;
			}
			x->s->bs = sz;
		}
		x = x->next;
	}
}

void static
_apply(void) {
	struct fix_s *x;
	x = fx;
	while(x != NULL) {
		if (x->type == _fx_jump) {
			x->s->buf[0] = _resin_op_jmp;
			*(_16_s*)(x->s->buf+1) = x->dis;
		}
		x = x->next;
	}
}

# include "../../string.h"
void
urek_resin(void) { 
   	_int_u i;
	i = 0;
	struct f_as_optray const *op;
	struct f_as_opbody *b;
	while(i != NOPS) {
		op = resin_optab+i;
		printf("OP_LOAD: %s, %u\n", op->name, op->nme_len);
		tun_hash_put(&f_ass.env, op->name, op->nme_len, op);
		i++;
	}
	f_ass.ins_post = _post;
	f_ass.getreg = _getreg;
	f_ass.fix = _fix;
	f_ass.apply = _apply;
	f_ass.suffix_map['b'] = _suffix_byte;
	f_ass.suffix_map['w'] = _suffix_word;
	f_ass.suffix_map['d'] = _suffix_dword;
	f_ass.suffix_map['q'] = _suffix_qword;
}
