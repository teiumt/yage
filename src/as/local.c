# include "as.h"
# include "alloca.h"
_16_u static n = 0;

long long static *array;
long long* as_local_new(_16_u __n) {
	if (!array) {
		n = __n+1;
		array = (long long*)turn_al(n*sizeof(long long));
	} else {
		if (__n>=n) {
			n = __n+1;
			array = (long long*)turn_ral(array, n*sizeof(long long));
		}
	}
	return array+__n;
}

long long* as_local_get(_16_u __n) {
	return array+__n;
}
