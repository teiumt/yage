# include "as.h"
# include "../string.h"
# include "../m_alloc.h"
stp stt_head = NULL;
stp stt_tail = NULL;
_int_u stt_off = 0;
_int_u ff_as_stt(char const *__str, _int_u __l) {
	_int_u l = (__l>0?__l:str_len(__str))+1;
	stt_off+=l;
	
	stp p = (stp)m_alloc(sizeof(struct st));
	mem_dup((void**)&p->p, __str, l);
	p->l = l;
	if (!stt_tail)
		stt_tail = p;
	if (stt_head != NULL)
		stt_head->prev = p;
	p->prev = NULL;
	p->next = stt_head;
	stt_head = p;
	return stt_off;
}

_64_u(*ff_as_stt_drop)(void);
