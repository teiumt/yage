#ifndef __turn__h
#define __turn__h
#include "../y_int.h"

enum {
	OPT_INPUT,
	OPT_OUTPUT,
	OPT_GROUNDER,
	OPT_PROCESSOR,
	OPT_FORMAT
};

struct he_data {
	void *p;
};

struct hep_struc {
	struct he_data *d;
	_8_i exist;
};

typedef struct hash_entry {
	struct hash_entry *next;
	_8_u const *key;
	struct he_data hed;
	_int_u len;
} *hash_entryp;

struct hash {
	struct hash_entry **table;
};

#include "urek.h"
/*
	hash.c
*/
void tun_hash_init(struct hash*);
void tun_hash_pgi(struct hash*, _8_u const*, _int_u, struct hep_struc*);
void tun_hash_put(struct hash*, _8_u const*, _int_u, void*);
void* tun_hash_get(struct hash*, _8_u const*, _int_u);
extern _8_u inmap[0x100];
#endif /*__turn__h*/
