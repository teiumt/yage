# ifndef __f__as__frag__h
# define __f__as__frag__h
# include "../y_int.h"
# define FR_FIX 0x01

// grow fragment by X about or (current fragment size)+X
# define fgrowb(__f, __by) \
	ff_as_fgrow(__f, (__f)->size+__by)
/*
	get frag by number

	example

	frag 0
	frag 1 
	frag 2
	frag 3

	say we have frag 1 and want frag the fragments from 1 to ...

_next:
	(frag i) = ff_as_fbn((frag 1)->f+i);
	i++;
	goto _next;

	could do this with linked lists but i like to keep my distance from useing them
	and only use them when necessary or theres no other way.
*/
extern _int_u fr_nr;
struct f_as_frag;
#define frag f_as_frag
// TODO: add frag by num x
struct frag *ff_as_fbn(_int_u);
/*
	subfrags go by order first to last
	size is left unknown as its not needed
*/
struct f_as_subfrag {
	struct f_as_subfrag *next;
	struct f_as_tang *t;
	_32_u off, end;
	_32_u size;
	struct f_as_tbc b;
};
/*
	fragments - (could be nameless) 
	jmp,_label:
	subfragments - nameless jump,_label:
*/
typedef struct f_as_frag {
	struct f_as_subfrag *sf;
	struct f_as_subfrag *sf_top, *sf_end;
	_int_u sf_n;
	// next fragment in list
	/*
		mostly used when writing fragments and cleanup <- later
	*/
	struct frag *next;
	// fragment number
	_int_u f;
	_8_u flags;
	/*
		page pointers 

		 . where fragment data is to be stored
	*/
	void **p;
	// number of pages
	_32_u page_c;
	/*
		size of fragment in (bytes)(used)
		does not included excess if not aligned to page size

		for real size: (page_c*PAGE_SIZE) = size+excess;
	*/
	_32_u size;
	/*
		where next piece of data should
		be placed within fragment.

		offset from base 
	*/
	_32_u offset;
} *f_as_fragp;

struct f_as_subfrag* f_as_sfnew(f_as_fragp);
// current fragment
extern struct frag *curfrag, *fr_head;
// new fragment
struct frag* ff_as_fnew(void);
/*
	write to fragment at X offset
*/
void ff_as_fwrite(struct frag*, _32_u, void*, _int_u);
/*
	read from fragment at X offset
*/
void ff_as_fread(struct frag*, _32_u, void*, _int_u);
/*
	TODO:
		add shrink function ?????? not needed but might be or could be usefull later ?

	grow fragment to X size
*/
void ff_as_fgrow(struct frag*, _32_u);
/*
	output fragments from head to tail
*/
void ff_as_foutput(void);
/*
	plant a piece of data at end of fragment

	NOTE:
		does not grow fragment only places
*/
void ff_as_plant(struct frag*, void*, _int_u);

// fragment is no longer needed as we are moving on to a new fragment
void ff_as_fdone(void);
#define frag_
#define frag_offset(__f) ((__f)->offset)
#define frag_flags(__f) (__f)->flags
#define pf_offset curfrag->offset
#define pf_flags curfrag->flags
# endif /*__f__as__frag__h*/
