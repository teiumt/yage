#include "malk.h"
#include "../m_alloc.h"
#include "../blf.h"
#include "../string.h"
#include "../assert.h"
//barren linker format
/*
	as its a simple format
	and is a barren wastland when
	it comes to it
*/
#include "../io.h"
struct str_ent {
    _64_u offset;
    char buf[256];
    _int_u len;
    struct str_ent *next;
};


struct str_ent dummy_str = {
	.next = NULL
};
struct {
    _64_u offset;
    struct str_ent *top, *tail;
} str_aloc = {
        0,
        &dummy_str,
        &dummy_str
};

_64_u static strent_alloc(char *__name, _int_u __len) {
    _64_u of = str_aloc.offset;
    struct blf_stent *b_ent;
	struct str_ent *e = m_alloc(sizeof(struct str_ent));
    str_aloc.tail->next = e;
    str_aloc.tail = e;
    str_aloc.offset+=sizeof(struct blf_stent)+__len+1;
	char *str_pm;
	str_pm = e->buf+sizeof(struct blf_stent);
	b_ent = e->buf;
	b_ent->len = __len;
	
	if (__len>0)
		mem_cpy(str_pm,__name,__len+1);
	str_pm[__len] = '\0';
	e->len = __len;

	e->offset = of;
	return of;
}

struct linkstrc {
	void *next;
};

struct symb {
	union {
	struct m_symb0 s;
	struct linkstrc link;
	};
};

struct wa {
	union {
	struct m_wa w;
	struct linkstrc link;
	};
};

struct _frag {
	union {
	struct {
	struct m_fat f;
	_64_u wa;
	_int_u nw;
	};
	struct linkstrc link;
	};
};

struct reloc {
	union {
	struct m_reloc rl;
	struct linkstrc link;
	};
};

struct smdata {
	union{
		struct m_smdata d;
		struct linkstrc link;
		_64_u pad[0x100];
	};
};

struct sect {
	union{
	struct {
	struct m_section s;
	_int_u wa;
	_int_u fr;
	_int_u rl;
	_int_u nwa;
	_int_u nfr;
	_int_u nrl;
	};
	struct linkstrc link;
	};
};

static _int_u nwa				= 0;
static _int_u nfr				= 0;
static _int_u nrl				= 0;
static _int_u nsec				= 0;
static _int_u smdn				= 0;
static struct wa *wa_top		= NULL;
static struct _frag *fr_top		= NULL;
static struct reloc *rl_top		= NULL;
static struct sect *sec_top		= NULL;

struct pot {
	void *top;
	_int_u nsb;
};
struct sect static *_sec = NULL;
void static
finish_sect(struct sect *sec) {
	sec->nwa = nwa-sec->wa;
	sec->nfr = nfr-sec->fr;
	sec->nrl = nrl-sec->rl;
}

#define LINKUP(__strc,__where)\
	(__strc)->link.next = __where;\
	__where = __strc;

struct m_section static*
sect_new(void) {
	if (_sec != NULL) {
		finish_sect(_sec);	
	}
	struct sect *s;
	s = m_alloc(sizeof(struct sect));
	s->wa = nwa;
	s->fr = nfr;
	s->rl = nrl;
	_sec = s;
	LINKUP(s,sec_top)
	nsec++;
	return s;
}

_int_u static nsb = 0;
static struct symb *smb_top = NULL;
static struct pot smd_pot = {NULL,0};
static struct pot smd_pot0 = {NULL,0};
struct m_symb0 static*
symb_new(void) {
	struct symb *s;
	s = m_alloc(sizeof(struct symb));
	s->s.off = nsb*sizeof(struct blf_sym);
	s->s.idx = nsb++;
	s->s.n = 0;
	LINKUP(s,smb_top)
	return s;
}

struct m_smdata*
sm_data_alloc(_int_u __n) {
	return m_alloc(sizeof(struct smdata)*__n);
}

struct m_wa static*
wa_new(void) {
	struct wa *w;
	w = m_alloc(sizeof(struct wa));
	w->w.off = nwa*sizeof(struct blf_wa);
	w->w.idx = nwa;
	nwa++;
	LINKUP(w,wa_top)
	return w;
}

static struct _frag *_fr = NULL;
void static fate_init(struct _frag *f) {
	_fr->nw = nwa-_fr->wa;
	f->wa =  nwa;
	_fr = f;
}
void static
fat_new(struct _frag *f) {
	f->f.rof = nfr*sizeof(struct blf_frag);
	f->f.idx = nfr;
	nfr++;
	LINKUP(f,fr_top)
}


struct m_realoc static*
reloc_new(void) {
	struct reloc *rl;
	rl =  m_alloc(sizeof(struct reloc));
	nrl++;
	rl->rl.flags = 0;
	LINKUP(rl,rl_top)
	return rl;
}

void static
_write(void *__buf, _int_u __n) {
	turn_strc.ops.out.write(NULL, __buf, __n);
	turn_strc.out.offset+=__n;
}

void static
sd_to_blf(struct blf_smdata *s, struct smdata *__s) {
	if (__s->d.w != NULL)	
		s->w = __s->d.w->idx;
	s->sec = __s->d.sec->tiein->sec_idx;
	s->flags = 0;
	s->domain = __s->d.dom;
}


static struct blf_hdr h;
void static
output(_64_u __size) {
	finish_sect(_sec);
	struct smdata *last_sd;
	/*
		its either this or we create a dummy fragment
	*/
	last_sd = m_strc.last_smd;
	last_sd->d.end = fr_top;
	
	_fr->nw = nwa-_fr->wa;

	h.start = turn_strc.out.offset-__size;
	h.size = __size;

	_int_u i;
	h.syt = turn_strc.out.offset;
	h.sytsz = nsb*sizeof(struct blf_sym);

	struct smdata *smd;
	struct blf_smdata *_sd;
	struct blf_smdata *sd;

	struct symb *sb;
	struct blf_sym *_sy = m_alloc(h.sytsz);
	struct blf_sym *sy;

	i = 0;
	sb = smb_top;
	for(;i != nsb;i++) {
		sy = _sy+sb->s.idx;
		sy->name = strent_alloc(sb->s.name, sb->s.len);
		sy->value = smdn;
		sy->n = sb->s.n;
		smd = sb->s.top;
		_int_u j;
		j = 0;
		while(smd != NULL) {
			struct pot *pt = smd->d.smd;
			LINKUP(smd,pt->top)
			pt->nsb++;

			smd->d.off = (j+smdn)*sizeof(struct blf_smdata);
			smd->d.idx = j+smdn;
			printf("SYMB%u_SMD%u: %u,%u.\n",i,j,smd->d.idx,smd->d.off);
			j++;
			smd = smd->d.next;
		}
		smdn+=sb->s.n;
		sb = sb->link.next;
	}

	if (!m_strc.entry)
		h.entry = BLF_NULL; 
	else
		h.entry = m_strc.entry->idx;
	
	h.w_m = m_strc.w_m;
	_write(_sy, h.sytsz);	
	m_free(_sy);

	h.smd = turn_strc.out.offset;
	h.smdn = smdn;

	_sd = m_alloc(smdn*sizeof(struct blf_smdata));
	smd = smd_pot.top;
	printf("SMD_NUM: %u of %u.\n", smd_pot.nsb+smd_pot0.nsb,smdn);
	i = 0;
	for(;i != smd_pot.nsb;i++) {
		sd = _sd+smd->d.idx;
		sd->x = smd->d.x;
		sd_to_blf(sd,smd);
		printf("-========- BLF_SMD: start: %u, stop: %u, %u->%u\n",smd->d.start->adr,smd->d.end->adr,smd->d.start->idx,smd->d.end->idx);
		sd->f_start	= smd->d.start->rof;
		sd->f_end	= smd->d.end->rof;
		sd->f_is	= smd->d.start->idx;
		sd->f_ie	= smd->d.end->idx;
		smd = smd->link.next;
	}
	/*
		???	 how can this be done better

		issue:
			we can create another fragment but its a wast and wont be used.
			so this???
	*/
	sd = _sd+last_sd->d.idx;
	sd->f_end+=sizeof(struct blf_frag);
	sd->f_ie++;


	smd = smd_pot0.top;
	i = 0;
	for(;i != smd_pot0.nsb;i++) {
		sd = _sd+smd->d.idx;
		sd->x = smd->d.x;
		sd->w = smd->d.wd.idx = i;
		sd->sec = smd->d.sec->tiein->sec_idx;
		sd->domain = smd->d.dom;
		sd->flags = smd->d.flags;
		sd->value = smd->d.value;
		sd->f_start = 0;
		sd->f_end = 0;
		sd->f_is = 0;
		sd->f_ie = 0;
		smd = smd->link.next;
	}

	_write(_sd, smdn*sizeof(struct blf_smdata));
	m_free(_sd);

	h.wat = turn_strc.out.offset;
	h.wsz = nwa*sizeof(struct blf_wa);
	struct blf_wa *_w = m_alloc(h.wsz);
	struct blf_wa *w;
	i = 0;
	struct wa *a;
	a = wa_top;
	for(;i != nwa;i++) {
		w = (struct blf_wa*)(((_8_u*)_w)+a->w.off);
		w->adr = 0;
		//doesent matter for now as only single wa for eatch tang premitted
		a = a->link.next;
	}
	_write(_w, h.wsz);
	m_free(_w);

	h.ft = turn_strc.out.offset;
	h.ftsz = nfr*sizeof(struct blf_frag);
	struct blf_frag *_fr = m_alloc(h.ftsz);
	struct blf_frag *fr;
	struct _frag *f;
	f = fr_top;
	i = 0;
	for(;i != nfr;i++) {
		fr = (struct blf_frag*)(((_8_u*)_fr)+f->f.rof);
		fr->off = f->f.off;
		fr->size = f->f.size;
		fr->over = f->f.over;
		fr->nov = f->f.nov;
		printf("FRAG' over{%x:%u}.\n", fr->over,fr->nov);
		fr->wa = f->wa;
		fr->nw = f->nw;
		fr->adr = f->f.adr;
		fr->words = f->f.words;
		f = f->link.next;
	}
	_write(_fr, h.ftsz);
	m_free(_fr);

	h.rl = turn_strc.out.offset;
	h.rsz = nrl*sizeof(struct blf_reloc);
	
	struct blf_reloc *_rl = m_alloc(h.rsz);
	struct blf_reloc *rl;
	struct reloc *r;
	r = rl_top;
	i = 0;
	for(;i != nrl;i++) {
		rl = _rl+i;
		if (r->rl.value != BLF_PIC_R_BSPACE) {
			rl->w = r->rl.to->idx;
		}else
			rl->sm = r->rl.sm->idx;
		rl->where = r->rl.where;
		rl->f = r->rl.from->idx;
		rl->value = r->rl.value;
		rl->flags = r->rl.flags;
		rl->dis = r->rl.dis;
		if (r->rl.sel != NULL) {
			rl->w0 = r->rl.sel->idx;
		}else
			rl->w0 = 0x41;
		printf("RELOC: %u.\n", rl->value);
		r = r->link.next;
	}
	_write(_rl, h.rsz);
	m_free(_rl);

	h.sec = turn_strc.out.offset;
	h.nsec = nsec;	
	struct blf_section *sects;
	struct blf_section *sec;
	struct sect *_s = sec_top;
	sects = m_alloc(sizeof(struct blf_section)*h.nsec);
	i = 0;
	for(;i != h.nsec;i++) {
		sec = sects+i;
		sec->idx = _s->s.tiein->sec_idx;
		sec->ft		= _s->fr;
		sec->wat	= _s->wa;
		sec->rl		= _s->rl;
		sec->nfr	= _s->nfr;
		sec->nw		= _s->nwa;
		sec->nr		= _s->nrl;
		_s = _s->link.next;
	}

	_write(sects,sizeof(struct blf_section)*h.nsec);
	m_free(sects);
	
	h.st_size = str_aloc.offset;
	h.st_ents = turn_strc.out.offset;
	printf("STRING: offset: %u, size: %u.\n",h.st_ents,h.st_size);
	_int_u ste_n = 0;
	if (str_aloc.offset>0) {
		_8_u *stt_buf = m_alloc(str_aloc.offset);
		struct str_ent *st_ent;
		//first entry is a dummy elemanating need for extra if statment in alloc routine
		st_ent = str_aloc.top->next;
		str_aloc.tail->next = NULL;
		while(st_ent != NULL) {
			_8_u *dst = stt_buf+st_ent->offset;
			printf("ENTRY: at: %u, len: %u, %s\n",st_ent->offset,st_ent->len,st_ent->buf+sizeof(struct blf_stent));		
			mem_cpy(dst,st_ent->buf,st_ent->len+sizeof(struct blf_stent)+1);
			st_ent = st_ent->next;
			ste_n++;
		}
		_write(stt_buf,str_aloc.offset);
		m_free(stt_buf);
	}
	h.nsy	= nsb;
	h.nfr	= nfr;
	h.nw	= nwa;
	h.nr	= nrl;
	h.st_n = ste_n;
	h.mech = m_strc.mech;
	turn_strc.ops.out.pwrite(NULL, &h, 512, 0);
	printf("OUTPUT: %u, %u, %u, %u, {%x,%x,%x,%x}\n", nsb, nwa, nfr, nrl, h.syt,h.wat,h.ft,h.rl);
}
static _int_u cursec = 0;
void static
_section(struct smdata *sm) {
	sm->d.sec_idx = cursec++;
	sm->d.flags = BLF_SYB_SECT;
	sm->d.w = &sm->d.wd;
}

void static
_comm(struct smdata *sm, _int_u __size) {
	sm->d.value = __size;
	sm->d.dom = BLF_SYB_D_BSPACE;
}

void malk_blf(void) {
	m_strc.mechtab[M_MECH_PIC] = BLF_M_PIC;
	m_strc.mechtab[M_MECH_PICEM] = BLF_M_PIC_EM;
	m_strc.rlvalmap[M_R_JMP] = BLF_PIC_R_JMP;
	m_strc.rlvalmap[M_R_CALL] = BLF_PIC_R_CALL;
	m_strc.rlvalmap[M_R_BSPACE] = BLF_PIC_R_BSPACE;
	printf("using BLF.\n");
	m_strc.sect_new = sect_new;
	m_strc.sm_data_alloc = sm_data_alloc;
	m_strc.symb_new = symb_new;
	m_strc.wa_new = wa_new;
	m_strc.fat_new = fat_new;
	m_strc.reloc_new = reloc_new;
	m_strc.fate_init = fate_init;
	turn_strc.out.offset = 512;
	m_strc.output = output;
	m_strc.fatsize = sizeof(struct _frag);
	struct _frag *f;
	f = m_strc.fatinit;
	f->wa = 0;
	f->nw = 0;
	m_strc.fist = f = m_alloc(sizeof(struct _frag));
	_fr = f;
	f->wa = 0;
	m_strc.section = _section;
	m_strc.comm = _comm;

	m_strc.smd = &smd_pot;
	m_strc.smd0 = &smd_pot0;
	
	
	m_strc.rlflags[M_R_UD] = BLF_R_UD;
	m_strc.rlflags[M_R_8] = BLF_R_8;
	m_strc.rlflags[M_R_16] = BLF_R_16;
	m_strc.rlflags[M_R_32] = BLF_R_32;
	m_strc.rlflags[M_R_64] = BLF_R_64;
	m_strc.rlflags[M_R_BH] = BLF_R_PART;

	m_strc.symflags[M_SYB_UD] = BLF_SYB_UD;
	m_strc.symflags[M_SYB_SECT] = BLF_SYB_SECT;
	m_strc.symdom[M_SYB_LCL]		= BLF_SYB_D_LOCL;
	m_strc.symdom[M_SYB_GBL]		= BLF_SYB_D_GLBL;
	m_strc.symdom[M_SYB_BSPACE]		= BLF_SYB_D_BSPACE;
}
