# include "as.h"
#include "../crypt.h"
# include "../string.h"
# include "../ffly_def.h"
void tun_hash_init(struct hash *__table) {
	__table->table = (hash_entryp*)turn_al(0x100*sizeof(struct hash_entry*));
	struct hash_entry **p = __table->table;
	struct hash_entry **bot = p+0x100;
	while(p != bot)
		*(p++) = NULL;
}

struct hash_entry static*
lookup(struct hash *__table, _8_u const *__key, _int_u __len) {
	_64_u sum = y_hash64(__key, __len);
	struct hash_entry *entry = *(__table->table+(sum&0xff));
	while(entry != NULL) {
		if (entry->len == __len) {
			if (!mem_cmp(entry->key, __key, __len)) return entry;
		}
		entry = entry->next;
	}
	return NULL;
}

void tun_hash_pgi(struct hash *__table, _8_u const *__key, _int_u __len, struct hep_struc *__struc) {
	hash_entryp entry;
	if ((entry = lookup(__table, __key, __len)) != NULL) {
		__struc->exist = 0;
		__struc->d = &entry->hed;
		return;
	}
	
	_64_u sum = y_hash64(__key, __len);
	struct hash_entry **table = __table->table+(sum&0xff);

	entry = (hash_entryp)turn_al(sizeof(struct hash_entry)); 
	entry->next = *table;
	*table = entry;

	entry->key = turn_memdup((void*)__key, __len);
	entry->len = __len;
	__struc->d = &entry->hed;
	__struc->exist = -1;
}

void tun_hash_put(struct hash *__table, _8_u const *__key, _int_u __len, void *__p) {
	struct hep_struc struc;
	tun_hash_pgi(__table, __key, __len, &struc);
	struc.d->p = __p;
}

void* tun_hash_get(struct hash *__table, _8_u const *__key, _int_u __len) {
	hash_entryp entry = lookup(__table, __key, __len);
	if (!entry) return NULL;
	return entry->hed.p;
}

