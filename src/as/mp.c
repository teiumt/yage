# include "mp.h"
# include "../string.h"
static struct f_as_mp mp;
void f_as_mp_init(void) {
	mem_set(mp.tbl, 0, F_AS_MP_TZ*sizeof(struct f_as_mpent*));
}

static struct f_as_mpent*
lookup(_8_u *__p, _int_u __len) {
	struct f_as_mpent *ent = *(mp.tbl+(__len>>F_AS_SHARP));
	while(ent != NULL) {
		if (ent->len == __len) {
			if (!mem_cmp(ent->ident, __p, __len)) return ent;
		}
		ent = ent->next;
	}
	return NULL;
}

void f_as_mp_put(_8_u *__p, _int_u __len, _ulonglong __d) {
	struct f_as_mpent *ent;
	ent = (struct f_as_mpent*)turn_al(sizeof(struct f_as_mpent));

	struct f_as_mpent **p = mp.tbl+(__len>>F_AS_SHARP);

	ent->next = *p;
	*p = ent;
	ent->data = __d;
	ent->ident = turn_memdup(__p, __len);
	ent->len = __len;
}

void f_as_mp_xput(struct f_as_mpent_s *__s, _int_u __n) {
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		struct f_as_mpent_s *s = __s+i;
		f_as_mp_put(s->p, s->len, s->data);
	}
}
_ulonglong f_as_mp_get(_8_u *__p, _int_u __len) {
	struct f_as_mpent *ent;
	ent = lookup(__p, __len);

	if (!ent)
		return NULL;
	return ent->data;
}
