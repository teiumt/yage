#ifndef __f__as__symb__struc__h
#define __f__as__symb__struc__h
#include "as.h"
#define symbol f_as_symbol
#define symbolp f_as_symbolp
typedef struct f_as_symbol {
	struct f_as_symbol *next;

	void *p;
	_int_u len;
	_32_u id, pm;
	_8_u flags;
	_8_u sign:1;
	_8_u sort:4;

	_8_u type;

	f_as_wap w;
	f_as_wap *w_buf[64];
	f_as_wap **w_bp;
	_16_u off;
} *f_as_symbolp;
#endif /*__f__as__symb__struc__h*/
