# include "as.h"
# include "../string.h"
# include "../ffly_def.h"
# include "../system/string.h"
# include "../elf.h"
# include "../io.h"
# include "../m_alloc.h"
# include "../remf.h"
# include "../linux/unistd.h"
# include "../assert.h"
struct flask *fak_;
symbolp static sy;

f_as_blobp static b;
void static
_macro(void) {
	if (!str_cmp(sy->p, "define")) {
		printf("got: macro\n");

	}
}

void static
_label(struct f_as_cg *__cg) {
	b = __cg->b;
	f_as_symbolp s;

	s = f_as_symb(b->p, b->len, &f_ass.symbols);
	/*
		a where abouts struct is needed
	*/
	if ((s->flags&F_AS_SY_HID)>0)
		s->w = f_as_wa_new();
	F_assert(s->w != NULL);
	s->w->adr = f_ass.v.off;
	printf("######## ADR: %u, %p\n", s->w->adr, s->w);
	s->flags |= F_AS_SY_FND;

	f_as_wap **cur;
	cur = s->w_buf;
	while(cur != s->w_bp) {
		printf("fixing linkage, %p\n", cur);
		**(cur++) = s->w;
	}

	ff_as_fdone();
	struct hash *h;
	h = (struct hash*)m_alloc(sizeof(struct hash));
	tun_hash_init(h);
	f_ass.localsymbs = h;
}

void
urek_text_asmbl(void) {
	f_ass.funcs.f_label.func = _label;

	while((b = f_as_parse()) != NULL) {
		if (b->id&(BLOB_DIR|BLOB_LABEL)) {
			b->cg.f->func(&b->cg);
		} else if (b->id == BLOB_INS){
			printf("--| %s:%u:%c\n", b->p, b->len, *(char*)b->p);
			struct flask fak;
			if ((f_ass.op_tray = b->op_tray) != NULL) {
				fak.n = 0;

				f_as_chompp cur;
				void **p = fak.p;
				_16_u info;
				if (!b->exp)
					goto _sk;
				else
					cur = b->exp->ch;
				while(cur != NULL) {
					if (is_chreg(cur)) {
						f_as_regp reg = (f_as_regp)cur->_0;
						struct f_as_reginfo *reginfo = (struct f_as_reginfo*)reg->priv;
						printf("-- reg, %u\n", reginfo->l);
						switch(reginfo->l) {
							case 1:
								info = _o_reg8;
							break;
							case 2:
								info = _o_reg16;
							break;
							case 4:
								info = _o_reg32;
							break;
							case 8:
								info = _o_reg64;
							break;
						}
						*p = reg->priv;
					} else if (is_chint(cur)) {
						printf("-- imm.\n");
						*p = cur->_0;
						_64_u val = cur->_0;
   						if (!(val&0xffffffffffffff00))
							info = _o_imm8;
						else if (!(val&0xffffffffffff0000))
							info = _o_imm16;
						else if (!(val&0xffffffff00000000))
							info = _o_imm32;
						else
							info  = _o_imm64;
						printf("val: %u\n", val);
					} else if (is_chsy(cur)) {
						*p = (void*)cur->_0;
						info = _o_label;
					}

					setinfo(&fak, info, p-fak.p);
					p++;
					cur = cur->next;
					fak.n++;
				}
			_sk:
				printf("operands: %u\n", fak.n);
				*p = NULL;
				fak_ = &fak;
				printf("FAK_INFO: %u.\n", fak.info[0]);
				/*
					post the instruction and output processer will take care of the rest
				*/
				f_ass.ins_post();
			} else {
				printf("unknown operation.\n");
			}
		}
	}
_r:
	return;
}

