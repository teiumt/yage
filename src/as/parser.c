#include "as.h"
#include "../io.h"
#include "../string.h"
#define _ 0xff
/*
	table for non-ins expressions
	e.g.
	.region *21 <----

	* will only work here not 
	
	movq *21
	if it did it would work diffrently
	only an example not used not here
*/
/*
	_0t and _1t for for diffrent expression undertaken
*/
_8_u static _0t[0x100] = {
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _f_ae_plus_01,  _f_ae_comma_01,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};
/*
	table for ins expressions
*/
_8_u static _1t[0x100] = {
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _f_ae_hash_1,  _f_ae_dollar_1,  _f_ae_percent_1,  _,  _,  _f_ae_left_paren_1,  _f_ae_right_paren_1,  _,  _f_ae_plus_01,  _f_ae_comma_01,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};

_8_u static tbl[0x100] = {
	_,  _,  _,  _,  _,  _,  _,  _,  _,  0,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	0,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  BLOB_DIR,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,
	_,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _,  _
};
struct syinfo {
	char const *name;
};

static struct syinfo _si[] = {
	{"SYMBOL"},
	{"INT"},
	{"REG"},
	{"UNKNOWN"}
};

f_as_blobp f_as_parse(void) {
	turn_strc.sp = turn_strc.stack;
	turn_linep ln;
	ln = turn_nextline();
	if (!ln)
		return NULL;
	if (!ln->len)
		return NULL;
	char *p = ln->p;
	_8_u register *_tp;//table ptr
	_int_u len;
	char c;
	c = *p;
	while(!tbl[c]) {
		c = *++p;
	}
	char buf[128], *bufp;
	bufp = buf;
	char *s = NULL;
	f_as_blobp b = (f_as_blobp)turn_al(sizeof(struct f_as_blob));
	b->cg.b = b;
	b->exp = NULL;
	/*
		directive char lookup
	*/
	b->id = tbl[*p];
	if (b->id != 0xff) {
		p++;
		if (b->id == BLOB_DIR) {
			c = *p;
			while((c>='a'&&c<='z')||(c>='A'&&c<='Z')) {
				*(bufp++) = c;
				c = *++p;
			}
			*bufp = '\0';
			struct f_as_dirv *d;
			d = f_as_d_lookup(buf, bufp-buf);
			if (!d) {
				printf("theres no such directive as '%s'.\n", buf);
				while(1);
			}	
		
			b->cg.f = &d->f;
		} else if (b->id == BLOB_MAC) {
			c = *p;
			while(c>='a'&&c<='z') {
				*(bufp++) = c;
				c = *++p;
			}	
		}
		_tp = _0t;
	} else {
		/*
			if all is at a lose then it must be an instruction or a label
		*/
		c = *p;
		while((c >= 'a' && c <= 'z') || c == '_' || (c >= '0' && c <= '9')) {
			*(bufp++) = c;
			c = *++p;
		}
		/*
			try to take the suffix if said suffix exists then remove it from instruction ident/string
		*/
		if (bufp>buf+1) {
			_8_u cv;
			f_ass.suffix = f_ass.suffix_map[*(bufp-1)];
			f_ass.suffix^=(cv=f_ass.chisel_map[*(bufp-2)]);
			if (f_ass.suffix != 0xff) {
				bufp--;
				if (cv != 0){
					bufp--;
				}
			}
		}
	
		*bufp = '\0';

		b->p = turn_memdup(buf, (len = (bufp-buf))+1);
		printf("---->%s.\n", b->p);
		b->len = len;

		if (*p == ':') {
			b->id = BLOB_LABEL;
			b->cg.f = &f_ass.funcs.f_label;
			goto _ret;
		} else {
			b->id = BLOB_INS;	
			b->op_tray = tun_hash_get(&f_ass.env, b->p, b->len);
		}
		_tp = _1t;
	}

	_int_u ld = p-(char*)ln->p;
	if (!(ln->len-ld)) {
		ln = ln->next;
		ld = 0;
	}
	if (ln != NULL) {
		b->exp = f_as_eval(ln, _tp, ld);
	
		f_as_chompp cur;
		cur = b->exp->ch;
		while(cur != NULL) {
			printf(":: %s\n", _si[cur->sort].name);
			if (cur->sort == SY_INT) {
				printf("INT: %u.\n", *(_64_u*)cur->_0);
			}
			cur = cur->next;
		}
	}
_ret:
	return b;
}
