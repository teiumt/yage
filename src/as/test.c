#include "../y_int.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
struct frag {
	_int_u sz;
	_int offset;
	_int label;

	struct frag *dst, *link;
	
	_int m, k;
	_int m0, k0;
	char const *ident;
	_int u;
};

#define FN 8
struct frag frags[] = {
	{20, 0, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31ma\e[0m", 0},
	{20, 20, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31mb\e[0m", 0},
	{20, 40, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31mc\e[0m", 0},
	{20, 60, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31md\e[0m", 0},
	{20, 80, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31me\e[0m", 0},
	{20, 100, 0, NULL, NULL, 0, 0, 0, 0,"\e[1;31mf\e[0m", 0},
	{20, 120, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31mg\e[0m", 0},
	{20, 140, 0, NULL, NULL, 0, 0, 0, 0, "\e[1;31mh\e[0m", 0}
};

_int_u __n(long long __val) {
	_int_u i;
	i = 0;
	while((__val&(0xffffffffffffffff<<i))>0)
		i++;
	return i;
}

void
prb(long long __val, _8_u __b) {
	_8_u i;
	_int_u d, offset;
	char buf[128];

	offset = 0;
	i = 0;
	for(;i != __b;i++) {
		if (((i-d)>>2)) {
			d+=(i-d);
			buf[i+offset] = ' ';
			offset++;
		}
		buf[i+offset] = '0'+((__val>>((__b-1)-i))&0x0000000000000001);
	}
	*(buf+i+offset) = '\0';
	printf("%s", buf);
}

int main(void) {
	struct frag *a0, *a1, *b0, *b1;
	a0 = frags;
	a1 = frags+2;
	b0 = frags+7;
	b1 = frags+6;

	a0->dst = b0;
	a1->dst = b1;
	a0->label = 20;
	a1->label = 20;

	_int_u m, k;
	struct frag *f;
	_int_u i;

	i = 0;
	while(i != FN) {
		if ((f = frags+(i++))->dst != NULL) {
			f->link = f->dst->link;
			f->dst->link = f;
		}
	}

	m = 0;
	k = 0;
	i = 0;
	while(i != FN) {
		f = frags+(i++);
		f->m = m;
		f->k = k;
		if (f->dst != NULL) {
			_int dis;
			dis = (f->dst->offset+f->label)-f->offset;
			if (dis<0)
				dis = -dis;
			_int_u n;
			n = __n(dis);
			m+=(n+((1<<3)-1))>>3;
		}
	}

	for(i = 0;i != FN;i++) {
		f = frags+i;
		printf("%s->%s: M: %u, K: %u\n", f->ident, !f->dst?"X":f->dst->ident, f->m, f->k);

	}
}
