# include "as.h"
# include "../string.h"
# include "../m_alloc.h"
# include "../remf.h"
# include "../io.h"
symbolp sy_head = NULL;

void f_as_sbi(f_as_symbolp __s, _8_u *__ident, _int_u __len) {
	__s->flags = 0x00;
	__s->w_bp = __s->w_buf;
	__s->p = turn_memdup(__ident, __len);
	__s->len = __len;
	__s->w = NULL;
}

f_as_symbolp
f_as_symb_new(_8_u *__ident, _int_u __len) {
	f_as_symbolp s;
	printf("SYMBOL CREATE.\n");
	s = (f_as_symbolp)m_alloc(sizeof(struct f_as_symbol));
	f_as_sbi(s, __ident, __len);
	s->next = sy_head;
	sy_head = s;

	return s;
}

f_as_symbolp f_as_symb(_8_u *__ident, _int_u __len, struct hash *__h) {
	f_as_symbolp s;
	struct hep_struc struc;
	tun_hash_pgi(__h, __ident, __len, &struc);
	s = (f_as_symbolp)struc.d->p;
	if (struc.exist == -1) {
		s = f_as_symb_new(__ident, __len);
		struc.d->p = s;
		s->flags |= F_AS_SY_HID;
	}
	return s;
}

f_as_symbolp f_as_symb_n(_8_u *__ident, _int_u __len, struct hash *__h, void *__p) {
	struct hep_struc struc;
	tun_hash_pgi(__h, __ident, __len, &struc);
	f_as_symbolp s;
	s = f_ass.symb_new(__p);
	f_as_sbi(s, __ident, __len);
	struc.d->p = s;
	return s;
}


_16_u static off = 0;
symbolp ff_as_syt(_8_u *__ident, _int_u __len) {
}

_64_u syt_dst;

