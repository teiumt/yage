# include "../y_int.h"
# include "../tmh.h"
# include "../types.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../linux/stat.h"
# include "../system/io.h"
int static fd;
void static _out(void *__buf, _int_u __size) {
	write(fd, __buf, __size);
}

_f_err_t ffmain(int __argc, char const *__argv[]) {
	if (__argc<=1) {
		printf("missing argument.\n");
		return -1;
	}
	fd = open(__argv[1], O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);

	f_tmh_file_glob.format(_out);

	close(fd);
}
