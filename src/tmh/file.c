# include "../tmh.h"
# include "../ffly_def.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../memory/mem_realloc.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../linux/stat.h"
# include "../system/io.h"
# include "../msg.h"
# include "../dep/mem_set.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_TMH)
/*
	TODO:
		rename to an real name and push file e.g. open, close, write with file disc to another place

	TODO:
		if a record is dumped then cleanup struct will need to be removed
*/

#define CU_RECORD 0
struct cleanup_s {
	_8_u id;
	void *p;
	struct cleanup_s *link;
};

struct _msit {
	_64_u si;
	_int_u n;
};
struct _rec {
	_64_u m, idx;
};

struct _mapping {
	_int_u size;
	_64_u slabs;
	_int_u slab_c;
};

#define MAG0 'T'
#define MAG1 'M' 
#define MAG2 'H'

#define H_INIT 0x01
struct header {
	_64_u recs;
	_int_u n_rec;
	_64_u off;
	_64_u m;
	_64_u n_maps;

	_64_u slab_tbl;
	_64_u slab_c;

	_int_u sc_off;

	_64_u msi;
	_8_u flags;
	char ident[3];
};

#define PAGE_SHFT 4
#define PAGE_SIZE (1<<PAGE_SHFT)
#define PAGE_MASK (PAGE_SIZE-1)
struct slab;
struct record;
struct slab_crap {
	struct slab **slabs;
	_int_u page_c, off;
};
struct node;
struct mapping;
typedef struct priv_ctx {
	struct mapping *m;
	struct slab *top, *bin;
	_int_u off;
	struct node *n;
	int fd;
	f_tmh_capp c;
	_int_u slab_c;
	struct slab_crap sc;
	struct cleanup_s *cu;
} *priv_ctxp;

struct mapping {
	struct slab **slabs;
	_int_u slab_c;
	_64_u off, size;
	struct mapping *next;
};

struct record {
	struct cleanup_s *cu;
	_int_u idx;
	struct mapping *m;
};

struct node {
	struct record *r;
	struct mapping *m;
	struct node *next;
};

#define _SL_PIB 0x01
#define SL_FREE 0x01
struct _slab {
	_64_u off;
	_64_u idx;
	_8_u flags;
};

typedef struct slab {
	_64_u off, idx, flags;
	struct slab *link, *next;
} *slabp;

void static
_initspage(struct slab *__page, _int_u __pg_off) {
	struct slab *s;
	_int_u i;
	i = 0;
	for(;i != PAGE_SIZE;i++) {
		s = __page+i;
		s->flags = 0x00;
		s->idx = __pg_off+i;
	}
}

void static cu_push(priv_ctxp __ctx, void *__p, _8_u __id) {
	struct cleanup_s *cus;
	cus = (struct cleanup_s*)__f_mem_alloc(sizeof(struct cleanup_s));

	cus->link = __ctx->cu;
	__ctx->cu = cus;
	cus->p = __p;
	cus->id = __id;
}

static slabp
slabget(priv_ctxp __ctx, _int_u __off) {;
	_int_u pg, pg_off;
	pg = __off>>PAGE_SHFT;
	pg_off = __off&PAGE_MASK;
	struct slab_crap *sc;
	sc = &__ctx->sc;
	if (pg>=sc->page_c) {
		_int_u _pg;
		_pg = sc->page_c;
		sc->page_c = pg+1;
		sc->slabs = (struct slab**)__f_mem_realloc(sc->slabs, sc->page_c*sizeof(struct slab*));
		while(_pg != sc->page_c) {
			_initspage(*(sc->slabs+_pg) = (struct slab*)__f_mem_alloc(PAGE_SIZE*sizeof(struct slab)), _pg<<PAGE_SHFT);
			_pg++;
		}
	}
	return sc->slabs[pg]+pg_off;
}

#define SLAB_SHFT 4
#define SLAB_SIZE (1<<SLAB_SHFT)
#define SLAB_MASK (SLAB_SIZE-1)
void static
slab_alloc_m(priv_ctxp __ctx, slabp *__slabs, _int_u __n) {
	_int_u i;
	i = 0;
	slabp sb;
	for(;i != __n;i++) {
		sb = slabget(__ctx, __ctx->sc.off++);
		MSG(INFO, "slab-(%p) allocated with id: %u, off: %u.\n", sb, __ctx->sc.off-1, __ctx->off);

		sb->off = __ctx->off;
		__ctx->off+=SLAB_SIZE;

		sb->next = __ctx->top;
		__ctx->top = sb;
		__slabs[i] = sb;
		__ctx->slab_c++;
	}
}

slabp
slab_alloc_s(priv_ctxp __ctx) {
	slabp sb;
	slab_alloc_m(__ctx, &sb, 1);
	return sb;
}

void static
slab_free(slabp __sb, priv_ctxp __ctx) {
	__sb->link = __ctx->bin;
	__ctx->bin = __sb;
}

void static*
ctx_new(f_tmh_capp __c) {
	priv_ctxp ct;
	ct = (priv_ctxp)__f_mem_alloc(sizeof(struct priv_ctx));
	ct->c = __c;

	return ct;
}

void static
ctx_destory(priv_ctxp  __ctx) {
	__f_mem_free(__ctx);
}

void static
init(priv_ctxp __ctx) {
	__ctx->off = sizeof(struct header);
	__ctx->m = NULL;
	__ctx->top = NULL;
	__ctx->bin = NULL;
	__ctx->n = NULL;
	__ctx->cu = NULL;
	__ctx->fd = open("tmh.dat", O_RDWR|O_CREAT, S_IRUSR|S_IWUSR);
	__ctx->sc.off = 0;
	__ctx->sc.page_c = 1;
	__ctx->slab_c = 0;
	__ctx->sc.slabs = (slabp*)__f_mem_alloc(sizeof(slabp));
	_initspage(*__ctx->sc.slabs = (slabp)__f_mem_alloc(PAGE_SIZE*sizeof(struct slab)), 0);
}

void static
deinit(priv_ctxp __ctx) {
	struct mapping *m, *b;
	m = __ctx->m;
	while(m != NULL) {
		m = (b = m)->next;
		__f_mem_free(b->slabs);
		__f_mem_free(b);
	}

	slabp *sb_pg, *sp_e;
	sb_pg = __ctx->sc.slabs;
	sp_e = sb_pg+__ctx->sc.page_c;
	while(sb_pg != sp_e) {
		__f_mem_free(*sb_pg);
		sb_pg++;
	}
	__f_mem_free(__ctx->sc.slabs);

	struct cleanup_s *cu, *_cu;
	_cu = __ctx->cu;
	while(_cu != NULL) {
		_cu = (cu = _cu)->link;
		switch(cu->id) {
			case CU_RECORD:
				__f_mem_free(cu->p);
			break;
		}
		__f_mem_free(cu);
	}

	close(__ctx->fd);
}
#define headerprint(__h)\
	MSG(INFO, "HEADER: records: %u, n_records: %u, off: %u, m: %u, n_maps: %u, slab_tbl: %u, slab_c: %u, sc_off: %u, msi: %u, magic_ident{%c, %c, %c}\n",\
	__h.recs, __h.n_rec, __h.off, __h.m, __h.n_maps, __h.slab_tbl, __h.slab_c, __h.sc_off, __h.msi, __h.ident[0], __h.ident[1], __h.ident[2])
#define mprint(__m)\
	printf("M: size: %u, slabs: %u, slab_c: %u.\n", __m.size, __m.slabs, __m.slab_c);
void static
save(priv_ctxp __ctx) {
	MSG(INFO, "saving.\n")
	struct header h;
	h.ident[0] = MAG0;
	h.ident[1] = MAG1;
	h.ident[2] = MAG2;

	h.off = __ctx->off;
	h.sc_off = __ctx->sc.off;
	int fd = __ctx->fd;
	_int_u i;
	_64_u off = __ctx->off;
	lseek(fd, off, SEEK_SET);
	h.slab_tbl = off;
	i = 0;
	slabp s;
	s = __ctx->top;
	while(s != NULL) {
		struct _slab _s;
		_s.flags = 0x00;
		if ((s->flags&SL_FREE)>0)
			_s.flags |= _SL_PIB;
		_s.off = s->off;
		_s.idx = s->idx;
		write(fd, &_s, sizeof(struct _slab));
		i++;
		s = s->next;
	}

	h.slab_c = i;
	off+=i*sizeof(struct _slab);

	struct si {
		_64_u *sidx;
		_int_u n;
		struct si *next;
	};

	struct si *si_top = NULL;
	_int_u ii;
	i = 0;
	struct mapping *m;
	struct _mapping _m;
	_int_u sti = 0;
	m = __ctx->m;
	h.m = off;
	while(m != NULL) {
		_m.slab_c = m->slab_c;
		sti+=m->slab_c;
		printf("#%u.\n", __ctx->slab_c-sti);
		_m.slabs = __ctx->slab_c-sti;
		_m.size = m->size;
		ii = 0;
		_64_u *sidx = (_64_u*)__f_mem_alloc(m->slab_c*sizeof(_64_u));
		for(;ii != m->slab_c;ii++) {
			s = m->slabs[ii];
			sidx[ii] = s->idx;
		}
		
		struct si *si_s;
		si_s = (struct si*)__f_mem_alloc(sizeof(struct si));
		si_s->sidx = sidx;
		si_s->n = ii;
		si_s->next = si_top;
		si_top = si_s;

		write(fd, &_m, sizeof(struct _mapping));

		m->off = i;

		off+=sizeof(struct _mapping);
		m = m->next;
		mprint(_m);
		i++;
	}
	h.n_maps = i;

	struct _msit _msi;
	_msi.si = off;
	_int_u tot;
	tot = 0;
	struct si *_si, *_bk;
	_si = si_top;
	ii = 0;
	while(_si != NULL) {
		_int_u sz;
		sz = _si->n*sizeof(_64_u);
		write(fd, _si->sidx, sz);	
		tot+=sz;
		__f_mem_free(_si->sidx);
		_si = (_bk = _si)->next;
		__f_mem_free(_bk);
	}
	off+=tot;
	h.msi = off;
	_msi.n = tot;
	write(fd, &_msi, sizeof(struct _msit));
	off+=sizeof(struct _msit);

	struct _rec _r;
	struct node *n;
	n = __ctx->n;
	h.recs = off;

	 i = 0;
	while(n != NULL) {
		_r.m = n->m->off;
		_r.idx = n->r->idx; 
		write(__ctx->fd, &_r, sizeof(struct _rec));
		i++;
		n = n->next;
	}

	h.n_rec = i;
	headerprint(h);
	h.flags = H_INIT;
	pwrite(__ctx->fd, &h, sizeof(struct header), 0);
}

void static
load(priv_ctxp __ctx) {
	MSG(INFO, "loading.\n")
	int fd = __ctx->fd;
	struct header h;
	pread(fd, &h, sizeof(struct header), 0);
	headerprint(h);
	struct _msit _msi;
	_int_u i, ii;
	_64_u *msi_table;
	struct mapping **maps;
	if (h.ident[0] != MAG0) {
		goto _sk;
	}

	if (h.ident[1] != MAG1) {
		goto _sk;
	}

	if (h.ident[2] != MAG2) {
		goto _sk;
	}

	if (!(h.flags&H_INIT)) {
		MSG(INFO, "file not initalized skipping loading process.\n")
		return;
	}

	__ctx->slab_c = h.slab_c;
	__ctx->off = h.off;
	__ctx->sc.off = h.sc_off;
	if (h.slab_c>0) {
		lseek(fd, h.slab_tbl, SEEK_SET);
		struct _slab *_stbl, *_s;
		_int_u size;
		_stbl = (struct _slab*)__f_mem_alloc(size = (h.slab_c*sizeof(struct _slab)));
		read(fd, _stbl, size);
		i = 0;
		struct slab *s;
		for(;i != h.slab_c;i++) {
			_s = _stbl+i;
			s = slabget(__ctx, _s->idx);
			s->next = __ctx->top;
			__ctx->top = s;
			if ((_s->flags&_SL_PIB)>0) {
				s->flags |= SL_FREE;
				s->link = __ctx->bin;
				__ctx->bin = s;
			}
			s->off = _s->off;
		}
	}

	pread(fd, &_msi, sizeof(struct _msit), h.msi);
	if (_msi.n>0) {
		msi_table = (_64_u*)__f_mem_alloc(_msi.n);
		pread(fd, msi_table, _msi.n, _msi.si);
	}

	if (h.n_maps>0) {
		maps = (struct mapping**)__f_mem_alloc(h.n_maps*sizeof(struct mapping*));
		lseek(fd, h.m, SEEK_SET);
		struct _mapping _m;
		i = 0;
		for(;i != h.n_maps;i++) {
			struct mapping *m;
			maps[i] = m = (struct mapping*)__f_mem_alloc(sizeof(struct mapping));
			read(fd, &_m, sizeof(struct _mapping));
			mprint(_m);
			m->slabs = (slabp*)__f_mem_alloc(_m.slab_c*sizeof(slabp));
	
			ii = 0;
			for(;ii != _m.slab_c;ii++) {
				m->slabs[ii] = slabget(__ctx, msi_table[_m.slabs+ii]);
			}
			m->slab_c = _m.slab_c;
			m->size = _m.size;
			m->next = __ctx->m;
			__ctx->m = m;
		}
	}

	if (h.n_rec>0) {
		struct _rec _r;
		lseek(fd, h.recs, SEEK_SET);
		_int_u i;
		i = 0;
		for(;i != h.n_rec;i++) {
			read(fd, &_r, sizeof(struct _rec));
			struct mapping *m;
			m = maps[_r.m];
			struct record *r;
			r = (struct record*)__f_mem_alloc(sizeof(struct record));
			r->idx = _r.idx;

			struct node *n;
			n = (struct node*)__f_mem_alloc(sizeof(struct node));
			n->next = __ctx->n;
			__ctx->n = n;
			n->m = m;
			n->r = r;

			struct f_tmh_recinfo ri;
			ri.a_size = m->size;
			ri.idx = r->idx;
			ri.a_priv = m;

		//	MSG(INFO, "RECORD: off: %u, size: %u, index: %u.\n", _r.off, _r.size, _r.idx)
			__ctx->c->bg->rec_new(__ctx->c, r, &ri);

		}
		
	}	
	
	return;
_sk:
	printf("file corroption.\n");
}

void static*
rec_new(priv_ctxp __ctx, _int_u __idx) {
	struct record *r;
	r = (struct record*)__f_mem_alloc(sizeof(struct record));
	cu_push(__ctx, r, CU_RECORD);
	r->idx = __idx;
//	r->cu = cu;

	return r;
}

void static
rec_destroy(priv_ctxp __ctx, struct record *__rec) {

}

void static
bound(priv_ctxp __ctx, struct mapping  *__map, struct record *__rec) {
	__rec->m = __map;

	struct node *n;
	n = (struct node*)__f_mem_alloc(sizeof(struct node));
	n->m = __map;
	n->r = __rec;
	n->next = __ctx->n;
	__ctx->n = n;
	MSG(INFO, "record-%u is now bound to mapping-(%p) with %u-slabs at a size of %u-bytes in total (%u*%u)=%u-bytes.\n",
		__rec->idx, __map, __map->slab_c, __map->size, __map->slab_c, SLAB_SIZE, __map->slab_c*SLAB_SIZE)
}

void static*
_map(priv_ctxp __ctx, _int_u __size) {
	MSG(INFO, "mapping.\n")
	struct mapping *m;
	m = (struct mapping*)__f_mem_alloc(sizeof(struct mapping));
	_int_u sc;
	sc = (__size+(SLAB_SIZE-1))>>SLAB_SHFT;
	m->slabs = (slabp*)__f_mem_alloc(sc*sizeof(slabp));
	slab_alloc_m(__ctx, m->slabs, sc);
	m->slab_c = sc;
	m->size = __size;
	m->next = __ctx->m;
	__ctx->m = m;
	return m;
}

void static 
_unmap(priv_ctxp __ctx, struct mapping *__map) {

}


void static
_write(priv_ctxp __ctx, struct mapping *__map, void *__buf, _int_u __size, _int_u __offset) {
	_int_u sb_off;
	sb_off = __offset&SLAB_MASK;
	_8_u *p;
	p = (_8_u*)__buf;
	int fd;
	fd = __ctx->fd;
	struct slab **s, **e;
	s = __map->slabs+(__offset>>SLAB_SHFT);
	e = s+(__size>>SLAB_SHFT);
	_int_u am;
	if (sb_off>0) {
		if (__size>(am = (SLAB_SIZE-sb_off))) {
			pwrite(fd, p, am, (*s)->off+sb_off);
			p+=am;
			sb_off = 0;
			s++;
		} else {
			pwrite(fd, p, __size, (*s)->off+sb_off);
			return;
		}
	}

	while(s<e) {
		pwrite(fd, p, SLAB_SIZE, (*s)->off);
		p+=SLAB_SIZE;
		s++;
	}

	_int_u left;
	left = __size-(p-(_8_u*)__buf);	
	if (left>0) {
		pwrite(fd, p, left, (*s)->off);
	}
}

void static
_read(priv_ctxp __ctx, struct mapping *__map, void *__buf, _int_u __size, _int_u __offset) {
	_int_u sb_off;
	sb_off = __offset&SLAB_MASK;

	_8_u *p;
	p = (_8_u*)__buf;
	int fd;
	fd = __ctx->fd;
	struct slab **s, **e;
	s = __map->slabs+(__offset>>SLAB_SHFT);
	e = s+(__size>>SLAB_SHFT);
	_int_u am;//am ount
	if (sb_off>0) {
		if (__size>(am = (SLAB_SIZE-sb_off))) {
			pread(fd, p, am, (*s)->off+sb_off);
			p+=am;
			sb_off = 0;
			s++;
		} else {
			pread(fd, p, __size, (*s)->off+sb_off);
			return;
		}
	}

	while(s<e) {
		pread(fd, p, SLAB_SIZE, (*s)->off);
		p+=SLAB_SIZE;
		s++;
	}

	_int_u left;
	left = __size-(p-(_8_u*)__buf);
	if (left>0) {
		pread(fd, p, left, (*s)->off);
	}
}

void static
_test(priv_ctxp __ctx) {
	return;
	slabp s;
	s = __ctx->top;
	while(s != NULL) {
		printf("slab: off: %u, flags: %u, idx: %u\n", s->off, s->flags, s->idx);
		s = s->next;
	}

	struct mapping *m;
	m = __ctx->m;
	while(m != NULL) {
		printf("mapping: slab_c: %u, size: %u\n", m->slab_c, m->size);
		_int_u i;
		i = 0;
		for(;i != m->slab_c;i++) {
			s = m->slabs[i];
			printf("slab: off: %u, flags: %u, idx: %u.\n", s->off, s->flags, s->idx);
		}

		m = m->next;
	}
}

void static
_format(void(*__out)(void*, _int_u)) {
	struct header h;
	f_mem_set(&h, 0, sizeof(struct header));
	*h.ident = MAG0;
	h.ident[1] = MAG1;
	h.ident[2] = MAG2;
	h.flags = 0x00;
	__out(&h, sizeof(struct header));
}

struct f_tmh_glob f_tmh_file_glob = {
	.format = _format
};
/*
	why do i do this like this why not just globalize the structure itself??
	because i like that fact that we have space to prep stuff with (f_tmh_file())
	and also stops us messing up the structure

	why not just add another func pointer to struct for prep???
	because its requirest a deref and f_tmh_file is a straight call
	no overhead.

	isent calling a function just to get a structure overhead?
	yes buts its setup shit & person calling can cache it in there 
	own glob making no over head for runtime only init
*/
static struct f_tmh_ops op = {
	.ctx_new = (void*)ctx_new,
	.ctx_destory = (void*)ctx_destory,
	.init = (void*)init,
	.deinit = (void*)deinit,
	.save = (void*)save,
	.load = (void*)load,
	.rec_new = (void*)rec_new,
	.rec_destroy = (void*)rec_destroy,
	.bound = (void*)bound,
	.map = (void*)_map,
	.unmap = (void*)_unmap,
	.write = (void*)_write,
	.read = (void*)_read,
	.test = (void*)_test
};

void f_tmh_file(f_tmh_opsp __op) {
	*__op = op;
}
