# include "lld.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "linux/stat.h"
# include "mutex.h"
F_DEF_MUTEX(static lock)
int static fd;
void f_lld_init(void) {
	fd = open("lld.out", O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
}

void f_lld_de_init(void) {
	close(fd);
}

# include "strf.h"
void f_lld_out(char const *__format, ...) {
	char buf[256];
	va_list args;
	_int_u n;
	va_start(args, __format);
	n = ffly_strfa(buf, 256, __format, args);
	va_end(args);
	mt_lock(&lock);
	write(fd, buf, n);
	mt_unlock(&lock);
}
