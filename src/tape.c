# include "tape.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "memory/mem_realloc.h"
# include "ffly_def.h"
# include "dep/mem_cpy.h"
/*
	code tape
*/

/*
	TODO:
		alloc,re,free should go thru function points if needed
*/
ffly_tapep ffly_tape_new(void) {
	ffly_tapep tape;

	tape = (ffly_tapep)__f_mem_alloc(sizeof(struct ffly_tape));
	tape->len = 0;
	tape->area = NULL;
	tape->page_c = 0;
	return tape;
}

void ffly_tape_get(ffly_tapep __tape, _int_u __where, void *__buf, _int_u __size) {
	f_mem_cpy(__buf, ((_8_u*)__tape->area)+__where, __size);
}

// rename
void ffly_tape_raze(ffly_tapep __tape) {
	void *area;

	area = __tape->area;
	if (area != NULL)
		__f_mem_free(area);
	__f_mem_free(__tape);
}

# define PAGE_SHIFT 3
# define PAGE_SIZE (1<<PAGE_SHIFT)
void ffly_tape_insert(struct ffly_tape *__tape, void *__buf, _int_u __size) {
	_int_u len;
	
	len = __tape->len+__size;
	_int_u pg, pg_c;

	pg = len>>PAGE_SHIFT;
	if (pg >= __tape->page_c) {
		pg_c = ((len+(0xffffffffffffffff>>(64-PAGE_SHIFT)))>>PAGE_SHIFT);
		if (!__tape->area)
			__tape->area = __f_mem_alloc(pg_c*PAGE_SIZE);
		else
			__tape->area = __f_mem_realloc(__tape->area, pg_c*PAGE_SIZE);
		__tape->page_c = pg_c;
	}

	f_mem_cpy(((_8_u*)__tape->area)+__tape->len, __buf, __size);
	__tape->len = len;
}
