# ifndef __f__blt__h
# define __f__blt__h
//billit
#include "y_int.h"
#include "mutex.h"
#include "hard.h"
struct blt_page {
	void *p;
	_8_u flags;
	/*
		last time since page access
		clock-decay = decay
	*/
	_64_u decay;
	mlock lock;
	struct y_hard *h;
	_int_u num;
};

typedef struct y_blt {
	struct blt_page **p;
	mlock lock;
	_int_u size, page_c;
	struct y_blt *next, **bk;
} *y_bltp;

y_bltp blt_new(void);
void blt_destroy(y_bltp);

void blt_init(void);
void blt_read(y_bltp, void*, _int_u, _32_u);
void blt_write(y_bltp, void*, _int_u, _32_u);
# endif /*__y__blt__h*/
