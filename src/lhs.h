# ifndef __f__lhs__h
# define __f__lhs__h
# include "y_int.h"
struct f_lhs {
	void *p;
_32_u offset;
};

void f_lhs_init(void);
void f_lhs_de_init(void);
struct f_lhs* f_lhs_alloc(_int_u);
void f_lhs_free(struct f_lhs*);
void f_lhs_read(struct f_lhs*, void*, _int_u, _32_u);
void f_lhs_write(struct f_lhs*, void*, _int_u, _32_u);
# endif /*__f__lhs__h*/
