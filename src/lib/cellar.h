#ifndef __yage__cellar__h
#define __yage__cellar__h
#include "../y_int.h"

struct cellar{
	void **p;
	_int_u page_c;
	_int_u off;
};

#define celr_at(__celr,__at,__pgsh,__type) (((__type*)(__celr)->p[(__at)>>__pgsh])+((__at)&((1<<__pgsh)-1)))
#define celr_overflow(__cel,__pgsh) if((__cel)->off>(__cel)->page_c<<__pgsh)

void celr_init(struct cellar*,_int_u);
void celr_more(struct cellar*,_int_u);
void celr_free(struct cellar*);
#endif/*__yage__cellar__h*/
