#include "cellar.h"
#include "../m_alloc.h"
void celr_init(struct cellar *__cel,_int_u __blksz){
	__cel->p = m_alloc(sizeof(void*));
	__cel->off = 0;
	__cel->page_c = 1;
	__cel->p[0] = m_alloc(__blksz);
}
void celr_free(struct cellar *__cel) {
	_int_u i;
	i = 0;
	for(;i != __cel->page_c;i++){
		m_free(__cel->p[i]);
	}
	m_free(__cel->p);
}
void celr_more(struct cellar *__cel,_int_u __blksz){
	_int_u pg;
	pg = __cel->page_c;
	__cel->p = (void**)m_realloc(__cel->p,(++__cel->page_c)*sizeof(void*));
	__cel->p[pg] = m_alloc(__blksz);
}
