# include <stdio.h>
# include <malloc.h>
# include "../y_int.h"

# define dot(__x0, __x1, __y0, __y1) \
		((__x0*__x1)+(__y0*__y1))

struct nt_vertex2 {
	_32_s x, y;
};

static _32_s x0, y0, z0;
static _32_s x1, y1, z1;
static _32_s x2, y2, z2;
# include <unistd.h>
char *pixels;
#define WIDTH 48
#define HEIGHT 48
void draw(void) {
	float a, b;
	float x, y;
	char *d;
	float i;

	float _x0, _y0;
	float _x1, _y1;
/*	if (y0<y1) {
		_y0 = y0;
		_y1 = y1;
	} else {
		_y0 = y1;
		_y1 = y0;
	}
	*/
	if (x0<x1) {
		_x0 = x0;
		_x1 = x1;
		_y0 = y0;
		_y1 = y1;
	} else {
		_x0 = x1;
		_x1 = x0;
		_y0 = y1;
		_y1 = y0;
	}
	a = _x1-_x0;
	b = _y1-_y0;
	x = _x0;
	y = 0;


	while(x != _x1) {
		y = _y0+((x-_x0)*b)/a;

		char *d;
		d = pixels+((_32_u)x)+(((_32_u)y)*WIDTH);
		if (*d == '.')
			*d = '#';
		printf("%f, %f\n", x, y);	
		x++;
	}

}

# include <string.h>
int main(void) {
	pixels = (char*)malloc(WIDTH*HEIGHT);
	memset(pixels, '.', WIDTH*HEIGHT);	

	x0 = 10;
	y0 = 10;

	x1 = 20;
	y1 = 20;
	pixels[x0+(y0*WIDTH)] = 'A';
	pixels[x1+(y1*WIDTH)] = 'B';

	z0 = 1;
	z1 = 0;
	z2 = 0;
	draw();
	_int_u x, y;
	y = 0;
	while(y != HEIGHT) {
		x = 0;
		printf("%u : ", y);
		while(x != WIDTH) {	
			printf("%c", *(pixels+x+(y*WIDTH)));
			x++;
		}
		printf("\n");
		y++;
	}
	free(pixels);
}
