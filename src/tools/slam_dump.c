#include "../y_int.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "../slam.h"
int main(int __argc, char const *__argv[]) {
    int fd;
    if (__argc<2) {
        printf("please provide file,\n\tusage: readremf [file].\n");
        return -1;
    }

    if (access(__argv[1], F_OK) == -1) {
        printf("file doesen't exist.\n");
        return -1;
    }

    if ((fd = open(__argv[1], O_RDONLY)) == -1) {

        return -1;
    }

	struct slam_hdr h;
	read(fd, &h, sizeof(h));

	struct slm_segment *sg;
	sg = malloc(h.ns*sizeof(struct slm_segment));
	pread(fd, sg, h.ns*sizeof(struct slm_segment), h.sg);
	printf("SEGMENTS: %u.\n", h.ns);
	int of;
	of = open("slam.out", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);

	_64_u buf = 0;
	_int_u nbts;


	_64_u buf0 = 0;
	_int_u nbts0 = 64;


	struct slm_segment *s;
	_int_u i;
	i = 0;
	for(;i != h.ns;i++) {
		nbts = 64;
		s = sg+i;
		printf("PROCESSING seg-%u, %u- %u, words: %u.\n", i, s->bytes, s->off, s->words);
		_8_u *_buf = malloc(s->bytes);
		pread(fd, _buf, s->bytes,s->off);

		_int_u j, k = 0;
		j = 0;
		for(;j != s->words;k++) {
			buf = buf>>8|((_64_u)_buf[k])<<56;
			nbts-=8;
			if(nbts<=50) {

				_64_u w = buf>>nbts;
			
				buf0 = (buf0>>14)|(w<<50);
				nbts0-=14;
				if (nbts0<=32) {
					_64_u w = buf0>>nbts0;
					write(of, &w, 4);			
					nbts0+=32;
				}
			
				nbts+=14;
				j++;
			}
		}
		free(_buf);
	}
	buf0 = buf0>>nbts0;
	write(of, &buf0, 4);

	//padding
	write(of, &buf0, 8);
	write(of, &buf0, 8);
	free(sg);
	close(of);
	close(fd);
}
