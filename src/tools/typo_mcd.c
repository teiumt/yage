# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include "../y_int.h"

struct typo_point {
	_int x, y;
};

struct glyph {
	_16_u idx;
	_32_u code;
	_int_u len;
};

struct segment {
	_16_u dst;
	_16_u src;
	_16_u size;
};

struct header {
	_32_u glyph_table;
	_int_u glyph_c;
	_int_u width, height;
	_int_u seg;
};

# define N 5
struct typo_point points[] = {
/*
	{0, 32},
	{8, 16},
	{16, 0},
	{32, 32},
	{24, 16},
	{16, 0},
	{24, 16},
	{8, 16}
*/
/*
    {29, 29},
    {16, 2},
    {2, 29},

    {6, 29},

    {10, 24}, // mid inner
//  {12, 28},

    {16, 8},

    {20, 20}, // mid inner

    {25, 29},
    {29, 29}
*/
	{29, 29},
	{29, 2},
	{2, 2},
	{2, 29},
	{29, 29}
};

int main() {
	printf("hello.\n");
	int fd;

	fd = open ("test.mcd", O_RDWR|O_TRUNC|O_CREAT, S_IRUSR|S_IWUSR);

	struct header hdr;
	hdr.glyph_table = sizeof(struct header);
	hdr.glyph_c = 1;
	hdr.width = 32;
	hdr.height = 32;

	struct segment seg;
	struct glyph g;
	g.idx = 0;
	g.code = sizeof(struct header)+sizeof(struct glyph)+(N*sizeof(struct typo_point))+sizeof(struct segment);
	g.len = (1+sizeof(_16_u)+sizeof(_16_u))+4;
	printf("film size: %u, addr: %u\n", g.len, g.code);
	hdr.seg = sizeof(struct header)+sizeof(struct glyph);
	write(fd, &hdr, sizeof(struct header));
	write(fd, &g, sizeof(struct glyph));

	seg.dst = 0;
	seg.src = hdr.seg+sizeof(struct segment);
	seg.size = N*sizeof(struct typo_point);
	write(fd, &seg, sizeof(struct segment));
	write(fd, points, N*sizeof(struct typo_point));
	_int_u i;
	_8_u op;
	op = 1;
	write(fd, &op, 1);
	op = 1;
	write(fd, &op, 1);

	op = 2;
	write(fd, &op, 1);

	_16_u n, adr;
	n = N;
	adr = 0;
	write(fd, &n, 2);
	write(fd, &adr, 2);

	op = 1;
	write(fd, &op, 1);
	op = 2;
	write(fd, &op, 1);
	close (fd);
}
