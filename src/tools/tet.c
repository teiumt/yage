# include <stdio.h>
# include <string.h>
# include <unistd.h>
# include <fcntl.h>
# include "../y_int.h"
struct point {
	_int x, y;
};

# define _abs(__val) \
	((__val)<0?-(__val):__val)

# define A 0
# define WIDTH 243
# define HEIGHT 243
//# define CPX

# ifdef CPX
# define N 7
# else
# define N 5
# endif

_8_i fix(struct point *__points, _int __x, _int __y) {
	_int_u i;

	i = 0;
	_int t0, t1, t2, t3;
	t0 = 0;
	t1 = 0;
	t2 = 0;
	t3 = 0;
	struct point *p0, *p1, *p2;
	while(i != N-1) {
		p0 = __points+(i++);
		p1 = __points+i;
//		p2 = __points+i+1;

		_int x0, y0;
		_int x, y;

		x0 = p1->x-p0->x;
		y0 = p1->y-p0->y;

		x = __x-p0->x;
		y = __y-p0->y;

		_int k0, k1;
		k0 = x*y0;
		k1 = x0*y;

		_int s;
		s = k0-k1;

		_int b0, b1, b2, b3;
		b0 = __x-p0->x;
		b1 = __x-p1->x;
		b2 = __y-p0->y;
		b3 = __y-p1->y;

//		if ((__y >= p0->y && __y <= p1->y) || (__y <= p0->y && __y >= p1->y)) {
		if ((b2 >= 0 && b3 <= 0) || (b2 <= 0 && b3 >= 0)) {
			t0+=s>0;
			t1+=s<0;
		}

//		if ((__x >= p0->x && __x <= p1->x) || (__x <= p0->x && __x >= p1->x)) {
		if ((b0 >= 0 && b1 <= 0) || (b0 <= 0 && b1 >= 0)) {
			t2+=s>0;
			t3+=s<=0;
		}

	}		
	
	if (t0-t1 < 0 && t2-t3 < 0) {		
		return 0;
	}
		
	if (t0-t1 > 0 && t2-t3 > 0) {
		return 0;
	}

	return -1;
}

# define scale 0
# define tdown(__x) \
	(((__x))<<scale)
# define tup(__x) \
	((__x>>scale))
int main() {
	struct point points[] = {
# ifdef CPX
		{2, 2},
		{29, 16},
		{2, 29},
		{29, 10},
		{5, 6},
		{2, 20},
		{2, 2}
# else
# define PAD 2
# define TAL 166
# define THIC TAL/6
	{PAD, PAD},
	{THIC-PAD, PAD},
	{THIC-PAD, TAL-PAD},
	{PAD, TAL-PAD},
	{PAD, PAD}
# endif
	};

	_int x, y;

	int out;
	out = open("test.ppm", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	char header[64];
	char buf[128];
	_int_u size;
	size = sprintf(buf, "P6\n%u %u\n255\n", tdown(WIDTH), tdown(HEIGHT));
	write(out, buf, size);
	_8_u row[tdown(WIDTH)*3];
	y = 0;
	while(y != tdown(HEIGHT)) {
		x = 0;
		while(x != tdown(WIDTH)) {
			_8_u in;
			in = fix(points, tup(x), tup(y));
			_8_u *dst;

			dst = row+(x*3);
			_8_u *r, *g, *b;
			r = dst;
			g = dst+1;
			b = dst+2;
			if (!in) {
				*r = 255;
				*g = 0;
				*b = 0;
			} else {
				*r = 0;
				*g = 0;
				*b = 0;
			}
			x++;
		}
		write(out, row, tdown(WIDTH)*3);
		y++;
	}
	close(out);
}
