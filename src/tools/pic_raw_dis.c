#include "../y_int.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../io.h"
#include "../string.h"
#include "../slam.h"
#include "../m_alloc.h"
void m_pic_dis(_8_u*, _int_u);
int main(int __argc, char const *__argv[]) {
    int fd;
    if (__argc<2) {
        printf("please provide file,\n\tusage: readremf [file].\n");
        return -1;
    }

    if (access(__argv[1], F_OK) == -1) {
        printf("file doesen't exist.\n");
        return -1;
    }

    if ((fd = open(__argv[1], O_RDONLY, 0)) == -1) {

        return -1;
    }

	struct stat st;
	fstat(fd, &st);
	void *buf;
	buf = m_alloc(st.st_size);
	read(fd, buf, st.st_size);
	m_pic_dis(buf, st.st_size);
	m_free(buf);
	close(fd);
}
