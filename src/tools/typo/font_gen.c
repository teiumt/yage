# include "../../y_int.h"
struct point {
	_int x, y;
};

# define N N0
struct point *points[0x100];
_int_u np[0x100];

# include "zero.h"
# include "one.h"
# include "two.h"
# include <stdio.h>
# include <string.h>
# include <unistd.h>
# include <fcntl.h>

void two(void) {
# define N2 31
	np['2'] = 31;
	points['2'] = two_points;
}

void one(void) {
# define N1 N2+5
	np['1'] = 5;
	points['1'] = one_points;
}

void zero(void) {
# define N0 N1+18
	np['0'] = 18;
	points['0'] = zero_points;
}


int main() {
	int out;
	out = open("test.h", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	memset(points, 0, 0x100*sizeof(struct point*));
	char buf[1024];

	zero();
	one();
	two();
	_int_u len;

	len = sprintf(buf, "# define N %u\n", N);
	write(out, buf, len);

	write(out, "struct typo_point points[] = {\n", 31);
	_int_u i;
	i = 0;
	while(i != 0x100) {
		if (points[i] != NULL) {
			struct point *p;
			p = points[i];
			_int_u n;
			n = np[i];
			
			_int_u si;
			si = 0;
			while(si != n) {
				len = sprintf(buf, "\t{%u, %u},\n", p[si].x, p[si].y);
				write(out, buf, len);
				si++;
			}

		}

		i++;
	}
	write(out, "};\n", 3);
	close(out);
}
