#include "../y_int.h"
#include "../linux/unistd.h"
#include "../linux/fcntl.h"
#include "../linux/stat.h"
#include "../io.h"
#include "../string.h"
#include "../slam.h"
#include "../m_alloc.h"
#include "../assert.h"
void m_pic_dis(_8_u*, _int_u);
char const static *notes = "";
int main(int __argc, char const *__argv[]) {
	printf("%s",notes);
	int fd;
    if (__argc<2) {
        printf("please provide file,\n\tusage: readremf [file].\n");
        return -1;
    }

    if (access(__argv[1], F_OK) == -1) {
        printf("file doesen't exist.\n");
        return -1;
    }

    if ((fd = open(__argv[1], O_RDONLY, 0)) == -1) {

        return -1;
    }

	struct slam_hdr h;
	read(fd, &h, sizeof(struct slam_hdr));

	printf("SLAM-HDR: %u, %u.\n",h.sg,h.ns);
	struct slm_segment *sg;
	sg = m_alloc(sizeof(struct slm_segment)*h.ns);
	pread(fd, sg, sizeof(struct slm_segment)*h.ns, h.sg);
	_int_u i;
	i = 0;
	assert(h.ns<0xffff);
	for(;i != h.ns;i++) {
		struct slm_segment *s = sg+i;
		printf("SEG. bytes: %u, off: %u, words: %u, adr: %u\n", s->bytes, s->off, s->words, s->adr);
		void *buf;
		buf = m_alloc(s->bytes);
		pread(fd, buf, s->bytes,s->off);
		m_pic_dis(buf, s->bytes);
		m_free(buf);
	}
	m_free(sg);

	close(fd);
}
