# include "pellet.h"
# include "dep/mem_cpy.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "system/io.h"
ffly_pelletp ffly_pellet_mk(_int_u __size) {
	ffly_pelletp ret = (ffly_pelletp)__f_mem_alloc(sizeof(struct ffly_pellet));
	ret->p = (_8_u*)__f_mem_alloc(__size);
	ret->end = ret->p+__size;
	ret->off = 0;
	return ret;
}

void ffly_pellet_put(ffly_pelletp __pel, void const *__p, _int_u __size) {
	_8_u *dst = __pel->p+__pel->off;
	if (dst>=__pel->end) {
		printf("pellet error, put\n");
		return;
	}
	f_mem_cpy(dst, __p, __size);
}

void ffly_pellet_get(ffly_pelletp __pel, void *__p, _int_u __size) {
	_8_u *src = __pel->p+__pel->off;
	if (src>=__pel->end) {
		printf("pellet error, get\n");
		return;
	}
	f_mem_cpy(__p, src, __size);
}

void ffly_pellet_incr(ffly_pelletp __pel, _int_u __by) {
	if (__pel->p+(__pel->off+__by)>__pel->end) {
		printf("pellet error, incr\n");
		return;
	}
	__pel->off+=__by;
}

void ffly_pellet_decr(ffly_pelletp __pel, _int_u __by) {
	if (__by>__pel->off) {
		printf("pellet error, decr{%u>%u}\n", __by, __pel->off);
		return;
	}
	__pel->off-=__by;
}

void ffly_pellet_free(ffly_pelletp __pel) {
	__f_mem_free(__pel->p);
	__f_mem_free(__pel);
}
