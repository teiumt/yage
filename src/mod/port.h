# ifndef __ffly__mod__port__h
# define __ffly__mod__port__h
# include "../y_int.h"
_int_u ffmod_port_shmid(void);
void ffmod_port(void);
void ffmod_port_open(_int_u);
void ffmod_port_close(void);
_int_u ffmod_portno(void);
# endif /*__ffly__mod__port__h*/
