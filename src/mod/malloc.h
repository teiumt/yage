# ifndef __ffly__mod__malloc__h
# define __ffly__mod__malloc__h
# include "../y_int.h"
# define FF_MAL_O_LOC 0x1
# define FF_MAL_O_OSD 0x2
void setmalopt(_8_u);
void* malloc(_int_u);
void free(void*);
void* realloc(void*, _int_u);
# endif /*__ffly__mod__malloc__h*/
