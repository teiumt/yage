# include "copy.h"
# include "ring.h"
# include "../call.h"
# include "../ffly_def.h"
void dcopy(void *__dst, void *__src, _int_u __n) {
	_8_u bed[16+sizeof(_int_u)];
	_8_u *p = bed;
	*(void**)p = __dst;
	p+=8;
	*(void**)p = __src;
	p+=8;
	*(_int_u*)p = __n;
	ffmod_ring(_ffcal_mod_dcp, NULL, bed);
}

void scopy(void *__dst, void *__src, _int_u __n) {
	_8_u bed[16+sizeof(_int_u)];
	_8_u *p = bed;
	*(void**)p = __dst;
	p+=8;
	*(void**)p = __src;
	p+=8;
	*(_int_u*)p = __n;
	ffmod_ring(_ffcal_mod_scp, NULL, bed);
}
