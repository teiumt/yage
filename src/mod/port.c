# include "port.h"
# include "../system/port.h"
_int_u static port;
_int_u ffmod_port_shmid(void) {
	return ffly_port_get_shmid(port);
}

_int_u ffmod_portno(void) {
	return port;
}

void ffmod_port(void) {
	_f_err_t err;
	port = ffly_port(8, FF_PORT_CREAT, 0, &err);
}

void ffmod_port_open(_int_u __shm_id) {
	_f_err_t err;
	port = ffly_port(8, FF_PORT_SHMM, __shm_id, &err);
}

void ffmod_port_close(void) {
	ffly_port_close(port);
}
