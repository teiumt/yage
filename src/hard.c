#include "hard.h"
#include "m_alloc.h"
#include "string.h"
struct y_hard* hard_alloc(_64_u __size) {
	struct y_hard *h;
	h = (struct y_hard*)m_alloc(sizeof(struct y_hard));
	h->p = m_alloc(__size);
	return h;
}

void hard_free(struct y_hard *__h) {
	m_free(__h->p);
}

void hard_read(struct y_hard *__h, void *__buf, _32_u __size, _64_u __offset) {
	mem_cpy(__buf, __h->p+__offset, __size);
}

void hard_write(struct y_hard *__h, void *__buf, _32_u __size, _64_u __offset) {
	mem_cpy(__h->p+__offset, __buf, __size);
}
