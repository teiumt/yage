# ifndef __ffly__pci__h
# define __ffly__pci__h
# include "y_int.h"

#define DEVICE_MAX 40
#define PCI_MAY_DOM 1
#define PCI_MAY_BUS 2
#define PCI_MAY_DEV 4
#define PCI_MAY_FUNC 8
#define PCI_DEV_ENABLE		1
#define PCI_DEV_DISABLE		0
struct pci_mem_region {
	_64_u h_addr;
	_64_u l_addr;
	_64_u size, flags;
};

struct pci_dev {
	_32_u handle;
	_8_u header_type;
	_32_u domain;
	_8_u bus;
	_8_u dev;
	_8_u func;

	_16_u vendor_id;
	_16_u device_id;
	_32_u device_class;
	_8_u revision;
	_16_u subvendor_id;
	_16_u subdevice_id;


	struct pci_mem_region regions[6];
};

struct pci_slot_match {
	_32_u domain;
	_8_u bus;
	_8_u dev;
	_8_u func;
	_8_u flags;
};

#define PCI_ITR_MAX 40
struct pci_dev_iter {
	struct pci_dev *devices[PCI_ITR_MAX];
	struct pci_dev **cur;
};

struct pci_sys_funcs;

struct pci_sys {
	struct pci_sys_funcs *funcs;
	_int_u ndev;
	struct pci_dev devices[DEVICE_MAX];
};

struct pci_io {
	_64_u base;
	_64_u size;
	void *mem;
	int fd;
};

struct pci_sys_funcs {
	void(*ncp)(_8_u, _ulonglong);
	void(*statechange)(struct pci_dev*, _8_u);
	void(*device_add)(struct pci_dev*, _32_u, _8_u, _8_u, _8_u);
	void(*io_open)(struct pci_dev*, struct pci_io*, int, _64_u, _64_u);
	void(*io_close)(struct pci_io*);
	void(*device_probe)(struct pci_dev*);
	void(*probe)(void);
};

struct pci_dev_iter* pci_device_iter_create(struct pci_slot_match*);
struct pci_dev* pci_device_next(struct pci_dev_iter*);
void pci_system_init(void);
struct pci_dev* pci_device_add(_32_u, _8_u, _8_u, _8_u);
void pci_device_statechange(struct pci_dev*, _8_u);
#define pci_probe _pci_sys->funcs->probe
extern struct pci_sys *_pci_sys;
# endif /*__ffly__pci__h*/
