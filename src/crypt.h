#ifndef __y__crypt__h
#define __y__crypt__h
#include "y_int.h"
#define CPT_REST (haze_funcs+0)
struct cpt_context {
};
void cpt_init(void);
void y_encpt(void*,_int_u,void*,void*);
void y_decpt(void*,_int_u,void*,void*);
_64_u y_hash64(void *__data, _int_u __length);
#endif/*__y__crypt__h*/
