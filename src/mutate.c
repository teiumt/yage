#include "pixels.h"
void rgb_to_rgba(_8_u *__d,_8_u *__s,_int_u __w,_int_u __h) {
	_int_u x,y;
	y = 0;
	for(;y != __h;y++) {
		x = 0;
		for(;x != __w;x++) {
			_8_u *d,*s;
			d = __d+((x+(y*__w))*4);
			s = __s+((x+(y*__w))*3);
			d[0] = s[0];
			d[1] = s[1];
			d[2] = s[2];
			d[3] = 255;
		}
	}
}
#include "io.h"
#define M mu_mat
float *mu_mat = NULL;
/*
	this is not only a conversion function but a loading function
*/
void rgb_to_rgba_float(float *__d,_8_u *__s,_int_u __w,_int_u __h) {
	_int_u x,y;
	y = 0;
	for(;y != __h;y++) {
		x = 0;
		for(;x != __w;x++) {
			_8_u *s;
			float *d;
			d = __d+((x+(y*__w))*4);
			s = __s+((x+(y*__w))*3);
			float r,g,b;
			
			r = (1./255.)*(float)s[0];
			g = (1./255.)*(float)s[1];
			b = (1./255.)*(float)s[2];
			if(mu_mat != NULL) {
				/*
					this is kinda bad, preformance wise.
					would be better to abstract it a bit.

					this is only here because .PPM file formats
					dont have an alpha channel.
					and other image formats are not avalable
					because we dont have the librarys made yet.
				*/
				d[0] = r*M[0]+g*M[1]+b*M[2]+M[4];
				d[1] = r*M[5]+g*M[6]+b*M[7]+M[9];
				d[2] = r*M[10]+g*M[11]+b*M[12]+M[14];
				d[3] = r*M[15]+g*M[16]+b*M[17]+M[19];
			}else{
				d[0] = r;
				d[1] = g;
				d[2] = b;
				d[3] = 1;
			}
		}
	}
}

void rgba_to_rgba_float(float *__d,_8_u *__s,_int_u __w,_int_u __h) {
	_int_u x,y;
	y = 0;
	for(;y != __h;y++) {
		x = 0;
		for(;x != __w;x++) {
			_8_u *s;
			float *d;
			d = __d+((x+(y*__w))*4);
			s = __s+((x+(y*__w))*4);
			d[0] = (1./255.)*(float)s[0];
			d[1] = (1./255.)*(float)s[1];
			d[2] = (1./255.)*(float)s[2];
			d[3] = (1./255.)*(float)s[3];
		}
	}
}

