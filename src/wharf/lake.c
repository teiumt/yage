# include "../wharf.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../memory/mem_realloc.h"
# include "../dep/mem_cpy.h"
#define PAGE_SHFT 4
#define PAGE_SIZE (1<<PAGE_SHFT)
#define PAGE_MASK (PAGE_SIZE-1)

f_whf_lakep f_whf_lake_new(struct f_whfl_info *__info) {
	f_whf_lakep lak;
	lak = (f_whf_lakep)__f_mem_alloc(sizeof(struct f_whf_lake));
	lak->info = __info;
	lak->page_c = 1;
	lak->off = 0;
	lak->p = (void**)__f_mem_alloc(sizeof(void*));
	*lak->p = __f_mem_alloc(lak->rbz = (PAGE_SIZE*__info->blksz));
	return lak;
}

void f_whf_lake_push(f_whf_lakep __lak, void *__p, _int_u __n) {
	_int_u pg, pg_off, apex, sz;
	apex = (__lak->off+(sz = (__n*__lak->info->blksz))+PAGE_MASK)>>PAGE_SHFT;
	if (apex>=__lak->page_c) {
		_int_u pgc = apex;
		__lak->p = (void**)__f_mem_realloc(__lak->p, pgc*sizeof(void*));
		_int_u pg;
		pg = __lak->page_c;
		__lak->page_c = pgc;
		while(pg != __lak->page_c) {
			*(__lak->p+(pg++)) = __f_mem_alloc(__lak->rbsz);
		}
	}

	pg = __lak->off>>PAGE_SHFT;
	pg_off = __lak->off&PAGE_MASK;
	_int_u rpo = pg_off*__lak->info->blksz;

	_8_u *p = (_8_u*)__p;
	if (pg_off>0) {
		_8_u *d;
		d = ((_8_u*)*(__lak->p+pg))+rpo;
		_int_u tg;
		tg = __lak->rbz-rpo;
		if (sz>tg) {
			f_mem_cpy(d, p, tg);
		} else {
			f_mem_cpy(d, p, sz);
			return;
		}
	}

	_int_u wp;
	wp = __n>>PAGE_SHFT;
	void **page = __lak->p;
	void **ep = page+wp;
	_8_u *d;
	while(page != ep) {
		d = (_8_u*)*(page++);
		f_mem_cpy(d, p, __lak->rbz);
		p+=__lak->rbz;
	}

	_int_u left;
	left = p-(_8_u*)__p;
	if (left>0) {
		f_mem_cpy(*page, p, left);
	}
}
