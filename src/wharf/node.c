# include "../wharf.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../dep/bzero.h"
# include "../dep/mem_cpy.h"
# include "../system/io.h"
# include "../inet.h"
# include "../in.h"

_f_err_t ffmain(int __argc, char const *__argv[]) {
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(10198);
	connect(fd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));

	_8_u buf[256];

	struct f_whf_space *s = (struct f_whf_space*)buf;
	s->id = 0;
	s->addr = 0;
	hdr.data = 128;
	hdr.space = 0;
	hdr.n = 1;
	hdr.size = 256;
	hdr.flags = F_WHF_SYNC;
	struct f_whf_header hdr;
	struct f_whf_struc struc;
	send(fd, &struc, sizeof(struct f_whf_struc), 0);
	struc.tsz = 1+sizeof(struct f_whf_header);
	_8_u code[64];
	*code = 1;
	*(struct f_whf_header*)(code+1) = hdr;
	send(fd, code, struc.tsz, 0);
	send(fd, buf, 256, 0);

	close(fd);
}
