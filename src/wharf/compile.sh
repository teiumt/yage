rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd wharf
ffly_objs="$ffly_objs"
gcc $cc_flags -o node node.c $ffly_objs -nostdlib
gcc $cc_flags -o wharf main.c $ffly_objs -nostdlib
