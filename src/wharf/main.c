# include "../wharf.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../dep/bzero.h"
# include "../dep/mem_cpy.h"
# include "../system/io.h"
# include "../inet.h"
# include "../in.h"
# include "../system/thread.h"
# include "../system/mutex.h"
# include "../system/doze.h"
# include "../tmu.h"
# include "../linux/ioctls.h"
# include "../linux/ioctl.h"
static _int_u node_c = 0;
struct box;
struct context {
	struct box *bx;
};

static _8_u *cc;
struct _op {
	_int_u size;
	char const *name;
	void *ptr;
};

struct place {
	void *p;
	_int_u max;
};

_int_u tot_size = 0;
#define N 1
static struct place _p[N] = {
	{NULL, 0}
};
/*
_int_u static mem_size;
_8_u static *mem = NULL;
void static
_build(void) {
	if (mem != NULL)
		__f_mem_free(mem);
	_int_u size;
	size = tot_size+(N*sizeof(struct f_whf_space))+sizeof(struct f_whf_header);
	mem = __f_mem_alloc(size);

	struct f_whf_header *hdr = (struct f_whf_header*)mem;

	hdr->data = sizeof(struct f_whf_header);
	hdr->space = hdr->data+tot_size;
	_8_u *data = mem+hdr->data;
	_8_u *space = mem+hdr->space;
	struct place *p;
	struct f_whf_space *sp;
	_int_u i, addr = 0;
	i = 0;
	for(;i != N;i++) {
		p = _p+i;
		sp = ((struct f_whf_space*)space)+i;
		sp->addr = addr;
		sp->id = i;
		sp->size = p->max;
		_8_u *d;
		d = data+addr;
		f_mem_cpy(d, p->p, sp->size);
		addr+=p->size;
	}
	mem_size = size;
}
*/
void static _sync(struct context*, _8_u*);


static struct _op ops[] = {
	{0, "DISCONNECT", NULL},
	{0, "sync", _sync}
};

static _8_i run;
void texec(struct box *__bx, _8_u *__t, _int_u __tsz) {
	_8_u *cc = __t;
	struct _op *p;
	_8_u *e = __t+__tsz, on;
	struct context ctx;
	ctx.bx = __bx;
	while(cc != e) {
		on = *(cc++);
		p = ops+on;
		printf("op: %s.\n", p->name);
		if (on == _f_whf_disconnect) {
			run = 0;
			break;
		}
		if (!(on>>7)) {
			((void(*)(struct context*, _8_u*))p->ptr)(&ctx, cc);
		}
		cc+=p->size;
	}
}

#define BOX_USABLE 0x01	
struct update;
struct box {
	struct update *prev_up;
	_8_u *data;
	struct f_whf_header hdr;
	struct f_whf_con con;
	struct box *next;
	_8_u flags;
};

struct update {
	struct box *from;
	struct update *next;
	_int_u ndone;
	_8_u flags;
};

static struct update *_c_update = NULL;
static struct update *_up = NULL;


static struct update*
update_new(struct box *__bx) {
	struct update *up;
	up = (struct update*)__f_mem_alloc(sizeof(struct update));
	up->from = __bx;
	up->flags = 0x00;
	up->next = _up;
	_up = up;
	up->ndone = 0;
	printf("new update pushed to stack.\n");
	return up;
}

#define UP_COMPLETE 0x01
static struct box *bx_top = NULL;

void _sync(struct context *__ctx, _8_u *__cc) {
	struct f_whf_header *hdr_in, hdr_out;
	int fd;
	fd = __ctx->bx->con.fd;
	hdr_in = (struct f_whf_header*)__cc;

	hdr_out.flags = 0x00;
	if (hdr_in->flags&F_WHF_SYNC) {
		_8_u *p;
		p = (_8_u*)__f_mem_alloc(hdr_in->size);
		recv(fd, p, hdr_in->size, 0);
		
		__ctx->bx->data = p;
		__ctx->bx->hdr = *hdr_in;
		update_new(__ctx->bx);
	}
/*
	if (_c_update != NULL) {
		if (__ctx->bx->prev_up != _c_update && _c_update->bx != __ctx->bx) {
			__ctx->bx->prev_up = _c_update;
			hdr_out = &_c_update->hdr; 
			_c_update->ndone++;
		}
	}

	send(fd, hdr_out, sizeof(struct f_whf_header), 0);
*/
}

static _8_i run = -1;
static _f_tid_t man_id;
static mlock lock = F_MUTEX_INIT;
struct box static*
new_box(void) {
	struct box *bx;
	bx = (struct box*)__f_mem_alloc(sizeof(struct box));
	bx->flags = 0x00;
	mt_lock(&lock);
	bx->next = bx_top;
	bx_top = bx;
	mt_unlock(&lock);
	return bx;
}



void* manage(void *__arg) {
	while(run == -1) {
		mt_lock(&lock);
		if (!_c_update) {
			_c_update = _up;	
		} else if (_c_update->ndone == node_c) {
			_c_update = _up;
			_up->flags |= UP_COMPLETE;
			/*
				now thats done sync with are local data
			*/
			_up = _up->next;
		}
		struct box *bx;
		bx = bx_top;
		struct f_whf_struc struc;
		_8_u *tape;
		int bytes;
		while(bx != NULL) {
			if (!(bx->flags&BOX_USABLE))
				goto _sk;
			ioctl(bx->con.fd, FIONREAD, (_64_u)&bytes);
			if (bytes>0) {
				recv(bx->con.fd, &struc, sizeof(struct f_whf_struc), 0);
				tape = (_8_u*)__f_mem_alloc(struc.tsz);
				recv(bx->con.fd, tape, struc.tsz, 0);
				texec(bx, tape, struc.tsz);
				__f_mem_free(tape);
			}
		_sk:
			bx = bx->next;
		}
		mt_unlock(&lock);
		f_doze(TIME_FMS(100), 0);
	}
}

void static
start(void) {
	int fd;

	fd = socket(AF_INET, SOCK_STREAM, 0);
	int val = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(int));

	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(10198);
	bind(fd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	f_bzero(&addr, sizeof(struct sockaddr_in));
	socklen_t len;
	len = sizeof(struct sockaddr_in);
	struct box *bx;
	struct f_whf_con con;
_back:
	listen(fd, 24);
	con.fd = accept(fd, &addr, &len);
	node_c++;
	printf("somebody has connected.\n");
	bx = new_box();	
	bx->con = con;
	bx->flags = BOX_USABLE;
	goto _back;
}

_f_err_t ffmain(int __argc, char const *__argv[]) {
	ffly_thread_create(&man_id, manage, NULL);
	start();
	run = 0;
	ffly_thread_wait(man_id);
}
