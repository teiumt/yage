# ifndef __f__lemon__las__h
# define __f__lemon__las__h
# include "../y_int.h"
# include "../lib/hash.h"
# include "lf.h"
# include "lemon.h"
#define F_LAS_NOPS 3
enum {
	_f_las_sym_ident,
	_f_las_sym_spc,
	_f_las_sym_label,
	_f_las_sym_int,
	_f_las_sym_label0,
	_f_las_sym_var,
	_f_las_sym_sg,
	_f_las_sym_str
};

typedef struct f_las_seg {
	_8_u buf[256];
	_32_u off, dst;
	struct f_las_seg *next;
} *f_las_segp;

typedef struct f_las_sym {
	_8_u kind;
	void *p;
	_int_u len;
	struct f_las_sym *next;
	_64_u ival;
} *f_las_symp;


struct f_lms_asc {
	_int_u(*in)(void*, _int_u);
	_int_u(*out)(void*, _int_u);
	void(*wto)(void*, _int_u, _32_u);
	_ulonglong(*iaf)(_8_u, _ulonglong);
	_int_u *ptr;
};

struct f_las_op {
	_8_u opcode[4];
	char const *id;
	_int_u len;
	_8_u noa;
};

struct f_las_specadit {
	_32_u list[4]	
};

#define _f_las_sa_ival 0
#define _f_las_sa_str 0
#define _f_las_sa_str_len 1
enum {
	_f_las_int,
	_f_las_uint,
	_f_las_str
};

#define _f_las_vial_a 0
enum {
	_f_las_vial_clutch,
	_f_las_vial_space
};

struct f_las_val {
	_8_u type;
	union {
		_64_u ival;
		struct {
			char const *s;
			_16_u len;
		};
	};
	struct f_las_specadit *sa;
};

typedef struct f_las_vial {
	_8_u buf[256];
	_8_u *p;
	_16_u n;
	struct f_las_vial *next;
	f_las_symp sy;
	_int_u type;

	struct f_las_val val[2];
} *f_las_vialp;

void f_las_init(void);
void f_las_de_init(void);
extern struct f_las_op f_las_op_tbl[F_LAS_NOPS];
void f_lmn_as(void);
char f_las_getchr(void);
void f_las_retract(char);
f_las_symp f_las_eval(_8_u*);
extern struct f_lms_asc _f_lms_asc;
# endif /*__f__lemon__las__h*/
