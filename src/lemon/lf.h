# ifndef __f__lemon__lf__h
# define __f__lemon__lf__h
# include "../y_int.h"
# include "lemon.h"
enum {
	_f_lf_as_clutch,
	_f_lf_as_brk_space
};
struct f_lf_astrc {
	_32_u fill[SEGA_EC];
};


struct f_lf_vial {
	_32_u src, size;
	_16_u n;
	struct f_lf_astrc as;
};

struct f_lf_seg {
	_32_u src, size;
	_32_u dst;
};

struct f_lf_hdr {
	_32_u src, len;
	_32_u vial, n_vial;
	_32_u seg, n_seg;
};

# endif /*__f__lemon__lf__h*/
