# include "las.h"
# include "../mort/malloc.h"
# include "../mort/string.h"
# include "../mort/stdio.h"
struct f_lms_asc _f_lms_asc;
static struct f_lhash gh_tbl;
static struct f_lhash h_tbl;
static struct f_lhash spec_tbl;
static char cbuf[64];
static char *cbp = cbuf;
char f_las_getchr(void) {
	char c;
	if (cbp>cbuf) {
		c = *(--cbp);
		return c;
	}
	if (_f_lms_asc.in(&c, 1) != 1) {
		printf("error.\n");
		return 0;
	}
	return c;
}

void f_las_retract(char __c) {
	*(cbp++) = __c;
}

static struct f_las_specadit _sa[] = {
	{{_f_lf_as_clutch}},
	{{_f_lf_as_brk_space}}
};

f_las_vialp static curvial = NULL;
f_las_segp static curseg = NULL;
static _int_u n_vials = 0;
static _int_u n_segs = 0;

static _32_u addr = 0;
f_las_segp static _new_seg(f_las_symp __sy) {
	if (curseg != NULL) {
		curseg->dst = addr;
		addr+=curseg->off;
	}
	f_las_segp sg;
	sg = (f_las_segp)malloc(sizeof(struct f_las_seg));

	if (curseg != NULL) {
		curseg->next = sg;
	}
	sg->off = 0;
	sg->next = NULL;
	curseg = sg;
	n_segs++;
	f_lhash_put(&gh_tbl, __sy->p, __sy->len, sg);
}

f_las_vialp static _new_vial(f_las_symp __sy) {
	f_las_vialp sg;
	sg = (f_las_vialp)malloc(sizeof(struct f_las_vial));

	sg->type = __sy->next->ival;
	sg->sy = __sy;
	sg->n = n_vials;
	n_vials++;
	printf("vial type: %u, name: %s\n", sg->type, __sy->p);
	if (sg->type == _f_las_vial_a) {
		sg->val[0].ival = 0;
		sg->val[0].type = _f_las_uint;
		sg->val[0].sa = _sa;

		sg->val[1].ival = 0;
		sg->val[1].type = _f_las_uint;
		sg->val[1].sa = _sa+1;
	}

	sg->p = sg->buf;
	if (curvial != NULL) {
		curvial->next = sg;
	}
	sg->next = NULL;
	curvial = sg;

	f_lhash_put(&gh_tbl, __sy->p, __sy->len, sg);
	return sg;
}

void static _addops(void) {
	_int_u i;
	i = 0;
	struct f_las_op *op;
	for(;i != F_LAS_NOPS;i++) {
		op = f_las_op_tbl+i;
		f_lhash_put(&h_tbl, op->id, op->len, op);
	}
}

struct spec {
	char const *name;
	_int_u len;
	_int_u id;
};


#define NSPEC 2
static struct spec spec_arr[] = {
	{"clutch", 6, _f_las_vial_clutch},
	{"space", 5, _f_las_vial_space}
};
void f_las_init(void) {
	f_lhash_init(&h_tbl);
	f_lhash_init(&gh_tbl);
	f_lhash_init(&spec_tbl);
	_addops();

	_int_u i;
	i = 0;
	for(;i != NSPEC;i++) {
		struct spec *sp;
		sp = spec_arr+i;

		f_lhash_put(&spec_tbl, sp->name, sp->len, sp);
	}
	*_f_lms_asc.ptr = sizeof(struct f_lf_hdr);
}

void f_las_de_init(void) {
	if (curseg != NULL) {
		curseg->dst = addr;
	}

	f_lhash_destroy(&h_tbl);
	f_lhash_destroy(&gh_tbl);
	f_lhash_destroy(&spec_tbl);

	struct f_lf_hdr h;
	h.src = sizeof(struct f_lf_hdr);
	h.len = (*_f_lms_asc.ptr)-sizeof(struct f_lf_hdr);//for now too lazy
	struct f_lf_vial *vials;
	vials = (struct f_lf_vial*)malloc(n_vials*sizeof(struct f_lf_vial));
	_int_u n, size;
	n = 0;
	size = 0;
	struct f_las_vial *p;
	p = curvial;
	while(p != NULL) {
		struct f_lf_vial *s;
		struct f_lf_astrc as;
		s = vials+n;

		_int_u i;
		i = 0;
		for(;i != SEGA_EC;i++) {
			struct f_las_val *v;
			v = p->val+i;
			switch(v->type) {
				case _f_las_uint:
					as.fill[v->sa->list[_f_las_sa_ival]] = *_f_lms_asc.ptr;
					size+=sizeof(_32_u);	

					_f_lms_asc.out(&v->ival, sizeof(_32_u));
				break;
				case _f_las_str:
					as.fill[v->sa->list[_f_las_sa_str]] = *_f_lms_asc.ptr;
					as.fill[v->sa->list[_f_las_sa_str_len]] = (*_f_lms_asc.ptr)+v->len;
					size+=v->len+sizeof(_16_u);
					_f_lms_asc.out(v->s, v->len);
					_f_lms_asc.out(&v->len, sizeof(_16_u));
				break;
			}
		}

		i = 0;
		for(;i != SEGA_EC;i++) {
			printf("dest: %u.\n", as.fill[i]);
		}

		s->src = *_f_lms_asc.ptr;
		s->size = size;
		s->n = p->n;
		printf("vial dest: %u.\n", p->n);
		s->as = as;
		n++;
		p = p->next;
	}
	h.vial = *_f_lms_asc.ptr;
	h.n_vial = n_vials;
	_f_lms_asc.out(vials, n_vials*sizeof(struct f_lf_vial));
	free(vials);

	struct f_lf_seg *segs;
	segs = (struct f_lf_seg*)malloc(n_segs*sizeof(struct f_lf_seg));
	struct f_las_seg *s;
	s = curseg;
	n = 0;
	while(s != NULL) {
		struct f_lf_seg *ss;
		ss = segs+n;

		ss->src = *_f_lms_asc.ptr;
		ss->size = s->off;
		ss->dst = s->dst;
		_f_lms_asc.out(s->buf, s->off);
		n++;
		s = s->next;
	}

	h.seg = *_f_lms_asc.ptr;
	h.n_seg = n_segs;
	_f_lms_asc.out(segs, n_segs*sizeof(struct f_lf_seg));
	free(segs);
	_f_lms_asc.wto(&h, sizeof(struct f_lf_hdr), 0);
}

void f_lmn_as(void) {
	_8_u state;
	f_las_symp s;
	struct f_las_op *op;
_again:
	state = 0;
	s = f_las_eval(&state);

	if (state || !s) {
		return;
	}

	if (s->kind == _f_las_sym_label) {
		_new_vial(s);
	} else if (s->kind == _f_las_sym_label0) {
		_new_seg(s);
	} else if (s->kind == _f_las_sym_spc) {

		_int_u i;

		struct spec *sp;
		sp = f_lhash_get(&spec_tbl, s->p, s->len);
		if (!sp) {
			while(1);
		}
		i = sp->id;
		printf("spec off: %u, set to %u. %s, type: %u\n", i, s->next->ival, !s->next->p?"NOPE":s->next->p, curvial->val[i].type);

		struct f_las_val *v;
		v = curvial->val+i;
		switch(v->type) {
			case _f_las_int:
				v->ival = s->next->ival;
			break;
			case _f_las_uint:
				v->ival = s->next->ival;
			break;
			case _f_las_str:
				v->s = s->next->p;
				v->len = s->next->len;
			break;
		}
	} else if (s->kind == _f_las_sym_var) {
		_8_u type;
	
		if (!strcmp(s->p, "str")) {
			type = 0;
		} else if (!strcmp(s->p, "dw")) {
			type = 1;
		}
	
		void *dst;
		dst = curseg->buf+curseg->off;
		_int_u size;
		switch(type) {
			case 0:
				size = s->next->len+1;
				memcpy(dst, s->next->p, size);
			break;
			case 1:
				size = sizeof(_32_u);
				*(_32_u*)dst = s->next->ival;
			break;
		}
		curseg->off+=size;
	} else {
		_8_u opbuf[64];
		_8_u *ob_ptr = opbuf;
		op = f_lhash_get(&h_tbl, s->p, s->len);
		if (op != NULL) {
			printf("found in op table: id, %s.\n", op->id);
		}

		*(ob_ptr++) = op->opcode[0];
		char const *def = "NOPE";
		_int_u i;
		i = 0;
		s = s->next;
		while(s != NULL) {
			if (s->kind == _f_las_sym_sg) {
				struct f_las_seg *sg;
				sg = (struct f_las_seg*)f_lhash_get(&gh_tbl, s->p, s->len);
				*(_16_u*)ob_ptr = sg->dst;
				ob_ptr+=2;
			} else
			if (s->kind == _f_las_sym_ident) {
				struct f_las_vial *sg;
				sg = (struct f_las_vial*)f_lhash_get(&gh_tbl, s->p, s->len);
				if (!sg) {
					printf("trying to kill me eh???\n");
				} else {
					*(_16_u*)ob_ptr = sg->n;
					ob_ptr+=2;
				}
			}
			if (!s->p)
				s->p = def;
			printf("%u: %u, %s, %u.\n", i++, s->kind, s->p, s->ival);
			s = s->next;
		}
		_f_lms_asc.out(opbuf, ob_ptr-opbuf);
	}
	if (!_f_lms_asc.iaf(0, 0))
		goto _again;

	struct f_las_vial *p;
	p = curvial;
	while(p != NULL) {
		_int_u i;
		i = 0;
		for(;i != SEGA_EC;i++) {
			struct f_las_val *v;
			v = p->val+i;
			switch(v->type) {
				case _f_las_uint:
					printf("value, %u.\n", v->ival);
				break;
				case _f_las_str:
					printf("spec value, %s, len: %u\n", v->s, v->len);
				break;
			}
		}
		p = p->next;
	}
}
