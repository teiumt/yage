# include "lemon.h"
# include "../mort/stdio.h"
# include "../mort/string.h"
# include "las.h"




_8_u static outbuf[4096];
_int_u static ob_ptr = 0;
char const static *p, *end;
static _ulonglong _iaf(_8_u __what, _ulonglong __arg) {
	return p>=end;
}


static void _wto(void *__buf, _int_u __size, _32_u __offset) {
	memcpy(outbuf+__offset, __buf, __size);
}
static _int_u _in(void *__buf, _int_u __size) {
	_int_u left;
	left = end-p;
	if (!left) {
		return 0;
	}

	if (__size>left) {
		memcpy(__buf, p, left);
		p = end;
		return left;
	}
	
	memcpy(__buf, p, __size);
	p+=__size;
	return __size;
}

static _int_u _out(void *__buf, _int_u __size) {
	_8_u *d;
	d = outbuf+ob_ptr;
	ob_ptr+=__size;
	memcpy(d, __buf, __size);
	return 0;
}

# include "../hexdump.h"
# include "../chrdump.h"
# include "../linux/unistd.h"
# include "../linux/stat.h"
# include "../linux/fcntl.h"
# include "../mort/malloc.h"
void f_lmn_exec(_8_u*, _int_u);
_f_err_t ffmain(int __argc, char const *__argv[]) {
//	char const *a_code =
//			"none: 0\n"
//			"~clutch 0\n"
//			"~space 0\n"
//			"start none\n"
//			"end";
	
	int fd;
	fd = open("test.lmn", O_RDONLY, 0);

	struct stat st;
	fstat(fd, &st);
	void *bed;
	p = bed = malloc(st.st_size);
	read(fd, bed, st.st_size);
	printf("filesize: %u.\n", st.st_size);

	end = ((char const*)bed)+st.st_size-1;
	_f_lms_asc.ptr = &ob_ptr;
	_f_lms_asc.wto = _wto;
	_f_lms_asc.in = _in;
	_f_lms_asc.out = _out;
	_f_lms_asc.iaf = _iaf;
	f_las_init();
	f_lmn_as();
	f_las_de_init();
	ffly_hexdump(outbuf, ob_ptr);
	ffly_chrdump(outbuf, ob_ptr);
	f_lmn_exec(outbuf, ob_ptr);
	free(bed);
	close(fd);
}

