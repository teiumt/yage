# include "../y_int.h"
# include "lf.h"
# include "../mort/malloc.h"
# include "../mort/stdio.h"
# include "../mort/string.h"
# include "struc.h"
# include "lemon.h"
static _8_u *ptr;

static _8_u stack[4096];
struct f_lmn_astrc astrc[NSEGS_A];
struct op {
	void(*func)(void);
	_8_u len;
};


void static
_ctc_brk(struct f_lmn_astrc *__arg) {
	char test[4096];
	if (__arg->brk.space>sizeof(test)) {
		printf("your going to have to do better then that, your the one that suffers if i fuckup.\n");
		return;
	}
	_int_u i;
	i = 0;
	for(;i != __arg->brk.space;i++) {
		test[i] = ' ';
	}
	test[i] = '\0';
	printf("%s", test);
}

static void(*clutch[NCTC])(struct f_lmn_astrc*) = {
	_ctc_brk
};

void static _start(void) {
	_16_u vial;
	printf("start, vial: %u\n", vial = *(_16_u*)ptr);

	struct f_lmn_astrc *s;
	s = astrc+vial;
	clutch[s->clutch](s);
}

void static _end(void) {
//	printf("end.\n");
}

void static _outs(void) {
	printf("%s", stack+*(_16_u*)ptr);
}

static struct op ops[] = {
	{_start, 2},
	{_end, 0},
	{_outs, 2}
};

void static _exec(_8_u *__ptr, _int_u __size) {
	_8_u *end;
	_8_u on;
	ptr = __ptr;
	end = ptr+__size;
	struct op *opp;
	while(ptr != end) {
		on = *(ptr++);
		opp = ops+on;
		opp->func();

		ptr+=opp->len;
	}
}

void static _loadstrca(_int_u __dst, _8_u *__src, struct f_lf_astrc *__table) {
	struct f_lmn_astrc *dst = astrc+__dst;

	dst->clutch = *(_32_u*)(__src+__table->fill[_f_lf_as_clutch]);
	dst->brk.space = *(_16_u*)(__src+__table->fill[_f_lf_as_brk_space]);
}


#define showhdr(__h)\
	printf("src: %u, len: %u\nvial: %u, n_vial: %u\n", __h->src, __h->len, __h->vial, __h->n_vial);
void f_lmn_exec(_8_u *__ptr, _int_u __size) {
	struct f_lf_hdr *h;
	h = (struct f_lf_hdr*)__ptr;
	showhdr(h)

	struct f_lf_vial *se;
	se = (struct f_lf_vial*)(__ptr+h->vial);
	_int_u i;
	i = 0;
	for(;i != h->n_vial;i++){
		printf("vial dest: %u.\n", se->n);
		_loadstrca(se->n, __ptr, &se->as);
		se++;
	}

	struct f_lf_seg *seg;
	seg = (struct f_lf_vial*)(__ptr+h->seg);
	i = 0;
	for(;i != h->n_seg;i++) {

		memcpy(stack+seg->dst, __ptr+seg->src, seg->size);
		seg++;
	}

	_exec(__ptr+h->src, h->len);
}
