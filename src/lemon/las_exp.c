# include "las.h"
# include "../mort/malloc.h"
# include "../mort/stdio.h"
# include "../mort/string.h"
# include "../strf.h"
// TODO: split up????
#define isident(__c) (__c>='a'&&__c<='z') 
#define whitespace(__c) (__c == ' ' || __c == '\t' || __c == '\0')
#define isnum(__c) (__c>='0'&&__c<='9')
f_las_symp f_las_eval(_8_u *__state) {
	char buf[256];
	char *p;
	f_las_symp s, top = NULL, end = NULL;
	char c;
_nextsym:
	s = (f_las_symp)malloc(sizeof(struct f_las_sym));
	s->p = NULL;
	s->ival = 0;
	if (!top)
		top = s;
	if (end != NULL) {
		end->next = s;
	}
	s->next = NULL;
	end = s;
_again:
	if (_f_lms_asc.iaf(0, 0)) {
		goto _eol;
	}

	c = f_las_getchr();
	if (whitespace(c))
		goto _again;
	if (c == '"') {
		s->kind = _f_las_sym_str;
		p = buf;
		do {
			c = f_las_getchr();
			if (c == '\\') {
				c = f_las_getchr();
				switch(c) {
					case 'n':
						c = '\n';			
					break;
				}
			}
			*(p++) = c;
		} while(c != '"');
		*(--p) = '\0';
		memdup(&s->p, buf, (s->len = (p-buf))+1);
	}else
	if (c == '#') {
		s->kind = _f_las_sym_sg;
		p = buf;
		do {
			c = f_las_getchr();
			*(p++) = c;
		} while(c>='a'&&c<='z');
		f_las_retract(c);
		*(--p) = '\0';
		memdup(&s->p, buf, (s->len = (p-buf))+1);
	}else
	if (c == '~' || c == '.') {
		s->kind = c == '~'?_f_las_sym_spc:_f_las_sym_var;
		p = buf;
		do {
			c = f_las_getchr();
			*(p++) = c;
		} while(c>='a'&&c<='z');
		f_las_retract(c);
		*(--p) = '\0';
		memdup(&s->p, buf, (s->len = (p-buf))+1);

	} else 
	if (isident(c)) {
		s->kind = _f_las_sym_ident;
		p = buf;
		while(isident(c)) {
			*(p++) = c;
			c = f_las_getchr();
		}
		if (c != ':' && c != '^') {
			f_las_retract(c);
		} else {
			if (c == ':')
				s->kind = _f_las_sym_label;
			else
				s->kind = _f_las_sym_label0;
		}
		*p = '\0';
	//	printf("-->%s.\n", buf);

		memdup(&s->p, buf, (s->len = (p-buf))+1);
	} else
	if (isnum(c)) {
		s->kind = _f_las_sym_int;
		p = buf;
		while(isnum(c)) {
			*(p++) = c;
			c = f_las_getchr();
		}
		f_las_retract(c);	
		s->ival = _ffly_dsn(buf, p-buf);
	}

	if (_f_lms_asc.iaf(0, 0)) {
		return top;
	}

	c = f_las_getchr();
	if (c != '\n') {
		goto _nextsym;
	}

_done:
	return top;
_eol:
	// figure of expression for like end of file but same meaning
	printf("end of the line.\n");
	*__state = 1;
	return NULL;
}
