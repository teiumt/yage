# include "wharf.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "dep/bzero.h"
# include "config.h"
static struct f_whf_con con;
static struct f_whf_stat _st;
void static
sendoff(_8_u *__code, _int_u __len) {

	struct f_whf_struc struc;
	struc.tsz = __len;
	f_whf_send(&con, &struc, sizeof(struct f_whf_struc));
	f_whf_send(&con, __code, __len);
}

void f_whf_load(_32_u __ph, void *__buf, _32_u __size) {
	_8_u code[9];
	*code = _f_whf_load;
	*(_32_u*)(code+1) = __ph;
	*(_32_u*)(code+5) = __size;
	sendoff(code, 9);
	f_whf_recv(&con, __buf, __size);
}

void f_whf_store(_32_u __ph, void *__buf, _32_u __size) {
	_8_u code[9];
	*code = _f_whf_store;
	*(_32_u*)(code+1) = __ph;
	*(_32_u*)(code+5) = __size;
	sendoff(code, 9);
	f_whf_send(&con, __buf, __size);
}

void f_whf_stat(struct f_whf_stat *__stat) {
	_8_u code[1];
	*code = _f_whf_stat;
	sendoff(code, 1);
	f_whf_recv(&con, __stat, sizeof(struct f_whf_stat));
}

void f_whf_connect(void) {
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);

	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = F_CONF(whf.ip);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(F_CONF(whf.port));

	connect(fd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
	con.fd = fd;
	f_whf_stat(&_st);
}

void f_whf_place(void) {
	struct f_whf_tc tc;
	f_whf_load(0, &tc, sizeof(struct f_whf_tc));
	F_CONF(tc.ip) = tc.addr;
}

void f_whf_disconnect(void) {
	_8_u code[1] = {_f_whf_disconnect};
	sendoff(code, 1);

	close(con.fd);
}
