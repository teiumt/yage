# ifndef __ffly__fs__h
# define __ffly__fs__h
# include "y_int.h"
/*
	TODO:
		this is diffrent then sys/fs <- user level

		also ignore this was only for fun

	this format/idea will be used in /src/felix as the 
	fs format ideal

	murge with sys/fs???
	issue
		fs/m will have a ptr to vmfs_node
		but murge will lead to contact between 
		usersurface and lowerend as M
		and i dont like that as its make stuff to high level
		only other issue

		is the double ptr function call
		e.g. 
		node->func();->func >> new_node->func();
*/
#define fs_open(__n, ...)\
	(__n)->fso->open(__n, __VA_ARGS__)
#define fs_close(__n, ...)\
	(__n)->fso->close(__n, __VA_ARGS__)
#define fs_pwrite(__n, ...)\
	(__n)->fso->pwrite(__n, __VA_ARGS__)
#define fs_pread(__n, ...)\
	(__n)->fso->pread(__n, __VA_ARGS__)
#define fs_mkdir\
	__ff_fso__->mkdir
struct ff_fso;
typedef struct f_vmfs_node {
	void *f;
	struct ff_fso *fso;
} *f_vmfs_nodep;
struct ff_fso {
	_8_s(*open)(f_vmfs_nodep, char const*, _32_u, _32_u);
	void(*close)(f_vmfs_nodep);
	void(*pwrite)(f_vmfs_nodep, void*, _int_u, _64_u);
	void(*pread)(f_vmfs_nodep, void*, _int_u, _64_u);
	void(*mkdir)(char const*);
};

enum {
	_ff_mfs
};
//vermis
f_vmfs_nodep f_vmfs_open(char const*, _32_u, _32_u);
void f_vmfs_close(f_vmfs_nodep);
struct ff_fso extern *__ff_fso__;
void(*ffly_fs(_8_u))(void);
# endif /*__ffly__fs__h*/
