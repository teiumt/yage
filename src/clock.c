# include "clock.h"
// temp
struct y_clockval clockval = {
	0
};

# include "linux/time.h"
/*
	subclicks to clicks
	7 clicks = 1 second

*/
void y_clock_gettime(struct y_timespec *__spec) {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	double long nsec;
	nsec = ts.tv_nsec;

  
	double long sclks;
	/*
		dont like this but we need nsec to fill the void
	*/
	sclks = nsec*(((double long)CLICK_BASE)*.000000001);
	_64_u _sclks = (_64_u)sclks;

	__spec->higher = (ts.tv_sec<<3)|(_sclks>>27);
	/*
		the THREE bits at the end are never used so cut them out
	*/
	__spec->lower = _sclks&(CLICK_BASE>>3);
}
