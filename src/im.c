#include "im.h"
#include "m_alloc.h"
#include "string.h"
#include "io.h"
#include "linux/unistd.h"
#include "linux/fcntl.h"
#include "linux/stat.h"

void static _read(_ulonglong __fd, void *__buf, _int_u __size) {
	read(__fd,__buf,__size);
}
void static _pread(_ulonglong __fd, void *__buf, _int_u __size,_64_u __offset) {
	pread(__fd,__buf,__size,__offset);
}
void(*im_read)(_ulonglong,void*,_int_u) = _read;
void(*im_pread)(_ulonglong,void*,_int_u,_64_u) = _pread;

struct y_img* im_creat(void) {
	return m_alloc(sizeof(struct y_img));
}
void ppm_imload(struct y_img *__im, int fd);
void im_load(struct y_img *__im,char const *__file) {
	char buf[64];
	char *bp = buf;
	_int_u ln;
	ln = str_len(__file);
	char c;
	while((c = __file[ln-1]) != '.') {
		*bp++ = c;
		ln--;
	}
	*bp = '\0';
	int fd;
	fd = open(__file,O_RDONLY,0);
	if (fd == -1) {
		printf("failed to open file.\n");
		return;
	}
	__im->fd = fd;
	if (!mem_cmp(buf,"mpp",bp-buf)) {
		ppm_imload(__im,fd);
	}
}


