# ifndef __ffly__clay__h
# define __ffly__clay__h
# include "y_int.h"
# include "clay/hash.h"
/*
	NOTE: single values dont exist but exist as a trove of things,
	an accumulation of stuff(an array)
*/
#define _clay_int(__p) \
	(*(_64_u*)_clay_gettext(__p,0))
#define _clay_float(__p) \
	(*(float*)_clay_gettext(__p,0))
typedef struct clay {
	_8_u *p;
	_32_u off, end;
	struct clay_hash table;
} *clayp;

struct clay_token {
	_8_u kind;
	_int_u sz;
	_8_u val;
	void *data;
	_64_u bits;
};

struct clay_val {
	_8_u type;
};

struct clay_trove {
	struct clay_hash table;
/*
	aligned to 8-byte segmented blocks
*/
	_64_u array_vals[16];
	struct clay_val array[16];
	_int_u n;
};

enum {
	_clay_str,
	_clay_int,
	_clay_float
};

enum {
	_clay_val,
	_clay_trove
};

enum {
	_clay_tok_str,
	_clay_tok_keywd,
	_clay_tok_no,
	_clay_tok_ident
};

enum {
	_clay_l_brace,
	_clay_r_brace,
	_clay_tilde
};


_8_u clay_at_eof(void);
void clay_init(clayp);
void clay_de_init(clayp);
_8_s clay_nexttokis(clayp, _8_u, _8_u);
_8_u clay_reckontok(clayp, _8_u, _8_u);
void clay_load(clayp, char const*);
void clay_read(clayp);
void* clay_get(char const*, clayp);
void* clay_tget(char const*, void*);
#define clay_gettext(...)
void *_clay_gettext(void*,_int_u);
void* clay_array(void*);
# endif /*__ffly__clay__h*/
