#ifndef __citric__h
#define __citric__h
#include "../yarn/yarn.h"
#include "ct_types.h"
#include "ct_font.h"
#include "../log.h"
#define ct_printf(...) log_printf(&ct_com.log, __VA_ARGS__)

/*
	all render shenanigans for ct is here,
	to allow use for things other then ct to reduce code replecation
*/
#define CY_KEY_a				0
#define CY_KEY_b				1
#define CY_KEY_c				2
#define CY_KEY_d				3
#define CY_KEY_e				4
#define CY_KEY_f				5
#define CY_KEY_g				6
#define CY_KEY_h				7
#define CY_KEY_i				8
#define CY_KEY_j				9
#define CY_KEY_k				10
#define CY_KEY_l				11
#define CY_KEY_m				12
#define CY_KEY_n				13
#define CY_KEY_o				14
#define CY_KEY_p				15
#define CY_KEY_q				16
#define CY_KEY_r				17
#define CY_KEY_s				18
#define CY_KEY_t				19
#define CY_KEY_u				20
#define CY_KEY_v				21
#define CY_KEY_w				22
#define CY_KEY_x				23
#define CY_KEY_y				24
#define CY_KEY_z				25

#define CY_KEY_A				26
#define CY_KEY_B				27
#define CY_KEY_C				28
#define CY_KEY_D				29
#define CY_KEY_E				30
#define CY_KEY_F				31
#define CY_KEY_G				32
#define CY_KEY_H				33
#define CY_KEY_I				34
#define CY_KEY_J				35
#define CY_KEY_K				36
#define CY_KEY_L				37
#define CY_KEY_M				38
#define CY_KEY_N				39
#define CY_KEY_O				40
#define CY_KEY_P				41
#define CY_KEY_Q				42
#define CY_KEY_R				43
#define CY_KEY_S				44
#define CY_KEY_T				45
#define CY_KEY_U				46
#define CY_KEY_V				47
#define CY_KEY_W				48
#define CY_KEY_X				49
#define CY_KEY_Y				50
#define CY_KEY_Z				51


#define CY_BTN_LEFT		 52
#define CY_BTN_RIGHT		53


#define CY_KEY_ESC					54
#define CY_KEY_SPACE		55
#define CY_KEY_LEFTSHIFT		56
#define CY_KEY_UP			 57
#define CY_KEY_DOWN		 58
#define CY_KEY_LEFT		 59
#define CY_KEY_RIGHT		60

#define CY_F1					 61
#define CY_F2					 62
#define CY_F3					 63
#define CY_F4					 64
#define CY_F5					 65
#define CY_F6					 66
#define CY_F7					 67
#define CY_F8					 68
#define CY_F9					 69
#define CY_F10					70
#define CY_F11					71
#define CY_F12					72
#define CY_KEY_BACKSPACE 73
#define CY_DOT			74
#define CY_SLASH		75
#define CT_RELEASE			 0
#define CT_PRESS				 1
#define CT_PTR_M				 2
struct ct_msg {
	_64_u code;
	_64_u value,id;
	_64_s x,y;
	_64_u ctx;
};

#define CTMSG_USERDATA _64_u userdata[64];


struct ct_msg_ {
	struct ct_msg m;
	CTMSG_USERDATA
};

struct ct_driver;
struct ct_gc;
struct ct_surface {
		_64_u userdata[128];
		_int_u width, height;
		struct ct_driver *drv;
		struct ct_gc *g;
		void *render;
};

struct ct_driver {
		void(*connect)(void);
		void(*close)(void);
		void(*surf_creat)(struct ct_surface*);
		void(*surf_map)(struct ct_surface*,_int_u,_int_u,_int_u,_int_u);
		void(*surf_unmap)(struct ct_surface*);
		void(*swapbufs)(struct ct_surface*);
	_int_u(*message)(struct ct_msg_*);
	void(*surf_move)(struct ct_surface*,_int_u,_int_u);
};
#define CT_VPSX(__v) ((__v)->scale[0])
#define CT_VPSY(__v) ((__v)->scale[1])
struct ct_viewport {
		_64_u private[64];
		float translate[4];
		float scale[4];
};

struct ct_gc {
		void(*init)(void);
		void(*crect)(float*,_int_u,float,float,float,float);
		void(*ui_rect)(float*,_int_u,struct yarn_texture*,float,float,float,float,float*);
	void(*viewport)(struct ct_viewport*);
		void(*framebuffer)(_int_u,_int_u);
		void(*clear)(struct ct_surface*);
		void(*rtgs)(void**,_int_u);
		/*
				init a private data of a viewport
		*/
		void(*initvp)(struct ct_viewport*);
		void(*tex_new)(struct yarn_texture*,_int_u,_int_u,struct yarn_fmtdesc*);
		void(*tex_destroy)(struct yarn_texture*);
		void(*tex_map)(struct yarn_texture*);
		void(*tex_unmap)(struct yarn_texture*);
	_64_u *nfmt;
		_64_u *dfmt;
};

struct ct_alloc_8_8 {
	_64_u at;
	_64_u free[256];
};

struct ct_struc {
	struct yarn_texture _8_8;
	struct ct_alloc_8_8 maps_alloc;
	ct_fontp fnt;
	struct ct_gc	*g;
	struct log_struc log;
	float transform[16];
	void(*foward)(void*);
	void(*foward2)(void*);
	void *conn;
};
#define CT_NF_FLOAT				 0
#define CT_NF_UNORM				 1

#define CT_DF_8_8_8_8			 0
#define CT_DF_32_32_32_32	 1
#define CTA_X(__n)((((__n)>>6)&15)<<3)
#define CTA_Y(__n)(((__n)>>10)<<3)
_64_u CT_8_8_ALLOC(struct ct_alloc_8_8*);
void ct_init(struct ct_gc*);
extern struct ct_struc ct_com;
extern struct ct_driver ct_mjx;
extern struct ct_gc ct_flue;
void ct_tex_fileload(struct ct_gc*,struct yarn_texture*,char const*);
void ct_tex_new(struct ct_gc *g, struct yarn_texture *__t, _int_u __width, _int_u __height, struct yarn_fmtdesc *__fmt);
void ct_tex_map(struct ct_gc *g, struct yarn_texture *__t);
void ct_tex_unmap(struct ct_gc *g, struct yarn_texture *__t);

struct ct_command * ct_cmdnew(void);
void ct_tex_unmap(struct ct_gc *g, struct yarn_texture *__t);
void ct_tex_destroy(struct ct_gc *g, struct yarn_texture *__t);
#endif/*__citric__h*/
