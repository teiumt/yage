# ifndef __ct__font__h
# define __ct__font__h
# include "../y_int.h"
# include "ct_types.h"
typedef struct ct_font *ct_fontp;
struct ct_font_driver {
	void(*load)(ct_fontp);
};

struct ct_font_char {
	_8_u *bm;
	float u0, v0;
	float u1, v1;
	_8_u init;
};

typedef struct ct_font {
	_int_u width, height;
	struct ct_font_driver *d;
	float ratio;
	struct ct_font_char cmap[0x100];
	//active char array
	_8_u *acarr;
	_int_u nac;
} *fontp;

struct ct_text_frag;
struct ct_text_line {
	struct ct_text_frag *frags;
};

struct ct_text_frag {
	char *s;
	_int_u len;
	struct ct_text_frag *next;
};

void ct_font_state_init(void);
#define FNT_HALIGN 0x01
#define FNT_VALIGN 0x02
#define FNT_bf fnt_drv
struct ct_font_driver ct_fnt_drv[20];
ct_fontp ct_font_load(void);
/*
	TODO:
		create new function to build/construct glyph list using ct_text_line or flat out string
*/
void ct_font_draw(ct_fontp, struct ct_text_line*,_int_u,_int_u, float, float, _8_u,float, struct ct_colour*, struct ct_colour*,float,float*,float);
void ct_textdraw(char const *__str,_int_u __len,float __x,float __y,float __r,float __g,float __b,float __a);
# endif /*__ct__font__h*/
