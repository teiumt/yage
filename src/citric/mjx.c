#include "citric.h"
#include "../flue/mjx.h"
#include "../flue/common.h"
#include "../assert.h"
static struct maj_conn *conn;
/*
	in the case we have a user who is a general user then the user should forward all control messages to this function
*/
void static controlmesg(struct vk_msg *m){
	if(m->type == (_VK_MSG_CONTROL|_VK_MSG_PATCH_MOVE)){
		struct maj_patch *pt;
		pt = conn->patches[m->newpatch.patch&63];
		pt->x = m->newpatch.x;
		pt->y = m->newpatch.y;
	}		
}

void ct_mjx_connect(void) {
	conn = maj_connect();
	ct_com.conn = conn;
	mjx_connect(conn);
	flue_prime();
	flue_sweep();
	FLUE_setctx(flue_ctx_new());
	flue_zbuffer(NULL);
	if(!ct_com.foward){
		ct_com.foward = controlmesg;
	}
	ct_com.foward2 = controlmesg;
}

void ct_mjx_close(void) {
	mjx_disconnect(conn);
}

struct ct_mjx_surface {
	void *priv;
};

void ct_mjx_surf_creat(struct ct_surface *__w) {
	struct ct_mjx_surface *surf = __w;

//	surf->priv = mjx_creatwindow();

}
void ct_mjx_surf_map(struct ct_surface *__w, _int_u __x, _int_u __y, _int_u __width, _int_u __height) {
	struct mjx_window *w = __w->userdata;
	mjx_wdmap(conn,__w->userdata,__x,__y,__width,__height);
	__w->render = &w->fb; 
	__w->width = __width;
	__w->height = __height;
}
void ct_mjx_surf_move(struct ct_surface *__w, _int_u __x, _int_u __y){
	struct maj_patch *pt = __w->userdata;
	pt->x = __x;
	pt->y = __y;
}

void ct_mjx_surf_unmap(struct ct_surface *__w) {
	mjx_wdunmap(conn,__w->userdata);
}

void ct_mjx_swapbufs(struct ct_surface *__w) {
	mjx_swapbufs(conn,__w->userdata);
}

static _8_u codemap[512] = {
	CY_KEY_a,
	CY_KEY_b,
	CY_KEY_c,
	CY_KEY_d,
	CY_KEY_e,
	CY_KEY_f,
	CY_KEY_g,
	CY_KEY_h,
	CY_KEY_i,
	CY_KEY_j,
	CY_KEY_k,
	CY_KEY_l,
	CY_KEY_m,
	CY_KEY_n,
	CY_KEY_o,
	CY_KEY_p,
	CY_KEY_q,
	CY_KEY_r,
	CY_KEY_s,
	CY_KEY_t,
	CY_KEY_u,
	CY_KEY_v,
	CY_KEY_w,
	CY_KEY_x,
	CY_KEY_y,
	CY_KEY_z,
	CY_KEY_A,
	CY_KEY_B,
	CY_KEY_C,
	CY_KEY_D,
	CY_KEY_E,
	CY_KEY_F,
	CY_KEY_G,
	CY_KEY_H,
	CY_KEY_I,
	CY_KEY_J,
	CY_KEY_K,
	CY_KEY_L,
	CY_KEY_M,
	CY_KEY_N,
	CY_KEY_O,
	CY_KEY_P,
	CY_KEY_Q,
	CY_KEY_R,
	CY_KEY_S,
	CY_KEY_T,
	CY_KEY_U,
	CY_KEY_V,
	CY_KEY_W,
	CY_KEY_X,
	CY_KEY_Y,
	CY_KEY_Z,	
	CY_BTN_LEFT,
	CY_BTN_RIGHT,
	CY_KEY_ESC,
	CY_KEY_SPACE,
	CY_KEY_LEFTSHIFT,
	CY_KEY_UP,
	CY_KEY_DOWN,
	CY_KEY_LEFT,
	CY_KEY_RIGHT,
	CY_F1,
	CY_F2,
	CY_F3,
	CY_F4,
	CY_F5,
	CY_F6,
	CY_F7,
	CY_F8,
	CY_F9,
	CY_F10,
	CY_F11,
	CY_F12,
	CY_KEY_BACKSPACE,
	CY_DOT,
	CY_SLASH
};

static _8_u valmap[512] = {
	[VK_RELEASE]	= CT_RELEASE,
	[VK_PRESS]		= CT_PRESS,
	[VK_PTR_M]		= CT_PTR_M
};

_int_u ct_mjx_msg(struct ct_msg_ *__m) { 
	struct {
		_32_u cnt;
		struct vk_msg buf[16];
	}__attribute__((packed)) msgbuf;
	maj_msg(conn, NULL, &msgbuf, sizeof(msgbuf));
	ct_printf("message buffer with size: %u.\n",msgbuf.cnt);
	if(!msgbuf.cnt)
		return NULL;
	struct vk_msg *mb = msgbuf.buf, *_m;
	if (msgbuf.cnt>16) {
		printf("too many messages to process, error!, got %u\n",msgbuf.cnt);
	}
	assert(msgbuf.cnt<=16);
	struct ct_msg_ *m;
	_int_u i = 0;
	_int_u j = 0;
	for(;i != msgbuf.cnt;i++){
		m = __m+j;
		_m = mb+i;	
		/*
			if we get a control message then filter it out and pass it to the user to decide whats done with it.
			TODO:
				push list of all control messages and not just one at a time.
		*/
		if((_m->type&0xff) == _VK_MSG_CONTROL){
			ct_com.foward(_m);
			continue;
		}
		m->m.x = _m->x;
		m->m.y = _m->y;
		m->m.id = 0;
		ct_printf("messages in{code: %u, value: %u}, %u, %u.\n",_m->code,_m->value,_m->x,_m->y);
		m->m.code = _m->code<512?codemap[_m->code]:~0;
		m->m.value = _m->value<512?valmap[_m->value]:~0;
		j++;
	}
	return j;
}
