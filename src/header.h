# ifndef __f__header__h
# define __f__header__h
# include "h_int.h"
/*
	BREF:
		all files that are to be used here must use this header
		this includes text files i know this will cause issues 
		but once we have are own text editor and tools they wont show the header
*/
enum {
	_f_remf
};
/*

	this header works like this


	we have 512 space for this header and the private one
	all must fit within 512-bytes
*/
# include "version.h"
#define FH_BS 512
#define FH_IL 5
#define FH_MAG0 '2'
#define FH_MAG1 '1'
#define FH_MAG2 '2'
#define FH_MAG3 '9'
#define FH_MAG4 '9'
#define FH_VERS (ffly_version_int)//0x0000000000000000
struct f_fh {
	_8_u ident[FH_IL];
	DWORD id;
	QWORD vers;
};

# endif /*__f__header__h*/
