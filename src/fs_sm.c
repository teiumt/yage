# include "fs_sm.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "tmh.h"
# include "msg.h"
# include "system/io.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_FSSM)
/*
	
	storage device
*/
// not previsly mapped
#define NPM 0x01
struct context;
struct tmh_ctx {
	struct context *ctx;
	f_tmh_wokp w;
	_64_u height;
};

struct stdev;
struct context {
	_8_u flags;
	void *devctx;
	struct stdev *d;
};

struct stdev {
	struct f_fsus_f f;
	void*(*ctx_new)(struct context*);
	void(*ctx_destroy)(void);
};


void static*
_tmh_ctx_new(struct context *__ctx) {
	struct tmh_ctx *ct;
	ct = (struct tmh_ctx*)__f_mem_alloc(sizeof(struct tmh_ctx));
	ct->ctx = __ctx;
/*
	later this will be automatic
*/
	ct->w = f_tmh_open(0);
	if (!(ct->w->flags&F_TMH_WMAPPED)) {
		printf("not previsly mappped to mapping.\n");
		f_tmh_map(ct->w, 2048);
		__ctx->flags = NPM;
	}
	ct->height = 2048;
	return ct;
}

void static
_tmh_ctx_destroy(struct tmh_ctx *__ctx) {

}

void static
_tmh_read(struct tmh_ctx *__ctx, void *__buf, _int_u __size, _64_u __offset) {
	if (__offset>=__ctx->height) {
		MSG(INFO, "max height is %u your trying to access %u but its nonexistent.\n", __ctx->height, __offset)
		return;
	}
	f_tmh_read(__ctx->w, __buf, __size, __offset);
}

void static
_tmh_write(struct tmh_ctx *__ctx, void *__buf, _int_u __size, _64_u __offset) {
	if (__offset>=__ctx->height) {
		MSG(INFO, "max height is %u your trying to access %u but its nonexistent.\n", __ctx->height, __offset)
		return;
	}
	f_tmh_write(__ctx->w, __buf, __size, __offset);
}

static struct stdev dev[] = {
	{
		.f={
			(void*)_tmh_write,
			(void*)_tmh_read
		},
		.ctx_new = _tmh_ctx_new,
		.ctx_destroy = _tmh_ctx_destroy
	}
};

static struct context*
_ctx_new(struct stdev *__d) {
	struct context *ct;
	ct = (struct context*)__f_mem_alloc(sizeof(struct context));

	ct->d = __d;
	ct->devctx = __d->ctx_new(ct);
	return ct;
}

void _ctx_destroy(struct context *__ctx) {

}

void f_fs_sm(f_fsus_mp __m, _8_u __what) {
	struct stdev *d;
	d = dev+__what;
	__m->f = &d->f;
	struct context *ct;
	__m->ctx = (ct = _ctx_new(d))->devctx;
	if ((ct->flags&NPM)>0) {
		__m->flags = F_FSUS_FN;
	}
}
