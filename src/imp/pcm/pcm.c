# include "pcm.h"
# include "../../linux/asound.h"
# include "../../strf.h"
# include <malloc.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdio.h>
# include <errno.h>
# include <string.h>
#define __f_mem_alloc malloc
#define __f_mem_free free
#define __f_mem_realloc realloc
/*
# include "../../memory/mem_alloc.h"
# include "../../memory/mem_free.h"
# include "../../linux/fcntl.h"
# include "../../linux/unistd.h"
*/

#define PCM_PLAYBACK	"/dev/snd/pcmC%uD%up"
void imp_pcm_open(struct imp_snd_pcm **__pcm, _int_u __card, _int_u __dev) {
	struct imp_snd_pcm *pcm;
	pcm = (struct imp_snd_pcm*)__f_mem_alloc(sizeof(struct imp_snd_pcm));

	char path[256];
	ffly_strf(path, 255, PCM_PLAYBACK, __card, __dev);
	pcm->fd = open(path, O_RDWR, 0);
	if (pcm->fd == -1) {
		printf("failed to open, %s\n", path);
	}
	*__pcm = pcm;
}

void imp_pcm_close(struct imp_snd_pcm *__pcm) {
	close(__pcm->fd);
	__f_mem_free(__pcm);
}

void imp_pcm_prep(struct imp_snd_pcm *__pcm) {
	int err;
	err = ioctl(__pcm->fd, SNDRV_PCM_IOCTL_PREPARE, 0);
	printf("prep: %d.\n", err);
}

void imp_pcm_writei(struct imp_snd_pcm *__pcm, void *__buf, _int_u __n) {
	struct snd_xferi xf;
	xf.frames = __n;
	xf.buf = __buf;
	xf.result = 0;
	int err;
	err = ioctl(__pcm->fd, SNDRV_PCM_IOCTL_WRITEI_FRAMES, (_64_u)&xf);


	printf("writei: frames: %u, %d, %d, %s\n", __n, xf.result, err, strerror(errno));
}


int main() {
	struct imp_snd_pcm *ha;
	imp_pcm_open(&ha, 0, 0);
	imp_pcm_prep(ha);
	_8_u code[256];
	imp_pcm_writei(ha, code, 4);
	imp_pcm_close(ha);
}
