# include "ad.h"
# include "m_alloc.h"
void static* dummy_alloc(_ulonglong __arg, _int_u __n) {
	return m_alloc(__n);
}
_8_s static dummy_free(_ulonglong __arg, void *__p) {
	m_free(__p);
	return 0;
}
void static* dummy_realloc(_ulonglong __arg, void *__p, _int_u __n) {
	return m_realloc(__p, __n);
}


static struct ffly_adentry atbl[2] = {
	{dummy_alloc, dummy_free, dummy_realloc},
	{NULL, NULL, NULL}
};
#define _ad_alloc(__d, __arg, __size)\
	atbl[__d].alloc(__arg, __size);
#define _ad_free(__d, __arg, __p)\
	atbl[__d].free(__arg, __p);
#define _ad_realloc(__d, __arg, __p, __nsze)\
	atbl[__d].realloc(__arg, __p, __nsze);

struct ffly_adentry* ffly_ad_entry(_16_u __n) {
	return atbl+__n;
}
void* ffly_ad_alloc(_16_u __d, _ulonglong __arg, _int_u __size) {
	return _ad_alloc(__d, __arg, __size)
}

_8_s ffly_ad_free(_16_u __d, _ulonglong __arg, void *__p) {
	_ad_free(__d, __arg, __p)
	return 0;
}

void* ffly_ad_realloc(_16_u __d, _ulonglong __arg, void *__p, _int_u __nsze) {
	return _ad_realloc(__d, __arg, __p, __nsze)
}
