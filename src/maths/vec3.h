# ifndef __ffly__vec3__h
# define __ffly__vec3__h
# include "../types.h"
typedef struct _y_vec3 {
	_y_float	x;
	_y_float	y;
	_y_float 	z;
} *y_vec3p, y_vec3;

typedef struct ffly_vec3 {
	float x, y, z;
} *ffly_vec3p;

void ffly_vec3_clr(ffly_vec3p);
float ffly_vec3_dist(const ffly_vec3p, const ffly_vec3p);
void ffly_vec3_rotate(ffly_vec3p, float);
# endif /*__ffly__vec3__h*/
