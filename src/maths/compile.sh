#!/bin/sh
$ffly_cc -c -o $dst_dir/vec2.o $root_dir/vec2.c
$ffly_cc -c -o $dst_dir/vec3.o $root_dir/vec3.c

$ffly_cc -c -o $dst_dir/min.o $root_dir/min.c
$ffly_cc -c -o $dst_dir/max.o $root_dir/max.c

#$ffly_cc -c -o $dst_dir/barycentric.o $root_dir/barycentric.c
$ffly_cc -c -o $dst_dir/rotate.o $root_dir/rotate.c
export ffly_objs="$dst_dir/vec2.o $dst_dir/vec3.o \
$dst_dir/min.o $dst_dir/max.o \
$dst_dir/rotate.o"
