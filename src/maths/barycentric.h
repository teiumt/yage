# ifndef __ffly__barycentric__h
# define __ffly__barycentric__h
# include "../y_int.h"
# include "../vertex.h"
# ifdef __cplusplus
extern "C" {
# endif
void ffly_barycentric(_int, _int, _int, ffly_vertexp, ffly_vertexp, ffly_vertexp, float*, float*, float*);
# ifdef __cplusplus
}
# endif
# endif /*__ffly__barycentric__h*/
