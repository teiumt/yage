#include "../maths.h"
#include "../y_int.h"
/*
	error: 7% of 1000000
*/
float y_sqtc(float x) {
/*
	n = mantissa*10^exponent

	if we half the exponent then it equates to a crude square root,
	as that what a square root comes down too.
*/
	_32_u y;
	y = *(_32_u*)&x;
	_32_u exp;
	exp = (y>>23)&0xff;
	y = y-(((exp-126)>>1)<<23);
	*(_32_u*)&x = y;
	return x;
}
/*
	error: -20% of 1000000
*/
_64_u y_sqtu(_64_u n) {
	_int_u x;
	x = 1;
	_int_u i = 0;
	while(n>x){x<<=1;i++;}
	return (n>>(i/2));
}
