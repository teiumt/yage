# include "uart.h"
# include <avr/io.h>
void uart_send_byte(_8_u __byte) {
	while(!(UCSR0A&(1<<UDRE0)));
	UDR0 = __byte;
}

void uart_recv_byte(_8_u *__byte) {
	while(!(UCSR0A&(1<<RXC0)));
	*__byte = UDR0;
}

void uart_send(void *__buf, _int_u __n) {
	_8_u *p;
	p = (_8_u*)__buf;
_again:	
	while(!(UCSR0A&(1<<UDRE0)));
	UDR0 = *p;		
	if (--__n>0) {
		p++;
		goto _again;
	}
}

void uart_recv(void *__buf, _int_u __n) {
	_8_u *p;
	p = (_8_u*)__buf;
_again:
	while(!(UCSR0A&(1<<RXC0)));
	*p = UDR0;
	if (--__n>0) {
		p++;
		goto _again;
	}
}

