# include "micro.h"
# include "uart.h"
# include "i2c.h"
# include <avr/io.h>
# include <util/delay.h>
# include "pic_prog.c"
//crunch
int init(void) {
// uart
    UBRR0H = (_8_u)(BAUD_PRESCALE>>8);
    UBRR0L = (_8_u)BAUD_PRESCALE;

    UCSR0C = (3<<UCSZ00); 
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
// i2c section
#define SCL_FREQ 400000
#define TWI_STAT (TWSR&0xf8)
	PORTC |= 3<<4;
    TWBR = ((F_CPU/SCL_FREQ)-16)/2;
    TWSR &= ~3;
    TWCR = 0;
    TWAMR = 0;
    TWAR = 4<<1;
}
