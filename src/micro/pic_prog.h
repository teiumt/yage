#ifndef __pic__prog__h
#define __pic__prog__h
#include "../y_int.h"
#define BUF_SH 8
#define BUF_SZ (1<<BUF_SH)
#define BUF_MSK (BUF_SZ-1)
#define CFW_BUF (6*8)
struct kt {
	_8_u cmd;
	_16_u len;
	_16_u pc;
	_16_u n;
	_16_u w;
	_16_u cw;
} __attribute__((packed));
struct cfw {
	_32_u pc;
	_16_u val;
} __attribute__((packed));
#endif/*__pic__prog__h*/
