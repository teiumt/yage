# ifndef __f__micro__i2c__h
# define __f__micro__i2c__h
# include "y_int.h"
void f_micro_i2c_init(void);
void i2c_write(void*, _int_u);
void i2c_read(void*, _int_u);
void i2c_start(void);
void i2c_stop(void);
void i2c_restart(void);
void i2c_write_byte(_8_u);
# endif /*__f__micro__i2c__h*/
