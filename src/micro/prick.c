# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <errno.h>
# include <sys/stat.h>
# include <stdint.h>
# include <stdlib.h>
# include <string.h>
# include <strings.h>
# include <termios.h>
# include <signal.h>
# include <errno.h>
# include <malloc.h>
#include <time.h>
# include "../y_int.h"
static _8_s run = 0;
static int fd;
void sig(int __sig) {
	run = -1;
	close(fd);
}

void static
_read(_8_u *__buf, _int_u __c) {
	int r;
	printf("reading: %u.\n", __c);
_ag:
	r = read(fd, __buf, __c);
	if (r<=0){
		printf("read error. %u-%d\n", __c, r);
		return;
	}
	__c-=r;
	if (__c != 0) {
		printf("more, %d\n", r);
		__buf+=r;
		goto _ag;
	}
}

void static
_write(_8_u *__buf, _int_u __c) {
	int r;
	_8_u ack = 0;
	r = read(fd, &ack, 1);
	if (r<=0) {
		printf("failed to recv ack.\n");
	} else
		printf("got ACK, %x\n", ack);

	if (ack != 0x21) {
		printf("write error, got %x.\n", ack);
		return;
	}

	r = write(fd, __buf, __c);
	if (r != __c) {
		printf("write error.\n");
	}
}

static _8_u *prog;

void prb(_64_u b, int c) {
    int i;
    for(i=0;i!=c;i++){
        printf("%u", b>>(c-1-i)&1);
    }
    printf("\n");
}

# include "pic_prog.h"
int main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("please provide program dump.\n");
		return -1;
	}
	int code;
	code = open(__argv[1], O_RDONLY);
	struct stat st;
	fstat(code, &st);
	prog = malloc(st.st_size);
	read(code, prog, st.st_size);
	close(code);
	signal(SIGINT, sig);
	fd = open("/dev/ttyUSB0", O_RDWR|O_SYNC|O_NOCTTY);
	if (fd == -1) {
		printf("failed to open.\n");
		return -1;
	}

	struct termios tty;
	memset(&tty, 0, sizeof(struct termios));
	tcgetattr(fd, &tty);
	cfsetospeed(&tty, B19200);
	cfsetispeed(&tty, B19200);

	tty.c_cflag = (tty.c_cflag&~(CSIZE|PARENB|CSTOPB|CRTSCTS))|CS8|CLOCAL|CREAD;
	tty.c_lflag &= ~(ECHO|ECHONL|ICANON|ISIG|IEXTEN);
	tty.c_iflag &= ~(IUCLC|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL|IXON);
	tty.c_oflag &= ~OPOST;

	tty.c_iflag |= IGNBRK|IGNPAR;
	tty.c_cc[VTIME] = 0;
	tty.c_cc[VMIN] = 1;
	if (tcsetattr(fd, TCSANOW, &tty) == -1) {
		printf("error.\n");
	}
	usleep(2000000);
	tcflush(fd, TCIFLUSH);
	_int_u nn = (st.st_size<<3)/14;
	nn++;
	usleep(4000000);
	printf("done waiting.\n");
	_int_u blocks = st.st_size>>BUF_SH;
	struct kt k;
	k.cmd = 0x01;
	k.w = blocks;
	k.n = nn;
	k.pc = 0;
	k.len = st.st_size&BUF_MSK;
	struct cfw cf[] = {
		{0x8007,(1<<4)|256|4},
		{0x8008,0xffff},
		{0x8009,0}
	};
	k.cw = sizeof(cf);
	printf(">>> %u.\n", k.cw);
	int r;
	_8_u ack;
	_again:
	ack = 0x21;
	r = write(fd, &ack, 1);
	if (r<=0) {
		printf("failed to write ack got error: %d.\n", r);
		return;
	}

	r = read(fd, &ack, 1);
	printf("ACK: %x.\n", ack);
	if (r<=0) {
		printf("failed to read ack got error: %d.\n", r);
		return;
	}
	if (ack != 0x21){
		printf("bad ack %x != %x\n", ack, 0x21);
		goto _again;
	}
	usleep(10000);
	double long delta;
	struct timespec t_strt, t_now;
	clock_gettime(CLOCK_MONOTONIC, &t_strt);
	printf("working with %u*%u+%u and %u.\n", k.w, BUF_SZ, k.len, k.n);
	write(fd, &k, sizeof(struct kt));
	_write(cf, k.cw);
	_int_u i;
	i = 0;
	for(;i != blocks;i++) {
		_write(prog+(i<<BUF_SH), BUF_SZ);
	}

	if (k.len>0) {
		_write(prog+(blocks<<BUF_SH), k.len);
	}

	printf("upload complete, working with %u-words.\n", nn);
	_8_u *tmp;
	_int_u words = ((nn*14)>>5)*4;
	words+=4;
	tmp = malloc(words);

	_read(tmp, words);

	printf("verifying.\n");
	_int_u mtc = 0;
	for(i = 0;i != words/4;i++) {
		_32_u w0, w1;
		printf("%u# %x == %x.\n", i, w0 = *(_32_u*)(prog+(i*4)), w1 = *(_32_u*)(tmp+(i*4)));
		if (w0 == w1) {
			mtc++;
		}
	}
	printf("%u out of %u.\n", mtc, i);
	clock_gettime(CLOCK_MONOTONIC, &t_now);
	delta = (t_now.tv_sec-t_strt.tv_sec)*1000;
	delta+=(t_now.tv_nsec-t_strt.tv_nsec)*0.000001;
	
	printf("compleation time: %llf-ms.\n", delta);
	close(fd);
	free(prog);
}
