f_cpu=16000000UL
device=atmega328p
inc_flags=
objs="main.o uart.o i2c.o"
avr-gcc -c -g -Wall $inc_flags -D__$device -DF_CPU=$f_cpu -Os -mmcu=$device -o main.o main.c

avr-gcc -c -g -Wall $inc_flags -D__$device -DF_CPU=$f_cpu -Os -mmcu=$device -o uart.o uart.c
avr-gcc -c -g -Wall $inc_flags -D__$device -DF_CPU=$f_cpu -Os -mmcu=$device -o i2c.o i2c.c
avr-gcc -g -Wall -mmcu=$device -D__$device -DF_CPU=$f_cpu $objs -o micro.elf
avr-objcopy -j .text -j .data -O ihex micro.elf micro.hex
