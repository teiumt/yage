# ifndef __f__micro__uart__h
# define __f__micro__uart__h
# include "y_int.h"
#define UART_BAUDRATE 19200
#define BAUD_PRESCALE ((F_CPU/16/UART_BAUDRATE)-1)
void uart_init(void);
void uart_send_byte(_8_u);
void uart_recv_byte(_8_u*);
void uart_send(void*, _int_u);
void uart_recv(void*, _int_u);
# endif /*__f__micro__uart__h*/
