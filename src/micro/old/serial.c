
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <errno.h>
# include <sys/stat.h>
# include <stdint.h>
# include <stdlib.h>
# include <string.h>
# include <strings.h>
# include <termios.h>
# include <signal.h>
# include <errno.h>
# include "../y_int.h"
# include "at_prog.h"
static _8_s run = 0;
int fd;
void sig(int __sig) {
	run = -1;
}
void out(void *__buf, _int_u __size) {
	_int_s res;
	_8_u *p;
	p = (_8_u*)__buf;
_again:
	if ((res = write(fd, p, __size))<=0) {
		printf("failed to write.\n");
		return;
	}
	if (res != __size) {
		if (!run) {
			__size-=res;
			p+=res;
			goto _again;
		}
	}
}
void in(void *__buf, _int_u __size) {
	_int_s res;
	_8_u *p;
	p = (_8_u*)__buf;
_again:
	if ((res = read(fd, p, __size))<=0) {
		printf("failed to read.\n");
		return;
	}
	if (res != __size) {
		if (!run) {
			__size-=res;
			p+=res;
			goto _again;
		}
	}
}

void bout(_64_u __data, _8_u __n) {
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		printf("%u", (__data>>((__n-1)-i))&0x01);
	}
	printf("\n");
}

void echo_test(void) {
	_8_u byte;
	byte = atpg_cmd_echo;
	out(&byte, 1);

	_64_u data;
	data = 0x21;
	out(&data, 8);
	data = 0;
	in(&data, 8);

	if (data != 0x21) {
		printf("echo failure malformed reply.\n");
	} else {
		printf("echo success.\n");
	}
}

void read_sig(void) {
	_8_u byte = atpg_cmd_rsig;
	out(&byte, 1);
	byte = 0;
	in(&byte, 1);
	printf("sig: %x.\n", byte);
	bout(byte, 8);
}

void enter_pgm(void) {
	_8_u cmd;
	cmd = atpg_cmd_entpm;
	out(&cmd, 1);

	_8_u sig;
	sig = 0x00;
	in(&sig, 1);
	printf("got sig: %x.\n", sig);
	bout(sig, 8);
}

void exit_pgm(void) {
	_8_u cmd;
	cmd = atpg_cmd_extpm;
	out(&cmd, 1);
}

_8_u
read_mem(_16_u __addr) {
	_8_u cmd[3];
	*cmd = atpg_cmd_rmem;
	*(_16_u*)(cmd+1) = __addr;
	out(cmd, 3);

	_8_u val;
	val = 0;
	in(&val, 1);
	return val;
}

_16_u
read_data(_16_u __addr) {
	_8_u cmd[3];
	*cmd = atpg_cmd_rdata;
	*(_16_u*)(cmd+1) = __addr;
	out(cmd, 3);
	_16_u val;
	val = 0;
	in(&val, 2);
	return val;
}

int main(void) {
	signal(SIGINT, sig);
//	int fd;
	fd = open("/dev/ttyUSB0", O_RDWR|O_SYNC|O_NOCTTY);
	if (fd == -1) {
		printf("failed to open.\n");
		return -1;
	}
	struct termios tty;
	memset(&tty, 0, sizeof(struct termios));
	tcgetattr(fd, &tty);
	cfsetospeed(&tty, B19200);
	cfsetispeed(&tty, B19200);

	tty.c_cflag = (tty.c_cflag&~(CSIZE|PARENB|CSTOPB))|CS8|CLOCAL|CREAD;
	tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP
                           | INLCR | IGNCR | ICRNL | IXON);
	tty.c_cc[VTIME] = ~0;
	tty.c_cc[VMIN] = 1;
	if (tcsetattr(fd, TCSANOW, &tty) == -1) {
		printf("error.\n");
	}
	usleep(4000000);

	printf("done waiting.\n");

	echo_test();

	enter_pgm();	
	read_sig();

	int out;
	out = open("out.dump", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	_16_u addr;
	addr = 0;

	while(addr != 86) {
		_16_u val;
		val = read_data(addr);
		write(out, &val, 2);
		printf("value at %x is %x.\n", addr++, val);
		usleep(10000);
	}
	close(out);

/*	while(!run) {
		
		_8_u o;
		out(&o, 1);


		_16_u data;
		in(&data, 2);
		bout(data, 16);

		printf("%u.\n", data);
//		ack();
		usleep(1000);
	}
*/
	exit_pgm();
	
	close(fd);
	printf("good luck.\n");

}
