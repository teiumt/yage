# include "i2c.h"
# include "uart.h"
# include <avr/io.h>
# include <util/delay.h>
void i2c_write(void *__buf, _int_u __size) {
	_int_u i;
	i = 0;
	for(;i != __size;i++) {
		TWDR =  *(((_8_u*)__buf)+i);
		TWCR = (1<<TWINT)|(1<<TWEN);
		while(!(TWCR&(1<<TWINT)));
	}
}

void i2c_write_byte(_8_u __buf) {
	TWDR =  __buf;
	TWCR = (1<<TWINT)|(1<<TWEN);
	while(!(TWCR&(1<<TWINT)));
}

void i2c_start(void) {
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN);
	while(!(TWCR&(1<<TWINT)));
}
void i2c_stop(void) {
	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN);
	while(!(TWCR&(1<<TWSTO)));
}

/*
	rename to reinstature??
*/
void i2c_restart(void) {
	i2c_stop();
	i2c_start();
}

void i2c_read(void *__buf, _int_u __size) {
	_int_u i;
	i = 0;
	for(;i != __size;i++) {
		TWCR = (1<<TWINT)|(1<<TWEN)|(1>>TWEA);
		while(!(TWCR&(1<<TWINT)));
		*(((_8_u*)__buf)+i) = TWDR;
	}
}
