#ifndef __m__y_int__h
#define __m__y_int__h
#define __y__int__h
typedef signed int _8_s __attribute__((__mode__(__QI__)));
typedef unsigned int _8_u __attribute__((__mode__(__QI__)));
typedef signed int _16_s __attribute__ ((__mode__(__HI__)));
typedef unsigned int _16_u __attribute__ ((__mode__(__HI__)));
typedef signed int _32_s __attribute__ ((__mode__(__SI__)));
typedef unsigned int _32_u __attribute__ ((__mode__(__SI__)));
typedef signed int _64_s __attribute__((__mode__(__DI__)));
typedef unsigned int _64_u __attribute__((__mode__(__DI__)));

typedef _32_u _int_u;
typedef _32_s _int;

# endif /*__m__y_int__h*/
