#include <avr/io.h>
#include <util/delay.h>
#include "i2c.h"
#define GPIO 0x09
#define IODIR 0x00
#define GPPU 0x06
#define IPOL 0x01
#define MCLR	0x01
#define VCC		0x02
#define PGC		0x08
#define PGD		0x04
#define PGP		0
#define PGD_S 2
//fig word
#define FIG_W 0x40
//pin direction
_8_u static FIGD;
//pin state
_8_u static FIGP;
_8_u static FIG[6];

void static
_write(_8_u __buf, _int_u __cnt) {
	FIG[1] = GPIO;
	_8_u dat;
	_int_u i;
	for(i = __cnt;i != 0;i--) {
		FIG[2] = PGP|PGC|(dat = (((__buf>>(i-1))&1)<<PGD_S));
		i2c_start();
		i2c_write(FIG, 3);
		i2c_restart();
		_delay_us(1);
		FIG[2] = PGP|dat;
		i2c_write(FIG, 3);
		i2c_stop();
		_delay_us(1);
	}
	_delay_us(4);
}

_8_u static
_read(_int_u __cnt) {
	_8_u buf = 0x00;
	FIG[1] = IODIR;
	FIG[2] = PGD;
	i2c_start();
	i2c_write(FIG, 3);
	i2c_stop();
	FIG[1] = GPIO;
	_int_u i;
	
	for(i = __cnt;i != 0;i--) {
		FIG[2] = PGP|PGC; 
		i2c_start();
		i2c_write(FIG, 3);
		i2c_restart();

		//insure high time is adequate
		_delay_us(1);
		i2c_write(FIG, 2);

		i2c_restart();
		i2c_write_byte(FIG_W|1);
		_8_u state;
		i2c_read(&state, 1);
		buf |= ((state>>PGD_S)&1)<<(i-1);
		i2c_restart();
		FIG[2] = PGP;
		i2c_write(FIG, 3);
		i2c_stop();
		_delay_us(1);
	}
	FIG[1] = IODIR;
	FIG[2] = 0;
	i2c_start();
	i2c_write(FIG, 3);
	i2c_stop();
	_delay_us(4);

	return buf;
}

void static
enter_progm(void) {
	_delay_ms(50);
	FIG[1] = GPIO;
	FIG[2] = VCC;
	i2c_start();
	i2c_write(FIG, 3);
	i2c_stop();
	_delay_ms(50);
	FIG[2] = 0;
	i2c_start();
	i2c_write(FIG, 3);
	i2c_stop();
	_delay_ms(50);
}

void static
exit_progm(void) {
	FIG[1] = GPIO;
	FIG[2] = VCC;
	i2c_start();
	i2c_write(FIG, 3);
	i2c_stop();
	_delay_ms(50);
	FIG[2] = VCC|MCLR;
	i2c_start();
	i2c_write(FIG, 3);
	i2c_stop();
	_delay_ms(50);

}

#define NVML 0b00000000
#define NVMR 0b11111100
#define INCR 0b11111000
#define PC	 0b10000000
#define BITP 0b11100000

void static
nvml(_32_u __data) {
	_write(NVML|1<<1, 8);
	_write(__data>>15, 8);
	_write(__data>>7, 8);
	_write(__data<<1, 8);	
}

_32_u static
nvmr(void) {
	_write(NVMR|1<<1, 8);
	_32_u r = 0;
	r = _read(8)<<15;
	r |= _read(8)<<7;
	r |= _read(8)>>1;
	return r;
}

//single shot
void static
nvmlss(_32_u __data) {
	_write(NVML, 8);
	_write(__data>>15, 8);
	_write(__data>>7, 8);
	_write(__data<<1, 8);	
}

_32_u static
nvmrss(void) {
	_write(NVMR, 8);
	_32_u r = 0;
	r = _read(8)<<15;
	r |= _read(8)<<7;
	r |= _read(8)>>1;
	return r;
}



void static
pc(_32_u __pc) {
	_write(PC, 8);
	_write(__pc>>15, 8);
	_write(__pc>>7, 8);
	_write(__pc<<1, 8);
}
void static fetch(_8_u *__buf, _int_u __c) {
	uart_send_byte(0x21);
	uart_recv(__buf, __c);
}

#include "pic_prog.h"
void static
prog(struct kt *k) {
	pc(0);
	_write(0b00011000, 8);
	_delay_ms(9);

	_8_u cfw[CFW_BUF];
	fetch(cfw, k->cw);
	_int_u i;
	i = 0;
	for(;i != k->cw;) {
		_8_u *p = cfw+i;
		pc(*(_32_u*)p);
		nvmlss(*(_16_u*)(p+4));
		_write(BITP, 8);
		_delay_ms(7);
		i+=6;
	}

	pc(0);
	_32_u clud;
	_64_u bts;
	_8_u buf[BUF_SZ];
	_int_u w, c;
	_int_u row = 0;
	c = 64;
	clud = 1;
	w = k->w;
	while(w != 0) {
		fetch(buf, BUF_SZ);
		i = 0;
		for(;i != BUF_SZ/2;i++) {
			bts = bts>>16|(((_64_u)*(_16_u*)(buf+i*2))<<48);
			c-=16;
			while(c<=(64-14)) {
				_32_u word;
				word = (bts>>c)&0x3fff;
				
				nvml(word);
				c+=14;
				clud<<=1;
				if (!clud) {
					pc(row*32);
					_write(BITP, 8);
					_delay_ms(7);
					clud = 1;
					row++;
					pc(row*32);
				}
			}
		}

		w--;
	}

	_int_u left = k->len;
	if (left>0) {
		fetch(buf, left);
		i = 0;
		for(;i != left/2;i++) {
			bts = bts>>16|(((_64_u)*(_16_u*)(buf+i*2))<<48);
			c-=16;
			while(c<=(64-14)) {
				_32_u word;
				word = (bts>>c)&0x3fff;
				
				nvml(word);
				c+=14;
				clud<<=1;
				if (!clud) {
					pc(row*32);
					_write(BITP, 8);
					_delay_ms(7);
					clud = 1;
					row++;
					pc(row*32);
				}
			}

		}

	}

	if (clud != 0) { 
	pc(row*32);
	_write(BITP, 8);
	_delay_ms(7);
	}

	pc(0);

	i = 0;
	c = 64;
	_16_u wp = 0;
	for(;i != k->n;i++) {
		_64_u v = nvmr();
		bts = bts>>14|v<<50;
		c-=14;
		if (c<=32) {
			*(_32_u*)(buf+wp) = (bts>>c)&0xffffffff;
			wp+=4;
			if (wp>=BUF_SZ) {
				wp = 0;
				uart_send(buf, BUF_SZ);
			}
			c+=32;
		}
	}

	if (wp>0) {
		uart_send(buf, wp);
	}
	bts>>=c;

	uart_send(&bts, 4);
}

int init(void);
int main(void) {
	DDRB |= 1;

	PORTB |= 1;
	init();
//my i2c address
	TWAR = 0x00;
	_delay_ms(1000);
	FIG[0] = FIG_W;
	FIG[1] = IODIR;
	FIG[2] = 0;
	i2c_start();
	i2c_write(FIG, 3);
	FIG[1] = GPIO;
	FIG[2] = VCC|MCLR;
	i2c_restart();
	i2c_write(FIG, 3);
	i2c_restart();
	FIG[1] = GPPU;
	FIG[2] = 0;
	i2c_write(FIG, 3);
	i2c_restart();
	FIG[1] = IPOL;
	FIG[2] = 0;
	i2c_write(FIG, 3);
	i2c_stop();
	_delay_ms(50);

	FIGD = VCC;
	enter_progm();

	struct kt k;
	while(1) {
		PORTB &= ~1;
		_8_u ack;
	_again:
		uart_recv_byte(&ack);
		uart_send_byte(ack);
		if (ack != 0x21) {
			goto _again;
		}
		uart_recv(&k, sizeof(k));
		PORTB |= 1;
		switch(k.cmd) {
			case 0x01:
				prog(&k);
			break;
			case 0x00:
			break;
		}
	}
}
