# include "bh.h"
# include "system/io.h"
# include "signal.h"
# include "system/error.h"
# include "brick.h"
# include "storage/cistern.h"
# include "dep/bzero.h"
# include "inet.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "memory/mem_realloc.h"
/*
	move server and client code to there own file coth coth bh/server.c/client.c
*/

/*
	store bricks in there own cistern
*/
struct ff_bh bh;
/*
	TODO:
		way to create x amount of bricks
*/

# define tape(__bh, __code, __len, __err)	\
{	\
	_32_u len;	\
	len = __len;	\
	ff_net_send(__bh->sock, &len, sizeof(_32_u), 0, __err);	\
	ff_net_send(__bh->sock, __code, __len, 0, __err);	\
}

struct ffly_cistern static ctn;
_8_i static to_shut = -1;
int static sockfd;
void sig(int __sig) {
	to_shut = 0;
	shutdown(sockfd, SHUT_RDWR);
}

# include "creek.h"
int static cis_fd;


void ff_bhs_prep(void) {
	/*
		capture signal of ctl-c
	*/
	struct sigaction sa;
	f_bzero(&sa, sizeof(struct sigaction));
	sa.sa_handler = sig;
	sigaction(SIGINT, &sa, NULL);

	
	cis_fd = open("bh.cis", O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	struct creek_file crk = {
		.fd = cis_fd
	};
		
	ffly_cistern_init(&ctn, &f_creek_create(2, &crk)->rws);
}


void ff_bhs_close(void) {
	ff_net_close(bh.sock);
}

void ff_bhs_open(void) {
	_f_err_t err;
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htons(INADDR_ANY);
	addr.sin_port = htons(40960);
	FF_SOCKET *sock;

	sock = ff_net_creat(&err, _NET_PROT_TCP);
	if (_err(err))
		return;

	if (_err(err = ff_net_bind(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)))) {
		return;
	}

	sock->prot.get(0x00, (long long)&sockfd, sock->prot_ctx);
	bh.sock = sock;
}

void static
bread(long long __arg, void *__p, _8_u __bhz) {
	ffly_cistern_read(&ctn, (void*)__arg, __p, 0, bricksz(__bhz));
}

void static
bwrite(long long __arg, void *__p, _8_u __bhz) {
	ffly_cistern_write(&ctn, (void*)__arg, __p, 0, bricksz(__bhz));
}

void static
bdel(long long __arg) {
	ffly_cistern_free(&ctn, (void*)__arg);
}

_f_err_t ff_bh_bnewm(ff_bhp __bh, _8_u __sz, _32_u *__b, _int_u __n) {
	_f_err_t err;
	_8_u code[6];
	*code = _bhop_bnewm;
	*(code+1) = __sz;
	*(_32_u*)(code+2) = __n;
	tape(__bh, code, 6, &err);

	ff_net_recv(__bh->sock, __b, __n*sizeof(_32_u), 0, &err);
	_int_u i;
	i = 0;
	for(;i != __n;i++)
		printf("new brick: %u\n", __b[i]);
	return err;
}

_f_err_t ff_bh_bridm(ff_bhp __bh, _32_u *__b, _int_u __n) {
	_f_err_t err;
	_8_u code[5];
	*code = _bhop_bridm;
	*(_32_u*)(code+1) = __n;
	tape(__bh, code, 5, &err);

	_int_u i;
	i = 0;
	for(;i != __n;i++)
		printf("rid brick: %u\n", __b[i]);

	ff_net_send(__bh->sock, __b, __n*sizeof(_32_u), 0, &err);
	return err;
}

_32_u ff_bh_bnew(ff_bhp __bh, _8_u __sz, _f_err_t *__err) {
	_f_err_t err;
	_8_u code[2];
	*code = _bhop_bnew;
	*(code+1) = __sz;
	tape(__bh, code, 2, &err);

	_32_u b;
	ff_net_recv(__bh->sock, &b, sizeof(_32_u), 0, &err);
	*__err = err;
	return b;
}

_f_err_t ff_bh_brid(ff_bhp __bh, _32_u __b) {
	_f_err_t err;
	_8_u code[5];
	*code = _bhop_brid;
	*(_32_u*)(code+1) = __b;
	tape(__bh, code, 5, &err);
	return err;
}

_f_err_t ff_bh_bopen(ff_bhp __bh, _32_u __b) {
	_f_err_t err;
	_8_u code[5];
	*code = _bhop_bopen;
	*(_32_u*)(code+1) = __b;
	tape(__bh, code, 5, &err);
	return err;
}

_f_err_t ff_bh_bwrite(ff_bhp __bh, _32_u __b, void *__buf, _int_u __len, _int_u __offset) {
	_f_err_t err;
	_8_u code[13];
	*code = _bhop_bwrite;
	*(_32_u*)(code+1) = __b;
	*(_32_u*)(code+5) = __len;
	*(_32_u*)(code+9) = __offset;
	tape(__bh, code, 13, &err);
	ff_net_send(__bh->sock, __buf, __len, 0, &err);
	return err;
}

_f_err_t ff_bh_bread(ff_bhp __bh, _32_u __b, void *__buf, _int_u __len, _int_u __offset) {
	_f_err_t err;
	_8_u code[13];
	*code = _bhop_bread;
	*(_32_u*)(code+1) = __b;
	*(_32_u*)(code+5) = __len;
	*(_32_u*)(code+9) = __offset;
	tape(__bh, code, 13, &err);
	ff_net_recv(__bh->sock, __buf, __len, 0, &err);
	return err;
}

_f_err_t ff_bh_bclose(ff_bhp __bh, _32_u __b) {
	_f_err_t err;
	_8_u code[5];
	*code = _bhop_bclose;
	*(_32_u*)(code+1) = __b;
	tape(__bh, code, 5, &err);
	return err;
}

_8_i ff_bh_bexist(ff_bhp __bh, _32_u __b, _f_err_t *__err) {
	_8_u code[5];
	*code = _bhop_bexist;
	*(_32_u*)(code+1) = __b;
	tape(__bh, code, 5, __err);
	_8_i r;
	ff_net_recv(__bh->sock, &r, 1, 0, __err);
	return r;
}

struct {FF_SOCKET *sock;} client;
_8_u static *bh_cc;

void static
bh_bnewm(void) {
	_f_err_t err;
	_8_u sz;
	_32_u n;
	sz = *bh_cc;
	n = *(_32_u*)(bh_cc+1);

	_32_u *b;
	b = (_32_u*)__f_mem_alloc(n*sizeof(_32_u));

	_int_u i;
	i = 0;

	while(i != n) {
		void *p;
		b[i++] = ffly_brick_new(sz, bread, bwrite, bdel, (long long)(p = ffly_cistern_alloc(&ctn, bricksz(sz))));
		printf("new brick: %u\n", b[i-1]);
	}
	ff_net_send(client.sock, b, n*sizeof(_32_u), 0, &err);
	__f_mem_free(b);
	printf("created new brick/s - %u, size: %u\n", n, bricksz(sz));
}

void static
bh_bridm(void) {
	_f_err_t err;
	_32_u n;
	n = *(_32_u*)bh_cc;

	_32_u *b;
	b = __f_mem_alloc(n*sizeof(_32_u));
	ff_net_recv(client.sock, b, n*sizeof(_32_u), 0, &err);
	_int_u i;

	i = 0;
	while(i != n) {
		printf("rid brick: %u\n", b[i]);
		ffly_brick_rid(b[i++]);
	}
	__f_mem_free(b);
}

void static
bh_bnew(void) {
	_32_u b;

	_f_err_t err;
	_8_u sz;
	sz = *bh_cc;
	b = ffly_brick_new(sz, bread, bwrite, bdel, (long long)ffly_cistern_alloc(&ctn, bricksz(sz)));

	ff_net_send(client.sock, &b, sizeof(_32_u), 0, &err);
	printf("created new brick - %u, size: %u\n", b, bricksz(sz));
}

void static
bh_brid(void) {
	_f_err_t err;
	_32_u b;
	b = *(_32_u*)bh_cc;
	ffly_brick_rid(b);
	printf("getting rid of brick - %u\n", b);
}

void static
bh_bopen(void) {
	_f_err_t err;
	_32_u b;
	b = *(_32_u*)bh_cc;
	ffly_brick_open(b);
	printf("opening brick - %u\n", b);
}

void static
bh_bwrite(void) {
	_f_err_t err;
	_32_u b;
	_32_u len;
	_32_u off;

	b = *(_32_u*)bh_cc;
	len = *(_32_u*)(bh_cc+4);
	off = *(_32_u*)(bh_cc+8);

	ffly_brick_open(b);
	void *p;
	if (!(p = ffly_brick_get(b))) {
		// error
	}

	ff_net_recv(client.sock, p, len, 0, &err);
	printf("brick : write -%u, buflen: %u\n", b, len);
	ffly_brick_close(b);
}

void static 
bh_bread(void) {
	_f_err_t err;
	_32_u b;
	_int_u len;
	_int_u off;

	b = *(_32_u*)bh_cc;
	len = *(_32_u*)(bh_cc+4);
	off = *(_32_u*)(bh_cc+8);

	ffly_brick_open(b);
	void *p;
	if (!(p = ffly_brick_get(b))) {
		// error
	}

	ff_net_send(client.sock, p, len, 0, &err);	
	printf("brick : read -%u, buflen: %u\n", b, len);
	ffly_brick_close(b);
}

void static
bh_bclose(void) {
	_f_err_t err;
	_32_u b;
	b = *(_32_u*)bh_cc;
	ffly_brick_close(b);
	printf("closing brick -%u\n", b);
}

void static
bh_bexist(void) {
	_f_err_t err;
	_32_u b;
	b = *(_32_u*)bh_cc;
	_8_i res;
	printf("checking for existance of brick %u\n", b);
	res = ffly_brick_exist(b);
	ff_net_send(client.sock, &res, 1, 0, &err);
}

_8_i static dc;
void bh_disconnect();
// this might break idk i dont think gcc pushes stuff to stack before call and popes them at return of said function
__asm__("bh_disconnect:\n\t"
		"movb $0, dc(%rip)\n\t"
		"ret");

static void(*op[])(void) = {
	bh_bnewm,
	bh_bridm,
	bh_bnew,
	bh_brid,
	bh_bopen,
	bh_bwrite,
	bh_bread,
	bh_bclose,
	bh_disconnect,
	bh_bexist
};

_int_u static osz[] = {
	5,
	4,
	1,
	4,
	4,
	12,
	12,
	4,
	0,
	4
};

/*
	operations will be split so

asm labels:

	_r0:
	{funtion routines}

	_r1:

	{brick operations}

	e.g.
	move brick, copy brick, etc the small shit
	so no overhead from stack setup
*/
void static
texec(bh_tapep __t) {
	_8_u *end;
	bh_cc = (_8_u*)__t->text;
	end = bh_cc+__t->len;
	_8_u on;
_again:
	if (bh_cc>=end)
		return;
	on = *(bh_cc++);
	op[on]();
	bh_cc+=osz[on];
	goto _again;
}

void ff_bhs_start(void) {
	_f_err_t err;
	FF_SOCKET *peer;
	FF_SOCKET *sock;

	sock = bh.sock;
	_32_u tsz;
	bh_tapep t;
	peer = NULL;
_again:
	if (_err(err = ff_net_listen(sock))) {
		printf("failed to listen.\n");
		return;
	}
	peer = ff_net_accept(sock, &err);
	if (_err(err))
		return;

	printf("some bigshot has decided to connected.\n");

	client.sock = peer;
	dc = -1;
	while(dc == -1) {
		_32_u tsz;
		ff_net_recv(peer, &tsz, sizeof(_32_u), 0, &err);
		if (_err(err))
			break;
		t = bh_tape_new(tsz);
		printf("tape size: %u\n", tsz);
		ff_net_recv(peer, t->text, tsz, 0, &err);
		if (_err(err))
			break;
		texec(t);
		bh_tape_destroy(t);
	}

	printf("thay left.\n");
	ff_net_close(peer);
	peer = NULL;
	goto _again;
_fail:
	if (peer != NULL) {
		ff_net_close(peer);
	}

//	while(to_shut<0);
	return;
}

void ff_bhs_closedown(void) {
	to_shut = 0;
	// wait for okay
	ff_bhs_close();
}

void ff_bhs_cleanup(void) {
	ffly_cistern_de_init(&ctn);
	close(cis_fd);
}

void ff_bh_open(ff_bhp __bh) {
	_f_err_t err;
	__bh->sock = ff_net_creat(&err, _NET_PROT_TCP);
}

void ff_bh_connect(ff_bhp __bh, char const *__ipadr, _16_u __port) {
	struct sockaddr_in addr;
	f_bzero(&addr, sizeof(struct sockaddr_in));
	addr.sin_addr.s_addr = inet_addr(__ipadr);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(__port);
	ff_net_connect(__bh->sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in));
}

void ff_bh_disconnect(ff_bhp __bh) {
	// tell server we are going to disconnect and to remove are existance
	_f_err_t err;
	_8_u code;
	code = _bhop_disconnect;
	tape(__bh, &code, 1, &err);
}

void ff_bh_close(ff_bhp __bh) {
	ff_net_shutdown(__bh->sock, SHUT_RDWR);
	ff_net_close(__bh->sock);
}
