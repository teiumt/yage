# include "tools.h" 
# include "io.h"
# include "string.h"
_8_u pb_gouge = 2;
_8_u static n = 0;
_int_u static end;

char static _buf[256];
_int_u static bn;
void static
deplete(void) {
	if (!--end) {
		_buf[bn++] = '\n';		
	} else
		_buf[bn++] = ' ';
	_buf[bn] = '\0';
	printf("%s", _buf);
	bn = 0;
}

void static
out(char *__buf) {
	_int_u gc, desired;
_again:
	gc = bn>>pb_gouge;	
	desired = (1<<pb_gouge)-(bn-(gc*(1<<pb_gouge)));
	if (n>=desired) {
		mem_cpy(_buf+bn, __buf, desired);
		bn+=desired;
		n-=desired;
		__buf+=desired;
		deplete();
		if (!n)
			return;
		goto _again;
	}

	mem_cpy(_buf+bn, __buf, n);
	bn+=n;
}

void ffly_prbin(_8_u const *__bin, _int_u __n) {
	bn = 0;
	end = (__n+((1<<pb_gouge)-1))>>pb_gouge;
	_int_u b;
	_8_u const *p;
	char buf[8];
	b = __n>>3;
	p = __bin+b;
	_int_u i;
	_8_u bits;

	_8_u bs;
	bs = __n-(b*8);
	if (bs>0) {
		i = 0;
		bits = *(__bin+b);
		for(;i != bs;i++) {
			buf[i] = '0'+((bits>>((bs-1)-i))&0x01);
		}
		n = bs;
		out(buf);
	}

	while(p != __bin) {
		bits = *(--p);
		*buf = '0'+((bits>>7)&0x01);
		buf[1] = '0'+((bits>>6)&0x01);
		buf[2] = '0'+((bits>>5)&0x01);
		buf[3] = '0'+((bits>>4)&0x01);
		buf[4] = '0'+((bits>>3)&0x01);
		buf[5] = '0'+((bits>>2)&0x01);
		buf[6] = '0'+((bits>>1)&0x01);
		buf[7] = '0'+(bits&0x01);
		n = 8;
		out(buf);
	}
	
	if (bn>0)
		deplete();
}


void y_floatprint(struct f_rw_struc *__dst, float *__f, _int_u __n) {
	_int_u c = 4;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		if(!c) {
			c = 4;
			ffly_fprintf(__dst,"\n");
		}
		ffly_fprintf(__dst,"%f,",__f[i]);
		c--;
	}
	ffly_fprintf(__dst,"\n");
}

void y_mat4print(float *m) {
	printf(
		"%f, %f, %f, %f\n"
		"%f, %f, %f, %f\n"
		"%f, %f, %f, %f\n"
		"%f, %f, %f, %f\n",
		m[0],m[1],m[2],m[3],
		m[4],m[5],m[6],m[7],
		m[8],m[9],m[10],m[11],
		m[12],m[13],m[14],m[15]
	);
}

