# ifndef __f__ihc__h
# define __f__ihc__h
# include "msg.h"
#ifdef __ihc_internal
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_IHC)
#endif
struct ihc_io {
	_32_u off, end;
};

void* ihc_mem_alloc(_int_u);
void ihc_mem_free(void*);
void ihc_clean(void);
# endif /*__f__ihc__h*/
