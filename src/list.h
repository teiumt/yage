#ifndef __y__list__h
#define __y__list__h
#include "ffly_def.h"
struct y_list_entry {
	struct y_list_entry *next;
};
struct y_list {
	struct y_list_entry *head,*tail;
};


// interweaved/interwoven list
struct y_iwlist{
	struct y_iwlist *next,*prev;
};

static __FORCE_INLINE__ void y_iwlist_init(struct y_iwlist *__ls){
	__ls->next = __ls->prev = NULL;
}

static __FORCE_INLINE__ void y_iwlist_link(struct y_iwlist *__head,struct y_iwlist *__ent){
	__head->next = __ent;
	__ent->prev = __head;
	__ent->next = NULL;
}

static __FORCE_INLINE__ void y_iwlist_unlink(struct y_iwlist *__ent){
	__ent->next->prev = __ent->prev;
	__ent->prev->next = __ent->next;
}

static __FORCE_INLINE__ void y_list_init(struct y_list *__lst){
	__lst->head = __lst->tail = NULL;
}

static __FORCE_INLINE__ void y_list_insert(struct y_list *__lst, struct y_list_entry *__ent){
	if(!__lst->head)
		__lst->head = __ent;

	if(__lst->tail != NULL) {
		__lst->tail->next = __ent;
	}
	__lst->tail = __ent;
	__ent->next = NULL;
}

/*
	pickoff the head
*/
static __FORCE_INLINE__ void y_list_pickoff(struct y_list *__lst) {
	__lst->head = __lst->head->next;
	if(!__lst->head)
		__lst->tail = NULL;
}
#define y_list_for_each(__cur,__lst)\
	for(__cur = __lst->head;__cur != NULL;__cur = __cur->next){

#endif/*__y__list__h*/
