# include "rand.h"
#include "proc.h"
//# define testing
# ifdef testing

# include <stdio.h>
void print_bin(_64_u __val) {
	_8_u i = 0;
	while(i != 64) {
		printf("%c", (__val>>(63-i)&0x1)?' ':'#');
//		printf("%u", __val>>(63-i)&0x1);
		i++;
	}
	printf(" - %lu\n", __val);
}
# endif

/*
	for rvw
	this may or may not be random so take note of that 
	this is here only to help contruct other parts of the project 
*/

// not 100% random but will do for now

//# define a
//# define v
/*
	TODO:
		make use of hardware random generation

		remove #l bit
*/
#ifdef __ylib
#define rv proc->rv
#else
static _32_u dummy;
static __thread _64_u rv = &dummy;
#endif
_8_u ffgen_rand8l(void) {
#ifdef __ylib
	struct proc_context *proc;
	proc = proc_ctx_fetch();
#endif
	rv = rv>>60|rv<<4;

	_64_u a0, a1, a2, a3, a4, a5, a6, a7;
	a0 = rv;
	a1 = rv>>8;
	a2 = rv>>16;
	a3 = rv>>24;
	a4 = rv>>32;
	a5 = rv>>40;
	a6 = rv>>48;
	a7 = rv>>56;

	_64_u b0, b1, b2, b3, b4, b5, b6;
	b0 = (a0^a1)^((rv&0xff)<<56);
	b1 = (a1^a2)^(((rv&0xffff))<<48);
	b2 = (a2^a3)^(((rv&0xffffff))<<40);
	b3 = (a3^a4)^(((rv&0xffffffff))<<32);
	b4 = (a4^a5)^(((rv&0xffffffffff))<<24);
	b5 = (a5^a6)^(((rv&0xffffffffffff))<<16);
	b6 = (a6^a7)^(((rv&0xffffffffffffff))<<8);

	rv = (b0^b1^b2^b3^b4^b4^b5^b6)+1;
//	print_bin(rv);
	return rv&0xff;
}

_16_u ffgen_rand16l(void) {
	return (_16_u)ffgen_rand8l()|(_16_u)ffgen_rand8l()<<8;
}

_32_u ffgen_rand32l(void) {
	return (_32_u)ffgen_rand8l()|(_32_u)ffgen_rand8l()<<8
		|(_32_u)ffgen_rand8l()<<16|(_32_u)ffgen_rand8l()<<24;
}

_64_u ffgen_rand64l(void) {
	return (_64_u)ffgen_rand8l()|(_64_u)ffgen_rand8l()<<8
		|(_64_u)ffgen_rand8l()<<16|(_64_u)ffgen_rand8l()<<24
		|(_64_u)ffgen_rand8l()<<32|(_64_u)ffgen_rand8l()<<40
		|(_64_u)ffgen_rand8l()<<48|(_64_u)ffgen_rand8l()<<56;
}

/*
	TODO:
		add 128, 512, 1024 ...
*/

_64_u ffly_rand() {
	return ffgen_rand64l();
}

# ifdef testing
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
int main() {
/*
	_32_u freq[0xff];
	bzero(freq, sizeof(freq));
	_64_u i;
	while(i != 0xff) {
		freq[ffgen_rand8l()]++;
		i++;
	}

	i = 0;
	while(i != 0xff) {
		printf("%lu: %ld\n", i, freq[i]);
		i++;	
	}
*/
# ifdef a
	while(1) {
		ffgen_rand8l();
		usleep(100000);
	}
# endif
# ifdef v
	_32_u table[0x100];
	bzero(table, sizeof(table));	


	_64_u const n = 0xffffff;
	_8_u start;
	start = ffgen_rand8l();
	_64_u c;
	_64_u i;

	i = 0;
	while(i != n) {
		if (ffgen_rand8l() == start) {
			_32_u *p = table+start;
			*p+=c;
			start = ffgen_rand8l();
			c = 0;
		} else
			c++;
		i++;
	}

	i = 0;
	while(i != 0xff) {
		printf("%u %u : : %f\n", i, table[i], ((float)table[i])/1000);
		usleep(100000);
		i++;
	}
# endif
/*
	_int_u i;
	while(1) {
		i = 0;
		while(i != 200) {
			printf("%c", !(ffgen_rand8l()%2)?'#':' ');
			i++;
		}
		printf("\n");
		usleep(100000);
	}
*/
}
# endif
