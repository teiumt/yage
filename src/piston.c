#define MSG_SUBDOM &pisdom
# include "piston.h"
# include "cradle.h"
# include "thread.h"
# include "time.h"
# include "clock.h"
# include "maths.h"
# include "atomic.h"
# include "msg.h"
#include "graph.h"
#include "strf.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_CORE)
static SUBDOM(pisdom, "piston", 6)

struct piston_struc {
	struct pcore *core;
	tstrucp t;
	struct graph g;
	_32_u sig;
};
#include "proc.h"
#define WAITFOR 0xff
#define HZ					4096.
#define FAV_TIME TIME_INSEC(1./HZ)
_8_s static stall = -1;
_8_s static wait = 0;
_int_u static n_done = 0; 
struct piston_struc static pistons[N_PISTON];
#ifdef __vekas
#include "errand.h"
#endif
void static*
handle(struct piston_struc *__pis) {
	while(!wait);
	
	_64_u ntick = 0;
	struct pcore *core;
	core = __pis->core;
	pctx.pcore = core;
	struct y_timespec start = {0,0}, stop = {0,0}, now;
	_64_u doze_time = 0;
	_64_u doze_last = 0;
	MSG(INFO, "piston online, thread{%u:%u, PID: %u}\n", __pis->t->id, __pis->t->sig, __pis->t->pid)
	goto _sk;
	while(stall == -1) {
		start = now;
		y_clock_gettime(&stop);
	_sk:
		y_clock_gettime(&now);
		doze(doze_time, 0);



		sched_tick(core);

#ifdef __vekas
		y_errand_tick(core);
#endif	
		_64_u d;
		d = TIME_WHO(stop.lower,stop.higher)-TIME_WHO(start.lower,start.higher);
		core->delay = d;	
		_64_s dd = FAV_TIME-d;
		if (dd>=0) {
			doze_time = dd&0xffffffff;
		} else {
			doze_time = 0;
		}
		if (!((ntick&WAITFOR)^WAITFOR)) {
			double rate;
			rate = ((double)((1<<30)-1))/((double)d);
			graph_ploty(&__pis->g,rate);
			MSG(INFO, "piston has a cyclic rate of %f\n", rate)
			MSG(INFO, "piston time taken, %u-clicks, %u-subclicks\n", stop.higher-start.higher, stop.lower-start.lower)
		}
        ntick++;

	}
	atomic_incr(&n_done);
	return NULL;
}


void static opengraph(struct piston_struc *pis){
	char path[200];
	ffly_strf(path,0,"graph%u.plt",pis->core->id);
	graph_open(&pis->g,path);
}
static _8_s err = 0;
void piston(void) {
	struct piston_struc *pis;
	struct pcore *p;
	_int_u i;
	i = 0;
	for(;i != N_PISTON;i++) {
		pis = pistons+i;
		tcreat(&pis->t, (void*)handle, pis);
		if (!pis->t){err = -1;return;}
		pis->sig = pis->t->sig;
		p = &_f_cradle.pcores[i];
		pis->core = p;

		p->id = i;
		p->sig = 1<<i;
		p->delay = 0;
		opengraph(pis);
	}
	_f_cradle.npc = N_PISTON;
	wait = -1;
}

void piston_stall(void) {
	if (err == -1)return;
	stall = 0;
	while(n_done != N_PISTON);
}
