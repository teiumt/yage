# ifndef __f__tmu__h
# define __f__tmu__h
/*
time mangament uniform????/utensils
*/
// 8 clicks
#define CTSB ((~(_32_u)0)>>5)
/*
	click space is 30-bits
	and the 3 last bits are ignored as there higher
	so 27-bits are for the lower
	CLICK_BASE = one second
	2^30 = two seconds

	30-bits are used because CLICK_BASE is divisible by 7
	but also that 30/3 = 10

	also that we can divide its power by 10
	
	2^3		= 1/10 of 2^30
	2^6		= 2/10 of 2^30
	2^9		= 3/10 of 2^30
	2^12	= 4/10 of 2^30
	2^15	= 5/10 of 2^30
	2^18	= 6/10 of 2^30
	2^21	= 7/10 of 2^30
	2^24	= 8/10 of 2^30
	2^27	= 9/10 of 2^30
	2^30	= 10/10 of 2^30

	
*/
#define CLICK_BASE8		0777777777
#define CLICK_BASE		07777777777
#define CLICK_BASE7		01111111111
#define TIME_WHO(__lower,__higher) ((_64_u)__lower|((_64_u)__higher<<27))

//	(((((__higher)&7)*CLICK_BASE7)+(__lower))+((((__higher)>>3)<<1)<<30))

#define TIME_INSEC(__sec) ((_64_u)CLICK_BASE*(__sec))
/*
	time&CLICK_BASE == CLICK_BASE = 1 second
*/
#define TIME_TOSEC(__time) (((_64_u)__time+1)>>30)
// time from seconds
#define TIME_FSC(__time) (((_64_u)__time)*(7<<27))
#define TIME_FMS(__time) (((_64_u)(__time/1000))*(7<<27))
#define TIME_LSEC(__sec) (CLICK_BASE*(__sec*8)) 
# endif /*__f__tmu__h*/
