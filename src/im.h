#ifndef __y__im__h
#define __y__im__h
#include "y_int.h"
struct y_img {
	_int_u width,height;
	_64_u start;
	int fd;
	void(*rgb_8_8_8)(struct y_img*,_8_u*);
	void(*rgba_8_8_8_8)(struct y_img*,_8_u*);
	void(*rgba_rgba_float)(struct y_img*,float*);
	void(*rgb_rgba_float)(struct y_img*,float*);
};

extern void(*im_read)(_ulonglong,void*,_int_u);
extern void(*im_pread)(_ulonglong,void*,_int_u,_64_u);

struct y_img* im_creat(void);
void im_load(struct y_img*,char const*);
#endif /*__y__im__h*/
