# ifndef __ffly__drm__h
# define __ffly__drm__h
# include "y_int.h"
# include <asm/ioctl.h>
# include "linux/types.h"
#define DRM_COMMAND_BASE 0x40
#define DRM_COMMAND_END 0xA0
#define DRM_IOCTL drm_ioctl
#define DRM_MAX_MINOR   16
struct drm_gem_close {
	_32_u handle;
	_32_u __pad;
};

struct drm_pci_dev_info {
	_16_u vendor_id;
	_16_u device_id;
};

typedef int unsigned drm_context_t;
struct drm_context {
	drm_context_t handle;
	int unsigned flags;
};

struct drm_device {
	union {
		struct drm_pci_dev_info pci;
	} devinfo;
};

/**
 * Type of memory to map.
 */
enum drm_map_type {
	_DRM_FRAME_BUFFER = 0,	/**< WC (no caching), no core dump */
	_DRM_REGISTERS = 1,   /**< no caching, no core dump */
	_DRM_SHM = 2,		 /**< shared, cached */
	_DRM_AGP = 3,		 /**< AGP/GART */
	_DRM_SCATTER_GATHER = 4,  /**< Scatter/gather memory for PCI DMA */
	_DRM_CONSISTENT = 5   /**< Consistent memory for PCI DMA */
};

/**
 * Memory mapping flags.
 */
enum drm_map_flags {
	_DRM_RESTRICTED = 0x01,	  /**< Cannot be mapped to user-virtual */
	_DRM_READ_ONLY = 0x02,
	_DRM_LOCKED = 0x04,	  /**< shared, cached, locked */
	_DRM_KERNEL = 0x08,	  /**< kernel requires access */
	_DRM_WRITE_COMBINING = 0x10, /**< use write-combining if available */
	_DRM_CONTAINS_LOCK = 0x20,   /**< SHM page that contains lock */
	_DRM_REMOVABLE = 0x40,	   /**< Removable mapping */
	_DRM_DRIVER = 0x80	   /**< Managed by driver */
};

/**
 * DMA flags
 *
 * \warning
 * These values \e must match xf86drm.h.
 *
 * \sa drm_dma.
 */
enum drm_dma_flags {
	/* Flags for DMA buffer dispatch */
	_DRM_DMA_BLOCK = 0x01,		/**<
					   * Block until buffer dispatched.
					   *
					   * \note The buffer may not yet have
					   * been processed by the hardware --
					   * getting a hardware lock with the
					   * hardware quiescent will ensure
					   * that the buffer has been
					   * processed.
					   */
	_DRM_DMA_WHILE_LOCKED = 0x02, /**< Dispatch while lock held */
	_DRM_DMA_PRIORITY = 0x04,	 /**< High priority dispatch */

	/* Flags for DMA buffer request */
	_DRM_DMA_WAIT = 0x10,		 /**< Wait for free buffers */
	_DRM_DMA_SMALLER_OK = 0x20,   /**< Smaller-than-requested buffers OK */
	_DRM_DMA_LARGER_OK = 0x40	 /**< Larger-than-requested buffers OK */
};

/**
 * DRM_IOCTL_GET_MAP, DRM_IOCTL_ADD_MAP and DRM_IOCTL_RM_MAP ioctls
 * argument type.
 *
 * \sa drmAddMap().
 */
struct drm_map {
    unsigned long offset;    /**< Requested physical address (0 for SAREA)*/
    unsigned long size;  /**< Requested physical size (bytes) */
    enum drm_map_type type;  /**< Type of memory to map */
    enum drm_map_flags flags;    /**< Flags */
    void *handle;        /**< User-space: "Handle" to pass to mmap() */
                 /**< Kernel-space: kernel-virtual address */
    int mtrr;        /**< MTRR slot used */
    /*   Private data */
};

struct drm_unique {
    __k_size_t unique_len;   /**< Length of unique */
    char *unique;     /**< Unique name for driver instantiation */
};

struct drm_dma {
	int context;			  /**< Context handle */
	int send_count;		   /**< Number of buffers to send */
	int *send_indices;	/**< List of handles to buffers */
	int *send_sizes;		  /**< Lengths of data to send */
	enum drm_dma_flags flags;	 /**< Flags */
	int request_count;		/**< Number of buffers requested */
	int request_size;		 /**< Desired size for buffers */
	int *request_indices;	 /**< Buffer information */
	int *request_sizes;
	int granted_count; 
};

struct drm_set_version {
    int drm_di_major;
    int drm_di_minor;
    int drm_dd_major;
    int drm_dd_minor;
};

struct drm_dma_request {
	struct drm_dma dma;
};

struct drm_buf_pub {
	int idx;			   /**< Index into the master buffer list */
	int total;			 /**< Buffer size */
	int used;			  /**< Amount of buffer in use (for DMA) */
	void *address;		 /**< Address of buffer */
};

struct drm_buf_map {
	int count;	  /**< Length of the buffer list */
	void *virt; /**< Mmap'd area in user-virtual */
	struct drm_buf_pub *list;   /**< Buffer information */
};

struct drm_auth {
	int unsigned magic;
};

#define DRM_RDWR O_RDWR
#define DRM_CLOEXEC O_CLOEXEC
struct drm_prime_handle {
    _32_u handle;

    /** Flags.. only applicable for handle->fd */
    _32_u flags;

    /** Returned dmabuf file descriptor */
    _32_s fd;
};

struct drm_event {
    _32_u type;
    _32_u length;
};

#define DRM_EVENT_VBLANK 0x01
#define DRM_EVENT_FLIP_COMPLETE 0x02
#define DRM_EVENT_CRTC_SEQUENCE 0x03

struct drm_event_vblank {
    struct drm_event base;
    _64_u user_data;
    _32_u tv_sec;
    _32_u tv_usec;
    _32_u sequence;
    _32_u crtc_id; /* 0 on older kernels that do not support this */
};

/* Event delivered at sequence. Time stamp marks when the first pixel
 * of the refresh cycle leaves the display engine for the display
 */
struct drm_event_crtc_sequence {
    struct drm_event    base;
    _64_u           user_data;
    _64_s           time_ns;
    _64_u           sequence;
};

typedef enum drm_map_type drm_map_type_t;
typedef enum drm_map_flags drm_map_flags_t;
int drm_get_magic(int,int unsigned*);
int drm_auth_magic(int,int unsigned);

void drm_master_set(int);
void drm_master_drop(int);
void* drm_map_add(int, unsigned long, _int_u, drm_map_type_t, drm_map_flags_t);
int drm_map_rm(int, void*);
struct drm_buf_map* drm_bufs_map(int);
int drm_bufs_unmap(struct drm_buf_map*);
int drm_dma(int, struct drm_dma_request*);
int drm_ioctl(int, unsigned long, void*);
int ffly_drm_open(char const*);
void ffly_drm_close();
int drm_open_by_busid(_32_u, _8_u, _8_u, _8_u);
int drm_prime_fd_to_handle(int,int,_32_u*);
int drm_prime_handle_to_fd(int,int*,_32_u,_32_u);

int ffly_drm_context_add(int, drm_context_t*);
int ffly_drm_context_rm(int, drm_context_t);

void ffly_drm_pci_dev(struct drm_device*, int, int);

int ffly_drm_cmd(int, unsigned long, void*, unsigned long, unsigned long);
#define drm_cmdread(...)		ffly_drm_cmd(__VA_ARGS__, DRM_IOC_READ)
#define drm_cmdwrite(...) 		ffly_drm_cmd(__VA_ARGS__, DRM_IOC_WRITE)
#define drm_cmdreadwrite(...)	ffly_drm_cmd(__VA_ARGS__, DRM_IOC_READWRITE)

#define DRM_IOCTL_BASE 'd'
#define DRM_IO(nr) _IO(DRM_IOCTL_BASE, nr)
#define DRM_IOR(nr, type) _IOR(DRM_IOCTL_BASE, nr, type)
#define DRM_IOW(nr, type) _IOW(DRM_IOCTL_BASE, nr, type)
#define DRM_IOWR(nr, type) _IOWR(DRM_IOCTL_BASE, nr, type)

#define DRM_IOCTL_PRIME_HANDLE_TO_FD    DRM_IOWR(0x2d, struct drm_prime_handle)
#define DRM_IOCTL_PRIME_FD_TO_HANDLE    DRM_IOWR(0x2e, struct drm_prime_handle)

#define DRM_IOCTL_AUTH_MAGIC        DRM_IOW( 0x11, struct drm_auth)
#define DRM_IOCTL_GET_MAGIC     DRM_IOR( 0x02, struct drm_auth)
#define DRM_IOCTL_MODE_PAGE_FLIP    DRM_IOWR(0xB0, struct drm_mode_crtc_page_flip)
#define DRM_IOCTL_MODE_CURSOR2      DRM_IOWR(0xBB, struct drm_mode_cursor2)

#define DRM_IOCTL_GET_UNIQUE        DRM_IOWR(0x01, struct drm_unique)
#define DRM_IOCTL_SET_VERSION       DRM_IOWR(0x07, struct drm_set_version)
#define DRM_IOCTL_GEM_CLOSE DRM_IOW (0x09, struct drm_gem_close)
#define DRM_IOCTL_MAP_BUFS	  DRM_IOWR(0x19, struct drm_buf_map)
#define DRM_IOCTL_FREE_BUFS	 DRM_IOW( 0x1a, struct drm_buf_free)
#define DRM_IOCTL_ADD_CTX	DRM_IOWR(0x20, struct drm_context)
#define DRM_IOCTL_RM_CTX	DRM_IOWR(0x21, struct drm_context)
#define DRM_IOCTL_DMA		DRM_IOWR(0x29, struct drm_dma)
#define DRM_IOCTL_ADD_MAP       DRM_IOWR(0x15, struct drm_map)
#define DRM_IOCTL_RM_MAP        DRM_IOW( 0x1b, struct drm_map)

#define DRM_IOCTL_SET_MASTER            DRM_IO(0x1e)
#define DRM_IOCTL_DROP_MASTER           DRM_IO(0x1f)

#define DRM_IOCTL_MODE_ADDFB        DRM_IOWR(0xae, struct drm_mode_fb_cmd)
#define DRM_IOCTL_MODE_RMFB     DRM_IOWR(0xaf, unsigned int)
#define DRM_IOCTL_MODE_SETCRTC      DRM_IOWR(0xa2, struct drm_mode_crtc)
#define DRM_IOCTL_MODE_CURSOR       DRM_IOWR(0xA3, struct drm_mode_cursor)
#define DRM_IOCTL_MODE_GETRESOURCES DRM_IOWR(0xa0, struct drm_mode_card_res)
#define DRM_IOCTL_MODE_GETCONNECTOR DRM_IOWR(0xa7, struct drm_mode_get_connector)
#define DRM_IOCTL_NR(n)	 _IOC_NR(n)
#define DRM_IOC_VOID		_IOC_NONE
#define DRM_IOC_READ		_IOC_READ
#define DRM_IOC_WRITE	   _IOC_WRITE
#define DRM_IOC_READWRITE   _IOC_READ|_IOC_WRITE
#define DRM_IOC(dir, group, nr, size) _IOC(dir, group, nr, size)
# endif /*__ffly__drm__h*/
