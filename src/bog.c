#include "bog.h"
#include "clock.h"
#include "io.h"
#include "linux/time.h"
#include "time.h"
#include "tmu.h"
#include "msg.h"
#include "m_alloc.h"
#include "oddity.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_BOG)
/* small internal routines
* underground/sub/watch
*/
#include "maths.h"
#define HZ                  100.
#define FAV_TIME TIME_INSEC(1./HZ)

_8_i static stop = -1;

_8_i static did_stop = 0;
_8_i static online = -1;
/*
	not just for the clock.
*/
#define WAITFOR 0xf
#include "freight.h"
#include "m.h"
static struct y_timespec  start = {0, 0}, now = {0, 0};
static _64_u ntick = 0;
void static*
update(void *__arg_p) {
	struct y_timespec t0, t1;
	online = 0;
	y_clock_gettime(&start);
	_64_s delay;
	_64_u doze_time;
	_64_u d;
	_64_u last_tick = 0;
	goto _sk;
_again:
	y_clock_gettime(&t0);
	d = (_64_u)(t0.lower-t1.lower);
	delay = FAV_TIME-_abs(d);
	if (delay<=0) {
		doze_time = 0;
	} else {
		doze_time = delay;	
	}
_sk:
	y_clock_gettime(&t1);
	if (!((ntick&WAITFOR)^WAITFOR)) {
		struct meminfo mi;
		meminfo(&mi);
		MSG(INFO, "ntick=%u, oddity_count: %u, mem_usage: %u.%u-mb, AA: %u\n", ntick, oddity_count, mi.used>>20, mi.used&((1<<20)-1), mi.aa);
	}

//	freight_man();
	doze(doze_time, 0);
	y_clock_gettime(&now);

	clockval.val = TIME_WHO(now.lower,now.higher);

	ntick++;
	if (stop == -1)
		goto _again;
	did_stop = -1;
	return NULL;
}

#include "thread.h"
tstrucp static t;
_32_u static t_sig;
void bog_start(void) {
	MSG(INFO, "initializing.\n")
	tcreat(&t, update, NULL);
	if (!t)return;
	t_sig = t->sig;
	while(online == -1);
}

void bog_stop(void) {
	if (!t)return;
	stop = 0;
	while(!did_stop);
//	twait(t, t_sig);
}


