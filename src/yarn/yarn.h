#ifndef __yarn__h
#define __yarn__h
#include "../y_int.h"
#include "../types.h"

/*
	yarn allows us to interact with underlying graphics functions,
	without the need to use the same structures.

	used in UI, so we can interact with CITRIC without using the same structures
*/
struct yarn_viewport {
	_64_u private[64];
	float translate[4];
	float scale[4];
};

struct yarn_texture {
	void *priv;
	void *ptr;
	_int_u width, height;
};

struct yarn_buffer {
	void *priv;
	void *cpu_ptr;
};

struct yarn_fmtdesc {
	_64_u nfmt;
	_64_u dfmt;
	_int_u depth;
};
#define YAR_NF_FLOAT				 0
#define YAR_NF_UNORM				 1

#define YAR_DF_8_8_8_8			 0
#define YAR_DF_32_32_32_32	 1

void yarn_flue_viewport(struct yarn_viewport*);

void yarn_flue_buffer_new(struct yarn_buffer*,_int_u);
void yarn_flue_buffer_map(struct yarn_buffer*);
void yarn_flue_buffer_unmap(struct yarn_buffer*);
void yarn_flue_buffer_destroy(struct yarn_buffer*);

void yarn_flue_init(void);
void yarn_flue_tex_map(struct yarn_texture*);
void yarn_flue_tex_unmap(struct yarn_texture*);
void yarn_flue_tex_destroy(struct yarn_texture*);
void yarn_flue_tex_new(struct yarn_texture*,_int_u,_int_u,struct yarn_fmtdesc*);
#endif/*__yarn__h*/
