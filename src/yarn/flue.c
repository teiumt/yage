#include "yarn.h"
#include "../flue/common.h"
#include "../io.h"
#include "../string.h"
#include "../m_alloc.h"
_64_u yarn_flue_nfmt[8];
_64_u yarn_flue_dfmt[8];

void yarn_flue_init(void) {
		yarn_flue_nfmt[YAR_NF_FLOAT]					 = FLUE_NF_FLOAT;
		yarn_flue_nfmt[YAR_NF_UNORM]					 = FLUE_NF_UNORM;
		yarn_flue_dfmt[YAR_DF_8_8_8_8]				 = FLUE_DF_8_8_8_8;
		yarn_flue_dfmt[YAR_DF_32_32_32_32]		 = FLUE_DF_32_32_32_32;
}

void yarn_flue_viewport(struct yarn_viewport *__vp) {
	struct flue_viewport *v;
	v = __vp;
	v->translate[0] = __vp->translate[0];
	v->translate[1] = __vp->translate[1];
	v->translate[2] = __vp->translate[2];
	v->translate[3] = __vp->translate[3];
	v->scale[0] = __vp->scale[0];
	v->scale[1] = __vp->scale[1];
	v->scale[2] = __vp->scale[2];
	v->scale[3] = __vp->scale[3];
	flue_viewport(__vp,0,1);
}

void yarn_flue_buffer_new(struct yarn_buffer *__buf,_int_u __size) {
	__buf->priv = flue_bo_data(__size);
}
void yarn_flue_buffer_map(struct yarn_buffer *__buf) {
	flue_bo_map(__buf->priv);
	struct flue_bo *priv = __buf->priv;
	__buf->cpu_ptr = __buf->priv;
}
void yarn_flue_buffer_unmap(struct yarn_buffer *__buf) {
	flue_bo_destroy(__buf->priv);
}
void yarn_flue_buffer_destroy(struct yarn_buffer *__buf) {
	flue_bo_destroy(__buf->priv);
}

void yarn_flue_tex_map(struct yarn_texture *tx) {
	flue_texp txx = tx->priv;
	tx->ptr = txx->tx.ptr;
}

void yarn_flue_tex_unmap(struct yarn_texture *tx) {

}

void yarn_flue_tex_new(struct yarn_texture *tx,_int_u __width, _int_u __height,struct yarn_fmtdesc *__fmt) {
	struct flue_fmtdesc fmt = {.depth=__fmt->depth,.nfmt=FLUE_NFMT(yarn_flue_nfmt[__fmt->nfmt]),.dfmt=FLUE_DFMT(yarn_flue_dfmt[__fmt->dfmt])};
	tx->priv = flue_tex_new(__width,__height,&fmt,0);
	tx->width	 = __width;
	tx->height	= __height;
}

void yarn_flue_tex_destroy(struct yarn_texture *tx) {
	flue_tex_destroy(tx->priv);
}
