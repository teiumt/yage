# include "lib.h"

static char const *enctbl = "abcdefgh01234567";
_int_u y4_enc(void const *__src, char *__dst, _int_u __len) {
	_8_u buf;
	_8_u const *in = (_8_u const*)__src;
	_8_u const *e = in+__len;
	char *dst = __dst;
_again:
	buf = *(in++);	
	*dst	= enctbl[(buf&0x0f)];
	dst[1]	= enctbl[(buf>>4)];
	dst+=2;
	if (in<e)
		goto _again;
	return dst-__dst;
}

_int_u y4_dec(char const *__src, void *__dst, _int_u __len) {
	char const *in = __src;
	char const *e = in+__len;
	_8_u *dst = (_8_u*)__dst;
	_8_u buf;
	char c0, c1;
_again:
	c0 = *in;
	c1 = in[1];
	in+=2;

	if (c1>='a')
		buf = c1-'a';
	else
		buf = 8+(c1-'0');

	buf<<=4;

	if (c0>='a')
		buf |= c0-'a';
	else
		buf |= 8+(c0-'0');

	*(dst++) = buf;
	if (in<e)
		goto _again;

	return dst-(_8_u*)__dst;
}
/*
# include <stdio.h>
int main() {
	char const *in = "caaaaaaaaaaaaaaa";
	_64_u buf;
	_int_u l;
	l = f_f4_dec(in, &buf, 8*2);
	printf("%u : %u\n", buf, l);
}*/
