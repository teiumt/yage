#ifndef __y__thread__h
#define __y__thread__h
#include "y_int.h"
#include "types.h"
#include "system/errno.h"
#include "m_alloc.h"
#include "mutex.h"
#include "atomic.h"
#include "msg.h"
#include "error.h"
#include "proc.h"
#include "lib/cellar.h"
#define FFLY_TID_NULL ((_f_tid_t)~0)
#define T_REPOSESSION 0x01
/*
	thread mechanics
	or threading mechanics 'tm' for short
or thrading mechanism
*/
typedef struct thread_struc {
	_f_bool_t alive;
	_8_u bits;
	_f_tid_t id;
	_32_u sig;
	_64_u ident;
	__linux_pid_t pid;
} *tstrucp;
#ifdef __thread_internal
#define BLKSIZE (sizeof(struct thread)<<PAGE_SHIFT)

/*
  yes alot of paging here why?
  because threads are always competing with eachother
  and it holds us up.

  i.e m_alloc less calls less fighting over resources
*/
#define PAGE_SHIFT 2
#define PAGE_SIZE (1<<PAGE_SHIFT)
#define PAGE_MASK (PAGE_SIZE-1)
#define UU_PAGE_SIZE 26

#include "m_alloc.h"
typedef struct thread {
	struct thread_struc t;
	_8_s lock;
	_16_u h;
	_f_bool_t alive;
	_8_i exit;
	_f_byte_t *sp, *tls;
	void*(*routine)(void*);
	void *arg_p;
	ffly_potp pot;
	_8_u flags;
	_8_s inited;
	union{
		_32_u id;
		struct{
			_16_u id0;
			_16_u id1;
		};
	};
	_int_u idx;
} *threadp;


#define tm_hset(__h,__val)	*(_tm.holes+(__h)) = (void*)(__val)
#define tm_hget(__h)		*(_tm.holes+(__h))
#define TM_NHOLES 48
#define TM_DEADEND ((void*)-1)
struct tm_struc {
	struct cellar _threads;
	ff_atomic_uint_t active_threads;
	ffly_potp pot;
	_32_u sig;
	struct{
		/*
			look this is alot better then what we had before,
			this only uses a small amount of memory but acheves alot
			theres no point in rationing this
		*/
		_32_u free[64];
		_int_u off;
	}uu;

	/*
		active or non-active thread structures that contain stack and resources
	*/
	_32_u joined[64];
	_int_u next_joined;

	void *holes[TM_NHOLES];
	mlock lock;
};
#define TTLS ((struct tls_storage*)proc_ctx_fetch()->th)
struct tls_storage {
	_int_u id;
};

extern struct tm_struc _tm;


#include "ffly_def.h"
threadp static __FORCE_INLINE__ get_thrbyid(_32_u id){
  return ((threadp)_tm._threads.p[id>>16])+((_16_u*)&id)[0];
}


_y_err tm_del(threadp);
void tm_morealloc(void);
_8_s tm_unused_fetch(threadp *__th,_32_u);
_32_i tm_obtain_hole(void);
//rename to abdicate? or vacate?
void tm_relinquish_hole(_16_u);
#define MSG_BITS MSG_DOMAIN(_DOM_SYSTHREAD)
#endif

#define tgetpid(__th)\
	(__th)->pid
#define T_ALIVE	0x01
#define T_DEAD	0x02
_f_tid_t ffly_gettid();
void tinit(void);

_y_err tcreat(tstrucp*, void*(*)(void*), void*);
_y_err tkill(tstrucp, _32_u);
_8_s twait(tstrucp, _32_u);
_8_s tstate(tstrucp, _32_u, _8_u);
_y_err ffly_thread_cleanup();
#endif /*__y__thread__h*/
