# include "inform.h"
# include "ffly_def.h"

/*
	to be used later

	inform us whats going on error??? warning??? issues?????

	a call back function to user space


	lets say we have a comm warning that needs user approval??
*/
void static __inform(_16_u __what) {
	switch(__what) {
		default:
			if (ffly_inform_user != NULL)
				ffly_inform_user(__what);
	}
}

void(*ffly_inform)(_16_u) = __inform;
void(*ffly_inform_user)(_16_u) = NULL;
