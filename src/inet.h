# ifndef __ffly__inet__h
# define __ffly__inet__h
# include "y_int.h"
#define INET_ADDR(__0, __1, __2, __3) \
	(((_32_u)__0)|((_32_u)__1)<<8|((_32_u)__2)<<16|((_32_u)__3)<<24)
_32_u inet_addr(char const*);
# endif /*__ffly__inet__h*/
