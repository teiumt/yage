# ifndef __f__pt__h
# define __f__pt__h
# include "y_int.h"
/*
	pt = parallel task

	rename


	a task that is ran on all pistons

*/
#define F_PT_N 20
struct f_pt {
	void(*func)(_ulonglong);
	_ulonglong arg;
	_int_u id;
};

struct f_pts {
	struct f_pt *t[F_PT_N];
	_int_u n;
};

void f_pt_run(void);
struct f_pt* f_pt_add(void(*)(_ulonglong), _ulonglong);
extern struct f_pts _f_pts;
# endif /*__f__pt__h*/
