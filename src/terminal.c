# include "types.h"
# include "system/err.h"
# include "system/pipe.h"
# include "system/io.h"
# include "dep/str_cmp.h"
# include "dep/str_dup.h"
# include "memory/mem_free.h"
# include "system/file.h"
# include "hatch.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
char static*
read_part(char **__p, char *__buf) {
	char *p = __buf;
	while(**__p != ' ' && **__p != '\n' && **__p != '\0') *(p++) = *((*__p)++);
	*p = '\0';
	(*__p)++;
	return __buf;
}

char static*
read_part_and_dupe(char **__p, char *__buf) {
	char *part = read_part(__p, __buf);
	return f_str_dupe(part);
}

char const *help = "commands:\n"
				   "   connect [hatch-key{/tmp/hatch.XXX}],\n"
				   "   meminfo,\n"
				   "   lsvec,\n"
				   "   exit\n";

_f_err_t static
sndop(_8_u __op, _int_u __pipe) {
	_f_err_t ret;
	printf("sending op.\n");
	ret= ffly_pipe_wr8l(__op, __pipe);
	printf("sent.\n");
	return ret;
}

_f_err_t static
lsvec(_int_u __pipe) {
	if (_err(sndop(_ffly_ho_lsvec, __pipe)))
		reterr;

	_f_err_t err;
	_int_u l;
	if ((_8_i)ffly_pipe_rd8l(__pipe, &err) == -1) {
		printf("nothing to be recved.\n");
		retok; 
	}

	if (_err(err))
		reterr;

	ffly_pipe_read(&l, sizeof(_int_u), __pipe); 
	if (_err(err))
		reterr;
	printf("buffer size: %u\n", l);
	char *s = (char*)__f_mem_alloc(l);
_next:
	ffly_pipe_read(s, l, __pipe);
	if (_err(err))
		reterr;
	printf("%s", s);
	if (!ffly_pipe_rd8l(__pipe, &err)) {
		goto _next;
	}
	if (_err(err))
		reterr;

	__f_mem_free(s);
}

void static lsmap(_int_u __pipe) {
}

_f_err_t ffmain(int __argc, char **__argv) {
	char buf[200];
	char line[200];
	char *p;
	_f_err_t err;
	_int_u pipe;
	_8_i conn = -1;
	while(1) {
		p = line;
		printf("~: ");
		ffly_fdrain(_ffly_out);
		ffly_rdline(line, 200, ffly_in);
		char *part = read_part(&p, buf);
		if (!conn) {
			if (!f_str_cmp(part, "meminfo")) {
				if (_err(sndop(_ffly_ho_meminfo, pipe)))
					break;
				struct ffly_meminfo info;
				ffly_pipe_read(&info, sizeof(struct ffly_meminfo), pipe);
				if (_err(err))
					break;
				printf("used: %u\n", info.used);
			} else if (!f_str_cmp(part, "lsvec"))
				lsvec(pipe);
			else if (!f_str_cmp(part, "lsmap"))
				lsmap(pipe);
			else if (!f_str_cmp(part, "shutdown")) {
				if (_err(sndop(_ffly_ho_shutdown, pipe)))
					break;
				conn = -1;
				ffly_pipe_close(pipe);
			} else
				goto _sk;
			continue;
		}

	_sk:
		if (!f_str_cmp(part, "connect")) {
			char *key = read_part_and_dupe(&p, buf);
			FF_FILE *file = ffly_fopen(key, FF_O_RDONLY, 0, &err);

			_int_u shm_id;
			ffly_fread(file, &shm_id, sizeof(_int_u));	 
			printf("connecting to pipe with shmid: %u\n", shm_id);
			_f_err_t err;
			pipe = ffly_pipe(8, FF_PIPE_SHMM, shm_id, &err);
			ffly_pipe_connect(pipe);
			conn = 0;  
			ffly_fclose(file); 
			__f_mem_free(key);
		} else if (!f_str_cmp(part, "help"))
			printf("%s", help);
		else if (!f_str_cmp(part, "exit")) {
			if (!conn)
				if (_err(sndop(_ffly_ho_disconnect, pipe)))
					break;
			printf("goodbye.\n");
			break;
		} else
			printf("unknown command, are you connected?\n");
	}

	if (!conn) {
		ffly_pipe_close(pipe);
	}
	retok;
}
