.extern mov01
.extern mov10
.globl test
test:
movlw $35
movwf $7
movlw $239
movwf $6
;sub16
movlw $1
subwf j[$6
movlw $0
subfwb j[$7
;stput
movlw $212
movwi j[$0
;bo
movlw $8
;push
;stash save
movwf $119
;stpos
;sub16
movlw $0
subwf j[$6
movlw $0
subfwb j[$7
;pop
;stash retrieve
movf $119
addwf $1
;stput
movwf $1
;add16
movlw $1
addwf j[$6
movlw $0
addwfc j[$7
ret
