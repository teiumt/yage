rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -Ddebug -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd blf_dis
gcc $cc_flags -o blfdis blf_dis.c $ffly_objs -nostdlib
