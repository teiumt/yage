
# ifndef __ffly__amdgpu__h
# define __ffly__amdgpu__h
# include "y_int.h"
# include "amdgpu_drm.h"

/*
	TODO:
		many banks limited to 28-bits eatch? 
		or so. as we can use the last 8 bits for a table.

		then for higher addresses we create another struct amdgpu_vam
*/
#define AMDGPU_VA_RESERVED_SIZE      (1ULL << 20)
#define AMDGPU_GPU_PAGE_SIZE 4096
#define AMDGPU_GPU_PAGE_MASK (AMDGPU_GPU_PAGE_SIZE - 1)
#define AMDGPU_GPU_PAGE_SHIFT 12
#define AMDGPU_GPU_PAGE_ALIGN(a) (((a) + AMDGPU_GPU_PAGE_MASK) & ~AMDGPU_GPU_PAGE_MASK)

#define AMDGPU_TIMEOUT_INFINITE			0xffffffffffffffffull
#define amdgpu_va_range_general(__dev) (&(__dev)->vam)
/*
	the ide here is to give us a point to sprout off from
*/
#define AMDGPU_HOLE_BANK(__x) (((__x)>>24)&0xff)
#define AMDGPU_HOLE_BANK_AMOUNT 0x100
enum {
	amdgpu_bo_handle_dma_buf_fd
};
struct amdgpu_va_hole;
struct amdgpu_boiler {
	struct amdgpu_va_hole *holes[64];
	/*
		which of the 0-63 contains a hole closest to the bottom of the bank,
		ie the largest offset
	*/
	struct amdgpu_va_hole **top;
	_64_u topoff;

};
struct amdgpu_vam {
	struct amdgpu_boiler *boiler;
	_64_u offset;
	_64_u n;
	_64_u stop;
	_64_u alloced;
	_64_u max;
};

struct amdgpu_dev {
	_8_u userdata[0x100];
	int fd;
	struct amdgpu_vam vam;
	struct drm_amdgpu_info_device info;
};

struct amdgpu_ctx {
	struct amdgpu_dev *dev;
	_32_u ctx_id;
};

struct amdgpu_cs_fence {
	struct amdgpu_ctx *ctx;
	_32_u ip_type;
	_32_u ip_inst;
	_32_u ring;
	_64_u seq_no;
};


struct amdgpu_info {
	// mhz
	_32_u gfx_sclk;
	_32_u gfx_mclk;

	// temperature in millidegrees
	_32_u temp;

	_32_u load;

	// watts
	_32_i avg_power;
};

struct amdgpu_va_hole {	
	struct amdgpu_va_hole *h, *bk;
	_64_u offset;
	_64_u size;
	char magic;
};

struct amdgpu_va {
	struct amdgpu_dev *dev;
	_64_u addr;
	_int_u size;
	struct amdgpu_vam *mgr;
};
struct amdgpu_bo {
	_64_u handle;
	struct amdgpu_dev *dev;
	_64_u size;
	void *cpu_ptr;
};

struct amdgpu_bo_import_result {
	struct amdgpu_bo *bo;
};
struct amdgpu_cs_ib_info {
	_32_u flags;
	_64_u addr;
	_32_u size;
};

struct amdgpu_cs_request {
	_64_u flags;
	_32_u ip_type;
	_32_u ip_inst;
	_32_u ring;
	_32_u ib_n;
	_64_u list_handle;
	struct amdgpu_cs_ib_info *ib;
};

struct amdgpu_bo_list {
	_64_u handle;
	struct amdgpu_dev *dev;
};
void amdgpu_va_tree(struct amdgpu_vam *__mgr);
void amdgpu_bo_export(struct amdgpu_dev*,struct amdgpu_bo*,_32_u,_32_u*);
void amdgpu_bo_import(struct amdgpu_dev*,_32_u,_32_u,struct amdgpu_bo_import_result*);
//extract device ptr from bo struct
#define BO_DEVICE(__bo) (((struct amdgpu_bo*)__bo)->dev)
struct amdgpu_bo_list* amdgpu_bo_list(struct amdgpu_dev*, _int_u, void*);
void amdgpu_bo_list_destroy(struct amdgpu_dev*, struct amdgpu_bo_list*);

_64_u amdgpu_cs_submit(struct amdgpu_ctx*, struct amdgpu_cs_request*);
int amdgpu_bo_va_op(struct amdgpu_dev*, struct amdgpu_bo*, _32_u, _64_u, _64_u, _64_u, _64_u);

struct amdgpu_dev* amdgpu_dev_init(int);
void amdgpu_dev_de_init(struct amdgpu_dev*);

void amdgpu_bo_alloc(struct amdgpu_dev*, struct amdgpu_bo*, _64_u, _64_u, _64_u, _64_u);
void amdgpu_bo_free(struct amdgpu_dev*, struct amdgpu_bo*);
void amdgpu_bo_cpu_map(struct amdgpu_dev*, struct amdgpu_bo*);
void amdgpu_bo_cpu_unmap(struct amdgpu_bo*);

struct amdgpu_ctx* amdgpu_ctx_create(struct amdgpu_dev*, _32_u);
void amdgpu_ctx_free(struct amdgpu_ctx*);
struct amdgpu_vam* amdgpu_vam_alloc(void);
void amdgpu_vam_init(struct amdgpu_vam*,_64_u, _64_u);
struct amdgpu_va* amdgpu_va_alloc(struct amdgpu_vam*, _64_u, _64_u);
void amdgpu_va_free(struct amdgpu_vam*, struct amdgpu_va*);
int ffly_amdgpu_gem_mmap(int, struct amdgpu_bo*, void**);
int ffly_amdgpu_gem_object_create(int, _64_u, _64_u, _64_u, _64_u, struct amdgpu_bo*);
int ffly_amdgpu_gem_object_free(int, struct amdgpu_bo*);

int amdgpu_cs_wait_fence(struct amdgpu_cs_fence*, _int_u, _8_u, _64_u, _32_u*, _32_u*);
int ffly_amdgpu_memory_info(int, struct drm_amdgpu_memory_info*);
int amdgpu_info(int, struct amdgpu_info*);
int ffly_amdgpu_ctx_alloc(int, struct amdgpu_ctx*);
int ffly_amdgpu_ctx_free(int, struct amdgpu_ctx*);
void amdgpu_showdevinfo(struct drm_amdgpu_info_device*);
int amdgpu_query_dev_info(int, struct drm_amdgpu_info_device*);
#endif /*__ffly__amdgpu__h*/
