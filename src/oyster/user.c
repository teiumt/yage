# include "../oyster.h"
# include "../dep/mem_dup.h"
# include "../memory/mem_alloc.h"
/*
	TODO:
		load from custom user file
*/
ff_db_userp
ff_db_get_user(ff_dbdp __d, _8_u const *__id, _int_u __il, _f_err_t *__err) {
	return ffly_map_get(&__d->users, __id, __il, __err); 
}

ff_db_userp
ff_db_add_user(ff_dbdp __d, _8_u const *__id, _int_u __il, _32_u __passkey) {
	ff_db_userp user = (ff_db_userp)__f_mem_alloc(sizeof(struct ff_db_user));
	f_mem_dupe((void**)&user->id, __id, __il);
	user->passkey = __passkey;
	user->loggedin = -1;

	ffly_map_put(&__d->users, __id, __il, user);
	return user;
}

void
ff_db_del_user(ff_dbdp __d, _8_u const *__id, _int_u __il, _32_u __passkey) {
	
}
