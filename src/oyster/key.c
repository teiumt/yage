# include "../oyster.h"
# include "../system/io.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../dep/mem_cpy.h"
# include "../dep/mem_cmp.h"
# include "../rand.h"
# include "../system/mutex.h"
// needs testing
/*
	NOTE:
		not finished only sketch
	connected users will need a key inorder to do anything.
*/

mlock static lock = FFLY_MUTEX_INIT;
struct node {
	_8_u *key;
	struct node *next;
};

void
ff_db_keygen(_8_u *__key) {
	_8_u *p = __key;
	while(p != __key+KEY_SIZE)
		*(p++) = ffgen_rand8l();
}

_int_u static
keysum(_8_u *__key) {
	_8_u *p = __key;
	_int_u ret = 0;
	while(p != __key+KEY_SIZE)
		ret+= *(p++);
	return ret; 
}

typedef struct node* nodep;
void
ff_db_add_key(ff_dbdp __d, _8_u *__key) {
	mt_lock(&lock);
	nodep n = (nodep)__f_mem_alloc(sizeof(struct node));
	n->key = (_8_u*)__f_mem_alloc(KEY_SIZE);
	f_mem_cpy(n->key, __key, KEY_SIZE);
	n->next = NULL;

	void **p = __d->list+(keysum(__key)&0xff);
	if (!*p)
		*p = (void*)n;
	else {
		n->next = (nodep)*p;
		*p = n;
	}
	mt_unlock(&lock);
}

void
ff_db_rm_key(ff_dbdp __d, _8_u *__key) { 
	mt_lock(&lock);
	_int_u sum = keysum(__key);
	nodep beg = (nodep)*(__d->list+(sum&0xff));
	nodep p = beg;
	nodep prev = NULL;
	while(p != NULL) {
		if (!f_mem_cmp(__key, p->key, KEY_SIZE)) {
			if (p == beg)
				*(__d->list+(sum&0xff)) = p->next; 
			else
				prev->next = p->next; 
 
			__f_mem_free(p->key);
			__f_mem_free(p);
			break; 
		}
		prev = p;
		p = p->next;
	} 
	mt_unlock(&lock);
}

_8_u
ff_db_valid_key(ff_dbdp __d, _8_u *__key) {
	mt_lock(&lock);
	_8_u ret;
	nodep p = *(__d->list+(keysum(__key)&0xff));
	while(p != NULL) {
		if (!f_mem_cmp(__key, p->key, KEY_SIZE)) {
			ret = 0;
			goto _end;
		}
		p = p->next;
	}
	ret = 1;
	_end:
	mt_unlock(&lock);
	return ret;
}

/*
# define TEST 200

int main() {
	ffly_io_init();
	struct ff_dbd daemon;
	daemon.list = (void**)__f_mem_alloc((KEY_SIZE*0xff)*sizeof(void*));
	_16_u key = 0;
	while(key != TEST) {
		ff_db_add_key(&daemon, (_8_u*)&key);
		key++;
	}

	key = 0;
	while(key != 50) {
		ff_db_rm_key(&daemon, (_8_u*)&key);
		key++;
	}
	
	key = 0;
	while(key != TEST) {
		printf("%u, valid: %s\n", key, !ff_db_valid_key(&daemon, (_8_u*)&key)?"yes":"no");
		key++;
	}
	__f_mem_free(daemon.list);
	ffly_io_closeup();
}*/


