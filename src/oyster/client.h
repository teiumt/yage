# ifndef __ffly__db__client__h
# define __ffly__db__client__h
# define __only_struc
# include "connect.h"
# include "../net.h"
# include "../types.h"
# include "../y_int.h"
typedef struct ff_db_cl {
	struct ff_db_conn conn;
	_f_tid_t thread;
	_int_u id;
} *ff_db_clp;

_int_u ff_db_client(FF_SOCKET*, void*);
void ff_db_client_destory(_int_u);
# endif /*__ffly__db__client__h*/
