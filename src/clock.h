# ifndef __y__clock__h
# define __y__clock__h
# include "y_int.h"
# include "tmu.h"
/*
	this is for non trivial things like output timing for example
	its to reduce clockget calls thru out internal process so to
	leave it all to bog.c
*/
struct y_clockval {
	_64_u val;
};
/*
	1 seconds = 1000000000 nanoseconds
	you see the problem??
	1 fits fine but NS does not
	we want SEC|NS < we want to be able to OR it
	as its simpler then
	ie 

	V = (SEC*1000000000)+NS

	also the resion why 8 = 1 seconds is because 
	to its anoying to have big numbers to delay below ONE second
	as i want to be able to do

	y_doze(0, 1); = 1/8 of a second
	and not
	y_doze(some large number ie, 1000000000, 0)
	larger numbers should be left for non interactive stuff
	yes i could just use a macro but yea




*/
#define TIME_HLMASK ((_32_u)~((_32_u)7<<29))
struct y_timespec {
	/*
		lower 32bit 0-1 of a click so subclicks

		subclicks
	*/
	_64_u lower;
	/*
		higher sec*8

		clicks
	*/
	_64_u higher;
};
#define clockv clockval.val
void y_clock_gettime(struct y_timespec*);
extern struct y_clockval clockval;
# endif /*__y__clock__h*/
