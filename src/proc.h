# ifndef __ffly__proc__h
# define __ffly__proc__h
# include "y_int.h"
//# include "memory/eh.h"
#define SCHED_PROC_DEN 0
#define NOUGHT_PROC_DEN 1
#define PROC_DEN_MAX 20

/*
	stands for process context(thread dependent)
*/
struct proc_context {
	// worstcase tls error
	char ident[3];
//	f_ehp e_heap;
	void *den[PROC_DEN_MAX];

	/*
		this is because we dont have access to the structures
	*/
	_64_u str[16];
	_64_u ar[16];
	_64_u th[16];

	_8_u flags;
	_64_u rv;
	void *pcore;
};

#define _f_eh_alloc(__size)\
	f_eh_alloc(proc_ctx_fetch()->e_heap, __size)
#define _f_eh_free(__p)\
	f_eh_free(proc_ctx_fetch()->e_heap, __p)
#define _f_eh_tree\
	f_eh_tree(proc_ctx_fetch()->e_heap);
extern __thread struct proc_context pctx;

void proc_ctx_setup(void);
void proc_ctx_cleanup(void);
#define proc_ctx_fetch() (&pctx)
# endif /*__ffly__proc__h*/
