# ifndef __sched__h
# define __sched__h
# include "sched_struc.h"
# include "pcore.h"

void sched_submit(sched_entityp);
enum{
	_sched_time_bound,
	_sched_just_run
};
sched_entityp sched_new(void);
void sched_migrate(sched_entityp,struct pcore*);
struct pcore* sched_preferred(void);
void sched_entity_attach(sched_entityp,struct pcore*);
void sched_entity_detach(sched_entityp);
void sched_tick(struct pcore*);
void sched_init(void);
void sched_de_init(void);

# endif /*__sched__h*/
