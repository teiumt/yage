#ifndef __f__assert__h
#define __f__assert__h
#include "io.h"
#include "lib.h"
void _assert(char const*,_int_u,char const*,char const*,...);
#define F_assert(__expr) massert(__expr,"\n")
#define assert(__expr) massert(__expr,"\n")
#define massert(__expr,...) if(!(__expr)){_assert(__FILE__,__LINE__,__func__,__VA_ARGS__);}
#endif /*__f__assert__h*/
