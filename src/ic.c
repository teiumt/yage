# include "ic.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "linux/unistd.h"
# include "linux/fcntl.h"
# include "system/error.h"
# include "system/io.h"
# include "system/errno.h"
# include "dep/mem_cpy.h"
# include "dep/mem_set.h"
# include "msg.h"
# include "linux/ioctls.h"
# include "linux/ioctl.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_IC)
void static
pktx_alloc(f_ic_pktp *__pkts, _int_u __n) {
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		f_ic_pktp pt;
		__pkts[i] = pt = (f_ic_pktp)__f_mem_alloc(sizeof(struct f_ic_pkt));
		pt->data = (_8_u*)__f_mem_alloc(F_IC_PKT_SIZE);
	}
}

f_ic_pktp static
pkt_alloc(void) {
	f_ic_pktp pkt;
	pktx_alloc(&pkt, 1);
	return pkt;
}

void f_ic_init(void) {

}

void f_ic_de_init(void) {

}

static struct f_ic_prot _prot[];

struct f_ic_ent static*
ent_new(struct f_ic_prot *__prot) {
	struct f_ic_ent *ent;
	ent = (struct f_ic_ent*)__f_mem_alloc(sizeof(struct f_ic_ent));
	ent->stack = (struct f_ic_stack){
		.lock = F_MUTEX_INIT,
		.ig_n = 0,
		.ig_bytes = 0,
		.npkt = 0,
		.ig = NULL,
		.og = NULL,
		.priv = NULL
	};
	ent->flags = F_IC_ENT_IGNORE;
	mt_lock(&__prot->lock);
	ent->bk = &__prot->ent;
	ent->next = __prot->ent;
	if (__prot->ent != NULL)
		__prot->ent->bk = &ent->next;
	__prot->ent = ent;
	mt_unlock(&__prot->lock);
	return ent;
}

struct tcp_context {
	f_ic_contextp ctx;
	int fd;
};

void static*
_tcp_ctx_new(f_ic_contextp __ctx) {
	struct tcp_context *ct;
	ct = __f_mem_alloc(sizeof(struct tcp_context));
	ct->ctx = __ctx;
	return ct;
}

void static
_tcp_ctx_destroy(void *__ctx) {
	__f_mem_free(__ctx);
}

void static
_tcp_mkc(struct tcp_context *__ctx, struct f_ic_cptcp *__cd) {
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	connect(fd, __cd->addr, __cd->len);
	__ctx->fd = fd;
}

void static
_tcp_mksp(struct tcp_context *__ctx, struct f_ic_cptcp *__cd) {
	int fd;
	fd = socket(AF_INET, SOCK_STREAM, 0);
	int val = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(int));
	bind(fd, __cd->addr, __cd->len);
	__ctx->fd = fd;
}

void static
_tcp_wic(struct tcp_context *__ctx, struct tcp_context *__ctx0) {
	int fd, newfd;
	fd = __ctx->fd;
	listen(fd, 4);
	struct sockaddr_in addr;
	socklen_t len;
	__ctx0->fd = accept(fd, (struct sockaddr*)&addr, &len);
}

struct f_ic_ent static*
_hook(f_ic_contextp __ctx) {
	__ctx->ent = ent_new(__ctx->prot);
	__ctx->ent->ctx = __ctx->priv_ctx;
	__ctx->ent->flags ^= F_IC_ENT_IGNORE;
	return __ctx->ent;
}

void static
_unhook(f_ic_contextp __ctx) {
	mt_lock(&__ctx->prot->lock);
	*__ctx->ent->bk = __ctx->ent->next;
	if (__ctx->ent->next != NULL)
		__ctx->ent->next->bk = __ctx->ent->bk;
	mt_unlock(&__ctx->prot->lock);
}
void static
_tcp_close(struct tcp_context *__ctx) {
	close(__ctx->fd);
}

static struct f_ic_prot _prot[] = {
	{
		.ctx_new = (void*)_tcp_ctx_new,
		.ctx_destroy = (void*)_tcp_ctx_destroy,
		.mkc = (void*)_tcp_mkc,
		.mksp = (void*)_tcp_mksp,
		.wic = (void*)_tcp_wic,
		.close = (void*)_tcp_close,
		.lock = F_MUTEX_INIT,
		.ent = NULL
	}
};

#define SEG_SHFT (7+F_IC_SHFT_BASE)
#define SEG_SIZE (1<<SEG_SHFT)
#define SEG_MASK (SEG_SIZE-1)
#define SH_SHFT (3+F_IC_SHFT_BASE)
#define SH_SIZE (1<<SH_SHFT)
#define SH_MASK (SH_SIZE-1)
#define SD_SIZE (SEG_SIZE-SH_SIZE)
/*
	how much space the header has
*/
#define HD_SPACE 32
#define CLUMP_SEGS 18
# include "hexdump.h"
void static
_tcp_ig(struct tcp_context *__ctx, struct f_ic_stack *__stack) {
	_int_u ns = CLUMP_SEGS;	
	_int_u sa = ns*SEG_SIZE;
	printf("sending %u-segments amassing to %u-bytes.\n", ns, sa);
	f_ic_pktp *pktbl;
	pktbl = (f_ic_pktp*)__f_mem_alloc(F_IC_PKT_CLUMP_SZ*sizeof(f_ic_pktp));
	f_ic_pktp pk = __stack->ig;
	_int_u i;
	i = 0;
	for(;i != F_IC_PKT_CLUMP_SZ;i++) {
		pktbl[i] = pk;
		pk = pk->next;
	}
	
	_8_u *seg_data;
	seg_data = (_8_u*)__f_mem_alloc(sa);
	
	struct f_ic_shdr *sh;
	i = 0;
	for(;i != ns;i++) {
		sh = (struct f_ic_shdr*)(seg_data+(i<<SEG_SHFT));
		sh->seg = i;
		sh->off = i*SD_SIZE;
	}

	_8_u *seg;
	seg = seg_data+SH_SIZE;
	struct f_ic_header *hdr = (struct f_ic_header*)seg;

	_int_u bof = HD_SPACE;

	_8_u *p;
	i = 0;
	for(;i != F_IC_PKT_CLUMP_SZ;i++) {
		pk = pktbl[i];
		p = pk->data;
		_int_u ii;
		ii = pk->size;
		if (bof>0) {
			_int_u needed = SD_SIZE-bof;
			f_mem_cpy(seg+bof, p, needed);
			seg+=SEG_SIZE;
			p+=needed;
			ii-=needed;
		}

		while(ii>=SD_SIZE) {
			f_mem_cpy(seg, p, SD_SIZE);
			seg+=SEG_SIZE;
			ii-=SD_SIZE;
			p+=SD_SIZE;
		}

		if (ii>0) {
			f_mem_cpy(seg, p, ii);
			bof = ii;
		}
	}

	i = 0;
	for(;i != ns;i++) {
		_8_u *p = seg_data+(i<<SEG_SHFT);
		sh = (struct f_ic_shdr*)p;
		printf("segment-%u.\n", sh->seg);
		ffly_hexdump(p+SH_SIZE, SD_SIZE);
		send(__ctx->fd, p, SEG_SIZE, 0);
	}
	__f_mem_free(seg_data);
}

void static
_tcp_og(struct tcp_context *__ctx, struct f_ic_stack *__stack) {
	_8_u *buf;
	buf = (_8_u*)__f_mem_alloc(CLUMP_SEGS*SD_SIZE);
	struct f_ic_shdr *sh;
	_8_u seg_buf[SEG_SIZE], *seg;
	sh = (struct f_ic_shdr*)seg_buf;
	seg = seg_buf+SH_SIZE;
	_32_u i = 0;
	_8_u *d;
	for(;i != CLUMP_SEGS;i++) {
		recv(__ctx->fd, seg, SEG_SIZE, 0);
		d = buf+sh->off;
		if (!sh->seg) {
			struct f_ic_header *hdr;
			hdr = (struct f_ic_header*)seg;
			f_mem_cpy(d, seg+HD_SPACE, SD_SIZE-HD_SPACE);
		} else {
			f_mem_cpy(d, seg, SD_SIZE);
		}
	}

	f_ic_pktp *pktbl, pk;
	pktbl = (f_ic_pktp*)__f_mem_alloc(F_IC_PKT_CLUMP_SZ*sizeof(f_ic_pktp));
	pktx_alloc(pktbl, F_IC_PKT_CLUMP_SZ);
	
	f_ic_pktp t, last;
	t = last = *pktbl;
	d = buf;
	i = 1;
	for(;i != F_IC_PKT_CLUMP_SZ;i++) {
		pk = pktbl[i];
		f_mem_cpy(pk->data, d, F_IC_PKT_SIZE);
		d+=F_IC_PKT_SIZE;
		last->next = pk;
		last = pk;
	}
	last->next = __stack->og;
	__stack->og = t;
	__f_mem_free(buf);
	__f_mem_free(pktbl);
}

void static
_tcp_tick(void) {
	MSG(INFO, "TCP TICK.\n");
	struct f_ic_prot *prot = _prot+_F_IC_CP_TCP;
	mt_lock(&prot->lock);
	struct f_ic_ent *ent = prot->ent;
	while(ent != NULL) {
		struct f_ic_stack *stack = &ent->stack;
		mt_lock(&stack->lock);
		struct tcp_context *ctx = (struct tcp_context*)ent->ctx;

		if (stack->npkt>=F_IC_PKT_CLUMP_SZ) {
			printf("ingoing found.\n");
			_tcp_ig(ent->ctx, stack);
			stack->ig = NULL;
			ffly_fdrain(_ffly_out);
			stack->npkt-=F_IC_PKT_CLUMP_SZ;
		}

		int bytes;
		ioctl(ctx->fd, FIONREAD, (_64_u)&bytes);
		if (bytes>0) {//yay we have data
			printf("we got incomming data yay.\n");
			_tcp_og(ent->ctx, stack);
		}
		mt_unlock(&stack->lock);
		ent = ent->next;
	}
	mt_unlock(&prot->lock);
}
# include "clock.h"
static _64_u last = 0;
void f_ic_tick(void) {
	if ((clock-last) >= TIME_FMS(1000)) {
		_tcp_tick();
		last = clock;
	}
}

f_ic_conp f_ic_mkc(void *__par, _8_u __proto) {
	f_ic_conp c;
	c = (f_ic_conp)__f_mem_alloc(sizeof(struct f_ic_con));

	c->ctx.prot = _prot+__proto;
	c->ctx.priv_ctx = c->ctx.prot->ctx_new(&c->ctx);
	c->ctx.prot->mkc(c->ctx.priv_ctx, __par);
	c->ctx.ent = _hook(&c->ctx);
	return c;
}

f_ic_conp f_ic_mksp(void *__par, _8_u __proto) {
	f_ic_conp c;
	c = (f_ic_conp)__f_mem_alloc(sizeof(struct f_ic_con));
	c->ctx.prot = _prot+__proto;
	c->ctx.priv_ctx = c->ctx.prot->ctx_new(&c->ctx);
	c->ctx.prot->mksp(c->ctx.priv_ctx, __par);
	return c;
}

// wait for incomming connection
f_ic_conp f_ic_wic(f_ic_conp __con) {
	f_ic_conp c;
	c = (f_ic_conp)__f_mem_alloc(sizeof(struct f_ic_con));
	c->ctx.prot = __con->ctx.prot;
	c->ctx.priv_ctx = c->ctx.prot->ctx_new(&c->ctx);
	__con->ctx.prot->wic(__con->ctx.priv_ctx, c->ctx.priv_ctx);
	c->ctx.ent = _hook(&c->ctx);
	return c;
}

void f_ic_cclose(f_ic_conp __con) {
	_unhook(&__con->ctx);
	__con->ctx.prot->close(__con->ctx.priv_ctx);
}
void f_ic_sclose(f_ic_conp __con) {
	__con->ctx.prot->close(__con->ctx.priv_ctx);
}

void f_ic_send(f_ic_conp __con, void *__buf, _int_u __size) {
	struct f_ic_stack *stack = &__con->ctx.ent->stack;

	_int_u npkts;
	npkts = (__size+(F_IC_PKT_SIZE-1))>>F_IC_PKT_SHFT;
	f_ic_pktp *pkts;
	pkts = (f_ic_pktp*)__f_mem_alloc(npkts*sizeof(f_ic_pktp));
	pktx_alloc(pkts, npkts);	
	
	f_ic_pktp pt;
	_8_u *p = (_8_u*)__buf;
	_int_u wp;//whole packets
	wp = __size>>F_IC_PKT_SHFT;
	_int_u i;
	i = 0;
	while(i != wp) {
		pt = pkts[i];
		f_mem_cpy(pt->data, p, pt->size = F_IC_PKT_SIZE);
		p+=F_IC_PKT_SIZE;
		i++;
	}

	_int_u left;
	left = __size&F_IC_PKT_MASK;
	if (left>0) {
		pt = pkts[i];
		f_mem_cpy(pt->data, p, pt->size = left);
	}

	f_ic_pktp t, last;
	t = last = *pkts;
	mt_lock(&stack->lock);
	t->seq_n = stack->ig_n++; 
	i = 1;
	// link up packets in chain for dumping
	for(;i != npkts;i++) {
		pt = pkts[i];
		pt->seq_n = stack->ig_n++;
		last->next = pt;
		last = pt;
	}
	last->next = NULL;
	__f_mem_free(pkts);
	last->next = stack->ig;
	stack->ig = t;
	stack->ig_bytes+=__size;
	stack->npkt+=npkts;
	mt_unlock(&stack->lock);
}

# include "system/nanosleep.h"
void static 
_waitfor_og(f_ic_conp __con) {
	printf("waiting for outcomming.\n");
	for(;;) {
		if (__con->og != NULL) {
			printf("got data breaking.\n");
			break;
		}
		ffly_nanosleep(0, 200000000);
	}
}
void f_ic_recv(f_ic_conp __con, void *__buf, _int_u __size) {
	_waitfor_og(__con);

}
# include "system/nanosleep.h"
# include "dep/bzero.h"
_f_err_t ffmain(int __argc, char const *__argv[]) {
	struct f_ic_con *s, *c;
	struct f_ic_cptcp cd;
	char cc;
	cc = *__argv[1];

	if (cc == 's') {
		struct sockaddr_in addr;
		f_bzero(&addr, sizeof(struct sockaddr_in));
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = htons(INADDR_ANY);
		addr.sin_port = htons(10198);
		cd.addr = &addr;
		cd.len = sizeof(struct sockaddr_in);
		s = f_ic_mksp(&cd, 0);
		c = f_ic_wic(s);	
		ffly_nanosleep(5, 0);
		f_ic_cclose(c);
		f_ic_sclose(s);
	} else {
		struct sockaddr_in addr;
		f_bzero(&addr, sizeof(struct sockaddr_in));
		addr.sin_addr.s_addr = inet_addr("127.0.0.1");
		addr.sin_family = AF_INET;
		addr.sin_port = htons(10198);
		cd.addr = &addr;
		cd.len = sizeof(struct sockaddr_in);
		c = f_ic_mkc(&cd, 0);
		char buf[5000];
		f_ic_send(c, buf, 5000);
		ffly_nanosleep(5, 0);

		f_ic_cclose(c);
	}

//	c.prot = &prot[0];
//	char buf[5000];
//	f_mem_set(buf, 0xff, 5000);
//	f_ic_send(&c, buf, 5000);
}
