#include "afac.h"
#include "../file.h"
#include "../system/string.h"
#include "../m_alloc.h"
#include "../strf.h"
#include "../io.h"
#include "../string.h"
#include "../assert.h"
#include "../string.h"
#include "../lib.h"
#include "../rws.h"
#include "../limits.h"
static _32_u _size;
/*
	HELP:

	'o' refers to object
		...
		'g' refers to a group of polygones within object
		...		
*/
/*

	do some sort of sanity check for values like 'obj->tot' if
	they are incorrect it will result in weard occurences
*/
static struct file wf_file;
struct block {
	char buf[512];
	_int_u len;
};
static _int_u line_num;
static _int_u content_pos;
static char *file_contents;
#define GETC file_contents[content_pos]
#define INC content_pos++
#define face_alloc(__obj) y_hungbuf_alloc(&__obj->curgroup->faces)
#define vec_alloc(__obj)	y_hungbuf_alloc(&__obj->vecs)
#define norm_alloc(__obj) y_hungbuf_alloc(&__obj->norms)
#define uv_alloc(__obj)	 y_hungbuf_alloc(&__obj->uv)


void static oversee_comments(void) {
	if(GETC == '#') {
		while(GETC != '\n'){
			INC;
		}
		line_num++;
		INC;//skip newline
	}
}

void static read_block(struct block *__b) {
_another:
	oversee_comments();
	if(GETC == '\n' || GETC == '\r' || GETC == ' '){
		INC;
	}
	if(GETC == ' ' || GETC == '\n' || GETC == '#' || GETC == '\r'){
		goto _another;
	}

	if(content_pos>=_size)return;

	char *buf = __b->buf;
	_int_u bp = 0;
	while(1) {
		if(GETC == ' ' || GETC == '\n' || GETC	== '\r') break;
		assert(bp<511);
		buf[bp++] = GETC;
		INC;
	}
	__b->buf[bp] = '\0';
	__b->len = bp;

//	printf("BLOCK: %s, %u.\n",__b->buf,__b->len);
}

void static read_vec3(struct afac_vec3 *v) {
	struct block b;
	read_block(&b);
	v->x = ffly_stfloat(b.buf);
	read_block(&b);
	v->y = ffly_stfloat(b.buf);
	read_block(&b);
	v->z = ffly_stfloat(b.buf);
}

void static read_vector(struct afac *__af) {
	struct afac_vec3 *v = vec_alloc(__af);
	read_vec3(v);
	struct afac_group *g;
	g = __af->obj->curgroup;
	struct afac_vec3 *near,*far;

	near = &g->prem.near;
	far = &g->prem.far;

	if(v->x<near->x){
		near->x = v->x;
	}
	if(v->y<near->y){
		near->y = v->y;
	}
	if(v->z<near->z){
		near->z = v->z;
	}
	if(v->x>far->x){
		far->x = v->x;
	}
	if(v->y>far->y){
		far->y = v->y;
	}
	if(v->z>far->z){
		far->z = v->z;
	}	
}
static struct afac_vec3 dummy_vec = {0,0,0};
void static read_face(struct afac *__af, struct afac_object *__obj) {
	struct block b;
	struct afac_face *f = face_alloc(__obj);
	char buf[128];
	_int_u i;
	i = 0;
	/*
		we are using bit flags because this format dosent tell us if UV coords or norms dont exist as of no header.
	*/
	_16_u
	flags = 0;
	while(1) {
		if(i>MAX_FACE_VERTS-1){
			printf("fuck.\n");
			assert(1 == 0);
		}
		afac_vert_indx *v = &f->v[i++];
		read_block(&b);
		_int_u len;
		len = snumext(buf,b.buf);
		/*
			i dont realy know but it seems that they use the 1-... approch and not 0-... because of this a subtract(1)
		*/
		v->pos = _ffly_dsn(buf,len)-1;
		flags |= AF_OBJ_HAS_POS;
		//we expect two double slashes(no textures)
		len++;
		if(b.buf[len] != '/'){
			_int_u ln;
			len+=ln = snumext(buf,b.buf+len);
			_int_u num;
			v->uv = num = _ffly_dsn(buf,ln)-1;
			flags |= AF_OBJ_HAS_UV;
		}
		len = snumext(buf,b.buf+len+1);

		v->norm = _ffly_dsn(buf,len)-1;
		flags |= AF_OBJ_HAS_NORM;
		if(GETC != ' ') {
			break;
		}
	}

	__obj->flags |= flags;
	//TODO:clean up
	if(i == 3){
		/*
			order of standard triangle
		*/
		f->tris_comprised[0] = (afac_tri_indx){{0,1,2}};
		f->n = 1;
		__obj->tot++;
		__obj->curgroup->n_faces+=1;
	}else if(i == 4){
		f->tris_comprised[0] = (afac_tri_indx){{0,1,2}};
		f->tris_comprised[1] = (afac_tri_indx){{0,2,3}};
		f->n = 2;
		__obj->tot+=2;
		__obj->curgroup->n_faces+=2;
	}else{
		printf("unsupported.\n");
		assert(1 == 0);
	}
}

void static read_normal(struct afac *__af) {
	struct afac_vec3 *v = norm_alloc(__af);
	read_vec3(v);
}

void static read_uv(struct afac *__af) {
	struct afac_vec3 *v = uv_alloc(__af);
	struct block b;
	read_block(&b);
	v->x = ffly_stfloat(b.buf);
	read_block(&b);
	v->y = 1.-ffly_stfloat(b.buf);
}

static struct afac_group *new_group(struct afac_object *__obj,char const *__name){
	struct afac_group*
	g = f_lhash_get(&__obj->_groups,__name,str_len(__name));
	if(!g){
		g = m_alloc(sizeof(struct afac_group));
		g->n_faces = 0;
		str_cpy(g->name,__name);
		f_lhash_put(&__obj->_groups,__name,str_len(__name),g);
		y_hbinit(&g->faces,sizeof(struct afac_face));
		g->next = __obj->groups;
		__obj->groups = g;
		g->prem = (struct afac_prem){
			.near ={//near
				DBL_MAX,
				DBL_MAX,
				DBL_MAX
			},
			.far = {//far
				-DBL_MAX,
				-DBL_MAX,
				-DBL_MAX
			}
		};
	}
	return g;
}

static struct afac_object* new_obj(void){
	struct afac_object*
	o = m_alloc(sizeof(struct afac_object));
	o->flags = 0;
	o->tot = 0;
	o->groups = NULL;
	f_lhash_init(&o->_groups);
	return o;
}

void static read_file(struct afac *__af) {
	struct block b;
	struct block cmd;
	struct afac_object *obj = NULL;
	while(content_pos < _size) {
		read_block(&cmd);
		if(content_pos>=_size)break;
		if(cmd.len == 1) {
			switch(cmd.buf[0]) {
				case 'v':
					read_vector(__af);
				break;
				case 'f':
					read_face(__af,obj);
				break;
				case 's':
					read_block(&b);//ignore
				break;
				case 'o':{
					read_block(&b);

					printf("OBJECT: %s, %u\n",b.buf,b.len);

					obj = new_obj();
					assert(obj != NULL);
					str_cpy(obj->name,b.buf);
					obj->curgroup = new_group(obj,obj->name);

					obj->next = __af->obj;
					__af->obj = obj;
					__af->n++;
				}
				break;
				case 'g':{
					read_block(&b);
					assert(obj != NULL);
					obj->curgroup = new_group(obj,b.buf);
				}
				break;
			}
		}else if(cmd.len == 2) {
			if(cmd.buf[0] == 'v' && cmd.buf[1] == 'n') {
				read_normal(__af);
			}else if(cmd.buf[0] == 'v' && cmd.buf[1] == 't') {
				read_uv(__af);
			}
		}else{
			if(!str_cmp(cmd.buf,"usemtl")) {
				read_block(&b);//ignore
			}else
			if(!str_cmp(cmd.buf,"mtllib")){
				read_block(&b);
			}
		}
	}
}

void afac_objdump(struct afac *__af,struct afac_object *__obj,_64_u __flags) {
	printf("OBJECT: %s.\n",__obj->name);
	struct afac_group *g;
	g = __obj->groups;
	while(g != NULL){
		printf("GROUP-%s, faces: %u\n",g->name,g->n_faces);
		if(!(__flags&AFAC_OBJDUMP_ONLY_OBJECTS_AND_GROUPS)){
		_int_u i;
		i = 0;
		for(;i != g->n_faces;i++){
			struct afac_face *f = y_hungbuf_at(&g->faces,i);
			printf("face-%u: {innerfaces: %u}\n",i,f->n);
			_int_u j;
			j = 0;
			for(;j != f->n;j++) {
				afac_tri_indx *t = f->tris_comprised+j;
				struct afac_vec3 *pos;
				struct afac_vec3 *norm;
				struct afac_vec3 *uv;
				_int_u k;
				k = 0;
				for(;k != 3;k++){
					afac_vert_indx *v = f->v+t->verts[k];
					printf("\tvertex-%u:",k);
					if(__obj->flags&AF_OBJ_HAS_POS){
						pos = y_hungbuf_at(&__af->vecs,v->pos);
						printf(" pos{%f, %f, %f},",pos->x,pos->y,pos->z);
					}
				
					if(__obj->flags&AF_OBJ_HAS_NORM){
						norm = y_hungbuf_at(&__af->norms,v->norm);
						printf(" normal{%f, %f, %f},",norm->x,norm->y,norm->z);
					}

					if(__obj->flags&AF_OBJ_HAS_UV){
						uv = y_hungbuf_at(&__af->uv,v->uv);
						printf(" uv{%f, %f, %f},",uv->x,uv->y,uv->z);
					}
					printf("\n");
				}
			}	
		}
		}
		g = g->next;
	}
}

void afac_load(struct afac *__af, struct rws_schematic *sch){
	struct rws_stat st;
	_64_u *opttab;
	_64_u *modetab;
	opttab = sch->funcs->opttab;
	modetab = sch->funcs->modetab;
	void *stg;//storage
	__af->n = 0;
	/*
		open the storage
	*/
	stg = sch->funcs->open(
		sch,
		opttab[RWS_RDONLY],
		0
	);

	//get size
	sch->funcs->stat(stg,&st);
	

	_64_u
	__size = st.st_size;

	_size = __size;
	line_num = 0;
	content_pos = 0;
	__af->obj = NULL;

	y_hbinit(&__af->vecs,sizeof(struct afac_vec3));
	y_hbinit(&__af->norms,sizeof(struct afac_vec3));
	y_hbinit(&__af->uv,sizeof(struct afac_vec3));

	file_contents = m_alloc(__size);
	_8_s errorval;
	sch->funcs->read(stg,file_contents,__size,&errorval);
	read_file(__af);	
	m_free(file_contents);
	sch->funcs->close(stg);
}

void afac_wf_export(struct afac_wf *__a, char const *__file) {
	struct file f;
	_8_s r;
	r = bopen(&f,__file,_O_RDWR|_O_TRUNC|_O_CREAT,_S_IRUSR|_S_IWUSR);
	if(r<0) {
		return;
	}
#define STRC "o Cube\n"
	 bwrite(&f,STRC,sizeof(STRC)-1);
#undef STRC
	_int_u i;
	char buf[512];
	i = 0;
	for(;i != __a->n_v;i++) {
		struct afac_blob *b = __a->v+i;
		_int_u j;
		j = 0;
		for(;j != b->len;j++) {
			struct _y_vec3 *v = ((_8_u*)b->v)+(j*__a->vdis);
			char *bp = buf;
			_int_u l;
			*(bp++) = 'v';
			*(bp++) = ' ';
			bp+=y_ftssc(bp,v->x);
			*(bp++) = ' ';
				
			bp+=y_ftssc(bp,v->y);
			*(bp++) = ' ';

			bp+=y_ftssc(bp,v->z);
			*(bp++) = '\n';
			bwrite(&f,buf,bp-buf);
		}
	}

	i = 0;
	for(;i != __a->n_vn;i++) {
		struct afac_blob *b = __a->vn+i;
		_int_u j;
		j = 0;
		for(;j != b->len;j++) {
			struct _y_vec3 *v = ((_8_u*)b->v)+(j*__a->vdis);
			char *bp = buf;
			_int_u l;
			*(bp++) = 'v';
			*(bp++) = 'n';
			*(bp++) = ' ';
			bp+=y_ftssc(bp,v->x);
			*(bp++) = ' ';
				
			bp+=y_ftssc(bp,v->y);
			*(bp++) = ' ';

			bp+=y_ftssc(bp,v->z);
			*(bp++) = '\n';
			bwrite(&f,buf,bp-buf);
		}
	}


#define STRC "vn 0.0000 1.0000 0.0000\n"
	bwrite(&f,STRC,sizeof(STRC)-1);
#undef STRC
#define STRC "usemtl Material\ns off\n"
	bwrite(&f,STRC,sizeof(STRC)-1);
	printf("number of faces. %u\n",__a->n_vf);	
	i = 0;
	for(;i != __a->n_vf;i++) {
		struct afac_wf_face *_f = __a->vf+i;
		char *bp = buf;
		*(bp++) = 'f';
		_int_u j = 0;
		for(;j != _f->n;j++) {
			*(bp++) = ' ';
			bp+=_ffly_nds(bp,_f->v_idx[j]+1);
			*(bp++) = '/';
			*(bp++) = '/';
			bp+=_ffly_nds(bp,_f->vn_idx[j]+1);
		}
		*(bp++) = '\n';
		bwrite(&f,buf,bp-buf);
	}

	bclose(&f);
}
