# include "brick.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "memory/mem_realloc.h"
# include "system/io.h"
#define PAGE_SHIFT 2
#define PAGE_SIZE (1<<PAGE_SHIFT)

/*
	later on we need to download files for shit to work e.g. config files
	but if the client cuts out downloading the file over again is wastful
	soo once the client is back online next try will start off at the brick it left on.
	may be slower but brick size is adjustable and its all about flexibility
*/

/*
	TODO:
		free memory after x amount of time after close
*/

/*
	needs testing
*/

static ffly_brickp *bricks = NULL;
static _int_u page_c = 0;
static _64_u off = 0;
static ffly_brickp bin = NULL;

static ffly_brickp top = NULL;

#define get_brick(__id) \
	((*(bricks+(__id>>PAGE_SHIFT)))+(__id&0xffffffffffffffff>>(64-PAGE_SHIFT)))
void static delink(ffly_brickp);

_32_u
ffly_brick_new(_8_u __sz,
	void(*__read)(long long, void*, _8_u), void(*__write)(long long, void*, _8_u),
	void(*__del)(long long), long long __arg)
{
	_32_u rd, pg_off;
	ffly_brickp b;
	_int_u page;
	if (bin != NULL) {
		b = bin;
		if ((bin = bin->fd) != NULL)
			bin->bk = &bin;
		rd = b->id;
		goto _sk0;
	}
	page = off>>PAGE_SHIFT;

	if (!bricks) {
		bricks = (ffly_brickp*)__f_mem_alloc(sizeof(ffly_brickp));
		page_c++;
	} else {
		if (page>page_c-1)
			bricks = (ffly_brickp*)__f_mem_realloc(bricks, (++page_c)*sizeof(ffly_brickp));
		else
			goto _sk;
	}

	*(bricks+page) = (ffly_brickp)__f_mem_alloc(PAGE_SIZE*sizeof(struct ffly_brick));
_sk:
	pg_off = (rd = off++)-(page*PAGE_SIZE);
	b = (*(bricks+page))+pg_off;

	b->next = top;
	if (top != NULL)
		top->pn = &b->next;
	b->pn = &top;
	top = b;
_sk0:
	b->arg = __arg;
	b->read = __read;
	b->write = __write;
	b->del = __del;
	b->p = NULL;
	b->flags = 0x00;
	b->inuse = 0;
	b->sz = __sz;
	return (b->id = rd);
}

void ffly_brick_open(_32_u __id) {
	ffly_brickp b;

	b = get_brick(__id);
	b->flags |= BRICK_OPEN;

	if (!b->p)
		b->p = __f_mem_alloc(1<<b->sz);
	b->read(b->arg, b->p, b->sz);
}
void ffly_brick_close(_32_u __id) {
	ffly_brickp b;
	
	b = get_brick(__id);
#ifndef be_vigilant
	b->flags ^= BRICK_OPEN;
#else
	b->flags &= ~BRICK_OPEN;
#endif

	if (b->p != NULL) {
		b->write(b->arg, b->p, b->sz);
		__f_mem_free(b->p);
		b->p = NULL;
	}
}

_8_i ffly_brick_exist(_32_u __id) {
	ffly_brickp b;
	if (__id<off) {
		b = get_brick(__id);
		if (!b->inuse) {
			return 0;
		}
	}
	return -1;
}

void* ffly_brick_get(_32_u __id) {
	ffly_brickp b = get_brick(__id);
	if (!b->p) {
		//error
		return NULL;
	}
	return b->p;
}

void ffly_brick_cleanup(void) {
	ffly_brickp cur = top;
	while(cur != NULL) {
		if (cur->p != NULL)
			__f_mem_free(cur->p);
		cur = cur->next;
	}

	_int_u page;

	printf("brick.c; %u pages to be freed.\n", page_c);
	page = 0;
	while(page != page_c) {
		__f_mem_free(*(bricks+page));
		page++;
	}

	if (bricks != NULL)
		__f_mem_free(bricks);
}

void static
deattach(ffly_brickp __b) {
	*__b->pn = __b->next;
	if (__b->next != NULL)
		__b->next->pn = __b->pn;
}

void
delink(ffly_brickp __b) {
	*__b->bk = __b->fd;
	if (__b->fd != NULL)
		__b->fd->bk = __b->bk;
}

void ffly_bricks_show(void) {
	ffly_brickp cur = top;
	_int_u i = 0;
	while(cur != NULL) {
		printf("%u: %s, id: %u, pg: %u\n", i++, !cur->inuse?"inuse":"not inuse", cur->id, cur->id>>PAGE_SHIFT);
		cur = cur->next;
	}
}

// get rid of brick
void ffly_brick_rid(_32_u __id) {
	ffly_brickp b = get_brick(__id);
	if (b->del != NULL)
		b->del(b->arg);
	_64_u bo = __id;

	if (bo == off-1 && page_c>1) {
		printf(".... next: %p\n", b->next);
		ffly_brickp cur = b->next;
		off--;
		deattach(b);
		while(cur != NULL) {
			b = cur;
			cur = cur->next;
			if (!b->inuse) {
				printf("brick found to be inuse, breaking\n");
				break;
			}
			if (!(off-1>>PAGE_SHIFT))
				break;
			if (b->p != NULL) {
				//display warn
				__f_mem_free(b->p);
			}
			printf("found more dead bricks, id: %u\n", b->id);
			delink(b);
			deattach(b);
			off--;
		}

		_int_u old_pgc = page_c;
		_int_u page = off>>PAGE_SHIFT;
		while(page < page_c-1) {
			page_c--;
			printf("removing page: %u\n", page_c);
			__f_mem_free(*(bricks+page_c));
		}
	
		if (page_c<old_pgc)
			bricks = (ffly_brickp*)__f_mem_realloc(bricks, page_c*sizeof(ffly_brickp));
		return;
	}

	if (b->p != NULL) {
		__f_mem_free(b->p);
		b->p = NULL;
	}
	b->fd = bin;
	b->inuse = -1;
	b->bk = &bin;
	if (bin != NULL)
		bin->bk = &b->fd;
	bin = b;
	printf("added brick to bin.\n");
}
