#include "iu.h"
#include "../havoc/common.h"
/*
	look i like to think as the composer as a scene.
	a performance if you will, where the actors are the children
	or example if we have a window the children abay the parents rule.
	just like a actor acts as the composer wants.
*/
void iu_compose(struct iu_composer *__comp) {
	float transform[16] = {
		1,0,0,__comp->vp.translate[0],
		0,1,0,__comp->vp.translate[1],
		0,0,0,0,
		0,0,0,1
	};
	h_matcopY(ct_com.transform,transform);
	iu_render_bin_render(&__comp->bin);
}
