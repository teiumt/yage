#ifndef __iu__h
#define __iu__h
#define iu_printf(...) log_printf(&iulog, __VA_ARGS__)
#include "../log.h"
#include "../y_int.h"
#include "../citric/citric.h"
extern struct log_struc iulog;
#include "iu_struc.h"
#include "iu_common.h"
#include "iu_window.h"
#include "../assert.h"
void iu_message(_ulonglong, struct ct_msg*);
struct iu_caprect* iu_captnew(void);
struct iu_render* iu_render(struct iu_composer*);
struct iu_caprect* _iu_captnew(_int_u __lv);
void iu_bininit(struct iu_render_bin*);

void iu_render_bin_render(struct iu_render_bin *b);
void iu_captattach(struct iu_caprect *__c);
void iu_captdetach(struct iu_caprect *__c);
struct iu_render* _iu_render(struct iu_composer *b, _int_u __n);
void iu_init(struct iu_gc*);
void iu_start(void);
void iu_load(struct iu_window*);
void iu_exccapt(struct iu_caplv**,struct iu_caplv*);
struct iu_window* iu_grabwd_at(_64_u __id);
#define iu_assert(__expr) if(!(__expr)){_iu_flush();_assert(__FILE__,__LINE__,__func__,"IU assert\n");}
void _iu_flush(void);
void iu_captinit(struct iu_caplv *__cl);
void iu_compose(struct iu_composer *__comp);
void iu_initsome(void);
#endif /*__iu__h*/
