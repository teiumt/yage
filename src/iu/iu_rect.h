#ifndef __iu__rect__h
#define __iu__rect__h
#include "iu_widget.h"
#define IU_RECT_PRESS		0
#define IU_RECT_RELEASE		1

//inputs
#define IU_RECT_HIDE	0
#define IU_RECT_SHOW	1
struct iu_rect {
	struct iu_widget _w;
	struct ct_colour c;
	float vtx[4];
};
_Static_assert(sizeof(struct iu_rect)<IU_WIDGET_SIZEMAX,"");
void iu_rect_init(struct iu_rect*);

#endif/*__iu__rect__h*/
