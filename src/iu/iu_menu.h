#ifndef __iu__menu__h
#define __iu__menu__h
#include "iu.h"
#define IU_MENU_EXTEND  0
#define IU_MENU_RETRACT 1
#define IU_MENU_ADJUSTMENT	2
struct iu_menu {
	struct iu_widget _w;
	struct iu_widget *childs[7];
	struct iu_caplv capt;
	_int_u nc;

	float min_height;
	float min_width;
	float max_width;
	_64_u id;
	/*
		the widget that last dictator the size change
	*/
	struct iu_widget *dictator;
};
void iu_menu_show(struct iu_menu *__m);
void iu_menu_hide(struct iu_menu *__m);
void iu_menu_init(struct iu_menu*);
void iu_menu_add(struct iu_menu*,struct iu_widget*);
void iu_menu_lay(struct iu_menu*);
#endif/*__iu__menu__h*/
