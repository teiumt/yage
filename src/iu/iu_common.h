#ifndef __iu__common__h
#define __iu__common__h
#include "../y_int.h"
#include "../types.h"
#include "iu_struc.h"
#define IU_DRAW0 0
#define IU_DRAW1 1
#define IU_DRAW2 1
#define IU_DRAW3 1
struct iu_common {
	/*
		for capt have two pointers
		and swap?
		or a makeroom function
		or not
	*/
	struct iu_caplv *capt;
 	struct iu_window *w;
    /*
        the composer at whitch widgets aligns its belief too.
        using a window would be too narrow minded.
    */
	struct iu_composer *b;
	struct iu_render draw[8];
	//message interseption(capture)
	void(*message)(_ulonglong,struct ct_msg*);
	_ulonglong msgarg;
	_8_s running;
	_64_u msgredir0[32];
	_64_u msgmask;
};
extern struct iu_common iu_com;
#endif/*__iu__common__h*/
