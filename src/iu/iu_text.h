#ifndef __iu__text__h
#define __iu__text__h
#include "iu_widget.h"
#include "../citric/ct_types.h"
#include "../ffly_def.h"
#define IU_TEXT_PRESS			0
#define IU_TEXT_SCOOT_LEFT		1
#define IU_TEXT_SCOOT_RIGHT		2
#define IU_TEXT_SELECT_START    3
#define IU_TEXT_SELECT_END      4
#define IU_TEXT_INSERT          5
#define IU_TEXT_DEINSERT        6

struct iu_text_frag {
	char *s;
	_int_u len, start;
	struct iu_text_frag *next, *prev;
};

struct iu_text {
	struct iu_widget _w;
	struct ct_text_line lines[2];
	_int_u ln;
	_int_u line;

	struct iu_text_frag *frags;	
	_int_u num_frgs;
	_int_u len;
	struct ct_colour c;
	_64_u bits;
	_int_u cursor_pos,cursor_end;
	_int_u w,h;

	_int_u char_limit;
	float size;
	_8_s in_focus;
	float min_reduct;
	float max;
	_int_u offset;
	float xd,yd;
	float glyph_height;
};
void iu_text_default(struct iu_text*);
void iu_text_charstr_selection(struct iu_text *__t, char *__buf, _int_u __start, _int_u __len);
void iu_text_charstr(struct iu_text*,char*);
void iu_text_deposit(struct iu_text*,char const*,_int_u);
#define TX_VALIGN 1
#define TX_HALIGN 2
void iu_text_determin(struct iu_text*);
void iu_text_config(struct iu_text*);
_Static_assert(sizeof(struct iu_text)<IU_WIDGET_SIZEMAX,"");
void iu_text_init(struct iu_text*);
#endif/*__iu__text__h*/
