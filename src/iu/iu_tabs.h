#ifndef __iu__tabs__h
#define __iu__tabs__h
#include "iu.h"
#include "iu_widget.h"
#include "iu_btn.h"
#include "iu_board.h"
struct iu_tabs;
struct iu_tabpage {
	struct iu_btn *button;
	struct iu_widget *w[16];
	struct iu_caplv capt;
	struct iu_tabs *priv;
	_int_u n;
	_int_u idx;
};
struct iu_tabs {
	struct iu_widget _w;
	_64_u userdata[16];
	struct iu_tabpage pages[4];
	_int_u num_pages;
	struct iu_tabpage *page;
	struct iu_caplv capt;
};

void iu_tab_init(struct iu_tabs*);
void iu_tab_wadd(struct iu_tabpage*,struct iu_widget*);
#endif/*__iu__tabs__h*/
