#ifndef __iu__signal__h
#define __iu__signal__h
#include "iu_common.h"
#include "iu_widget.h"

struct iu_sigrange {
	_64_u mask[4];
	_int_u code;
	char const *ident;
};
struct iu_sigrange* iu_sigrangefor(struct iu_sigrange*,_int_u,_64_u,_int_u);
#endif/*__iu__signal__h*/
