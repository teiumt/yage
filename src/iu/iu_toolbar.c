#include "iu.h"
#include "iu_toolbar.h"
#include "../ffly_def.h"
void static draw(struct iu_toolbar *__tb) {
/*    float vtx[4] = {
        __tb->_w.x,
        __tb->_w.y,
        __tb->_w.x+__tb->_w._w,
        __tb->_w.y+__tb->_w._h
    };
    ct_com.g->crect(vtx,0,1,1,.2,0.8);
*/
}

void static move(_ulonglong __arg, struct iu_toolbar *__tb,_int_u __x, _int_u  __y) {
	__tb->_w.x = __x;
	__tb->_w.y = __y;
}

void static sizer(struct iu_toolbar *__tb) {
	iu_toolbar_confine(__tb);
	if(__tb->_w.codetab != NULL && __tb->_w.output != NULL) {
		_int_u code = __tb->_w.codetab[TOOLBAR_RESIZE];
		if (code != -1) {
			iu_printf("IU_TOOLBAR_RESIZE.\n");
			struct ct_msg _m;
			_m.code = code;
			__tb->_w.output(__tb,__tb->_w.out,&_m);
		}
	}
}

void iu_toolbar_init(struct iu_toolbar *__tb) {
	iu_wginit(__tb);
	__tb->_w.size = sizer;
	__tb->_w.draw = draw;
	__tb->_w.move = move;
	__tb->row = 0;
	__tb->sway = 0;
	__tb->nc = 0;
	__tb->w = 1;
	__tb->h = 1;
}
void iu_toolbar_confine(struct iu_toolbar *__tb) {
	_int_u i;
	i = 0;
	__tb->_w._h = __tb->_w.h;
	float _w = 0,_h = 0;
	for(;i != __tb->nc;i++) {
		struct iu_widget *w = __tb->childs[i];
		iu_wgsize1(w);		
		_h = w->_h;
	
	}

	_w = __tb->row+__tb->childs[__tb->nc-1]->_w;
	__tb->_w._w = _w;
	__tb->_w._h = _h;
}

void iu_toolbar_add(struct iu_toolbar *__tb,struct iu_widget *__w, _int_u __spacing) {
    iu_wgchild(__w,__tb,__tb->_w.draw_chain);
    iu_wgchild(__w,__tb,__tb->_w.move_chain);

	__tb->row+=__spacing;
	__w->xdyd[__tb->sway]-=__tb->row;
	__tb->childs[__tb->nc++] = __w;
}
