#include "iu_shell.h"
#define BORDER_WIDTH 1
#define BORDER_COLOUR 0,1,0,1
void static draw(struct iu_shell*__s) {
	float vtx[4],x,y;
	x=__s->_w.x;
	y=__s->_w.y;

	float w,h;
	w = __s->w->width_limit;
	h = __s->w->_h;

	/*
		background
	*/
	vtx[0] = x+BORDER_WIDTH;
	vtx[1] = y+BORDER_WIDTH;
	vtx[2] = vtx[0]+w;
	vtx[3] = vtx[1]+h;
	ct_com.g->crect(vtx,0,1,1,1,1);


	/*
		boarder
	*/

	vtx[0] = x;
	vtx[1] = y;

	//left
	vtx[2] = x+BORDER_WIDTH;
	vtx[3] = y+h+BORDER_WIDTH*2;
	ct_com.g->crect(vtx,0,BORDER_COLOUR);

	vtx[0] = x+BORDER_WIDTH;

	//top
	vtx[2] = vtx[0]+w;
	vtx[3] = y+BORDER_WIDTH;
	ct_com.g->crect(vtx,0,BORDER_COLOUR);

	//bottom
	vtx[1] = y+h+BORDER_WIDTH;
	vtx[3] = vtx[1]+BORDER_WIDTH;
	ct_com.g->crect(vtx,0,BORDER_COLOUR);

	vtx[0] = x+w+BORDER_WIDTH;
	vtx[1] = y;

	vtx[2] = vtx[0]+BORDER_WIDTH;
	vtx[3] = y+h+BORDER_WIDTH*2;
	ct_com.g->crect(vtx,0,BORDER_COLOUR);
}


void static move(_ulonglong __arg, struct iu_shell  *__s,_int_u __x, _int_u __y) {
	__s->_w.x = __x;
	__s->_w.y = __y;
}


void iu_shell_init(struct iu_shell *__s) {
	iu_wginit(__s);
	__s->_w.childs = NULL;
	__s->_w.next = NULL;
	__s->_w.move = move;
	__s->_w.draw = draw;
}

void iu_shell_add(struct iu_shell *__s, struct iu_widget *__w) {
	__s->w = __w;
	__w->xd0 = -1;
	__w->yd0 = -1;
}
