#include "iu.h"
#include "../m_alloc.h"
struct iu_common iu_com;

static struct iu_window *window = NULL;
void iu_window_new(struct iu_window *__w) {
	__w->surf = m_alloc(sizeof(struct ct_surface));
	ct_mjx.surf_creat(__w->surf);
	__w->surf->g = &ct_flue;
	__w->surf->drv = &ct_mjx;
	window = __w;
	iu_com.w = __w;
	iu_com.b = &__w->_b.comp;
}
#include "../io.h"
void _iu_flush(void) {
	ffly_fdrain(iulog._out);
}
static struct iu_window *windows[1];
struct iu_window* iu_grabwd_at(_64_u __id) {
	return windows[__id];
}

void iu_window_map(struct iu_window *__w, _int_u __x, _int_u __y, _int_u __width, _int_u __height) {
	__w->surf->drv->surf_map(__w->surf,__x,__y,__width,__height);
	__w->_b.comp.vp = (struct ct_viewport){
		.translate = {0,0,0,0},//translate
		.scale = {1,1,1,1}//scale
	};
	__w->_b.comp.vpw = 1./__w->_b.comp.vp.scale[0];
	__w->_b.comp.vph = 1./__w->_b.comp.vp.scale[1];
	__w->_b.comp.asp = __w->_b.comp.vp.scale[0]/__w->_b.comp.vp.scale[1];
	windows[0] = __w;
	iu_wdinit(__w);
}

void iu_window_move(struct iu_window *__w,_int_u __x, _int_u __y){
	__w->surf->drv->surf_move(__w->surf,__x,__y);
}
void iu_window_unmap(struct iu_window *__w) {
	__w->surf->drv->surf_unmap(__w->surf);
}
void iu_window_destroy(struct iu_window *__w) {

}

void iu_window_swapbufs(struct iu_window *__w) {
	__w->surf->drv->swapbufs(__w->surf);
}
void static sig_handler(int s){
	iu_com.running = -1;
}



void iu_bininit(struct iu_render_bin *b) {
	_int_u i;
	i = 0;
	for(;i != 4*4;i++) {
		b->bns[i].rt = NULL;
	}
	b->msk = 0xf;
}

void iu_captinit(struct iu_caplv *__cl) {
	_int_u i;
	i = 0;
	for(;i != 4*4;i++) {
		__cl->capt[i] = NULL;
	}

	__cl->msk = 0xf; 
}
struct iu_caprect* iu_captnew(void) {
	return _iu_captnew(3);
}

struct iu_caprect* _iu_captnew(_int_u __lv) {
	struct iu_caprect *r;
	r = m_alloc(sizeof(struct iu_caprect));
	r->cap = iu_com.capt;
	r->lv = __lv;
	r->sub.skim = -1;
	r->sub.inside = -1;
	return r;
}
	
void iu_captattach(struct iu_caprect *__c) {
	__c->next = __c->cap->capt[__c->lv];
	__c->cap->capt[__c->lv] = __c;
}

void iu_captdetach(struct iu_caprect *__c) {
}

struct iu_render* iu_render(struct iu_composer *b) {
	return _iu_render(b,1);
}
struct iu_render* _iu_render(struct iu_composer *b, _int_u __n) {

	struct iu_render *rn;
	rn = m_alloc(sizeof(struct iu_render));
	rn->skim = -1;
	rn->next = b->bns[__n].rt;
	b->bns[__n].rt = rn;
	return rn;
}
/*
	look lets say we have a function nest of a depth of 4,

	so if we use a term like iu_window at a depth of 3
	this will mean 2 register to register moves.

	if we global define this insted of passing as register,
	then will only equate to a single move operation
*/
void iu_load(struct iu_window *w) {
	iu_com.b = &w->_b.comp;
	iu_com.w = w;
	iu_com.capt = &w->_b.capt;
}

void iu_exccapt(struct iu_caplv **ex,struct iu_caplv *replacement) {
	if(ex != NULL)
		*ex = iu_com.capt;
	iu_com.capt = replacement;
}

#include "../citric/ct_font.h"
#include "../vekas/maj.h"
#include "../flue/common.h"
void _iu_init(void);
void _iu_deinit(void);
#include "../signal.h"
_8_s _iu_msg(struct ct_msg *m);
void _iu_postmsg(struct ct_msg *m);


/*
	arg is only used when messages are being intersepted
*/
static _8_s found_within;
void iu_message(_ulonglong __arg, struct ct_msg *m) {
	struct iu_caplv *cl = iu_com.capt;
	struct iu_caprect *r;
	_int_u lv = 0;
	for(;lv != 4*4;lv++) {
	if(!((1<<(lv>>2))&cl->msk))continue;
	r = cl->capt[lv];
	while(r != NULL) {
		struct iu_crsub *s = &r->sub;

		if (s->update != NULL && s->skim == -1) {
			iu_printf("level-%u, MSG-%p: %d>=%d && %d>=%d && %d<%d && %d<%d\n",lv,r,m->x,s->hb.x,m->y,s->hb.y,m->x,s->hb.xfar,m->y,s->hb.yfar);			
			if (m->x>=s->hb.x && m->y>=s->hb.y && m->x<s->hb.xfar && m->y<s->hb.yfar) {
				found_within = 0;
				/*
					TODO: murge both inside single call,
					add control int or use single bits or somthing.
				*/
				
				s->update(s->arg,m);
				if (s->inside == -1) {
					struct ct_msg_ _m;
					_m.m.value = IU_PTR_ENTER;
					s->update(s->arg,&_m);
					s->inside = 0;
				}
				return;
			}else{
				if(!s->inside) {
					struct ct_msg_ _m;
					_m.m.value = IU_PTR_EXIT;
					s->update(s->arg,&_m);
					s->inside = -1;
				}
			}
		}
		r = r->next;
	}
	}
}
void static lowerlevel_msg(_ulonglong __arg, struct ct_msg *m) {
	/*
		this stays here, in the case that 
		a focusing event takes place within.
		iu_com.message will be redirected to capture
		all events bypassing everything, including this.
	*/

	struct ct_msg fixme;
	fixme = *m;
	struct maj_patch *pt;
	pt = window->surf->userdata;

	fixme.x -= pt->x;
	fixme.y -= pt->y;
	iu_printf("fixme: %u, %u, %u-%u, %u-%u\n",fixme.code,fixme.value,fixme.x,fixme.y,pt->x,pt->y);
	iu_com.message(__arg,&fixme);

	/*
		in some cases the user may want to take in messages
	*/
	if((1<<m->value)&iu_com.msgmask){
		_64_u h;
		h = m->code&31;
		if((1<<(m->code>>5))&h){
			_iu_msg(m);
		}
	}
}

void iu_render_bin_render(struct iu_render_bin *b) {
	struct iu_render *rn;
	_int_u i;

	i = 0;
	for(;i != 8;i++) {
		if(!((1<<(i>>2))&b->msk))continue;
		rn = b->bns[i].rt;
		while(rn != NULL) {
			rn->func(rn->priv);
	
			rn = rn->next;
		}
	}

}

#include "../havoc/common.h"
#include "../linux/time.h"
extern _int_u cnt_gbl;
void _iu_tick(void);
struct log_struc iulog;

void iu_initsome(void){
	y_log_file(&iulog,"iulog");
}

void iu_start(void) {
	iu_com.running = 0;
	iu_com.message = iu_message;
	y_log_file(&iulog,"iulog");
	struct sigaction sig;
	sig.sa_handler = sig_handler;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags = 0;
	sigaction(SIGINT,&sig,NULL);
	ct_mjx.connect();
	iu_com.capt = NULL;
	
	ct_com.g = &ct_flue;
	ct_com.g->init();
	
	ct_init(ct_com.g);
	
	_iu_init();

	struct ct_msg_ msgbuf[16];
	while(!iu_com.running) {
		_int_u mcn;
		mcn = ct_mjx.message(msgbuf);
		if(!window)continue;		
		struct ct_surface *surf = window->surf;
		struct ct_gc *g = surf->g;
		struct ct_driver *drv = surf->drv;

		_int_u i;
		i = 0;
		for(;i != mcn;i++) {
			struct iu_msgdata *m;
			m = msgbuf+i;
			m->m.ctx = 0;
			m->id = -1;
			lowerlevel_msg(iu_com.msgarg,m);
		}

		void *tgs[] = {
			window->surf->render
		};
		g->rtgs(tgs,1);
		g->framebuffer(surf->width,surf->height);
		iu_printf("VIEWPORT{%f, %f, %f, %f, %f, %f}\n",
			window->_b.comp.vp.translate[0],window->_b.comp.vp.translate[1],window->_b.comp.vp.translate[2],
			window->_b.comp.vp.scale[0],window->_b.comp.vp.scale[1],window->_b.comp.vp.scale[2]
		);

		struct timespec start,stop;
		clock_gettime(CLOCK_MONOTONIC,&start);
		_iu_tick();
		
		g->viewport(&window->_b.comp.vp);
		//the first layer
		i = 0;
		for(;i != cnt_gbl;i++){
			struct iu_render *rn = iu_com.draw+((cnt_gbl-1)-i);
			/*
				look i dont like the skim. it should be the parents bit mask or somthing
			*/
			if(rn->skim == -1) {
				rn->func(rn->priv);
			}
		}
		
		clock_gettime(CLOCK_MONOTONIC,&stop);
		double timetaken = (stop.tv_sec-start.tv_sec)*1000000000;
		timetaken+=stop.tv_nsec-start.tv_nsec;
		iu_printf("#FRAME-RATE(%f-fps)\n",1000000000./timetaken);
		drv->swapbufs(window->surf);
	}
	_iu_deinit();
	ct_mjx.close();
	flue_deinit();
	y_log_fileend(&iulog);
}
