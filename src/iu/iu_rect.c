#include "iu.h"
#include "iu_rect.h"
void static draw(struct iu_rect *__r) {	
	ct_com.g->crect(__r->vtx,0,__r->c.r,__r->c.g,__r->c.b,__r->c.a);
	iu_printf("rect hole-%p: %u, %u -> %u, %u\n",__r->_w.cap,
		__r->_w.cap->sub.hb.x,__r->_w.cap->sub.hb.y,
		__r->_w.cap->sub.hb.xfar,__r->_w.cap->sub.hb.yfar
	);
}

#include "../io.h"
void static move(_ulonglong __arg, struct iu_rect *__r,_int_u __x, _int_u __y) {
	__r->_w.x = __x;
	__r->_w.y = __y;
	__r->vtx[0] = __r->_w.x;
	__r->vtx[1] = __r->_w.y;
	__r->vtx[2] = __r->_w.x+__r->_w._w;
	__r->vtx[3] = __r->_w.y+__r->_w._h;
	
	iu_printf("IURECT, %f, %f, %f, %f\n",__r->vtx[0],__r->vtx[1],__r->vtx[2],__r->vtx[3]);
}

void static input(struct iu_widget *__w,struct iu_rect *__r, struct ct_msg *m) {
	if (m->code == IU_RECT_HIDE) {
		__r->_w.skim = 0;
	}else if (m->code == IU_RECT_SHOW) {
		__r->_w.skim = -1;
	}
	__r->_w.cap->sub.skim = __r->_w.skim;
}

/*
	NOTE:
		update is not realy well defined, update -> input?

		update - its ment to convert are inbound message from outside to inside
		
		for example, update mesages may condence into input messages,

		say we want to send an input to the button when discrete things happen,
		so say that a button if inside triggers an event that causes the button to change its colour,
		this may cascade into more then a single input, so it could change its color but also move it to lets say another part of the screen.

		so in input we can define some routine say

		BUTTON_EVENT0 = move button to the left by 20 pixels,
		BUTTON_EVENT1 = change colour

		input is where the functionality lies, where as update its
		for things to happen? 

		update can overlook stuff, but input can not.
*/
void static update(struct iu_rect *__r, struct ct_msg *m) {
	iu_printf("RECT message update.\n");
	if (m->code == CY_BTN_LEFT) {
		struct ct_msg _m;
		_m.x = m->x;
		_m.y = m->y;
		_int_u code = 0;
		if(m->value == CT_PRESS) {
		code = IU_RECT_PRESS;
		}else if(m->value == CT_RELEASE) {
		code = IU_RECT_RELEASE;
		}else{
			return;
		}
		if (__r->_w.output != NULL) {
			_m.code = __r->_w.codetab[code];
			__r->_w.output(__r,__r->_w.out,&_m);
		}
	}
}
void static sizer(struct iu_rect *__r) {
	iu_wgsize_calu(__r);
	__r->vtx[2] = __r->_w.x+__r->_w._w;
    __r->vtx[3] = __r->_w.y+__r->_w._h;
	iu_wgevaluate_size(__r);
}
void iu_rect_init(struct iu_rect *__r) {
	iu_wginit(__r);
	__r->_w.size = sizer;
	__r->c.r = 1;
	__r->c.g = 1; 
	__r->c.b = 1;
	__r->c.a = 1;
	__r->_w.input = input;
	__r->_w.draw = draw;
	__r->_w.move = move;
	__r->_w.cap->sub.arg = __r;
	__r->_w.cap->sub.update = update;
}

