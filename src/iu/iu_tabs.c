#include "iu_tabs.h"
#include "../havoc/common.h"
#include "../m_alloc.h"
void iu_tabupdate(struct iu_tabs *__t, struct ct_msg *m) {
	iu_printf("message was forwared to children of tabs{code: %u, value: %u}.\n",m->code,m->value);
	struct iu_msgdata _m = {*m};
	_m.x = _m.m.x;
	_m.y = _m.m.y;

	_m.m.x-=__t->_w.x;
	_m.m.y-=__t->_w.y;
	struct iu_window *w;
	w = iu_com.w;
	void *tmp = iu_com.capt;
	iu_com.capt = &__t->capt;
	iu_message(0,&_m);
	iu_com.capt = &__t->page->capt;
	iu_message(0,&_m);

	iu_com.capt = tmp;
}

void static draw(struct iu_tabs *__r) {
	float vtx[4] = {
		__r->_w.x,
		__r->_w.y+24,
		__r->_w.x+__r->_w._w,
		__r->_w.y+__r->_w._h+24
	};

	ct_com.g->crect(vtx,0,0.7,0.7,0.7,1);
	vtx[1] = __r->_w.y;
	vtx[2] = __r->_w.x+__r->_w._w;
	vtx[3] = __r->_w.y+24;
	ct_com.g->crect(vtx,0,1,1,1,1);

	float transform[16] = {
		1,0,0,__r->_w.x,
		0,1,0,__r->_w.y,
		0,0,0,0,
		0,0,0,1
	};

	float tmp[16];
	h_matcopY(tmp,ct_com.transform);
	h_matcopY(ct_com.transform,transform);

	_int_u i;
	i = 0;
	for(;i != __r->num_pages;i++) {
		__r->pages[i].button->_w.draw(__r->pages[i].button);
	}
	
	i = 0;
	struct iu_widget *w;
	for(;i != __r->page->n;i++) {
		w = __r->page->w[i];
		w->draw(w);
	}
	h_matcopY(ct_com.transform,tmp);
}
/*
void static movwidget(struct iu_widget *w, _int_u __x, _int_u __y) {
	_int_u i;
	i = 0;
	for(;i != w->nchilds_in_list;i++) {
		movwidget(w->childs_list[i],__x,__y);
	}

	w->dhd = w->x+__x;
	w->dvd = w->y+__y;
}
*/
void static move(_ulonglong __arg, struct iu_tabs *__t,_int_u __x, _int_u __y) {
	__t->_w.x = __x;
	__t->_w.y = __y;
	/*_int_u i;
	i = 0;
	struct iu_widget *w;
	for(;i != __t->page->n;i++) {
		w = __t->page->w[i];
		movwidget(w,__x,__y);
	}
	*/
}
#include "../strf.h"

static _int_u transtab[] = {
	[IU_BTN_PRESS]  =  0
};

void static btn_func(struct iu_widget*w,struct iu_tabpage *__t, struct ct_msg *m) {
	if(m->code == 0) {
		__t->priv->page = __t->priv->pages+__t->idx;
	}
}

/*
	add diffrent modes

	ALIGN MIDDLE
	FIT/FILL with button of max width
*/
void static initpage(struct iu_tabs *__tb, struct iu_tabpage *__t,_int_u __num) {
	__t->n = 0;
	iu_captinit(&__t->capt);
	struct iu_btn *b;
	__t->button = b = m_alloc(sizeof(struct iu_btn));
	
	iu_btn_init(b);
	b->_w.w = 80;
	b->_w.h = 20;
	char buf[64];
	ffly_strf(buf,0,"page-%u",__num);
	iu_btn_with_text(b,buf);
	iu_wgmove(0,b,2+__num*82,2);
	__t->priv = __tb;
	b->_w.codetab = transtab;
	b->_w.output = btn_func;
	b->_w.out = __t;
	__t->idx = __num;
	b->_w.cap->cap = &__tb->capt;
	iu_wgtrajection(b);
}
void iu_tab_init(struct iu_tabs *__t) {
	iu_wginit(__t);
	iu_captinit(&__t->capt);
	initpage(__t,__t->pages,0);
	initpage(__t,__t->pages+1,1);
	initpage(__t,__t->pages+2,2);
	initpage(__t,__t->pages+3,3);
	__t->num_pages = 4;
	__t->_w.cap->sub.arg = __t;
	__t->_w.cap->sub.update = iu_tabupdate;
	__t->page = __t->pages;
	__t->_w.move = move;
	__t->_w.draw = draw;
}


void iu_tab_wadd(struct iu_tabpage *__t, struct iu_widget *__w) {
	__w->yd = -20;
	__t->w[__t->n++] = __w;
	__w->cap->cap = &__t->capt;
}
