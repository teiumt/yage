#include "iu_window.h"
#include "iu_common.h"
#include "../ffly_def.h"
#include "iu.h"
/*
	window is not! a widget.

	however its a control widget.
	using iu_board as its foundation
*/
void iu_wdlink(struct iu_composer *__b, struct iu_window *__w) {
	iu_brdlink(__b,__w);
}

void static update(struct iu_window *__w, struct ct_msg *m) {
	if(m->value == CT_RELEASE && !__w->_b.griped) {
		iu_com.message = iu_message;
		__w->_b.griped = -1;
	}

	if(m->value == CT_PTR_M && !__w->_b.griped) {
		float x,y;
		x = m->x;
		y = m->y;
		x-=__w->_b.x;
		y-=__w->_b.y;

		__w->move_decor(__w->_b.parent,__w->decor,x,y);
		iu_wgmove(__w->_b.parent,__w,x,y);
	}else{
		iu_brdupdate(__w,m);
	}
}

void iu_wdhide(struct iu_window *__w) {
	iu_brdhide(__w);
	_int_u code;
	code = !__w->_b._w.skim?IU_BOARD_HIDE:IU_BOARD_SHOW;
	struct ct_msg _m;
	_m.code = __w->decor_codetab[code];
	__w->decor_output(__w,__w->decor,&_m);
}

void static input(struct iu_widget*w,struct iu_window *__w, struct ct_msg *m) {
	if (m->code == IU_BOARD_HIDE_SHOW) {
		iu_wdhide(__w);
	}else if(m->code == IU_BOARD_GRAB) {
        iu_com.message = update;
        iu_com.msgarg = __w;
        __w->_b.griped = 0;
        __w->_b.x = m->x-__w->_b._w.x;
        __w->_b.y = m->y-__w->_b._w.y;
	}else{
		iu_brdinput(NULL,__w,m);
	}
}

void iu_wdinit0(struct iu_window *__w) {
	iu_board_init(__w);
	__w->_b._w.input = input;
}

void iu_wdinit(struct iu_window *__w) {
	iu_brdinit(__w);
}
