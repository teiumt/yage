#ifndef __iu__toolbar__h
#define __iu__toolbar__h
#include "iu_widget.h"
#define TOOLBAR_RESIZE	0
struct iu_toolbar {
	struct iu_widget _w;
	struct iu_widget *childs[7];
	_int_u nc;
	_int_u row;
	/*
		does it sway to the left or bottom?
	*/
	_int_u sway;
	_int_u w,h;
};
void iu_toolbar_confine(struct iu_toolbar *__tb);
void iu_toolbar_init(struct iu_toolbar*);
void iu_toolbar_add(struct iu_toolbar*,struct iu_widget*,_int_u);
#endif/*__iu__toolbar__h*/
