#include "iu_signal.h"
struct iu_sigrange* iu_sigrangefor(struct iu_sigrange *__r,_int_u __n,_64_u __mask, _int_u __idx) {
	if(__idx>3)return NULL;
	_int_u i;
	i = 0;
	for(;i != __n;i++) {
		if(__mask&__r[i].mask[__idx]) {
			return __r+i;
		}
	}
	return NULL;
}

