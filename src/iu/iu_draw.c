#include "iu.h"
#include "iu_rect.h"
#include "iu_btn.h"
#include "iu_text.h"
void static draw_boarder(float __thickness, float x, float y, float w, float h, float __r, float __g, float __b, float __a) {
 	//very bad
	float vtx[4];
	//left
	vtx[0] = x;
	vtx[1] = y;
	vtx[2] = x+__thickness;
	vtx[3] = y+h;
	ct_com.g->crect(vtx,0,__r,__g,__b,__a);

	//right
	vtx[0] = x+w;
	vtx[1] = y;
	vtx[2] = x+w-__thickness;
	vtx[3] = y+h;
	ct_com.g->crect(vtx,0,__r,__g,__b,__a);

	vtx[0] = x+__thickness;
	vtx[1] = y;
	vtx[2] = x+w-__thickness;
	vtx[3] = y+__thickness;
	ct_com.g->crect(vtx,0,__r,__g,__b,__a);

	vtx[0] = x+__thickness;
	vtx[1] = y+h;
	vtx[2] = x+w-__thickness;
	vtx[3] = y+h-__thickness;
	ct_com.g->crect(vtx,0,__r,__g,__b,__a);
}

#define THICKNESS 1
#define BOARDER_COLOUR 0,0,0,1

/*
	TODO: rename to simple !typo
*/
void iu_btn_draw_sample(struct iu_btn *__b) {
	__b->rect->c = __b->c;

	__b->rect->_w.draw(__b->rect);
	__b->text->_w.draw(__b->text);
	struct iu_widget *w = __b;
	draw_boarder(THICKNESS,w->x,w->y,w->_w,w->_h,BOARDER_COLOUR);
}
