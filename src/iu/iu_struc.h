#ifndef __iu__struc__h
#define __iu__struc__h
#include "../citric/citric.h"
struct iu_hbox {
    _int_s x, y;
    _int_s xfar, yfar;
};

struct iu_crsub {
    struct iu_hbox hb;
    _ulonglong arg;
    _8_s inside;
	_8_s skim;
    void(*update)(_ulonglong, struct ct_msg*);
};

struct iu_msgdata {
	struct ct_msg m;
	_int_u x,y;
	_64_u id;
	/*64-qword of data space*/
};

#define IU_PTR_ENTER    3
#define IU_PTR_EXIT     4
/*
    message capture rectangle
*/
struct iu_caplv;
struct iu_caprect {
    struct iu_crsub sub;
    struct iu_caprect *next;
	struct iu_caplv *cap;
	_int_u lv;
};

struct iu_caplv {
	struct iu_caprect *capt[4*4];
	_64_u msk;
};

struct iu_icon {
    float uv[4];
};

struct iu_vec2{
    float x,y;
};
struct iu_render;
struct iu_render_band {
	struct iu_render *rt;
};

struct iu_render_bin {
	struct iu_render_band bns[4*4];
	_64_u msk;
};


struct iu_render {
    struct iu_render *next;
    void(*func)(void*);
    void *priv;
	void *_2priv;
	_8_s skim;
};
//segregative perpose

//composer/orchestrator
/*
	to whom has been entrusted to configure and layout.
	ie. the composer just composes what we want to see.
*/
struct iu_composer {
	struct ct_viewport vp;
	//aspect ratio(width over height)
	float vpw;
	float vph;
	float asp;
	union{
		struct {
			struct iu_render_band bns[4*4];
			_64_u msk;//dont move
		};
		struct iu_render_bin bin;
	};	
	struct iu_caplv *capt;
};

#endif/*__iu__struc__h*/
