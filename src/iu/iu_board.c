#include "iu.h"
#include "iu_board.h"
_int_u cnt_gbl = 1;
void iu_brdupdate(struct iu_board *__brd, struct ct_msg *m) {
	iu_printf("message was forwared to children of board{code: %u, value: %u}.\n",m->code,m->value);
	struct iu_msgdata _m = {*m};
	_m.x = _m.m.x;
	_m.y = _m.m.y;
	_m.m.x-=__brd->comp.vp.translate[0];
	_m.m.y-=__brd->comp.vp.translate[1];
	

	struct iu_window *w;
	w = iu_com.w;
	iu_load(__brd);
	iu_message(0,&_m);
	iu_load(w);
}

void  iu_brdlink(struct iu_composer *__b, struct iu_board *__brd) {
	struct ct_viewport *p = &__b->vp;
	__brd->comp.vp = (struct ct_viewport){
		.translate = {CT_VPSX(p)*__brd->_w.x,CT_VPSY(p)*__brd->_w.y,0,0},//translate
		.scale = {1.,1.,1,1}//scale
	};
	ct_com.g->initvp(&__brd->comp.vp);
	iu_printf("BOARDLINK: translate{%f, %f}, scale{%f, %f}\n",
		__brd->comp.vp.translate[0],
		__brd->comp.vp.translate[1],
		__brd->comp.vp.scale[0],
		__brd->comp.vp.scale[1]
	);
	__brd->comp.vpw = 1./__brd->comp.vp.scale[0];
	__brd->comp.vph = 1./__brd->comp.vp.scale[1];
	__brd->comp.asp = __brd->comp.vp.scale[0]/__brd->comp.vp.scale[1];
}
void iu_board_load(struct iu_board *w) {
	iu_com.b = &w->comp;
	iu_com.w = w;
	iu_com.capt = &w->capt;
}

void static move(_ulonglong __arg, struct iu_board *__brd,_int_u __x, _int_u __y) {
	__brd->_w.x = __x;
	__brd->_w.y = __y;
	iu_brdlink(__brd->parent,__brd);
}


void static draw(struct iu_board *__r) {
	float vtx[4] = {
		0,
		0,
		__r->_w._w,
		__r->_w._h
	};

	ct_com.g->crect(vtx,0,0.7,0.7,0.7,1);
}

void iu_brdinput(struct iu_widget*w,struct iu_board *__brd, struct ct_msg *m) {
	if (m->code == IU_BOARD_HIDE_SHOW) {
		iu_brdhide(__brd);
	}
}

void iu_brdhide(struct iu_board *__brd) {
	__brd->_w.skim = !__brd->_w.skim?-1:0;
	__brd->_w.cap->sub.skim = __brd->_w.skim;
	__brd->rn->skim = __brd->_w.skim;
}
void iu_brdinit(struct iu_board *__brd) {
	ct_com.g->initvp(&__brd->comp.vp);
	iu_bininit(&__brd->comp.bin);
	__brd->rn = &iu_com.draw[cnt_gbl++];	
	__brd->rn->priv = &__brd->comp;
	__brd->rn->func = iu_compose;
	__brd->rn->skim = -1;
	iu_captinit(&__brd->capt);
}

void iu_board_init(struct iu_board *__brd) {
	iu_wginit(__brd);
	__brd->_w.move = move;
	__brd->_w.update = iu_brdupdate;
	__brd->_w.input = iu_brdinput;
	__brd->_w.draw = draw;
	__brd->_w.cap->sub.arg = __brd;
	__brd->_w.cap->sub.update = iu_brdupdate;
	__brd->griped = -1;
	iu_captinit(&__brd->capt);
}

float iu_board_xalign(struct iu_board *__brd, struct iu_widget *__w) {
	float hz;
	hz = ((float)__brd->_w._w)*0.5;
	return hz-((float)__w->_w)*0.5;
}

void iu_board_deinit(struct iu_board *__brd) {

}
