#include "iu_tuck.h"
#include "../m_alloc.h"
void static draw(struct iu_tuck *__tc) {
	struct iu_board *brd = __tc;
	float vtx[4] = {
		0,
		0,
		20,
		brd->_w._h
	};
	ct_com.g->crect(vtx,0,1,1,1,1);
	
	__tc->collapse->_w.draw(__tc->collapse);
}

static _int_u transtab[] = {
    [IU_BTN_PRESS]  =  0
};

void static btn_func(struct iu_widget*w,struct iu_tuck *__t, struct ct_msg *m) {
	if(m->code == 0) {
		struct iu_board *brd = __t;
		brd->comp.msk ^= 1;
		struct iu_btn *b = w;
		if(brd->comp.msk&1) {
			brd->capt.msk = 0xf;
			b->icon = __t->icon0;
		}else{
			brd->capt.msk = 0xf^1;
			b->icon = __t->icon1;
		}
	}
}

void iu_tuck_init(struct iu_tuck *__tc) {
	struct iu_btn *b;
	__tc->collapse = b = m_alloc(sizeof(struct iu_btn));
	
	struct iu_board *brd = __tc;
	
	iu_btn_init(b);

	b->icon = __tc->icon0;
	b->tex = __tc->tex;
	iu_btn_with_icon(b);
	//iu_btn_with_text(b,"Tools");
	b->c = (struct ct_colour){0,0,0,1};
	b->_w.w = 20;
	b->_w.h = 20;
	iu_wgsize1(b);
	iu_wgmove(NULL,b,0,__tc->_w._h-b->_w.h);
	b->_w.cap->lv = 4;
	iu_wgtrajection(b);
	b->_w.codetab = transtab;
    b->_w.output = btn_func;
	b->_w.out = __tc;
	b->_w.priv = __tc;
	
	struct iu_render *rn;
	rn = _iu_render(&brd->comp,4);
	rn->func = draw;
	rn->priv = __tc;
}
