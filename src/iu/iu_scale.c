#include "iu.h"
#include "iu_scale.h"
#include "../strf.h"
#define WIDTH	102
#define HEIGHT	8
void static move(_ulonglong __arg, struct iu_scale *__scale,_int_u __x,_int_u  __y) {
	__scale->_w.x = __x;
	__scale->_w.y = __y;
}

void static draw(struct iu_scale *__scale) {
	float x = __scale->_w.x,y = __scale->_w.y;
	float vtx[4];
	vtx[0] = x;
	vtx[1] = y;
	vtx[2] = x+WIDTH;
	vtx[3] = y+HEIGHT;
	ct_com.g->crect(vtx,0,1,1,1,1);
	float xdi = WIDTH/20.;

	float xd = WIDTH*__scale->value;
	vtx[0] = x+xd;
	vtx[1] = y-5;
	vtx[2] = x+xdi+xd;
	vtx[3] = y+HEIGHT+5;
	ct_com.g->crect(vtx,0,1,0,0,1);

	char buf[128];
	buf[0] = '\0';
	_int_u len;
	len = _ffly_nds(buf,__scale->value*100.);
	struct iu_label *tx = &__scale->tx;
	tx->size = 9;
	tx->s = buf;
	tx->len = len;
	tx->_w.x = vtx[2]+1;
	tx->_w.y = vtx[1]-4;
	tx->_w.draw(tx);
}
static _8_s lockon = -1;
void static update(struct iu_scale *__scale, struct ct_msg *m) {
	
	if(!lockon && m->value == CT_RELEASE) {
		lockon = -1;
		iu_com.message = iu_message;
	}
	if(m->value == CT_PRESS) {
		lockon = 0;
		iu_com.message = update;
		iu_com.msgarg = __scale;
	}
	if((m->value == CT_PRESS && m->code == CY_BTN_LEFT) || !lockon) {
		/*
			we need to remove "m->x)*iu_com.b->vpw" and push it into the message meta data?
		*/
		float xd = (((float)m->x)*iu_com.b->vpw)-__scale->_w.x;
	
		__scale->value = xd*(1./WIDTH);
		if(__scale->value<0.) __scale->value = 0;
		if(__scale->value>1.) __scale->value = 1;
	}
}

void iu_scale_init(struct iu_scale *__scale) {
	iu_wginit(__scale);
	__scale->_w.draw = draw;
	__scale->_w.move = move;
	__scale->value = 0;
	__scale->_w._w = WIDTH;
	__scale->_w._h = HEIGHT;
	__scale->_w.cap->sub.arg = __scale;
	__scale->_w.cap->sub.update = update;
	iu_label_init(&__scale->tx);
}

void iu_scale_deinit(struct iu_scale *__scale) {

}
