#include "iu.h"
#include "iu_rect.h"
#include "iu_btn.h"
#include "iu_text.h"
#include "../m_alloc.h"
void iu_btn_draw_sample(struct iu_btn *__b);
void static draw(struct iu_btn *__b) {
	iu_btn_draw_sample(__b);
}

void static move(_ulonglong __arg, struct iu_btn *__b, _int_u __x, _int_u __y) {
	__b->_w.x = __x;
	__b->_w.y = __y;
}

void static update(struct iu_btn *__b, struct ct_msg *m) {
	if (m->value == IU_PTR_ENTER) {
		__b->save.r = __b->c.r;
		__b->save.g = __b->c.g;
		__b->save.b = __b->c.b;
		__b->c.r = __b->hover.r;
		__b->c.g = __b->hover.g;
		__b->c.b = __b->hover.b;
	}else if (m->value == IU_PTR_EXIT){
		__b->c.r = __b->save.r;
		__b->c.g = __b->save.g;
		__b->c.b = __b->save.b;	
	}else{
		if (m->code == CY_BTN_LEFT) {
			struct ct_msg _m;
			_m.x = m->x;
			_m.y = m->y;
			_m.ctx = 0;
			_int_u code = 0;
			if(m->value == CT_PRESS) {
			code = IU_BTN_PRESS;
			}else{
				return;
			}
			if (__b->_w.output != NULL) {
				_m.code = __b->_w.codetab[code];
				__b->_w.output(__b,__b->_w.out,&_m);
			}
		}
	}
}
void static sizer(struct iu_btn *__b) {
    iu_wgsize_calu(__b);
	struct iu_text *tx = __b->text;
	struct iu_rect *rct = __b->rect;
	tx->_w.wj = __b->_w._w;
	tx->_w.hj = __b->_w._h;

	tx->_w.w = __b->_w._w;
	tx->_w.h = __b->_w._h;
	iu_printf("#BUTTON] requested{%u, %u}\n",tx->_w.w,tx->_w.h);
	iu_wgsize1(tx);
	iu_printf("#BUTTON_TEXT] %u,%u.\n",tx->w,tx->h);
	rct->_w.w = __b->_w._w;
	rct->_w.h = __b->_w._h;
	iu_wgsize1(rct);
	iu_wgevaluate_size(__b);
}

void static sizerno_icon(void *a){
	iu_wgsize_calu(a);
	iu_wgevaluate_size(a);
}

void iu_btn_init(struct iu_btn *__b) {
	iu_wginit(__b);
	__b->c.r = 1;
	__b->c.g = 1;
	__b->c.b = 1;
	__b->c.a = 1;

	__b->_w.childs = NULL;
	__b->_w.next = NULL;
	__b->_w.move = move;
	__b->_w.cap->sub.arg = __b;
	__b->_w.cap->sub.update = update;
	__b->hover = (struct ct_colour){0.1,0.38,0.38};
}


void static draw_icon(struct iu_btn *__b) {
	float vtx[10];
	vtx[0] = __b->_w.x;
	vtx[1] = __b->_w.y;
	vtx[2] = __b->_w.x+__b->_w._w;
	vtx[3] = __b->_w.y+__b->_w._h;
	vtx[4] = __b->icon->uv[0];
	vtx[5] = __b->icon->uv[1];
	vtx[6] = __b->icon->uv[2];
	vtx[7] = __b->icon->uv[1];
	vtx[8] = __b->icon->uv[0];
	vtx[9] = __b->icon->uv[3];
	float overlay[4] = {1,1,1,1};
	ct_com.g->ui_rect(vtx,1,__b->tex,__b->c.r,__b->c.g,__b->c.b,1,overlay);
	iu_printf("%f, %f, %f, %f.\n",__b->icon->uv[0],__b->icon->uv[1],__b->icon->uv[2],__b->icon->uv[3]);
}

void iu_btn_with_icon(struct iu_btn *__b) {
	__b->_w.draw = draw_icon;
	__b->_w.size = sizerno_icon;
	iu_wgsize(__b,1,1);
	__b->c = (struct ct_colour){0,0,0,1};
}

void iu_btn_with_text(struct iu_btn *__b,char const *__text) {
	__b->_w.draw = draw;
	__b->_w.size = sizer;
	struct iu_text *tx;
	tx = iu_wgnew();
	iu_text_default(tx);
	iu_text_init(tx);

	iu_wgchild(tx,__b,__b->_w.move_chain);
	iu_text_deposit(tx,__text,str_len(__text));
	tx->size = 8;
	tx->bits = TX_VALIGN|TX_HALIGN;//pinpoint mode	

	__b->text = tx;

	struct iu_rect *rct;
	rct = iu_wgnew();
	iu_rect_init(rct);

	iu_wgchild(rct,__b,__b->_w.move_chain);
	
	__b->rect = rct;
	rct->_w.w = 1;
	rct->_w.h = 1;
	iu_wgsize(__b,1,1);
	__b->_w.nchilds_in_list = 2;
	__b->_w.childs_list[0] = tx;
	__b->_w.childs_list[1] = rct;
}
