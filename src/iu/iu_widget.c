#include "iu_widget.h"
#include "../m_alloc.h"
#include "iu.h"
#include "../assert.h"

void iu_wgtrajection(struct iu_widget *__w) {
	iu_captattach(__w->cap);
}
void
iu_wgevaluate_size(struct iu_widget *__w) {
	_int_u __x = __w->x;
	_int_u __y = __w->y;

	struct iu_hbox *hb = &__w->cap->sub.hb;
	hb->x = __x;
	hb->y = __y;
	hb->xfar = hb->x+(__w->_w+__w->bbhd);
	hb->yfar = hb->y+(__w->_h+__w->bbvd);
}

void iu_wgmove(struct iu_composer *c, struct iu_widget *__w,_int_u __x,_int_u __y) {
	assert(__w != NULL);
	assert(__w->move_chain<2);
	struct iu_widget *w;
	struct iu_widget *_w = ((_64_u*)__w)+__w->move_chain;
	w = _w->childs;
	while(w != NULL) {
		/*
			moving shit is relatively straight forward
		*/

		iu_wgmove(c,w,__x-__w->xd,__y-__w->yd);
		_w = ((_64_u*)w)+__w->move_chain;
		w = _w->next;
	}
	__x-=__w->xd;
	__y-=__w->yd;
	__w->move(c,__w,__x-__w->xd0,__y-__w->yd0);
	iu_wgevaluate_size(__w);
}

void iu_wgdraw(struct iu_widget *__w) {
	if (!__w->skim)return;
	assert(__w->draw_chain<2);
	struct iu_widget *w;
	struct iu_widget *_w = ((_64_u*)__w)+__w->draw_chain;
	w = _w->childs;
	while(w != NULL) {
		iu_wgdraw(w);
		_w = ((_64_u*)w)+__w->draw_chain;
		w = _w->next;
	}
	__w->draw(__w);
}
/*
	the only reason why sx,sy exist is because of windows,
	is we have a window thats not the same as the parent window
	then this needs to be dependent on the size of the main window.
	meaning we scale it down.
	its also a 2x2 matrix;
*/
void iu_wgsize_calu(struct iu_widget *__w) {
	__w->_w = __w->w;
	__w->_h = __w->h;
}

void iu_wgsize(struct iu_widget *__w,float __sx, float __sy) {
	__w->sx = __sx;
	__w->sy = __sy;
	__w->size(__w);
}

void iu_wgsize1(struct iu_widget *__w) {
	__w->size(__w);
}

void iu_wgchild(struct iu_widget *__w,struct iu_widget *__parent, _64_u __chain) {
	/*
		look sorry im just fucking lazy and cant be botherd with naming structures ie dont want to create new structure for this or put in array because it would require changing crap
		TODO: fix
	*/
	struct iu_widget *_w = ((_64_u*)__w)+__chain;
	__parent = ((_64_u*)__parent)+__chain;
	_w->next = __parent->childs;
	__parent->childs = __w;
}

void iu_wgdefault(struct iu_widget *__w) {
	__w->w = 0;
	__w->h = 0;
	__w->_w = 0;
	__w->_h = 0;
}

void iu_wginit(struct iu_widget *__w) {
	__w->xd = __w->yd = 0;
	__w->xd0 = __w->yd0 = 0;
	__w->output = NULL;	
	__w->parent = NULL;
	__w->draw_chain = 1;
	__w->move_chain = 0;
	__w->childs = NULL;
	__w->skim = -1;
	__w->class = -1;
	__w->childs_draw = NULL;
	__w->cap = iu_captnew();
	__w->cap->sub.update = NULL;

	__w->cntl.cntl_2press.state = 0;
	__w->sx = __w->sy = 1;
	__w->dhd = __w->dvd = __w->bbhd = __w->bbvd = 0;
	__w->rn = NULL;
	__w->nchilds_in_list = 0;
}

/*
	look having many diffrent allocation methods for diffrent widgets is well not the.
	we allocate things like this because if the user needs more data then use a diffrent allocfuncion like iu_wgnew2
	this way we cut down on unnecessary code; ie alot of diffrent malloc functions with diffrent 'sizeof struct example' 
*/
struct iu_widget *iu_wgnew(void) {
	return m_alloc(sizeof(struct iu_widget)+IU_WIDGET_EXTRA);
}
/*
	adjust location with accordance to parent
*/
void iu_cntl_alwatp(struct iu_widget *caller,struct iu_widget *w,struct ct_msg *m) {
	if(m->code == IU_ALWATP_ADJUSTMENT) {
		float x,y;
		x = caller->x;
		y = caller->y+caller->_h;
		iu_printf("IU_ALWATP_ADJUSTMENT. to: %f, %f.\n",x,y);
		iu_wgmove(NULL,w,x,y);
	}else{
		caller->cntl_output(caller,w,m);
	}
}

void iu_cntl_2press(struct iu_widget *caller,struct iu_widget *w,struct ct_msg *m) {
	caller->cntl.cntl_2press.state^=1;

	caller->codetab = caller->codetab_resv[caller->cntl.cntl_2press.state];
	caller->cntl_output(caller,w,m);
}

