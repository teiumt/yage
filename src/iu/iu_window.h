#ifndef __iu__window__h
#define __iu__window__h
#include "iu_widget.h"
#include "../citric/citric.h"
#include "iu_struc.h"
#include "iu_board.h"
struct iu_window {
	union {
		/*if its part of the ui domain*/
		struct iu_board _b;
	   	struct ct_surface *surf;
	};

	void(*move_decor)(_ulonglong,_ulonglong,_int_u,_int_u);
	void(*decor_output)(struct iu_widget*,_ulonglong,struct ct_msg*);
	_int_u *decor_codetab;
	_ulonglong decor;
};
void iu_wdinit0(struct iu_window*);
void iu_wdlink(struct iu_composer*,struct iu_window*);
void iu_wdinit(struct iu_window*);
void iu_wdhide(struct iu_window*);
void iu_window_new(struct iu_window*);
void iu_window_map(struct iu_window*,_int_u,_int_u,_int_u,_int_u);
void iu_window_unmap(struct iu_window*);
void iu_window_destroy(struct iu_window*);
void iu_window_move(struct iu_window*,_int_u,_int_u);
#endif/*__iu__window__h*/
