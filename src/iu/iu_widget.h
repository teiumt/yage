#ifndef __iu__widget__h
#define __iu__widget__h
#include "iu_struc.h"
#define IU_WIDGET_SIZEMAX (sizeof(struct iu_widget)+IU_WIDGET_EXTRA)
#define IU_WIDGET_EXTRA (512*8)
#define IU_WIDGET_SPACE _64_u userdata[128];
struct iu_cntl_2press {
	_8_u state;
};
#define IU_WGCLASS_TEXT 0
#define IU_ALWATP_ADJUSTMENT 0x41
struct iu_widget {
	/*
		dont change order or remove from top placement
	*/
	struct iu_widget *childs;
	struct iu_widget *childs_draw;
	struct iu_widget *next;
	struct iu_widget *next_draw;

	_64_u draw_chain;
	_64_u move_chain;
	struct iu_caprect *cap;
	union {
		struct iu_cntl_2press cntl_2press;
	} cntl;
	void(*cntl_output)(struct iu_widget*,struct iu_widget*,struct ct_msg*);
	/*
		how are outputs map to inputs of other components
		BTN_CLICK -> RECT_TOGGLE
	*/
	_int_u *codetab;
	_int_u *codetab_resv[2];
	void(*draw)(struct iu_widget*);
	void(*move)(_ulonglong,struct iu_widget*,_int_u,_int_u);
	void(*output)(struct iu_widget*,struct iu_widget*,struct ct_msg*);
	void(*input)(struct iu_widget*,struct iu_widget*,struct ct_msg*);
	void(*update)(struct iu_widget*,struct ct_msg*);
	void(*size)(struct iu_widget*);
	void(*ascertain)(struct iu_widget*);
	void(*tellof_size)(struct iu_widget*,struct iu_widget*);
	struct iu_render *rn;
	_32_u class;
	void(*control_sizer)(struct iu_widget*);
	struct iu_widget *parent;
	_ulonglong out;

	_int_u width_limit;
	_int_u x,y;

	/*
		_w = (w*_c[0])+(h*_c[1])
		_h = (w*_c[2])+(h*_c[3])
	*/
	/*
		final size(viewport specific)
	*/
	_int_u _w,_h;
//	float _c[4];
	/*
		size thats is constatained to its parent
	*/
	_int_u w,h;
	/*
		negative displacment ie Xnew = Xold-Dx
	
		xd & yd will affect both itself and its children.

		xd0 & yd0 will only have an affect on itself and not its children
	
		if we move a widget somwhere it will have this displacement
	*/


	/*displacment for all*/
	union {
		struct {
			_int_s xd;
			_int_s yd;
		};
		_int_u xdyd[2];
	};

	/*parent displacment only*/
	_int_s xd0,yd0;
	/*
		there is no such thing as parent widget,
		but however imaginary parents do exist.

		wj = parent width
		hj = parent height

		why not just use a parent pointer?
		because we want to be able to tweek values like this.
		using a parent pointer would cause issues to emerge.
		what can we do?
	*/
	_int_u wj,hj;
	float sx,sy;	
	
	/*
		bounding box
	*/

	float bbhd,bbvd;

	//draw location
	float dhd,dvd;
	_8_s skim;
	struct iu_widget *childs_list[16];
	_int_u nchilds_in_list;
	void *priv;
};
void iu_wgtrajection(struct iu_widget *__w);
void iu_wgdefault(struct iu_widget*);
void iu_cntl_alwatp(struct iu_widget *caller,struct iu_widget *w,struct ct_msg *m);
void iu_wgsize_calu(struct iu_widget *__w);
void
iu_wgevaluate_size(struct iu_widget *__w);
void iu_wgsize1(struct iu_widget*);
void iu_wgchild(struct iu_widget*,struct iu_widget*,_64_u);
void iu_wgdraw(struct iu_widget*);
void iu_wginit(struct iu_widget*);
void iu_wgmove(struct iu_composer*,struct iu_widget*,_int_u,_int_u);
struct iu_widget *iu_wgnew(void);
void iu_wgsize(struct iu_widget*,float,float);
void iu_cntl_2press(struct iu_widget*,struct iu_widget*,struct ct_msg*);
#endif/*__iu__widget__h*/
