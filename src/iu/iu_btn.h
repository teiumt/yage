#ifndef __iu__btn__h
#define __iu__btn__h
#include "iu.h"
#include "iu_text.h"
#include "iu_rect.h"
#define IU_BTN_PRESS	0

struct iu_btn {
	struct iu_widget _w;
	struct iu_text *text;
	struct iu_rect *rect;
	struct ct_texture *tex;
	struct iu_icon *icon;
	struct ct_colour c, save;
	struct ct_colour hover;
};
_Static_assert(sizeof(struct iu_btn)<IU_WIDGET_SIZEMAX,"");
void iu_btn_init(struct iu_btn*);
void iu_btn_with_icon(struct iu_btn*);
void iu_btn_with_text(struct iu_btn*,char const*);
#endif/*__iu__btn__h*/
