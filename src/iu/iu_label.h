#ifndef __iu__label__h
#define __iu__label__h
#include "iu_widget.h"
struct iu_label {
	struct iu_widget _w;
	char const *s;
	_int_u len;
	struct ct_colour c;
	float size;
};


void iu_label_init(struct iu_label*);
void iu_label_text(struct iu_label*,char const*);

#endif/*__iu__label__h*/
