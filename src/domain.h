# ifndef __f__domain__h
# define __f__domain__h

/*
	a space within the engine

	so src/bron would be one 
	src/vec would be one

	might need to rethink
*/
#include "msg.h"
#define _DOM_NOWHERE (doms)		//0
#define _DOM_SYSTHREAD (doms+1)		//1
#define _DOM_LAT (doms+2)			//2
#define _DOM_CORE (doms+3)			//3
#define _DOM_NET (doms+4)			//4
#define _DOM_TCP (doms+5)			//5
#define _DOM_UDP (doms+6)			//6
#define _DOM_SYSCONFIG (doms+7)		//7
#define _DOM_FIREFLY (doms+8)		//8
#define _DOM_TC (doms+9)				//9
#define _DOM_RS (doms+10)				//10
#define _DOM_SYSSCHED (doms+11)		//11
#define _DOM_CACHE (doms+12)			//12
#define _DOM_BOG (doms+13)			//13
#define _DOM_ENGINE (doms+14)			//14
#define _DOM_EVENT (doms+15)			//15
#define _DOM_SYSQUEUE (doms+16)		//16
#define _DOM_SYSPIPE (doms+17)		//17
#define _DOM_NT (doms+18)				//18
#define _DOM_GRFJOB (doms+19)			//19
#define _DOM_VAT (doms+20)			//20
#define _DOM_FLUE (doms+21)		//21
#define _DOM_PULSE (doms+22)			//22
#define _DOM_BILLET (doms+23)			//23
#define _DOM_WH (doms+24)				//24
#define _DOM_SYSIO (doms+25)			//25
#define _DOM_BOLE (doms+26)			//26
#define _DOM_SYSBUFF (doms+27)		//27
#define _DOM_FSSM (doms+28)			//28
#define _DOM_MFS (doms+29)			//29
#define _DOM_TMH (doms+30)			//30
#define _DOM_PMS (doms+31)			//31
#define _DOM_IC (doms+32)				//32
#define _DOM_IHC (doms+33)				//33
#define _DOM_FLUE_NT _DOM_FLUE
#define _DOM_HAVOC (doms+34)
# endif /*__f__domain__h*/
