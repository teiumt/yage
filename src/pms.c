# include "pms.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "tmh.h"
# include "msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_PMS)
struct f_pms_ent* f_pms_open(_64_u __id) {
	struct f_pms_ent *e;
	e = (struct f_pms_ent*)__f_mem_alloc(sizeof(struct f_pms_ent));
	
	f_tmh_wokp w;
	e->priv = w = f_tmh_open(__id);

	e->flags = w->flags&F_TMG_WFM;
	return e;
}

void f_pms_map(struct f_pms_ent *__e, _int_u __size) {
	if ((__e->flags&F_PMS_MAPPED)>0) {
		MSG(WARN, "mapping the mapped is not possible.\n");
		return;
	}
	f_tmh_map(__e->priv, __size);
}

void f_pms_pwrite(struct f_pms_ent *__e, void *__buf, _int_u __size, _int_u __offset) {
	f_tmh_write(__e->priv, __buf, __size, __offset);
}

void f_pms_pread(struct f_pms_ent *__e, void *__buf, _int_u __size, _int_u __offset) {
	f_tmh_read(__e->priv, __buf, __size, __offset);
}

void f_pms_close(struct f_pms_ent *__e) {
	f_tmh_close(__e->priv);
	__f_mem_free(__e);
}
