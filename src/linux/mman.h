# ifndef __ffly__linux__mmap__h
# define __ffly__linux__mmap__h
# include "../y_int.h"
#define PROT_READ	0x1
#define PROT_WRITE	0x2
#define PROT_EXEC	0x4	

#define MAP_SHARED	0x01
#define MAP_PRIVATE	0x02	
#define MAP_ANONYMOUS	0x20

#define MREMAP_MAYMOVE 1
#define MAP_FAILED ((void*)-1)
void* mmap(void*, _64_u, _64_u, _64_u, _64_u, _64_u);
_64_u mremap(_64_u,_64_u,_64_u,_64_u,_64_u);
_32_s munmap(void*, _64_u);
# endif /*__ffly__linux__mmap__h*/
