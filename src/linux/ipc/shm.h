# ifndef __ffly__linux__shm__h
# define __ffly__linux__shm__h
# include "../../y_int.h"
# include "../types.h"
# include "../ipc.h"

struct shmid_ds {
	struct ipc_perm shm_perm;
	_32_s shm_segsz;
	__linux_time_t shm_atime;
	__linux_time_t shm_dtime;
	__linux_time_t shm_ctime;
	__linux_ipc_pid_t shm_cpid;
	__linux_ipc_pid_t shm_lpid;
	_16_u shm_nattch;
	_16_u shm_unused;
	void *shm_unused2;
	void *shm_unused3;
};

_32_s shmctl(_32_s, _32_s, struct shmid_ds*);
_32_s shmget(__linux_key_t, __linux_size_t, _32_s);
void* shmat(_32_s, void*, _32_s);
_32_s shmdt(void*);
# endif /*__ffly__linux__shm__h*/
