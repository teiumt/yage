# ifndef __ffly__linux__resource__h
# define __ffly__linux__resource__h
# include "../y_int.h"
# include "types.h"
# include "time.h"

struct rusage {
	struct timeval ru_utime;
	struct timeval ru_stime;
	_64_s ru_maxrss;
	_64_s ru_ixrss;
	_64_s ru_idrss;
	_64_s ru_isrss;
	_64_s ru_minflt;
	_64_s ru_majflt;
	_64_s ru_nswap;
	_64_s ru_inblock;
	_64_s ru_oublock;
	_64_s ru_msgsnd;
	_64_s ru_msgrcv;
	_64_s ru_nsignals;
	_64_s ru_nvcsw;
	_64_s ru_nivcsw;
};

# endif /*__ffly__linux__resource__h*/
