# ifndef __ffly__linux__input__h
# define __ffly__linux__input__h
# include "../y_int.h"
# include "time.h"
# include "ioctl.h"

#define EVIOCGID	_IOR('E', 0x02, struct input_id)
struct input_id {
    _16_u bustype;
    _16_u vendor;
	_16_u product;
    _16_u version;
};

struct input_event {
	struct timeval time;
	_16_u type;
	_16_u code;
	_32_s value;
};

#define ID_BUS          0
#define ID_VENDOR       1
#define ID_PRODUCT      2
#define ID_VERSION      3

#define BUS_PCI         0x01
#define BUS_ISAPNP      0x02
#define BUS_USB         0x03
#define BUS_HIL         0x04
#define BUS_BLUETOOTH       0x05
#define BUS_VIRTUAL     0x06

#define BUS_ISA         0x10
#define BUS_I8042       0x11
#define BUS_XTKBD       0x12
#define BUS_RS232       0x13
#define BUS_GAMEPORT        0x14
#define BUS_PARPORT     0x15
#define BUS_AMIGA       0x16
#define BUS_ADB         0x17
#define BUS_I2C         0x18
#define BUS_HOST        0x19
#define BUS_GSC         0x1A
#define BUS_ATARI       0x1B
#define BUS_SPI         0x1C
#define BUS_RMI         0x1D
#define BUS_CEC         0x1E
#define BUS_INTEL_ISHTP     0x1F


# endif /*__ffly__linux__input__h*/
