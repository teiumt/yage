# ifndef __ffly__linux__in__h
# define __ffly__linux__in__h
# include "../y_int.h"
# include "types.h"
struct in_addr {
	_32_u s_addr;
};

#define __SOCK_SIZE__ 16
struct sockaddr_in {
	__linux_sa_family_t sin_family;
	_16_u sin_port;
	struct in_addr sin_addr;
	_8_u __pad[__SOCK_SIZE__-sizeof(_16_u)-
		sizeof(__linux_sa_family_t)-sizeof(struct in_addr)];
};

# endif /*__ffly__linux__in__h*/
