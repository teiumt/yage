# ifndef __ffly__linux__mkdev__h
# define __ffly__linux__mkdev__h
# include "types.h"
_32_s mknod(char const*, __linux_mode_t, unsigned int);
# endif /*__ffly__linux__mkdev__h*/
