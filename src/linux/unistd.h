# ifndef __ffly__linux__unistd__h
# define __ffly__linux__unistd__h
# include "../y_int.h"
# include "types.h"

#define SEEK_SET 0
#define SEEK_CUR 1
#define SEEK_END 2

#define R_OK 4
#define W_OK 2
#define X_OK 1
#define F_OK 0
__linux_pid_t getpid(void);
__linux_pid_t getppid(void);

_32_s open(char const*, _64_u, _64_u);
_32_s read(_64_u, void*, _64_u);
_32_s write(_64_u, void*, _64_u);
_32_s pwrite(_64_u, void*, _64_u, _64_u);
_32_s pread(_64_u, void*, _64_u, _64_u);
_32_s access(char const*, _64_u);

_32_s fsync(_64_u);
_32_s lseek(_64_u, _64_u, _64_u);
_32_s exit(_64_s);
_32_s getcwd(char*, _64_u);
void* brk(void*);
_32_s execve(const char*, char *const*, char *const*);
_32_s rmdir(char const*);
_32_s unlink(char const*);
_32_s ftruncate(_64_u, _64_u);
_32_s dup2(_64_u, _64_u);
_32_s pipe(int*);
# endif /*__ffly__linux__unistd__h*/
