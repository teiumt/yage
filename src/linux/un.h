#ifndef __un
#define __un
#include "types.h"
#define UNIX_PATH_MAX   108
struct sockaddr_un {
	__k_sa_family sun_family; /* AF_UNIX */
	char sun_path[UNIX_PATH_MAX];   /* pathname */
};

#endif/*__un*/
