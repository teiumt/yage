# ifndef __ffly__linux__fcntl__h
# define __ffly__linux__fcntl__h
# include "../y_int.h"
#define O_ACCMODE   00000003
#define O_RDONLY    00000000
#define O_WRONLY    00000001
#define O_RDWR      00000002
#define O_CREAT     00000100    /* not fcntl */
#define O_EXCL      00000200    /* not fcntl */
#define O_NOCTTY    00000400    /* not fcntl */
#define O_TRUNC     00001000    /* not fcntl */
#define O_APPEND    00002000
#define O_NONBLOCK  00004000
#define O_DSYNC     00010000    /* used to be O_SYNC, see below */
#define FASYNC      00020000    /* fcntl, for BSD compatibility */
#define O_DIRECT    00040000    /* direct disk access hint */
#define O_LARGEFILE 00100000
#define O_DIRECTORY 00200000    /* must be a directory */
#define O_NOFOLLOW  00400000    /* don't follow links */
#define O_NOATIME   01000000
#define O_CLOEXEC   02000000    /* set close_on_exec */
#define F_DUPFD     0   /* dup */
#define F_GETFD     1   /* get close_on_exec */
#define F_SETFD     2   /* set/clear close_on_exec */
#define F_GETFL     3   /* get file->f_flags */
#define F_SETFL     4   /* set file->f_flags */
//types need fixing, for now im too lazy ie _64_u* should be off_t
_32_s splice(_64_u,_64_u*,_64_u,_64_u*,_64_u,_64_u);
_32_s tee(_64_u,_64_u,_64_u,_64_u);
_32_s fcntl(_64_u, _64_u, _64_u);
_32_s close(_64_u);
_32_s creat(char const*, _64_u);
# endif /*__ffly__linux__fcntl__h*/
