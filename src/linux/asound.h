# ifndef __ffly__linux__asound__h
# define __ffly__linux__asound__h
# include "../y_int.h"
# include "ioctl.h"
#define SNDRV_PCM_IOCTL_PVERSION    _IOR('A', 0x00, int)
#define SNDRV_PCM_IOCTL_INFO        _IOR('A', 0x01, struct snd_pcm_info)
#define SNDRV_PCM_IOCTL_TSTAMP      _IOW('A', 0x02, int)
#define SNDRV_PCM_IOCTL_TTSTAMP     _IOW('A', 0x03, int)
#define SNDRV_PCM_IOCTL_USER_PVERSION   _IOW('A', 0x04, int)
#define SNDRV_PCM_IOCTL_HW_REFINE   _IOWR('A', 0x10, struct snd_pcm_hw_params)
#define SNDRV_PCM_IOCTL_HW_PARAMS   _IOWR('A', 0x11, struct snd_pcm_hw_params)
#define SNDRV_PCM_IOCTL_HW_FREE     _IO('A', 0x12)
#define SNDRV_PCM_IOCTL_SW_PARAMS   _IOWR('A', 0x13, struct snd_pcm_sw_params)
#define SNDRV_PCM_IOCTL_STATUS      _IOR('A', 0x20, struct snd_pcm_status)
#define SNDRV_PCM_IOCTL_DELAY       _IOR('A', 0x21, snd_pcm_sframes_t)
#define SNDRV_PCM_IOCTL_HWSYNC      _IO('A', 0x22)
#define SNDRV_PCM_IOCTL_SYNC_PTR    _IOWR('A', 0x23, struct snd_pcm_sync_ptr)
#define SNDRV_PCM_IOCTL_STATUS_EXT  _IOWR('A', 0x24, struct snd_pcm_status)
#define SNDRV_PCM_IOCTL_CHANNEL_INFO    _IOR('A', 0x32, struct snd_pcm_channel_info)
#define SNDRV_PCM_IOCTL_PREPARE     _IO('A', 0x40)
#define SNDRV_PCM_IOCTL_RESET       _IO('A', 0x41)
#define SNDRV_PCM_IOCTL_START       _IO('A', 0x42)
#define SNDRV_PCM_IOCTL_DROP        _IO('A', 0x43)
#define SNDRV_PCM_IOCTL_DRAIN       _IO('A', 0x44)
#define SNDRV_PCM_IOCTL_PAUSE       _IOW('A', 0x45, int)
#define SNDRV_PCM_IOCTL_REWIND      _IOW('A', 0x46, snd_pcm_uframes_t)
#define SNDRV_PCM_IOCTL_RESUME      _IO('A', 0x47)
#define SNDRV_PCM_IOCTL_XRUN        _IO('A', 0x48)
#define SNDRV_PCM_IOCTL_FORWARD     _IOW('A', 0x49, snd_pcm_uframes_t)
#define SNDRV_PCM_IOCTL_WRITEI_FRAMES   _IOW('A', 0x50, struct snd_xferi)
#define SNDRV_PCM_IOCTL_READI_FRAMES    _IOR('A', 0x51, struct snd_xferi)
#define SNDRV_PCM_IOCTL_WRITEN_FRAMES   _IOW('A', 0x52, struct snd_xfern)
#define SNDRV_PCM_IOCTL_READN_FRAMES    _IOR('A', 0x53, struct snd_xfern)
#define SNDRV_PCM_IOCTL_LINK        _IOW('A', 0x60, int)
#define SNDRV_PCM_IOCTL_UNLINK      _IO('A', 0x61)

typedef unsigned long snd_pcm_uframes_t;
typedef signed long snd_pcm_sframes_t;

struct snd_xferi {
	snd_pcm_sframes_t result;
	void *buf;
	snd_pcm_uframes_t frames;
};
# endif /*__ffly__linux__asound__h*/
