# ifndef __ffly__linux__uio__h
# define __ffly__linux__uio__h
# include "../y_int.h"
# include "types.h"
struct iovec {
	void *iov_base;
	__linux_size_t iovlen;
};

_32_s writev(_32_u, struct iovec*, _32_u);
_32_s readv(_32_u, struct iovec*, _32_u);
# endif /*__ffly__linux__uio__h*/
