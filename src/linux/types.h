# ifndef __ffly__linux__types__h
# define __ffly__linux__types__h
# include "../y_int.h"
/*
	TODO:
		remove __linux and just use __size_t or __ksize_t
*/

typedef long int __k_slong;
typedef long unsigned __k_ulong;
typedef long int __k_long;

typedef _32_u __k_dev_t;
// remove linux so __pid_t
typedef _32_s __linux_pid_t;
typedef __k_slong __linux_off_t;
typedef long long __linux_loff_t;

typedef _32_s __linux_key_t;
typedef _32_s __linux_mqd_t;

typedef _32_u __linux_uid32_t;
typedef _32_u __linux_uid_t;
typedef _32_u __linux_gid_t;
typedef _32_u __linux_mode_t;
typedef __k_slong __linux_time_t;
typedef _32_s __linux_timer_t;

typedef _32_s __linux_ipc_pid_t;
typedef __k_ulong __linux_size_t;
typedef __k_slong __linux_ssize_t;
typedef __k_ulong __k_size_t;

typedef __k_ulong __linux_suseconds_t;
typedef _16_u __linux_sa_family_t;
typedef _32_s __linux_clockid_t;
typedef	__k_slong __linux_clock_t;
typedef _16_u __linux_umode_t;
typedef unsigned short __k_sa_family;
# endif /*__ffly__linux__type__h*/
