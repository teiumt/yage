#ifndef __futex__h
#define __futex__h
#include "time.h"
#define FUTEX_WAIT      0
#define FUTEX_WAKE      1

long futex(_32_u*,int,_32_u,struct timespec*,_32_u*,_32_u);
#endif/*__futex__h*/
