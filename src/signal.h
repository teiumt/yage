# ifndef __ffly__signal__h_
# define __ffly__signal__h_
# include "linux/signal.h"
# include "y_int.h"
void sigemptyset(sigset_t*);
_32_s sigaction(_32_s, struct sigaction const*, struct sigaction*);
# endif /*__ffly__signal__h_*/
