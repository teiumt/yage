# include "pool.h"
# include "../memory/mem_alloc.h"
# include "../memory/mem_free.h"
# include "../memory/mem_realloc.h"
# include "io.h"
# include "errno.h"
# include "mutex.h"
# include "err.h"
# include "../oddity.h"
/*
	move to memory/pool.h
*/

#ifndef FF_POOL_SA
#define mem_alloc(__n)\
	__f_mem_alloc(__n)	
#define mem_free(__p)\
	__f_mem_free(__p)
#define mem_realloc(__p, __n)\
	__f_mem_realloc(__p, __n)
#else
#define mem_alloc(__n)\
	ffly_ad_alloc(__FSPS->ad, __FSPS->ma_arg, __n)
#define mem_free(__p)\
	ffly_ad_free(__FSPS->ad, __FSPS->ma_arg, __p)
#define mem_realloc(__p, __n)\
	ffly_ad_realloc(__FSPS->ad, __FSPS->ma_arg, __p, __n)
#endif
_f_err_t ffly_pool_init(struct ffly_pool *__pool, _f_size_t __size, _f_size_t __slice_size) {
	if ((__pool->p = mem_alloc(__size*__slice_size)) == NULL) {
		ffly_fprintf(ffly_err, "pool: failed to allocate memory.\n");
		caught_oddity;
		return FFLY_FAILURE;
	}
	__pool->lock = FFLY_MUTEX_INIT;
	__pool->off = 0;
	__pool->size = __size;
	__pool->slice_size = __slice_size;
	__pool->uu_slices = NULL;
	__pool->uu_slice_c = 0;
	return FFLY_SUCCESS;
}

_f_err_t ffly_pool_de_init(struct ffly_pool *__pool) {
	mem_free(__pool->p);
	mem_free(__pool->uu_slices);
	/*
	_f_err_t err;
	if (_err(err = mem_free(__pool->p))) {
		ffly_fprintf(ffly_err, "pool: failed to free.\n");
		return err;
	}

	if (__pool->uu_slices != NULL) {
		if (_err(err = mem_free(__pool->uu_slices))) {
			ffly_fprintf(ffly_err, "pool: failed to free.");
			return err;
		}
	}
	*/
	return FFLY_SUCCESS;
}

_f_bool_t ffly_pool_full(struct ffly_pool *__pool) {
	return ((__pool->off == __pool->size-1)&&!__pool->uu_slice_c);
}

void* ffly_pool_get(struct ffly_pool *__pool, _int_u __off) {
	return ((_8_u*)__pool->p+(__off*__pool->slice_size));
}

void* ffly_pool_alloc(struct ffly_pool *__pool) {
	if (!__pool->uu_slice_c && __pool->off == __pool->size-1) return NULL;
	void *ret;
	mt_lock(&__pool->lock);
	if (__pool->uu_slice_c > 0) {
		ret = *(void**)((_8_u*)__pool->uu_slices+((__pool->uu_slice_c-1)*sizeof(void*)));
		if (!(__pool->uu_slice_c-1)) {
		/*
			if (_err(mem_free(__pool->uu_slices))) {
				ffly_fprintf(ffly_err, "pool: failed to free.\n");
				goto _fatal;
			}
		*/
			mem_free(__pool->uu_slices);
			__pool->uu_slices = NULL;
			__pool->uu_slice_c--;
		} else {
			if ((__pool->uu_slices = (void*)mem_realloc(__pool->uu_slices, ((--__pool->uu_slice_c)*sizeof(void*)))) == NULL) {
				ffly_fprintf(ffly_err, "pool: failed to reallocate memory.\n");
				goto _fatal;
			}
		}
		mt_unlock(&__pool->lock);
		return ret;
	}
	ret = (void*)((_8_u*)__pool->p+((__pool->off++)*__pool->slice_size));
	mt_unlock(&__pool->lock);
	return ret;
_fatal:
	mt_unlock(&__pool->lock);
	return NULL;
}

_f_err_t ffly_pool_free(struct ffly_pool *__pool, void *__p) {
	_f_err_t err = FFLY_SUCCESS;
	mt_lock(&__pool->lock);
	if (!__pool->uu_slice_c) {
		if ((__pool->uu_slices = (void*)mem_alloc(sizeof(void*))) == NULL) {
			ffly_fprintf(ffly_err, "pool: failed to allocate memory.\n");
			err = FFLY_FAILURE;
			goto _end;
		}
		__pool->uu_slice_c++;
	} else {
		if ((__pool->uu_slices = (void*)mem_realloc(__pool->uu_slices, (++__pool->uu_slice_c)*sizeof(void*))) == NULL) {
			ffly_fprintf(ffly_err, "pool: failed to reallocate memory.\n");
			err = FFLY_FAILURE;
			goto _end;
		}
	}
	*(void**)((_8_u*)__pool->uu_slices+((__pool->uu_slice_c-1)*sizeof(void*))) = __p;
_end:
	mt_unlock(&__pool->lock);
	return err;
}
