# ifndef __ffly__file__h
# define __ffly__file__h
# include "../y_int.h"
# include "../types.h"
# include "../fsus.h"
# include "stat.h"
# include "../rws.h"
# ifdef __fflib
# include "../linux/stat.h"
# include "../linux/fcntl.h"
# include "../linux/unistd.h"
# else
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# endif
# include "../mutex.h"
#define f_fclose ffly_fclose
/*
	TODO:
		work more on this, write buffer, etc.
		allow for flags to be passed to write to bypass buffer and write
		directly to file, if error happens be wont be able to print
		as its going to get stuck in a loop


	NOTE:
		USER-FILES?

	here we allow for redirect of streams
*/
#define ffly_fileno(__file) \
    (__file)->fn->fd
#define OBUFSZ 76
struct ffly_file {
	char const *path;
	f_fsus_nodep fn;
# ifndef __fflib
	FILE *libc_fp;
# endif
	_8_u flags;
	_32_u bufdst;
	_8_i drain;
	_32_u off;
	_8_u obuf[OBUFSZ];
	_8_u *ob_p;
	struct f_rw_struc *rws;
	mlock lock;
};

/*
	struct for doing stuff by n

	n open
	n close
	nwrite?? ????? 

	rename
*/
struct f_file_struc {
	union {
		struct ffly_file *f_in;
		struct {
			struct ffly_file **f;
			char const *path;
			int flags;
			_32_u mode;
		};
	};
	_f_err_t error;	
};

#define f_fopen ffly_fopen
#define FF_STREAM 0x01
#define FF_NOBUFF 0x02
#define FF_O_TRUNC O_TRUNC
#define FF_O_RDONLY O_RDONLY
#define FF_O_WRONLY O_WRONLY
#define FF_O_RDWR O_RDWR
#define FF_O_CREAT O_CREAT
#define FF_S_IRWXU S_IRWXU // r/w/x
#define FF_S_IRUSR S_IRUSR // r
#define FF_S_IWUSR S_IWUSR // w
#define FF_S_IXUSR S_IXUSR // e
#define FF_SEEK_SET SEEK_SET
#define FF_SEEK_CUR SEEK_CUR
#define FF_FILE struct ffly_file
#define F_FILE struct ffly_file
#define Y_FILE struct ffly_file
# ifdef __cplusplus
extern "C" {
# endif
void f_fxclose(struct f_file_struc*, _int_u);
void f_fsopen(struct f_file_struc*, _int_u);
struct ffly_file* ffly_fopen(char const*, int, _32_u, _f_err_t*);
void ffly_fopt(struct ffly_file*, _8_u);
_f_err_t ffly_fcreat(char const*, _32_u);
_f_err_t ffly_fstat(char const*, struct ffly_stat*);
_f_off_t ffly_fseek(struct ffly_file*, _f_off_t, int);


#define ffly_fwrite(...) f_fwrite(__VA_ARGS__, NULL)
#define ffly_fread(...) f_fread(__VA_ARGS__, NULL)
_f_err_t f_fwrite(struct ffly_file*, void*, _int_u, _32_u*);
_f_err_t f_fread(struct ffly_file*, void*, _int_u, _32_u*);

_f_err_t ffly_fclose(struct ffly_file*);
_f_err_t ffly_fdrain(struct ffly_file*);
// solid write
_f_err_t ffly_fwrites(struct ffly_file*, void*, _int_u);
_f_err_t ffly_fpread(struct ffly_file*, void*, _int_u, _int_u);
_f_err_t ffly_fpwrite(struct ffly_file*, void*, _int_u, _int_u);
# ifdef __cplusplus
}
# endif
# endif /*__ffly__file__h*/
