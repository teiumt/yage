# include "clock.h"
# include "../linux/time.h"
# include "../tmu.h"

/*
	subclicks to clicks
	8 clicks = 1 second

*/
void y_clock_gettime(struct f_timespec *__spec) {
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);

	double long nsec;
	nsec = ts.tv_nsec;

	double long sclks;
	/*
		dont like this but we need nsec to fill the void
	*/
	sclks = nsec*(((double long)((_32_u)~0))*.000000001);
	_64_u _sclks = (_64_u)sclks;
	/*
		nsec should never exceed 1 second
		
		higher bits
		00000000	00000000	00000000	00000000	00000000	00000000	00000000	00000000
																								 |||
																								 ||\_
																								 |\__ interlevate part 0-7 after this part so 8 onwards will count
																								 \___ as one second 1 here is 1/8 of a second so 0.125	
	*/
	__spec->higher = (ts.tv_sec<<3)|(_sclks>>29);
	/*
		the THREE bits at the end are nether used to cut them out
	*/
	__spec->lower = _sclks&~(_64_u)((_64_u)7<<29);
}
