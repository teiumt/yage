# ifndef __ffly__task__h
# define __ffly__task__h
# include "../y_int.h"
typedef struct ffly_task {
	_8_i(*func)(void*);
	void *arg_p;
	struct ffly_task *prev, *next;
} *ffly_taskp;

# endif /*__ffly__task__h*/
