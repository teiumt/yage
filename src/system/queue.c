# include "queue.h"
# include "io.h"
# include "err.h"
# include "errno.h"
# include "../m_alloc.h"
# include"../string.h"
# include "../msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSQUEUE)
/*
	TODO:
		queue size should depend on (0xffffffffffffffff>>(64-{QUEUE SIZE}))
		so 

		QUEUE_SIZE&MASK


		this will loop back to zero
*/

typedef struct queue_priv {
	void **p;
	_int_u page_c;
	_f_size_t blk_size;
	_f_off_t top, end;
} *queue_privp;

/*
	TODO:
		major cleaning and improv
*/
_f_err_t static
queue_init(queue_privp __queue, _int_u __blk_size) {
	__queue->page_c = 1;
/*
	preallocate data
*/
	if ((__queue->p = (void**)m_alloc(sizeof(void*))) == NULL) {
		MSG(CRIT, "failed to alloc memory.\n")
		return FFLY_FAILURE;
	}

	__queue->blk_size = __blk_size;
	if ((*__queue->p = (void*)m_alloc(QUEUE_PAGE_SIZE*__queue->blk_size)) == NULL) {
		MSG(CRIT, "failed to init page.\n")
		return FFLY_FAILURE;
	}
	__queue->top = 0;
	__queue->end = 0;
	return FFLY_SUCCESS;
}

_f_err_t static
queue_de_init(queue_privp __queue) {
	if (__queue->p != NULL) {
		void **itr = __queue->p;
		void **end = itr+__queue->page_c;
		void *page;
		/*
			free all pages if page exists
		*/
		while(itr != end) {
			if ((page = *itr) != NULL) {
				MSG(INFO, "page-%u is going to be freed.\n", itr-__queue->p)
				m_free(page);
			}
			itr++;
		}
		m_free(__queue->p);
	}
	return FFLY_SUCCESS;
}

_f_size_t static
queue_size(queue_privp __queue) {
	_f_size_t size = 0;
	if (__queue->end<__queue->top)
		// dont know if this works but (TODO: TEST)
		size = __queue->end+((__queue->page_c*QUEUE_PAGE_SIZE)-__queue->top);
	else if (__queue->end>__queue->top)
		size = __queue->end-__queue->top;
	return size;
}

_f_err_t static
queue_push(queue_privp __queue, void *__p) {
	// are we overlapping 
	if (__queue->end+1 == __queue->top) {
		MSG(WARN, "overflow.\n")
		return FFLY_NOP;
	}

	_int_u page;
	void **pg;
	if (!__queue->p) {
		page = 0;
		__queue->page_c++;
		if ((__queue->p = (void**)m_alloc(sizeof(void*))) == NULL) {
			MSG(CRIT, "failed to alloc memory.\n")
			return FFLY_FAILURE;
		}
		goto _init_page;
 	}

	if ((page = (__queue->end>>QUEUE_PAGE_SHIFT)) == __queue->page_c) {
		if ((__queue->p = (void**)m_realloc(__queue->p, (++__queue->page_c)*sizeof(void*))) == NULL) {
			MSG(CRIT, "failed to realloc memory.\n")
			return FFLY_FAILURE;
		}
		goto _init_page;
	}

	goto _sk_init_page;
_init_page:
	if ((*(__queue->p+page) = (void*)m_alloc(QUEUE_PAGE_SIZE*__queue->blk_size)) == NULL) {
		MSG(CRIT, "failed to init page.\n")
		return FFLY_FAILURE;
	}

_sk_init_page:
	if (__queue->end == QUEUE_MAX_PAGE_C*QUEUE_PAGE_SIZE) {
		if (!__queue->top) {
			MSG(WARN, "no more space waiting for pop to take place to reset the push head back to zero.\n");
			return FFLY_NOP;
		}
		__queue->end = 0;
	}

	pg = __queue->p+page;
/*
	page might be freed to save memory if page is not used or contains nothing
*/
	if (!*pg) {
		if ((*pg = (void*)m_alloc(QUEUE_PAGE_SIZE*__queue->blk_size)) == NULL) {
			MSG(CRIT, "failed to alloc memory.\n")
			return FFLY_FAILURE;
		}
	}

	mem_cpy((void*)(((_8_u*)*pg)+((__queue->end++)-(page*QUEUE_PAGE_SIZE))*__queue->blk_size), __p, __queue->blk_size);
	return FFLY_SUCCESS;
}


_f_err_t static
queue_pop(queue_privp __queue, void *__p) {
	if (__queue->top == __queue->end) {
		MSG(WARN, "underflow.\n")
		return FFLY_FAILURE;
	}

	if (__queue->top == QUEUE_MAX_PAGE_C*QUEUE_PAGE_SIZE) {	
		__queue->top = 0;
	}

	_int_u page = __queue->top>>QUEUE_PAGE_SHIFT;

	void **pg;
	pg = __queue->p+page;
	mem_cpy(__p, (void*)(((_8_u*)*pg)+((__queue->top++)-(page*QUEUE_PAGE_SIZE))*__queue->blk_size), __queue->blk_size);

	/*
		if we are at the edge of the page ^up here and +1 now we are in the next page free previous one as its unneed
	*/
	if (__queue->top>>QUEUE_PAGE_SHIFT>page) {
		m_free(*pg);
		*pg = NULL;
	}
	return FFLY_SUCCESS;
}

void static*
queue_front(queue_privp __queue) {
	_f_off_t top = __queue->top;
	return (void*)((_8_u*)(*(__queue->p+(top>>QUEUE_PAGE_SHIFT)))+((top-((top>>QUEUE_PAGE_SHIFT)*QUEUE_PAGE_SIZE))*__queue->blk_size));}
void static*
queue_back(queue_privp __queue) {
	_f_off_t end = __queue->end;
	return (void*)((_8_u*)(*(__queue->p+(end>>QUEUE_PAGE_SHIFT)))+(((end-((end>>QUEUE_PAGE_SHIFT)*QUEUE_PAGE_SIZE))-1)*__queue->blk_size));}

void ffly_queue_loadfuncs(ffly_queuep __q) {
	__q->init		= (_f_err_t(*)(long long, _int_u))queue_init;
	__q->de_init	= (_f_err_t(*)(long long))queue_de_init;
	__q->size		= (_f_size_t(*)(long long))queue_size;
	__q->push		= (_f_err_t(*)(long long, void*))queue_push;
	__q->pop		= (_f_err_t(*)(long long, void*))queue_pop;
	__q->front		= (void*(*)(long long))queue_front;
	__q->back		= (void*(*)(long long))queue_back;
	struct queue_priv *pv;
	pv = m_alloc(sizeof(struct queue_priv));


	__q->ctx = (long long)pv;
}
