# ifndef __ffly__port__h
# define __ffly__port__h
# include "../y_int.h"
# include "../types.h"

# define FF_PORT_CREAT 0x1
# define FF_PORT_SHMM 0x2
/*
	rename,
	allow for multiple to chat of diffrent bands using one piece of memory
*/
_int_u ffly_port_get_shmid(_int_u);
_f_err_t ffly_port_send(_32_u, void*, _int_u, _int_u);
_f_err_t ffly_port_recv(_32_u, void*, _int_u, _int_u);
_int_u ffly_port(_int_u, _8_u, _int_u, _f_err_t*);

_8_i ffly_port_band(_int_u, _32_u);
void ffly_port_close(_int_u);
# endif /*__ffly__port__h*/
