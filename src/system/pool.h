# ifndef __ffly__pool__h
# define __ffly__pool__h
# include "../y_int.h"
# include "../types.h"

//#define FF_POOL_SA

/*
	move to /memory as it has nothing to do with /system
*/
struct ffly_pool {
#ifdef FF_POOL_SA
	_ulonglong ma_arg;
	_16_u ad;
#endif
	void *p, *uu_slices;
	_f_size_t size, slice_size;
	_f_off_t off;
	_int_u uu_slice_c;
	mlock lock;
};

# ifdef __cplusplus
extern "C" {
# endif
_f_err_t ffly_pool_init(struct ffly_pool*, _f_size_t, _f_size_t);
_f_err_t ffly_pool_de_init(struct ffly_pool*);
void* ffly_pool_get(struct ffly_pool*, _int_u);
void* ffly_pool_alloc(struct ffly_pool*);
_f_err_t ffly_pool_free(struct ffly_pool*, void*);
_f_bool_t ffly_pool_full(struct ffly_pool*);
# ifdef __cplusplus
}
# endif
# endif /*__ffly__pool__h*/
