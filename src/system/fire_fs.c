# include "fs.h"
# include "../fs.h"
/*
	a DB file system, make is easer to example read write from data base using filesystem like operations,
	easy storage that doesent require kernel interaction only using file ops and such
*/
_8_s static
_open(f_fsus_nodep __n, char const *__path, _32_u __flags, _32_u __mode) {
//	__n->n = f_vmfs_open(__path, __flags, __mode);
	return 0;
}

_8_s static
_close(f_fsus_nodep __n) {
//	f_vmfs_close(__n->n);
	return 0;
}

_64_u static
_pwrite(f_fsus_nodep __n, void *__buf, _64_u __size, _64_u __offset, _8_s *__error) {
//	fs_pwrite(__n->n, __buf, __size, __offset);
}

_64_u static
_pread(f_fsus_nodep __n, void *__buf, _64_u __size, _64_u __offset, _8_s *__error) {
//	fs_pread(__n->n, __buf, __size, __offset);
}

void static
_mkdir(char const *__path, _8_s *__error) {
//	fs_mkdir(__path);
}

static struct ffly_fsop _op = {
	.open = _open,
	.close = _close,
	.pread = _pread,
	.pwrite = _pwrite,
	.mkdir = _mkdir
};

void fire_fsop(struct ffly_fsop *__op) {
	*__op = _op;
}
