# ifndef __ffly__system__fs__h
# define __ffly__system__fs__h
# include "dir.h"
# include "file.h"
# include "../fsus.h"
#define FF_POSIX_FS 0
#define FF_FIRE_FS 1
/*
	NOTE:
		not going with platform stuff as i want to be able to change file system
		to somthing more local if need to be 
*/
#ifndef __struconly
# include "../cradle.h"
#endif
# include "fs_struc.h"
#define sfs(__func, ...)\
	F_CRADLE(sys.fs)->__func(__VA_ARGS__)
#define fsopen\
	f_fs_open
#define fsclose(...)\
	f_fs_close
#define fslseek(__n, ...)\
	(__n)->ops->lseek(__n, __VA_ARGS__)
#define fsread(__n, ...)\
	(__n)->ops->read(__n, __VA_ARGS__)
#define fswrite(__n, ...)\
	(__n)->ops->write(__n, __VA_ARGS__)
#define fspread(__n, ...)\
	(__n)->ops->pread(__n, __VA_ARGS__)
#define fspwrite(__n, ...)\
	(__n)->ops->pwrite(__n, __VA_ARGS__)
#define fsaccess(...)\
	sfs(access, __VA_ARGS__)
#define fsmkdir(...)\
	sfs(mkdir, __VA_ARGS__)
#define fscreat(...)\
	sfs(creat, __VA_ARGS__)
f_fsus_nodep f_fs_open(char const*, _32_u, _32_u);
_8_s f_fs_close(f_fsus_nodep);


void ffly_sysfs(_8_u);
# endif /*__ffly__system__fs__h*/
