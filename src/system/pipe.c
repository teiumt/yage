# include "pipe.h"
# include "shm.h"
# include "../string.h"
#ifdef __fflib
# include "../linux/ipc.h"
# include "../linux/stat.h"
#else
# include <sys/ipc.h>
# include <sys/stat.h>
#endif
#ifndef __ffly_module
# include "io.h"
#else
# include "../mod/io.h"
#endif
# include "../config.h"
# include "err.h"
# include "mutex.h"
# include "nanosleep.h"
# include "../msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSPIPE)
#define delay ffly_nanosleep(0, 100000);
// needs testing
/*
	NOTE: cleanup
	TODO:
		add timeout
		add delay in loops to reduce cpu load

	put debug define over debug statment
	
	NOTE:
		i was not going to use shm at first but decided it might be faster then real pipes,
		i might be wrong but i dont like using system calls i find that there the big pitfull when is comes to stuff like this 
		that requires alot of calls and would cause too much core jumping and will result in lag lots of it.

		yes i know nanosleep is a syscall
*/

typedef struct hdr {
	_16_u size;
} *hdrp;

#define STRT	0x01
#define STOP	0x02
#define DUMP	0x04
#define OK		0x08
struct pipe {
	_int_u shm_id;
	_8_u *bits;
	hdrp h;
	void *buf, *p;
	_int_u width;
	_8_u flags;
	_8_i to_shut;
};

#define is_flag(__flags, __flag) \
	((__flags&__flag) == __flag)
#define clr_bit(__bits, __bit) \
	(*__bits) ^= *__bits&__bit;
#define set_bit(__bits, __bit)\
	(*__bits) |= __bit;
#define is_bit(__bits, __bit) \
	((*__bits&__bit) == __bit)
#define get_pipe(__id) (pipes+__id)
#define MAX 5
struct pipe static pipes[MAX];
struct pipe static *next = pipes;
struct pipe static *dead[MAX];
struct pipe static **reuse = dead;

mlock static lock = FFLY_MUTEX_INIT;
_int_u ffly_pipe_get_shmid(_int_u __id) {
	return get_pipe(__id)->shm_id;
}

_f_err_t
ffly_pipe_wrl(_64_u __val, _8_u __l, _int_u __pipe) {
	return ffly_pipe_write(&__val, __l, __pipe);
}

_64_u
ffly_pipe_rdl(_8_u __l, _int_u __pipe, _f_err_t *__err) {
	_64_u ret = 0;
	*__err = ffly_pipe_read(&ret, __l, __pipe);
	return ret;
}

void ffly_pipe_shutoff(_int_u __id) {
	struct pipe *pi = get_pipe(__id);
	pi->to_shut = 0;
}

void static
discard(struct pipe *__pi) {
	mt_lock(&lock);
	if (__pi == next-1)
		next--;
	else
		*(reuse++) = __pi;
	mt_unlock(&lock);
}

_int_u
ffly_pipe(_int_u __width, _8_u __flags,
	_int_u __id, _f_err_t *__err)
{
	_int_u id;
	mt_lock(&lock);
	if (reuse>dead)
		id = *(--reuse)-pipes;
	else
		id = (next++)-pipes;
	mt_unlock(&lock);
	struct pipe *pi = pipes+id;
	pi->flags = __flags;
	pi->to_shut = -1;
	int flags = 0;
	if (is_flag(__flags, FF_PIPE_CREAT))
		flags |= IPC_CREAT;
	if (is_flag(__flags, FF_PIPE_SHMM))
		pi->shm_id = __id;

	pi->p = ffly_shmget(&pi->shm_id, __width+sizeof(struct hdr)+1, 
		flags|S_IRWXU|S_IRWXO|S_IRWXG, is_flag(__flags, FF_PIPE_SHMM)?FF_SHM_MCI:0);
	if (!pi->p) {
		ffly_fprintf(ffly_out, "pipe failure.\n");
		discard(pi);
		*__err = FFLY_FAILURE;
		return 0;
	}
	_8_u *p = (_8_u*)pi->p;
	pi->bits = p++;
	if (is_flag(__flags, FF_PIPE_CREAT))
		*pi->bits = 0x0;
	pi->h = (hdrp)p;
	p+=sizeof(struct hdr);
	pi->buf = (void*)p;
	pi->width = __width;
	*__err = FFLY_SUCCESS;
	return id;
}

# include "nanosleep.h"
_f_err_t
ffly_pipe_listen(_int_u __id) {
	struct pipe *pi = get_pipe(__id);
	_8_i *to_shut = &pi->to_shut;
	while(!is_bit(pi->bits, OK)) {
		if (!*to_shut)
			reterr;
	}
	clr_bit(pi->bits, OK);
	retok;
}

_f_err_t
ffly_pipe_connect(_int_u __id) {
	struct pipe *pi = get_pipe(__id);
	_8_i *to_shut = &pi->to_shut;
	set_bit(pi->bits, OK); 
	while(is_bit(pi->bits, OK)) {
		if (!*to_shut)
			reterr;
	}
	retok;
}

_f_err_t
ffly_pipe_write(void *__buf, _int_u __size, _int_u __id) {
	struct pipe *pi = get_pipe(__id);
	_8_i *to_shut = &pi->to_shut;
	FF_DUG(0, "write %u.\n", *pi->bits)

	set_bit(pi->bits, STRT);
	while(is_bit(pi->bits, STRT)) {
		if (!*to_shut)
			reterr;
		delay
	}

	_8_u *p = (_8_u*)__buf;
	_8_u *end = p+__size;
	while(p < end) {
		_int_u left = __size-(p-(_8_u*)__buf);
		_int_u size = left>pi->width?pi->width:left;
		bcopy(pi->buf, p, size);
		p+=size;
		pi->h->size = size;

		FF_DUG(0, "size: %u\n", pi->h->size)
		set_bit(pi->bits, DUMP);
		FF_DUG(0, "waiting for peer.\n")

		while(is_bit(pi->bits, DUMP)) {
			if (!*to_shut)
				reterr;
		}
		FF_DUG(0, "okay.\n")
	}

	set_bit(pi->bits, STOP);
	while(!is_bit(pi->bits, OK)) {
		if (!*to_shut)
			reterr;
		delay
	}
	clr_bit(pi->bits, OK);
	
	retok;
}

_f_err_t
ffly_pipe_read(void *__buf, _int_u __size, _int_u __id) {
	struct pipe *pi = get_pipe(__id);
	_8_i *to_shut = &pi->to_shut;
	while(is_bit(pi->bits, OK)) {
		if (!*to_shut)
			reterr;
		delay
	}
	FF_DUG(0, "read. %u\n", *pi->bits)
	while(!is_bit(pi->bits, STRT));
	FF_DUG(0, "got start bit.\n")
	clr_bit(pi->bits, STRT);

	_8_u *p = (_8_u*)__buf;
	_8_u *end = p+__size;
	while(!is_bit(pi->bits, STOP)) {
		if (!*to_shut)
			reterr;
		if (is_bit(pi->bits, DUMP) && p < end) {
			_int_u size = pi->h->size;
			FF_DUG(0, "size: %u\n", size)
			bcopy(p, pi->buf, size);  
			p+=size;
			clr_bit(pi->bits, DUMP); 
		}
	}

	FF_DUG(0, "got end bit.\n")
	clr_bit(pi->bits, STOP);
	set_bit(pi->bits, OK);
	while(is_bit(pi->bits, OK)) {
		if (!*to_shut)
			reterr;
		delay
	}
	retok;
}

void ffly_pipe_close(_int_u __id) {
	struct pipe *pi = get_pipe(__id);
	ffly_shmdt(pi->shm_id);
	if (is_flag(pi->flags, FF_PIPE_CREAT)) 
		ffly_shm_free(pi->shm_id);
	ffly_shm_cleanup(pi->shm_id);
	printf("id: %u\n", pi->shm_id);
	discard(pi);
}
