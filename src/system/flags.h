# ifndef __ffly__flags__h
# define __ffly__flags__h
# include "../y_int.h"
# include "../types.h"
#define TW_FLG_ACTIVE 0x01
#define f_is_flag ffly_is_flag
#define f_add_flag ffly_add_flag
#define f_rm_flag ffly_rm_flag
#define ffly_is_flag(__flags, __flag) \
	(((__flags)&(__flag))==(__flag))
#define ffly_add_flag(__flags, __flag) \
	__flags |= __flag
#define ffly_rm_flag(__flags, __flag) \
	__flags ^= (__flags)&(__flag)
# endif /*__ffly__flags__h*/
