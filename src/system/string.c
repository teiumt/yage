# include "string.h"
# include "../y_int.h"
# include "../ffly_def.h"
#include "../proc.h"
/*
			README:
			
			copying memory is wastful!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

			the whole thing here is to avoide at any cost

			DONT IGNORE THIS FACT

			char final[....]
			char buf[256];
			float_to_string(buf,1.0)
			copy(final,buf)

			float_to_string(buf,1.0)
			copy(final,buf)

			write_to_IO(final)



			THE ABOVE IS AS BAD AS YOU COULD GET

			BELOW IS HOW IT SHOULD BE!
			
			char final[....]
			float_to_string(final,1.0)
			float_to_string(final,1.0)
			write_to_IO(final)


			this is only good for users of upper levels 
			when dealing with low levels we use src/strf.h
*/
/*
	big improvments to be made at the moment this is not the main focus
*/

/*
	TODO:
		remove number to hex <- replace with pointer form
*/

/*
	NOTE/THINK?
		i wanted to avoid buffers in this, instead went with function pointer to 
		buffer thats emited in 'CUT SIZES' but because of this things might be slower
		and more obstacles to get around.
	TODO:
		replace switch statments
		with array

		allow for static buffer and no use of cut buffer method
		for all routines below
*/
// make inv power of 10 
_64_u static powof10[] = {
	1,
	10,
	100,
	1000,
	10000,
	100000,
	1000000,
	10000000,
	100000000,
	1000000000,
	10000000000,
	100000000000,
	1000000000000,
	10000000000000,
	100000000000000,
	1000000000000000,
	10000000000000000,
	100000000000000000,
	1000000000000000000
};
#define TLS ((struct tls_storage*)proc_ctx_fetch()->str)
struct tls_storage {
	_8_u *buf;
};

char static cn_tbl[] = "0123456789";
# ifndef __fflib
# include <string.h>
static __thread _8_u *buf;
# else
# include "../string.h"
# include "tls.h"
static _int_u buf_tls;
# endif

void 
ffly_string_init(void) {
}

void static
drain(_8_u *__p, _16_u __size, _16_u __cut, _8_u __off) {
# ifndef __fflib
	memcpy(buf+(__cut*CUT_SIZE)+__off, __p, __size);
# else
	_8_u *buf = TLS->buf;
	mem_cpy(buf+(__cut*CUT_SIZE)+__off, __p, __size);
# endif
}

_int_u ffly_nots(_64_u __no, char *__buf) {
# ifndef __fflib
	buf = __buf;
# else
	TLS->buf = __buf;
# endif
	_16_u cut = 0;
	_8_u off = 0;
	return _ffly_nots(__no, &cut, &off, drain);
}

_int_u _ffly_nots(_64_u __no, _16_u *__cut, _8_u *__off, void(*__drain)(_8_u*, _16_u, _16_u, _8_u)) {
	if(__no>1000000000000000000) return 0;
	char buf[CUT_SIZE];
	_16_u cut = *__cut;
	_8_u ct_off = *__off;
	if (ct_off>=CUT_SIZE) {
		cut++;
		ct_off-=CUT_SIZE;
	}

	if (!__no) {
		*buf = '0';
		*(buf+1) = '\0';
		__drain(buf, 2, cut, ct_off++);
		if (ct_off>=CUT_SIZE) {
			cut++;
			ct_off-=CUT_SIZE;
		}
		*__cut = cut;
		*__off = ct_off;
		return 1;
	}

	_int_u l = 0;
	_64_u ret = 0, pl = 1;
	for (;pl <= __no;pl*=10,l++);

	_64_u *pow = powof10+(l-1);
	_64_u g = 0, r;
	char *p = buf;
	char *end = p+CUT_SIZE;
	_int_u i = 0;
	while(i != l) {
		if (p>=end)
			__drain(p = buf, CUT_SIZE, cut++, ct_off);
		if (!(__no-g))
			*p = '0';
		else {
			r = (_64_u)((__no-g)/(*pow));
			*p = cn_tbl[r];
			g+=r**pow;
		}
		p++;
		i++;
		pow--;
	}
	if (p>buf) {
		_int_u left;
		_int_u l = p-buf;
		__drain(buf, l, cut, ct_off);
		if ((left = (0xff-ct_off))<l) {
			l-=l-left;
			cut++;
			ct_off = 0;
		}

		ct_off+=l;
	}

	char c = '\0';
	__drain(&c, 1, cut, ct_off);

	*__cut = cut;
	*__off = ct_off;
	return l;
}

_64_u ffly_sntno(char *__s, _int_u __len) {
	if (__len == 1) {
		return (*__s)-'0';
	}

	_8_u sign;
	if (sign = (*__s == '-')) __s++;

	_64_u no = 0;
	char *p, *e;

	p = __s;
	e = p+__len;
	_int_u i;

	i = __len;
	for(;p != e;p++)
		no+=(*p-'0')*powof10[--i];
	if (sign)
		return -no;
	return no;	
}

_64_u ffly_stno(char *__s) {
	if(*(__s+1) == '\0') {
		return *__s-'0';
	}

	_8_u sign;
	if (sign = (*__s == '-')) __s++;

	_64_u no = 0;
	char *p;
	
	p = __s;
	for(;*p != '\0';p++) {
		no = no*10;
		no+=*p-'0';
	}

	if (sign)
		return -no;
	return no;
}

char ffly_tolow(char __c) {
	if (__c>='a'&&__c<='z') return __c;
	return 'a'+(__c-'A');
}

_8_i ffly_islen(char *__s, _int_u __l) {
	while(*__s != '\0') {
		if (!(__l--)) { 
			if (*(__s+1) == '\0')
				return 1;  
			else
				return -1;	   
		}
		__s++;
	}	 
	return 0;
}

_64_u ffly_htint(char *__s) {
	char *p = __s, c;
	_64_u ret = 0;
	while(*p != '\0') {
		c = *(p++);
		if (c >= 'A' && c <= 'F')
			c = ffly_tolow(c);
		if (c >= '0' && c <= '9')
			ret = (ret<<4)|((c-'0')&0x0f);
		else if (c >= 'a' && c <= 'f')
			ret = (ret<<4)|(((c-'a')+10)&0x0f);
	}
	return ret;
}

//# include <stdio.h>
// needs testing
//float to string with decamal point
_int_u y_ftswdp(double __no, char *__buf) {

}

_int_u ffly_floatts(double __no, char *__buf) {
# ifndef __fflib
	buf = __buf;
# else
	TLS->buf = __buf;
# endif
	_16_u cut = 0;
	_8_u off = 0;
	return _ffly_floatts(__no, &cut, &off, drain);
}

_int_u _ffly_floatts(double __no, _16_u *__cut, _8_u *__off, void(*__drain)(_8_u*, _16_u, _16_u, _8_u)) {
	char buf[CUT_SIZE];
	_16_u cut = *__cut;
	_8_u ct_off = *__off;
	_8_u neg;
	if ((neg = __no<0)) {
		__no = -__no;
		char n = '-';
		__drain(&n, 1, cut, ct_off++);
	}

	if (!__no) {
		*buf = '0';
		*(buf+1) = '\0';
		__drain(buf, 2, cut, ct_off++);
		if (ct_off>=CUT_SIZE) {
			cut++;
			ct_off-=CUT_SIZE;
		}
		*__cut = cut;
		*__off = ct_off;
		return 1;
	}

	_8_u i = 0;
	double v = __no;
	while(i != 7) {
		if (v<1) break; 
		v *= 0.1;
		i++;	   
	}

	// total offset
	_int_u dpp;
	dpp = (cut*CUT_SIZE)+ct_off+i;

	_64_u no, s = 0;
	_int_u l = 0;
	char *p = buf;
	char *end = p+CUT_SIZE;
_bk:
	if (__no*10<1) {
		while(__no*10<1) {
/*
	TODO:
		count x zeros then fill buf with '0' 
		and emit x after count to remove assignment of buf to zero
*/
			if (s>=CUT_SIZE) {
				__drain(buf, s, cut++, ct_off);
				s = 0;
			}
			buf[s++] = '0';			
			__no*=10;
			l++;
		}
		if (s>0){
			__drain(buf, s, cut, ct_off);
			ct_off+=s;
			if (ct_off>=CUT_SIZE) {
				cut++;
				ct_off-=CUT_SIZE;
			}
		}
		s = 0;
		no = (_64_u)__no;
		goto _bk;
	} else
		no = (_64_u)((__no+0.00000005/*fix rounding*/)*10000000.0);

	_64_u *pow = powof10;
	_64_u pl = 1;
	for (;pl <= no;pl*=10,pow++);

	_int_u t = 0;
	while(--pow != powof10) {
		_64_u n = no/(*pow);
	_again:
		if (p>=end)
			__drain(p = buf, CUT_SIZE, cut++, ct_off);

		if (t++ == i) {
			*(p++) = '0';
			l++;
			goto _again;
		}

		// ignore 
		// *p = '0'+n;
		// fix
		*p = cn_tbl[n];
		p++;
		l++;
		no-=n*(*pow);
	}
	if (p>buf) {
		__drain(buf, p-buf, cut, ct_off);
		ct_off+=p-buf;
		if (ct_off>=CUT_SIZE) {
			cut++;
			ct_off-=CUT_SIZE;
		}
	}

	*buf = '\0';
	__drain(buf, 1, cut, ct_off);
	// temp fix
	*buf = '.';
	__drain(buf, 1, dpp>>CUT_SHIFT, dpp&CUT_MASK);
	*__cut = cut;
	*__off = ct_off;
	return l;
}

_int_u ffly_noths(_64_u __no, char *__buf) {
# ifndef __fflib
	buf = __buf;
# else
	TLS->buf = __buf;
# endif
	_16_u cut = 0;
	_8_u off = 0;
	return _ffly_noths(__no, &cut, &off, drain);
}

_int_u _ffly_noths(_64_u __no, _16_u *__cut, _8_u *__off, void(*__drain)(_8_u*, _16_u, _16_u, _8_u)) {
	char buf[CUT_SIZE];
	_16_u cut = *__cut; 
	_8_u ct_off = *__off;


	if (!__no) {
		*buf = '0';
		*(buf+1) = '\0';
		__drain(buf, 2, cut, ct_off);
		ct_off+=2;
		if (ct_off>=CUT_SIZE) {
			(*__cut)++;
			ct_off-=CUT_SIZE;
		}
		*__off = ct_off;
		return 1;
	}

	char *p = buf;
	char *end = p+CUT_SIZE;
	_8_u offset = 0;
	_8_u sk = 1;
	_int_u l = 0;
	while(!((offset++)>>4)) {
		_8_u b = __no>>60&0xf;
		if (b>0 && sk) // skip until somthing other then zero pops up
			sk = 0;
		if (!sk) {
			if (p>=end)
				__drain(p = buf, CUT_SIZE, cut++, ct_off);
			if (b>=0 && b<10)
				*(p++) = '0'+b;
			else if (b>=10 && b <=15)
				*(p++) = 'a'+(b-10); 
			l++;
		}
		__no<<=4;
	}
	if (p>buf) {
		__drain(buf, p-buf, cut, ct_off);
		ct_off+=p-buf;
		if (ct_off>=CUT_SIZE) {
			cut++;
			ct_off-=CUT_SIZE;
		}
	}

	*buf = '\0';
	__drain(buf, 1, cut, ct_off);
	*__cut = cut;
	*__off = ct_off;
	return l;
}

// needs testing
static double t[] = {
	0.1,
	0.01,
	0.001,
	0.0001,
	0.00001,
	0.000001,
	0.0000001,
	0.00000001,
	0.000000001,
	0.0000000001
};


//# include <unistd.h>
/*
# include <stdio.h>
int main() {
	char buf[200];
	_int_u l;
	l = ffly_floatts(216.387733334, buf);
	printf("%s\n", buf);
//	l = ffly_noths(21299, buf);
//	printf("%s, '%c'\n", buf, *(buf+l));
//	l = ffly_nots(21299, buf);
//	_64_u i = 2376345750600;
//	_16_u cut = 0;
//	_8_u off = 0;
//	ffly_nots(21299, &cut, &off, drain);
//	printf("%s, '%c'\n", buf, *(buf+l));
}
*/
/*
# include "../data/str_len.h"
# include <stdio.h>
int main() {
	char s[1000];
//	  _int_u ii = ffly_floatts(21.22, s);
	//ffly_nots(2129900000000, s);
//	  _int_u i = f_str_len(s);
	ffly_noths(101987, s);
	printf("'%s'\n", s);


//	  _8_u i = 0;
  //  while(i != 4) {
	//	  printf("%lf\n", 0.01/t[i]);
	 //  i++;
	//}

	//printf("%u\n", 21299/1000);
//	  ffly_floatts(4.2332, s);
}
*/

/*
# include <stdio.h>
int main() {
	char s[1220];
	_int_u len = ffly_floatts(-0.00001, s);

	printf("%s:%u\n", s, len);
}*/

double ffly_stfloat(char *__s) {
	double ret;
	_8_u sign;
	if (sign = (*__s == '-')) __s++; 

	_int_u frac;
	char *p = __s, *end = NULL;
	while(*p != '.' && *p != '\0') p++;

	_int_u k = 0;
	if (*p == '.') {
		p++;
		frac = 0;
		while(*p != '\0') {
			frac = frac*10+(*p-'0');
			k++;
			p++;
		}
	}

	p = __s;
	_32_u v = 0;
	while(*p != '.' && *p != '\0') {
		v = v*10+(*p-'0');
		p++;
	}

	ret = (double)v+((double)frac*t[k-1]);
	if (sign)
		return -ret;  
	return ret;
}
