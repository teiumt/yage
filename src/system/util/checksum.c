# include "checksum.h"
_32_u ffly_bsd_cksum32(void *__p, _int_u __bc) {
    _32_u ret = 0;
    _8_u *p = (_8_u*)__p;
    while(p != (_8_u*)__p+__bc)
        ret = (((ret>>1)+((ret&0x1)<<15))+*(p++))&0xFFFF;
    return ret;
}

_64_u ffly_bsd_cksum64(void *__p, _int_u __bc) {
    _64_u ret = 0;
    _8_u *p = (_8_u*)__p;
    while(p != (_8_u*)__p+__bc)
        ret = (((ret>>1)+((ret&0x1)<<31))+*(p++))&0xFFFFFFFF;
    return ret;
}

_64_u ffly_ff_cksum64(void *__p, _int_u __bc) {
    _64_u ret = 0;
    _8_u *p = (_8_u*)__p;
    while(p != (_8_u*)__p+__bc) {
        ret = (((ret<<2)|(ret>>30&0x2))+(((*p)>>1)*(p-(_8_u*)__p)))&0xFFFFFF;
        p++;
    }

    return ret;
}

_32_u ffly_cksum32(void *__p, _int_u __bc) {
    return ffly_bsd_cksum32(__p, __bc);
}

_64_u ffly_cksum64(void *__p, _int_u __bc) {
    return ffly_bsd_cksum64(__p, __bc);
}
