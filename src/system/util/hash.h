# ifndef __ffly__hash__h
# define __ffly__hash__h
# include "../../y_int.h"
# ifdef __cplusplus
extern "C" {
# endif
//_64_u ffly_hash(_8_u const*, _int_u);
extern _64_u(*ffly_hash)(_8_u const*, _int_u);
# ifdef __cplusplus
}
namespace mdl {
namespace firefly {
namespace system {
namespace util {
static _64_u(*hash)(_8_u const*, _int_u) = ffly_hash;
}
}
}
}
# endif
# endif /*__ffly__hash__h*/
