# ifndef __ffly__ff5__h
# define __ffly__ff5__h
# include "../../y_int.h"
_int_u ffly_ff5_enc(void const*, char*, _int_u);
_int_u ffly_ff5_dec(char const*, void*, _int_u);
# endif /*__ffly__ff5__h*/
