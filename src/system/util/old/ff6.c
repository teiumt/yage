# include "ff6.h"
//# include "../io.h"
_int_u ffly_ff6_enc(void const *__src, char *__dst, _int_u __len) {
	_8_u const *in = (_8_u const*)__src;
	_8_u const *e = in+__len;
	char c0;
	_16_i *dst = (_16_u*)__dst;
	_64_u buf;
	_8_u v;

	_int_u n;


	buf = 0;
	n = 0;
_again:
	buf = buf<<8|*in; 
	n+=8;
_bk:
	if (n>=6) {
		v = ((buf>>(n-6))&0x3f);
		c0 = 'a'+(v>>3);
		v-=((v>>3)*(1<<3));
		*(_16_u*)dst = ('0'+v)<<8|c0;
		dst++;
		n-=6;
		goto _bk;
	}
	
	if (in<e) {
		in++;
		goto _again;
	}

	if (n>0) {
		v = (buf&(0xffffffffffffffff>>(64-n)));
		v = v<<(6-n);
		c0 = 'a'+(v>>3);
		v-=((v>>3)*(1<<3));
		*(_16_u*)dst = ('0'+v)<<8|c0;
		dst++;
	}

	return (char*)dst-__dst;
}

_int_u ffly_ff6_dec(char const *__src, void *__dst, _int_u __len) {
	char c0, c1;
	char const *in = __src;
	char const *e = in+__len;
	_8_u *dst = (_8_u*)__dst;

	_64_u buf;
	_int_u n;

	buf = 0;
	n = 0;
_again:
	if(in<e) {
		c0 = *(in++);
		if (c0 < 'a' || c0 > 'h') {
//			printf("error.\n");
			goto _fail;
		}

		c1 = *(in++);
		if (c1 < '0' || c1 > '7') {
//			printf("error.\n");
			goto _fail;
		}
		
		buf = (buf<<6)|(((c0-'a')<<3)+(c1-'0'));
		n+=6;

		if (n>=8) {
			*(dst++) = buf>>(n-8);
			n-=8;
			buf &= 0xffffffffffffffff>>(64-n);
		}
		goto _again;
	}
	return dst-(_8_u*)__dst;
_fail:
	return 0;
}
