# include "hash.h"
// going to be using this for now
_64_u static
ff_hash(_8_u const *__key, _int_u __bc) {
	_8_u const *p = __key;
	_8_u const *end = p+__bc;
	_64_u ret_val = 2<<(__bc>>2);
	while(p != end) {
		ret_val = (((ret_val>>1)+1)*(ret_val<<2))+(*p*(((p-__key)+1)<<1));
		p++;
	}
	return ret_val;
}

_64_u static
ff_dvhash(_8_u const *__key, _int_u __bc) {
	_8_u const *p = __key;
	_8_u const *end = p+__bc;
	_64_u ret_val = 1;
	while(p != end) {
		ret_val = ((ret_val&0xffffffffffffffff<<8)|(((ret_val&0xff)**p))>>2)<<4;
		ret_val<<=2;
		ret_val |= ret_val>>60;
		p++;
	}
	return ret_val;
}

_64_u static
ff_ahash(_8_u const *__key, _int_u __bc) {
	_8_u const *p = __key;
	_8_u const *end = p+__bc;
	_64_u ret_val = 1;
	while(p != end) {
		ret_val = ret_val<<16|(ret_val>>48);
		ret_val = ret_val^(*p*0x67452301);
		ret_val &= 0xffffffffffffff;
		p++;
	}
	return ret_val;
}

_64_u static
ff_rest(_8_u const *__key, _int_u __bc) {
	_8_u const *p = __key;
	_8_u const *end = p+__bc;
	_64_u ret_val = 0xffffffffffffffff;
	while(p != end) {
		ret_val = ret_val>>4|((ret_val>>60)^((ret_val&0xf)<<60));
		ret_val	= ret_val^(*__key^(ret_val&0xff));
		p++;
	}
	return ret_val;
}

_64_u(*ffly_hash)(_8_u const*, _int_u) = &ff_rest;
