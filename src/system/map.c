# include "map.h"
# include "../m_alloc.h"
#include "../crypt.h"
# include "../string.h"
# include "errno.h"
# include "err.h"
# include "../io.h"
# include "../oddity.h"
#define __MFSPS __mfsps
#define __VFSPS __vfsps
#define __CFSPS common
#define map_mask(__map) ((~(_64_u)0)>>(64-__map->size))
#define PAGE_SHIFT 4
#define PAGE_SIZE (1<<PAGE_SHIFT)

#define M_DEFLOCAL\
	struct f_map_common *common = &__MFSPS->cs;
#define V_DEFLOCAL\
	struct f_map_common *common = __VFSPS->cs;

#ifndef FF_MAP_SA
#define mem_alloc(__n)\
	m_alloc(__n)   
#define mem_free(__p)\
	m_free(__p)
#define mem_realloc(__p, __n)\
	m_realloc(__p, __n)
#else
#define mem_alloc(__n)\
	common->ad->alloc(common->ma_arg, __n)
#define mem_free(__p)\
	common->ad->free(common->ma_arg, __p)
#define mem_realloc( __p, __n)\
	common->ad->realloc(common->ma_arg, __p, __n)
#endif
/*
	TODO:
		remove any trace of vec and use pages of pointers
*/
/*still working on this*/
typedef struct {
	_64_u val;
	_8_u const *key;
	_int_u bc;
	void *p;
	void *prev, *next;
	struct vec *blk;
} map_entry_t;

#define vec_fbegin(__vec)\
	((__vec)->face)
#define vec_fend(__vec)\
	((__vec)->face+(__vec)->size-1)
#define vec_size(__vec)\
	((__vec)->size)
struct vec_blk;
typedef struct vec {
	void **p;
	_int_u page_c;
	_int_u off;
	_int_u bks;
	_int_u size;
	void **face;
	struct vec_blk *uu;
	struct f_map_common *cs;
} *vecp;

typedef struct vec_blk {
	_int_u off;
	_8_i inuse;
	_int_u n;
	struct vec_blk **bk, *fd;
} *vec_blkp;

_f_err_t static vec_init(vecp __VFSPS, _int_u __blk_size, struct f_map_common *__cs) {
	__VFSPS->bks = __blk_size+sizeof(struct vec_blk);
	__VFSPS->p = NULL;
	__VFSPS->page_c = 0;
	__VFSPS->off = 0;
	__VFSPS->uu = NULL;
	__VFSPS->size = 0;
	__VFSPS->face = NULL;
	__VFSPS->cs = __cs;
	return 0;
}

_f_err_t static vec_de_init(vecp __VFSPS) {
	V_DEFLOCAL
	if (__VFSPS->p != NULL) {
		void **pg, **end, *p;
		pg = __VFSPS->p;
		end = pg+__VFSPS->page_c;
		while(pg != end) {
			if ((p = *pg) != NULL) {
				mem_free(p);
			} else {
				// something is wrong
				caught_oddity;
			}
			pg++;
		}
	}
	mem_free(__VFSPS->p);
	return 0;
}

_f_err_t static vec_push_back(vecp __VFSPS, void **__p) {
	V_DEFLOCAL
	__VFSPS->size++;
	if (!__VFSPS->face) {
		__VFSPS->face = (void**)mem_alloc(__VFSPS->size*sizeof(void*));
	} else {
		__VFSPS->face = (void**)mem_realloc(__VFSPS->face, __VFSPS->size*sizeof(void*));
	}

	void **fp;

	fp = __VFSPS->face+(__VFSPS->size-1);

	if (__VFSPS->uu != NULL) {
		vec_blkp b;
		b = __VFSPS->uu;
		__VFSPS->uu = b->fd;
		b->fd->bk = b->bk;
		b->bk = NULL;
		b->fd = NULL;
		b->n = __VFSPS->size-1;
		b->inuse = 0;
		*fp = *__p = ((_8_u*)b)+sizeof(struct vec_blk);
		return 0;
	}

	_int_u pg, pg_off;
	pg = __VFSPS->off>>PAGE_SHIFT;
	pg_off = __VFSPS->off-(pg<<PAGE_SHIFT);
	if (pg>=__VFSPS->page_c) {
		_int_u pgc;
		pgc = ++__VFSPS->page_c;
		if (!__VFSPS->p) {
			__VFSPS->p = (void**)mem_alloc(pgc*sizeof(void*));
		} else {
			__VFSPS->p = (void**)mem_realloc(__VFSPS->p, pgc*sizeof(void*));
		}
		*(__VFSPS->p+pg) = mem_alloc(PAGE_SIZE*__VFSPS->bks);
	}
	_8_u *p;
	p = ((_8_u*)*(__VFSPS->p+pg))+(pg_off*__VFSPS->bks);
	
	vec_blkp b;
	b = (vec_blkp)p;
	b->bk = NULL;
	b->fd = NULL;
	b->off = __VFSPS->off;
	b->inuse = 0;
	b->n = __VFSPS->size-1;
	*fp = *__p = (void*)(p+sizeof(struct vec_blk));
	__VFSPS->off++;
	return 0;
}

_f_err_t static vec_pop_back(vecp __VFSPS, void *__p) {
	V_DEFLOCAL
	__VFSPS->size--;
	void *fe;

	fe = *(__VFSPS->face+__VFSPS->size);
	if (__VFSPS->size>1) {
		__VFSPS->face = (void**)mem_realloc(__VFSPS->face, __VFSPS->size*sizeof(void*));
	}

	_8_u *p;
	p = (_8_u*)__p;

	vec_blkp b;
	b = (vec_blkp)(p-sizeof(struct vec_blk));
	if (b->off == __VFSPS->off-1) {
		__VFSPS->off--;
		_int_u pg;
		pg = __VFSPS->off>>PAGE_SHIFT;
		if (pg < __VFSPS->page_c-1 && __VFSPS->page_c>1) {
			__VFSPS->page_c--;
			mem_free(*(__VFSPS->p+pg));
			__VFSPS->p = (void**)mem_realloc(__VFSPS->p, __VFSPS->page_c*sizeof(void*));
		}
		return 0;
	}
	
	*(__VFSPS->face+b->n) = fe;
	if (__VFSPS->uu != NULL)
		__VFSPS->uu->bk = &b->fd;
	b->fd = __VFSPS->uu;
	__VFSPS->uu = b;
	b->bk = &__VFSPS->uu;
	b->inuse = -1;
	return 0;
}

_f_err_t _ffly_map_init(ffly_mapp __MFSPS, _int_u __size, struct ei_struc *__ei) {
	M_DEFLOCAL
#ifdef FF_MAP_SA
	if (__ei != NULL) {
		__MFSPS->cs.ad = __ei->ad;
		goto _sk;
	}
	__MFSPS->cs.ad = ffly_ad_entry(0);
_sk:
#endif
	__MFSPS->size = __size;
	__MFSPS->table = mem_alloc((map_mask(__MFSPS)+1)*sizeof(struct vec*));
	struct vec **itr = (struct vec**)__MFSPS->table;
	while(itr != ((struct vec**)__MFSPS->table)+map_mask(__MFSPS)+1)
		*(itr++) = NULL;
	__MFSPS->begin = NULL;
	__MFSPS->end = NULL;
	__MFSPS->parent = NULL;
	return FFLY_SUCCESS;
}

ffly_mapp _ffly_map_creat(_int_u __size, struct ei_struc *__ei) {
	ffly_mapp p;
	void*(*alloc)(long long, _int_u);
	struct ffly_adentry *ad;
#ifdef FF_MAP_SA
	if (__ei != NULL) {
		ad = __ei->ad;
		goto _sk;
	}
	ad = ffly_ad_entry(0);
#else
	ad = ffly_ad_entry(0);
#endif
_sk:
	alloc = ad->alloc;
	p = (ffly_mapp)alloc(~0, sizeof(struct ffly_map));
	_ffly_map_init(p, __size, __ei);
	return p;
}

ffly_mapp _ffly_map(_int_u __size, struct ei_struc *__ei) {
	ffly_mapp p;

	p = _ffly_map_creat(__size, __ei);

//	p = (ffly_mapp)mem_alloc(sizeof(struct ffly_map));
//	_ffly_map_init(p, __size, __ei);
	return p;
}

void ffly_map_free(ffly_mapp __MFSPS) {
	M_DEFLOCAL
	mem_free(__MFSPS);
}

void ffly_map_destroy(ffly_mapp __MFSPS) {
	M_DEFLOCAL
	ffly_map_de_init(__MFSPS);
	mem_free(__MFSPS);
}

void const* ffly_map_begin(ffly_mapp __MFSPS) {
	return (void const*)__MFSPS->begin;
}

void const* ffly_map_beg(ffly_mapp __MFSPS) {
	return (void const*)__MFSPS->begin;
}

void const* ffly_map_end(ffly_mapp __MFSPS) {
	return (void const*)__MFSPS->end;
}

void ffly_map_del(ffly_mapp __MFSPS, void const *__p) {
	M_DEFLOCAL
	map_entry_t *p = (map_entry_t*)__p;
	mem_free((void*)p->key);
	map_entry_t *end;
	vec_pop_back(p->blk, (void*)&end);
	*p = *end;
	mem_free(end);
}

void ffly_map_fd(ffly_mapp __MFSPS, void const **__p) {
	*(void**)__p = ((map_entry_t*)*__p)->next;
}

void ffly_map_bk(ffly_mapp __MFSPS, void const **__p) {
	*(void**)__p = ((map_entry_t*)*__p)->prev;
}

void ffly_map_itr(ffly_mapp __MFSPS, void const **__p, _8_u __dir) {
	if (__dir == MAP_ITR_FD)
		ffly_map_fd(__MFSPS, __p);
	else if (__dir == MAP_ITR_BK)
		ffly_map_bk(__MFSPS, __p);	
}

void *const ffly_map_getp(void const *__p) {
	return ((map_entry_t*)__p)->p;
}

void ffly_map_parent(ffly_mapp __MFSPS, ffly_mapp __child) {
	__child->parent = __MFSPS;
}

map_entry_t static* map_find(ffly_mapp __MFSPS, _8_u const *__key, _int_u __bc) {
	_64_u val = y_hash64(__key, __bc);
	struct vec **blk = ((struct vec**)__MFSPS->table)+(val&map_mask(__MFSPS));
	if (!*blk) {
		ffly_fprintf(ffly_log, "map block is null, nothing hear.\n");
		return NULL;
	}

	void **itr;
	if (vec_size(*blk)>0) {
		itr = vec_fbegin(*blk);
		while(itr <= vec_fend(*blk)) {
			map_entry_t *entry = *(map_entry_t**)*itr;
			if (entry->val == val && entry->bc == __bc)
				if (!mem_cmp((void*)__key, (void*)entry->key, __bc)) return entry;
			itr++;
		}
	}

	if (__MFSPS->parent != NULL)
		return map_find(__MFSPS->parent, __key, __bc);
	ffly_fprintf(ffly_log, "diden't find.\n");
	return NULL;
}

_f_err_t ffly_map_put(ffly_mapp __MFSPS, _8_u const *__key, _int_u __bc, void *const __p) {
	M_DEFLOCAL
	_64_u val = y_hash64(__key, __bc);
	struct vec **blk = ((struct vec**)__MFSPS->table)+(val&map_mask(__MFSPS));
	if (!*blk) {
		*blk = (struct vec*)mem_alloc(sizeof(struct vec));
		if (_err(vec_init(*blk, sizeof(map_entry_t*), &__MFSPS->cs))) {
			ffly_fprintf(ffly_err, "failed to init vec.\n");
		}
	}

	map_entry_t **entry;
	if (_err(vec_push_back(*blk, (void**)&entry))) {
		ffly_fprintf(ffly_err, "failed to push map entry.\n");
	}

	_8_u *key;
	if (_err(mem_dup((void**)&key, (void*)__key, __bc))) {
		ffly_fprintf(ffly_err, "failed to dupe key.\n");
	}

	*entry = (map_entry_t*)mem_alloc(sizeof(map_entry_t));
	**entry = (map_entry_t) {
		.val = val,
		.key = key,
		.bc = __bc,
		.p = __p,
		.next = NULL,
		.blk = *blk
	};

	if (__MFSPS->end != NULL) {
		((map_entry_t*)__MFSPS->end)->next = (void*)*entry;
		(*entry)->prev = __MFSPS->end;
		__MFSPS->end = (void*)*entry;
	} else
		__MFSPS->end = (void*)*entry;

	if (!__MFSPS->begin)
		__MFSPS->begin = (void*)*entry;
}

void const* ffly_map_fetch(ffly_mapp __MFSPS, _8_u const *__key, _int_u __bc) {
	return (void const*)map_find(__MFSPS, __key, __bc);
}

void *const ffly_map_get(ffly_mapp __MFSPS, _8_u const *__key, _int_u __bc, _f_err_t *__err) {
	map_entry_t *entry = map_find(__MFSPS, __key, __bc);
	if (!entry) {
		ffly_fprintf(ffly_log, "entry was not found.\n");
		*__err	= FFLY_FAILURE;
		return NULL;
	}
	*__err = FFLY_SUCCESS;
	return entry->p;
}

void static free_blk(struct f_map_common *__CFSPS, struct vec **__blk) {
	void **itr;
	if (vec_size(*__blk)>0) {
		map_entry_t *et;
		itr = vec_fbegin(*__blk);
		while(itr <= vec_fend(*__blk)) {
			et = *(map_entry_t**)*itr;
			mem_free((void*)et->key);
			mem_free(et);
			itr++;
		}
	}
	vec_de_init(*__blk);
	mem_free(*__blk);
}

_f_err_t ffly_map_de_init(ffly_mapp __MFSPS) {
	M_DEFLOCAL
	struct vec **itr = (struct vec**)__MFSPS->table;
	while(itr != ((struct vec**)__MFSPS->table)+map_mask(__MFSPS)) {
		if (*itr != NULL) free_blk(&__MFSPS->cs, itr);
		itr++;
	}

	mem_free(__MFSPS->table);
	return FFLY_SUCCESS;
}
