# ifndef __buff__h
# define __buff__h
# include "../y_int.h"
# include "../types.h"
# include "../io.h"
# include "../mutex.h"
# include "error.h"
# include "flags.h"
#ifdef __cplusplus
#	include "errno.h"
#endif
# include "../ei_struc.h"
# include "../ad.h"
#define FF_BUFF_SA
/*
	TODO:
		put, get function no single block add n param/arg what every its fucking called
*/
#define f_buffp ffly_buffp
struct ffly_buff {
#ifdef FF_BUFF_SA
	_ulonglong ma_arg;
	struct ffly_adentry *ad;
#endif
	mlock lock;
	_8_u *p;
	_f_off_t off;
	_int_u ofof;
	_int_u rs_blk_c;
	_int_u blk_c;
	_f_size_t blk_size;
    ff_flag_t flags; //for later
};

typedef struct ffly_buff* ffly_buffp;
# ifdef __cplusplus
extern "C" {
# endif
#define ffly_buff_init(__buff, __bc, __bs)\
	_ffly_buff_init(__buff, __bc, __bs, NULL)
_f_err_t _ffly_buff_init(ffly_buffp, _int_u, _f_size_t, struct ei_struc*);
_f_err_t ffly_buff_put(ffly_buffp, void*);
_f_err_t ffly_buff_get(ffly_buffp, void*);
_f_err_t ffly_buff_resize(ffly_buffp, _int_u);
_f_err_t ffly_buff_de_init(ffly_buffp);

_f_err_t _f_buf_incr(ffly_buffp, _32_u);
_f_err_t _f_buf_decr(ffly_buffp, _32_u);
#define ffly_buff_incr(__buf) _f_buf_incr(__buf, 1)
#define ffly_buff_decr(__buf) _f_buf_decr(__buf, 1)

_f_err_t ffly_buff_reset(ffly_buffp);
_f_err_t f_buf_xput(f_buffp, void*, _int_u);
_f_err_t f_buf_xget(f_buffp, void*, _int_u);
void* ffly_buff_at(ffly_buffp, _int_u);
void* ffly_buff_getp(ffly_buffp);
# ifdef __cplusplus
}
# endif
_int_u static __inline__ ffly_buff_ublk_c(struct ffly_buff *__buff){return __buff->off;}
void static __inline__ ffly_buff_off_reset(struct ffly_buff *__buff) {__buff->off = 0;}
void static __inline__* ffly_buff_begin(struct ffly_buff *__buff) {return (void*)__buff->p;}
void static __inline__* ffly_buff_end(struct ffly_buff *__buff) {return (void*)(__buff->p+((__buff->off-1)*__buff->blk_size));}
void static __inline__ ffly_buff_lock(struct ffly_buff *__buff) {mt_lock(&__buff->lock);}
void static __inline__ ffly_buff_unlock(struct ffly_buff *__buff) {mt_unlock(&__buff->lock);}
_f_bool_t static __inline__ ffly_buff_full(struct ffly_buff *__buff) {return (__buff->off == __buff->blk_c-1);}
_f_bool_t static __inline__ ffly_buff_empty(struct ffly_buff *__buff) {return !__buff->off;}
_f_off_t static __inline__ ffly_buff_off(struct ffly_buff *__buff){return __buff->off;}
_int_u static __inline__ ffly_buff_size(struct ffly_buff *__buff){return __buff->blk_c;}
# ifdef __cplusplus
namespace ff {
static err_t(*buff_init)(ffly_buffp, uint_t, size_t) = &ffly_buff_init;
static err_t(*buff_put)(ffly_buffp, void*) = &ffly_buff_put;
static err_t(*buff_get)(ffly_buffp, void*) = &ffly_buff_get;
static err_t(*buff_resize)(ffly_buffp, uint_t) = &ffly_buff_resize;
static err_t(*buff_de_init)(ffly_buffp) = &ffly_buff_de_init;
err_t static __inline__ buff_incr(struct ffly_buff *__buff) {return ffly_buff_incr(__buff);}
err_t static __inline__ buff_decr(struct ffly_buff *__buff) {return ffly_buff_decr(__buff);}
void static __inline__ buff_off_reset(struct ffly_buff *__buff) {ffly_buff_off_reset(__buff);}
void static __inline__* buff_begin(struct ffly_buff *__buff) {return ffly_buff_begin(__buff);}
void static __inline__* buff_end(struct ffly_buff *__buff) {return ffly_buff_end(__buff);}
static err_t(*buff_reset)(ffly_buffp) = &ffly_buff_reset;

template<typename _T>
struct buff {
	buff(uint_t __size) {
		if (this->init(__size) != FFLY_SUCCESS)
			fprintf(ffly_err, "buff: failed to init.\n");}
	~buff() {
		if (this->de_init() != FFLY_SUCCESS)
			fprintf(ffly_err, "buff: failed to de_init.\n");}
	err_t init(size_t __size) {buff_init(&this->raw, __size, sizeof(_T));}
	err_t de_init() {buff_de_init(&this->raw);}

	_T* begin() {return buff_begin(&this->raw);}
	_T* end() {return buff_end(&this->raw);}

	void put(_T __elem) {buff_put(&this->raw, &__elem);}
	_T get() {
		_T ret;
		buff_get(&this->rawbuff, &ret);
		return ret;
	}

	err_t resize(size_t __size) {return buff_resize(&this->raw, __size);}
	void reset_off() {buff_off_reset(&this->raw);}
	void reset() {buff_reset(&this->raw);}
	void incr() {buff_incr(&this->raw);}
	void decr() {buff_decr(&this->raw);}

	void operator++(int) {this->incr();}
	void operator--(int) {this->decr();}
	_T& operator*() {
		return *static_cast<_T*>(ffly_buff_getp(&this->raw));
	}
	struct ffly_buff raw;
};
}
# endif
# endif /*__buff__h*/

