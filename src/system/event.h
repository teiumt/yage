# ifndef __ffly__system__event__h
# define __ffly__system__event__h
# include "../y_int.h"
# include "../types.h"
# include "../event.h"
# include "queue.h"
# ifdef __cplusplus
extern "C" {
# endif
ffly_event_t* ffly_event_dup(ffly_event_t*);
_f_err_t ffly_event_push(ffly_event_t*);
_f_err_t ffly_event_pop(ffly_event_t**);
_f_err_t ffly_event_peek(ffly_event_t**);
_f_bool_t ffly_pending_event(void);
/*
    event intercept routine - READ ONLY.
    - wont effect queue.
*/
_f_err_t ffly_add_eir(_f_err_t(*)(ffly_event_t*, void*), void*);
# ifdef __cplusplus
}
namespace mdl {
namespace firefly {
namespace system {
static types::err_t(*event_push)(types::event_t*) = &ffly_event_push;
static types::err_t(*event_pop)(types::event_t**) = &ffly_event_pop;
static types::err_t(*event_peek)(types::event_t**) = &ffly_event_peek;
static types::bool_t(*pending_event)() = &ffly_pending_event;
}
}
}
# endif
# endif /*__ffly__system__event__h*/
