#ifndef __ffly__tls__h
#define __ffly__tls__h
#include "../y_int.h"
#include "../types.h"
#include "../ffly_def.h"

_64_u __FORCE_INLINE__ tls_getoff(void *p){
	void *ptr;
	__asm__("movq %%fs:0, %0" : "=r"(ptr));
	return ((_64_u)p)-((_64_u)ptr);
}


#define TLS_BASE(__p) (((_64_u)__p)+REAL_TLS)
#define TLS_REVERT(__p) (((_64_u)__p)-REAL_TLS)
#define TLS_SIZE 32768/*4096-quadwords*/
#define TLS_PRIVATE 8
#define REAL_TLS (TLS_SIZE-TLS_PRIVATE)
/*
	ignore as we dont have access to the compiler __thread we have to use this
	NOTE: might change as plan is to make are own c compiler thus src/clang
*/
void ffly_tls_toinit(void(*)(void));
void ffly_tls_init(void);
_f_err_t ffly_tls_new(void);
void ffly_tls_destroy(void);
_int_u ffly_tls_alloc(_int_u);
#endif /*__ffly__tls__h*/
