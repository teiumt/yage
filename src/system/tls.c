#include "tls.h"
#include "../linux/mman.h"
#include "../linux/prctl.h"
#include "../ffly_def.h"
#include "error.h"
#include "../assert.h"
/*
%fs:0 +8 is reserved for ptr
*/
static __thread int test;
_f_err_t
ffly_tls_new(void) {
	void *p;
	p = mmap(NULL, TLS_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
	if (p == (void*)-1) {
		reterr;		
	}
	
	p = ((_64_u)p)+REAL_TLS;
	if (arch_prctl(ARCH_SET_FS,p) == -1) {
		// error
	}
	*(void**)p = p;
	retok;
}

void ffly_tls_destroy(void) {
	void *p;
	if (arch_prctl(ARCH_GET_FS, (_64_u)&p) == -1) {
		// error
	}
	
	void *pp;
	__asm__("movq %%fs:0, %0" : "=r"(pp));
	massert(pp == p,"tls error.\n");
	p = ((_64_u)p)-REAL_TLS;
	munmap(p, TLS_SIZE);
}

/*
	list of functions to call to init tls 
*/
static void(*init[26])(void);
static void(**end)(void) = init;

// error handle
void ffly_tls_toinit(void(*__func)(void)) {
	*(end++) = __func;
}

void ffly_tls_init(void) {
	void(**cur)(void) = init;
	while(cur != end)
		(*(cur++))();
}
