# ifndef __ffly__barrel__h
# define __ffly__barrel__h
# include "../y_int.h"
# include "../types.h"
# define FFLY_BARREL_SIZE 40


/*
	TODO:
		needs work done on 
	what is this for?
		like pool.c but fixed size and linkable
*/

// for user
#define ffly_barrel_link(__barrel, __to) \
	(__barrel)->link = __to
typedef struct ffly_barrel {
	void **blocks;
	void **fresh;
	void **unused;
	void **next;
	_int_u blksize;
	struct ffly_barrel *link;
} *ffly_barrelp;

void ffly_barrel_init(ffly_barrelp, _int_u);
void ffly_barrel_de_init(ffly_barrelp);
_f_bool_t ffly_barrel_full(ffly_barrelp);

void* ffly_barrel_alloc(ffly_barrelp);
void ffly_barrel_free(ffly_barrelp, void*);
# endif /*__ffly__barrel__h*/
