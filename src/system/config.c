# include "config.h"
# include "../bole.h"
# include "errno.h"
# include "err.h"
# include "../io.h"
# include "../m_alloc.h"
# include "../config.h"
# include "../string.h"
# include "../msg.h"
#define MSG_BITS FFLY_MSG_DOMAIN(_DOM_SYSCONFIG)
#define DEF_MAX_THREADS 20
#define ALLOCA_SSIZE 400
#define BINFILE "syscf.bin"

/*
	TODO cleanup

	why is this dont like this and not in struct form????
	
	i dont know, well its easer to write back to disk then a struct 
	as its would have to be serialized and alignment is not guaranteed to be the same

	i like it this way but dont at the same time.

	all data is writen to one place
	so ffly_sysconf i think?

	this includes strings ints and all no pointers just raw data
	so when we write to file all we do it write(ffly_sysconf)
	thats it
	this is why its built like this

	if dont with struct?

	struct {
		int
		int

		<cant use pointers>
		char *str

		so
		char str[200] <- fixed size(big fucking issue) 
		we want to be able to handle large strings
	};

	this way might be more ineffecent then a structure i dont know
	if it is as havent checked but most likly is

	as i would think it would be

	STRCTURE???
	lea offset(structPTR), %rax
	mov data, (%rax)

	THIS
	lea offset(SYSCONF), %rax
	mov data, (%rax)

	i think it would make no diffrence.

	also this way allows for more dynamic allocation of space for strings,embedded structs.

	STRING ACCESS
	lea offset(SYSCONF), %rax
	%rax = (pointer of string)(START)
*/
_8_u ffly_sysconf[2048];
_64_u static scdoff = 0;

_8_u static ldcode[512];
_8_u static *lc = ldcode;

void static* alloc(_int_u __size) {
	_32_u of;
	of = scdoff;
	scdoff+=__size;
	return (ffly_sysconf+of+SCF_SZ);
}

char const static* _strdup(char const *__str) {
	char const *s;
	_int_u size;
	s = (char const*)alloc(size = (str_len(__str)+1));
	mem_cpy(s, __str, size);
	return s;
}

#define _MAX_THREADS		0x0000000000000001
#define _ALSSIZE			0x0000000000000002
static _64_u flags = 0;
#define isset(__what) \
	((flags&(__what)) == (__what))
#define isnset(__what) \
	!isset(__what)

# include "../config.h"
// move up to conf real level
void
f_sysconf_moveup(void) {
	MSG(INFO, "moving up to core config level.\n")

	MSG(INFO, "setting max_thread=%u.\n", SYSC_AT(max_threads))
	F_CONF(max_threads) = SYSC_AT(max_threads);
}

void
ff_sysconf_init(void) {
	mem_set(ffly_sysconf, 0, 256);
	*sysconf_get(loaded) = -1;
	*sysconf_get(db_loaded) = -1;
}

void static
ld_def_max_threads(void) {
	*sysconf_get(max_threads) = DEF_MAX_THREADS;
}

void static
ld_def_alssize(void) {
	*sysconf_get(alssize) = ALLOCA_SSIZE;
}

void ffly_ld_sysconf_def(void) {
	ld_def_max_threads();
	ld_def_alssize();
}

void static
ld_max_threads(ffbole *__cf) {
	void const *p;
	if ((p = ffly_bole_get(__cf, "max_threads")) != NULL) {
		*sysconf_get(max_threads) = ffly_bole_int_u(p);
		flags |= _MAX_THREADS;
	}
	f_fprintf(ffly_log, "max threads: %u\n", *sysconf_get(max_threads));
}

void static
ld_alssize(ffbole *__cf) {
	void const *p;
	if ((p = ffly_bole_get(__cf, "alssize")) != NULL) {
		*sysconf_get(alssize) = ffly_bole_int_u(p);
		flags |= _ALSSIZE;
	}
	f_fprintf(ffly_log, "alloca ssize: %u\n", *sysconf_get(alssize));
}

void static
ld_moddir(ffbole *__cf) {
	void const *p;
	void *p0;
	if ((p = ffly_bole_get(__cf, "moddir")) != NULL) {
		p0 = *sysconf_get(moddir) = _strdup(ffly_bole_str(p));
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_moddir;
		lc+=5;
	}
}

void static
ld_inidir(ffbole *__cf) {
	void const *p;
	void *p0;
	if ((p = ffly_bole_get(__cf, "inidir")) != NULL) {
		p0 = *sysconf_get(inidir) = _strdup(ffly_bole_str(p));
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_inidir;
		lc+=5;
	}
}

void static
ld_modl(ffbole *__cf) {
	void const *p;
	void *p0, *p1;
	if ((p = ffly_bole_get(__cf, "modl")) != NULL) {
		f_printf("module list len: %u\n", ffly_bole_arr_len(p));
		_8_u i, l = ffly_bole_arr_len(p);
		p0 = *sysconf_get(modl) = (char const**)alloc((l+1)*sizeof(char const*));
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_modl;	
		lc+=5;

		i = 0;
		while(i != l) {
			p1 = (*sysconf_get(modl))[i] = _strdup(ffly_bole_str(ffly_bole_arr_elem(p, i)));
			f_printf("module: %s\n", ffly_bole_str(ffly_bole_arr_elem(p, i)));
			*lc = SCF_LC_PP;
			*(_16_u*)(lc+1) = offof(p1);
			*(_16_u*)(lc+3) = offof(p0+(i*sizeof(void*)));
			lc+=5;
			i++;
		}
		(*sysconf_get(modl))[i] = NULL;
	}
}

void static
ld_inil(ffbole *__cf) {
	void const *p;
	void *p0, *p1;
	if ((p = ffly_bole_get(__cf, "inil")) != NULL) {
		f_printf("init list len: %u\n", ffly_bole_arr_len(p));
		_8_u i, l = ffly_bole_arr_len(p);
		if (!(p0 = *sysconf_get(inil) = (char const**)alloc((l+1)*sizeof(char const*)))) {
			f_printf("error.\n");
			return;
		}

		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_inil;
		lc+=5;

		i = 0;
		while(i != l) {
			p1 = (*sysconf_get(inil))[i] = _strdup(ffly_bole_str(ffly_bole_arr_elem(p, i)));
			f_printf("init: %s\n", ffly_bole_str(ffly_bole_arr_elem(p, i)));
			*lc = SCF_LC_PP;
			*(_16_u*)(lc+1) = offof(p1);
			*(_16_u*)(lc+3) = offof(p0+(i*sizeof(void*)));
			lc+=5;
			i++;
		}
		(*sysconf_get(inil))[i] = NULL;
	}
}

void static
ld_db(void const *__db) {
	void const *ip_addr;
	void const *port;
	void const *enckey;
	void const *user;
	void const *passwd;

	void *p0;
	f_printf("*db-composition:\n");
	if ((ip_addr = ffly_bole_struc_get(__db, "ip_addr")) != NULL) {
		p0 = *sysconf_get(db_ip_addr) = _strdup(ffly_bole_str(ip_addr));
		f_printf("\t-ip addr: %s\n", *sysconf_get(db_ip_addr));
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_db_ip_addr;
		lc+=5;
	}

	if ((port = ffly_bole_struc_get(__db, "port")) != NULL) {
		*sysconf_get(db_port) = ffly_bole_16l_u(port);
		f_printf("\t-port: %u\n", *sysconf_get(db_port));
	}

	if ((enckey = ffly_bole_struc_get(__db, "enckey")) != NULL) {
		p0 = *sysconf_get(db_enckey) = _strdup(ffly_bole_str(enckey));
		f_printf("\t-enckey: %s\n", *sysconf_get(db_enckey));
		*lc = SCF_LC_PP;
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_db_enckey;
		lc+=5;
	}

	if ((user = ffly_bole_struc_get(__db, "user")) != NULL) {
		p0 = *sysconf_get(db_user) = _strdup(ffly_bole_str(user));
		f_printf("\t-username: %s\n", *sysconf_get(db_user));
		*lc = SCF_LC_PP;
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_db_user;
		lc+=5;
	}

	if ((passwd = ffly_bole_struc_get(__db, "passwd")) != NULL) {
		p0 = *sysconf_get(db_passwd) = _strdup(ffly_bole_str(passwd));
		f_printf("\t-password: %s\n", *sysconf_get(db_passwd));
		*lc = SCF_LC_PP;
		*lc = SCF_LC_PP;
		*(_16_u*)(lc+1) = offof(p0);
		*(_16_u*)(lc+3) = SCF_db_passwd;
		lc+=5;
	}
	*sysconf_get(db_loaded) = 0;
}

# include "../linux/unistd.h"
# include "../linux/fcntl.h"
# include "../linux/stat.h"
# include "../oddity.h"
/*
	provide hash
*/
void static exec(_int_u __n) {
	_8_u *p, *e;
	p = ldcode;
	e = p+__n;

	_8_u on;
	while(p != e) {
		on = *(p++);
		switch(on) {
			case 0x00:
				goto _end;
			case 0x01: {
				MSG(INFO, "op - place pointer.\n")
				_16_u s, d;
				s = *(_16_u*)p;
				d = *(_16_u*)(p+2);
				*(void**)(ffly_sysconf+d) = ffly_sysconf+s;
				p+=4;
				break;
			}
			default:
				caught_oddity;

		}
	}
_end:
	return;
}

struct binhdr {
	_32_u data;
	_32_u code;
	_int_u cs;
};

void static
save_bin(void) {
	int fd;
	fd = open(BINFILE, O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR);
	struct binhdr h;
	h.data = sizeof(struct binhdr);
	h.code = h.data+sizeof(ffly_sysconf);
	h.cs = lc-ldcode;

	MSG(INFO, "data: %u, code: %u, codelen: %u\n", h.data, h.code, h.cs)
	lseek(fd, h.data, SEEK_SET);
	write(fd, ffly_sysconf, sizeof(ffly_sysconf));
	write(fd, ldcode, h.cs);
	pwrite(fd, &h, sizeof(struct binhdr), 0);
	close(fd);
}

void static
load_bin(void) {
	int fd;
	fd = open(BINFILE, O_RDONLY, 0);
	struct binhdr h;
	read(fd, &h, sizeof(struct binhdr));

	MSG(INFO, "data: %u, code: %u, codelen: %u\n", h.data, h.code, h.cs)
	pread(fd, ffly_sysconf, sizeof(ffly_sysconf), h.data);
	pread(fd, ldcode, h.cs, h.code);
	exec(h.cs);
	close(fd);
}

# include "../path.h"
# include "../linux/unistd.h"
_f_err_t ffly_ld_sysconf(char const *__path) {
	if (!access("syscf.bin", F_OK)) {
		MSG(INFO, "loading binary config.\n");
		load_bin();
		return;
	}

	struct ffly_bole conf;
	_f_err_t err = FFLY_SUCCESS;
	if (_err(err = ffly_bole_init(&conf))) {
		f_fprintf(ffly_err, "failed to init config.\n");
		return err;
	}

	if (_err(err = ffly_bole_ld(&conf, __path))) {
		f_fprintf(ffly_err, "failed to load config.\n");
		return err;
	}
	
	if (_err(err = ffly_bole_read(&conf))) {
		f_fprintf(ffly_err, "failed to read config.\n");
		return err;
	}

	ffbole cf;
	// deposit
	ffly_bole_depos(&conf, &cf);

	void const *version;
	
	version = ffly_bole_get(&cf, "version");
	if (!ffly_bole_is_str(version)) {
		f_fprintf(ffly_err, "can't read version as type does not match.\n");
		err = FFLY_FAILURE;
		goto _fail;
	}

	void const *root_dir;
	
	root_dir = ffly_bole_get(&cf, "root_dir");
	if (!ffly_bole_is_str(root_dir)) {

	}

	void const *db;
	if ((db = ffly_bole_get(&cf, "db")) != NULL)
		ld_db(db);

	ld_max_threads(&cf);
	ld_alssize(&cf);
	ld_moddir(&cf);
	ld_inidir(&cf);
	ld_modl(&cf);
	ld_inil(&cf);

	void *p0;

	p0 = *sysconf_get(version) = _strdup(ffly_bole_str(version));
	*lc = SCF_LC_PP;
	*(_16_u*)(lc+1) = offof(p0);
	*(_16_u*)(lc+3) = SCF_version;
	lc+=5;


	p0 = *sysconf_get(root_dir) = _strdup(ffly_bole_str(root_dir));
	*lc = SCF_LC_PP;
	*(_16_u*)(lc+1) = offof(p0);
	*(_16_u*)(lc+3) = SCF_root_dir;
	lc+=5;

	p0 = *sysconf_get(loaded) = 0;

	if (isnset(_MAX_THREADS)) {
		f_printf("'max threads' not supplied in config, loading default.\n");
		ld_def_max_threads();
	}

	if (isnset(_ALSSIZE)) {
		f_printf("'alloca stack size' not supplied in config, loading default.\n");
		ld_def_alssize();
	}
_fail:
	if (_err(err = ffly_bole_free(&conf))) {
		f_fprintf(ffly_err, "failed to free config:0.\n");
		return err;
	}

	if (_err(err = ffbole_free(&cf))) {
		f_fprintf(ffly_err, "failed to free config:1.\n");
		return err;
	}

	save_bin();
	return FFLY_SUCCESS;
}

# include "../macros.h"
void ffly_free_sysconf(void) {


}
