#!/bin/sh
compile () {
	$ffly_cc $cc_flags -c -o $dst_dir/$1 $root_dir/$2
}

compile file.o file.c
compile string.o string.c
compile tls.o tls.c
compile errno.o errno.c
compile fire_fs.o fire_fs.c
compile posix_fs.o posix_fs.c
compile fs.o fs.c
compile config.o config.c
compile vec.o vec.c
compile buff.o buff.c
compile map.o map.c
compile err.o err.c
export ffly_objs="$dst_dir/tls.o $dst_dir/string.o \
$dst_dir/file.o $dst_dir/errno.o $dst_dir/fire_fs.o \
$dst_dir/posix_fs.o $dst_dir/fs.o $dst_dir/config.o $dst_dir/vec.o $dst_dir/buff.o \
$dst_dir/map.o $dst_dir/err.o"
