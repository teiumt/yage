# include "fs.h"
#ifdef __fflib
# include "../linux/unistd.h"
# include "../linux/fcntl.h"
#else
# include <fcntl.h>
# include <unistd.h>
#endif
# include "error.h"
/*
	TODO:
		make better use direct syscall??? or call __open and not open?????
*/
_8_s static
_open(f_fsus_nodep __n, char const *__path, _32_u __flags, _32_u __mode) {
	_32_s fd;
	fd = open(__path, __flags, __mode);
	if (fd<0)
		return -1;
	__n->fd = fd;
	return 0;
}

_8_s static
_close(f_fsus_nodep __n) {
	_32_s res;
	res = close(__n->fd);
	if (res<0)
		return -1;
	return 0;
}

_32_s static
_lseek(f_fsus_nodep __n, _64_u __off, _32_u __whence, _8_s *__error) {
	return lseek(__n->fd, __off, __whence);
}

_64_u static 
_read(f_fsus_nodep __n, void *__buf, _64_u __size, _8_s *__error) {
	_32_s res;
	res = read(__n->fd, __buf, __size);
	if (res<0)
		*__error = -1;
	*__error = 0;

	return res;
}

_64_u static
_write(f_fsus_nodep __n, void *__buf, _64_u __size, _8_s *__error) {
	_32_s res;
	res = write(__n->fd, __buf, __size);
	if (res<0)
		*__error = -1;
	*__error = 0;


	return res;
}

_64_u static
_pwrite(f_fsus_nodep __n, void *__buf, _64_u __size, _64_u __offset, _8_s *__error) {
	_32_s res;
	res = pwrite(__n->fd, __buf, __size, __offset);
	if (res<0)
		*__error = -1;
	*__error = 0;

	return res;
}

_64_u static
_pread(f_fsus_nodep __n, void *__buf, _64_u __size, _64_u __offset, _8_s *__error) {
	_32_s res;
	res = pread(__n->fd, __buf, __size, __offset);
	if (res<0)
		*__error = -1;
	*__error = 0;


	return res;
}

_32_s static
_access(char const *__path, _32_u __mode, _8_s *__error) {
	return access(__path, __mode);
}

_32_s static
_creat(char const *__path, _32_u __mode, _8_s *__error) {
	return creat(__path, __mode);
}

static struct ffly_fsop _op = {
	_open,
	_close,
	_lseek,
	_read,
	_write,
	_pwrite,
	_pread,
	_access,
	_creat
};

void posix_fsop(struct ffly_fsop *__op) {
	*__op = _op;
}
