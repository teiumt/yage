# include "port.h"
# include "shm.h"
# include "../string.h"
# include "../linux/ipc.h"
# include "../linux/stat.h"
# include "error.h"
# include "mutex.h"
# include "io.h"
#define STRT 0x1
#define STOP 0x2
#define DUMP 0x4
#define OK 0x8

#define is_flag(__flags, __flag) \
	((__flags&__flag) == __flag)
#define clr_bit(__bits, __bit) \
	(*__bits) ^= *__bits&__bit;
#define set_bit(__bits, __bit)\
	(*__bits) |= __bit;
#define is_bit(__bits, __bit) \
	((*__bits&__bit) == __bit)
#define get_port(__id) (ports+__id)
#define MAX 5

typedef struct hdr {
	_16_u size;
} *hdrp;

struct port {
	_int_u shm_id;
	_8_u *bits;
	hdrp h;
	void *buf, *p;
	_int_u width;
	_8_u flags;
	mlock *tx_lock;
	_32_u *band;
};

struct port static ports[MAX];
struct port static *next = ports;
struct port static *dead[MAX];
struct port static **reuse = dead;

#define ADRNULL 0xff
_int_u ffly_port_get_shmid(_int_u __id) {
	return get_port(__id)->shm_id;
}

void static
discard(struct port *__pi) {
	if (__pi == next-1)
		next--;
	else
		*(reuse++) = __pi;
}

_int_u ffly_port(_int_u __width, _8_u __flags,
	_int_u __id, _f_err_t *__err)
{
	*__err = FFLY_SUCCESS;
	_int_u id;
	if (reuse>dead)
		id = *(--reuse)-ports;
	else
		id = (next++)-ports;
	struct port *pt = ports+id;
	pt->flags = __flags;
	int flags = 0;
	if (is_flag(__flags, FF_PORT_CREAT))
		flags |= IPC_CREAT;
	if (is_flag(__flags, FF_PORT_SHMM))
		pt->shm_id = __id;
	pt->p = ffly_shmget(&pt->shm_id, __width+sizeof(struct hdr)+1+sizeof(mlock)+sizeof(_32_u),
		flags|S_IRUSR|S_IWUSR, is_flag(__flags, FF_PORT_SHMM)?FF_SHM_MCI:0);
	if (!pt->p) {
		ffly_fprintf(ffly_out, "pipe failure.\n");
	}

	pt->width = __width;
	_8_u *p = (_8_u*)pt->p;
	pt->bits = p++;
	pt->h = (hdrp)p;
	p+=sizeof(struct hdr);
	pt->buf = (void*)p;
	p+=__width;
	pt->tx_lock = (mlock*)p;
	p+=sizeof(mlock);
	pt->band = (_32_u*)p;

	if (is_flag(__flags, FF_PORT_CREAT)) {
		*pt->bits = 0x0;
		*pt->tx_lock = FFLY_MUTEX_INIT;
		*pt->band = ADRNULL;
	}
	return id;
}

void ffly_port_close(_int_u __id) {
	struct port *pt = get_port(__id);
	ffly_shmdt(pt->shm_id);
	if (is_flag(pt->flags, FF_PORT_CREAT))
		ffly_shm_free(pt->shm_id);
	ffly_shm_cleanup(pt->shm_id);
	discard(pt);
}

void static
port_snd(_int_u __port, void *__buf, _int_u __size) {
	struct port *pt = get_port(__port);
	set_bit(pt->bits, STRT);
	while(is_bit(pt->bits, STRT));

	_8_u *p = (_8_u*)__buf;
	_8_u *end = p+__size;
	while(p < end) {
		_int_u left = __size-(p-(_8_u*)__buf);
		_int_u size = left>pt->width?pt->width:left;
		bcopy(pt->buf, p, size);
		p+=size;
		pt->h->size = size;
		set_bit(pt->bits, DUMP);
		while(is_bit(pt->bits, DUMP));
	}

	set_bit(pt->bits, STOP);
	while(!is_bit(pt->bits, OK));
	clr_bit(pt->bits, OK);
}

void static
port_rcv(_int_u __port, void *__buf, _int_u __size) {
	struct port *pt = get_port(__port);
	while(is_bit(pt->bits, OK));
	while(!is_bit(pt->bits, STRT));
	clr_bit(pt->bits, STRT);

	_8_u *p = (_8_u*)__buf;
	_8_u *end = p+__size;
	while(!is_bit(pt->bits, STOP)) {
		if (is_bit(pt->bits, DUMP) && p < end) {
			_int_u size = pt->h->size;
			bcopy(p, pt->buf, size);
			p+=size;
			clr_bit(pt->bits, DUMP);
		}
	}
	clr_bit(pt->bits, STOP);
	set_bit(pt->bits, OK);
	while(is_bit(pt->bits, OK));
}


_f_err_t
ffly_port_send(_32_u __band, void *__buf, _int_u __size, _int_u __port) {
	struct port *pt = get_port(__port);
	mt_lock(pt->tx_lock);
	*pt->band = __band;
	port_snd(__port, __buf, __size);
	mt_unlock(pt->tx_lock);
	retok;
}

_8_i ffly_port_band(_int_u __port, _32_u __band) {
	return ((_8_i)(*get_port(__port)->band == __band))-1;
}

_f_err_t
ffly_port_recv(_32_u __band, void *__buf, _int_u __size, _int_u __port) {
	struct port *pt = get_port(__port);
	while(*pt->band != __band);
	port_rcv(__port, __buf, __size);
	retok;
}
