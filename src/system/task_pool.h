# ifndef __ffly__task__pool__h
# define __ffly__task__pool__h
# include "../y_int.h"
# include "servant.h"
# define max_servants 2
# define max_latency 1 //ms

/*
	TODO:
		get working and update
		NOTE:
			this is user side helping
			no where in the engine shall this be used
*/
typedef struct ffly_task_pool {
	ffly_servantp *servants;
	ffly_servantp *fresh;
} *ffly_task_poolp;

void ffly_task_pool_init(ffly_task_poolp, _int_u);
void ffly_task_pool_cleanup(ffly_task_poolp);
_f_err_t ffly_task_pool_add(ffly_task_poolp, _8_i(*)(void*), void*);

extern struct ffly_task_pool __ffly_task_pool__;
# endif /*__ffly__task__pool__h*/
