# ifndef __ffly__system__string__h
# define __ffly__system__string__h
# include "../y_int.h"
#define CUT_SHIFT 3
#define CUT_SIZE (1<<CUT_SHIFT)
#define CUT_MASK (CUT_SIZE-1)
#define islrc(__c) \
	((__c)>='a'&&(__c)<='z')
#define ishrc(__c) \
	((__c)>='A'&&(__c)<='Z')
# ifdef __cplusplus
extern "C" {
# endif

// string of x size to number
_64_u ffly_sntno(char*, _int_u);
_64_u ffly_stno(char*);
_int_u ffly_nots(_64_u, char*);
_int_u _ffly_nots(_64_u, _16_u*, _8_u*, void(*)(_8_u*, _16_u, _16_u, _8_u));
double ffly_stfloat(char*);
_int_u ffly_floatts(double, char*);
_int_u _ffly_floatts(double, _16_u*, _8_u*, void(*)(_8_u*, _16_u, _16_u, _8_u));
char ffly_tolow(char);
_64_u ffly_htint(char*);
_int_u ffly_noths(_64_u, char*);
_int_u _ffly_noths(_64_u, _16_u*, _8_u*, void(*)(_8_u*, _16_u, _16_u, _8_u));
_8_i ffly_islen(char*, _int_u);
# ifdef __cplusplus
}
# endif
# endif /*__ffly__system__string__h*/
