# ifndef __f_pms__h
# define __f_pms__h
# include "y_int.h"
# include "misc.h"
/*
	permanent storage - userface

	like hard storage

	NOTE:
	hard storage != permanent storage

	hard storage means a more solid storage other then memory 
*/
#define F_PMS_MAPPED F_ST_MAPPED
struct f_pms_ent {
	void *priv;
	_8_u flags;
};

struct f_pms_ent* f_pms_open(_64_u);
void f_pms_map(struct f_pms_ent*, _int_u);
void f_pms_remap(struct f_pms_ent*, _int_u);
void f_pms_unmap(struct f_pms_ent*);
void f_pms_close(struct f_pms_ent*);

void f_pms_pwrite(struct f_pms_ent*, void*, _int_u, _int_u);
void f_pms_pread(struct f_pms_ent*, void*, _int_u, _int_u);
# endif /*__f_pms__h*/
