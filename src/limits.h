#ifndef __y__limits__h
#define __y__limits__h

#define FLT_MAX		__FLT_MAX__
#define DBL_MAX		__DBL_MAX__
#define LDBL_MAX	__LDBL_MAX__
#endif/*__y__limits__h*/
