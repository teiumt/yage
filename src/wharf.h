# ifndef __f__wharf__h
# define __f__wharf__h
# include "y_int.h"
# include "types.h"
# include "linux/types.h"
# include "linux/socket.h"
# include "linux/in.h"
# include "in.h"
# include "linux/net.h"

typedef struct f_whf_con {
	int fd;
} *f_whf_conp;

struct f_whf_tc {
	_32_u addr;
};

struct f_whf_stat {
	_32_u tc, tc_n;
};

#define F_WHF_SYNC 0x01
struct f_whf_header {
	_8_u flags;
	_32_u size;
	_32_u space, n;
	_32_u data;
};

struct f_whf_space {
	_8_u id;
	_32_u addr;
};


struct f_whfl_info {
	_int_u blksz;
};

/*
i know my naming conventions are shit but i dont have time to be naming shit
*/
typedef struct f_whf_lake {
	struct f_whfl_info *info;
	void **p;
	_32_u off;
	_32_u page_c;
	_int_u rbsz;
} *f_whf_lakep;

struct f_whf_place {
	f_whf_lakep globl, local;
	struct f_whfl_info info;
};

struct f_whf_struc {
	_int_u tsz;
};

enum {
	_f_whf_disconnect,
	_f_whf_stat,
	_f_whf_load,
	_f_whf_store
};
void f_whf_send(f_whf_conp, void*, _int_u);
void f_whf_recv(f_whf_conp, void*, _int_u);

void f_whf_connect(void);
void f_whf_place(void);
void f_whf_disconnect(void);
# endif /*__f__wharf__h*/
