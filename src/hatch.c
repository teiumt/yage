# include "system/error.h"
# include "system/pipe.h"
# include "linux/types.h"
# include "linux/unistd.h"
# include "system/io.h"
# include "hatch.h"
# include "string.h"
# include "m_alloc.h"
# include "system/vec.h"
# include "system/map.h"
# include "system/flags.h"
# include "lib.h"
# include "system/thread.h"
# include "nail.h"
static char const tmpl[] = "/tmp/hatch.XXXXXX";

struct io_context {
	_int_u pipe;
};

static struct io_context ioct;

void static
_ht_init(void) {
	_f_err_t err;
	ioct.pipe = ffly_pipe(8, FF_PIPE_CREAT, 0, &err);
}


void static
_ht_de_init(void) {
	ffly_pipe_close(ioct.pipe);
}

_8_s static
_ht_listen(void) {
	if (_err(ffly_pipe_listen(ioct.pipe))) {
		return -1;
	}
	return 0;
}

_8_s static
_ht_write(void *__buf, _int_u __size) {
	if (_err(ffly_pipe_write(__buf, __size, ioct.pipe))) {
		return -1;
	}
	return 0;

}

_8_s static
_ht_read(void *__buf, _int_u __size) {
	if (_err(ffly_pipe_read(__buf, __size, ioct.pipe))) {
		return -1;
	}
	return 0;
}

_8_u static
rcvop(_f_err_t *__err){
	_8_u op = 0;
	printf("recving op.\n");
	_ht_read(&op, 1);
	*__err = F_SUCCESS;
	printf("recved.\n");
	return op;
} 

# define flg_sep if (p != buf) *p++ = ',';
char static*
vec_flags(ffly_vecp __vec) {
	char static buf[200];
	char *p = buf;
	if (ffly_is_flag(__vec->flags, VEC_AUTO_RESIZE))
		p+=str_cpy(p, "auto-resize");
	if (ffly_is_flag(__vec->flags, VEC_BLK_CHAIN)) {
		flg_sep
		p+=str_cpy(p, "block-chain");
	}
	if (ffly_is_flag(__vec->flags, VEC_UUU_BLKS)) {
		flg_sep
		p+=str_cpy(p, "uuu-blocks");
	}
	if (ffly_is_flag(__vec->flags, VEC_NONCONTINUOUS)) {
		flg_sep
		p+=str_cpy(p, "noncontinuous");
	}
	*p = '\0';
	return buf;
}

static _8_s _sndbte(_8_u __byte) {
	return _ht_write(&__byte, 1);
}

_f_err_t static
lsvec(_int_u __pipe) {
	struct f_nail *n;
	ffly_vecp p;
	if (!(p = f_nailat(F_NW_VEC))) {
		printf("nothing to be sent.\n");
		_sndbte(-1);
		retok;
	}

	if (_err(_sndbte(0)))
		reterr;
	_int_u l = 1024;
	char *buf = (char*)m_alloc(l);
	if (_err(_ht_write(&l, sizeof(_int_u))))
		reterr;
	_int_u i = 0;
	while(n != NULL) {
		p = (ffly_vecp)n->ptr;
		ffly_sprintf(buf, "vec %u; size: %u, page_c: %u, blk_size: %u - off: %u, flags: %s\n",
			i, p->size, p->page_c, p->blk_size, p->off, vec_flags(p));
		printf("sent: %s\n", buf);
		if (_err(_ht_write(buf, l)))
			reterr;
		n = n->next;
		if (_err(_sndbte(!p?-1:0)))
			reterr;
	} 
	m_free(buf);
	retok;
}

void static
lsmap(_int_u __pipe) {

}

_int_u static pipe;
_f_tid_t static thread;

_8_i static live = -1;

void static*
hatch(void *__arg_p) {
	char file[sizeof(tmpl)];
	str_cpy(file, tmpl);
	int fd;
	if ((fd = ff_mktemp(file)) == -1) {
		return NULL;
	}

	_f_err_t err;
	printf("key: %s\n", file); 
	_ht_init();
	/*
		put flags
	*/
	_int_u shm_id = ffly_pipe_get_shmid(ioct.pipe);
	write(fd, &shm_id, sizeof(_int_u));

	_8_u op;
_bk:
	live = 0;
	if (_err(_ht_listen())) {
		printf("failed to listen.\n");
		goto _end;
	}
_again:
	op = rcvop(&err);
	if (_err(err))
		goto _end;
	if (op == _ffly_ho_login) {
		/*
			recv hashed password then send a session key
		*/
	} else if (op == _ffly_ho_logout) {
			
	} else if (op == _ffly_ho_meminfo) {
		struct ffly_meminfo info;
		ffly_meminfo(&info);
		if (_err(_ht_write(&info, sizeof(struct ffly_meminfo))))
			goto _end;
	} else if (op == _ffly_ho_disconnect)
		goto _bk;  
	else if (op == _ffly_ho_shutdown) {
		printf("recved shutdown command.\n");
		goto _end;
	} else {
		switch(op) {
			case _ffly_ho_lsvec: lsvec(pipe); break;
			case _ffly_ho_lsmap: lsmap(pipe); break;
			default:
				printf("unknown.\n");
			goto _end;
		}
	}
	goto _again;
_end:
	_ht_de_init();
	unlink(file);
	return NULL;
}

_f_err_t
ffly_hatch_start() {
	ffly_thread_create(&thread, hatch, NULL);
	while(live<0);
}

void ffly_hatch_shutoff() {
	ffly_pipe_shutoff(ioct.pipe);
}

void ffly_hatch_wait() {
	ffly_thread_wait(thread);
}

/*
# include "system/vec.h"
_f_err_t ffmain(int __argc, char **__argv) {
//	_f_err_t err;
//	ffly_vecp p = ffly_vec(1, VEC_AUTO_RESIZE|VEC_NONCONTINUOUS, &err);
//	ffly_hatch_run();
//	ffly_vec_destroy(p);
	ffly_hatch_start();
	printf("shutoff pipe.\n");
	ffly_hatch_shutoff();

	ffly_hatch_wait();
	retok;
}*/
