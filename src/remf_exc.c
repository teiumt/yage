# include "exec.h"
# include "remf.h"
# include "memory/mem_alloc.h"
# include "memory/mem_free.h"
# include "resin/exec.h"
# include "rdm.h"
# include "hexdump.h"
# include "system/io.h"
void f_remf_exec(struct f_exc_ctx *__ctx) {
	struct remf_hdr *hdr = __ctx->hdr;
	hdr = (struct remf_hdr*)__ctx->hdr;
	if (*hdr->ident != FF_REMF_MAG0) {
		f_printf("REMF_EXC' mag0 corrupted.\n");
		return;
	}

	if (hdr->ident[1] != FF_REMF_MAG1) {
		f_printf("REMF_EXC' mag1 corrupted.\n");
		return;
	}

	if (hdr->ident[2] != FF_REMF_MAG2) {
		f_printf("REMF_EXC' mag2 corrupted.\n");
		return;
	}

	if (hdr->ident[3] != FF_REMF_MAG3) {
		f_printf("REMF_EXC' mag3 corrupted.\n");
		return;
	}
	f_exc_segmentp ps = NULL;
	f_exc_su(__ctx, hdr->adr);
	f_printf("REMF_HEADER' ADR: %u, SSTS: %u, SG: %u\n", hdr->adr, hdr->ssts, hdr->sg);
	if (hdr->sg != FF_REMF_NULL) {
		remf_seg_hdrp s, __s;
		f_exc_segmentp _s;
		ps = (f_exc_segmentp)__f_mem_alloc(hdr->sn*sizeof(struct f_exc_segment));
		s = (remf_seg_hdrp)__f_mem_alloc(hdr->ssts);
		f_exc_read(__ctx, s, hdr->ssts, hdr->sg);
		_int_u i;
		i = 0;
		for(;i != hdr->sn;i++) {
			_s = ps+i;
			__s = s+i;

			_s->adr = __s->adr;
			_s->offset = __s->offset;
			_s->size = __s->sz;
			f_printf("REMF_EXC' SEGMENT' adr: %u, offset: %u, size: %u.\n", _s->adr, _s->offset, _s->size);
		}

		__f_mem_free(s);
		f_exc_ldsegs(__ctx, ps, hdr->sn);
	}
	ffly_hexdump(__ctx->p, hdr->adr);
	f_printf("ENTRY_POINT: %u.\n", hdr->routine);
	switch(hdr->id) {
		case REMF_ID_RES:
			f_rdmp(__ctx->p, hdr->adr);
			f_rsexc(__ctx, hdr->adr, hdr->routine);
		break;
	}
}
