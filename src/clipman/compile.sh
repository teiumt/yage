rm -f *.o
root_dir=$(realpath ../)
cc_flags="-std=c99 -D__ffly_crucial -D__noengine"
dst_dir=$root_dir
cd ../ && . ./compile.sh && cd clipman
ffly_objs="$ffly_objs"
gcc $cc_flags -o client client.c $ffly_objs -nostdlib
gcc $cc_flags -o clipman clipman.c $ffly_objs -nostdlib
