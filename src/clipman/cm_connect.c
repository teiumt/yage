#include "clipman.h"
#include "../tug/tug.h"
static struct tug_conn con;
void cm_connect() {
	if(tug_connect(&con) == -1) {
		return;
	}
	tug_establish(&con,~(_64_u)0);
	tug_hello(&con);
}

void cm_set(_8_u *__buf,_int_u __size) {
	struct tug_pkt pkt;
	struct cm_msg m;

	m.type = CM_SET;
	pkt.size = sizeof(struct cm_msg);
	pkt.to = 0;
	pkt.from = con.bay;
	tug_pktsnd(&con,&pkt,&m);

	pkt.size = __size;
	tug_pktsnd(&con,&pkt,__buf);

}

void cm_get(_8_u *__buf,_int_u *__size) {
	struct tug_pkt pkt;
	struct cm_msg m;

	m.type = CM_GET;
	pkt.size = sizeof(struct cm_msg);
	pkt.to = 0;
	pkt.from = con.bay;
	tug_pktsnd(&con,&pkt,&m);

	tug_pktrcv(&con,&pkt,__buf);
	*__size = pkt.size;
}
void cm_disconnect(void) {
	tug_lullaby(&con);
	tug_disconnect(&con);
}
