#include "clipman.h"
#include "../tug/tug.h"
#include "../m_alloc.h"
#include "../io.h"
#include "../types.h"
#include "../signal.h"
static _8_s running = 0;
void static sig_handler(int s){
	running = -1;
}
static struct tug_conn con;
static _8_u clipspace[1024];
static _int_u clipsize = 0;
#include "../tools.h"
struct tug_pkt _pkt;
void static _cm_set(void) {
	struct tug_pkt pkt;
	tug_pktrcv(&con,&pkt,clipspace);
	ffly_chrdump(clipspace,1024);

	ffly_fdrain(_ffly_out);
	clipsize = pkt.size;
}
void static _cm_get(void) {
	struct tug_pkt pkt;
	pkt.to = _pkt.from;
	pkt.from = con.bay;
	pkt.size = clipsize;
	tug_pktsnd(&con,&pkt,clipspace);
}
void clipman_main(void) {
	struct sigaction sig;
	sig.sa_handler = sig_handler;
	sigemptyset(&sig.sa_mask);
	sig.sa_flags = 0;
	sigaction(SIGINT,&sig,NULL);

	while(!running) {
		struct cm_msg m;
		tug_pktrcv(&con,&_pkt,&m);
		printf("RECVED MESSAGE: type: %u\n",m.type);
		ffly_fdrain(_ffly_out);
		if (m.type == CM_SET) {
			_cm_set();
		}else if (m.type == CM_GET) {
			_cm_get();
		}
	}
}

_y_err main(int __argc, char const **__argv) {
	if(tug_connect(&con) == -1) {
		printf("failed to connect.\n");
		return -1;
	}

	tug_establish(&con,0);
	tug_hello(&con);

	clipman_main();
	tug_lullaby(&con);
	tug_disconnect(&con);

}
