#include "../types.h"
#include "clipman.h"
#include "../string.h"
#include "../tools.h"
_y_err main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		return -1;
	}
	cm_connect();
	char c = __argv[1][0];
	if (c == 's') {
		if (__argc>2) {
			cm_set(__argv[2],str_len(__argv[2]));
		}
	}else if(c == 'g') {
		char buf[1024];
		_int_u size = 0;
		cm_get(buf,&size);
		ffly_chrdump(buf,size);
	}

	cm_disconnect();
	return 0;
}
