#include "../remf.h"
#include "../io.h"
#include "../string.h"
#include "../m_alloc.h"
#include "../linux/stat.h"
#include "../linux/fcntl.h"
#include "../linux/unistd.h"
#include "../y_int.h"
#include "../tools.h"
#include "../rdm.h"
int main(int __argc, char const *__argv[]) {
	if (__argc<2) {
		printf("no file provided.");
		return -1;
	}
	int fd;
	fd = open(__argv[1],O_RDONLY,0);
	
	struct remf_hdr hdr;
	read(fd, &hdr, remf_hdrsz);

	if (hdr.rg != FF_REMF_NULL) {

	struct remf_reg_hdr *rh, *r;
	rh = (struct remf_reg_hdr*)m_alloc(hdr.rsts);
	pread(fd, rh, hdr.rsts, hdr.rg);

	_int_u i;
	i = 0;
	for(;i != hdr.rn;i++) {
		r = rh+i;
		
		if (i>F_RG_RLT) {	
		void *p = m_alloc(r->size);
		pread(fd, p, r->size, r->offset);
		ffly_hexdump(p,r->size);
		rdmp(p,r->size);
		}
	}

	}
	close (fd);
}

