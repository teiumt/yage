# ifndef __pcore__h
# define __pcore__h
# include "y_int.h"
/*
	primary core
*/
# include "sched_struc.h"
#include "vat.h"
struct pcore {
	struct sched_core sched;
	struct y_vat *vat;
	_64_u delay;
	_32_u id;
	_64_u sig;
};

# endif /*__pcore__h*/
