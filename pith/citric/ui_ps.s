mov32s %m#0,%s#8
intrp_p1f32'x4 %v#12,%v#0
intrp_p1f32'y4 %v#13,%v#0
intrp_p2f32'x4 %v#12,%v#1
intrp_p2f32'y4 %v#13,%v#1
s_ldw,4 0,%s#12,%s#0
waitcnt 0
;VOP1
cvt_i32f32 %v#12,%v#12
;VOP1
cvt_i32f32 %v#13,%v#13
image_load %v#12,%v#8,%s#3,%s#0
waitcnt 0
mov32s %m#0,%s#8
intrp_p1f32'x1 %v#12,%v#0
intrp_p2f32'x1 %v#12,%v#1
intrp_p1f32'y1 %v#13,%v#0
intrp_p2f32'y1 %v#13,%v#1
intrp_p1f32'z1 %v#14,%v#0
intrp_p2f32'z1 %v#14,%v#1
intrp_p1f32'w1 %v#15,%v#0
intrp_p2f32'w1 %v#15,%v#1
;LOAD
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 0,0,%v#16,%s#3,0
waitcnt 0
;VOP2
mul32f %v#12,%v#16,%v#20
;VOP2
mul32f %v#13,%v#17,%v#21
;VOP2
mul32f %v#14,%v#18,%v#22
;VOP2
mul32f %v#15,%v#19,%v#23
mov32v %v#16,%v#20
mov32v %v#17,%v#21
mov32v %v#18,%v#22
mov32v %v#19,%v#23
;VOP2
mul32f %v#8,%v#16,%v#20
;VOP2
mul32f %v#9,%v#17,%v#21
;VOP2
mul32f %v#10,%v#18,%v#22
;VOP2
mul32f %v#11,%v#19,%v#23
mov32v %v#16,%v#20
mov32v %v#17,%v#21
mov32v %v#18,%v#22
mov32v %v#19,%v#23
;VOP3A
cvt_pkrtz_f16_f32 %v#4,%v#16,%v#17
;VOP3A
cvt_pkrtz_f16_f32 %v#5,%v#18,%v#19
expw %mrt#0,%v#4,%v#5,!,!`
waitcnt 0
end 0
