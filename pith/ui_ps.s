mov32s %m#0,%s#8
intrp_p1f32'x4 %v#12,%v#0
intrp_p2f32'x4 %v#12,%v#1
intrp_p1f32'y4 %v#13,%v#0
intrp_p2f32'y4 %v#13,%v#1
s_ldw,4 0,%s#12,%s#0
waitcnt 0
;VOP1
cvt_i32f32 %v#12,%v#12
;VOP1
cvt_i32f32 %v#13,%v#13
image_load %v#12,%v#8,%s#3,%s#0
waitcnt 0
;LOAD
s_ldw,4 0,%s#12,%s#2
waitcnt 0
u_ldw,4 0,0,%v#12,%s#3,0
waitcnt 0
;VOP2
mul32f %v#8,%v#12,%v#16
;VOP2
mul32f %v#9,%v#13,%v#17
;VOP2
mul32f %v#10,%v#14,%v#18
;VOP2
mul32f %v#11,%v#15,%v#19
mov32v %v#12,%v#16
mov32v %v#13,%v#17
mov32v %v#14,%v#18
mov32v %v#15,%v#19
;VOP3A
cvt_pkrtz_f16_f32 %v#4,%v#12,%v#13
;VOP3A
cvt_pkrtz_f16_f32 %v#5,%v#14,%v#15
expw %mrt#0,%v#4,%v#5,!,!`
waitcnt 0
end 0
