;VOP1
mov32v %v#8,0.0
;VOP1
mov32v %v#9,-512.0
;VOP1
mov32v %v#10,0.0
;VOP1
mov32v %v#11,0.0
mov32s %m#0,%s#8
intrp_p1f32'x0 %v#12,%v#0
intrp_p2f32'x0 %v#12,%v#1
intrp_p1f32'y0 %v#13,%v#0
intrp_p2f32'y0 %v#13,%v#1
intrp_p1f32'z0 %v#14,%v#0
intrp_p2f32'z0 %v#14,%v#1
intrp_p1f32'w0 %v#15,%v#0
intrp_p2f32'w0 %v#15,%v#1
mov32s %m#0,%s#8
intrp_p1f32'x1 %v#16,%v#0
intrp_p2f32'x1 %v#16,%v#1
intrp_p1f32'y1 %v#17,%v#0
intrp_p2f32'y1 %v#17,%v#1
intrp_p1f32'z1 %v#18,%v#0
intrp_p2f32'z1 %v#18,%v#1
intrp_p1f32'w1 %v#19,%v#0
intrp_p2f32'w1 %v#19,%v#1
;VOP2
sub32f %v#8,%v#16,%v#20
;VOP2
sub32f %v#9,%v#17,%v#21
;VOP2
sub32f %v#10,%v#18,%v#22
;VOP2
sub32f %v#11,%v#19,%v#23
mov32v %v#8,%v#20
mov32v %v#9,%v#21
mov32v %v#10,%v#22
mov32v %v#11,%v#23
;VOP2
mul32f %v#12,%v#12,%v#4
;VOP2
mac32f %v#13,%v#13,%v#4
;VOP2
mac32f %v#14,%v#14,%v#4
;VOP2
mac32f %v#15,%v#15,%v#4
;VOP1
rsq32f %v#4,%v#4
;VOP2
mul32f %v#12,%v#4,%v#20
;VOP2
mul32f %v#13,%v#4,%v#21
;VOP2
mul32f %v#14,%v#4,%v#22
;VOP2
mul32f %v#15,%v#4,%v#23
mov32v %v#12,%v#20
mov32v %v#13,%v#21
mov32v %v#14,%v#22
mov32v %v#15,%v#23
;VOP2
mul32f %v#8,%v#8,%v#4
;VOP2
mac32f %v#9,%v#9,%v#4
;VOP2
mac32f %v#10,%v#10,%v#4
;VOP2
mac32f %v#11,%v#11,%v#4
;VOP1
rsq32f %v#4,%v#4
;VOP2
mul32f %v#8,%v#4,%v#16
;VOP2
mul32f %v#9,%v#4,%v#17
;VOP2
mul32f %v#10,%v#4,%v#18
;VOP2
mul32f %v#11,%v#4,%v#19
mov32v %v#20,%v#16
mov32v %v#21,%v#17
mov32v %v#22,%v#18
mov32v %v#23,%v#19
;VOP2
mul32f %v#12,%v#20,%v#8
;VOP2
mac32f %v#13,%v#21,%v#8
;VOP2
mac32f %v#14,%v#22,%v#8
;VOP2
mac32f %v#15,%v#23,%v#8
;VOP1
mov32v %v#16,%v#8
;VOP2
max32f 0.0,%v#16,%v#8
;VOP1
mov32v %v#16,%v#8
;VOP2
min32f 1.0,%v#16,%v#8
;VOP1
mov32v %v#16,%v#8
mov32s %m#0,%s#8
intrp_p1f32'x2 %v#16,%v#0
intrp_p1f32'y2 %v#17,%v#0
intrp_p2f32'x2 %v#16,%v#1
intrp_p2f32'y2 %v#17,%v#1
s_ldw,4 48,%s#12,%s#0
waitcnt 0
s_ldw,4 64,%s#16,%s#0
waitcnt 0
image_sample %v#16,%v#8,%s#3,%s#4
waitcnt 0
;VOP1
mov32v %v#11,1.0
;VOP3A
cvt_pkrtz_f16_f32 %v#4,%v#8,%v#9
;VOP3A
cvt_pkrtz_f16_f32 %v#5,%v#10,%v#11
expw %mrt#0,%v#4,%v#5,!,!`
waitcnt 0
end 0
