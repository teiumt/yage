[![YAGE](/logo.png)](https://gitlab.com/mrdoomlittle/yage)
##### YAGE has been an ongoing project, with the main objective of creating a game.
##### Yes, this is abit overkill for a game but there are other objective then just that.
##### project includes, sub projects such as a C-compiler and assembler, to a MCU-PIC emulator and graphics shader 
##### compiler and assembler. Theres VEKAS display server, and FLUE graphics for GPU rendering.
##### it's a small project, but has alot of things going on behind the scenes.
##### it may never be completed, but is fun to experiment with.
##### ignore some files that seem out of place as theres a lot of old files that are not used anymore and are still present.
##### see yage-docs for more information.
##### yage = yagé
