/*
# A
this approch is very good in that the address of 'int x' is already known
*/
int x;
void test(void){
	int i;
	i = 0;
	while(i != 100){
		x+=i;
		i++;
	}
}
int main(){
	int _x;
	_x = 45;

	x = _x;
	test();
	test();
	_x = x;
}
/*
# B
very bad, and very slow. here we are moving a pointer from memory to a register then operating on it using add instruction.
*/
void test(int *x){
	int i;
	i = 0;
	while(i != 100){
		*x+=i;
		i++;
	}
}
int main(){
	int x;
	test(&x);
	test(&x);
}

/*
# C
much better then (B) but not as good as (A) as this stores and loads x, moving it from pointer to stack(4 times)
meaning each call to test() degrades the performance unlike (A)
*/
void test(int *x){
	int _x;
	_x = *x;
	int i;
	i = 0;
	while(i != 100){
		_x+=i;
		i++;
	}

	*x = _x;
}

int main(){
	int x;
	test(&x);
	test(&x);
}
