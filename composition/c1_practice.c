/*
	in this file we give an example usage of things.

	EXAMPLE: text processor
*/


/*
	where all are belongings are kept
*/
struct match{
	char buf[64];
	_int_u len;
};

struct context{
	struct match matlist[64];
	_int_u n_matches;
	char const *text;
	_int_u pos;
};

struct context *ctx;
struct match matlist[64];
_int_u n_matches;

_int_u ctx_pos;
/*
	find a specific word
*/
void add_match(char const *__keywd){
	str_cpy(ctx->matlist[ctx->n_matches].buf.buf,__keywd);
	ctx->matlist[ctx->n_matches].len = str_len(__keywd);
	ctx->n_matches++;
}

_8_s is_match(struct match *__m){
	if(!mem_cmp(__m->buf,ctx->text+ctx_pos,__m->len)){
		return 0;
	}
	return -1;
}


/*
	accessing 'pos' is slow.
	
	here you can see where this routine starts to fall apart.
	we are accessing the elements of the 'ctx' pointer many many times.

	there is another issue with this as well. matlist.
	if matlist were a pointer then we would be accessing matlist though two pointers(VERY FUCKING BAD).
*/
void process_textA(void){
	while(ctx->pos != 100){
		_int_u i;
		i = 0;
		for(;i != ctx->n_matches;i++){
			if(!is_match(&ctx->matlist[i])){

			}
		}

		ctx->pos++;
	}
}

/*
	this is alot better, this avoids access though pointer.
	however this now becomes an issue as 'struct match'*64 
	is alot of memory.
	if the user wanted to keep a context then this data would have to be copyed over(every time).
*/
void process_textB(void){
	while(ctx_pos != 100){
		_int_u i;
		i = 0;
		for(;i != n_matches;i++){
			if(!is_match(&matlist[i])){

			}
		}

		ctx_pos++;
	}
	ctx->pos = ctx_pos;
}

/*
	CONCLUSION:
	pointers are great, but do have there downsides.
	in the future its most likly that registers might become abundant.
	and accessing pointers though registers is very good compared to 
	though memory(no double deref).

	however pointers wont be faster then general memory space.
	as accessing data directly and not though a pointer is always going to be better.


	the question is how to deal with the ctx pointer? im thinking of thing like this

	struct context{
	};

	struct context_desc{
		struct context *ptr;
	};

	struct context_desc ctx;

	i realy dont know :(
*/
int main(){
	struct context ct;
	ctx = &ct;
	add_match("find_me");
	process_text();
}
