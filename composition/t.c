/*
  okay this is a stretch but.

  there are two options.

  A)
  struct structure *s;

  void work(){
    while(1){
      s->test++;
    }
  }

  or 
  B)

  struct structure s;

  void work(){
    while(1){
      s.test++;
    }
  }

  
  the best here is (B) as it avoids pointers.
  however B comes with downside that it will need to be copyed.
  where as A can just be replaced with a fresh structure.

  in either case (B) comes out on top as pointers can be just put inside.
  and we can just copy.
*/
struct other{
	_64_u *data;
};

struct test{
	struct other *oth;
};

struct test *ts;

// is a fucking issue
void set_data(_64_u __data, _64_u __idx){
	ts->oth->data[__idx] = __data;
}

// still an issue
void set_data(struct test *ts,_64_u __data, _64_u __idx){
	ts->oth->data[__idx] = __data;
}

int main(){
	struct test _test;
	ts = &_test;
	while(1){
		set_data(&_test,0,0);
		set_data(0,0);
	}
}
