struct test{
	// data representing the structures real size
	int data[4096];
};

/*
	this routine prevails on speed. however the big issue here is the copying of the structure.
	its size reflects the speed of this routine.

	for example calling this routine repeatedly would hit overall performance.
	in compareason testB() would be much faster.

	NOTE:
		this measure of speed depends on size of the structure.
		in the case the structure is very small testA() would win.
		however once it gets to a specific size testB() takes the lead.
*/
void testA(struct test *a){
	struct test t;
	t = *a;
	int i;
	i = 0;
	while(i != 4096){
		t.data[0]+=i;
		i++;
	}

	*a = t;
}

/*
	this routine is very good,
	however when it comes to accessing the same place repeatedly
	this becomes slow.

	for this routine to be efficent. we have to be accessing things in a
	unorderly way.
*/
void testB(struct test *a){
	int i;
	i = 0;
	while(i != 4096){
		a->data[0]+=i;
		i++;
	}
}

int main(){
	struct test a;
	test(&a);
}
